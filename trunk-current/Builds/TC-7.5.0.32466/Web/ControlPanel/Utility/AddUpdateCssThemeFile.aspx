﻿<%@ Page Language="C#" AutoEventWireup="false" EnableViewState="false" CodeBehind="AddUpdateCssThemeFile.aspx.cs" Inherits="Telligent.Evolution.Web.ControlPanel.Utility.AddUpdateCssThemeFile" MasterPageFile="~/ControlPanel/Masters/Modal.master" %>


<asp:Content ContentPlaceHolderId="bcr" runat="Server">

    <TEControl:Title runat="server" ResourceName="AddUpdateThemeFile_Title" ResourceFile="ControlPanelResources.xml" IncludeSiteName="false" IncludeSectionOrHubName="false" />

	<div class="CommonContentArea">
	    <div class="CommonContent">
            <div class="CommonDescription">
                <CP:ResourceControl ResourceName="AddUpdateThemeFile_Instructions" runat="server" />
            </div>
            <div class="CommonFormArea">
            
                <TEControl:WrappedLiteral runat="server" ID="ErrorMessage" Tag="Div" CssClass="CommonMessageError" />
            
                <div class="CommonFormFieldName">
                    <strong><CP:ResourceControl ResourceName="AddUpdateThemeFile_File" runat="server" /></strong>
                </div>
                <div class="CommonFormField">
					<asp:Literal ID="ExistingFile" runat="server" />
					<TWC:FileUpload id="File" runat="server" />
				</div>

				<div class="CommonFormField">
					<asp:CheckBox ID="NonModal" runat="server" />
				</div>

				<div class="CommonFormField">
					<asp:CheckBox ID="Modal" runat="server" />
				</div>

				<div class="CommonFormField">
					<asp:CheckBox ID="AuthorizationRequest" runat="server" />
				</div>

				<div class="CommonFormField">
					<asp:CheckBox ID="IE" runat="server" />
					<asp:TextBox runat="server" ID="IEVersion" Columns="4" />
					<asp:RangeValidator ID="IERangeValidator" ControlToValidate="IEVersion" MinimumValue="0" MaximumValue="100" runat="server" Text="*" Type="Integer" />
				</div>

				<div class="CommonFormField">
					<asp:CheckBox ID="Media" runat="server" />
					<asp:TextBox runat="server" ID="MediaQuery" Columns="30" />
				</div>
                    
            </div>
	                    
	        <div class="CommonFormArea">
                <div class="CommonFormField PanelSaveButton">
                    <TEControl:ResourceButton runat="server" ID="Save" ResourceName="Save" OnClick="Save_Click" />
                </div>
            </div>
        </div>
    </div>

</asp:Content>
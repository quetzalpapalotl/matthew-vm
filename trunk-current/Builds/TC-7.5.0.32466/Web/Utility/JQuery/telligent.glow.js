/// @name glowColorSelector
/// @category jQuery Plugin
/// @description Decorates a text input with a color selector
/// 
/// ### jQuery.fn.glowColorSelector
/// 
/// This plugin decorates a text input with a popup color selection tool.
/// 
/// ### Usage
/// 
/// Initiate a selection of text inputs to display a color selection tool when focused.
/// 
///     $('selector').glowColorSelector(options)
/// 
/// where selector contains `<input type="text" />` element(s) and `options` is an optional object argument
/// 
/// ### Options
/// 
/// * `blendImageUrl`: image used as the background for color blending.  *Evolution pre-sets intelligent defaults*
/// * `hueBlendImageUrl`: image used as the background for hue blending.  *Evolution pre-sets intelligent defaults*
/// 
/// ### Events
/// 
/// * `change`: triggered on color selection
/// 
/// ### Methods
/// 
/// #### val
/// 
/// Retrieves and/or sets the current value of the color selector.  The value of the selected text input is also updated by the color selector.
/// 
/// 	// get the current color
///     var value = $('selector').glowColorSelector('val');
/// 
/// 	// set the current color to red
/// 	$('selector').glowColorSelector('val', '#FF0000')
/// 
/// #### disabled
/// 
/// Retrieves and/or sets whether the color selection popup is disabled.  When enabled, focusing on the input will pop open the color selector.  When disabled, it will not.
/// 
/// 	// get the current disabled state
///     var isDisabled = $('selector').glowColorSelector('disabled');
/// 
/// 	// set the disabled state
/// 	$('selector').glowColorSelector('disabled', false);
/// 
/// 

(function($, global){
    // public api
    var api = {
        val: function(value) {
            return this.val(value);            
        },
        disabled: function(isDisabled) {
            if(typeof isDisabled !== 'undefined') {
                this.filter('input').each(function(){                    
                    var input = $(this);
                    if(!isDisabled && _isDisabled(input)) {
                        _enable(input);
                    } else if (isDisabled && !_isDisabled(input)) {
                        _disable(input);
                    } 
                });
            }
            return this.is('input:disabled');
        }
    };
    
    // private methods    
    var _util = null,
        _colorPicker = null,
        _eventNameSpace = '.glowColorSelector',
        _init = function(options) {
            var settings = $.extend({}, $.fn.glowColorSelector.defaults, options || {});
            // only 1 actual picker will exist per page. if it doesn't exist yet, build one
            if(_colorPicker === null) {
                _util = $.telligent.glow.utility;
                _colorPicker = _buildPicker(settings);                
            }
            return this.each(function() {
                _initialize($(this));
            });            
        },
        _buildPicker = function(settings) {
            // base object
            var picker = {
                blendImageUrl: settings.blendImageUrl,
                hueBlendImageUrl: settings.hueBlendImageUrl,
                moveStartX: null,
                moveStartY: null,
                elementMoveStartX: null,
                elementMoveStartY: null                
            };
            
            // build ui elements

            // container
            var container = $('<div></div>')
                .css({
                    width: '290px',
                    padding: '2px',
                    paddingLeft: '6px',
                    borderStyle: 'solid',
                    borderWidth: '1px',
                    borderColor: '#000000',
                    backgroundColor: '#eeeeee' });
                        
            // pop up panel
            picker.popupPanel = $('<div></div>')
                .glowPopUpPanel({
                    cssClass:'',
                    position:'updown',
                    zIndex:1000,
                    hideOnDocumentClick:false })
                .glowPopUpPanel('empty')
                .glowPopUpPanel('append', container);
            
            // background
            picker.selectBoxBackground = $('<div></div>')
                .css({
                    width: '256px',
                    height: '256px',
                    borderWidth: '1px',
                    borderStyle: 'solid',
                    borderColor: '#000000',
                    padding: '0px',
                    cssFloat: 'left' })
                .appendTo(container);
            
            // select box
            picker.selectBox = $('<div></div>')
                .css({
                    width: '256px',
                    height: '256px',
                    cssFloat: 'left',
                    padding: '0px',
                    cursor: 'pointer' })
                .bind('mousedown' + _eventNameSpace, function(e){
                    picker.clickValueSaturationHandler(e); 
                    return false;
                    })
                .appendTo(picker.selectBoxBackground);          
            if (_util.isIE()) {
                picker.selectBox.css({
                    filter:'progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled=true,src=\'' + picker.blendImageUrl + '\')'
                });             
            } else {
                picker.selectBox.css({
                    backgroundImage: 'url(\'' + picker.blendImageUrl + '\')',
                    backgroundRepeat: 'no-repeat',
                    backgroundPosition: 'top left'
                });
            }
            
            // cursor
            picker.selectBoxCursor = $('<div></div>')
                .css({
                    position: 'absolute',
                    borderWidth: '3px',
                    borderStyle: 'solid',
                    borderColor: '#666666',
                    height: '7px',
                    width: '7px',
                    zIndex: 1000,
                    left: '0px',
                    top: '0px',
                    marginLeft: '2px',
                    marginTop: '-1px',
                    cursor: 'pointer',
                    filter: 'none' })
                .bind('mousedown' + _eventNameSpace, function(e){
                    picker.cursorMouseDownHandler(e);                
                    })
                .appendTo(picker.selectBoxBackground);

            // e ??
            $('<div></div>')
                .css({
                    position: 'absolute',
                    borderWidth: '3px',
                    borderStyle: 'solid',
                    borderColor: '#ffffff',
                    height: '1px',
                    width: '1px',
                    overflow: 'hidden' })
                .appendTo(picker.selectBoxCursor);

            // hue select box
            picker.hueSelectBox = $('<div></div>')
                .css({
                    width: '20px',
                    height: '256px',
                    lineHeight: '256px',
                    boderStyle: 'solid',
                    borderWidth: '1px',
                    borderColor: '#666666',
                    marginLeft: '5px',
                    cssFloat: 'left',
                    cursor: 'default',
                    backgroundImage: 'url(\'' + picker.hueBlendImageUrl + '\')' })
                .appendTo(container);

            // hueSelectBoxChild
            var hueSelectBoxChild = $('<div></div>')
                .css({
                    position: 'relative',
                    width: '20px',
                    height: '256px'
                    })
                .appendTo(picker.hueSelectBox);

            // hue select cursor
            picker.hueSelectCursor = $('<div></div>')
                .css({
                    position: 'absolute',
                    borderStyle: 'solid',
                    borderWidth: '3px',
                    borderColor: '#666666',
                    height: '5px',
                    width: '22px',
                    marginLeft: '-4px',
                    marginTop: '-5px',
                    left: '0px',
                    top: '0px',
                    cursor: 'pointer' })
                .bind('mousedown' + _eventNameSpace, function(e){
                    picker.hueCursorMouseDownHandler(e);
                    })
                .appendTo(hueSelectBoxChild);

            // hue picker child
            $('<div></div>')
                .css({
                    position: 'absolute',
                    borderWidth: '2px',
                    borderStyle: 'solid',
                    borderColor: '#ffffff',
                    height: '1px',
                    width: '18px',
                    overflow: 'hidden' })
                .appendTo(picker.hueSelectCursor);

            // selectedColor input
            picker.selectedColor = $('<input type="text" />')
                .val('#ffffff')
                .css({
                    clear: 'both',
                    width: '279px',
                    textAlign: 'center',
                    fontFamily: 'Courier New, Courier',
                    fontWeight: 'bold',
                    padding: '0px',
                    margin: '4px',
                    marginLeft: 'auto',
                    marginRight: 'auto' })
                .bind('keyup' + _eventNameSpace, function(e) {
                        picker.parseEnteredColor(); 
                        return true;
                    })
                .bind('change' + _eventNameSpace, function(e) {
                        picker.updateTemporaryValue(true); 
                        return true;
                    })
                .appendTo(container);

            // select button
            picker.selectButton = $('<input type="button" />')
                .val('OK')
                .css({
                    width: '92px',
                    padding: '0px',
                    margin: '0px' })
                .bind('click' + _eventNameSpace, function(e){
                    picker.selectTemporaryColor(); 
                    return false;
                    })
                .appendTo(container);

            // clear button
            picker.clearButton = $('<input type="button" />')
                .val('Clear')
                .css({
                    width: '92px',
                    padding: '0px',
                    margin: '0px',
                    marginLeft: '4px' })
                .bind('click' + _eventNameSpace, function(e){
                    picker.selectNoColor(); 
                    return false;
                    })
                .appendTo(container);

            // cancel button
            picker.cancelButton = $('<input type="button" />')
                .val('Cancel')
                .css({
                    width: '92px',
                    padding: '0px',
                    margin: '0px',
                    marginLeft: '4px' })
                .bind('click' + _eventNameSpace, function(e){
                        try { 
                            picker.currentInput.focus(); 
                        } catch (ex) {}
                        picker.popupPanel.glowPopUpPanel('hide'); 
                        return false;
                    })
                .appendTo(container);
            
            
            
            // picker api methods
            
            var methods = {
                cursorMouseDownHandler: function(event)
                {                    
                    picker.moveStartX = event.pageX;
                    picker.moveStartY = event.pageY;
                    picker.elementMoveStartX = parseInt(picker.selectBoxCursor.css('left'), 10);
                    picker.elementMoveStartY = parseInt(picker.selectBoxCursor.css('top'), 10);
                    
                    $(document)
                        .bind('mouseup' + _eventNameSpace, function(e) {
                            methods.endMoveTracking(e); 
                            return false;
                        })
                        .bind('mousemove' + _eventNameSpace, function(e){
                            methods.cursorMouseMoveHandler(e); 
                            return false;
                        });
                },

                cursorMouseMoveHandler: function(event)
                {
                    var xChange = event.pageX - picker.moveStartX;
                    var yChange = event.pageY - picker.moveStartY;
                    
                    var moveToY = picker.elementMoveStartY + yChange;
                    if (moveToY < 0) { moveToY = 0;  }                      
                    if (moveToY > 255) { moveToY = 255; }

                    var moveToX = picker.elementMoveStartX + xChange;
                    if (moveToX < 0) { moveToX = 0; }                       
                    if (moveToX > 255) { moveToX = 255; }                       

                    picker.selectBoxCursor.css({
                        left: moveToX + 'px',
                        top: moveToY + 'px'
                    });
                    methods.updateTemporaryValue();
                },

                hueCursorMouseDownHandler: function (event)
                {
                    picker.moveStartX = event.pageX;
                    picker.moveStartY = event.pageY;
                    picker.elementMoveStartX = parseInt(picker.hueSelectCursor.css('left'), 10);
                    picker.elementMoveStartY = parseInt(picker.hueSelectCursor.css('top'), 10);

                    $(document)
                        .bind('mouseup' + _eventNameSpace, function(e) {
                            methods.endMoveTracking(e); 
                            return false;  
                        })
                        .bind('mousemove' + _eventNameSpace, function(e) {
                            methods.hueCursorMouseMoveHandler(e); 
                            return false;
                        });
                },

                hueCursorMouseMoveHandler: function(event)
                {
                    var eventY = typeof(event.pageY) != 'undefined' ? event.pageY : event.screenY;
                    var yChange = eventY - picker.moveStartY;   

                    var moveTo = picker.elementMoveStartY + yChange;
                    if (moveTo < 0) { moveTo = 0; }                     
                    if (moveTo > 255) { moveTo = 255; }                     

                    picker.hueSelectCursor.css({ top: moveTo + 'px' });
                    methods.updateTemporaryValue();
                },

                endMoveTracking: function(event)
                {
                    $(document)
                        .unbind('mouseup' + _eventNameSpace)
                        .unbind('mousemove' + _eventNameSpace);
                },

                clickValueSaturationHandler: function(event)
                {
                    var globalInfo = _util.getWindowInfo();

                    var elementInfo = _util.getElementInfo(picker.selectBox);

                    var moveToX = event.pageX - elementInfo.Left;
                    var moveToY = event.pageY - elementInfo.Top;    

                    picker.selectBoxCursor.css({
                        left: moveToX + 'px',
                        top: moveToY + 'px'                     
                    });
                    methods.updateTemporaryValue();

                    methods.cursorMouseDownHandler(event);
                },

                updateTemporaryValue: function(ignoreSelectedColorText)
                {                    
                    var val = parseInt(picker.selectBoxCursor.css('left'), 10);
                    var sat = (255 - parseInt(picker.selectBoxCursor.css('top'), 10)) / 255;
                    var hue = ((255 - parseInt(picker.hueSelectCursor.css('top'), 10)) * (360 / 255));

                    var rgbColor = _util.convertHsvColorToRgbColor([hue, 1, 255]);
                    picker.selectBoxBackground.css({
                        backgroundColor: _util.convertRgbColorToHtmlColor(rgbColor)
                    });

                    rgbColor = _util.convertHsvColorToRgbColor([hue, sat, val]);
                    var htmlColor = _util.convertRgbColorToHtmlColor(rgbColor);

                    picker.selectedColor.css({
                        backgroundColor: htmlColor
                    });

                    if (!ignoreSelectedColorText) {
                        picker.selectedColor.val(htmlColor);                        
                    }

                    picker.selectedColor.css({
                        color: _util.getContrastingHtmlColorForRgbColor(rgbColor)
                    });
                },

                parseEnteredColor: function()
                {
                    var hsvColor = _util.convertRgbColorToHsvColor(_util.convertHtmlColorToRgbColor(picker.selectedColor.val()));

                    picker.selectBoxCursor.css({
                        left: hsvColor[2] + 'px',
                        top: ((hsvColor[1] * -255) + 255) + 'px'
                    });
                    picker.hueSelectCursor.css({
                        top: ((hsvColor[0] / -(360 / 255)) + 255) + 'px'
                    });

                    methods.updateTemporaryValue(true);
                },

                showColorSelector: function(colorSelector)
                {
                    picker.popupPanel.glowPopUpPanel('hide');

                    picker.currentInput = colorSelector;

                    picker.popupPanel.glowPopUpPanel('show', picker.currentInput);

                    var hsvColor = _util.convertRgbColorToHsvColor(_util.convertHtmlColorToRgbColor(picker.currentInput.val()));

                    picker.selectBoxCursor.css({
                        left: hsvColor[2] + 'px',
                        top: ((hsvColor[1] * -255) + 255) + 'px'
                    });
                    picker.hueSelectCursor.css({
                        top: ((hsvColor[0] / -(360 / 255)) + 255) + 'px'
                    });

                    picker.selectedColor.val(picker.currentInput.val());
                    methods.updateTemporaryValue(true);

                    global.setTimeout(methods.focusColorSelector, 99);
                },

                focusColorSelector: function()
                {
                    if (!picker.popupPanel.glowPopUpPanel('isOpening'))
                    {
                        try
                        {
                            picker.selectedColor.focus().select();
                        } catch (e) { }
                    } else {
                        global.setTimeout(methods.focusColorSelector, 99);                      
                    }
                },

                selectTemporaryColor: function()
                {
                    if (picker.currentInput !== null)
                    {
                        _setValue(picker.currentInput, picker.selectedColor.val(), true);
                        try
                        {
                            picker.currentInput.focus();
                        } catch (e) {}
                        picker.popupPanel.glowPopUpPanel('hide');
                    }
                },

                selectNoColor: function()
                {
                    if (picker.currentInput !== null)
                    {
                        _setValue(picker.currentInput, '', true);
                        try
                        {
                            picker.currentInput.focus();
                        } catch (e) {}
                        picker.popupPanel.glowPopUpPanel('hide');
                    }
                }
            };
            
            $.extend(picker, methods);                
            return picker;
        },
        _setValue = function(input, value, processChanged)
        {
            if (input)
            {
                if (value) {
                    value = _util.convertRgbColorToHtmlColor(
                        _util.convertHtmlColorToRgbColor(value));                    
                } else {
                    value = '';                 
                }

                var changed = input.val() != value;

                input.val(value)
                    .css({
                        backgroundColor: input.val(),
                        color: _util.getContrastingHtmlColorForRgbColor(
                            _util.convertHtmlColorToRgbColor(input.val()))
                    });

                if (changed && processChanged) {
                    input.trigger('change');                                    
                }
            }
        },
        _getValue = function(input)
        {
            if (input && input.val()) {
                return _util.convertRgbColorToHtmlColor(
                    _util.convertHtmlColorToRgbColor(input.val()));
            } else {
                return '';              
            }
        },
        _initialize = function(input)
        {
            input.css({
                backgroundColor: _getValue(input),
                textAlign: 'center',
                color: _util.getContrastingHtmlColorForRgbColor(
                    _util.convertHtmlColorToRgbColor(_getValue(input)))
            });            

            if (_isDisabled(input)) {
                _disable(input);                
            } else {
                _enable(input);             
            }
        },
        _disable = function(input)
        {
            input
                .unbind('click' + _eventNameSpace)
                .unbind('keydown' + _eventNameSpace)
                .css({ cursor: 'default' })
                .attr('disabled', 'disabled');
        },
        _enable = function(input)
        {
            input
                .bind('click' + _eventNameSpace, function(){
                    // input.blur(); 
                    input.blur();
                    _colorPicker.showColorSelector(input); 
                    return false;               
                })
                .bind('keydown' + _eventNameSpace, function(event) {
                    if (event.keyCode != 9 && event.keyCode != 16) { 
                        // input.blur(); 
                        input.blur();
                        _colorPicker.showColorSelector(input);                  
                        return false; 
                    } else { 
                        return true; 
                    }
                })
                .css({
                    cursor: 'pointer'
                })
                .removeAttr('disabled');
        },
        _isDisabled = function(input)
        {
            return input.is('input:disabled');
        };        
    
    $.fn.glowColorSelector = function(method) {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowColorSelector');
        }                        
    };
    $.extend($.fn.glowColorSelector, {
        defaults: {
            blendImageUrl: 'hue_blend.png',
            hueBlendImageUrl: 'sv_blend.png'
        }
    });
})(jQuery, window);
 
 
/// @name glowDateTimeSelector
/// @category jQuery Plugin
/// @description Decorates a text input with a date selector
/// 
/// ### jQuery.fn.glowDateTimeSelector
/// 
/// This plugin decorates a text input for use as a formatted date and time input with an optional calendar date selector.
/// 
/// ### Usage
/// 
/// Initiate a selection of text inputs to be date time selectors.
/// 
///     $('selector').glowDateTimeSelector(options)
/// 
/// where selector contains `<input type="text" />` element(s) and `options` is an optional object argument
/// 
/// ### Options
/// 
///  * `pattern`: string pattern containing all desired segments of a date+time string along with their possible values.  
///    * default: *Empty String*
///    * *Examples*:
///    * `"<00-23>:<00-59>"`
///    * `"<Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec> <1-31>, <1900-3000>"`
///    * `"<January,February,March,April,May,June,July,August,September,October,November,December> <1-31>, <1900-3000> at <1-12>:<00-59> <am,pm>"`
///  * `yearIndex`: integer index of where, in a whitespace-split value string, the year is expected to be parsed.  When < 0, default parsing is used.  
///    * default: -1
///  * `monthIndex`: integer index of where, in a whitespace-split value string, the month is expected to be parsed.  When < 0, default parsing is used.  
///    * default: -1
///  * `dayIndex`: integer index of where, in a whitespace-split value string, the day is expected to be parsed.  When < 0, default parsing is used.  
///    * default: -1
///  * `hourIndex`: integer index of where, in a whitespace-split value string, the hour is expected to be parsed.  When < 0, default parsing is used.  
///    * default: -1
///  * `minuteIndex`: integer index of where, in a whitespace-split value string, the minute is expected to be parsed.  When < 0, default parsing is used.  
///    * default: -1
///  * `amPmIndex`: integer index of where, in a whitespace-split value string, am or pm is expected to be parsed.  When < 0, default parsing is used.  
///    * default: -1
///  * `showPopup`: boolean of whether a calendar selection popup should be available.  When a corresponding `calendarButtonImageUrl` is provided, the image will be used as the popup trigger.  Otherwise, a button will be rendered with the text of `calendarButtonText`
///    * default: true
///  * `calendarButtonText`: When `showPopUp` is `true` and a `calendarButtonImageUrl` is not provided, a button will be rendered with the `calendarButtonText`
///    * default: 'Select...'
///  * `calendarButtonImageUrl`: When `showPopUp` is `true` and a `calendarButtonImageUrl` is provided an image with `calendarButtonImageUrl` as the source will be used as the popup trigger.  
///    * default: Evolution-provided calendar icon
///  * `allowBlankvalue`: boolean value of whether or not to allow an empty date
///    * default: true
///  * `monthNames`: Array of string names of months to use in calendar popups.
///    * default: Evolution-provided localized strings.
///  * `dayNames`: Array of string names of weekday names to use in calendar popups.
///    * default: Evolution-provided localized strings.
///  * `calendarButtonCssClass`: string classname applied to the calendar popop trigger button
///    * default: *empty string*
///  * `previousButtonCssClass`: string classname applied to next buttons within a calendar popup
///    * default: *empty string*
///  * `nextButtonCssClass`: string classname applied to previous buttons within a calendar popup
///    * default: *empty string*
/// 
/// ### Events
/// 
/// * `glowDateTimeSelectorChange`: triggered when the value is changed
/// 
/// ### Methods
/// 
/// The `glowDateTimeSelector`'s initiation is delayed from its use by 9 ms.  To use `'val'` or `'disabled'` immediately after applying `glowDateTimeSelector()` to a selection, it requires delaying the call by 10 ms or more.  This is not required when calling `'val'` or `'disabled'` in response to user interaction.
/// 
/// #### val
/// 
/// Retrieves and/or sets the current value of the date time selector.  The value of the selected text input is also updated by the date time selector.
/// 
///     // get the current datetime as a Date object
///     var date = $('selector').glowDateTimeSelector('val');
/// 
///     // set the current datetime to a date object value
///     $('selector').glowDateTimeSelector('val', new Date());
/// 
/// #### disabled
/// 
/// Retrieves and/or sets whether the date time selector is disabled.  When enabled, focusing on the input will allow changing the value.  When disabled, it will not.
/// 
///     // get the current disabled state
///     var isDisabled = $('selector').glowDateTimeSelector('disabled');
/// 
///     // set the disabled state
///     $('selector').glowDateTimeSelector('disabled', false);
/// 

(function($, global){

    // publicly-available api methods
    var api = 
    {
        val: function(date)
        {
            if (date === undefined)
            {
                var context = $(this).data(_dataKey);
                if (context === null) {
                    return null;                    
                }
                
                var values = context.internal.patternedTextBox.glowPatternedTextBox('getPatternValues');
                
                var year, month, day;
                
                if (context.settings.yearIndex >= 0)
                {
                    year = parseInt(values[context.settings.yearIndex], 10);
                    if (isNaN(year)) {
                        return null;                        
                    }
                } else {
                    year = (new Date()).getFullYear();                    
                }
                
                if (context.settings.dayIndex >= 0)
                {
                    day = parseInt(values[context.settings.dayIndex], 10);
                    if (isNaN(day)) {
                        return null;
                    }
                } else {
                    day = (new Date()).getDate();                    
                }
                
                if (context.settings.monthIndex >= 0)
                {
                    month = _getMonthNumber(context, values[context.settings.monthIndex]) - 1;
                    if (isNaN(month)) {
                        return null;                        
                    }
                } else {
                    month = (new Date()).getMonth();                    
                }
                
                date = new Date(year, month, day);
                
                if (context.settings.hourIndex >= 0)
                {
                    var hour = parseInt(values[context.settings.hourIndex], 10);
                    if (!isNaN(hour))
                    {
                        if (context.settings.amPmIndex >= 0) {
                            date.setHours(_get24Hour(context, hour, values[context.settings.amPmIndex]));                            
                        } else {
                            date.setHours(_get24Hour(context, hour, -1));                            
                        }
                    } else {
                        return null;                        
                    }
                }
                
                if (context.settings.minuteIndex >= 0)
                {
                    var minute = parseInt(values[context.settings.minuteIndex], 10);
                    if (!isNaN(minute)) {
                        date.setMinutes(minute);                        
                    } else {
                        return null;                        
                    }
                }
                
                return date;
            }
            else
            {
                return this.each(function()
                {
                    var context = $(this).data(_dataKey);
                    
                    var values = context.internal.patternedTextBox.glowPatternedTextBox('getPatternValues');
                    
                    if (context.settings.yearIndex >= 0) {
                        values[context.settings.yearIndex] = date.getFullYear();                        
                    }
                    
                    if (context.settings.monthIndex >= 0) {
                        values[context.settings.monthIndex] = _getMonthValue(context, date.getMonth() + 1);                        
                    }
                    
                    if (context.settings.dayIndex >= 0) {
                        values[context.settings.dayIndex] = date.getDate();                        
                    }
                    
                    if (context.settings.hourIndex >= 0) {
                        values[context.settings.hourIndex] = _getHourValue(context, date.getHours());                        
                    }
                    
                    if (context.settings.minuteIndex >= 0) {
                        values[context.settings.minuteIndex] = date.getMinutes();                        
                    }
                    
                    if (context.settings.amPmIndex >= 0) {
                        values[context.settings.amPmIndex] = _getAmPmValue(context, date.getHours());                        
                    }
                                        
                    context.internal.patternedTextBox.glowPatternedTextBox('setPatternValues', values);
                });
            }
        }, 
        disabled: function(disable)
        {
            if (disable === undefined)
            {
                var context = this.data(_dataKey);
                if (context) {
                    return context.internal.patternedTextBox.glowPatternedTextBox('disabled');                    
                }
                
                return false;
            }
            else
            {
                return this.each(function()
                {
                    var context = $(this).data(_dataKey);
                
                    context.internal.patternedTextBox.glowPatternedTextBox('disabled', disable);

                    if (context.internal.popupButton)
                    {
                        if (disable)
                        {
                            context.internal.popupButton
                                .attr('disabled','disabled')
                                .unbind('click' + _eventNameSpace);
                        }
                        else
                        {
                            context.internal.popupButton
                                .removeAttr('disabled')
                                .unbind('click' + _eventNameSpace)
                                .bind('click' + _eventNameSpace, function(e) {
                                    _open(context);
                                });
                        }
                    }
                });
            }
        }
    };

    // private internal methods 
    var _dataKey = '_glowDateTimeSelector',
        _changeEventName = 'glowDateTimeSelectorChange',
        _eventNameSpace = '.glowDateTimeSelector',
        _init = function(options) 
        {
            return this.each(function() 
            {
                var context = {
                    settings: $.extend({}, $.fn.glowDateTimeSelector.defaults, options || {}),
                    internal: {
                        state: $(this),
                        initialized: false,
                        patternedTextBox: null,
                        popupButton: null,
                        popupPanel: null,
                        currentDate: new Date(),
                        currentYear: (new Date()).getFullYear(),
                        currentMonth: (new Date()).getMonth() + 1
                    }
                };
            
                $(this).data(_dataKey, context);

                global.setTimeout(function() { _initialize(context); }, 9);
            });
        },
        _changed = function(context)
        {
            context.internal.state.trigger(_changeEventName);
        },
        _initialize = function(context)
        {
            context.internal.patternedTextBox = 
                context.internal.state
                    .glowPatternedTextBox({
                        pattern: context.settings.pattern,
                        allowBlanks: context.settings.allowBlankValue,
                        blankCharacter: '_',
                        onValidation: function(jq, values, type, modifiedIndex) { 
                            return _validateDate(context, values, type, modifiedIndex); 
                        }})
                    .bind('glowPatternedTextBoxChange' + _eventNameSpace, function() { _changed(context); });
        
            if (context.settings.showPopup && context.internal.state)
            {
            	if (context.settings.calendarButtonImageUrl)
            	{
            		context.internal.popupButton = $('<img style="border: 1px solid #ccc; padding: 2px; align: top; margin-left: 2px; background-color: #eee;" />')
            			.css({
		 					borderTopColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-top-color', 'borderTopColor', '#999'),
		                    borderLeftColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-left-color', 'borderLeftColor', '#999'),
		                    borderRightColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-right-color', 'borderRightColor', '#999'),
		                    borderBottomColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-bottom-color', 'borderBottomColor', '#999'),
		                    backgroundColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'background-color', 'backgroundColor', '#fff')
		            	})
		            	.attr('src', context.settings.calendarButtonImageUrl)
		            	.insertAfter(context.internal.state);
            	}
            	else
            	{	
	                context.internal.popupButton = $('<input type="button" />')
	                    .val(context.settings.calendarButtonText)
	                    .addClass(context.settings.calendarButtonCssClass)
	                    .insertAfter(context.internal.state);
                }

                context.internal.popupPanel = $('<div></div>')
                    .insertAfter(context.internal.popupButton)
                    .glowPopUpPanel({
                        cssClass: '',
                        position: 'updown',
                        zIndex: 1000,
                        hideOnDocumentClick: true
                    });
            }
        
            api.disabled.apply(context.internal.state, [context.internal.patternedTextBox.glowPatternedTextBox('disabled')]);
                    
            context.internal.initialized = true;
        }, 
        _validateDate = function(context, values, type, modifiedIndex)
        {
            if (!context.internal.initialized || 
                context.settings.monthIndex < 0 || 
                context.settings.dayIndex < 0) {
                return values;                
            }
            
            var month = _getMonthNumber(context, values[context.settings.monthIndex]);
            var day = parseInt(values[context.settings.dayIndex], 10);
            var year = -1;
            if (context.internal.yearIndex >= 0) {
                year = parseInt(values[context.settings.yearIndex], 10);                
            }
        
            if (isNaN(month) || isNaN(day)) {
                return values;                
            }

            var newDay = values[context.settings.dayIndex];
            if (month == 2)
            {
                if (!isNaN(year) && year > -1 && ((year % 4 === 0 && year % 100 !== 0) || year % 400 === 0))
                {
                    if (day > 29)
                    {
                        if (modifiedIndex == context.settings.dayIndex && type == 'next') {
                            newDay = "01";                            
                        } else {
                            newDay = "29";                            
                        }
                    }
                }
                else if (day > 28)
                {
                    if (modifiedIndex == context.settings.dayIndex && type == 'next') {
                        newDay = "01";                        
                    } else {
                        newDay = "28";
                    }
                }
            }
            else if ((month == 4 || month == 6 || month == 9 || month == 11) && day > 30)
            {
                if (modifiedIndex == context.settings.dayIndex && type == 'next') {
                    newDay = "01";                    
                } else {
                    newDay = "30";                    
                }
            }
        
            values[context.settings.dayIndex] = newDay;
            
            return values;
        }, 
        _getMonthNumber = function(context, value)
        {
            if (value === undefined) {
                return parseInt('NaN', 10);
            }
        
            if (isNaN(parseInt(value, 10)))
            {
                var index = context.internal.patternedTextBox.glowPatternedTextBox('getPatternValueOptionIndex', context.settings.monthIndex, value);
                if (index !== undefined) {
                    return index + 1;                                   
                }
            
                return parseInt('NaN', 10);
            } else {
                return parseInt(value, 10);                
            }
        }, 
        _get24Hour = function(context, value, amPmValue)
        {
            var hours = parseInt(value, 10);
            if (isNaN(hours)) {
                return hours;                
            }
        
            if (amPmValue !== undefined && context.settings.amPmIndex > -1)
            {
                var index = context.internal.patternedTextBox.glowPatternedTextBox('getPatternValueOptionIndex', context.settings.amPmIndex, amPmValue);
            
                if (index === 0)
                {
                    if (hours == 12) {
                        hours = 0;                                                
                    }
                }
                else if (index === 1)
                {
                    if (hours != 12) {
                        hours += 12;                        
                    }
                }
            }
        
            return hours;
        }, 
        _getMonthValue = function(context, number)
        {
            var optionValue = context.internal.patternedTextBox.glowPatternedTextBox('getPatternValueOption', context.settings.monthIndex, number - 1);
            if (!optionValue) {
                return parseInt(number, 10);                
            } else {
                return optionValue;                
            }
        }, 
        _getHourValue = function(context, hour)
        {
            if (context.settings.amPmIndex > -1 && hour > 12) {
                return hour - 12;                
            } else {
                return hour;                
            }
        }, 
        _getAmPmValue = function(context, hour)
        {
            if (context.settings.amPmIndex > -1) {
                if (hour >= 12) {
                    return context.internal.patternedTextBox.glowPatternedTextBox('getPatternValueOption', context.settings.amPmIndex, 1);                    
                } else {
                    return context.internal.patternedTextBox.glowPatternedTextBox('getPatternValueOption', context.settings.amPmIndex, 0);                    
                }
            } else {
                return '';                
            }
        }, 
        _refresh = function(context)
        {
            if (!context.internal.popupPanel) {
                return;                
            }
        
            context.internal.popupPanel.glowPopUpPanel('empty');
        
            var container = $('<div></div>')
                .css({
                    borderTopWidth: '1px',
                    borderLeftWidth: '1px',
                    borderRightWidth: '1px',
                    borderBottomWidth: '1px',
                    borderTopStyle: 'solid',
                    borderLeftStyle: 'solid',
                    borderRightStyle: 'solid',
                    borderBottomStyle: 'solid',
                    borderTopColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-top-color', 'borderTopColor', '#999'),
                    borderLeftColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-left-color', 'borderLeftColor', '#999'),
                    borderRightColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-right-color', 'borderRightColor', '#999'),
                    borderBottomColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-bottom-color', 'borderBottomColor', '#999'),
                    fontFamily: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-family', 'fontFamily', 'Arial, Helvetica'),
                    backgroundColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'background-color', 'backgroundColor', '#fff'),
                    padding: '2px',
                    fontSize: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-size', 'fontSize', '9pt'),
                    color: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000')
                });
        
            var table = $('<table></table>')
                .attr('cellPadding',0)
                .attr('cellSpacing',0)
                .css({
                    borderWidth: '0px'
                })
                .appendTo(container);
        
            var tbody = $('<tbody></tbody>')
                .appendTo(table);
        
            // navigation and month/year
            var tr = $('<tr></tr>');                
            
            var td = $('<td></td>')
                .css({
                    textAlign: 'center',
                    padding: '2px',
                    cursor: 'pointer'
                })
                .bind('click' + _eventNameSpace, function(e) {
                    return _prev(context, e);
                })
                .appendTo(tr);

            var img = $('<div></div>')
                .addClass(context.settings.previousButtonCssClass)
                .html('&lt;')
                .appendTo(td);

            td = $('<td></td>')
                .css({
                    textAlign: 'center',
                    padding: '2px',
                    fontWeight: 'bold',
                    fontSize: '90%'
                })
                .appendTo(tr);
                
            if (context.settings.dayIndex> -1) {
            	td.attr('colSpan', 5).html(context.settings.monthNames[context.internal.currentMonth - 1] + ' ' + context.internal.currentYear)
            } else {
            	td.html(context.internal.currentYear)
            }                
                
            td = $('<td></td>')
                .css({
                    textAlign: 'center',
                    padding: '2px',                    
                    cursor: 'pointer'
                })
                .bind('click' + _eventNameSpace, function(e) {
                    return _next(context, e);
                })
                .appendTo(tr);
                
            img = $('<div></div>')
                .addClass(context.settings.nextButtonCssClass)
                .html('&gt;')
                .appendTo(td);

            tbody.append(tr);
        
        	if (context.settings.dayIndex > -1) {
	        	// day names
	            tr = $('<tr></tr>');
	            for (var i = 0; i < context.settings.dayNames.length; i++)
	            {
	                td = $('<td></td>')
	                    .html(context.settings.dayNames[i])
	                    .css({
	                        textAlign: 'center',
	                        padding: '2px',
	                        fontWeight: 'bold',
	                        borderBottomWidth: '1px',
	                        borderBottomStyle: 'solid',
	                        fontSize: '90%',
	                        width: '14%',
	                        borderBottomColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-bottom-color', 'borderBottomColor', '#999')
	                    })
	                    .appendTo(tr);
	            }
	            tbody.append(tr);

	            var tempDate = new Date(context.internal.currentYear, context.internal.currentMonth - 1, 1);

	            // days
	            tr = $('<tr></tr>');
	            var lastDayOfWeek = 0;
	            var rowsLeft = 5;
	            while (lastDayOfWeek < tempDate.getDay())
	            {
	                td = $('<td></td>')
	                    .html('&nbsp;')
	                    .css({
	                        fontSize: '90%',
	                        padding: '2px',
	                        textAlign: 'center',
	                        width: '14%'
	                    })
	                    .appendTo(tr);
	                lastDayOfWeek++;
	            }

	            var daysInMonth = 31;
	            if (context.internal.currentMonth == 2)
	            {
	                if (!isNaN(context.internal.currentYear) && 
	                    context.internal.currentYear > -1 && 
	                    (
	                        (context.internal.currentYear % 4 === 0 && context.internal.currentYear % 100 !== 0) || 
	                        context.internal.currentYear % 400 === 0
	                    )
	                ) {
	                    daysInMonth = 29;                    
	                } else {
	                    daysInMonth = 28;                    
	                }
	            }
	            else if (context.internal.currentMonth == 4 || 
	                context.internal.currentMonth == 6 || 
	                context.internal.currentMonth == 9 || 
	                context.internal.currentMonth == 11) 
	            {
	                daysInMonth = 30;                
	            }
	        
	            var currentDay = -1;
	            if (context.internal.currentMonth == context.internal.currentDate.getMonth() + 1 
	                && context.internal.currentYear == context.internal.currentDate.getFullYear()) {
	                    currentDay = context.internal.currentDate.getDate();                    
	                }
	        
	            for (var i = 1; i <= daysInMonth; i++)
	            {
	                lastDayOfWeek++;
	                td = $('<td></td>')
	                    .html(i)
	                    .css({
	                        textAlign: 'center',
	                        padding: '2px',
	                        fontSize: '90%',
	                        cursor: 'pointer',
	                        width: '14%'
	                    })
	                    .bind('click' + _eventNameSpace, $.telligent.glow.utility.makeBoundFunction(_click, null, [context, i]))
	                    .bind('mouseover' + _eventNameSpace, function(e) {
	                        $(e.target).css({
	                            color: $.telligent.glow.utility.getCurrentStyleValue(
	                                context.internal.state, 
	                                'background-color', 
	                                'backgroundColor', 
	                                '#fff'),
	                            backgroundColor: $.telligent.glow.utility.getCurrentStyleValue(
	                                context.internal.state, 
	                                'color', 
	                                'color', 
	                                '#000')                            
	                        });
	                    })
	                    .bind('mouseout' + _eventNameSpace, function(e) {
	                        $(e.target).css({
	                            color: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000'),
	                            backgroundColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'background-color', 'backgroundColor', '#fff')
	                        });
	                    });
	            
	                if (i == currentDay)
	                {
	                    td.css({
	                        padding: '1px',
	                        borderTopWidth: '1px',
	                        borderLeftWidth: '1px',
	                        borderRightWidth: '1px',
	                        borderBottomWidth: '1px',
	                        borderTopStyle: 'solid',
	                        borderLeftStyle: 'solid',
	                        borderRightStyle: 'solid',
	                        borderBottomStyle: 'solid',
	                        borderTopColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000'),
	                        borderLeftColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000'),
	                        borderRightColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000'),
	                        borderBottomColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000')
	                    });
	                }
	            
	                tr.append(td);
	            
	                if (lastDayOfWeek == 7)
	                {
	                    lastDayOfWeek = 0;
	                    tbody.append(tr);
	                    tr = $('<tr></tr>');
	                    rowsLeft--;
	                }
	            }
	        
	            while (lastDayOfWeek < 7)
	            {
	                td = $('<td></td>')
	                    .html('&nbsp;')
	                    .css({
	                        fontSize: '90%',
	                        padding: '2px',
	                        textAlign: 'center',
	                        width: '14%'
	                    })
	                    .appendTo(tr);
	                    
	                lastDayOfWeek++;
	            }
	        
	            tbody.append(tr);
	        
	            while(rowsLeft > 0)
	            {
	                rowsLeft--;
	                tr = $('<tr></tr>');
	                for (var i = 0; i < 7; i++)
	                {
	                    td = $('<td></td>')
	                        .html('&nbsp;')
	                        .css({
	                            fontSize: '90%',
	                            padding: '2px',
	                            textAlign: 'center',
	                            width: '14%'
	                        })
	                        .appendTo(tr);
	                }
	                tbody.append(tr);
	            }
            } else {
            	var currentMonth = -1;
	            if (context.internal.currentYear == context.internal.currentDate.getFullYear()) {
	                    currentMonth = context.internal.currentDate.getMonth();                    
	                }
            	
            	for (var i = 0; i < 12; i++) {
            		if (i % 3 == 0) {
            			tr = $('<tr></tr>');
            			tbody.append(tr);
            		}
            			
            		td = $('<td></td>')
            			.html(context.settings.monthNames[i])
            			.css({
	                        textAlign: 'center',
	                        padding: '2px',
	                        fontSize: '90%',
	                        cursor: 'pointer',
	                        width: '14%'
	                    })
	                    .bind('click' + _eventNameSpace, $.telligent.glow.utility.makeBoundFunction(_click, null, [context, i]))
	                    .bind('mouseover' + _eventNameSpace, function(e) {
	                        $(e.target).css({
	                            color: $.telligent.glow.utility.getCurrentStyleValue(
	                                context.internal.state, 
	                                'background-color', 
	                                'backgroundColor', 
	                                '#fff'),
	                            backgroundColor: $.telligent.glow.utility.getCurrentStyleValue(
	                                context.internal.state, 
	                                'color', 
	                                'color', 
	                                '#000')                            
	                        });
	                    })
	                    .bind('mouseout' + _eventNameSpace, function(e) {
	                        $(e.target).css({
	                            color: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000'),
	                            backgroundColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'background-color', 'backgroundColor', '#fff')
	                        });
	                    });
	                    
	                if (i == currentMonth)
	                {
	                    td.css({
	                        padding: '1px',
	                        borderTopWidth: '1px',
	                        borderLeftWidth: '1px',
	                        borderRightWidth: '1px',
	                        borderBottomWidth: '1px',
	                        borderTopStyle: 'solid',
	                        borderLeftStyle: 'solid',
	                        borderRightStyle: 'solid',
	                        borderBottomStyle: 'solid',
	                        borderTopColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000'),
	                        borderLeftColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000'),
	                        borderRightColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000'),
	                        borderBottomColor: $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000')
	                    });
	                }
	            
	                tr.append(td);
            	}
            }
        
            context.internal.popupPanel.glowPopUpPanel('append', container);
        },
        _next = function(context, event)
        {
        	if (context.settings.dayIndex > -1) {
	            context.internal.currentMonth++;
	            if (context.internal.currentMonth > 12)
	            {
	                context.internal.currentMonth = 1;
	            
	                if (context.internal.currentYear < 9999) {
	                    context.internal.currentYear++;                    
	                }
	            }
	        } else {
	        	context.internal.currentYear++;
	        }
        
            _refresh(context);
                
            event.stopPropagation();                

            return false;
        },
        _prev = function(context, event)
        {
        	if (context.settings.dayIndex > -1) {
	            context.internal.currentMonth--;
	            if (context.internal.currentMonth < 1)
	            {
	                context.internal.currentMonth = 12;
	            
	                if (context.internal.currentYear > 0) {
	                    context.internal.currentYear--;                    
	                }
	            }
			} else { 
				context.internal.currentYear--;
			}
        
            _refresh(context);
            
            event.stopPropagation();                

            return false;
        },
        _click = function(context, val)
        {
            if (context)
            {
                var d = api.val.apply(context.internal.state);
                if (d === null) {
                    d = new Date();                    
                }

				if (context.settings.dayIndex > -1) {
                	d = new Date(context.internal.currentYear, context.internal.currentMonth - 1, val, d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds());
                } else {
                	d = new Date(context.internal.currentYear, val, d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds(), d.getMilliseconds());
                }
            
                var d2 = api.val.apply(context.internal.state);
                if (!d2 || d2.getTime() != d.getTime())
                {
                    api.val.apply(context.internal.state, [d]);
                    context.internal.state.trigger(_changeEventName);
                }
            
                _close(context);
            }
        },
        _open = function(context)
        {
            var currentDate = api.val.apply(context.internal.state);
            if (!currentDate) {
                currentDate = new Date();                
            }
        
            context.internal.currentDate = currentDate;
            context.internal.currentMonth = currentDate.getMonth() + 1;
            context.internal.currentYear = currentDate.getFullYear();

            _refresh(context);
            context.internal.popupPanel.glowPopUpPanel('show', context.internal.popupButton);
        },
        _close = function(context)
        {
            context.internal.popupPanel.glowPopUpPanel('hide');
        };
    
    $.fn.glowDateTimeSelector = function(method) 
    {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowDateTimeSelector');
        }
    };
    $.extend($.fn.glowDateTimeSelector, {
        defaults: {
            pattern: '',
            yearIndex: -1,
            monthIndex: -1,
            dayIndex: -1,
            hourIndex: -1,
            minuteIndex: -1,
            amPmIndex: -1,
            showPopup: true,
            allowBlankvalue: true,
            monthNames: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            dayNames: ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'],
            calendarButtonCssClass: '',
            previousButtonCssClass: '',
            nextButtonCssClass: '',
            calendarButtonText: 'Select...',
            calendarButtonImageUrl: null
        }
    });
    // events
    //   glowDateTimeSelectorChange
})(jQuery, window);
 
 
/// @name glowDelayedMouseEnter
/// @category jQuery Event
/// @description delayed version of mouseenter
/// 
/// ### jQuery.event.special.glowDelayedMouseEnter
/// 
/// This plugin exposes a specialized version of mouseEnter which does not fire until after a delay.  The special event supports both binding and delegation.
/// 
/// ### Usage
/// 
///     // 1 second delay from actual mouse enter to when the event is triggered
///     $('selector').bind('glowDelayedMouseEnter', 1000, function(e){
///         // handle mouse enter
///     });
/// 
///     // live-bound default 500 ms delay from mouse enter to when glowDelayedMouseEnter is triggered
///     $('selector').live('glowDelayedMouseEnter', function(e){
///         // handle mouse enter
///     });
///

/// @name glowDelayedMouseLeave
/// @category jQuery Event
/// @description delayed version of mouseleave
/// 
/// ### jQuery.event.special.glowDelayedMouseLeave
/// 
/// This plugin exposes a specialized version of mouseLeave which does not fire until after a delay.  The special event supports both binding and delegation.
/// 
/// ### Usage
/// 
///     // default 500 ms delay from mouse leave to when glowDelayedMouseLeave is triggered
///     $('selector').bind('glowDelayedMouseLeave', function(e){
///         // handle mouse leave
///     });
/// 
///     // live-bound default 3s delay from mouse leave to when glowDelayedMouseLeave is triggered
///     $('selector').live('glowDelayedMouseLeave', 3000, function(e){
///         // handle mouse leave
///     });
/// 

/* a delayed version of the normal mouseenter/mouseleave event, doesn't fire until
  after a given delay (default 500 ms).	 And only fires if still over element
  at end of delay.	Works with live/delegate
	$(selector).live('glowDelayedMouseEnter', 3000, function(){ ... });
*/
(function($){
	var defaultDelay = 500,
		contextKey = '_delayedEnterLeaveContextKey',
		getOrCreateContext = function(element) {
			var context = element.data(contextKey);
			if(context === null || typeof context === 'undefined') {
				context = {
					enterTimeout: null,
					leaveTimeout: null
				};
				element.data(contextKey, context);
			}
			return context;
		};
	$.extend($.event.special, {
		glowDelayedMouseEnter: {
			add: function(handle) {
				var delay = handle.data || defaultDelay;
				$(handle.selector)
					.live('mouseenter', function(e){
						var item = $(this),
							context = getOrCreateContext(item);
						context.enterTimeout = setTimeout(function(){
							item.trigger('glowDelayedMouseEnter');
							clearTimeout(context.enterTimeout);
						}, delay);
					})
					.live('mouseleave', function(){
						var context = getOrCreateContext($(this));
						clearTimeout(context.enterTimeout);
					});
			}
		},
		glowDelayedMouseLeave: {
			add: function(handle) {
				var delay = handle.data || defaultDelay;
				$(handle.selector)
					.live('mouseleave', function(e){
						var item = $(this),
							context = getOrCreateContext(item);
						context.leaveTimeout = setTimeout(function(){
							item.trigger('glowDelayedMouseLeave');
							clearTimeout(context.leaveTimeout);
						}, delay);
					})
					.live('mouseenter', function(){
						var context = getOrCreateContext($(this));
						clearTimeout(context.leaveTimeout);
					});
			}
		}
	});
}(jQuery)); 
 
/// @name glowDropDownList
/// @category jQuery Plugin
/// @description Decorates a select with more custom markup and stylability
/// 
/// ### jQuery.fn.glowDropDownList
/// 
/// This plugin gracefully enhances a standard HTML select input with the ability to be fully styled with custom markup 
/// 
/// ### Usage
/// 
/// Initiate a selection of select elements to be drop down lists
/// 
///     $('selector').glowDropDownList(options)
/// 
/// where selector contains `<select>` element(s) and `options` is an optional object argument
/// 
/// ### Options
/// 
///  * `selectedItemWidth`: pixel width of a currently-selected item
///    * default: 200
///  * `selectedItemHeight`: pixel height of a currently-selected item
///    * default: 32
///  * `itemsWidth`: pixel width of items
///    * default: 200
///  * `itemsHeight`: pixel height of items
///    * default: 300
///  * `itemsHtml`: array of html fragment representations of elements.  When provided, elements from this array will be used to render selectable options.  
///    * default: []
///  * `showHtmlWhenSelected`: boolean value of whether or not to show the html-representation of the selected option as the currently selected item
///    * default: true
///  * `buttonImageUrl`: image path to use for a drop down arrow
///    * default: Evolution-provided button image
/// 
/// ### Methods
/// 
/// #### val
/// 
/// Gets and/or sets the current value of the drop down list
/// 
///     var value = $('selector').glowDropDownList('val');
///     $('selector').glowDropDownList('val', 'someValue');
/// 
/// #### selectedIndex
/// 
/// Gets and/or sets the current index of the selected item in the drop down list.  
/// 
///     var index = $('selector').glowDropDownList('selectedIndex');
///     $('selector').glowDropDownList('selectedIndex', 5);       // will set the value
///     $('selector').glowDropDownList('selectedIndex', 5, true); // will set the value and process change events
/// 
/// #### getText
/// 
/// Gets the text of the currently selected item
/// 
/// #### disabled
/// 
/// Gets and/or sets whether the ability to select items is disabled
/// 
///     var disabled = $('selector').glowDropDownList('disabled');
///     $('selector').glowDropDownList('disabled', true);
/// 
/// #### add
/// 
/// Adds a new DropDownList item to list of selectable options
/// 
///     var item = $('selector').glowDropDownList('createItem', { value: 'some value', text: 'some text', html: 'html to display');
///     $('selector').glowDropDownList('add', item));
/// 
/// #### remove
/// 
/// Removes a DropDownList item from the selectable options
/// 
///     $('selector').glowDropDownList('remove', item);
/// 
/// #### clear
/// 
/// Removes all selectable options from the DropDownList
/// 
///     $('selector').glowDropDownList('clear');
/// 
/// #### insert
/// 
/// Adds a new DropDownList item to list of selectable options at a specific index position
/// 
///     var item = $('selector').glowDropDownList('createItem', { value: 'some value', text: 'some text', html: 'html to display');
///     $('selector').glowDropDownList('add', item, 2);
/// 
/// #### getByIndex
/// 
/// Gets a DropDownList item at a given position index
/// 
///     var item = $('selector').glowDropDownList('getByIndex' 2);
/// 
/// #### count
/// 
/// Returns the number of selectable items
/// 
///     var count = $('selector').glowDropDownList('count');
///     
/// #### createItem
/// 
/// Creates a new DropDownList item which can then be inserted or added
/// 
///     var item = $('selector').glowDropDownList('createItem', { value: 'some value', text: 'some text', html: 'html to display');
/// 

(function($){

    // publicly-accessible api
    var api = 
    {
        val: function(value)
        {
            if (value === undefined)
            {
                var context = this.data('dropDownList');
            
                if (context && context.internal.state && context.internal.initialized && context.internal.state.selectedIndex >= 0 && context.internal.state.selectedIndex < context.internal.state.options.length)
                    return context.internal.state.options[context.internal.state.selectedIndex].value;
                
                return '';
            }
            else
            {
                return this.each(function()
                {
                    var context = $(this).data('dropDownList');
                    
                    if (context.internal.state && context.internal.initialized)
                    {
                        for (var i = 0; i < context.internal.state.options.length; i++)
                        {
                            if (context.internal.state.options[i].value == value)
                            {
                                api.selectedIndex.apply($(context.internal.state), [i]);
                                return;
                            }
                        }
                        
                        api.selectedIndex.apply($(context.internal.state), [0]);
                    }
                });
            }
        },
        selectedIndex: function(index, processChanged)
        {
            if (index === undefined)
            {
                var context = this.data('dropDownList');
            
                if (context && context.internal.state && context.internal.initialized)
                    return context.internal.state.selectedIndex;

                return -1;
            }
            else
            {
                return this.each(function()
                {
                    var context = $(this).data('dropDownList');
                    
                    if (context.internal.state && context.internal.initialized && index >= 0 && index < context.internal.items.length && index < context.internal.state.options.length)
                    {
                        var changed = context.internal.state.selectedIndex != index;
                        
                        context.internal.state.selectedIndex = index;
                        
                        if (context.settings.showHtmlWhenSelected)
                            context.internal.selectedItem.innerHTML = context.internal.items[index].html;
                        else
                            context.internal.selectedItem.innerHTML = context.internal.state.options[index].innerHTML;
                        
                        if (changed && processChanged && context.internal.state.onchange)
                            context.internal.state.onchange();
                    }
                });
            }   
        },
        getText : function()
        {
            var context = this.data('dropDownList');
            
            if (context && context.internal.state && context.internal.initialized && context.internal.state.selectedIndex >= 0 && context.internal.state.selectedIndex < context.internal.items.length)
                return context.internal.items[context.internal.state.selectedIndex].html;

            return '';
        }, 
        disabled : function(disable)
        {
            if (disable === undefined)
            {
                var context = this.data('dropDownList');
                if (context)
                    return context.internal.state.readonly;
                
                return undefined;
            }
            else
            {
                return this.each(function()
                {
                    var context = $(this).data('dropDownList');
                    
                    if (disable)
                    {
                        context.internal.button.style.cursor = 'default';   
                        context.internal.button.onclick = null;
                        context.internal.button.style.backgroundImage = 'none';
                        context.internal.button.style.backgroundColor = context.internal.backgroundColor;
                        context.internal.selectedItem.style.borderColor = context.internal.backgroundColor;
                        context.internal.state.readonly = true;
                    }
                    else
                    {
                        context.internal.button.style.cursor = 'pointer';   
                        context.internal.button.onclick = function() { _show(context); };
                        if (context.settings.buttonImageUrl)
                            context.internal.button.style.backgroundImage = 'url(\'' + context.settings.buttonImageUrl + '\')';
                            
                        context.internal.button.style.backgroundColor = '#cccccc';
                        context.internal.selectedItem.style.borderColor = '#999999';
                        context.internal.state.readonly = false;
                    }
                });
            }
        }, 
        refresh : function()
        {
            return this.each(function()
            {           
                var context = $(this).data('dropDownList');
                
                if (context.internal.initialized)
                {
                    var value = api.val.apply($(context.internal.state), []);
                    
                    while(context.internal.state.childNodes.length > 0)
                        context.internal.state.removeChild(context.internal.state.childNodes[0]);
                    
                    while (context.internal.itemsContainer.childNodes.length > 0)
                        context.internal.itemsContainer.removeChild(context.internal.itemsContainer.childNodes[0]);
                    
                    var selectedIndex = -1;
                    for (var i = 0; i < context.internal.items.length; i++)
                    {
                        var item = document.createElement('div');
                        item.onmouseover = $.telligent.glow.utility.makeBoundFunction(_itemMouseOver, null, [context, i]);
                        item.onclick = $.telligent.glow.utility.makeBoundFunction(api.selectedIndex, $(context.internal.state), [i, true]);
                        item.style.padding = '2px';
                        item.style.cursor = 'pointer';
                        item.style.borderStyle = 'dotted';
                        item.style.borderWidth = '1px';
                        item.style.borderColor = context.internal.backgroundColor;
                        item.innerHTML = context.internal.items[i].html;
                        context.internal.itemsContainer.appendChild(item);
                        
                        var option = document.createElement('option');
                        option.value = context.internal.items[i].value;
                        option.innerHTML = context.internal.items[i].text;
                        context.internal.state.appendChild(option);
                        
                        if (context.internal.items[i].value == value)
                            selectedIndex = i;
                    }
                    
                    if (selectedIndex >= 0)
                    {
                        context.internal.state.selectedIndex = selectedIndex;
                        
                        if (context.settings.showHtmlWhenSelected)
                            context.internal.selectedItem.innerHTML = context.internal.items[selectedIndex].html;
                        else
                            context.internal.selectedItem.innerHTML = context.internal.state.options[selectedIndex].innerHTML;
                    }
                    else
                    {
                        context.internal.selectedItem.innerHTML = '&nbsp;';
                        api.selectedIndex.apply($(context.internal.state), [0]);
                    }
                    
                    context.internal.highlightedItemIndex = -1;
                }
            });
        }, 
        add : function(dropDownListItem)
        {
            if (!dropDownListItem)
                return this;
            
            api.remove.apply(this, [dropDownListItem]);
            
            var isFirst = true;
            return this.each(function()
            {
                if (isFirst)
                    isFirst = false;
                else
                    dropDownListItem = dropDownListItem._clone();
                
                var context = $(this).data('dropDownList');         

                context.internal.items[context.internal.items.length] = dropDownListItem;
            });
        }, 
        remove : function(dropDownListItem)
        {
            if (!dropDownListItem)
                return this;
            
            return this.each(function()
            {
                var context = $(this).data('dropDownList');
            
                var ddlItems = new Array();
                var found = false;
                for (var i = 0; i < context.internal.items.length; i++)
                {
                    if (context.internal.items[i].value == dropDownListItem.value)
                        found = true;
                    else
                        ddlItems[ddlItems.length] = context.internal.items[i];
                }
                
                if (found)
                    context.internal.items = ddlItems;
            });
        }, 
        clear : function()
        {
            return this.each(function()
            {
                var context = $(this).data('dropDownList');
            
                context.internal.items = new Array();
            });
        }, 
        insert : function(dropDownListItem, index)
        {
            if (!dropDownListItem)
                return this;
            
            api.remove.apply($(context.internal.state), [dropDownListItem]);
            
            var isFirst = true;
            return this.each(function()
            {
                if (isFirst)
                    isFirst = false;
                else
                    dropDownListItem = dropDownListItem._clone();
                
                var context = $(this).data('dropDownList');
            
                var ddlItems = new Array();
                var inserted = false;
                for (var i = 0; i < context.internal.items.length; i++)
                {
                    if (i == index)
                    {
                        inserted = true;
                        ddlItems[ddlItems.length] = dropDownListItem;
                    }
                    
                    ddlItems[ddlItems.length] = context.internal.items[i];
                }
                
                if (!inserted)
                    ddlItems[ddlItems.length] = dropDownListItem;
                
                context.internal.items = ddlItems;
            });
        }, 
        getByIndex : function(index)
        {
            var context = this.data('dropDownList');
            
            if (context && index >= 0 && index < context.internal.items.length)
                return context.internal.items[index];

            return null;
        }, 
        count : function()
        {
            var context = this.data('dropDownList');
            if (context)
                return context.internal.items.length;
            
            return undefined;
        },
        createItem: function(options)
        {
            return new DropDownListItem(options);
        }
    };
    
    // private methods
    var _init = function(options) 
        {
            return this.each(function() 
            {
                var context = {
                    settings: $.extend({}, $.fn.glowDropDownList.defaults, options || {}),
                    internal: {
                        state: this,
                        highlightedItemIndex: -1,
                        button: null,
                        items: null,
                        popupPanel: null,
                        selectedItem: null,
                        backgroundColor: null,
                        itemsContainer: null,
                        initialized: false
                    }
                };
            
                $(this).data('dropDownList', context);

                _initialize(context);
            });
        }, 
        _initialize = function(context)
        {
            if(!context.internal.state.offsetHeight)
            {
                window.setTimeout(function() { _initialize(context); }, 249);
                return;
            }
        
            if (context.internal.state.disabled)
            {
                context.internal.state.disabled = false;
                context.internal.state.readonly = true;
            }
        
            context.internal.button = document.createElement('span');
            context.internal.button.style.borderStyle = 'outset';
            context.internal.button.style.borderWidth = '1px';
            context.internal.button.style.borderColor = '#999999';
            try { context.internal.button.style.display = 'inline-table'; } catch (e) {}
            try { context.internal.button.style.display = '-moz-inline-box'; } catch (e) {}
            try { context.internal.button.style.display = 'inline-block'; } catch (e) {}
            context.internal.button.style.paddingRight = '16px';
            context.internal.button.style.backgroundRepeat = 'no-repeat';
            context.internal.button.style.backgroundPosition = 'right center';
            context.internal.button.style.marginLeft = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'margin-left', 'marginLeft', '1px;');
            context.internal.button.style.marginTop = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'margin-top', 'marginTop', '1px;');
            context.internal.button.style.marginRight = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'margin-right', 'marginRight', '1px;');
            context.internal.button.style.marginBottom = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'margin-bottom', 'marginBottom', '1px;');
            context.internal.state.parentNode.insertBefore(context.internal.button, context.internal.state);
        
            context.internal.backgroundColor = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'background-color', 'backgroundColor', '#ffffff');
            if (context.internal.backgroundColor.replace(/\#[0-9a-f]*/gi, '') != '')
                context.internal.backgroundColor = '#ffffff';
        
            context.internal.selectedItem = document.createElement('span');
            context.internal.selectedItem.style.backgroundColor = context.internal.backgroundColor;
            context.internal.selectedItem.style.fontFamily = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-family', 'fontFamily', 'Arial, Helvetica');
            context.internal.selectedItem.style.fontSize = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-size', 'fontSize', '100%');
            context.internal.selectedItem.style.lineHeight = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'line-height', 'lineHeight', '100%');
            context.internal.selectedItem.style.color = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000000');
            context.internal.selectedItem.style.borderStyle = 'outset';
            context.internal.selectedItem.style.borderWidth = '0px';
            context.internal.selectedItem.style.borderRightWidth = '1px';
            context.internal.selectedItem.style.padding = '2px';
            try { context.internal.selectedItem.style.display = 'inline-table'; } catch (e) {}
            try { context.internal.selectedItem.style.display = '-moz-inline-box'; } catch (e) {}
            try { context.internal.selectedItem.style.display = 'inline-block'; } catch (e) {}
        
            if (context.settings.selectedItemWidth <= 23)
                context.internal.selectedItem.style.width = (context.internal.state.offsetWidth - 23) + 'px';
            else
                context.internal.selectedItem.style.width = (context.settings.selectedItemWidth - 23) + 'px';
        
            if (context.settings.selectedItemHeight <= 4)
                context.internal.selectedItem.style.height = (context.internal.state.offsetHeight - 4) + 'px';
            else
                context.internal.selectedItem.style.height = (context.settings.selectedItemHeight - 4) + 'px';
        
            context.internal.selectedItem.style.overflow = 'hidden';
            context.internal.button.appendChild(context.internal.selectedItem);
        
            var popup = document.createElement('div');
            context.internal.state.parentNode.insertBefore(popup, context.internal.state);
            context.internal.popupPanel = $(popup).glowPopUpPanel({
                'cssClass':'',
                'position':'updown',
                'zIndex':1000,
                'hideOnDocumentClick':true
            });
            context.internal.popupPanel.glowPopUpPanel('empty');
        
            context.internal.itemsContainer = document.createElement('div');
            context.internal.itemsContainer.style.borderStyle = 'outset';
            context.internal.itemsContainer.style.borderWidth = '1px';
            context.internal.itemsContainer.style.borderColor = '#999999';
        
            if (context.settings.itemsHeight <= 4)
                context.internal.itemsContainer.style.height = (($.telligent.glow.utility.getWindowInfo().Height / 4) + 24) + 'px';
            else
                context.internal.itemsContainer.style.height = (context.settings.itemsHeight - 4) + 'px';
        
            if (context.settings.itemsWidth <= 4)
                context.internal.itemsContainer.style.width = (context.internal.state.offsetWidth - 4) + 'px';
            else
                context.internal.itemsContainer.style.width = (context.settings.itemsWidth - 4) + 'px';
        
            context.internal.itemsContainer.style.overflow = 'auto';
            context.internal.itemsContainer.style.overflowX = 'hidden';
            context.internal.itemsContainer.style.padding = '1px';
            context.internal.itemsContainer.style.backgroundColor = context.internal.backgroundColor;
            context.internal.itemsContainer.style.fontFamily = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-family', 'fontFamily', 'Arial, Helvetica');
            context.internal.itemsContainer.style.fontSize = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-size', 'fontSize', '100%');
            context.internal.itemsContainer.style.lineHeight = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'line-height', 'lineHeight', '100%');
            context.internal.itemsContainer.style.color = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000000');
            context.internal.popupPanel.glowPopUpPanel('append', context.internal.itemsContainer);
        
            context.internal.items = new Array();
            for (var i = 0; i < context.internal.state.options.length; i++)
            {
                if (i < context.settings.itemsHtml.length)
                    context.internal.items[context.internal.items.length] = new DropDownListItem({value: context.internal.state.options[i].value, text: context.internal.state.options[i].text, html: context.settings.itemsHtml[i]});
                else
                    context.internal.items[context.internal.items.length] = new DropDownListItem({value: context.internal.state.options[i].value, text: context.internal.state.options[i].text, html: context.internal.state.options[i].text});                    
            }
        
            api.disabled.apply($(context.internal.state), [context.internal.state.readonly == true]);
            context.internal.state.style.display = 'none';
        
            context.internal.initialized = true;
            api.refresh.apply($(context.internal.state), []);
        }, 
        _show = function(context)
        {
            if (context.internal.popupPanel.glowPopUpPanel('isShown')) 
                context.internal.popupPanel.glowPopUpPanel('hide'); 
            else 
            { 
                if (!$.telligent.glow.utility.isSafari())
                {
                    context.internal.itemsContainer.style.overflow = 'hidden';
                    context.internal.popupPanel.glowPopUpPanel('show', context.internal.button); 
                    window.setTimeout(function() { _reenableScrolling(context); }, 49);
                }
                else
                {
                    context.internal.popupPanel.glowPopUpPanel('show', context.internal.button);            
                    if (context.internal.state.selectedIndex >= 0)
                        _highlightItem(context, context.internal.state.selectedIndex);
                }
            }
        }, 
        _reenableScrolling = function(context)
        {
            if (!context.internal.popupPanel.glowPopUpPanel('isOpening'))
            {
                try
                {
                    context.internal.itemsContainer.style.overflow = 'auto';
                    context.internal.itemsContainer.style.overflowX = 'hidden';
                } catch (e) { }
            
                if (context.internal.state.selectedIndex >= 0)
                        _highlightItem(context, context.internal.state.selectedIndex);
            }
            else
                window.setTimeout(function() { _reenableScrolling(); }, 49);
        }, 
        _itemMouseOver = function(context, index)
        {
            _highlightItem(context, index);
        }, 
        _highlightItem = function(context, index)
        {
            var e = context.internal.itemsContainer.childNodes[index];
            e.style.backgroundColor = '#cccccc';
            e.style.borderColor = '#666666';
        
            if (e.offsetTop + e.offsetHeight > context.internal.itemsContainer.scrollTop + context.internal.itemsContainer.offsetHeight)
                context.internal.itemsContainer.scrollTop = (e.offsetTop + e.offsetHeight) - context.internal.itemsContainer.offsetHeight;
            else if (e.offsetTop < context.internal.itemsContainer.scrollTop)
                context.internal.itemsContainer.scrollTop = e.offsetTop;
        
            if (context.internal.highlightedItemIndex >= 0 && context.internal.highlightedItemIndex != index)
            {
                e = context.internal.itemsContainer.childNodes[context.internal.highlightedItemIndex];
                e.style.backgroundColor = context.internal.backgroundColor; 
                e.style.borderColor = context.internal.backgroundColor;
            }
        
            context.internal.highlightedItemIndex = index;
        };    
    
    $.fn.glowDropDownList = function(method) 
    {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowDropDownList');
        }
    };  
    $.extend($.fn.glowDropDownList, {
        defaults: {
            selectedItemWidth: 200,
            selectedItemHeight: 32,
            itemsWidth: 200,
            itemsHeight: 300,
            itemsHtml: [],
            showHtmlWhenSelected: true,
            buttonImageUrl: 'button.gif'
        }
    });
        
    var DropDownListItem = function(options)
    {
        var settings = 
        {
            value: '',
            text: '',
            html: options.text
        };

        if (options)
            $.extend(settings, options);

        this.text = settings.text;
        this.value = settings.value;
        this.html = settings.html;
    }

    DropDownListItem.prototype._clone = function()
    {
        return new DropDownListItem({ value: this.value, text: this.text, html: this.html});
    }   
})(jQuery);

 
 
/// @name glowUpload
/// @category jQuery Plugin
/// @description Renders a single file upload form supporting chunked uploads with a visual progress bar
/// 
/// ### jQuery.fn.glowUpload
/// 
/// Renders a single file upload form supporting chunked uploads with a visual progress bar
/// 
/// ### Usage
/// 
///     $('SELECTOR').glowUpload(options)
/// 
/// where 'SELECTOR' is a div element containing file input
/// 
/// ### Options
/// 
///  * `fileFilter`: Array of filters to apply when selecting files.  Each filter is an object with format `{title : "Zip files", extensions : "zip"}`
///    * default: *null*
///  * `maxFileSize`: Maximum file size of a selectable file
///    * default: *'51200kb'*
///  * `width`: Width of control
///    * default: *'350px'*
///  * `uploadUrl`: URL to upload against.  With Studio Widgets, this is the result of a call to create a context ID and build an upload URL for it:  `$core_v2_uploadedFile.GetUploadUrl($core_v2_uploadedFile.CreateContextId())`
///    * default: *'/'*
///  * `swfUrl`: Path to SWF upload component
///    * default: *$.telligent.evolution.site.getBaseUrl() + 'utility/images/glow/plupload.flash.swf'*
///  * `xapUrl`: Path to Silverlight upload component
///    * default: *$.telligent.evolution.site.getBaseUrl() + 'utility/images/glow/plupload.silverlight.swf'*
///  * `uploadedFormat`: HTML template to use for an uploaded item
///    * default: *'&lt;div style="overflow: hidden; text-overflow: ellipsis; color: #000; padding: .25em;"&gt;&lt;div style="float: right; color: #3a3; margin-left: .25em;"&gt;Uploaded&lt;/div&gt;{0}&lt;/div&gt;'*
///  * `uploadingFormat`: HTML template to use for an uploading item
///    * default: *'&lt;div style="overflow: hidden; text-overflow: ellipsis; color: #000; padding: .25em;"&gt;&lt;div style="float: right; margin-top: .25em; margin-left: .25em; border: solid 1px #999; background-color: #eee; width: 50px; height: .5em;"&gt;&lt;div style="background-color: #7e7; width: {1}%; height: .5em;"&gt;&lt;/div&gt;&lt;/div&gt;{0}&lt;/div&gt;'*
///  * `toUploadFormat`: HTML template to use for an item to be uploaded
///    * default: *'&lt;div style="overflow: hidden; text-overflow: ellipsis; color: #666; font-style: italic; padding: .25em;"&gt;{0}&lt;/div&gt;'*
///  * `errorFormat`: HTML template to use for an error
///    * default: *'&lt;div style="overflow: hidden; text-overflow: ellipsis; color: #000; padding: .25em;"&gt;&lt;div style="float: right; color: #f00; margin-left: .25em;"&gt;Error&lt;/div&gt;{0}&lt;/div&gt;'*
///  * `runtimes`: Known upload runtimes to attempt to use
///    * default: *'gears,silverlight,flash,html5'*
/// 
/// ### Methods
/// 
/// #### val
/// 
/// Returns the file name of the uploaded file
/// 
///     var fileName = $('SELECTOR').glowUpload('val');
/// 

(function($,global){

    // public api
    var api = {
        val: function() {
            var context = $(this).data(_dataKey);
            if(context !== null && typeof context.file !== 'undefined' && context.file !== null) {
                return { name: context.file.name };
            } else {
                return null;
            }
        }
    };

    // private methods
    var _dataKey = '_glowUpload',
        _eventNames = {
            begun: 'glowUploadBegun',
            complete: 'glowUploadComplete',
            error: 'glowUploadError'
        },
        _init = function(options) {
            return this.each(function()
            {
                var elm = $(this);

                var context = $.extend({}, $.fn.glowUpload.defaults, options || {}, {
                    pluploader: null,
                    file: null,
                    fileOffset: 0,
                    fileState: '',
                    isUploading: false,
                    currentUploadPercent: 0,
                    fileContainer: null
                });

                $.extend(context, {
                    container: elm,
                    initialized: false
                });

                $(this).data(_dataKey, context);

                context.container.find('input[type="file"]')
                .bind('change', function(){
                    _inputChange(context);
                }).hide();

                $('<div></div>')
                .css({
                    fontSize: '11px',
                    borderWidth: '1px',
                    borderStyle: 'solid',
                    borderColor: '#999',
                    width: context.width,
                    overflow: 'auto',
                    overflowX: 'hidden',
                    backgroundColor: '#eee',
                    textAlign: 'center',
                    padding: '.25em'
                })
                .append($('<input type="Button" value="Loading..." />')
                	.attr('disabled','disabled')
                	.css({
                		borderColor: 'Transparent',
                		backgroundColor: 'Transparent'
                	})
                )
                .appendTo(context.container)

                _determineUploadSpeed(context);
            });
        },
        _determineUploadSpeed = function(context) {
        	var testStrings = ["1234567890", "0987654321", "0192837465", "5647382910"];
        	var testData = [];
        	for (var i = 0; i < 2500; i++) { testData[testData.length] = testStrings[Math.floor((Math.random()*testStrings.length))]; }
        	var startMs = (new Date()).getTime();

        	jQuery.ajax({
        		type: 'POST',
        		url: context.uploadUrl,
        		data: {
        			test: testData.join('')
        		},
        		success: function() {
        			var endMs = (new Date()).getTime();
        			_initialize(context, ((25000 * 8) / ((endMs - startMs) / 1000)) * 45);
        		},
        		error: function() {
        			// test failed: default to max upload of 1mb
        			_initialize(context, 1000000);
        		}
        	});
        },
        _initialize = function(context, maxUploadSpeed) {
            if (/Mozilla[^\(]*\(.*? rv:([0-9]+)\.([0-9]+)\.([0-9]+)/.test(navigator.userAgent))
            {
                if (parseInt(RegExp.$1, 10) < 1 || parseInt(RegExp.$2, 10) < 9 || parseInt(RegExp.$3, 10) < 2)
                {
                    // not using the version of Gecko in FireFox 3.6+, use HTML5 only.
                    context.runtimes = context.runtimes.replace(/,?flash/i, '');
                }
            }

            context.file = {
                name: '',
                id: 'preuploaded',
                fileState: 'uploaded'
            };

            var chunkSize = '15mb';
            if (maxUploadSpeed < 15000000)
            {
            	chunkSize = Math.floor(maxUploadSpeed);
            	if (chunkSize < 1000000)
            	{
            		chunkSize = 1000000
            	}

            	chunkSize = Math.floor(maxUploadSpeed / 1000000) + 'mb'
            }

            var pluploadOptions = {
                runtimes: context.runtimes,
                max_file_size: context.maxFileSize,
                container: context.container.attr('id'),
                url: context.uploadUrl,
                flash_swf_url: context.swfUrl,
                silverlight_xap_url: context.silverlightUrl,
                chunk_size: chunkSize,
                multi_selection: false,
                drop_element: context.container.attr('id')
            };
            if (context.fileFilter !== null) {
                pluploadOptions.filters = context.fileFilter;
            }
            context.pluploader = new plupload.Uploader(pluploadOptions);

            context.pluploader.bind('Init', function(up) {
                _uploaderInit(context);
            });
            context.pluploader.bind('FilesAdded', function(up, files) {
                _filesAdded(context, files);
            });
            context.pluploader.bind('QueueChanged', function(up) {
                _filesQueued(context);
            });
            context.pluploader.bind('UploadProgress', function(up, file) {
                _fileProgress(context, file);
            });
            context.pluploader.bind('FileUploaded', function(up, file, response) {
                _fileComplete(context, file);
            });
            context.pluploader.bind('Error', function(up, err) {
                _error(context, err);
            });
            global.setTimeout(function() {
                context.pluploader.init();
            }, 9);
        },
        _uploaderInit = function(context)
        {
            // remove the non-plupload control
            context.container.children(':not(.plupload)').remove();

            var innerContainer = $('<div></div>')
                .css({
                    fontSize: '11px',
                    borderWidth: '1px',
                    borderStyle: 'solid',
                    borderColor: '#999',
                    width: context.width,
                    overflow: 'auto',
                    overflowX: 'hidden',
                    backgroundColor: '#fff'
                })
                .appendTo(context.container);

            var table = $('<table></table>')
                .attr('border', 0)
                .attr('cellSpacing', 0)
                .attr('cellPadding', 0)
                .css({
                    width: '100%'
                });

            var tbody = $('<tbody></tbody>')
                .appendTo(table);

            var tr = $('<tr></tr>')
                .appendTo(tbody);

            context.fileContainer = $('<td></td>')
                .appendTo(tr)
                .css({ width: '100%' });

            var td = $('<td></td>')
                .css({
                    backgroundColor: '#eee',
                    borderLeftWidth: '1px',
                    borderLeftStyle: 'solid',
                    borderLeftColor: '#999',
                    padding: '.25em'
                })
                .attr('vAlign', 'top')
                .appendTo(tr);

            var browseButton = $('<input type="Button" value="Browse..." id="' + context.container.attr('id') + '_browseButton" />')
                .appendTo(td);

            innerContainer.append(table);

            _updateFileUi(context);

            context.pluploader.settings.browse_button = context.container.attr('id') + '_browseButton';

            context.initialized = true;
        },
        _updateFileUi = function(context)
        {
            var ui = '';

            if (context.file)
            {
                if (context.fileState === 'uploaded') {
                    ui = context.uploadedFormat.replace('{0}', context.file.name);
                }
                else if (context.fileState === 'error') {
                    ui = context.errorFormat.replace('{0}', context.file.name);
                }
                else if (context.isUploading) {
                    ui = context.uploadingFormat.replace('{0}', context.file.name).replace('{1}', context.currentUploadPercent);
                }
                else {
                    ui = context.toUploadFormat.replace('{0}', context.file.name);
                }
            }

            context.fileContainer.html(ui);
        },
        _inputChange = function(context)
        {
            context.container
                .trigger(_eventNames.begun)
                .trigger(_eventNames.complete);
        },
        _filesAdded = function(context, files)
        {
            context.addedFiles = files;
        },
        _filesQueued = function(context)
        {
            if (!context.addedFiles) {
                return;
            }

            var files = [];
            for (var i = 0; i < context.pluploader.files.length; i++) {
                for (var j = 0; j < context.addedFiles.length; j++) {
                    if (context.pluploader.files[i].id == context.addedFiles[j].id) {
                        files[files.length] = context.addedFiles[j];
                        break;
                    }
                }
            }
            context.addedFiles = null;

            if (files.length === 0) {
                return;
            }

            if (context.file)
            {
                if (context.isUploading) {
                    context.pluploader.stop();
                }

                var oldId = context.file.id;
                global.setTimeout(function() {
                    _cancelFile(context, oldId);
                }, 99);
            }

            context.file = files[0];
            context.fileState = '';
            _updateFileUi(context);

            if (context.file) {
                context.pluploader.start();
            }
        },
        _error = function(context, err)
        {
            if (!context.initialized)
            {
            	var message = context.notSupportedMessage;
            	if (!message)
            	{
	            	var runtimes = [];
	            	if (context.runtimes.indexOf('html5') > -1)
	            	{
	            		runtimes.push('An HTML5 capable browser');
	            	}
	            	
	            	if (context.runtimes.indexOf('silverlight') > -1)
	            	{
	            		runtimes.push('<a href="http://microsoft.com/getsilverlight/" target="_blank">Silverlight</a>');
	            	}
	            	
	            	if (context.runtimes.indexOf('flash') > -1)
	            	{
	            		runtimes.push('<a href="http://get.adobe.com/flashplayer/" target="_blank">Flash</a>');
	            	}
	            	
	            	var message;
	            	if (runtimes.length == 0)
	            	{
	            		message = 'Uploading is not supported.';
	            	}
	            	else
	            	{
	            		if (runtimes.length > 1)
	            		{
	            			runtimes[runtimes.length - 1] = 'or ' + runtimes[runtimes.length - 1];
	            		}
	            		
	            		message = runtimes.join(', ') + ' is required to upload.';
	            	}
	            }
            	
            	context.container.children().hide();
            	$('<div></div>')
                .css({
                    fontSize: '11px',
                    borderWidth: '1px',
                    borderStyle: 'solid',
                    borderColor: '#999',
                    width: context.width,
                    overflow: 'auto',
                    overflowX: 'hidden',
                    backgroundColor: '#eee',
                    textAlign: 'center',
                    padding: '.25em',
                   	color: '#000'
                })
                .html(message)
                .appendTo(context.container)                
            }

            if (err.file)
            {
                context.isUploading = false;
                context.fileState = 'error';
                _updateFileUi(context);

                context.container.trigger(_eventNames.error);
            }
        },
        _fileProgress = function(context, file)
        {
            if (!file || !context.file || file.id != context.file.id) {
                return;
            }
            else if (file.status == plupload.UPLOADING)
            {
                if (!context.isUploading)
                {
                    var execEvent = !context.isUploading;
                    context.isUploading = true;
                    context.currentUploadPercent = 0;
                    _updateFileUi(context);

                    if (execEvent) {
                        context.container.trigger(_eventNames.begun);
                    }
                }

                context.currentUploadPercent = file.percent;
                _updateFileUi(context);
            }
            else if (file.status == plupload.FAILED)
            {
                context.isUploading = false;
                context.fileState = 'error';
                _updateFileUi(context);

                context.container.triggerr('glowUploadError');
            }
        },
        _fileComplete = function(context, file)
        {
            context.fileState = 'uploaded';
            _updateFileUi(context);

            context.container.trigger(_eventNames.complete, { name: file.name });
        },
        _cancelFile = function(context, id)
        {
            if (context.pluploader && id != 'preuploaded')
            {
                try
                {
                    context.pluploader.removeFile(context.pluploader.getFile(id));
                }
                catch (e) { }
            }
        };

    $.fn.glowUpload = function(method) {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowUpload');
        }
    };
    $.extend($.fn.glowUpload, {
        defaults: {
            fileFilter: null,
            maxFileSize: '2048mb',
            width: '350px',
            uploadUrl: '/',
            swfUrl: 'plupload.flash.swf',
            silverlightUrl: '',
            uploadedFormat: '<div style="overflow: hidden; text-overflow: ellipsis; color: #000; padding: .25em;"><div style="float: right; color: #3a3; margin-left: .25em;">Uploaded</div>{0}</div>',
            uploadingFormat: '<div style="overflow: hidden; text-overflow: ellipsis; color: #000; padding: .25em;"><div style="float: right; margin-top: .25em; margin-left: .25em; border: solid 1px #999; background-color: #eee; width: 50px; height: .5em;"><div style="background-color: #7e7; width: {1}%; height: .5em;"></div></div>{0}</div>',
            toUploadFormat: '<div style="overflow: hidden; text-overflow: ellipsis; color: #666; font-style: italic; padding: .25em;">{0}</div>',
            errorFormat: '<div style="overflow: hidden; text-overflow: ellipsis; color: #000; padding: .25em;"><div style="float: right; color: #f00; margin-left: .25em;">Error</div>{0}</div>',
            runtimes: 'html5,flash'
        }
    });

})(jQuery, window);

 
 
/// @name glowLookUptextBox
/// @category jQuery Plugin
/// @description Renders an auto-completing textbox
/// 
/// ### jQuery.fn.glowLookUptextBox
/// 
/// This plugin presents an auto-completing textbox that allows for single or multiple selections
/// 
/// ### Usage
/// 
/// Initiate a selection of text input elements to be lookup text boxes
/// 
///     $('selector').glowLookUpTextBox(options)
/// 
/// where selector contains `<input type="text" />` element(s) and `options` is an optional object argument
/// 
/// ### Options
/// 
///  * `delimiter`: delimiter to insert between selected items when multiple items are allowed and to use when 
///    * default: `','`
///  * `allowDuplicates`: boolean value of whether or not multiple same items can be selected
///    * default: `false`
///  * `maxValues`: maximum number of selectable items per control
///    * default: `0`
///  * `onGetLookUps`: callback function which is passed both the current lookup text box instance and the currently-typed text.  Typically used to perform an ajax callback and then populate the textbox with available matching options.
///    * default: `null`
///  * `emptyHtml`: string displayed when no selection is made
///    * default: *empty string*
///  * `selectedLookUpsHtml`: array of strings to pre-populate as selected
///    * default: `[]`
///  * `deleteImageUrl`: URL of image to render within selected options which acts as a trigger for deleting the selected option
///    * default: *empty string*
///    
/// ### Events
/// 
///  * `glowLookUpTextBoxChange`: triggered when the value is changed
/// 
/// ### Methods
/// 
/// #### disabled
/// 
/// Gets and/or sets whether the lookup text box is disabled
/// 
///     var disabled = $('selector').glowLookUpTextBox('disabled');
///     $('selector').glowLookUpTextBox('disabled', false);
/// 
/// #### updateSuggestions
/// 
/// Sets the current list of autocompletion suggestions.  Often used after a successful Ajax callback.
/// 
///     var suggestions = ["abc", "def", "ghi"];
///     $('selector').glowLookUpTextBox('updateSuggestions', suggestions);
/// 
/// #### removeByIndex
/// 
/// Removes a current selection by index
/// 
///     $('selector').glowLookUpTextBox('removeByIndex', 3);
/// 
/// #### removeByValue
/// 
/// Removes a current selection by value
/// 
///     $('selector').glowLookUpTextBox('removeByValue', 'value');
/// 
/// #### add
/// 
/// Adds a selected value
/// 
///     var lookup = $('selector').glowLookUpTextBox('createLookup', 'value', 'selected html', 'suggested html', true);
///     $('selector').glowLookUpTextBox('removeByValue', lookup);
/// 
/// #### getByIndex
/// 
/// Gets the a selected lookup by index
/// 
///     var selectedLookup = $('selector').glowLookUpTextBox('getByIndex', 2);
/// 
/// #### count
/// 
/// Gets the count of selected lookups
/// 
///     var count = $('selector').glowLookUpTextBox('count');
/// 
/// #### getCurrentSearchText
/// 
/// Gets the current search text
/// 
///     var text = $('selector').glowLookUpTextBox('getCurrentSearchText');
/// 
/// #### createLookUp
/// 
/// Manually creates a new lookup object suitable for adding as a selected lookup
/// 
///     var lookup = $('selector').glowLookUpTextBox('createLookup', 'value', 'selected html', 'suggested html', true);
/// 

(function($){

    // publicly exposed api
    var api = 
    {
        disabled: function(disable)
        {
        	if (disable === undefined)
        	{
        		var context = this.data('lookUpTextBox');
	            if (context)
	                return context.internal.state.readonly;
	            
	            return undefined;
        	}
        	else
        	{
	            return this.each(function() 
	            {
	                var context = $(this).data('lookUpTextBox');
	                
	                if (disable)
	                {
		                context.internal.cursor.style.visibility = 'hidden';
		                context.internal.container.style.cursor = 'default';
		                context.internal.container.onclick = null;
		                context.internal.state.readonly = true;
		                context.internal.emptyHtmlContainer.style.visibility = 'hidden';
	                }
	                else
	                {
	                	context.internal.cursor.style.visibility = 'visible';
		                context.internal.container.style.cursor = 'text';
		                context.internal.container.onclick = function(event) { _containerClick(context, event); };
		                context.internal.state.readonly = false;
		                context.internal.emptyHtmlContainer.style.visibility = 'visible';
	                }
	                
	                _refreshSelectedLookUps(context);
	            });
			}
        },
        updateSuggestions: function(suggestions)
        {
            return this.each(function() 
            {
                var context = $(this).data('lookUpTextBox');
                context.internal.lookUps = new Array();

                if (suggestions) 
                {
                    for (var i = 0; i < suggestions.length; i++) 
                    {
                        context.internal.lookUps[i] = suggestions[i];
                    }
                }

                context.internal.lookUpsIndex = -1;
                _refreshLookUps(context);
            });
        },
        removeByIndex: function(index)
        {
            return this.each(function() 
            {
                var context = $(this).data('lookUpTextBox');
                
                if (index >= 0 && index < context.internal.selectedLookUps.length) 
                {
                    var slu = new Array();
                    for (var i = 0; i < context.internal.selectedLookUps.length; i++) 
                    {
                        if (i != index)
                            slu[slu.length] = context.internal.selectedLookUps[i];
                    }

                    context.internal.selectedLookUps = slu;
                    _refreshSelectedLookUps(context);
                    _save(context);
                }
            });
        },
        removeByValue: function(value)
        {
            return this.each(function()
            {
                var context = $(this).data('lookUpTextBox');
                
                var slu = new Array();
                var found = false;
                for (var i = 0; i < context.internal.selectedLookUps.length; i++) 
                {
                    if (context.internal.selectedLookUps[i].Value != value)
                        slu[slu.length] = context.internal.selectedLookUps[i];
                    else
                        found = true;
                }

                if (found) 
                {
                    context.internal.selectedLookUps = slu;
                    _refreshSelectedLookUps(context);
                    _save(context);
                }
            });
        },
        add: function(lookUp)
        {
            return this.each(function()
            {
                if (lookUp != null && lookUp.SelectedHtml) 
                {
                    var context = $(this).data('lookUpTextBox');
                    
                    context.internal.selectedLookUps[context.internal.selectedLookUps.length] = lookUp;
                    _refreshSelectedLookUps(context);
                    _save(context);
                }
            });
        },
        getByIndex: function(index)
        {
            var context = this.data('lookUpTextBox');
            if (context && index >= 0 && index < context.internal.selectedLookUps.length)
                return context.internal.selectedLookUps[index];

            return null;
        },
        count: function()
        {
            var context = this.data('lookUpTextBox');
            if (context)
                return context.internal.selectedLookUps.length;
            else
                return 0;
        },
        getCurrentSearchText: function()
        {
            var context = this.data('lookUpTextBox');
            if (context)
                return context.internal.lastCursorValue;
            else
                return '';
        },
        createLookUp: function(value, selectedHtml, suggestedHtml, isSelectable)
        {
            var lu = {};
            lu.Value = value;
            lu.SelectedHtml = selectedHtml;
            lu.SuggestedHtml = suggestedHtml;
            lu.Selectable = isSelectable;
            
            return lu;
        }
    }   
    
    // private methods
    var _changeEventName = 'glowLookUpTextBoxChange',
        _init = function(options) 
        {
            return this.each(function() 
            {
                var context = {
                    settings: $.extend({}, $.fn.glowLookUpTextBox.defaults, options || {}),
                    internal: {
                        state: this,
                        cursorContainer: null,
                        cursor: null,
                        cursorText: null,
                        lookUpsContainer: null,
                        lookUpsIndex: null,
                        lookUpsIndex: -1,
                        popupPanel: null,
                        lookUps: new Array(),
                        lastCursorValue: '',
                        selectedLookUps: new Array(),
                        mouseIsOverPopup: false,
                        emptyHtmlContainer: null,
                        cursorHasFocus: false,
                        cursorChangeHandler: null,
                        height: 18,
                        backgroundColor: '#ffffff',
                        lineHeight: '100%',
                        initialized: false
                    }
                };                  
            
                $(this).data('lookUpTextBox', context);

                _render(context);
            });
        },    
        _render = function(context)
        {
            if (!context.internal.state.offsetHeight) 
            {
                window.setTimeout(function ()
                {
                    _render(context);
                }, 249);
            
                return;
            }
        
            if (context.internal.state.disabled) 
            {
                context.internal.state.disabled = false;
                context.internal.state.readonly = true;
            }

            context.internal.backgroundColor = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'background-color', 'backgroundColor', '#ffffff');
            if (context.internal.backgroundColor.replace(/\#[0-9a-f]*/gi, '') != '')
                context.internal.backgroundColor = '#ffffff';

            context.internal.height = context.internal.state.offsetHeight - (parseInt($.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'padding-top', 'paddingTop', '0px'), 10) + parseInt($.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'padding-bottom', 'paddingBottom', '0px'), 10) + parseInt($.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-top-width', 'borderTopWidth', '0px'), 10) + parseInt($.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-bottom-width', 'borderBottomWidth', '0px'), 10));
            if (context.internal.height <= 0)
                context.internal.height = 18;

            context.internal.lineHeight = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'line-height', 'lineHeight', '100%');

            context.internal.container = document.createElement('div');
            context.internal.container.style.width = (context.internal.state.offsetWidth - (parseInt($.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'padding-left', 'paddingLeft', '0px'), 10) + parseInt($.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'padding-right', 'paddingRight', '0px'), 10) + parseInt($.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-left-width', 'borderLeftWidth', '0px'), 10) + parseInt($.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'border-right-width', 'borderRightWidth', '0px'), 10))) + 'px';
            context.internal.container.style.backgroundColor = context.internal.backgroundColor;
            context.internal.container.style.color = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000000');
            context.internal.container.style.borderStyle = 'solid';
            context.internal.container.style.borderWidth = '1px';
            context.internal.container.style.borderColor = '#999999';
            context.internal.container.style.padding = '2px';
            context.internal.container.style.display = 'block';
            context.internal.container.style.overflow = 'hidden';
            context.internal.state.parentNode.insertBefore(context.internal.container, context.internal.state);

            context.internal.cursorContainer = document.createElement('div');
            context.internal.cursorContainer.style.cssFloat = 'left';
            context.internal.cursorContainer.style.styleFloat = 'left';
            context.internal.cursorContainer.style.backgroundColor = context.internal.backgroundColor;
            context.internal.cursorContainer.style.borderWidth = '0px';
            context.internal.cursorContainer.style.padding = '1px 5px 5px 5px';
            context.internal.cursorContainer.style.overflow = 'hidden';
            context.internal.cursorContainer.style.width = 'auto';
            context.internal.cursorContainer.style.minWidth = '20px';
            context.internal.cursorContainer.style.position = 'relative';
            context.internal.cursorContainer.style.height = context.internal.height + 'px';
            context.internal.cursorContainer.style.lineHeight = context.internal.lineHeight;

            context.internal.emptyHtmlContainer = document.createElement('div');
            context.internal.emptyHtmlContainer.style.position = 'absolute';
            context.internal.emptyHtmlContainer.style.top = '3px';
            context.internal.emptyHtmlContainer.style.left = '3px';
            context.internal.emptyHtmlContainer.innerHTML = context.settings.emptyHtml;
            context.internal.cursorContainer.appendChild(context.internal.emptyHtmlContainer);

            context.internal.cursor = document.createElement('input');
            context.internal.cursor.type = 'text';
            context.internal.cursor.style.backgroundColor = context.internal.backgroundColor;
            context.internal.cursor.style.fontFamily = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-family', 'fontFamily', 'Arial, Helvetica');
            context.internal.cursor.style.fontSize = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-size', 'fontSize', '100%');
            context.internal.cursor.style.lineHeight = context.internal.lineHeight;
            context.internal.cursor.style.color = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000000');
            context.internal.cursor.style.borderWidth = '0px';
            context.internal.cursor.style.padding = '0px';
            context.internal.cursor.style.height = context.internal.height + 'px';
            context.internal.cursor.style.width = '100%';
            context.internal.cursor.onkeydown = function(event) { return _cursorKeyDown(context, event); };
            context.internal.cursor.onkeyup = function(event) { return _cursorChange(context, event); };
            context.internal.cursor.onchange = function(event) { return _cursorChange(context, event); }
            context.internal.cursor.onpaste = function(event) { return _cursorChange(context, event); };
            context.internal.cursor.onblur = function(event) { return _cursorBlur(context, event); };
            context.internal.cursor.onclick = function(event) { return _containerClick(context, event); };
            context.internal.cursor.onfocus = function(event) { return _cursorFocus(context, event); };
            context.internal.cursor.tabIndex = context.internal.state.tabIndex;
            context.internal.cursorContainer.appendChild(context.internal.cursor);

            context.internal.container.appendChild(context.internal.cursorContainer);

            var clearDiv = document.createElement('div');
            clearDiv.style.clear = 'both';
            context.internal.container.appendChild(clearDiv);

            var popup = document.createElement('div');
            context.internal.container.appendChild(popup);
            context.internal.popupPanel = $(popup);

            context.internal.popupPanel
                .glowPopUpPanel({
                    cssClass:'',
                    position:'updown',
                    zIndex:1000,
                    hideOnDocumentClick:true })
                .bind({
                    glowPopUpPanelHidden: function() { context.internal.mouseIsOverPopup = false; },
                    glowPopUpPanelMouseOver: function() { _popupMouseOver(context); },
                    glowPopUpPanelMouseOut: function() { _popupMouseOut(context); } });
        
            context.internal.popupPanel.glowPopUpPanel('empty');

            context.internal.lookUpsContainer = document.createElement('div');
            context.internal.lookUpsContainer.style.borderStyle = 'outset';
            context.internal.lookUpsContainer.style.borderWidth = '1px';
            context.internal.lookUpsContainer.style.borderColor = '#999999';

            context.internal.lookUpsContainer.style.height = (($.telligent.glow.utility.getWindowInfo().Height / 4) + 24) + 'px';
            context.internal.lookUpsContainer.style.width = (context.internal.state.offsetWidth - 4) + 'px';

            context.internal.lookUpsContainer.style.overflow = 'auto';
            context.internal.lookUpsContainer.style.overflowX = 'hidden';
            context.internal.lookUpsContainer.style.padding = '1px';
            context.internal.lookUpsContainer.style.backgroundColor = context.internal.backgroundColor;
            context.internal.lookUpsContainer.style.fontFamily = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-family', 'fontFamily', 'Arial, Helvetica');
            context.internal.lookUpsContainer.style.fontSize = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-size', 'fontSize', '100%');
            context.internal.lookUpsContainer.style.lineHeight = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'line-height', 'lineHeight', '100%');
            context.internal.lookUpsContainer.style.color = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000000');
            context.internal.popupPanel.glowPopUpPanel('append', context.internal.lookUpsContainer);

            context.internal.state.style.display = 'none';

            var selectedValues = context.internal.state.value.split(context.settings.delimiter);
            if (selectedValues.length == context.settings.selectedLookUpsHtml.length) {
                context.internal.selectedLookUps = new Array();
                for (var i = 0; i < selectedValues.length; i++) {
                    context.internal.selectedLookUps[context.internal.selectedLookUps.length] = api.createLookUp(selectedValues[i], context.settings.selectedLookUpsHtml[i], context.settings.selectedLookUpsHtml[i], true);
                }
            }

            context.internal.initialized = true;
        
            _refreshLookUps(context);

			api.disabled.apply($(context.internal.state), [context.internal.state.readonly === true]);
        },
        _showLookUps = function(context)
        {
            context.internal.lookUpsContainer.style.overflow = 'hidden';
            context.internal.lookUpsContainer.style.height = '200px';
            context.internal.popupPanel.glowPopUpPanel('show', context.internal.container, context.internal.popupPanel.glowPopUpPanel('isShown'));
            window.setTimeout(function() { _reenableScrolling(context); }, 49);
        },
        _hideLookUps = function(context)
        {
            if (context.internal.popupPanel && context.internal.popupPanel.glowPopUpPanel('isShown'))
                context.internal.popupPanel.glowPopUpPanel('hide');
        },
        _reenableScrolling = function(context)
        {
            if (!context.internal.popupPanel.glowPopUpPanel('isOpening')) 
            {
                try 
                {
                    context.internal.lookUpsContainer.style.overflow = 'auto';
                    context.internal.lookUpsContainer.style.overflowX = 'hidden';
                } 
                catch (e) { }

                if (context.internal.state.selectedIndex >= 0)
                    _highlightItem(context, context.internal.state.selectedIndex);
            }
            else
                window.setTimeout(function() { _reenableScrolling(context); }, 49);
        },
        _refreshLookUps = function(context)
        {
            if (context && context.internal.initialized)
            {
                while (context.internal.lookUpsContainer.childNodes.length > 0)
                    context.internal.lookUpsContainer.removeChild(context.internal.lookUpsContainer.childNodes[0]);

                if (context.internal.lookUps.length > 0) 
                {
                    var selectedIndex = context.internal.lookUpsIndex;

                    for (var i = 0; i < context.internal.lookUps.length; i++) 
                    {
                        var item = document.createElement('div');
                        item.onmouseover = $.telligent.glow.utility.makeBoundFunction(_lookUpMouseOver, null, [context, i]);
                        item.onclick = $.telligent.glow.utility.makeBoundFunction(_lookUpClick, null, [context, i]);
                        item.style.padding = '2px';
                        item.style.cursor = 'pointer';
                        item.style.borderStyle = 'dotted';
                        item.style.borderWidth = '1px';
                        item.style.borderColor = context.internal.backgroundColor;
                        item.innerHTML = context.internal.lookUps[i].SuggestedHtml;
                        context.internal.lookUpsContainer.appendChild(item);
                    }

                    if (selectedIndex < 0 || selectedIndex >= context.internal.lookUps.length)
                        selectedIndex = 0;
                
                    _lookUpMouseOver(context, selectedIndex);
                    _showLookUps(context);
                }
                else
                    _hideLookUps(context);
            }
        },
        _refreshSelectedLookUps = function(context) 
        {
            while (context.internal.container.childNodes.length > 3)
                context.internal.container.removeChild(context.internal.container.childNodes[0]);

            for (var i = 0; i < context.internal.selectedLookUps.length; i++) 
            {
                var outerItem = document.createElement('div');
                outerItem.style.padding = '2px';
                if (context.settings.maxValues != 1) 
                {
                    outerItem.style.cssFloat = 'left';
                    outerItem.style.styleFloat = 'left';
                }

                var item = document.createElement('div');

                item.style.borderStyle = 'solid';
                item.style.borderColor = '#bbbbbb';
                item.style.backgroundColor = '#eeeeee';
                item.style.borderWidth = '1px';
                item.style.padding = '2px';
                item.style.paddingRight = '6px';
                if (api.disabled.apply($(context.internal.state)))
                    item.style.paddingLeft = '6px';
                item.style.height = context.internal.height + 'px';
                item.style.lineHeight = context.internal.lineHeight;
                item.style.whiteSpace = 'nowrap';
                item.style.cursor = 'default';
                item.style.fontFamily = context.internal.cursor.style.fontFamily;
                item.style.fontSize = context.internal.cursor.style.fontSize;

                if (!api.disabled.apply($(context.internal.state))) 
                {
                    if (context.settings.deleteImageUrl)
                    {
                        var image = document.createElement('img');
                        image.src = context.settings.deleteImageUrl;
                        image.align = 'top';
                        image.style.padding = '4px';
                        image.style.paddingRight = '6px';
                        image.style.cursor = 'pointer';
                        image.onclick = $.telligent.glow.utility.makeBoundFunction(_deleteClick, null, [context, i]);
                        item.appendChild(image);
                    }
                    else
                    {
                        var a = document.createElement('a');
                        a.href = '#';
                        a.onclick = $.telligent.glow.utility.makeBoundFunction(_deleteClick, null, [context, i]);
                        a.style.textDecoration = 'none';
                        a.style.color = '#f00';
                        a.style.fontWeight = 'bold';
                        a.style.fontSize = '80%';
                        a.style.lineHeight = context.internal.lineHeight;
                        a.style.marginLeft = '4px';
                        a.style.marginRight = '6px';
                        a.innerHTML = 'X';
                        item.appendChild(a);
                    }
                }

                var s = document.createElement('span');
                s.innerHTML = context.internal.selectedLookUps[i].SelectedHtml;
                s.style.cursor = 'default';
                item.appendChild(s);

                outerItem.appendChild(item);

                context.internal.container.insertBefore(outerItem, context.internal.cursorContainer);
            }

            if (context.settings.maxValues > 0 && context.settings.maxValues == context.internal.selectedLookUps.length) 
            {
                context.internal.container.childNodes[context.internal.container.childNodes.length - 2].style.display = 'none';
                context.internal.container.childNodes[context.internal.container.childNodes.length - 3].style.display = 'none';
            }
            else 
            {
                context.internal.container.childNodes[context.internal.container.childNodes.length - 2].style.display = 'inline';
                context.internal.container.childNodes[context.internal.container.childNodes.length - 3].style.display = 'inline';
            }
        },
        _deleteClick = function(context, index)
        {
            api.removeByIndex.apply($(context.internal.state), [index]);

            $(context.internal.state).trigger(_changeEventName)
        
            return false;
        },
        _selectLookUp = function(context, index)
        {
            if (context.internal.lookUps[index] && context.internal.lookUps[index].Selectable) 
            {
                if (!context.settings.allowDuplicates) 
                {
                    for (var i = 0; i < context.internal.selectedLookUps.length; i++) 
                    {
                        if (context.internal.selectedLookUps[i].Value == context.internal.lookUps[index].Value)
                            return;
                    }
                }

                context.internal.selectedLookUps[context.internal.selectedLookUps.length] = api.createLookUp(context.internal.lookUps[index].Value, context.internal.lookUps[index].SelectedHtml);
                context.internal.cursor.value = '';
                _hideLookUps(context);
                _refreshSelectedLookUps(context);
                _save(context);
                try { context.internal.cursor.focus(); } catch (err) { }

                $(context.internal.state).trigger(_changeEventName)
            }
        },
        _lookUpMouseOver = function(context, index)
        {
            if (context.internal.lookUps[index] && context.internal.lookUps[index].Selectable) 
            {
                var e = context.internal.lookUpsContainer.childNodes[index];
                e.style.backgroundColor = '#cccccc';
                e.style.borderColor = '#666666';

                if (e.offsetTop + e.offsetHeight > context.internal.lookUpsContainer.scrollTop + context.internal.lookUpsContainer.offsetHeight)
                    context.internal.lookUpsContainer.scrollTop = (e.offsetTop + e.offsetHeight) - context.internal.lookUpsContainer.offsetHeight;
                else if (e.offsetTop < context.internal.lookUpsContainer.scrollTop)
                    context.internal.lookUpsContainer.scrollTop = e.offsetTop;

                if (context.internal.lookUpsIndex >= 0 && context.internal.lookUpsIndex != index) 
                {
                    e = context.internal.lookUpsContainer.childNodes[context.internal.lookUpsIndex];
                    e.style.backgroundColor = context.internal.backgroundColor;
                    e.style.borderColor = context.internal.backgroundColor;
                }

                context.internal.lookUpsIndex = index;
            }
        },
        _lookUpClick = function(context, index)
        {
            _selectLookUp(context, index);
            return false;   
        },
        _checkForChange = function(context)
        {
            if (context.internal.lastCursorValue != context.internal.cursor.value && context.internal.cursor.value.length > 0) 
                _cursorChange(context);
            else
                context.internal.cursorChangeHandler = window.setTimeout(function() { _checkForChange(context) }, 149);
        },
        _cursorChange = function(context, event)
        {
            if (!event)
                event = window.event;

            if (context.internal.lastCursorValue != context.internal.cursor.value) 
            {
                if (context.internal.cursorChangeHandler)
                    window.clearTimeout(context.internal.cursorChangeHandler);

                if (context.settings.maxValues > 0 && context.internal.selectedLookUps.length >= context.settings.maxValues) 
                {
                    window.setTimeout(function() { context.internal.cursor.value = ''; }, 19);
                    return false;
                }

                context.internal.lastCursorValue = context.internal.cursor.value;

                if (context.internal.lastCursorValue != '') 
                {
                    if (context.settings.onGetLookUps)
                        context.settings.onGetLookUps($(context.internal.state), context.internal.lastCursorValue);
                }
                else
                    _hideLookUps(context);
            }

            return true;
        },
        _cursorFocus = function(context, event)
        {
            context.internal.emptyHtmlContainer.style.display = 'none';
            context.internal.cursorHasFocus = true;
            context.internal.cursorChangeHandler = window.setTimeout(function() { _checkForChange(context); }, 149);
        },
        _cursorBlur = function(context, event)
        {
            if (context.internal.cursorChangeHandler)
                clearTimeout(context.internal.cursorChangeHandler);
        
            context.internal.cursorHasFocus = false;
            window.setTimeout(function() { _cursorBlurComplete(context) }, 99);
        },
        _cursorBlurComplete = function(context)
        {
            context.internal.cursor.value = '';
            context.internal.lastCursorValue = '';

            if (!context.internal.mouseIsOverPopup)
                _hideLookUps(context);

            if (context.internal.selectedLookUps.length == 0 && !context.internal.cursorHasFocus)
                context.internal.emptyHtmlContainer.style.display = 'block';
        },
        _cursorKeyDown = function(context, event)
        {
            if (!event)
                event = window.event;

            // up = 38
            // down = 40
            // backspace = 8
            // enter = 13
            // tab = 9

            if (event.keyCode == 8) {
                // delete previous look-up      

                if (context.internal.selectedLookUps.length > 0 && $.telligent.glow.utility.getCurrentCursorIndex(context.internal.cursor) == 0) 
                {
                    api.removeByIndex.apply($(context.internal.state), [context.internal.selectedLookUps.length - 1]);

                    $(context.internal.state).trigger(_changeEventName)                        
                }

                return true;
            }
            else if (event.keyCode == 38 || event.keyCode == 40 || event.keyCode == 13) 
            {
                if (event.keyCode == 38) 
                {
                    // move up in look-up list
                    if (context.internal.popupPanel.glowPopUpPanel('isShown') && context.internal.lookUpsIndex > 0)
                        _lookUpMouseOver(context, context.internal.lookUpsIndex - 1);
                }
                else if (event.keyCode == 40) 
                {
                    // move down in look-up list
                    if (context.internal.popupPanel.glowPopUpPanel('isShown') && context.internal.lookUpsIndex < context.internal.lookUps.length - 1) 
                        _lookUpMouseOver(context, context.internal.lookUpsIndex + 1);
                }
                else if (event.keyCode == 13) 
                {
                    // select current look-up list entry or clear text input
                    if (context.internal.popupPanel.glowPopUpPanel('isShown') && context.internal.lookUpsIndex >= 0)
                        _selectLookUp(context, context.internal.lookUpsIndex);
                    else 
                    {
                        context.internal.cursor.value = '';
                        context.internal.lastCursorValue = '';
                    }
                }

                event.cancelBubble = true;

                if (event.preventDefault)
                    event.preventDefault();

                if (event.stopProgagation)
                    event.stopPropagation();

                return false;
            }
            else if (event.keyCode == 9) 
            {
                // select current look-up list entry or clear text input
                if (context.internal.popupPanel.glowPopUpPanel('isShown') && context.internal.lookUpsIndex >= 0)
                    _selectLookUp(context, context.internal.lookUpsIndex);
                else 
                {
                    context.internal.cursor.value = '';
                    context.internal.lastCursorValue = '';
                }
            }
            else if (context.settings.maxValues > 0 && context.internal.selectedLookUps.length >= context.settings.maxValues) 
            {
                event.cancelBubble = true;

                if (event.preventDefault)
                    event.preventDefault();

                if (event.stopProgagation)
                    event.stopPropagation();

                return false;
            }

            return true;
        },
        _containerClick = function(context, event)
        {
            context.internal.emptyHtmlContainer.style.display = 'none';
            try { context.internal.cursor.focus(); } catch (err) { }
            return false;
        },
        _save = function(context)
        {
            var values = new Array();
            for (var i = 0; i < context.internal.selectedLookUps.length; i++)
                values[values.length] = context.internal.selectedLookUps[i].Value;

            context.internal.state.value = values.join(context.settings.delimiter);
        },
        _popupMouseOver = function(context) 
        {
            context.internal.mouseIsOverPopup = true;
        },
        _popupMouseOut = function(context)
        {
            context.internal.mouseIsOverPopup = false;
        };
    
    
    $.fn.glowLookUpTextBox = function(method) 
    {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowLookUpTextBox');
        }
    };  
    $.extend($.fn.glowLookUptextBox, {
        defaults: {
            delimiter:',',
            allowDuplicates:false,
            maxValues:0,
            onGetLookUps:null,
            emptyHtml:'',
            selectedLookUpsHtml:new Array(),
            deleteImageUrl:''           
        }       
    });
    // events 
    //   glowLookUpTextBoxChange
})(jQuery);

 
 
/// @name glowModal
/// @category jQuery Plugin
/// @description Renders a modal popup
///
/// ### jQuery.fn.glowModal
/// 
/// Renders a modal popup and provides an API for communicating data back from the modal
/// 
/// ### Usage
/// 
/// 	$.glowModal(options)
/// 
/// ### Options
/// 
///  * `width`: initial width of modal upon loading
///    * default: *300*
///  * `height`: initial width of modal upon loading
///    * default: *300*
///  * `onClose`: callback executed upon close of the modal.  If the modal passes a return value, the value is passed as the argument to `onClose`
///    * default: *null*
///  * `x`: initial x coordinate at which to open the modal
///    * default: *null*
///  * `y`: initial y coordinate at which to open the modal
///    * default: *null*
///  * `ignoreCloseAndAnimation`: when true, ignores modal closes
///    * default: *null*
///  * `isManuallyresized`: sets whether the window is being manually resized
///    * default: *null*
///  * `loadingHtmlUrl`: required URL of modal content to load
///    * default: *''*
///  * `windowCssClasses`: List of classes which will convert and map to corresponding nested divs around a modal
///    * default: *Theme-specific defaults, or []*
///  * `windowTitleCssClasses`: List of classes which will convert and map to corresponding nested divs around a modal title
///    * default: *Theme-specific defaults, or []*
///  * `windowCloseCssClasses`: List of classes which will convert and map to corresponding nested divs around a modal close button
///    * default: *Theme-specific defaults, or []*
///  * `windowContentCssClasses`: List of classes which will convert and map to corresponding nested divs around modal content
///    * default: *Theme-specific defaults, or []*
///  * `windowMaskCssClasses`: List of classes which will convert and map to corresponding background masks for the modal
///    * default: *Theme-specific defaults, or []*
///  * `windowFooterCssClasses`: List of classes which will convert and map to corresponding nested divs around a modal's footer
///    * default: *Theme-specific defaults, or []*
///  * `windowResizeCssClasses`: List of classes which will convert and map to corresponding nested divs around a modal's resize button
///    * default: *Theme-specific defaults, or []*
///  * `zIndex`: z-index at which to render the modal
///    * default: *1000*
///  * `enableAnimation`: enables animation
///    * default: *false*
///  * `enableAutoResizing`: enables auto-resizing based on content
///    * default: *true*
///  * `openInParentModal`: enables nested opening of child modals
///    * default: *true*
/// 
/// ### Methods
/// 
/// #### close
/// 
/// Closes a modal, optionally passing a return value to the parent.  The following example demonstrates referencing the parent's instance of glowModal for closing:
/// 
/// 	window.parent.$.glowModal.opener(window).$.glowModal.close('return value');
/// 
/// #### moveTo
/// 
/// Moves a modal to a new x/y coordinate
/// 
/// 	$.glowModal.moveTo(x, y, openerWindow);
/// 
/// #### resize
/// 
/// Resizes a modal
/// 
/// 	$.glowModal.resize(width, height, preventAutomaticResizing, openerWindow);
/// 
/// #### refresh
/// 
/// Refreshes a modal window
/// 
/// 	$.glowModal.refresh(openerWindow);
/// 
/// #### opener
/// 
/// Returns the opening window for a given modal
/// 
/// 	var opener = $.glowModal.opener(modalWindow, openerWindow);
/// 
/// #### visible
/// 
/// Returns whether a given modal is visible
/// 
/// 	var isVisible = $.glowModal.visible(openerWindow)
/// 

(function($, global){
	// internal methods

	// public API
	var glowModal = function(url, options)
	{
		var settings = {};
		// one parameter of just options (no explicit url)
		if (url && $.isPlainObject(url)) {
			settings = $.extend({}, $.glowModal.defaults, url || {});
		} else {
			settings = $.extend({}, $.glowModal.defaults, options || {}, { url: url });
		}

		if (!settings.openerWindow) {
			settings.openerWindow = global;
		}

		var w = _getParentWindow(settings.openerWindow);
		if (w)
		{
			w.$.glowModal(settings);
			return;
		}

		var mw = null;

		for(var i = 0; i<_modals.length; i++) {
			if (!_modals[i].shown())
			{
				mw = _modals[i];
				break;
			}
		}

		if (!mw)
		{
			_modals[_modals.length] = new ModalWindow(
				'telligent_modal_' + _modals.length);
			mw = _modals[_modals.length - 1];
		}

		mw.open(settings);
	};
	$.extend(glowModal, {
		defaults: {
			width: 300,
			height: 300,
			onClose: null,
			x: null,
			y: null,
			ignoreCloseAndAnimation: null,
			isManuallyresized: null,
			loadingHtmlUrl: '',
			windowCssClasses: [],
			windowTitleCssClasses: [],
			windowCloseCssClasses: [],
			windowContentCssClasses: [],
			windowMaskCssClasses: [],
			windowFooterCssClasses: [],
			windowResizeCssClasses: [],
			zIndex: 1000,
			enableAnimation: false,
			enableAutoResizing: true,
			openInParentModal: true,
			url: null,
			html: null,
			title: null
		},
		close: function(returnValue, openerWindow)
		{
			if (!openerWindow) {
				openerWindow = global;
			}

			var w = _getParentWindow(openerWindow);
			if (w)
			{
				w.$.glowModal.close(returnValue, openerWindow);
				return;
			}

			var mw = _getModalWindow(openerWindow);
			if (mw) {
				mw.close(returnValue);
			}
		},
		moveTo: function(x, y, openerWindow)
		{
			if (!openerWindow) {
				openerWindow = global;
			}

			var w = _getParentWindow(openerWindow);
			if (w)
			{
				w.$.glowModal.moveTo(x, y, openerWindow);
				return;
			}

			var mw = _getModalWindow(openerWindow);
			if (mw) {
				mw.moveTo(x, y);
			}
		},
		resize: function(width, height, preventAutomaticResizing, openerWindow)
		{
			if (!openerWindow) {
				openerWindow = global;
			}

			var w = _getParentWindow(openerWindow);
			if (w)
			{
				w.$.glowModal.resize(width, height, preventAutomaticResizing, openerWindow);
				return;
			}

			var mw = _getModalWindow(openerWindow);
			if (mw) {
				mw.resize(width, height, preventAutomaticResizing);
			}
		},
		refresh: function(openerWindow)
		{
			if (!openerWindow) {
				openerWindow = global;
			}

			var w = _getParentWindow(openerWindow);
			if (w)
			{
				w.$.glowModal.refresh(openerWindow);
				return;
			}

			$.each(_modals, function(i, modal) {
				modal.refresh();
			});
		},
		opener: function(modalWindow, openerWindow)
		{
			if (!openerWindow) {
				openerWindow = global;
			}

			var w = _getParentWindow(openerWindow);
			if (w) {
				return w.$.glowModal.getWindowopener(modalWindow, openerWindow);
			}

			var modal = _first(_modals, function(modal) {
				return modal.shown() && modal._modalIframe.contentWindow == modalWindow;
			});
			return modal !== null ? modal._openerWindow : null;
		},
		visible: function(openerWindow)
		{
			if (!openerWindow) {
				openerWindow = global;
			}

			var w = _getParentWindow(openerWindow);
			if (w) {
				return w.$.glowModal.shown(openerWindow);
			}

			var mw = _getModalWindow(openerWindow);
			if (mw) {
				return mw.shown();
			} else {
				return false;
			}
		}
	});

	$.extend({ glowModal: glowModal });

	var _eventNameSpace = '.glowModal',
		_modals = [],
		_first = function(list, fn) {
			var results = $.grep(list, fn);
			if(results !== null && results.length > 0) {
				return results[0];
			} else {
				return null;
			}
		},
		_getParentWindow = function(openerWindow)
		{
			if (glowModal.defaults.openInParentModal)
			{
				try
				{
					if (global.parent && global.parent != global && parent.$ && parent.$.glowModal) {
						return global.parent;
					}
				}
				catch (e) { }
			}

			return null;
		},
		_getModalWindow = function(openerWindow)
		{
			return _first(_modals, function(modal) {
				return modal.shown() && modal._openerWindow == openerWindow;
			});
		};

	var ModalWindow = function(id)
	{
		this._id = id;
		this._isShown = false;
		this._initialized = false;
		this._modal = null;
		this._modalTitle = null;
		this._modalClose = null;
		this._modalAnimationMask = null;
		this._modalMask = null;
		this._modalIframe = null;
		this._modalResize = null;
		this._modalFooter = null;
		this._modalContent = null;
		this._animationHandle = null;
		this._isOpening = false;
		this._checkForScrollresizeHandle = null;
		this._lastModalInfo = null;
		this._lastWindowInfo = null;
		this._isDragging = false;
		this._moveModalInfo = null;
		this._resizeModalInfo = null;
		this._isResizing = false;
	};
	$.extend(ModalWindow.prototype, {
		_animate: function(targetValue, nextValue, step, acceleration)
		{
			if (this._animationHandle) {
				global.clearTimeout(this._animationHandle);
			}

			if (!this._isOpening) {
				return;
			}

			var currValue = parseInt(this._modal.style.top, 10);
			if ((step < 0 && currValue < targetValue) || (step > 0 && currValue > targetValue) || Math.abs(step) < 1)
			{
				// complete

				this._modal.style.top = targetValue + 'px';
				this._modalAnimationMask.style.overflow = 'visible';
				this._animationHandle = null;

				if (!this._isResizing && !this._isDragging && this._settings.url) {
					this._modalIframe.style.visibility = 'visible';
				}

				this._isOpening = false;

				this._lastWindowInfo = $.telligent.glow.utility.getWindowInfo();
				var self = this;
				this._checkForScrollresizeHandle = global.setTimeout(function(){
					self._checkForScrollresize();
				}, 249);
			}
			else
			{
				// continue animation
				this._modal.style.top = nextValue + 'px';

				nextValue = nextValue + step;
				if (step > 0 && nextValue > targetValue) {
					nextValue = targetValue;
				} else if (step < 0 && nextValue < targetValue) {
					nextValue = targetValue;
				}

				step = step * acceleration;

				//this._animationHandle = global.setTimeout(new Function('glowModal._animate(' + targetValue + ',' + nextValue + ',' + step + ',' + acceleration + ');'), 19);
				var self = this;
				this._animationHandle = global.setTimeout(function(){
					self._animate(targetValue, nextValue, step, acceleration);
				}, 19);
			}
		},

		_startDrag: function(event)
		{
			if (!this._initialized) {
				this._initialize();
			}

			if (!event) {
				event = global.event;
			}

			this._moveModalInfo = {};

			this._moveModalInfo.StartMouseX = event.pageX ? event.pageX : event.screenX;
			this._moveModalInfo.StartMouseY = event.pageY ? event.pageY : event.screenY;
			this._moveModalInfo.StartModalX = this._lastModalInfo.X;
			this._moveModalInfo.StartModalY = this._lastModalInfo.Y;
			this._moveModalInfo.Button = event.button;

			var self = this;
			$(document)
				.bind('mouseup' + _eventNameSpace, function(event) {
					self._endDrag(event);
					return false;
				})
				.bind('mousemove' + _eventNameSpace, function(event) {
					self._drag(event);
					return false;
				});

			this._isDragging = true;
			if(this._modalIframe)
				this._modalIframe.style.visibility = 'hidden';
		},

		_endDrag: function(event)
		{
			if (!this._initialized) {
				this._initialize();
			}

			$(document)
				.unbind('mouseup' + _eventNameSpace)
				.unbind('mousemove' + _eventNameSpace);

			if(this._settings.url)
				this._modalIframe.style.visibility = 'visible';
			this._isDragging = false;
			this._moveModalInfo = null;
		},

		_drag: function(event)
		{
			if (!this._initialized) {
				this._initialize();
			}

			if (!event) {
				event = global.event;
			}

			if (event.button != this._moveModalInfo.Button)
			{
				this._endDrag(event);
				return;
			}

			var eventX = typeof (event.pageX) != 'undefined' ? event.pageX : event.screenX;
			var eventY = typeof (event.pageY) != 'undefined' ? event.pageY : event.screenY;

			var xChange = eventX - this._moveModalInfo.StartMouseX;
			var yChange = eventY - this._moveModalInfo.StartMouseY;

			this.moveTo(this._moveModalInfo.StartModalX + xChange, this._moveModalInfo.StartModalY + yChange);
		},

		_startresize: function(event)
		{
			if (!this._initialized) {
				this._initialize();
			}

			if (!event) {
				event = global.event;
			}

			this._resizeModalInfo = {};

			this._resizeModalInfo.StartMouseX = event.pageX ? event.pageX : event.screenX;
			this._resizeModalInfo.StartMouseY = event.pageY ? event.pageY : event.screenY;
			this._resizeModalInfo.StartModalWidth = this._lastModalInfo.Width;
			this._resizeModalInfo.StartModalHeight = this._lastModalInfo.Height;
			this._resizeModalInfo.Button = event.button;

			//document.onmouseup = new Function('event', 'glowModal._endresize(event); return false;');
			//document.onmousemove = new Function('event', 'glowModal._resize(event); return false;');
			var self = this;
			$(document)
				.bind('mouseup' + _eventNameSpace, function(event) {
					self._endresize(event);
					return false;
				})
				.bind('mousemove' + _eventNameSpace, function(event) {
					self._resize(event);
					return false;
				})

			if(this._modalIframe)
				this._modalIframe.style.visibility = 'hidden';
			this._isResizing = true;
		},

		_endresize: function(event)
		{
			if (!this._initialized) {
				this._initialize();
			}

			this._isResizing = false;
			this._resizeModalInfo = null;
			$(document)
				.unbind('mouseup' + _eventNameSpace)
				.unbind('mousemove' + _eventNameSpace);
			if(this._settings.url)
				this._modalIframe.style.visibility = 'visible';
		},

		_resize: function(event)
		{
			if (!this._initialized) {
				this._initialize();
			}

			if (!event) {
				event = global.event;
			}

			if (event.button != this._resizeModalInfo.Button)
			{
				this._endresize(event);
				return;
			}

			var eventX = typeof (event.pageX) != 'undefined' ? event.pageX : event.screenX;
			var eventY = typeof (event.pageY) != 'undefined' ? event.pageY : event.screenY;

			var xChange = eventX - this._resizeModalInfo.StartMouseX;
			var yChange = eventY - this._resizeModalInfo.StartMouseY;

			this.resize(this._resizeModalInfo.StartModalWidth + xChange, this._resizeModalInfo.StartModalHeight + yChange, true);
		},

		_checkForScrollresize: function()
		{
			if (this._checkForScrollresizeHandle) {
				global.clearTimeout(this._checkForScrollresizeHandle);
			}

			if (this._isShown && !this._isOpening && this._lastWindowInfo)
			{
				if(this._settings.title) {
					$(this._modalTitle.childNodes[1]).html(this._settings.title);
				} else if(this._settings.url) {
					try
					{
						if (document.all) {
							this._modalTitle.childNodes[1].innerText = this._modalIframe.contentWindow.document.title;
						} else {
							this._modalTitle.childNodes[1].textContent = this._modalIframe.contentWindow.document.title;
						}
					}
					catch (err)
					{
					}
				}

				var globalInfo = $.telligent.glow.utility.getWindowInfo();
				if (globalInfo.ScrollX != this._lastWindowInfo.ScrollX || globalInfo.ScrollY != this._lastWindowInfo.ScrollY || globalInfo.Width != this._lastWindowInfo.Width || globalInfo.Height != this._lastWindowInfo.Height)
				{
					this.resize(this._lastModalInfo.Width, this._lastModalInfo.Height, false);
					return;
				}

				if (this._settings.enableAutoResizing && !this._isDragging && !this._isResizing)
				{
					try
					{
						if(this._settings.url) {
							var iFrameDocument = this._modalIframe.contentWindow.document;
							if (!iFrameDocument.readyState || iFrameDocument.readyState == 'complete')
							{
								var iframeInfo = $.telligent.glow.utility.getWindowInfo(this._modalIframe.contentWindow);

								var currentHeight = iframeInfo.ContentHeight;
								var currentWidth = iframeInfo.ContentWidth;

								var modalContentOffset = $.telligent.glow.utility.getStyleOffset(this._modalContent);
								var heightOffset = (this._modal.offsetHeight - this._modalContent.offsetHeight) - modalContentOffset.Height;
								var widthOffset = (this._modal.offsetWidth - this._modalContent.offsetWidth) - modalContentOffset.Width;

								if (currentHeight > globalInfo.Height - heightOffset) {
									currentHeight = globalInfo.Height - heightOffset;
								}

								if (currentWidth > globalInfo.Width - widthOffset) {
									currentWidth = globalInfo.Width - widthOffset;
								}

								var changeHeight = (currentHeight != this._lastModalInfo.Height
									&& (currentHeight > this._lastModalInfo.Height || (this._lastModalInfo.Height - currentHeight > 64 && currentHeight > 32))
									&& (!this._lastModalInfo.IsManuallyresized || currentHeight > this._lastModalInfo.Height)
									);
								var changeWidth = (currentWidth != this._lastModalInfo.Width
									&& (currentWidth > this._lastModalInfo.Width || (this._lastModalInfo.Width - currentWidth > 64 && currentWidth > 32))
									&& (!this._lastModalInfo.IsManuallyresized || currentWidth > this._lastModalInfo.Width)
									);

								if (changeHeight || changeWidth)
								{
									this.resize(changeWidth ? currentWidth : this._lastModalInfo.Width, changeHeight ? currentHeight : this._lastModalInfo.Height, false);
									return;
								}
							}
						}
					}
					catch (e) {	 }
				}

				var self = this;
				this._checkForScrollresizeHandle = global.setTimeout(function(){
					self._checkForScrollresize();
				}, 249);
			}
		},

		_initialize: function()
		{
			this._modalMask = document.createElement('div');
			this._modalMask.style.height = '0';
			this._modalMask.style.position = 'absolute';
			this._modalMask.style.visibility = 'hidden';
			this._modalMask.style.overflow = 'hidden';

			var mm = this._modalMask;
			if (this._settings.windowMaskCssClasses.length > 0)
			{
				mm.className = this._settings.windowMaskCssClasses[0];
				for (var i = 1; i < this._settings.windowMaskCssClasses.length; i++)
				{
					mm.appendChild(document.createElement('div'));
					mm = mm.childNodes[0];
					mm.className = this._settings.windowMaskCssClasses[i];
					mm.style.width = 'auto';
					mm.style.height = 'auto';
				}
			}

			document.body.appendChild(this._modalMask);

			this._modalAnimationMask = document.createElement('div');
			this._modalAnimationMask.style.position = 'absolute';
			this._modalAnimationMask.style.visibility = 'hidden';
			this._modalAnimationMask.style.overflow = 'hidden';
			this._modalAnimationMask.style.height = '0';

			this._modal = document.createElement('div');
			this._modal.style.width = 'auto';
			this._modal.style.height = 'auto';
			this._modal.style.position = 'absolute';
			this._modal.style.visibility = 'hidden';

			var m = this._modal;
			if (this._settings.windowCssClasses.length > 0)
			{
				m.className = this._settings.windowCssClasses[0];
				for (var i = 1; i < this._settings.windowCssClasses.length; i++)
				{
					m.appendChild(document.createElement('div'));
					m = m.childNodes[0];
					m.className = this._settings.windowCssClasses[i];
					m.style.width = 'auto';
					m.style.height = 'auto';
				}
			}

			this._modalTitle = document.createElement('div');
			m.appendChild(this._modalTitle);
			if (this._settings.windowTitleCssClasses.length > 0)
			{
				this._modalTitle.className = this._settings.windowTitleCssClasses[0];
				for (var i = 1; i < this._settings.windowTitleCssClasses.length; i++)
				{
					this._modalTitle.appendChild(document.createElement('div'));
					this._modalTitle = this._modalTitle.childNodes[0];
					this._modalTitle.className = this._settings.windowTitleCssClasses[i];
				}
			}
			//this._modalTitle.onmousedown = new Function('event', 'glowModal._startDrag(event); return false;');
			var self = this;
			$(this._modalTitle).bind('mousedown' + _eventNameSpace, function(event) {
				self._startDrag(event);
				return false;
			});

			this._modalClose = document.createElement('div');
			this._modalTitle.appendChild(this._modalClose);

			var mc = this._modalClose;
			if (this._settings.windowCloseCssClasses.length > 0)
			{
				mc.className = this._settings.windowCloseCssClasses[0];
				for (var i = 1; i < this._settings.windowCloseCssClasses.length; i++)
				{
					mc.appendChild(document.createElement('div'));
					mc = mc.childNodes[0];
					mc.className = this._settings.windowCloseCssClasses[i];
				}
			}

			//this._modalClose.onclick = new Function('glowModal.close(); return false;');
			var self = this;
			$(this._modalClose).bind('click' + _eventNameSpace, function(){
				self.close();
				return false;
			})

			this._modalTitle.appendChild(document.createElement('span'));

			var e = document.createElement('div');
			e.style.clear = 'both';
			this._modalTitle.appendChild(e);

			this._modalContent = document.createElement('div');
			m.appendChild(this._modalContent);
			if (this._settings.windowContentCssClasses.length > 0)
			{
				this._modalContent.className = this._settings.windowContentCssClasses[0];
				for (var i = 1; i < this._settings.windowContentCssClasses.length; i++)
				{
					this._modalContent.appendChild(document.createElement('div'));
					this._modalContent = this._modalContent.childNodes[0];
					this._modalContent.className = this._settings.windowContentCssClasses[i];
				}
			}

			if(this._settings.url) {
				this._modalIframe = document.createElement('iframe');
				this._modalIframe.src = this._settings.loadingHtmlUrl;
				this._modalIframe.width = '100%';
				this._modalIframe.border = '0';
				this._modalIframe.frameBorder = '0';
				this._modalIframe.style.borderLeftWidth = '0px';
				this._modalIframe.style.borderRightWidth = '0px';
				this._modalIframe.style.borderTopWidth = '0px';
				this._modalIframe.style.borderBottomWidth = '0px';
				this._modalIframe.id = this._id + '_iframe';
				this._modalContent.appendChild(this._modalIframe);
			} else if(this._settings.html) {
				$(this._modalContent).html(this._settings.html);
			}

			this._modalFooter = document.createElement('div');
			m.appendChild(this._modalFooter);
			var mf = this._modalFooter;
			if (this._settings.windowFooterCssClasses.length > 0)
			{
				mf.className = this._settings.windowFooterCssClasses[0];
				for (var i = 1; i < this._settings.windowFooterCssClasses.length; i++)
				{
					mf.appendChild(document.createElement('div'));
					mf = mf.childNodes[0];
					mf.className = this._settings.windowFooterCssClasses[i];
				}
			}
			this._modalResize = document.createElement('div');
			mf.appendChild(this._modalResize);

			var e = document.createElement('div');
			e.style.clear = 'both';
			mf.appendChild(e);

			var mr = this._modalResize;
			if (this._settings.windowResizeCssClasses.length > 0)
			{
				mr.className = this._settings.windowResizeCssClasses[0];
				for (var i = 1; i < this._settings.windowResizeCssClasses.length; i++)
				{
					mr.appendChild(document.createElement('div'));
					mr = mr.childNodes[0];
					mr.className = this._settings.windowResizeCssClasses[i];
				}
			}

			//this._modalResize.onmousedown = new Function('event', 'glowModal._startresize(event); return false;');
			var self = this;
			$(this._modalResize).bind('mousedown' + _eventNameSpace, function(event) {
				self._startresize(event);
				return false;
			});

			this._modalAnimationMask.appendChild(this._modal);

			document.body.appendChild(this._modalAnimationMask);

			this._initialized = true;
		},

		shown: function()
		{
			return this._isShown;
		},

		resize: function(width, height, preventAutomaticResizing)
		{
			if (this._isShown && !this._isOpening && this._lastModalInfo) {
				this.open({
					url: this._settings.url,
					html: this._settings.html,
					title: this._settings.title,
					width: width,
					height: height,
					onClose: this._lastModalInfo.onClose,
					x: this._lastModalInfo.X,
					y: this._lastModalInfo.Y,
					ignorecloseAndAnimation: true,
					isManuallyresized: preventAutomaticResizing ? true : this._lastModalInfo.IsManuallyresized
				});
			}
		},

		moveTo: function(x, y)
		{
			if (this._isShown && !this._isOpening && this._lastModalInfo) {
				this.open({
					url: this._settings.url,
					html: this._settings.html,
					title: this._settings.title,
					width: this._lastModalInfo.Width,
					height: this._lastModalInfo.Height,
					onClose: this._lastModalInfo.onClose,
					x: x,
					y: y,
					ignorecloseAndAnimation: true,
					isManuallyresized: this._lastModalInfo.IsManuallyresized
				});

			}
		},

		open: function(options)
		{
			this._settings = $.extend({}, $.glowModal.defaults, options);

			var onClose = this._settings.onClose,
				x = this._settings.x,
				y = this._settings.y,
				ignorecloseAndAnimation = this._settings.ignorecloseAndAnimation,
				isManuallyresized = this._settings.isManuallyresized,
				openerWindow = this._settings.openerWindow;

			if (openerWindow) {
				this._openerWindow = openerWindow;
			}

			if (!ignorecloseAndAnimation && this._isShown) {
				this.close();
			}

			if (!this._initialized) {
				this._initialize();
			}

			if(this._settings.title) {
				$(this._modalTitle.childNodes[1]).html(this._settings.title);
			} else if(this._settings.url) {
				try
				{
					if (document.all) {
						this._modalTitle.childNodes[1].innerText = this._modalIframe.contentWindow.document.title;
					} else {
						this._modalTitle.childNodes[1].textContent = this._modalIframe.contentWindow.document.title;
					}
				}
				catch (err)
				{
				}

				if (!ignorecloseAndAnimation && this._settings.url) {
					this._modalIframe.src = this._settings.url;
				}

				try
				{
					if(this._settings.url) {
						this._modalIframe.contentWindow.opener = global;
					}
				}
				catch (err)
				{
				}
			}

			// retrieve the global info
			this._lastWindowInfo = $.telligent.glow.utility.getWindowInfo();
			if (!ignorecloseAndAnimation)
			{
				this._modalAnimationMask.style.position = 'absolute';
				this._modalAnimationMask.style.zIndex = this._settings.zIndex;
				this._modalAnimationMask.style.visibility = 'hidden';
				this._modalAnimationMask.style.height = 'auto';
				this._modalAnimationMask.style.overflow = 'hidden';

				this._modal.style.position = 'absolute';
				this._modal.style.visibility = 'hidden';
				this._modal.style.left = '0px';
				this._modal.style.top = '0px';

				this._modalMask.style.position = 'absolute';
				this._modalMask.style.visibility = 'visible';
				this._modalMask.style.zIndex = this._settings.zIndex;
				this._modalMask.style.height = 'auto';
			}

			var modalContentOffset = $.telligent.glow.utility.getStyleOffset(this._modalContent);

			// width/height of panel
			var widthOffset = (this._modal.offsetWidth - this._modalContent.offsetWidth) - modalContentOffset.Width;
			var heightOffset = (this._modal.offsetHeight - this._modalContent.offsetHeight) - modalContentOffset.Height;

			if (this._settings.width + widthOffset > this._lastWindowInfo.Width) {
				this._settings.width = this._lastWindowInfo.Width - widthOffset;
			}

			this._modalAnimationMask.style.width = this._settings.width + 'px';
			this._modalContent.style.width = this._settings.width + 'px';

			if (this._settings.height + heightOffset > this._lastWindowInfo.Height) {
				this._settings.height = this._lastWindowInfo.Height - heightOffset;
			}

			if (this._settings.width < this._modalResize.offsetWidth * 2) {
				this._settings.width = this._modalResize.offsetWidth * 2;
			}

			if (this._settings.width < this._modalClose.offsetWidth * 2) {
				this._settings.width = this._modalClose.offsetWidth * 2;
			}

			if (this._settings.height < this._modalTitle.offsetHeight + this._modalFooter.offsetHeight) {
				this._settings.height = this._modalTitle.offsetHeight + this._modalFooter.offsetHeight - heightOffset;
			}

			if(this._settings.url) {
				this._modalIframe.style.height = this._settings.height + 'px';
				this._modalIframe.style.width = this._settings.width + 'px';
			}
			this._modalContent.style.height = this._settings.height + 'px';
			this._modalContent.style.width = this._settings.width + 'px';
			this._modalAnimationMask.style.width = this._modal.offsetWidth + 'px';
			this._modalAnimationMask.style.height = this._modal.offsetHeight + 'px';

			this._modalMask.style.left = '0px';
			this._modalMask.style.top = '0px';
			this._modalMask.style.width = this._lastWindowInfo.ContentWidth + 'px';
			this._modalMask.style.height = this._lastWindowInfo.ContentHeight + 'px';

			this._lastWindowInfo = $.telligent.glow.utility.getWindowInfo();

			var panelWidth = this._modal.offsetWidth;
			var panelHeight = this._modal.offsetHeight;
			var animatePropertyName, animateTargetValue, animateNextValue;

			if (typeof (x) == 'undefined' || isNaN(parseInt(x, 10))) {
				x = ((this._lastWindowInfo.Width - panelWidth) / 2) + this._lastWindowInfo.ScrollX;
			}

			if (x + panelWidth > this._lastWindowInfo.Width + this._lastWindowInfo.ScrollX) {
				x = this._lastWindowInfo.Width + this._lastWindowInfo.ScrollX - panelWidth;
			}

			if (x < this._lastWindowInfo.ScrollX) {
				x = this._lastWindowInfo.ScrollX;
			}

			if (typeof (y) == 'undefined' || isNaN(parseInt(y, 10))) {
				y = ((this._lastWindowInfo.Height - panelHeight) / 2) + this._lastWindowInfo.ScrollY;
			}

			if (y + panelHeight > this._lastWindowInfo.Height + this._lastWindowInfo.ScrollY) {
				y = this._lastWindowInfo.Height + this._lastWindowInfo.ScrollY - panelHeight;
			}

			if (y < this._lastWindowInfo.ScrollY) {
				y = this._lastWindowInfo.ScrollY;
			}

			this._modalAnimationMask.style.left = x + 'px';
			this._modalAnimationMask.style.top = y + 'px';

			animateTargetValue = 0;
			animateNextValue = -panelHeight;

			this._modalAnimationMask.style.overflow = 'hidden';
			if (!ignorecloseAndAnimation)
			{
				this._modal.style.visibility = 'visible';
				this._modalAnimationMask.style.visibility = 'visible';

				// detect and hide select boxes
				$.telligent.glow.utility.hideSelectBoxes(this._modalAnimationMask, true);
			}

			this._isOpening = true;
			if (!this.EnableAnimation || ignorecloseAndAnimation)
			{
				this._animate(0, 0, 0, 0);
			}
			else
			{
				if(this._settings.url) {
					this._modalIframe.style.visibility = 'hidden';
				}
				this._animate(0, -panelHeight, panelHeight / 3, .67);
			}

			this._lastModalInfo = {
				Url: (this._settings.url ? this._modalIframe.src : null),
				onClose: onClose,
				X: x,
				Y: y,
				Width: parseInt(this._settings.width, 10),
				Height: parseInt(this._settings.height, 10),
				IsManuallyresized: isManuallyresized
			};
			this._isShown = true;
		},

		close: function(returnValue)
		{
			if (this._isShown)
			{
				if (!this._initialized) {
					this._initialize();
				}

				this._modal.style.position = 'absolute';
				this._modal.style.visibility = 'hidden';
				this._modal.style.height = '0';
				this._modalAnimationMask.style.position = 'absolute';
				this._modalAnimationMask.style.visibility = 'hidden';
				this._modalAnimationMask.style.height = '0';
				this._modalMask.style.position = 'absolute';
				this._modalMask.style.visibility = 'hidden';
				if(this._settings.url) {
					this._modalIframe.src = this._settings.loadingHtmlUrl;
				}

				var onClose = this._lastModalInfo.onClose;

				this._isShown = false;
				this._lastModalInfo = null;
				this._globalInfo = null;

				$.telligent.glow.utility.showSelectBoxes(this._modalAnimationMask);

				if (onClose) {
					global.setTimeout(function() { onClose(returnValue); }, 9);
				}

				// this.Dispose();
				this.dispose();
			}
		},

		refresh: function()
		{
			if (this._animationHandle) {
				global.clearTimeout(this._animationHandle);
			}

			this.dispose();

			if (this._isShown && this._lastModalInfo) {
				this.resize(this._lastModalInfo.Width, this._lastModalInfo.Height, false);
			}
		},

		dispose: function()
		{
			if (this._initialized)
			{
				if (this._animationHandle) {
					global.clearTimeout(this._animationHandle);
				}

				this._isShown = false;
				this._isOpening = false;

				if (document && document.body)
				{
					document.body.removeChild(this._modalAnimationMask);
					document.body.removeChild(this._modalMask);
					$(this._modalClose).unbind('click' + _eventNameSpace);
					$(this._modalTitle).unbind('mousedown' + _eventNameSpace);
					$(this._modalResize).unbind('mousedown' + _eventNameSpace);
					this._modal = null;
					this._modalTitle = null;
					this._modalClose = null;
					this._modalAnimationMask = null;
					this._modalMask = null;
					this._modalIframe = null;
					this._modalResize = null;
					this._modalFooter = null;
					this._modalContent = null;
				}

				this._initialized = false;
			}
		}
	});

})(jQuery, window);
 
 
/// @name glowMultiUpload
/// @category jQuery Plugin
/// @description Renders a multi-file upload input
/// 
/// ### jQuery.fn.glowMultiUpload
/// 
/// Renders a multi-file upload input supporting chunked uploads with visual progress bars
/// 
/// ### Usage
/// 
///     $('SELECTOR').glowMultiUpload(options)
/// 
/// where 'SELECTOR' is a div element containing a file input
/// 
/// ### Options
/// 
///  * `fileFilter`: Array of filters to apply when selecting files.  Each filter is an object with format `{ title : "Zip files", extensions : "zip" }`
///    * default: *null*
///  * `maxFileSize`: Maximum file size of a selectable file
///    * default: *'51200kb'*
///  * `width`: Width of control
///    * default: *'350px'*
///  * `height`: Height of control
///    * default: *'200px'*   
///  * `uploadUrl`: URL to upload against.  With Studio Widgets, this is the result of a call to create a context ID and build an upload URL for it:  `$core_v2_uploadedFile.GetUploadUrl($core_v2_uploadedFile.CreateContextId())`
///    * default: *'/'*
///  * `autoUpload`: Automatically uploads files when true
///    * default: *true*   
///  * `maxFileCount`: Maximum selectable files to upload
///    * default: *5*   
///  * `fileList`: Default 
///    * default: *[]*   
///  * `swfUrl`: Path to SWF upload component
///    * default: *$.telligent.evolution.site.getBaseUrl() + 'utility/images/glow/plupload.flash.swf'*
///  * `xapUrl`: Path to Silverlight upload component
///    * default: *$.telligent.evolution.site.getBaseUrl() + 'utility/images/glow/plupload.silverlight.swf'*
///  * `uploadedFormat`: HTML template to use for an uploaded item
///    * default: *'&lt;div style="overflow: hidden; text-overflow: ellipsis; color: #000; padding: .25em;"&gt;&lt;div style="float: right; color: #3a3; margin-left: .25em;"&gt;Uploaded&lt;/div&gt;{0}&lt;/div&gt;'*
///  * `uploadingFormat`: HTML template to use for an uploading item
///    * default: *'&lt;div style="overflow: hidden; text-overflow: ellipsis; color: #000; padding: .25em;"&gt;&lt;div style="float: right; margin-top: .25em; margin-left: .25em; border: solid 1px #999; background-color: #eee; width: 50px; height: .5em;"&gt;&lt;div style="background-color: #7e7; width: {1}%; height: .5em;"&gt;&lt;/div&gt;&lt;/div&gt;{0}&lt;/div&gt;'*
///  * `toUploadFormat`: HTML template to use for an item to be uploaded
///    * default: *'&lt;div style="overflow: hidden; text-overflow: ellipsis; color: #666; font-style: italic; padding: .25em;"&gt;{0}&lt;/div&gt;'*
///  * `errorFormat`: HTML template to use for an error
///    * default: *'&lt;div style="overflow: hidden; text-overflow: ellipsis; color: #000; padding: .25em;"&gt;&lt;div style="float: right; color: #f00; margin-left: .25em;"&gt;Error&lt;/div&gt;{0}&lt;/div&gt;'*
///  * `cancelledFormat`: HTML template to use for a cancelled item
///    * default: *'&lt;div style=\"overflow: hidden; text-overflow: ellipsis; color: #666; font-style: italic; border-top: solid 1px #eee; padding-top: .25em; margin-top: .25em;\"&gt;&lt;div style=\"float: right; color: #666; font-style: italic; margin-left: .25em;\"&gt;Cancelled&lt;/div&gt;{0}&lt;/div&gt;'*   
///  * `runtimes`: Known upload runtimes to attempt to use
///    * default: *'gears,silverlight,flash,html5'*
///  * `firstItemRemove`: HTML template to strip from the first item when removed (rarely necessary to modify)
///    * default: *`border-top: solid 1px #eee; padding-top: .25em; margin-top: .25em;"`*
/// 
/// ### Methods
/// 
/// #### val
/// 
/// Returns the list of uploaded files
/// 
///     var files = $('SELECTOR').glowMultiUpload('val');
/// 

(function($,global){

    // public api
    var api = {
        val: function() {
            var files = $(this).data(_dataKey).files;
            if(files !== null) {
                return $.map(files, function(file){
                    return { name: file.name };
                });
            } else {
                return [];
            }
        }
    };

    // private methods
    var _dataKey = '_glowMultiUpload',
        _eventNames = {
            begun: 'glowMultiUploadBegun',
            complete: 'glowMultiUploadComplete',
            error: 'glowMultiUploadError'
        },
        _init = function(options) {
            return this.each(function()
            {
                var elm = $(this);

                var context = $.extend({}, $.fn.glowMultiUpload.defaults, options || {}, {
                    pluploader: null,
                    files: [],
                    fileOffset: 0,
                    uploadedFiles: {},
                    erroredFiles: {},
                    cancelledFiles: {},
                    isUploading: false,
                    currentUploadPercent: 0,
                    fileContainer: null,
                    currentUploadId: 0,
                    listContainer: null,
                    uploadButton: null
                });

                $.extend(context, {
                    container: elm,
                    initialized: false
                });

                $(this).data(_dataKey, context);

				_determineUploadSpeed(context);
            });
        },
       	_determineUploadSpeed = function(context) {
        	var testStrings = ["1234567890", "0987654321", "0192837465", "5647382910"];
        	var testData = [];
        	for (var i = 0; i < 2500; i++) { testData[testData.length] = testStrings[Math.floor((Math.random()*testStrings.length))]; }
        	var startMs = (new Date()).getTime();

        	jQuery.ajax({
        		type: 'POST',
        		url: context.uploadUrl,
        		data: {
        			test: testData.join('')
        		},
        		success: function() {
        			var endMs = (new Date()).getTime();
        			_initialize(context, context.fileList, ((25000 * 8) / ((endMs - startMs) / 1000)) * 45);
        		},
        		error: function() {
        			// test failed: default to max upload of 1mb
        			_initialize(context, context.fileList, 1000000);
        		}
        	});
        },
        _initialize = function(context, fileList, maxUploadSpeed) {
            if (/Mozilla[^\(]*\(.*? rv:([0-9]+)\.([0-9]+)\.([0-9]+)/.test(navigator.userAgent))
            {
                if (parseInt(RegExp.$1, 10) < 1 || parseInt(RegExp.$2, 10) < 9 || parseInt(RegExp.$3, 10) < 2)
                {
                    // not using the version of Gecko in FireFox 3.6+, use HTML5 only.
                    context.runtimes = context.runtimes.replace(/,?flash/i, '');
                }
            }

            for (var i = 0; i < fileList.length; i++) {
                var file = {
                    name: fileList[i],
                    id: 'preuploaded_' + i,
                    status: plupload.DONE,
                    percent: 100,
                    size: 'unknown',
                    loaded: 'unknown'
                };
                context.files[context.files.length] = file;
                context.uploadedFiles[file.id] = true;
            }

            context.fileOffset = context.files.length;

            var chunkSize = '15mb';
            if (maxUploadSpeed < 15000000)
            {
            	chunkSize = Math.floor(maxUploadSpeed);
            	if (chunkSize < 1000000)
            	{
            		chunkSize = 1000000
            	}

            	chunkSize = Math.floor(maxUploadSpeed / 1000000) + 'mb'
            }

            var pluploadOptions = {
                runtimes: context.runtimes,
                max_file_size: context.maxFileSize,
                container: context.container.attr('id'),
                url: context.uploadUrl,
                flash_swf_url: context.swfUrl,
                silverlight_xap_url: context.silverlightUrl,
                chunk_size: chunkSize
            };
            if (context.fileFilter !== null) {
                pluploadOptions.filters = context.fileFilter;
            }
            context.pluploader = new plupload.Uploader(pluploadOptions);

            context.pluploader.bind('Init', function(up){
                _uploaderInit(context);
            });
            context.pluploader.bind('StateChanged', function(up){
                _stateChanged(context);
            });
            context.pluploader.bind('FilesAdded', function(up, files){
                _filesAdded(context, files);
            });
            context.pluploader.bind('QueueChanged', function(up){
                _filesQueued(context);
            });
            context.pluploader.bind('UploadProgress', function(up, file) {
                _fileProgress(context, file);
            });
            context.pluploader.bind('FileUploaded', function(up, file, response) {
                _fileComplete(context, file);
            });
            context.pluploader.bind('Error', function(up, err) {
                _error(context, err);
            });
            context.pluploader.bind('UploadComplete', function(up, files) {
                _uploadComplete(context, files);
            });

            global.setTimeout(function(){
                context.pluploader.init();
            }, 9);

            context.container.find('input[type="file"]').hide();
        },



        _filesAdded = function(context, files)
        {
            context.addedFiles = files;
        },

        _filesQueued = function(context)
        {
            global.clearTimeout(context.autoUploadHandle);

            if (!context.addedFiles) {
                return;
            }

            var files = [];
            for (var i = 0; i < context.pluploader.files.length; i++)
            {
                for (var j = 0; j < context.addedFiles.length; j++)
                {
                    if (context.pluploader.files[i].id == context.addedFiles[j].id)
                    {
                        files[files.length] = context.addedFiles[j];
                        break;
                    }
                }
            }
            context.addedFiles = null;

            for (var j = 0; j < files.length; j++)
            {
                for (var i = 0; i < context.files.length; i++)
                {
                    if (context.files[i].name == files[j].name)
                    {
                    	_cancelFile(context, context.files[i].id);
                        if (context.files[i].status == plupload.QUEUED)
                        {
                            var newFiles = [];
                            for (var k = 0; k < context.files.length; k++)
                            {
                                if (context.files[k].id != context.files[i].id) {
                                    newFiles[newFiles.length] = context.files[k];
                                }
                            }
                            context.files = newFiles;
                        }
                    }
                }
            }

            if (context.maxFileCount > 0 && context.files.length + files.length > context.maxFileCount)
            {
                for (var j = 0; j < files.length; j++) {
                	_cancelFile(context, files[j].id);
                }

                context.container.trigger(_eventNames.error);

                return;
            }

            for (var j = 0; j < files.length; j++)
            {
                context.files[context.files.length] = files[j];
            }

            _updateFileList(context);
            if (context.uploadButton) {
                context.uploadButton.removeAttr('disabled');
            }

            if (files.length > 0 && context.autoUpload) {
                context.autoUploadHandle = global.setTimeout(function(){
                	_upload(context);
                }, 499);
            }
        },

        _updateFileList = function(context)
        {
            global.clearTimeout(context.fileListRefreshHandle);
            var currTime = (new Date()).getTime();
            if (context.lastFileListRefresh > 0 && currTime - context.lastFileListRefresh < 1000)
            {
                context.fileListRefreshHandle = global.setTimeout(function(){
                    _updateFileList(context);
                }, 1001 - (currTime - context.lastFileListRefresh));
                return;
            }

            context.lastFileListRefresh = currTime;

            var sb = [];
            for (var i = 0; i < context.files.length; i++)
            {
                if (context.uploadedFiles[context.files[i].id]) {
                    sb[sb.length] = context.uploadedFormat.replace('{0}', context.files[i].name);
                }
                else if (context.erroredFiles[context.files[i].id]) {
                    sb[sb.length] = context.errorFormat.replace('{0}', context.files[i].name);
                }
                else if (context.cancelledFiles[context.files[i].id]) {
                    sb[sb.length] = context.cancelledFormat.replace('{0}', context.files[i].name);
                }
                else if (context.currentUploadId == context.files[i].id) {
                    sb[sb.length] = context.uploadingFormat.replace('{0}', context.files[i].name).replace('{1}', context.currentUploadPercent);
                }
                else {
                    sb[sb.length] = context.toUploadFormat.replace('{0}', context.files[i].name).replace('{1}', context.variableName).replace('{2}', context.files[i].id);
                }
                if (i === 0) {
                    sb[sb.length - 1] = sb[sb.length - 1].replace(context.firstItemRemove, '');
                }
            }

            context.listContainer.html(sb.join(''));
        },

        _stateChanged = function(context)
        {
            if (context.pluploader.state == plupload.STOPPED)
            {
                if (context.upload) {
                    context.upload.disabled = false;
                }
            }
            else if (context.pluploader.state == plupload.STARTED)
            {
                if (context.upload) {
                    context.upload.disabled = true;
                }
            }
        },

        _error = function(context, err)
        {
            if (!context.initialized)
            {
                var message = context.notSupportedMessage;
            	if (!message)
            	{
	            	var runtimes = [];
	            	if (context.runtimes.indexOf('html5') > -1)
	            	{
	            		runtimes.push('An HTML5 capable browser');
	            	}
	            	
	            	if (context.runtimes.indexOf('silverlight') > -1)
	            	{
	            		runtimes.push('<a href="http://microsoft.com/getsilverlight/" target="_blank">Silverlight</a>');
	            	}
	            	
	            	if (context.runtimes.indexOf('flash') > -1)
	            	{
	            		runtimes.push('<a href="http://get.adobe.com/flashplayer/" target="_blank">Flash</a>');
	            	}
	            	
	            	var message;
	            	if (runtimes.length == 0)
	            	{
	            		message = 'Uploading is not supported.';
	            	}
	            	else
	            	{
	            		if (runtimes.length > 1)
	            		{
	            			runtimes[runtimes.length - 1] = 'or ' + runtimes[runtimes.length - 1];
	            		}
	            		
	            		message = runtimes.join(', ') + ' is required to upload.';
	            	}
	            }
            	
            	context.container.children().hide();
            	$('<div></div>')
                .css({
                    fontSize: '11px',
                    borderWidth: '1px',
                    borderStyle: 'solid',
                    borderColor: '#999',
                    width: context.width,
                    overflow: 'auto',
                    overflowX: 'hidden',
                    backgroundColor: '#eee',
                    textAlign: 'center',
                    padding: '.25em',
                   	color: '#000'
                })
                .html(message)
                .appendTo(context.container)
            }

            if (err.file)
            {
                context.isUploading = false;
                context.erroredFiles[err.file.id] = true;
                context.currentUploadId = null;
                _updateFileList(context);
                context.container.trigger(_eventNames.error);
            }
        },

        _fileProgress = function(context, file)
        {
            if (file.status == plupload.UPLOADING)
            {
                if (!context.isUploading || context.currentUploadId != file.id)
                {
                    var execEvent = !context.isUploading;
                    context.isUploading = true;
                    context.currentUploadId = file.id;
                    context.currentUploadPercent = 0;
                    _updateFileList(context);

                    if(execEvent) {
                        context.container.trigger(_eventNames.begun);
                    }
                }

                context.currentUploadPercent = file.percent;
                _updateFileList(context);
            }
            else if (file.status == plupload.FAILED)
            {
                context.isUploading = false;
                context.erroredFiles[file.id] = true;
                context.currentUploadId = null;
                _updateFileList(context);

                context.container.trigger(_eventNames.error);
            }
        },

        _fileComplete = function(context, file)
        {
            context.uploadedFiles[file.id] = true;
            _updateFileList(context);

            for (var i = 0; i < context.files.length; i++)
            {
                if (!context.uploadedFiles[context.files[i].id] && !context.cancelledFiles[context.files[i].id] && !context.erroredFiles[context.files[i].id]) {
                    return;
                }
            }

            context.isUploading = false;
            context.currentUploadId = null;
            _updateFileList(context);
            if (context.uploadButton) {
                context.uploadButton.attr('disabled','disabled');
            }

        },

        _uploadComplete = function(context, files)
        {
            files = $.map((files || []), function(file) {
                return { file: file.name };
            });
            context.container.trigger(_eventNames.complete, files);
        },

        _uploaderInit = function(context)
        {
            // remove the non-plupload control
            context.container.children(':not(.plupload)').remove();

            context.listContainer = $('<div></div>')
                .css({
                    fontSize: '11px',
                    borderWidth: '1px',
                    borderStyle: 'solid',
                    borderColor: '#999',
                    borderBottomWidth: '0px',
                    width: context.width,
                    overflow: 'auto',
                    overflowX: 'hidden',
                    padding: '.35em',
                    backgroundColor: '#fff'
                });

            if (context.height && context.height != '100%') {
                context.listContainer.css({ height: context.height });
            }
            else {
                context.listContainer.css({minHeight: '50px'});
            }

            context.container.append(context.listContainer);


            var buttonContainer = $('<div></div>')
                .css({
                    fontSize: '11px',
                    borderWidth: '1px',
                    borderStyle: 'solid',
                    borderColor: '#999',
                    width: context.width,
                    padding: '.35em',
                    backgroundColor: '#eee',
                    textAlign: 'left'
                });

            if (context.autoUpload === false)
            {
                context.uploadButton = $('<input type="button" />')
                    .val('Upload')
                    .css({ cssFloat: 'right' })
                    .bind('click', function(){
                    	_upload(context);
                        return false;
                    })
                    .attr('disabled', 'disabled')
                    .appendTo(buttonContainer);
            }


            var browseButton = $('<input type="button" />')
                .val('Browse...')
                .css({ cssFloat: 'left'})
                .attr('id', context.variableName + "_browseButton")
                .appendTo(buttonContainer);

            var clearDiv = $('<div></div>')
                .css({
                    clear: 'both',
                    height: '0px',
                    overflow: 'hidden'
                })
                .appendTo(buttonContainer);

            context.container.append(buttonContainer);
            _updateFileList(context);

            context.pluploader.settings.browse_button = browseButton.attr('id');
            context.pluploader.settings.drop_element = context.container.attr('id');

            context.initialized = true;
        },
        _upload = function(context) {
            if (context.pluploader) {
                global.setTimeout(function(){
                    context.pluploader.start();
                }, 99);
            }
        },
        _cancelFile = function(context, fileId) {
            if (context.pluploader)
            {
                try
                {
                    var f = context.pluploader.getFile(file_id);
                    if (f) {
                        context.pluploader.removeFile(f);
                    }
                }
                catch (e) { }
            }

            context.cancelledFiles[file_id] = true;
            _updateFileList(context);
        }



    $.fn.glowMultiUpload = function(method) {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowMultiUpload');
        }
    };
    $.extend($.fn.glowMultiUpload, {
        defaults: {
            fileFilter: null,
            maxFileSize: '2048mb',
            width: '350px',
            height: '200px',
            uploadUrl: '/',
            autoUpload: true,
            maxFileCount: 5,
            fileList: [],
            swfUrl: 'plupload.flash.swf',
            silverlightUrl: '',
            uploadedFormat: "<div style=\"overflow: hidden; text-overflow: ellipsis; color: #000; border-top: solid 1px #eee; padding-top: .25em; margin-top: .25em;\"><div style=\"float: right; color: #3a3; margin-left: .25em;\">Uploaded</div>{0}</div>",
            uploadingFormat: "<div style=\"overflow: hidden; text-overflow: ellipsis; color: #000; border-top: solid 1px #eee; padding-top: .25em; margin-top: .25em;\"><div style=\"float: right; margin-top: .25em; margin-left: .25em; border: solid 1px #999; background-color: #eee; width: 50px; height: .5em;\"><div style=\"background-color: #7e7; width: {1}%; height: .5em;\"></div></div>{0}</div>",
            toUploadFormat: "<div style=\"overflow: hidden; text-overflow: ellipsis; color: #666; font-style: italic; border-top: solid 1px #eee; padding-top: .25em; margin-top: .25em;\"><div style=\"float: right; margin-left: .25em; font-style: normal;\"><a href=\"#\" style=\"color: #f00;\" onclick=\"window.{1}.CancelFile('{2}'); return false;\">Cancel</a></div>{0}</div>",
            errorFormat: "<div style=\"overflow: hidden; text-overflow: ellipsis; color: #000; border-top: solid 1px #eee; padding-top: .25em; margin-top: .25em;\"><div style=\"float: right; color: #f00; margin-left: .25em;\">Error</div>{0}</div>",
            cancelledFormat: "<div style=\"overflow: hidden; text-overflow: ellipsis; color: #666; font-style: italic; border-top: solid 1px #eee; padding-top: .25em; margin-top: .25em;\"><div style=\"float: right; color: #666; font-style: italic; margin-left: .25em;\">Cancelled</div>{0}</div>",
            firstItemRemove: "border-top: solid 1px #eee; padding-top: .25em; margin-top: .25em;",
            runtimes: 'html5,flash'
        }
    });

})(jQuery, window);

 
 
/// @name glowOrderedList
/// @category jQuery Plugin
/// @description Renders a sortable list
/// 
/// ### jQuery.fn.glowOrderedList
/// 
/// This plugin presents a sortable list which supports ordering, adding, and removing items
/// 
/// ### Usage
/// 
/// Initiate an orderable list of items
/// 
/// 	$('selector').glowOrderedList(options)
/// 
/// where selector contains `select` element(s) and `options` is an optional object argument
/// 
/// ### Options
/// 
///  * `width`: width of the ordered list
///    * default: `200`
///  * `height`: height of the ordered list
///    * default: `300`
///  * `itemsHtml`: array of html element strings to render as items in the ordered list
///    * default: `[]`
///  * `draggableOrderedLists`:
///    * default: `null`
/// 
/// ### Events
/// 
///  * `glowOrderedListItemSelected`: triggered when an item is selected
///  * `glowOrderedListItemRemoved`: triggered when an item is removed
///  * `glowOrderedListItemMoved`: triggered when an item is moved
///  * `glowOrderedListItemAdded`: triggered when an item is addeds
/// 
/// ### Methods
/// 
/// #### getIndexByPosition
/// 
/// Returns the index of item by an absolute x, y pixel position
/// 
///     var x = 100, y = 200;
///     $('selector').glowOrderedList('getIndexByPosition', x, y)
/// 
/// #### highlight
/// 
/// Boolean value which sets whether the items are highlighted
/// 
///     $('selector').glowOrderedList('getIndexByPosition', true)
/// 
/// #### addDraggedItem
/// 
/// Adds a dragged item at a given index
/// 
///     var index = 2;    
///     var item = $('selector').glowOrderedList('createItem', {value: 'value', text: 'text', html: 'html'});
///     $('selector').glowOrderedList('addDraggedItem', item, index, event);
/// 
/// #### selectedIndex
/// 
/// Returns the current selected index
/// 
///     var index = $('selector').glowOrderedList('selectedIndex');
/// 
/// #### moveUp
/// 
/// Moves the currently selected item up by one position
/// 
///     $('selector').glowOrderedList('moveUp');
/// 
/// #### moveDown
/// 
/// Moves the currently selected item down by one position
/// 
///     $('selector').glowOrderedList('moveDown');
/// 
/// #### resize
/// 
/// Resizes the ordered list to a given pixel width and height
/// 
///     var x = 400;
///     var y = 300;
///     $('selector').glowOrderedList('resize', x, y);
/// 
/// #### disabled
/// 
/// Gets and/or sets whether the ordered list is disabled
/// 
///     var isDisabled = $('selector').glowOrderedList('disabled');
///     $('selector').glowOrderedList('disabled', true);
/// 
/// #### select
/// 
/// Selects an item by index
/// 
///     var index = 4;
///     $('selector').glowOrderedList('select', index);
/// 
/// #### add
/// 
/// Add a new ordered list item
/// 
///     var item = $('selector').glowOrderedList('createItem', {value: 'value', text: 'text', html: 'html'});
///     $('selector').glowOrderedList('add', item);
/// 
/// #### remove
/// 
/// Removes an ordered list item
/// 
///     $('selector').glowOrderedList('remove', item);
/// 
/// #### clear
/// 
/// Removes all items
/// 
///     $('selector').glowOrderedList('clear');
/// 
/// #### insert
/// 
/// Add a new ordered list item at a specific index
/// 
///     var item = $('selector').glowOrderedList('createItem', {value: 'value', text: 'text', html: 'html'});
///     $('selector').glowOrderedList('insert', item, 4);
/// 
/// #### getByIndex
/// 
/// Returns an item at a specific index
/// 
///     var item = $('selector').glowOrderedList('getByIndex', 4);
/// 
/// #### count
/// 
/// Returns the count of items
/// 
///     var count = $('selector').glowOrderedList('count');
/// 
/// #### createItem
/// 
/// Creates a new ordered list item suitable for adding/inserting
/// 
///     var item = $('selector').glowOrderedList('createItem', {value: 'value', text: 'text', html: 'html'});
/// 

(function($){

    // public api
    var api = 
    {
        refreshPagePosition : function()
        {
            return this.each(function()
            {
                var context = $(this).data('orderedList');
                context.internal.pagePosition = $.telligent.glow.utility.getElementInfo(context.internal.itemsContainer);
            });
        }, 
        getIndexByPosition : function(x, y)
        {
            var context = this.data('orderedList');
            if (context)
            {
                if (context.internal.pagePosition == null)
                    api.refreshPagePosition.apply($(context.internal.state));
                
                if (x > context.internal.pagePosition.Left && x < context.internal.pagePosition.Left + context.internal.pagePosition.Width
                    && y > context.internal.pagePosition.Top && y < context.internal.pagePosition.Top + context.internal.pagePosition.Height)
                {
                    var yOffset = (y - context.internal.pagePosition.Top) + context.internal.itemsContainer.scrollTop;
                    var i = 0;
                    for (; i < context.internal.itemsContainer.childNodes.length; i++)
                    {
                        if (context.internal.itemsContainer.childNodes[i].offsetHeight < yOffset)
                            yOffset -= context.internal.itemsContainer.childNodes[i].offsetHeight;
                        else
                            return i;
                    }
                    
                    return i;
                }
            }

            return -1;
        }, 
        highlight : function(highlight)
        {
            return this.each(function()
            {
                var context = $(this).data('orderedList');
                    
                if (highlight)
                {
                    for (var i = 0; i < context.internal.itemsContainer.childNodes.length; i++)
                    {
                        context.internal.itemsContainer.childNodes[i].style.backgroundColor = '#cccccc'; 
                    }
                    
                    context.internal.itemsContainer.style.backgroundColor = '#cccccc'; 
                    context.internal.itemsContainer.style.borderColor = '#666666';
                }
                else
                {
                    for (var i = 0; i < context.internal.itemsContainer.childNodes.length; i++)
                    {
                        if (context.internal.selectedIndex != i)
                        {
                            context.internal.itemsContainer.childNodes[i].style.backgroundColor = context.internal.backgroundColor; 
                            context.internal.itemsContainer.childNodes[i].style.borderColor = context.internal.backgroundColor;
                        }
                    }

                    context.internal.itemsContainer.style.backgroundColor = context.internal.backgroundColor; 
                    context.internal.itemsContainer.style.borderColor = '#999999';
                }               
            });
        }, 
        addDraggedItem : function(orderedListItem, index, event)
        {
            if (!orderedListItem)
                return;
            
            var isFirst = true;
            return this.each(function()
            {
                if (isFirst)
                    isFirst = false;
                else
                    orderedListItem = orderedListItem._clone();
                
                var context = $(this).data('orderedList');
                
                if (!event)
                    event = window.event;
                
                api.insert.apply($(context.internal.state), [orderedListItem, index]);
                api.refresh.apply($(context.internal.state), []);
                
                $(context.internal.state).trigger(_eventNames.itemAdded, orderedListItem)
                
                _itemMouseOver(context, index);
                _scrollToItem(context, index);
                _itemMouseDown(context, event);
                _initializeDragging(context);
            });
        }, 
        selectedIndex : function()
        {
            var context = this.data('orderedList');
            if (context)
                return context.internal.selectedIndex;
            
            return undefined;
        }, 
        moveUp : function()
        {
            return this.each(function()
            {
                var context = $(this).data('orderedList');
                
                if (context.internal.selectedIndex > 0)
                {
                    var startIndex = context.internal.selectedIndex;
                    var newIndex = context.internal.selectedIndex - 1;
                    _move(context, context.internal.selectedIndex, newIndex);
                    api.select.apply($(context.internal.state), [newIndex, true]);
                    
                    _onItemMoved(context,
                        context.internal.items[newIndex], 
                        startIndex, 
                        newIndex);
                }
            });
        }, 
        moveDown : function()
        {
            return this.each(function()
            {
                var context = $(this).data('orderedList');
                
                if (context.internal.selectedIndex >= 0 && context.internal.selectedIndex < context.internal.itemsContainer.childNodes.length - 1)
                {
                    var startIndex = context.internal.selectedIndex;
                    var newIndex = context.internal.selectedIndex + 1;
                    _move(context, context.internal.selectedIndex, newIndex);
                    api.select.apply($(context.internal.state), [newIndex, true]);
                    
                    _onItemMoved(context,
                        context.internal.items[newIndex], 
                        startIndex, 
                        newIndex);
                }
            });
        }, 
        resize : function(width, height)
        {
            if (width <= 4)
                width = 4;
            
            if (height <= 4)
                height = 4;
            
            return this.each(function()
            {
                var context = $(this).data('orderedList');
            
                context.internal.itemsContainer.style.width = (width - 4) + 'px';
                context.internal.itemsContainer.style.height = (height - 4) + 'px';
            });
        }, 
        disabled: function(disable)
        {
            if (disable === undefined)
            {
                var context = this.data('orderedList');
                if (context)
                    return context.internal.state.readonly;
                
                return undefined;
            }
            else
            {
                return this.each(function()
                {
                    var context = $(this).data('orderedList');
                    context.internal.state.readonly = disable;
                    api.refresh.apply($(context.internal.state), []);
                });
            }
        },
        refresh : function()
        {
            return this.each(function()
            {
                var context = $(this).data('orderedList');
                
                if (context.internal.initialized)
                {
                    while (context.internal.itemsContainer.childNodes.length > 0)
                        context.internal.itemsContainer.removeChild(context.internal.itemsContainer.childNodes[0]);
                    
                    var selectedIndex = -1;
                    for (var i = 0; i < context.internal.items.length; i++)
                    {
                        var item = document.createElement('div');
                        
                        if (!context.internal.state.readonly)
                        {
                            item.onmouseover = $.telligent.glow.utility.makeBoundFunction(_itemMouseOver, null, [context, i]);
                            item.onmouseout = $.telligent.glow.utility.makeBoundFunction(_itemMouseOut, null, [context, i]);
                            item.onclick = $.telligent.glow.utility.makeBoundFunction(api.select, $(context.internal.state), [i]);
                            item.onmousedown = function(event) { _itemMouseDown(context, event); };
                            item.style.cursor = 'pointer';
                        }
                        else
                            item.style.cursor = 'default';
                        
                        item.onselectstart = function() { return false; };
                        try { item.style.MozUserSelect = 'none'; } catch (e) {}
                        try { item.style.userSelect = 'none'; } catch (e) {}
                        try { item.style.WebkitUserSelect = 'ignore'; } catch (e) {}
                        
                        item.style.padding = '2px';
                        item.style.borderStyle = 'dotted';
                        item.style.borderWidth = '1px';
                        item.style.display = 'block';
                        item.style.borderColor = context.internal.backgroundColor;      
                        item.innerHTML = context.internal.items[i].html;
                        context.internal.itemsContainer.appendChild(item);
                    }
                    
                    context.internal.selectedIndex = -1;
                }
            });
        }, 
        select : function(index, ignoreDeselection)
        {
            return this.each(function()
            {
                var context = $(this).data('orderedList');
                
                if (context.internal.initialized && index > -1)
                {
                    if (context.internal.selectedIndex != index || ignoreDeselection)
                    {
                        if (context.internal.selectedIndex > -1)
                        {
                            context.internal.itemsContainer.childNodes[context.internal.selectedIndex].style.backgroundColor = context.internal.backgroundColor;
                            context.internal.itemsContainer.childNodes[context.internal.selectedIndex].style.borderColor = context.internal.backgroundColor;
                        }
                        
                        context.internal.selectedIndex = index;
                        context.internal.itemsContainer.childNodes[index].style.backgroundColor = '#cccccc'; 
                        context.internal.itemsContainer.childNodes[index].style.borderColor = '#666666';
                        _scrollToItem(context, index);
                        
                        try
                        {
                            if (context.internal.moveItemLastIndex != null)
                                _itemMouseOut(context, context.internal.moveItemLastIndex);
                        }
                        catch (e) {}
                        
                        $(context.internal.state).trigger(_eventNames.itemSelected, 
                            [context.internal.items[context.internal.selectedIndex]]
                            );
                    }
                    else
                    {
                        context.internal.itemsContainer.childNodes[context.internal.selectedIndex].style.backgroundColor = context.internal.backgroundColor;
                        context.internal.itemsContainer.childNodes[context.internal.selectedIndex].style.borderColor = context.internal.backgroundColor;
                        context.internal.selectedIndex = -1;    
                    }
                }
            });
        }, 
        add : function(orderedListItem)
        {
            if (!orderedListItem)
                return this;
            
            api.remove.apply(this, [orderedListItem]);
            
            var isFirst = true;
            
            return this.each(function()
            {
                if (isFirst)
                    isFirst = false;
                else
                    orderedListItem = orderedListItem._clone();
                
                var context = $(this).data('orderedList');
                context.internal.items[context.internal.items.length] = orderedListItem;
                
                var option = document.createElement('option');
                option.name = orderedListItem.text;
                option.value = orderedListItem.value;
                
                context.internal.state.appendChild(option);
                
                $(context.internal.state).trigger(_eventNames.itemAdded, option);
            });
        }, 
        remove : function(orderedListItem)
        {
            if (!orderedListItem)
                return this;
            
            return this.each(function()
            {
                var context = $(this).data('orderedList');
                
                var ddlItems = new Array();
                var found = false;
                for (var i = 0; i < context.internal.items.length; i++)
                {
                    if (context.internal.items[i].value == orderedListItem.value)
                        found = true;
                    else
                        ddlItems[ddlItems.length] = context.internal.items[i];
                }
                
                for (var i = 0; i < context.internal.state.options.length; i++)
                {
                    if (context.internal.state.options[i].value == orderedListItem.value)
                    {
                        context.internal.state.removeChild(context.internal.state.options[i]);
                        break;
                    }
                }
                
                $(context.internal.state).trigger(_eventNames.itemRemoved, orderedListItem);
                
                if (found)
                    context.internal.items = ddlItems;
            });
        }, 
        clear : function()
        {
            return this.each(function()
            {
                var context = $(this).data('orderedList');
            
                context.internal.items = new Array();
                $(context.internal.state).empty();
            });
        }, 
        insert : function(orderedListItem, index)
        {
            if (!orderedListItem)
                return this;
            
            api.remove.apply(this, [orderedListItem]);
            
            return this.each(function()
            {
                var context = $(this).data('orderedList');
                
                var option = document.createElement('option');
                option.name = orderedListItem.text;
                option.value = orderedListItem.value;
                
                var ddlItems = new Array();
                var inserted = false;
                for (var i = 0; i < context.internal.items.length; i++)
                {
                    if (i == index)
                    {
                        inserted = true;
                        ddlItems[ddlItems.length] = orderedListItem;

                        if (i == context.internal.items.length - 1)
                            context.internal.state.appendChild(option);
                        else
                            context.internal.state.insertBefore(option, context.internal.state.options[i]);
                    }
                    
                    ddlItems[ddlItems.length] = context.internal.items[i];
                }
                
                if (!inserted)
                {
                    ddlItems[ddlItems.length] = orderedListItem;
                    context.internal.state.appendChild(option);
                }

                context.internal.items = ddlItems;
            });
        }, 
        getByIndex : function(index)
        {
            var context = this.data('orderedList');
            if (context && index >= 0 && index < context.internal.items.length)
                return context.internal.items[index];

            return null;
        }, 
        count : function()
        {
            var context = this.data('orderedList');
            if (context)
                return context.internal.items.length;
            
            return 0;
        },
        createItem: function(options)
        {
            return new OrderedListItem(options);
        }
    };


    // private methods
    var _eventNames = {
            itemSelected: 'glowOrderedListItemSelected',
            itemRemoved: 'glowOrderedListItemRemoved',
            itemMoved: 'glowOrderedListItemMoved',
            itemAdded: 'glowOrderedListItemAdded'
        },
        _init = function(options) 
        {
            return this.each(function() 
            {
            
                var context = {
                    settings: $.extend({}, $.fn.glowOrderedList.defaults, options || {}),
                    internal: {
                        state: this,
                        selectedIndex: -1,
                        pagePosition: null,
                        draggingInitalized: false,
                        moveStartY: null,
                        moveItemHeights: null,
                        moveItemStartIndex: null,
                        moveItemLastIndex: null,
                        moveScrollStartY: null,
                        moveItemFloat: null,
                        moveItemIsFloating: false,
                        items: null,
                        backgroundColor: null,
                        itemsContainer: null,
                        initialized: false,
                        itemsContainer: null,
                        draggableOrderedLists: []
                    }
                };              
            
                $(this).data('orderedList', context);

                _initialize(context);
            });
        },  
        _initializeMoveFloat = function(context, index)
        {
            if (!context.internal.moveItemFloat)
            {
                context.internal.moveItemFloat = document.createElement('div');
                context.internal.moveItemFloat.style.position = 'absolute';
                context.internal.moveItemFloat.style.opacity = '.75';
                if ($.telligent.glow.utility.isIE())
                    context.internal.moveItemFloat.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=75)';

                context.internal.moveItemFloat.style.backgroundColor = '#cccccc'; 
                context.internal.moveItemFloat.style.borderColor = '#666666';
                context.internal.moveItemFloat.style.fontFamily = context.internal.itemsContainer.style.fontFamily;
                context.internal.moveItemFloat.style.fontSize = context.internal.itemsContainer.style.fontSize;
                context.internal.moveItemFloat.style.lineHeight = context.internal.itemsContainer.style.lineHeight;
                context.internal.moveItemFloat.style.color = context.internal.itemsContainer.style.color;
                context.internal.moveItemFloat.style.padding = '2px';
                context.internal.moveItemFloat.style.cursor = 'move';
                context.internal.moveItemFloat.style.borderStyle = 'dotted';
                context.internal.moveItemFloat.style.borderWidth = '1px';
            
                document.body.appendChild(context.internal.moveItemFloat);
            }
        
            while (context.internal.moveItemFloat.childNodes.length > 0)
                context.internal.moveItemFloat.removeChild(context.internal.moveItemFloat.childNodes[context.internal.moveItemFloat.childNodes.length - 1]);
        
            context.internal.moveItemFloat.style.width = context.internal.itemsContainer.childNodes[index].offsetWidth + 'px';
            context.internal.moveItemFloat.style.height = context.internal.itemsContainer.childNodes[index].offsetHeight + 'px';
            context.internal.moveItemFloat.innerHTML = context.internal.itemsContainer.childNodes[index].innerHTML;
        }, 
        _initializeDraggableOrderedLists = function(context)
        {
            context.internal.draggableOrderedLists = new Array();
            if (!context.settings.draggableOrderedLists || context.settings.draggableOrderedLists.length <= 0)
                return;
        
            context.settings.draggableOrderedLists.each(function()
            {
                try
                {
                    if ($(this).glowOrderedList('disabled') === false)
                        context.internal.draggableOrderedLists[context.internal.draggableOrderedLists.length] = $(this);    
                } catch (e) { }
            });
        }, 
        _initializeDragging = function(context)
        {
            api.refreshPagePosition.apply($(context.internal.state));       
            _initializeDraggableOrderedLists(context);
            var i;
            for (i = 0; i < context.internal.draggableOrderedLists.length; i++)
            {
                context.internal.draggableOrderedLists[i].glowOrderedList('highlight', true);
                context.internal.draggableOrderedLists[i].glowOrderedList('refreshPagePosition');
            }

            if (context.internal.draggableOrderedLists.length > 0)
            {   
                context.internal.itemsContainer.style.cursor = 'move';
                for(i = 0; i < context.internal.itemsContainer.childNodes.length; i++)
                {
                    context.internal.itemsContainer.childNodes[i].style.cursor = 'move';
                }
            }
            else
            {
                context.internal.itemsContainer.style.cursor = 'n-resize';
                for(i = 0; i < context.internal.itemsContainer.childNodes.length; i++)
                {
                    context.internal.itemsContainer.childNodes[i].style.cursor = 'n-resize';
                }
            }
        
            context.internal.draggingInitalized = true;
        }, 
        _isMouseOver = function(context, x, y, allowMargin)
        {
            if (context.internal.pagePosition == null)
                api.refreshPagePosition.apply($(context.internal.state));
        
            var left = context.internal.pagePosition.Left;
            var right = context.internal.pagePosition.Left + context.internal.pagePosition.Width
            var top = context.internal.pagePosition.Top;
            var bottom = context.internal.pagePosition.Top + context.internal.pagePosition.Height;
        
            if (allowMargin)
            {
                left -= 24;
                right += 24;
                top -= 32;
                bottom += 32;
            
                if (context.internal.moveItemLastIndex >= 0)
                {
                    if (context.internal.moveItemLastIndex > 0)
                        top -= context.internal.itemsContainer.childNodes[context.internal.moveItemLastIndex - 1].offsetHeight;

                    if (context.internal.moveItemLastIndex < context.internal.itemsContainer.childNodes.length - 1)
                        bottom += context.internal.itemsContainer.childNodes[context.internal.moveItemLastIndex + 1].offsetHeight;
                }
            }
        
            return (x > left && x < right && y > top && y < bottom);
        }, 
        _move = function(context, oldIndex, newIndex)
        {
            if (oldIndex < 0 || newIndex < 0 || newIndex == oldIndex)
                return;
        
            if (oldIndex < context.internal.items.length && newIndex < context.internal.items.length)
            {
                var item = context.internal.items[oldIndex];
                if (oldIndex < newIndex)
                {
                    for (var i = oldIndex; i < newIndex; i++)
                        context.internal.items[i] = context.internal.items[i + 1];
                }
                else
                {
                    for (var i = oldIndex; i > newIndex; i--)
                        context.internal.items[i] = context.internal.items[i - 1];
                }
                context.internal.items[newIndex] = item;
            }
        
            if (context.internal.initialized && oldIndex < context.internal.itemsContainer.childNodes.length && newIndex < context.internal.itemsContainer.childNodes.length)
            {
                if (newIndex == context.internal.itemsContainer.childNodes.length - 1)
                    context.internal.itemsContainer.appendChild(context.internal.itemsContainer.childNodes[oldIndex]);
                else if (oldIndex > newIndex)
                    context.internal.itemsContainer.insertBefore(context.internal.itemsContainer.childNodes[oldIndex], context.internal.itemsContainer.childNodes[newIndex]);
                else if (oldIndex < newIndex)
                    context.internal.itemsContainer.insertBefore(context.internal.itemsContainer.childNodes[oldIndex], context.internal.itemsContainer.childNodes[newIndex + 1]);
            
                if (oldIndex < newIndex)
                {
                    for (var i = oldIndex; i <= newIndex; i++)
                    {
                        context.internal.itemsContainer.childNodes[i].onmouseover = $.telligent.glow.utility.makeBoundFunction(_itemMouseOver, null, [context, i]);
                        context.internal.itemsContainer.childNodes[i].onmouseout = $.telligent.glow.utility.makeBoundFunction(_itemMouseOut, null, [context, i]);
                        context.internal.itemsContainer.childNodes[i].onclick = $.telligent.glow.utility.makeBoundFunction(api.select, $(context.internal.state), [i]);
                    }
                }
                else
                {
                    for (var i = oldIndex; i >= newIndex; i--)
                    {
                        context.internal.itemsContainer.childNodes[i].onmouseover = $.telligent.glow.utility.makeBoundFunction(_itemMouseOver, null, [context, i]);
                        context.internal.itemsContainer.childNodes[i].onmouseout = $.telligent.glow.utility.makeBoundFunction(_itemMouseOut, null, [context, i]);
                        context.internal.itemsContainer.childNodes[i].onclick = $.telligent.glow.utility.makeBoundFunction(api.select, $(context.internal.state), [i]);
                    }
                }
            }
        }, 
        _getMovingIndex = function(context, yChange)
        {
            var newIndex = context.internal.moveItemStartIndex;
            if (yChange > 0)
            {
                // move down
                var yLeft = yChange;
                while (newIndex + 1 < context.internal.moveItemHeights.length && yLeft > context.internal.moveItemHeights[newIndex + 1])
                {
                    newIndex++;
                    yLeft -= context.internal.moveItemHeights[newIndex];
                }
            }
            else
            {
                // move up
                var yLeft = -yChange;
                while (newIndex - 1 >= 0 && yLeft > context.internal.moveItemHeights[newIndex - 1])
                {
                    newIndex--;
                    yLeft -= context.internal.moveItemHeights[newIndex];
                }
            }
        
            return newIndex;
        }, 
        _isMoving = function(context)
        {
            return context.internal.moveStartY != null && context.internal.moveItemLastIndex != null;
        },
        _itemMouseDown = function (context, event)
        {
            if (!event)
                event = window.event;
        
            if (context.internal.moveItemLastIndex >= 0 && context.internal.moveItemLastIndex < context.internal.itemsContainer.childNodes.length)
            {
                if (typeof(event.pageY) != 'undefined')
                    context.internal.moveStartY = event.pageY;
                else
                {
                    var windowInfo = $.telligent.glow.utility.getWindowInfo();
                    context.internal.moveStartY = event.clientY + windowInfo.ScrollY;
                }
            
                context.internal.moveScrollStartY = context.internal.itemsContainer.scrollTop;
                context.internal.moveItemStartIndex = context.internal.moveItemLastIndex;
            
                context.internal.moveItemHeights = new Array();
                var i;
                for(i = 0; i < context.internal.itemsContainer.childNodes.length; i++)
                {
                    context.internal.moveItemHeights[context.internal.moveItemHeights.length] = context.internal.itemsContainer.childNodes[i].offsetHeight;
                }

                context.internal.draggingInitalized = false;
            
                $(document).bind('mouseup.orderedList', function(event) { _endMoveTracking(context, event); return false; });
                $(document).bind('mousemove.orderedList', function(event) { _mouseMoveHandler(context, event); return false; });
            }
        }, 
        _mouseMoveHandler = function(context, event)
        {
            if (!event)
                event = window.event;
        
            var eventY = 0, eventX = 0;
            if (typeof(event.pageY) != 'undefined')
            {
                eventY = event.pageY;
                eventX = event.pageX;
            }
            else
            {
                var windowInfo = $.telligent.glow.utility.getWindowInfo();
                eventY = event.clientY + windowInfo.ScrollY;
                eventX = event.clientX + windowInfo.ScrollX;
            }

            var newIndex;
        
            if (!_isMoving(context))
                return;
        
            if (!context.internal.draggingInitalized)
            {
                _initializeDragging(context);
            }
        
            if (context.internal.draggableOrderedLists.length > 0 && !context.internal.moveItemIsFloating && !_isMouseOver(context, eventX, eventY, true))
            {
                // make floating
                _initializeMoveFloat(context, context.internal.moveItemLastIndex);
                context.internal.moveItemIsFloating = true;
                context.internal.moveItemFloat.style.display = 'block';
            }
        
            if (context.internal.moveItemIsFloating)
            {
                // check for overlap of other ordered lists
                context.internal.moveItemFloat.style.left = (eventX - (context.internal.moveItemFloat.offsetWidth / 2)) + 'px';
                context.internal.moveItemFloat.style.top = (eventY - (context.internal.moveItemFloat.offsetHeight / 2)) + 'px';
            
                if (_isMouseOver(context, eventX, eventY, true))
                {
                    context.internal.moveItemFloat.style.display = 'none';
                    context.internal.moveItemIsFloating = false;
                }
                else
                {
                    for (var i = 0; i < context.internal.draggableOrderedLists.length; i++)
                    {
                        if ((newIndex = context.internal.draggableOrderedLists[i].glowOrderedList('getIndexByPosition', eventX, eventY)) >= 0)
                        {
                            var item = context.internal.items[context.internal.moveItemLastIndex];
                            _endMoveTracking(context, event, true);
                            api.remove.apply($(context.internal.state), [item]);
                            api.refresh.apply($(context.internal.state), []);
                        
                            $(context.internal.state).trigger(_eventNames.itemRemoved, item);
                        
                            context.internal.draggableOrderedLists[i].glowOrderedList('addDraggedItem', item, newIndex, event);
                            context.internal.moveItemFloat.style.display = 'none';
                            context.internal.moveItemIsFloating = false;
                        }
                    }
                }
            }

            if (_isMoving(context) && !context.internal.moveItemIsFloating)
            {
                var yChange = eventY - context.internal.moveStartY;
                yChange += context.internal.itemsContainer.scrollTop - context.internal.moveScrollStartY;   
            
                newIndex = _getMovingIndex(context, yChange);
                if (newIndex != context.internal.moveItemLastIndex && newIndex != null)
                {
                    _move(context, context.internal.moveItemLastIndex, newIndex);
                    context.internal.moveItemLastIndex = newIndex;
                    api.select.apply($(context.internal.state), [newIndex, true]);
                }
            }
        }, 
        _onItemMoved = function(context, item, startIndex, lastIndex) {
            $(context.internal.state).trigger(_eventNames.itemMoved, 
                [ item, startIndex, lastIndex ]);
        },
        _endMoveTracking = function(context, event, ignoreMoveEvents)
        {
            var i;
            for(i = 0; i < context.internal.itemsContainer.childNodes.length; i++)
            {
                context.internal.itemsContainer.childNodes[i].style.cursor = 'pointer';
            }
        
            if (!ignoreMoveEvents && 
                context.internal.moveItemStartIndex != null && 
                context.internal.moveItemLastIndex != null && 
                context.internal.moveItemStartIndex != context.internal.moveItemLastIndex) {
                    _onItemMoved(
                        context,
                        context.internal.items[context.internal.moveItemLastIndex], 
                        context.internal.moveItemStartIndex, 
                        context.internal.moveItemLastIndex);                        
                }
        
            context.internal.itemsContainer.style.cursor = 'auto';
            context.internal.moveStartY = null;
            context.internal.moveItemLastIndex = null;
            context.internal.moveItemHeights = null;
            context.internal.moveItemStartIndex = null;
            $(document).unbind('.orderedList');
        
            if (context.internal.moveItemIsFloating)
            {
                context.internal.moveItemIsFloating = false;
                context.internal.moveItemFloat.style.display = 'none';
            }
        
            for (i = 0; i < context.internal.draggableOrderedLists.length; i++)
            {
                context.internal.draggableOrderedLists[i].glowOrderedList('highlight', false);
            }
        
            api.highlight.apply($(context.internal.state), [false]);
        }, 
        _scrollToItem = function(context, index)
        {
            var itemElement = context.internal.itemsContainer.childNodes[index];
        
            if (itemElement.offsetTop + itemElement.offsetHeight > context.internal.itemsContainer.scrollTop + context.internal.itemsContainer.offsetHeight)
                context.internal.itemsContainer.scrollTop = (itemElement.offsetTop + itemElement.offsetHeight) - context.internal.itemsContainer.offsetHeight;
            else if (itemElement.offsetTop < context.internal.itemsContainer.scrollTop)
                context.internal.itemsContainer.scrollTop = itemElement.offsetTop;
        }, 
        _initialize = function(context)
        {
            if(!context.internal.state.offsetHeight)
            {
                window.setTimeout(function() { _initialize(context); }, 249);
                return;
            }
        
            if (context.internal.state.disabled)
            {
                context.internal.state.disabled = false;
                context.internal.state.readonly = true;
            }
        
            context.internal.backgroundColor = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'background-color', 'backgroundColor', '#ffffff');
            if (context.internal.backgroundColor.replace(/\#[0-9a-f]*/gi, '') != '')
                context.internal.backgroundColor = '#ffffff';
        
            context.internal.itemsContainer = document.createElement('div');
            context.internal.itemsContainer.style.backgroundColor = context.internal.backgroundColor;
            context.internal.itemsContainer.style.fontFamily = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-family', 'fontFamily', 'Arial, Helvetica');
            context.internal.itemsContainer.style.fontSize = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-size', 'fontSize', '100%');
            context.internal.itemsContainer.style.lineHeight = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'line-height', 'lineHeight', '100%');
            context.internal.itemsContainer.style.color = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000000');
            context.internal.itemsContainer.style.borderStyle = 'outset';
            context.internal.itemsContainer.style.borderWidth = '1px';
            context.internal.itemsContainer.style.borderColor = '#999999';
            context.internal.itemsContainer.style.padding = '2px';
            context.internal.itemsContainer.style.overflow = 'auto';
            context.internal.itemsContainer.style.overflowX = 'hidden';
            context.internal.itemsContainer.style.position = 'relative';
        
            if (context.settings.width <= 4)
                context.internal.itemsContainer.style.width = (context.internal.state.offsetWidth - 4) + 'px';
            else
                context.internal.itemsContainer.style.width = (context.settings.width - 4) + 'px';
        
            if (context.settings.height <= 4)
                context.internal.itemsContainer.style.height = (context.internal.state.height - 4) + 'px';
            else
                context.internal.itemsContainer.style.height = (context.settings.height - 4) + 'px';
        
            context.internal.state.parentNode.insertBefore(context.internal.itemsContainer, context.internal.state);

            context.internal.items = new Array();
            for (var i = 0; i < context.internal.state.options.length; i++)
            {
                if (i < context.settings.itemsHtml.length)
                    context.internal.items[context.internal.items.length] = new OrderedListItem({value: context.internal.state.options[i].value, text: context.internal.state.options[i].name, html: context.settings.itemsHtml[i]});
                else
                    context.internal.items[context.internal.items.length] = new OrderedListItem({value: context.internal.state.options[i].value, text: context.internal.state.options[i].name, html: context.internal.state.options[i].name});                 
            }
            
            context.internal.state.style.display = 'none';
            context.internal.initialized = true;
        
            api.disabled.apply($(context.internal.state), [context.internal.state.readonly == true]);
        }, 
        _itemMouseOver = function(context, index)
        {
            if (!_isMoving(context))
            {
                if (context.internal.selectedIndex != index)
                {
                    context.internal.itemsContainer.childNodes[index].style.backgroundColor = '#cccccc'; 
                    context.internal.itemsContainer.childNodes[index].style.borderColor = '#666666';
                }
        
                context.internal.moveItemLastIndex = index;
            }
        }, 
        _itemMouseOut = function(context, index)
        {
            if (!_isMoving(context))
            {
                if (context.internal.selectedIndex != index)
                {
                    context.internal.itemsContainer.childNodes[index].style.backgroundColor = context.internal.backgroundColor;
                    context.internal.itemsContainer.childNodes[index].style.borderColor = context.internal.backgroundColor;
                }
        
                context.internal.moveItemLastIndex = null;
            }
        };

    
    $.fn.glowOrderedList = function(method) 
    {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowOrderedList');
        }
    };  
    $.extend({
        defaults: {
            width:200,
            height:300,
            itemsHtml:[],
            draggableOrderedLists:null
        }       
    });
    // events
    //   glowOrderedListItemSelected
    //   glowOrderedListItemRemoved
    //   glowOrderedListItemMoved
    //   glowOrderedListItemAdded

    var OrderedListItem = function(options)
    {
        var settings = 
        {
            value: '',
            text: '',
            html: options.text
        };

        if (options)
            $.extend(settings, options);

        this.text = settings.text;
        this.value = settings.value;
        this.html = settings.html;
    }

    OrderedListItem.prototype._clone = function()
    {
        return new OrderedListItem({value: this.value, text: this.text, html: this.html});
    };  
})(jQuery);
 
 
/// @name glowPatternedTextBox
/// @category jQuery Plugin
/// @description Decorates a text input with pattern support
/// 
/// ### jQuery.fn.glowPatternedTextBox
/// 
/// Decorates a text input with patterned input functionality
/// 
/// ### Usage
/// 
///     $('SELECTOR').glowPatternedTextBox(options)
/// 
/// where 'SELECTOR' contains the text input element(s) which will be patterned and the options object can contain:
/// 
/// ### Options
/// 
///  * `pattern`: The pattern to use for the text box.  Two types of pattern definitions are supported (any text between a pattern definition is considered static and uneditable):
///    * Numeric range pattern: <X-Y> where X and Y are integers.  Any value between X and Y (inclusive) is considered valid.
///    * Selection range pattern: <Value1,Value2,Value3,Value4,Value5> where any defined textual value is considered valid.
///    * Examples:
///      * Date: `<Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec> <1-31>, <1900-3000>`
///      * Time: `<1-12>:<00-59> <AM,PM>`
///      * Unit: `<0-1600> <px,pt,%,pc,in,mm,cm,em,ex>`
///      * Short-hand Date: `<01-12>/<01-31>/<1900-3000>`
///    * default: `empty string`
///  * `allowBlanks`: whether to allow blank entries
///    * default: `true`
///  * `blankCharacter`: pattern character which represents blanks
///    * default: `-`
///  * `onValidation`: callback function which can provide custom logic for handling the input of a patterned text box.  called whenever a segment of the pattern is iterated up or down.  callback returns an array of selected indexes corresponding to each region in the patterned text box.  callback is passed:
///     * jQ - the patternedTextBox instance jQuery selection
///     * regions - string array of current regions
///     * type - 'next' or 'previous' string - the direction of selection 
///     * modifiedIndex - current index of regions being iterated    
/// 
/// ### Events
/// 
///  * `glowPatternedTextBoxChange` - triggered when the text box changes value
///  
/// ### Methods
/// 
/// #### getPatternValues
/// 
/// Returns a string array of the selected pattern values
/// 
///     var patternValues = $('SELECTOR').glowPatternedTextBox('getPatternValues')
/// 
/// #### getPatternValueOptionIndex
/// 
/// Returns the index of a given pattern value within a given pattern identified by its pattern index
/// 
///     var patternIndex = 2;
///     var value = 'vale';
///     var valueIndex = $('SELECTOR').glowPatternedTextBox('getPatternValueOptionIndex', patternIndex, value)
/// 
/// #### getPatternValueOption
/// 
/// Returns the value of a given pattern index within a given pattern identified by its pattern index
/// 
///     var patternIndex = 2;
///     var valueIndex = 3;
///     var valueOption = $('SELECTOR').glowPatternedTextBox('getPatternValueOptionIndex', patternIndex, valueIndex);
/// 
/// #### setPatternValues
/// 
/// Applies selected pattern indices to each pattern
/// 
///     var patternValues = [4,3,9];
///     $('SELECTOR').glowPatternedTextBox('setPatternValues', patternValues);
/// 
/// #### val
/// 
/// Gets and/or sets the current value of the patterned text box
///     
///     var value = $('SELECTOR').glowPatternedTextBox('val');    
///     $('SELECTOR').glowPatternedTextBox('val', newValue);
/// 
/// #### disabled
/// 
/// Gets and/or sets whether the current patterned text box is disabled 
/// 
///     var disabled = $('SELECTOR').glowPatternedTextBox('disabled');    
///     $('SELECTOR').glowPatternedTextBox('disabled', true);
/// 

(function($){

    var _changeEventName = 'glowPatternedTextBoxChange',
        _eventNameSpace = '.glowPatternedTextBox',
        _dataKey = '_glowPatternedTextBox',
        _init = function(options) 
        {
            return this.each(function() 
            {               
                var context = {
                    settings: $.extend({}, $.fn.glowPatternedTextBox.defaults, options || {}),
                    internal: {
                        state: $(this),
                        patterns: [],
                        initialized: false,
                        matchRE: null,
                        delayedValue: null,
                        cursorSetTimeout: null
                    }
                };                  
                            
                $(this).data(_dataKey, context);

                _initialize(context, context.settings.pattern);
            });
        },
        _changed = function(context)
        {
            context.internal.state.trigger(_changeEventName);
        },
        _getPatternByIndex = function(context, index)
        {
            var j = 0;
            for (var i = 0; i < context.internal.patterns.length; i++)
            {
                if (!context.internal.patterns[i].isStatic())
                {
                    if (j == index) {
                        return context.internal.patterns[i];                        
                    } else {
                        j++;                        
                    }
                }
            }
        },
        _getPatternDataAtIndex = function(context, index)
        {
            if (!context.internal.initialized) {
                return;                
            }
        
            var totalString = '';
            var matches = context.internal.matchRE.exec(context.internal.state.val());
            var patternData = {};
            if (matches)
            {
                for (var i = 1; i < matches.length && i - 1 < context.internal.patterns.length; i++)
                {
                    totalString += matches[i];
                
                    if (
                            (
                                index < totalString.length 
                                || 
                                (
                                    index == totalString.length 
                                    && 
                                    (
                                        matches[i].length < context.internal.patterns[i - 1]._maxLength
                                        ||
                                        i == context.internal.patterns.length
                                    )
                                )
                            ) 
                            && 
                            !context.internal.patterns[i - 1].isStatic()
                        )
                    {
                        if (!patternData.currentPattern)
                        {
                            patternData.currentPattern = context.internal.patterns[i - 1];
                            patternData.currentPatternStartIndex = totalString.length - matches[i].length;
                            patternData.currentPatternEndIndex = totalString.length;
                        }
                        else
                        {
                            patternData.nextPattern = context.internal.patterns[i - 1];
                            patternData.nextPatternStartIndex = totalString.length - matches[i].length;
                            patternData.nextPatternEndIndex = totalString.length;
                            return patternData;
                        }
                    }
                    else if (!context.internal.patterns[i - 1].isStatic())
                    {
                        patternData.previousPattern = context.internal.patterns[i - 1];
                        patternData.previousPatternStartIndex = totalString.length - matches[i].length;
                        patternData.previousPatternEndIndex = totalString.length;   
                    }
                }
            }

            return patternData;
        },
        _processExternalValidation = function(context, patternChanged, movement)
        {
            if (context.settings.onValidation)
            {
                var regions = [];
                var modifiedIndex = -1;
                if (!movement) {
                    movement = 'set';                    
                }
            
                for (var i = 0; i < context.internal.patterns.length; i++)
                {
                    if (!context.internal.patterns[i].isStatic())
                    {
                        regions[regions.length] = context.internal.patterns[i].getValue();
                        if (patternChanged == context.internal.patterns[i]) {
                            modifiedIndex = regions.length - 1;                            
                        }
                    }
                }
            
                regions = context.settings.onValidation(context.internal.state, regions, movement, modifiedIndex);
                if (regions) {
                    api.setPatternValues.apply(context.internal.state, [regions]);
                }
            }
        },
        _initialize = function(context, pattern)
        {
            if (context.internal.state)
            {
                var initialValue = context.internal.state.val();

                var patternRE = new RegExp('<([^<>]{1,}?)>', 'ig');
                var matches;
                var lastIndex = 0;
                var matchPattern = '^';
            
                while (matches = patternRE.exec(pattern))
                {
                    if (matches.index > lastIndex)
                    {
                        context.internal.patterns[context.internal.patterns.length] = new textPattern(pattern.substr(lastIndex, matches.index - lastIndex), context.settings.blankCharacter, context.settings.allowBlanks, true);
                        matchPattern += '(' + context.internal.patterns[context.internal.patterns.length - 1].getRegExpPattern() + ')';
                    }

                    context.internal.patterns[context.internal.patterns.length] = new textPattern(matches[1], context.settings.blankCharacter, context.settings.allowBlanks, false);
                    matchPattern += '(' + context.internal.patterns[context.internal.patterns.length - 1].getRegExpPattern() + ')';

                    lastIndex = patternRE.lastIndex;
                }
            
                if (lastIndex < pattern.length)
                {
                    context.internal.patterns[context.internal.patterns.length] = new textPattern(pattern.substr(lastIndex, pattern.length - lastIndex), context.settings.blankCharacter, context.settings.allowBlanks, true);
                    matchPattern += '(' + context.internal.patterns[context.internal.patterns.length - 1].getRegExpPattern() + ')';
                }
            
                matchPattern += '$';
            
                if (context.internal.state.is('input:disabled')) {
                    api.disabled.apply(context.internal.state, [true]);                    
                } else {
                    api.disabled.apply(context.internal.state, [false]);                    
                }
            
                context.internal.matchRE = new RegExp(matchPattern, 'i');
                context.internal.initialized = true;
            
                api.val.apply(context.internal.state, [initialValue]);
            }
        },
        _focus = function(context, event)
        {
            var patternData;
            if (event.shiftKey) {
                patternData = _getPatternDataAtIndex(context, context.internal.state.val().length);                
            } else {
                patternData = _getPatternDataAtIndex(context, 0);                
            }

            if (patternData.currentPattern) {
                $.telligent.glow.utility.setCurrentCursorSelection(
                    context.internal.state, 
                    patternData.currentPatternStartIndex, 
                    patternData.currentPatternEndIndex, false);                
            }
        },
        _change = function(context, event)
        {
            if (!context.internal.initialized) {
                return false;                
            }
        
            api.val.apply(context.internal.state, [context.internal.state.val()]);
            _changed(context);
        },
        _keyDown = function(context, event)
        {
            if (!context.internal.initialized) {
                return false;                
            }
        
            // up = 38
            // down = 40
            // tab = 9
            // backspace = 8
            // shift+tab = (16 .. 9)
            // enter = 13
            // space = 32

            if (event.keyCode == 38 || event.keyCode == 40 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 9 || event.keyCode == 8)
            {
                var index = $.telligent.glow.utility.getCurrentCursorIndex(context.internal.state);
            
                if ((event.keyCode == 37 || event.keyCode == 8) && index > 0) {
                    index--;                    
                }
        
                if (event.keyCode == 39 && index < context.internal.state.val().length) {
                    index++;                    
                }
            
                var patternData = _getPatternDataAtIndex(context, index);
            
                if (patternData.currentPattern)
                {
                    if (event.keyCode == 38)
                    {
                        patternData.currentPattern.next();
                        _processExternalValidation(context, patternData.currentPattern, 'next');
                        api.refresh.apply(context.internal.state, []);
                    
                        patternData = _getPatternDataAtIndex(context, index);
                        if (patternData.currentPattern) {
                            $.telligent.glow.utility.setCurrentCursorSelection(
                                context.internal.state, 
                                patternData.currentPatternStartIndex, 
                                patternData.currentPatternEndIndex, true);                            
                        }
                    
                        _changed(context);
                    }
                    else if (event.keyCode == 40)
                    {
                        patternData.currentPattern.previous();
                        _processExternalValidation(context, patternData.currentPattern, 'previous');
                        api.refresh.apply(context.internal.state, []);

                        patternData = _getPatternDataAtIndex(context, index);
                        if (patternData.currentPattern) {
                            $.telligent.glow.utility.setCurrentCursorSelection(
                                context.internal.state, 
                                patternData.currentPatternStartIndex, 
                                patternData.currentPatternEndIndex, true);                            
                        }
                    
                        _changed(context);
                    }
                    else if (event.keyCode == 9)
                    {
                        if (event.shiftKey)
                        {
                            if (patternData.previousPattern) {
                                $.telligent.glow.utility.setCurrentCursorSelection(
                                    context.internal.state, patternData.previousPatternStartIndex, 
                                    patternData.previousPatternEndIndex, true, 
                                    context.internal.state.val());                                
                            } else {
                                return true;                                
                            }
                        }
                        else
                        {
                            if (patternData.nextPattern) {
                                $.telligent.glow.utility.setCurrentCursorSelection(
                                    context.internal.state, 
                                    patternData.nextPatternStartIndex, 
                                    patternData.nextPatternEndIndex, true, 
                                    context.internal.state.val());                                
                            } else {
                                return true;                                
                            }
                        }
                    }
                    else if (event.keyCode == 37 || event.keyCode == 8)
                    {
                        if (index < patternData.currentPatternStartIndex && patternData.previousPattern) {
                            $.telligent.glow.utility.setCurrentCursorSelection(
                                context.internal.state, 
                                patternData.previousPatternEndIndex - 1, 
                                patternData.previousPatternEndIndex, true, 
                                context.internal.state.val());                            
                        } else if (index > patternData.currentPatternStartIndex && index <= patternData.currentPatternEndIndex) {
                            $.telligent.glow.utility.setCurrentCursorSelection(
                                context.internal.state, 
                                index, patternData.currentPatternEndIndex, true, 
                                context.internal.state.val());
                        } else {
                            $.telligent.glow.utility.setCurrentCursorSelection(
                                context.internal.state, 
                                patternData.currentPatternStartIndex, 
                                patternData.currentPatternEndIndex, true, 
                                context.internal.state.val());
                        }
                    }
                    else if (event.keyCode == 39)
                    {
                        if (index > patternData.currentPatternEndIndex && patternData.nextPattern) {
                            $.telligent.glow.utility.setCurrentCursorSelection(
                                context.internal.state, 
                                patternData.nextPatternStartIndex, 
                                patternData.nextPatternEndIndex, true, 
                                context.internal.state.val());                            
                        } else if (index > patternData.currentPatternStartIndex && index <= patternData.currentPatternEndIndex) {
                            $.telligent.glow.utility.setCurrentCursorSelection(
                                context.internal.state, index, 
                                patternData.currentPatternEndIndex, true, 
                                context.internal.state.val());
                        } else {
                            $.telligent.glow.utility.setCurrentCursorSelection(context.internal.state, 
                                patternData.currentPatternStartIndex, 
                                patternData.currentPatternEndIndex, true, 
                                context.internal.state.val());
                        }
                    } else {
                        $.telligent.glow.utility.setCurrentCursorSelection(
                            context.internal.state, index, index, true, 
                            context.internal.state.val());
                    }
                } else {
                    $.telligent.glow.utility.setCurrentCursorSelection(
                        context.internal.state, index, index, true, 
                        context.internal.state.val());                    
                }
                
                event.stopPropagation();
            
                return false;
            } else {
                return true;                
            }
        },
        _keyPress = function (context, event)
        {
            /* this doesn't really do anything, but, without it, Opera doesn't work :) */
        
            if (!context.internal.initialized)
            {
                event.stopPropagation();            
                return false;
            }
        
            return true;
        },
        _keyUp = function (context, event)
        {
            if (!context.internal.initialized)
            {
                event.stopPropagation();
            
                return false;
            }
        
            if (event.keyCode != 38 && event.keyCode != 40 && event.keyCode != 37 && event.keyCode != 39 && event.keyCode != 9 && event.keyCode != 8)
            {
                var index = $.telligent.glow.utility.getCurrentCursorIndex(context.internal.state);

                api.val.apply(context.internal.state, [context.internal.state.val()]);
            
                var patternData = _getPatternDataAtIndex(context, index);
            
                if (patternData.currentPattern)
                {
                    if (index > patternData.currentPatternStartIndex && index <= patternData.currentPatternEndIndex) {
                        $.telligent.glow.utility.setCurrentCursorSelection(
                            context.internal.state, index, 
                            patternData.currentPatternEndIndex, true);                        
                    } else {
                        $.telligent.glow.utility.setCurrentCursorSelection(
                            context.internal.state, 
                            patternData.currentPatternStartIndex, 
                            patternData.currentPatternEndIndex, true);                        
                    }
                } else {
                    $.telligent.glow.utility.setCurrentCursorSelection(
                        context.internal.state, index, index, true);                    
                }
            
                _changed(context);
            }
        
            return true;
        };
    
    var api = 
    {
        getPatternValues : function()
        {
            var context = this.data(_dataKey);
            if (context)
            {
                var values = [];
                for (var i = 0; i < context.internal.patterns.length; i++)
                {
                    if (!context.internal.patterns[i].isStatic())
                    {
                        values[values.length] = context.internal.patterns[i].getValue();
                    }
                }
                
                return values;
            }
            
            return null;
        },
        getPatternValueOptionIndex: function(patternIndex, value)
        {
            var context = this.data(_dataKey);
            if (context !== null)
            {
                var pattern = _getPatternByIndex(context, patternIndex);
                if (pattern !== null && pattern._patternType == 'option')
                {
                    value = value.toLowerCase();
                    for (var i = 0; i < pattern._options.length; i++)
                    {
                        if (pattern._options[i].toLowerCase() == value) {
                            return i;                            
                        }
                    }
                }
            }
            
            return undefined;
        },
        getPatternValueOption: function(patternIndex, optionIndex)
        {
            var context = this.data(_dataKey);
            if (context !== null)
            {
                var pattern = _getPatternByIndex(context, patternIndex);
                if (pattern !== null && 
                    pattern._patternType == 'option' && 
                    optionIndex >= 0 && 
                    optionIndex < pattern._options.length) {
                        return pattern._options[optionIndex];                         
                    }
            }
            
            return undefined;
        },
        setPatternValues : function(values)
        {
            return this.each(function()
            {
                var context = $(this).data(_dataKey);
                
                var j = 0;
                for (var i = 0; i < context.internal.patterns.length && j < values.length; i++)
                {
                    if (!context.internal.patterns[i].isStatic())
                    {
                        if (context.internal.patterns[i]._patternType == 'range')
                        {
                            var tempValue = String(values[j]);
                            while (tempValue.length < context.internal.patterns[i]._minLength) {
                                tempValue = "0" + tempValue;                                
                            }
                            
                            context.internal.patterns[i].parseValue(tempValue);             
                        }
                        else {
                            context.internal.patterns[i].parseValue(values[j]);                            
                        }

                        j++;
                    }
                }
                
                api.refresh.apply($(this), []);
            });
        },
        refresh : function()
        {
            return this.each(function()
            {
                var context = $(this).data(_dataKey);
                
                if (context.internal.initialized)
                {
                    var value = '';
                    for (var i = 0; i < context.internal.patterns.length; i++)
                    {
                        value += context.internal.patterns[i].getValue();
                    }
                    
                    context.internal.state.val(value);
                }
            });
        },
        val : function(value)
        {
            if (value === undefined)
            {
                var context = this.data(_dataKey);
                if (context)
                {
                    if (context.internal.initialized)
                    {
                        var value = '';
                        for (var i = 0; i < context.internal.patterns.length; i++)
                        {
                            if (!context.internal.patterns[i].isStatic() && !context.internal.patterns[i].hasValue()) {
                                return null;                                
                            } else {
                                value += context.internal.patterns[i].getValue();                                
                            }
                        }       
                        
                        return value;
                    }
                }

                return null;
            }
            else
            {
                return this.each(function()
                {
                    var context = $(this).data(_dataKey);
                
                    if (!context.internal.initialized) {
                        return;                        
                    }
                    
                    var matches = context.internal.matchRE.exec(value);
                    if (matches)
                    {
                        for (var i = 1; i < matches.length && i - 1 < context.internal.patterns.length; i++)
                        {
                            context.internal.patterns[i - 1].parseValue(matches[i]);
                        }
                        
                        _processExternalValidation(context);
                    }
                    
                    api.refresh.apply($(this), []);
                });
            }
        },
        disabled : function(disable)
        {
            if (disable === undefined)
            {
                var context = this.data(_dataKey);
                if (context) {
                    return context.internal.state.is('input:disabled');
                }
                
                return false;
            }
            else
            {
                return this.each(function()
                {
                    var context = $(this).data(_dataKey);
                
                    if (disable)
                    {   
                        context.internal.state
                            .unbind('keypress' + _eventNameSpace)
                            .unbind('keyup' + _eventNameSpace)
                            .unbind('focus' + _eventNameSpace)
                            .unbind('keydown' + _eventNameSpace)
                            .unbind('change' + _eventNameSpace)
                            .attr('disabled', 'disabled');
                    }
                    else
                    {
                    	context.internal.state
                    		.unbind('keypress' + _eventNameSpace)
                            .unbind('keyup' + _eventNameSpace)
                            .unbind('focus' + _eventNameSpace)
                            .unbind('keydown' + _eventNameSpace)
                            .unbind('change' + _eventNameSpace);
                    	
                        if ($.telligent.glow.utility.isOpera()) {
                            context.internal.state
                                .bind('keypress' + _eventNameSpace, function(event) {
                                    _keyPress(context, event);
                                });
                        }

                        context.internal.state
                            .bind('keyup' + _eventNameSpace, function(event) {
                                    _keyUp(context, event);
                                })
                            .bind('focus' + _eventNameSpace, function(event) {
                                    _focus(context, event);
                                })
                            .bind('keydown' + _eventNameSpace, function(event) {
                                    _keyDown(context, event);
                                })
                            .bind('change' + _eventNameSpace, function(event) {
                                    _change(context, event);
                                })
                            .removeAttr('disabled');
                    }
                });
            }
        }
    };
    
    var textPattern = function(pattern, blankCharacter, allowBlanks, isStatic)
    {
        this._minLength = 0;
        this._maxLength = 0;
        this._options = [];
        this._rangeStart = 0;
        this._rangeEnd = 0;
        this._patternType = '';
        this._currentValue = '';
        this._placeholder = blankCharacter;
        this._allowBlanks = allowBlanks;
        
        // patternType = 'static', 'range', 'option'
        
        if (isStatic)
        {
            this._patternType = 'static';
            this._minLength = this._maxLength = pattern.length;
            this._currentValue = pattern;
        }
        else if (pattern.match(/^[0-9]{1,}\-[0-9]{1,}$/))
        {
            var range = pattern.split('-');
            this._rangeStart = parseInt(range[0], 10);
            this._rangeEnd = parseInt(range[1], 10);
            this._patternType = 'range';
            this._minLength = String(range[0]).length;
            this._maxLength = String(range[1]).length;
            
            if (!this._allowBlanks)
            {
                this._currentValue = String(this._rangeStart);
                while (this._currentValue.length < this._minLength) {
                    this._currentValue = "0" + this._currentValue;                    
                }
            }
        }
        else
        {
            this._options = pattern.split(',');
            this._patternType = 'option';
            this._minLength = String(this._options[0]).length;
            this._maxLength = String(this._options[0]).length;
            for (var i = 0; i < this._options.length; i++)
            {
                var length = String(this._options[i]).length;
                if (length > this._maxLength) {
                    this._maxLength = length;                    
                }
                
                if (length < this._minLength) {
                    this._minLength = length;                    
                }
            }
            
            if (!this._allowBlanks) {
                this._currentValue = this._options[0];                
            }
        }
    };
    
    $.extend(textPattern.prototype, {
        isStatic: function()
        {
            return this._patternType == 'static';
        },

        next: function()
        {
            if (this._patternType == 'static')
            {
                // cannot change static content :)
            }
            else if (this._patternType == 'range')
            {
                if (this._currentValue === '')
                {
                    this._currentValue = String(this._rangeStart);
                    while (this._currentValue.length < this._minLength) {
                        this._currentValue = "0" + this._currentValue;                    
                    }
                }
                else
                {
                    var value = parseInt(this._currentValue, 10);
                    if (value < this._rangeEnd)
                    {
                        this._currentValue = String(value + 1);
                        while (this._currentValue.length < this._minLength) {
                            this._currentValue = "0" + this._currentValue;                        
                        }
                    }
                    else
                    {
                        this._currentValue = String(this._rangeStart);
                        while (this._currentValue.length < this._minLength) {
                            this._currentValue = "0" + this._currentValue;                        
                        }
                    }
                }
            }
            else if (this._patternType == 'option')
            {
                if (this._currentValue == '') {
                    this._currentValue = this._options[0];                
                }
                else
                {
                    for (var i = 0; i < this._options.length; i++)
                    {
                        if (this._options[i] == this._currentValue)
                        {
                            if (i < this._options.length - 1) {
                                this._currentValue = String(this._options[i + 1]);                            
                            }
                            else {
                                this._currentValue = String(this._options[0]);                            
                            }

                            break;
                        }
                    }
                }
            }
        },

        previous: function()
        {
            if (this._patternType == 'static')
            {
                // cannot change static content :)
            }
            else if (this._patternType == 'range')
            {
                if (this._currentValue === '')
                {
                    this._currentValue = String(this._rangeEnd);
                    while (this._currentValue.length < this._minLength) {
                        this._currentValue = "0" + this._currentValue;                    
                    }
                }
                else
                {
                    var value = parseInt(this._currentValue, 10);
                    if (value > this._rangeStart)
                    {
                        this._currentValue = String(value - 1);
                        while (this._currentValue.length < this._minLength) {
                            this._currentValue = "0" + this._currentValue;                        
                        }
                    }
                    else
                    {
                        this._currentValue = String(this._rangeEnd);
                        while (this._currentValue.length < this._minLength) {
                            this._currentValue = "0" + this._currentValue;                        
                        }
                    }
                }
            }
            else if (this._patternType == 'option')
            {
                if (this._currentValue === '') {
                    this._currentValue = this._options[this._options.length - 1];                
                } else {
                    for (var i = 0; i < this._options.length; i++)
                    {
                        if (this._options[i] == this._currentValue)
                        {
                            if (i > 0) {
                                this._currentValue = String(this._options[i - 1]);                            
                            } else {
                                this._currentValue = String(this._options[this._options.length - 1]);                            
                            }

                            break;
                        }
                    }
                }
            }
        },

        parseValue: function(value)
        {
            if (this._patternType == 'static') {
                return;            
            }

            value = String(value).replace(new RegExp($.telligent.glow.utility.escapeForRegExp(this._placeholder), 'g'), '');

            if (this._allowBlanks && value === '' && isNaN(parseInt(value, 10)))
            {
                this._currentValue = '';
                return this._currentValue;
            }
            else if (this._patternType == 'range')
            {
                if (value.length > this._maxLength) {
                    value = value.substr(0, this._maxLength);                
                } else {
                    while (value.length < this._minLength)
                    {
                        value += "0";
                    }
                }

                value = parseInt(value, 10);
                if (isNaN(value)) {
                    value = 0;                
                }

                var foundValue = false;
                var newValue;

                // value is in range
                if (value >= this._rangeStart && value <= this._rangeEnd)
                {
                    newValue = value;
                    foundValue = true;
                }

                // value is below range, multiply by 10 to get in range         
                if (!foundValue && value < this._rangeStart && value > 0)
                {
                    var tempValue = value;
                    while (tempValue < this._rangeStart && (String(tempValue)).length <= this._maxLength && !foundValue)
                    {
                        tempValue *= 10;
                        if (tempValue >= this._rangeStart && tempValue <= this._rangeEnd)
                        {
                            newValue = tempValue;
                            foundValue = true;
                        }
                    }
                }

                // value is above range, truncate right-most digits
                if (!foundValue && value > this._rangeEnd && this._rangeEnd !== 0)
                {
                    var tempValue = value;
                    while (tempValue > this._rangeEnd && tempValue >= this._rangeStart && !foundValue)
                    {
                        tempValue = Math.round(tempValue / 10);
                        if (tempValue >= this._rangeStart && tempValue <= this._rangeEnd)
                        {
                            newValue = tempValue;
                            foundValue = true;
                        }
                    }
                }

                // set value to range start or end
                if (!foundValue)
                {
                    if (value < this._rangeStart) {
                        newValue = this._rangeStart;                    
                    } else {
                        newValue = this._rangeEnd;                    
                    }
                }

                this._currentValue = String(newValue);
                while (this._currentValue.length < this._minLength) {
                    this._currentValue = "0" + this._currentValue;                
                }

                return this._currentValue;
            }
            else if (this._patternType == 'option')
            {
                value = value.toLowerCase();

                while (value.length > 0)
                {
                    for (var i = 0; i < this._options.length; i++)
                    {
                        if (this._options[i].substr(0, value.length).toLowerCase() == value)
                        {
                            this._currentValue = String(this._options[i]);
                            return this._currentValue;
                        }
                    }

                    if (value.length > 0) {
                        value = value.substr(0, value.length - 1);                    
                    }
                }

                return this._currentValue;
            }
        },

        getValue: function()
        {
            if (this._patternType == 'static')
            {
                return this._currentValue;
            }
            else
            {
                if (this._currentValue === '' && isNaN(parseInt(this._currentValue, 10)))
                {
                    var value = '';
                    while (value.length < this._minLength)
                    {
                        value += this._placeholder;
                    }

                    return value;
                } else {
                    return this._currentValue;                
                }
            }
        },

        hasValue: function()
        {
            return !(this._currentValue === '' && isNaN(parseInt(this._currentValue, 10)));
        },

        getRegExpPattern: function()
        {
            if (this._patternType == 'range') {
                return '[0-9' + $.telligent.glow.utility.escapeForRegExp(this._placeholder) + ']{0,' + (this._maxLength + 1) + '}';            
            } else if (this._patternType == 'option') {
                return '.{0,' + (this._maxLength + 1) + '}';                    
            } else {
                return $.telligent.glow.utility.escapeForRegExp(this._currentValue);            
            }
        }        
    });
    
    $.fn.glowPatternedTextBox = function(method) 
    {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowPatternedTextBox');
        }
    };  
    $.extend($.fn.glowPatternedTextBox, {
        defaults: {
            pattern:'',
            allowBlanks:true,
            blankCharacter:'_',
            onValidation:null
        }       
    });
    // events 
    //   glowPatternedTextBoxChange
})(jQuery);
 
 
/// @name glowPopUpMenu
/// @category jQuery Plugin
/// @description Renders a nested pop up menu
/// 
/// ### jQuery.fn.glowPopUpMenu
/// 
/// Renders a nested pop up menu
/// 
/// ### Usage
/// 
///     $('SELECTOR').glowPopUpMenu(options)
/// 
/// where 'SELECTOR' is a div element to be used as a container for the menu
/// 
/// ### Options
/// 
///  * `groupCssClass`: Class applied to a set of menu options
///    * default: *empty string*
///  * `itemCssClass`: Class applied to an individual menu option
///    * default: *empty string*
///  * `itemSelectedCssClass`: Class applied to a selected individual menu option
///    * default: *empty string*
///  * `itemExpandedCssClass`: Class applied to a selected individual menu option when navigating the item's children
///    * default: *empty string*
///  * `expandImageUrl`: Image URL path to image displayed when an option has children
///    * default: *empty string*
///  * `expandImageWidth`: Width of the expand image
///    * default: `0`
///  * `iconWidth`: Width of icons shown in menu options
///    * default: `0`
///  * `iconHeight`: Height of icons shown in menu options
///    * default: `0`
///  * `position`: PopupPanel opening direction to use. any combination of left, right, up, down
///    * default: `updown`
///  * `zIndex`: z-index at which to render the panel
///    * default: `100`
///  * `menuItems`: Nested array of menu options.  Each menu option is, itself, represented by an array of the format:
///    0. id: unique id
///    1. text: display text
///    2. url: when provided, will redirect to URL when clicked
///    3. target: target in which to open URL
///    4. callback: optional callback to execute when clicked
///    5. icon url
///    6. childItems: optional array of child menu items
///  * `closeOnMouseOut`: when true, the menu closes upon mouseout
///    * default: `true`
/// 
/// ### Events
/// 
///  * `glowPopUpMenuOpened` - triggered when a menu is opened
///  * `glowPopUpMenuClosed` - triggered when a menu is closed
///  * `glowPopUpMenuItemOver` - triggered when mouse is over a menu option
/// 
/// ### Methods
/// 
/// #### isOpen
/// 
/// Returns whether the menu is currently open
/// 
///     var isOpen = $('SELECTOR').glowPopUpPanel('isOpen');
/// 
/// #### open
/// 
/// Opens an instance of a popup menu.
/// 
///     $('SELECTOR').glowPopUpPanel('open');
/// 
/// #### close
/// 
/// Closes an instance of a popup menu.
/// 
///     $('SELECTOR').glowPopUpPanel('close');
/// 
/// #### refresh
/// 
/// Refreshes the contents of a glowPopUpMenu after its options have been programmatically changes
/// 
///     $('SELECTOR').glowPopUpPanel('refresh');
/// 
/// #### add
/// 
/// Adds a new menu item to the end of the menu
/// 
///     var menuItem = $('SELECTOR').glowPopUpMenu('createMenuItem', {
///         id: 'new',
///         text: 'This is a new item!',
///         onClick: functino(){ alert("I'm new"); }
///     });
///     $('SELECTOR').glowPopUpMenu('add', menuItem);
/// 
/// #### remove
/// 
/// Removes an instance of a menu item from the menu
/// 
///     var menuItem = $('SELECTOR').glowPopUpMenu('getById', 5);
///     $('SELECTOR').glowPopUpMenu('remove', menuItem);    
/// 
/// #### insert
/// 
/// Adds a new menu item instance to the menu at a given index
/// 
///     var menuItem = $('SELECTOR').glowPopUpMenu('createMenuItem', {
///         id: 'new',
///         text: 'This is a new item!',
///         onClick: functino(){ alert("I'm new"); }
///     });
///     $('SELECTOR').glowPopUpMenu('insert', menuItem, 4);
/// 
/// #### clear
/// 
/// Clears a menu of all options
/// 
///     $('SELECTOR').glowPopUpMenu('clear');
/// 
/// #### getById
/// 
/// Returns a menu item by its ID
/// 
///     var item = $('SELECTOR').glowPopUpMenu('getById', 4);
/// 
/// #### getByText
/// 
/// Returns a menu item by its display text
/// 
///     var item = $('SELECTOR').glowPopUpMenu('getById', 'Menu Option');
/// 
/// #### getByIndex
/// 
/// Returns a menu item by its index
/// 
///     var item = $('SELECTOR').glowPopUpMenu('getByIndex', 3);
/// 
/// #### getCurrent
/// 
/// Returns the currently-selected menu item
/// 
///     var item = $('SELECTOR').glowPopUpMenu('getCurrent');
/// 
/// #### getCurrentByLevel
/// 
/// Returns the currently-selected menu item at a given menu level
/// 
///     var item = $('SELECTOR').glowPopUpMenu('getCurrentByLevel', 2);
/// 
/// #### count
/// 
/// Returns the number of menu items in a menu
/// 
///     var count = $('SELECTOR').glowPopUpMenu('count');
/// 
/// #### createMenuItem
/// 
/// Creates a new menu item object suitable for adding or inserting into a menu    
/// 
///     var menuItem = $('SELECTOR').glowPopUpMenu('createMenuItem', {
///         id: 'new',
///         text: 'This is a new item!',
///         onClick: functino(){ alert("I'm new"); }
///     });
/// 
/// 
/*
menuItems = array[index]
    [0] = id
    [1] = text
    [2] = url
    [3] = target
    [4] = javascript
    [5] = icon url
    [6] = childItems
*/

(function($){

    var _eventNames = {
            opened: 'glowPopUpMenuOpened',
            closed: 'glowPopUpMenuClosed',
            itemOver: 'glowPopUpMenuItemOver'
        },
        _init = function(options) 
        {
            return this.each(function() 
            {               
                var context = {
                    settings: $.extend({}, $.fn.glowPopUpMenu.defaults, options || {}),
                    internal: {
                        state: this,
                        menuItems: new Array(),
                        menuLevels: new Array(),
                        initialized: false,
                        currentLevel: -1,
                        isOpen: false,
                        cancelClick: false,
                        openingElement: null,
                        closeTimeout: null
                    }
                };                  
            
                $(this).data('popUpMenu', context);
                _parseMenuItems(context, context.settings.menuItems, null);
            });
        },  
        _parseMenuItems = function(context, menuItems)
        {
            this._menuItems = new Array();
            if (!menuItems || menuItems.length == 0)
                return;

            for (var i = 0; i < menuItems.length; i++)
            {
                context.internal.menuItems[i] = new PopUpMenuItem({'id':menuItems[i][0],'text':menuItems[i][1],'url':menuItems[i][2],'target':menuItems[i][3],'onClick':menuItems[i][4],'iconUrl':menuItems[i][5]});
                context.internal.menuItems[i]._popupMenu = $(this);
                if (menuItems[i][6] && menuItems[i][6].length > 0)
                    context.internal.menuItems[i]._parseMenuItems(menuItems[i][6]);
            }
        },
        _disposeLevel= function(context, menuLevel)
        {
            // remove all circular references involving DOM nodes within this level
            if (menuLevel && menuLevel._popupPanel)
            {
                var nodes = menuLevel._popupPanel.glowPopUpPanel('children');
                if (nodes[0] && nodes[0].childNodes[0])
                {
                    var tbody = nodes[0].childNodes[0];
                    for (var j = 0; j < tbody.childNodes.length; j++)
                    {
                        if (tbody.childNodes[j].childNodes[0] && tbody.childNodes[j].childNodes[0]._menuItem)
                        {
                            tbody.childNodes[j].childNodes[0]._menuItem._element = null;
                            tbody.childNodes[j].childNodes[0]._menuItem = null;
                            tbody.childNodes[j].childNodes[0].onmouseover = null;
                            tbody.childNodes[j].childNodes[0].onclick = null;
                        }
                    }
                }
            }
        },
        _registerDocumentOnClick= function(context)
        {
            $(document).bind('click.popUpMenu', function() { _documentOnClick(context); });
        },
        _mouseOut= function(context)
        {
            if (context.settings.closeOnMouseOut)
            {
                window.clearTimeout(context.internal.closeTimeout); 
                context.internal.closeTimeout = window.setTimeout(function() { api.close.apply($(context.internal.state)); }, 249);
            }
        },
        _mouseOver= function(context)
        {
            if (context.settings.closeOnMouseOut)
                window.clearTimeout(context.internal.closeTimeout);
        },
        _itemClick= function(context, menuItem, level)
        {
            context.internal.cancelClick = true;
        
            if (menuItem.onClick || menuItem.url)
                api.close.apply($(context.internal.state));
        
            // execute the client script, if defined
            if (menuItem.onClick)
                menuItem.onClick(menuItem);
            
            // navigate to the navigate url, if defined
            if (menuItem.url)
            {
                if (!menuItem.target)
                    window.location = menuItem.url;
                else
                    window.open(menuItem.url, menuItem.target);
            }
        
            return false;
        },
        _itemMouseOver= function(context, menuItem, level, ignoreHideAndAnimation)
        {
            if (context.internal.menuLevels.length <= level || (context.internal.currentLevel == level && context.internal.menuLevels[level]._currentMenuItem == menuItem && !ignoreHideAndAnimation))
                return;

            if (context.internal.menuLevels[level]._currentMenuItem != menuItem)
            {
                if (!ignoreHideAndAnimation)
                {
                    // close all levels above the current one
                    api.close.apply($(context.internal.state), [(level + 1)]);
                }
            
                // update the css class of the previously selected item (if different)
                if (context.internal.menuLevels[level]._currentMenuItem && context.internal.menuLevels[level]._currentMenuItem != menuItem)
                    context.internal.menuLevels[level]._currentMenuItem._element.className = context.settings.itemCssClass;

                // mark the "parent" item as expanded
                if (level > 0 && context.internal.menuLevels[level - 1]._currentMenuItem)
                    context.internal.menuLevels[level - 1]._currentMenuItem._element.className = context.settings.itemExpandedCssClass;
            
                // update the current menu item css class and status
                menuItem._element.className = context.settings.itemSelectedCssClass;
                context.internal.menuLevels[level]._currentMenuItem = menuItem;

                // expand the item's sub-menu, if it exists
                if (menuItem._menuItems != null && menuItem._menuItems.length > 0)
                {
                    if (context.internal.menuLevels.length == level + 1)
                    {
                        var panel = document.createElement('div');
                        context.internal.state.appendChild(panel);          
                        $(panel)
                            .glowPopUpPanel({
                                'cssClass':context.settings.groupCssClass,
                                'position':'leftright',
                                'zIndex':context.settings.zIndex,
                                'hideOnDocumentClick':false
                                })
                            .bind({
                                glowPopUpPanelHidden: $.telligent.glow.utility.makeBoundFunction(api.close, $(context.internal.state), [level + 1]),
                                glowPopUpPanelMouseOver: function() { _mouseOver(context); },
                                glowPopUpPanelMouseOut: function() { _mouseOut(context); }                              
                                });
                        context.internal.menuLevels[level + 1] = new PopUpMenuLevel(level + 1, menuItem, $(panel), this);
                    }
                    else
                        context.internal.menuLevels[level + 1].MenuItem = menuItem;

                    _populatePopupPanel(context, context.internal.menuLevels[level + 1], menuItem._menuItems);
                    context.internal.menuLevels[level + 1]._popupPanel.glowPopUpPanel('show', menuItem._element, ignoreHideAndAnimation);
                    context.internal.currentLevel = level + 1;      
                }
            
                $(context.internal.state).trigger(_eventNames.itemOver);
            }
            else
            {
                if (!ignoreHideAndAnimation)
                    api.close.apply($(context.internal.state), [(level + 2)]);
                        
                if (context.internal.menuLevels[level + 1] && context.internal.menuLevels[level + 1]._currentMenuItem)
                {
                    context.internal.menuLevels[level + 1]._currentMenuItem._element.className = context.settings.itemCssClass;
                    context.internal.menuLevels[level + 1]._currentMenuItem = null;
                }
            
                context.internal.menuLevels[level]._currentMenuItem._element.className = context.settings.itemSelectedCssClass;
            }
        },
        _initialize= function(context)
        {
            context.internal.menuLevels = new Array();

            var panel = document.createElement('div');
            $(panel)
                .glowPopUpPanel({
                    cssClass: context.settings.groupCssClass,
                    position: context.settings.position,
                    zIndex: context.settings.zIndex,
                    hideOnDocumentClick: false })
                .bind({
                    glowPopUpPanelHidden: function() { api.close.apply($(context.internal.state), [0]); },
                    glowPopUpPanelMouseOver: function() { _mouseOver(context); },
                    glowPopUpPanelMouseOut: function() { _mouseOut(context); }                  
                    });
            context.internal.menuLevels[0] = new PopUpMenuLevel(0, null, $(panel), this);
            _populatePopupPanel(context, context.internal.menuLevels[0], context.internal.menuItems);
            context.internal.initialized = true;
        },
        _populatePopupPanel= function(context, menuLevel, menuItems)
        {
            _disposeLevel(context, menuLevel);

            menuLevel._popupPanel.glowPopUpPanel('empty');
            menuLevel._currentMenuItem = null;
            if (!menuItems || menuItems.length == 0)
                return;
        
            var outerTable = document.createElement('table');
            outerTable.cellPadding = '0';
            outerTable.cellSpacing = '0';
            outerTable.appendChild(document.createElement('tbody'));
        
            var i;
            var hasIcons = false;
            var hasExpandable = false;
            for (i = 0; i < menuItems.length; i++)
            {
                if (menuItems[i].iconUrl)
                    hasIcons = true;

                if (context.settings.expandImageUrl && menuItems[i]._menuItems && menuItems[i]._menuItems.length > 0)
                    hasExpandable = true;
            
                if (hasIcons && hasExpandable)
                    break;
            }
        
            for (i = 0; i < menuItems.length; i++)
            {
                var outerRow = document.createElement('tr');
                outerTable.childNodes[0].appendChild(outerRow);

                var container = document.createElement('td');
                container.className = context.settings.itemCssClass;
                container.id = menuItems[i].id;
                container._menuItem = menuItems[i];
                outerRow.appendChild(container);
            
                var innerTable = document.createElement('table');
                innerTable.cellPadding = '0';
                innerTable.cellSpacing = '0';
                innerTable.style.width = '100%';
                innerTable.appendChild(document.createElement('tbody'));
                container.appendChild(innerTable);
        
                var row = document.createElement('tr');
                innerTable.childNodes[0].appendChild(row);
            
                var cell;
            
                if (hasIcons)
                {
                    cell = document.createElement('td');
                    if (menuItems[i].iconUrl)
                    {
                        var img = document.createElement('img');
                        img.src = menuItems[i].iconUrl;
                        img.style.paddingRight = '4px';
                    
                        if (context.settings.iconHeight)
                            img.height = context.settings.iconHeight;
                    
                        if (context.settings.iconWidth)
                            img.width = context.settings.iconWidth;
                    
                        cell.appendChild(img);
                    }
                    else
                    {
                        cell.appendChild(document.createElement('div'));
                        cell.childNodes[0].style.paddingRight = '4px';
                    
                        if (context.settings.iconHeight)
                            cell.childNodes[0].style.height = context.settings.iconHeight + 'px';
                    
                        if (context.settings.iconWidth)
                            cell.childNodes[0].style.width = context.settings.iconWidth + 'px';
                    }
                
                    row.appendChild(cell);
                }
            
                cell = document.createElement('td');
                cell.style.whiteSpace = 'nowrap';
                cell.width = '100%';
                cell.innerHTML = menuItems[i].text;
                row.appendChild(cell);

                if (hasExpandable)
                {
                    cell = document.createElement('td');            
                    if (context.settings.expandImageUrl && menuItems[i]._menuItems && menuItems[i]._menuItems.length > 0)
                    {
                        var img = document.createElement('img');
                        img.src = context.settings.expandImageUrl;
                        img.style.paddingLeft = '4px';
                    
                        if (context.settings.expandImageHeight)
                            img.height = context.settings.expandImageHeight;
                    
                        if (context.settings.expandImageWidth)
                            img.width = context.settings.expandImageWidth;
                    
                        cell.appendChild(img);
                    }
                    else
                    {
                        cell.appendChild(document.createElement('div'));
                        cell.childNodes[0].style.paddingRight = '4px';
                    
                        if (context.settings.expandImageHeight)
                            cell.childNodes[0].style.height = context.settings.expandImageHeight + 'px';
                    
                        if (context.settings.expandImageWidth)
                            cell.childNodes[0].style.width = context.settings.expandImageWidth + 'px';
                    }           
                
                    row.appendChild(cell);
                }

                container.onclick = $.telligent.glow.utility.makeBoundFunction(_itemClick, null, [context, menuItems[i], menuLevel._level]);
                container.onmouseover = $.telligent.glow.utility.makeBoundFunction(_itemMouseOver, null, [context, menuItems[i], menuLevel._level]);
                menuItems[i]._element = container;          
            }
        
            menuLevel._popupPanel.glowPopUpPanel('append', outerTable);
        },
        _documentOnClick= function(context)
        {
            if (context.internal.isOpen && !context.internal.menuLevels[context.internal.currentLevel]._popupPanel.glowPopUpPanel('isOpening') && !context.internal.cancelClick)
                api.close.apply($(context.internal.state), [0]);
            else
                context.internal.cancelClick = false;
        },
        _getLevel= function(context, level)
        {
            if (level > 0 && level >= context.internal.currentLevel)
                return context.internal.menuLevels[level];
            else
                return null;
        };
    
    var api = 
    {
        isOpen: function()
        {
            var context = this.data('popUpMenu');
            if (context)
                return context.internal.isOpen;
            
            return false;
        },
        open: function()
        {
            var x, y, positionWidth, positionHeight, openingElement;
            
            if (typeof arguments[0] === 'object')
            {
                openingElement = arguments[0];
                var elementInfo = $.telligent.glow.utility.getElementInfo(arguments[0]);
                x = elementInfo.Left;
                y = elementInfo.Top;
                positionWidth = elementInfo.Width;
                positionHeight = elementInfo.Height;
            }
            else
            {
                openingElement = null;
                x = arguments[0];
                y = arguments[1];
                positionWidth = arguments[2];
                positionHeight = arguments[3];
            }
            
            return this.each(function() 
            {
                var context = $(this).data('popUpMenu');
                
                if (!context.internal.initialized)
                    _initialize(context);

                if (!context.internal.isOpen)
                    window.setTimeout(function() { _registerDocumentOnClick(context); }, 49);

                context.internal.menuLevels[0]._popupPanel.glowPopUpPanel('show', x, y, positionWidth, positionHeight);
                context.internal.currentLevel = 0;
                context.internal.isOpen = true;
                context.internal.openingElement = openingElement;
                if (openingElement)
                {
                    $(openingElement).bind('mouseover.popUpMenu', function() { _mouseOver(context); });
                    $(openingElement).bind('mouseout.popUpMenu', function() { _mouseOut(context); });
                }

                $(context.internal.state).trigger(_eventNames.opened);
            });
        },
        close: function(level)
        {
            // if no level is defined, close the entire menu
            if (!level)
                level = 0;
            
            return this.each(function()
            {
                var context = $(this).data('popUpMenu');
                
                if (level == 0 && context.internal.openingElement)
                {
                    $(context.internal.openingElement).unbind('.popUpMenu');
                    context.internal.openingElement = null;
                }

                for (var i = context.internal.menuLevels.length - 1; i >= level; i--)
                {
                    if (context.internal.menuLevels[i])
                    {
                        if (context.internal.menuLevels[i]._currentMenuItem)
                        {
                            context.internal.menuLevels[i]._currentMenuItem._element.className = context.settings.itemCssClass;
                            context.internal.menuLevels[i]._currentMenuItem = null;
                        }

                        context.internal.menuLevels[i]._popupPanel.glowPopUpPanel('hide');
                    }
                }

                context.internal.currentLevel = level - 1;

                if (context.internal.currentLevel <= -1)
                {
                    if (context.internal.isOpen)
                        $(document).unbind('.popUpMenu');

                    context.internal.isOpen = false;

                    $(context.internal.state).trigger(_eventNames.closed);
                }
            });
        },
        refresh: function()
        {
            return this.each(function()
            {
                var context = $(this).data('popUpMenu');
                
                if (context.internal.initialized)
                {
                    var selectedItems = new Array();
                    var i;
                    for (i = 0; i <= context.internal.currentLevel; i++)
                    {
                        selectedItems[i] = context.internal.menuLevels[i]._currentMenuItem;
                    }
                    
                    _populatePopupPanel(context, context.internal.menuLevels[0], context.internal.menuItems);

                    if (selectedItems.length > 0)
                    {
                        // attempt to reselect the previous menu path
                        
                        var j;
                        var menuItems = context.internal.menuItems;
                        var found;
                        for (i = 0; i < selectedItems.length; i++)
                        {   
                            found = false;
                            for (j = 0; menuItems && j < menuItems.length; j++)
                            {
                                if (selectedItems[i] == menuItems[j])
                                {
                                    // the item still exists, select/expand it
                                    _itemMouseOver(context, menuItems[j], i, true);
                                    menuItems = menuItems[j]._menuItems;
                                    found = true;
                                    break;
                                }
                            }
                            
                            if (!found)
                            {
                                if (menuItems && menuItems.length > 0)
                                    api.close.apply($(this), [i + 1]);
                                else
                                    api.close.apply($(this), [i]);
                                
                                break;
                            }
                        }
                    }
                }
            });
        },  
        add: function(popUpMenuItem)
        {
            if (popUpMenuItem)
            {
                api.remove.apply(this, [popUpMenuItem]);
                var isFirst = true;
                
                return this.each(function()
                {
                    var context = $(this).data('popUpMenu');
                    
                    var item;
                    if (isFirst)
                    {
                        item = popUpMenuItem;
                        isFirst = false;
                    }
                    else
                        item = popUpMenuItem._clone();
                    
                    item._popupMenu = $(this);
                    item._parentMenuItem = null;
                    context.internal.menuItems[context.internal.menuItems.length] = item;
                });
            }
            else
                return this;
        },
        remove: function(popUpMenuItem)
        {
            if (popUpMenuItem)
            {
                return this.each(function()
                {
                    var context = $(this).data('popUpMenu');
                    
                    var menuItems = new Array();
                    var found = false;
                    for (var i = 0; i < context.internal.menuItems.length; i++)
                    {
                        if (context.internal.menuItems[i].id == popUpMenuItem.id)
                            found = true;
                        else
                            menuItems[menuItems.length] = context.internal.menuItems[i];
                    }
                    
                    if (found)
                        context.internal.menuItems = menuItems;
                });
            }
            else
                return this;
        },
        insert: function(popUpMenuitem, index)
        {
            if (popUpMenuItem)
            {
                api.remove.apply(this, [popUpMenuItem]);
                
                return this.each(function()
                {   
                    var menuItems = new Array();
                    var inserted = false;
                    for (var i = 0; i < context.internal.menuItems.length; i++)
                    {
                        if (i == index)
                        {
                            inserted = true;
                            popUpMenuItem._popupMenu = this;
                            popUpMenuItem._parentMenuItem = null;
                            menuItems[menuItems.length] = popUpMenuItem;
                        }
                        
                        menuItems[menuItems.length] = context.internal.menuItems[i];
                    }
                    
                    if (!inserted)
                        menuItems[menuItems.length] = popUpMenuItem;
                    
                    context.internal.menuItems = menuItems;
                });
            }
            else
                return this;
        },
        clear: function()
        {
            return this.each(function()
            {
                var context = $(this).data('popUpMenu');
                context.internal.menuItems = new Array();
            });
        },
        getById: function(id)
        {
            var context = $(this).data('popUpMenu');
            if (context)
            {
                for (var i = 0; i < context.internal.menuItems.length; i++)
                {
                    if (context.internal.menuItems[i].ID == id)
                        return context.internal.menuItems[i];
                    else
                    {
                        var item = context.internal.menuItems[i].getById(id);
                        if (item)
                            return item;
                    }
                }
            }
            
            return null;
        },
        getByText: function(text)
        {
            var context = $(this).data('popUpMenu');
            if (context)
            {
                var items = new Array();
    
                for (var i = 0; i < context.internal.menuItems.length; i++)
                {
                    if (context.internal.menuItems[i].Text == text)
                        items[items.length] = context.internal.menuItems[i];
                    
                    var subitems = context.internal.menuItems[i].getByText(text);
                    for (var j = 0; j < subitems.length; j++)
                        items[items.length] = subitems[j];
                }
                
                return items;
            }
            
            return new Array();
        },
        getByIndex: function(index)
        {
            var context = $(this).data('popUpMenu');
            if (context)
            {
                if (index >= 0 && index < context.internal.menuItems.length)
                    return context.internal.menuItems[index];
                else
                    return null;
            }
            
            return null;
        },
        getCurrent: function()
        {
            var context = $(this).data('popUpMenu');
            if (context)
            {
                if (context.internal.currentLevel > -1)
                {
                    if (context.internal.menuLevels[context.internal.currentLevel]._currentMenuItem)
                        return context.internal.menuLevels[context.internal.currentLevel]._currentMenuItem;
                    else if (context.internal.currentLevel > 0)
                        return context.internal.menuLevels[context.internal.currentLevel - 1]._currentMenuItem;
                }
            }
    
            return null;
        },
        getCurrentByLevel: function(level)
        {
            var context = $(this).data('popUpMenu');
            if (context)
            {
                if (level > 0 && context.internal.currentLevel >= level)
                    return context.internal.menuLevels[level]._currentMenuItem;
            }
            
            return null;
        },
        count: function()
        {
            var context = $(this).data('popUpMenu');
            if (context)
                return context.internal.menuItems.length;
            
            return 0;
        },
        createMenuItem: function(options)
        {
            return new PopUpMenuItem(options);
        }
    }
    
    $.fn.glowPopUpMenu = function(method) 
    {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowPopUpMenu');
        }
    };  
    $.extend($.fn.glowPopUpMenu, {
        defaults: {
            groupCssClass:'',
            itemCssClass:'',
            itemSelectedCssClass:'',
            itemExpandedCssClass:'',
            expandImageUrl:'',
            expandImageWidth:0,
            iconWidth:0,
            iconHeight:0,
            position:'updown',
            zIndex:100,
            menuItems:null,
            closeOnMouseOut:true
        }
    });
    // events
    //   glowPopUpMenuOpened
    //   glowPopUpMenuClosed
    //   glowPopUpMenuItemOver
    
    
    var PopUpMenuItem = function(options)
    {
        var settings = 
        {
            id: (new Date()).getTime(),
            text: '',
            url: '',
            target: '',
            onClick: null,
            iconUrl: ''
        };

        if (options)
            $.extend(settings, options);

        this.id = settings.id;
        this.text = settings.text;
        this.url = settings.url;
        this.target = settings.target;
        this.onClick = settings.onClick;
        this.iconUrl = settings.iconUrl;

        this._menuItems = new Array();
        this._element = null;
        this._popupMenu = null;
        this._parentMenuItem = null;
    }

    PopUpMenuItem.prototype._clone = function()
    {
        return new PopUpMenuItem({'id':this.id,'text':this.text,'url':this.url,'target':this.target,'onClick':this.onClick,'iconUrl':this.iconUrl});
    }

    PopUpMenuItem.prototype.add = function(popUpMenuItem)
    {
        this.remove(popUpMenuItem);
        if (popUpMenuItem)
        {
            popUpMenuItem._popupMenu = this._popupMenu;
            popUpMenuItem._parentMenuItem = this;
            this._menuItems[this._menuItems.length] = popUpMenuItem;
        }
    }

    PopUpMenuItem.prototype.remove = function(popUpMenuItem)
    {
        var menuItems = new Array();
        var found = false;
        for (var i = 0; i < this._menuItems.length; i++)
        {
            if (this._menuItems[i].id == popUpMenuItem.id)
                found = true;
            else
                menuItems[menuItems.length] = this._menuItems[i];
        }

        if (found)
            this._menuItems = menuItems;
    }

    PopUpMenuItem.prototype.clear = function()
    {
        this._menuItems = new Array();
    }

    PopUpMenuItem.prototype.insert = function(popUpMenuItem, index)
    {
        this.remove(popUpMenuItem);

        var menuItems = new Array();
        var inserted = false;
        for (var i = 0; i < this._menuItems.length; i++)
        {
            if (i == index)
            {
                inserted = true;
                popUpMenuItem._popupMenu = this._popupMenu;
                popUpMenuItem._parentMenuItem = this;
                menuItems[menuItems.length] = popUpMenuItem;
            }

            menuItems[menuItems.length] = this._menuItems[i];
        }

        if (!inserted)
            menuItems[menuItems.length] = popUpMenuItem;

        this._menuItems = menuItems;
    }

    PopUpMenuItem.prototype.getById = function(id)
    {
        for (var i = 0; i < this._menuItems.length; i++)
        {
            if (this._menuItems[i].ID == id)
                return this._menuItems[i];
            else
            {
                var item = this._menuItems[i].getById(id);
                if (item)
                    return item;
            }
        }

        return null;
    }

    PopUpMenuItem.prototype.getByText = function(text)
    {
        var items = new Array();

        for (var i = 0; i < this._menuItems.length; i++)
        {
            if (this._menuItems[i].Text == text)
                items[items.length] = this._menuItems[i];

            var subitems = this._menuItems[i].getByText(text);
            for (var j = 0; j < subitems.length; j++)
                items[items.length] = subitems[j];
        }

        return items;
    }

    PopUpMenuItem.prototype.getByIndex = function(index)
    {
        if (index >= 0 && index < this._menuItems.length)
            return this._menuItems[index];
        else
            return null;
    }

    PopUpMenuItem.prototype.getPopUpMenu = function()
    {
        return this._popupMenu;
    }

    PopUpMenuItem.prototype.getParent = function()
    {
        return this._parentMenuItem;
    }

    PopUpMenuItem.prototype.count = function()
    {
        return this._menuItems.length;
    }

    PopUpMenuItem.prototype._parseMenuItems = function(menuItems)
    {
        this._menuItems = new Array();
        if (!menuItems || menuItems.length == 0)
            return;

        for (var i = 0; i < menuItems.length; i++)
        {
            this._menuItems[i] = new PopUpMenuItem({'id':menuItems[i][0],'text':menuItems[i][1],'url':menuItems[i][2],'target':menuItems[i][3],'onClick':menuItems[i][4],'iconUrl':menuItems[i][5]});
             this._menuItems[i]._popupMenu = this;
            if (menuItems[i][6] && menuItems[i][6].length > 0)
                this._menuItems[i]._parseMenuItems(menuItems[i][6]);
        }
    }

    var PopUpMenuLevel = function(level, parentMenuItem, popupPanel, popupMenu)
    {
        this._level = level;
        this._parentMenuItem = parentMenuItem;
        this._popupPanel = popupPanel;
        this._popupMenu = popupMenu;
        this._currentMenuItem = null;
    };  
    
})(jQuery); 
 
/// @name glowPopUpPanel
/// @category jQuery Plugin
/// @description Renders an animated display panel on top of the document
/// 
/// ### jQuery.fn.glowPopUpPanel
/// 
/// Renders an animated display panel on top of the document
/// 
/// ### Usage
/// 
///     $('SELECTOR').glowPopUpPanel(options)
/// 
/// where 'SELECTOR' is a div element containing content to display as a pop up panel
/// 
/// ### Options
/// 
///  * `cssClass`: string class name to apply to the pop panel
///    * default: *empty string*
///  * `position`: position to open the panel.  any combination of left, right, up, down, center
///    * default: `updown`
///  * `zIndex`: z-index at which to render the panel
///    * default: `100`
///  * `hideOnDocumentClick`: boolean value of whether to hide the panel when the document is clicked
///    * default: `true`
/// 
/// ### Events
/// 
///  * `glowPopUpPanelShown` - triggered when a panel is shown
///  * `glowPopUpPanelHidden` - triggered when a panel is hidden
///  * `glowPopUpPanelMouseOver` - triggered when a panel is moused over
///  * `glowPopUpPanelMouseOut` - triggered when a panel is moused out
/// 
/// ### Methods
/// 
/// #### isShown
/// 
/// Returns whether the panel is currently visible
/// 
///     var isShown = $('SELECTOR').glowPopUpPanel('isShown');    
/// 
/// #### isOpening
/// 
/// Returns whether the panel is currently opening
/// 
///     var isOpening = $('SELECTOR').glowPopUpPanel('isOpening');    
/// 
/// #### show
/// 
/// Shows the panel
/// 
///     $('SELECTOR').glowPopUpPanel('show');    
/// 
/// #### hide
/// 
/// Hides the panel
///     
///     $('SELECTOR').glowPopUpPanel('hide');    
/// 
/// #### empty
/// 
/// Empties the content of the panel
/// 
///     $('SELECTOR').glowPopUpPanel('empty');    
/// 
/// #### html
/// 
/// Sets new HTML to display in a panel
/// 
///     $('SELECTOR').glowPopUpPanel('html', '<strong>HTML Content</strong>');    
/// 
/// #### append
/// 
/// Appends new content to existing content in the panel where content is a string, element, or selection
/// 
///     $('SELECTOR').glowPopUpPanel('append', '<strong>HTML Content</strong>');    
/// 
/// #### remove
/// 
/// Removes a the the pop up panel's element
/// 
///     $('SELECTOR').glowPopUpPanel('remove', element);    
/// 
/// #### children
/// 
/// Returns all current children elements of the panel's content
/// 
///     var children = $('SELECTOR').glowPopUpPanel('children');
/// 
/// 
(function($, global){

    var _eventNames = {
            shown:'glowPopUpPanelShown',
            hidden:'glowPopUpPanelHidden',
            mouseOver:'glowPopUpPanelMouseOver',
            mouseOut:'glowPopUpPanelMouseOut'
        },
        _dataKey = '_glowPopUpPanel',
        _eventNameSpace = '.glowPopUpPanel',
        _init = function(options)
        {
            return this.each(function()
            {
                var context = {
                    settings: $.extend({}, $.fn.glowPopUpPanel.defaults, options || {}),
                    internal: {
                        state: $(this),
                        isShown:false,
                        initialized:false,
                        lastPosition:null,
                        panel:null,
                        panelMask:null,
                        animationHandle:null,
                        isOpening:false,
                        checkForScrollResizeHandle:null,
                        lastWindowInfo:null
                    }
                };

                $(this).data(_dataKey, context);

                _render(context);
            });
        },
        _render= function(context)
        {
            context.internal.panel = $('<div></div>')
                .css({
                    position: 'absolute',
                    display: 'none',
                    left: '0px',
                    top: '0px'
                })
                .addClass(context.settings.CssClass)
                .append(context.internal.state.children());

            context.internal.panelMask = $('<div></div>')
                .css({
                    position: 'absolute',
                    display: 'none'
                })
                .append(context.internal.panel);

            context.internal.state.css({ displaly: 'none'});
        },
        _onMouseOver= function(context, event)
        {
            context.internal.state.trigger(_eventNames.mouseOver);
        },
        _onMouseOut= function(context, event)
        {
            context.internal.state.trigger(_eventNames.mouseOut);
        },
        _registerDocumentOnClick= function(context)
        {
            $(document).bind('click' + _eventNameSpace, function() { _documentOnClick(context); });
        },
        _checkForScrollResize= function(context)
        {
            if (context.internal.checkForScrollResizeHandle) {
                global.clearTimeout(context.internal.checkForScrollResizeHandle);
            }

            if (context.internal.isShown && !context.internal.isOpening && context.internal.lastWindowInfo)
            {
                var windowInfo = $.telligent.glow.utility.getWindowInfo();

                // did check: windowInfo.ScrollX != this._lastWindowInfo.ScrollX || windowInfo.ScrollY != this._lastWindowInfo.ScrollY ||
                if (windowInfo.Width != context.internal.lastWindowInfo.Width ||
                    windowInfo.Height != context.internal.lastWindowInfo.Height)
                {
                    api.hide.apply(context.internal.state);
                } else {
                    context.internal.checkForScrollResizeHandle = global.setTimeout(function() {
                        _checkForScrollResize(context);
                    }, 999);
                }
            }
        },
        _initialize= function(context)
        {
            if (document.forms.length == 1) {
                $('form:first').append(context.internal.panelMask);
            } else {
                $('body').append(context.internal.panelMask);
            }

            context.internal.panel
                .bind('mouseover' + _eventNameSpace, function() {
                    _onMouseOver(context);
                })
                .bind('mouseout' + _eventNameSpace, function() {
                    _onMouseOut(context);
                });

            context.internal.initialized = true;
        },
        _animate = function(context, propertyName, targetValue, nextValue, step, acceleration)
        {
            if (context.internal.animationHandle) {
                global.clearTimeout(context.internal.animationHandle);
            }

            currValue = parseInt(context.internal.panel.css(propertyName), 10);

            if ((step < 0 && currValue < targetValue) ||
                (step > 0 && currValue > targetValue) || Math.abs(step) < 1)
            {
                // complete
                context.internal.panel
                    .css('propertyName',targetValue + 'px')
                    .css({
                        position: 'static',
                        overflow: 'visible'
                    });

                context.internal.animationHandle = null;
                context.internal.isOpening = false;

                context.internal.lastWindowInfo = $.telligent.glow.utility.getWindowInfo();
                context.internal.checkForScrollResizeHandle = global.setTimeout(function() {
                    _checkForScrollResize(context);
                }, 999);
            }
            else
            {
                // continue animation

                context.internal.panel.css(propertyName, nextValue + 'px');

                nextValue = nextValue + step;
                if (step > 0 && nextValue > targetValue) {
                    nextValue = targetValue;
                }
                else if (step < 0 && nextValue < targetValue) {
                    nextValue = targetValue;
                }

                step = step * acceleration;

                context.internal.animationHandle = global.setTimeout(function() {
                    _animate(context, propertyName, targetValue, nextValue, step, acceleration);
                }, 19);
            }
        },
        _documentOnClick= function(context)
        {
            if (context.internal.isShown && !context.internal.isOpening) {
                api.hide.apply(context.internal.state);
            }
        };


    var api =
    {
        isShown: function()
        {
            var context = this.data(_dataKey);
            return context && context.internal.isShown;
        },
        isOpening: function()
        {
            var context = this.data(_dataKey);
            return context && context.internal.isOpening;
        },
        show: function()
        {
            var x, y, positionWidth, positionHeight, ignoreHideAndAnimation, ignorePanel = false;

            if (typeof arguments[0] === 'object')
            {
                var elm = arguments[0];
                if(arguments[0] instanceof jQuery) {
                    elm = arguments[0].get(0);
                }
                var elementInfo = $.telligent.glow.utility.getElementInfo(elm);
                x = elementInfo.Left;
                y = elementInfo.Top;
                positionWidth = elementInfo.Width;
                positionHeight = elementInfo.Height;
                ignoreHideAndAnimation = arguments[1];
				ignorePanel = arguments[2];
            }
            else
            {
                x = arguments[0];
                y = arguments[1];
                positionWidth = arguments[2];
                positionHeight = arguments[3];
                ignoreHideAndAnimation = arguments[4];
            }

            return this.each(function()
            {
                var context = $(this).data(_dataKey);

                if (!context.internal.initialized) {
                    _initialize(context);
                }

                if (!context.internal.isShown && context.settings.hideOnDocumentClick) {
                    global.setTimeout(function() {
                        _registerDocumentOnClick(context);
                    }, 49);
                }

                if (!ignoreHideAndAnimation && context.internal.isShown) {
                    api.hide.apply(context.internal.state);
                } else {
                    $.telligent.glow.utility.showSelectBoxes(context.internal.panelMask);
                }

                var originalX = x;
                var originalY = y;

                context.internal.panelMask.css({
                    position: 'absolute',
                    zIndex: context.settings.zIndex,
                    display: 'block',
                    visibility: 'hidden',
                    overflow: 'visible'
                });

                context.internal.panel
                    .css({
                        position: 'absolute',
                        display: 'block',
                        visibility: 'hidden',
                        left: '0px',
                        top: '0px' })
                    .addClass(context.settings.cssClass);

                // width/height of panel
                var panelWidth = context.internal.panel.outerWidth();
                var panelHeight = context.internal.panel.outerHeight();

				if(!ignorePanel) {
	                context.internal.panelMask.css({
	                    width: panelWidth + 'px',
	                    height: panelHeight + 'px'
	                });					
				}

                // retrieve the window info
                context.internal.lastWindowInfo = $.telligent.glow.utility.getWindowInfo();

                var animatePropertyName, animateTargetValue, animateNextValue;

                var iLeft = context.settings.position.indexOf('left');
                var iRight = context.settings.position.indexOf('right');
                var iUp = context.settings.position.indexOf('up');
                var iDown = context.settings.position.indexOf('down');
                var iCenter = context.settings.position.indexOf('center');

                if (iLeft === 0 || iRight === 0 || iCenter === 0)
                {
                    // position left/right, then up/down

                    if (!(iLeft > -1 && iRight == -1)
                        && (
                            (iRight > -1 && iLeft == -1)
                            || (context.internal.lastWindowInfo.Width + context.internal.lastWindowInfo.ScrollX) - (x + positionWidth) > x - context.internal.lastWindowInfo.ScrollX
                            || (x - context.internal.lastWindowInfo.ScrollX) < panelWidth
                            ))
                    {
                        // position right
                        context.internal.panelMask.css({ left: (x + positionWidth) + "px" });
                        animatePropertyName = 'left';
                        animateTargetValue = 0;
                        animateNextValue = -panelWidth;
                    }
                    else if(iCenter > -1)
                    {
                        // position center
                        context.internal.panelMask.css({ left: (x - panelWidth/2) + "px" });
                        animatePropertyName = 'left';
                        animateTargetValue = 0;
                        animateNextValue = panelWidth/2;
                    }
                    else
                    {
                        // position left
                        context.internal.panelMask.css({ left: (x - panelWidth) + "px" });
                        animatePropertyName = 'left';
                        animateTargetValue = 0;
                        animateNextValue = panelWidth;
                    }

                    if (!(iUp > -1 && iDown == -1)
                        && (
                            (iDown > -1 && iUp == -1)
                            || (context.internal.lastWindowInfo.Height + context.internal.lastWindowInfo.ScrollY) - y > (y + positionHeight) - context.internal.lastWindowInfo.ScrollY
                            || (y + positionHeight) - context.internal.lastWindowInfo.ScrollY < panelHeight
                            ))
                    {
                        // position down
                        context.internal.panelMask.css({ top: y + "px" });
                    }
                    else
                    {
                        // position up
                        context.internal.panelMask.css({'top': (y + positionHeight - panelHeight) + "px"});
                    }
                }
                else
                {
                    // position up/down, then left/right
                    if(iCenter > -1)
                    {
                        // position left
                        context.internal.panelMask.css({ left: (x + (positionWidth - panelWidth)/2) + "px" });
                    }
                    else if (!(iLeft > -1 && iRight == -1)
                        && (
                            (iRight > -1 && iLeft == -1)
                            || (context.internal.lastWindowInfo.Width + context.internal.lastWindowInfo.ScrollX) - x > (x + positionWidth) - context.internal.lastWindowInfo.ScrollX
                            || (x + positionWidth) - context.internal.lastWindowInfo.ScrollX < panelWidth
                            ))
                    {
                        // position right
                        context.internal.panelMask.css({ left: x + "px" });
                    }
                    else
                    {
                        // position left
                        context.internal.panelMask.css({ left: (x + positionWidth - panelWidth) + "px" });
                    }

                    if (!(iUp > -1 && iDown == -1)
                        && (
                            (iDown > -1 && iUp == -1)
                            || (context.internal.lastWindowInfo.Height + context.internal.lastWindowInfo.ScrollY) - (y + positionHeight) > (y - context.internal.lastWindowInfo.ScrollY)
                            || (y - context.internal.lastWindowInfo.ScrollY) < panelHeight
                            ))
                    {
                        // position down
                        context.internal.panelMask.css({ top: (y + positionHeight) + "px" });
                        animatePropertyName = 'top';
                        animateTargetValue = 0;
                        animateNextValue = -panelHeight;
                    }
                    else
                    {
                        // position up
                        context.internal.panelMask.css({ top: (y - panelHeight) + "px" });
                        animatePropertyName = 'top';
                        animateTargetValue = 0;
                        animateNextValue = panelHeight;
                    }
                }

                context.internal.panel.css({ visibility: 'visible' });
                context.internal.panelMask.css({
                    visibility: 'visible',
                    overflow: 'hidden'
                });

                // detect and hide select boxes
                $.telligent.glow.utility.hideSelectBoxes(context.internal.panelMask);

                this._isOpening = true;
                if (ignoreHideAndAnimation) {
                    context.internal.animationHandle = global.setTimeout(function() {
                        _animate(context, animatePropertyName, animateTargetValue, animateTargetValue ,0,0);
                    }, 9);
                } else {
                    _animate(context, animatePropertyName, animateTargetValue, animateNextValue,
                        animateNextValue > animateTargetValue ? -((animateNextValue - animateTargetValue) / 3) : ((animateTargetValue - animateNextValue) / 3), .67);
                }

                if (!context.internal.isShown)
                {
                    context.internal.isShown = true;
                    context.internal.lastPosition = { X: originalX, Y: originalY, Width: positionWidth, Height: positionHeight };

                    $(context.internal.state).trigger(_eventNames.shown);
                }
            });
        },
        hide: function()
        {
            return this.each(function()
            {
                var context = $(this).data(_dataKey);

                if (context.internal.isShown)
                {
                    if (!context.internal.initialized) {
                        _initialize(context);
                    }

                    context.internal.panel.css({
                        position: 'absolute',
                        display: 'none'
                    });
                    context.internal.panelMask.css({
                        position: 'absolute',
                        display: 'none'
                    });

                    context.internal.isShown = false;
                    context.internal.lastPosition = null;

                    $.telligent.glow.utility.showSelectBoxes(context.internal.panelMask);

                    if (context.settings.hideOnDocumentClick)
                    {
                        $(document).unbind(_eventNameSpace);
                    }

                    context.internal.state.trigger(_eventNames.hidden);
                }
            });
        },
        empty: function()
        {
            return this.each(function() {
                var context = $(this).data(_dataKey);
                context.internal.panel.empty();
            }).glowPopUpPanel('refresh');
        },
        html: function(html)
        {
            return this.each(function() {
                var context = $(this).data(_dataKey);
                context.internal.panel.html(html);
            }).glowPopUpPanel('refresh');
        },
        append: function(element)
        {
            element = $(element);

            var first = true;
            return this.each(function() {
                var context = $(this).data(_dataKey);

                if (first) {
                    $(context.internal.panel).append(element);
                    first = false;
                } else {
                    $(context.internal.panel).append(element.clone());
                }
            }).glowPopUpPanel('refresh');
        },
        remove: function(element)
        {
            return $(element).remove().glowPopUpPanel('refresh');
        },
        children: function()
        {
            var context = this.data(_dataKey);
            if (context) {
                return context.internal.panel.children();
            }
            return $();
        },
        refresh: function()
        {
            return this.each(function()
            {
                var context = $(this).data(_dataKey);

                if (context.internal.animationHandle) {
                    global.clearTimeout(context.internal.animationHandle);
                }

                if (context.internal.isShown && context.internal.lastPosition) {
                    api.show.apply(
                        context.internal.state,
                        [
                            context.internal.lastPosition.X,
                            context.internal.lastPosition.Y,
                            context.internal.lastPosition.Width,
                            context.internal.lastPosition.Height,
                            true
                        ]);
                }
            });
        }
    };

    $.fn.glowPopUpPanel = function(method)
    {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowPopUpPanel');
        }
    };
    $.extend($.fn.glowPopUpPanel, {
        defaults: {
            cssClass:'',
            position:'updown',
            zIndex:100,
            hideOnDocumentClick:true
        }
    });
    // events
    //   glowPopUpPanelShown
    //   glowPopUpPanelHidden
    //   glowPopUpPanelMouseOver
    //   glowPopUpPanelMouseOut
})(jQuery, window); 
 
/// @name glowTabbedPanes
/// @category jQuery Plugin
/// @description Renders and manages a set of selectable tab panes
/// 
/// ### jQuery.fn.glowTabbedPanes
/// 
/// Renders and manages a set of selectable tab panes
/// 
/// ### Usage
/// 
///     $('SELECTOR').glowTabbedPanes(options)
/// 
/// where 'SELECTOR' contains a div element whose child child div elements are rendered as tab pane contents
/// 
/// ### Options
/// 
///  * `cssClass`: string class name applied to the div wrapping the tab panes
///    * default: *empty string*
///  * `tabSetCssClass`: string class name applied to the div wrapping the tabs
///    * default: *empty string*
///  * `tabCssClasses`: array of string classes to apply to tabs.  The first class is applied to the rendered link for the tab.  Any subsequent classes are transformed into nested `div` elements within the tab with the class names applied, one-per-class.
///    * default: `[]`
///  * `tabSelectedCssClasses`: array of string classes to apply to selected tabs.  The first class is applied to the rendered link for the tab.  Any subsequent classes are transformed into nested `div` elements within the tab with the class names applied, one-per-class.
///    * default: `[]`
///  * `tabHoverCssClasses`: array of string classes to apply to hovered tabs.  The first class is applied to the rendered link for the tab.  Any subsequent classes are transformed into nested `div` elements within the tab with the class names applied, one-per-class.
///    * default: `[]`
///  * `enableResizing`: boolean value of whether to allow resizing of the tab set
///    * default: `true`
///  * `tabs`: declarative array of tab items.  Each tab is matched by array index position to a child div of the selector as that tab's content.  Each tab item is, itself, an array of the following format:
///    * `[0]`: tab id string
///    * `[1]`: tab displaly text
///    * `[2]`: callback function when clicked
///    * `[3]`: boolean value of whether tab is disabled
///    * `[4]`: specific string class name applied to this tab
/// 
/// ### Events
/// 
///  * `glowTabbedPanesResize` - triggered when modifications of the tabbed panes result in a resize
/// 
/// ### Methods
/// 
/// #### add
/// 
/// Adds a new pane to the tabbed panes
/// 
///     var pane = $('SELECTOR').glowTabbedPanes('createTab', {id:'id',text:'text'});
///     $('SELECTOR').glowTabbedPanes('add', pane);
/// 
/// #### remove
/// 
/// Removes a pane from the tabbed panes.  Optional second boolean parameter declares whether to preserve the tab's content.
/// 
///     $('SELECTOR').glowTabbedPanes('remove', pane);        // preserves content by default
///     $('SELECTOR').glowTabbedPanes('remove', pane, false); // do not preserve tab content
/// 
/// #### clear
/// 
/// Removes all tab items from the tab set
/// 
///     $('SELECTOR').glowTabbedPanes('clear');
/// 
/// #### insert
/// 
/// Adds a new pane to the tabbed panes at a specific index
/// 
///     var pane = $('SELECTOR').glowTabbedPanes('createTab', {id:'id',text:'text'});
///     var index = 3;
///     $('SELECTOR').glowTabbedPanes('add', pane, index);
/// 
/// #### getById
/// 
/// Retrieves an existing pane by its id
/// 
///     var pane = $('SELECTOR').glowTabbedPanes('getById', 'tabId');
/// 
/// #### getByText
/// 
/// Retrieves an existing pane by its display text
/// 
///     var pane = $('SELECTOR').glowTabbedPanes('getByText', 'text');
/// 
/// #### getByIndex
/// 
/// Retrieves an existing pane by its indexed position
/// 
///     var pane = $('SELECTOR').glowTabbedPanes('getByIndex', 3);
///     
/// #### count
/// 
/// Returns the count of available panes
/// 
///     var count = $('SELECTOR').glowTabbedPanes('count');
/// 
/// #### selected
/// 
/// Gets and/or sets the currently selected tab pane (or `null` if not selected.)
/// 
///     var currentlySelectedTab = $('SELECTOR').glowTabbedPanes('selected');
///     $('SELECTOR').glowTabbedPanes('selected', tabToSelect);
/// 
/// #### createTab
/// 
/// Creates a new tab pane tab suitable for adding/inserting into the tab set
/// 
///     var pane = $('SELECTOR').glowTabbedPanes('createTab', {
///         id:'id', 
///         text:'text', 
///         disabled:false,
///         onClick:function(){}, 
///         cssClass:'perTabClass'});
/// 
/// 
/*
-tabs[index]
-    [0] = id
-    [1] = text
-    [2] = onClick
-    [3] = disabled
*/

(function($){

    // public api
    var api = 
    {
        refresh: function()
        {
            return this.each(function()
            {
                var context = $(this).data('tabbedPanes');

                context.internal.isRendered = false;
                
                var selectedTab = context.internal.selectedTab;
                context.internal.selectedTab = null;
                
                if (context.settings.enableResizing)
                {
                    context.internal.state.style.width = '100%';
                    context.internal.state.style.height = '100%';
                    context.internal.state.style.overflow = 'auto';
                }
                else
                {
                    context.internal.state.style.width = 'auto';
                    context.internal.state.style.height = 'auto';
                    context.internal.state.style.overflow = 'visible';
                }
                
                context.internal.paneContainer.className = context.settings.cssClass;
                context.internal.tabSet.glowTabSet('clear');
                
                for (var i = 0; i < context.internal.tabs.length; i++)
                {
                    if (!context.internal.tabs[i]._tabsetTab)
                    {
                        var tab = context.internal.tabSet.glowTabSet('createTab', {
                            id: context.internal.tabs[i].id, 
                            text: context.internal.tabs[i].text, 
                            cssClass: context.internal.tabs[i].cssClass,
                            onClick: $.telligent.glow.utility.makeBoundFunction(_tabClick, null, [context, context.internal.tabs[i]])
                        });
                        context.internal.tabs[i]._tabsetTab = tab;
                        tab._tabbedPanesTab = context.internal.tabs[i];             
                    }
                    
                    if (!context.internal.tabs[i]._tabPane)
                    {
                        var pane = document.createElement('div');
                        context.internal.paneContainer.appendChild(pane);
                        context.internal.tabs[i]._tabPane = pane;
                    }
                
                    if (!context.internal.tabs[i].disabled)
                    {
                        context.internal.tabSet.glowTabSet('add', context.internal.tabs[i]._tabsetTab);
                        if (selectedTab && context.internal.tabs[i].id == selectedTab.id)
                            selectedTab = context.internal.selectedTab = context.internal.tabs[i];
                    }
                }
                
                if (context.internal.selectedTab == null && context.internal.tabSet.glowTabSet('count') > 0)
                {
                    selectedTab = context.internal.selectedTab = context.internal.tabSet.glowTabSet('getByIndex', 0)._tabbedPanesTab;
                    context.internal.tabSet._selectedTab = context.internal.tabSet.glowTabSet('getByIndex', 0);
                }   
                
                context.internal.tabSet.glowTabSet('refresh');
                
                for (var i = 0; i < context.internal.tabs.length; i++)
                {
                    if (!context.internal.tabs[i].disabled)
                    {
                        if (context.internal.tabs[i].id == selectedTab.id)
                        {
                            selectedTab = context.internal.selectedTab = context.internal.tabs[i];
                            context.internal.tabSet.glowTabSet('selected', context.internal.tabs[i]._tabsetTab);
                            _setPaneVisibility(context, context.internal.tabs[i]._tabPane, true);
                        }
                        else
                            _setPaneVisibility(context, context.internal.tabs[i]._tabPane, false);
                    }
                    else
                        _setPaneVisibility(context, context.internal.tabs[i]._tabPane, false);
                }
                
                context.internal.isRendered = true;
            });
        },
        add: function(tab)
        {
            api.remove.apply(this, [tab, true]);
            
            var isFirst = true;         
            return this.each(function()
            {
                if (isFirst)
                    isFirst = false;
                else
                    tab = tab._clone();
                
                var context = $(this).data('tabbedPanes');

                tab._tabbedPanes = this;
                if (!tab._tabPane)
                {
                    var pane = document.createElement('div');
                    context.internal.paneContainer.appendChild(pane);
                    tab._tabPane = pane;
                }
                
                context.internal.tabs[context.internal.tabs.length] = tab;
            });
        },
        remove: function(tab, preserveContent)
        {
            return this.each(function()
            {
                var context = $(this).data('tabbedPanes');
            
                var tabs = new Array();
                var found = false;
                for (var i = 0; i < context.internal.tabs.length; i++)
                {
                    if (context.internal.tabs[i].id == tab.id)
                    {
                        if (context.internal.tabs[i]._tabPane)
                        {
                            context.internal.paneContainer.removeChild(context.internal.tabs[i]._tabPane);
                            
                            if (preserveContent === false)
                                context.internal.tabs[i]._tabPane = null;
                        }
                        
                        found = true;
                    }
                    else
                        tabs[tabs.length] = context.internal.tabs[i];
                }
                
                if (found)
                    context.internal.tabs = tabs;
            });
        },
        clear: function()
        {
            return this.each(function()
            {
                var context = $(this).data('tabbedPanes');
                
                for(var i = 0; i < context.internal.tabs.length; i++)
                {
                    if (context.internal.tabs[i]._tabPane)
                    {
                        context.internal.paneContainer.removeChild(context.internal.tabs[i]._tabPane);
                        context.internal.tabs[i]._tabPane = null;
                    }
                }
                
                context.internal.tabs = new Array();
            });
        },
        insert: function(tab, index)
        {
            api.remove(this, [tab, true]);
            
            var isFirst = true;
            return this.each(function()
            {
                if (isFirst)
                    isFirst = false;
                else
                    tab = tab._clone();
                
                var context = $(this).data('tabbedPanes');
    
                var tabs = new Array();
                var inserted = false;
                for (var i = 0; i < context.internal.tabs.length; i++)
                {
                    if (i == index)
                    {
                        inserted = true;
                        tab._tabbedPanes = this;
                        if (!tab._tabPane)
                        {
                            var pane = document.createElement('div');
                            context.internal.paneContainer.insertBefore(pane, context.internal.paneContainer.childNodes[i + 1]);
                            tab._tabPane = pane;
                        }
                        
                        tabs[tabs.length] = tab;
                    }
                    
                    tabs[tabs.length] = context.internal.tabs[i];
                }
                
                if (!inserted)
                    api.add.apply($(this), tab);
                else
                    context.internal.tabs = tabs;
            });
        },
        getById: function(id)
        {
            var context = this.data('tabbedPanes');
            if (context)
            {
                for (var i = 0; i < context.internal.tabs.length; i++)
                {
                    if (context.internal.tabs[i].id == id)
                        return context.internal.tabs[i];
                }
            }
    
            return null;
        },
        getByText: function(text)
        {
            var tabs = [];
            var context = this.data('tabbedPanes');
            if (context) {
            	$.each(context.internal.tabs, function(i, tab) {
            		if(tab.text === text) {
            			tabs.push(tab);
            		}	
            	});
            }    
            return tabs;            
        },
        getByIndex: function(index)
        {
            var context = this.data('tabbedPanes');
            if (context && index >= 0 && index < context.internal.tabs.length)
                return context.internal.tabs[index];
    
            return null;
        },
        count: function()
        {
            var context = this.data('tabbedPanes');
            if (context)
                return context.internal.tabs.length;
            
            return 0;
        },
        selected: function(tab)
        {
        	if (tab === undefined)
        	{
        		var context = this.data('tabbedPanes');
	            if (context)
	                return context.internal.selectedTab;
	            
	            return null;
        	}
        	else
        	{
        		if (!tab)
        			return this;
        		
	            return this.each(function()
	            {
	                var context = $(this).data('tabbedPanes');
	                var t = api.getById.apply($(this), [tab.id]);
	                if (t)
	                    context.internal.tabSet.glowTabSet('selected', t._tabsetTab);
	            });
           	}
        },
        createTab: function(options)
        {        
            return new TabbedPanesTab(options);
        }
    };
    
    // private api
    var _resizeEventName = 'glowTabbedPanesResize',
        _init = function(options) 
        {
            return this.each(function() 
            {
                var context = {
                    settings: $.extend({}, $.fn.glowTabbedPanes.defaults, options || {}),
                    internal: {
                        state: this,
                        tabSet: null,
                        tabs: [],
                        isRendered: false,
                        selectedTab: null,
                        paneContainer: null,
                        previousContainerOffsetWidth: -1                        
                    }
                };                  
            
                context.internal.paneContainer = document.createElement('div');
                while(this.childNodes.length > 0)
                    context.internal.paneContainer.appendChild(this.childNodes[0]);
            
                var tabSetContainer = document.createElement('div');
            
                this.appendChild(tabSetContainer);
                this.appendChild(context.internal.paneContainer);
            
                context.internal.tabSet = $(tabSetContainer)
                    .glowTabSet({
                        cssClass: context.settings.tabSetCssClass,
                        tabCssClasses: context.settings.tabCssClasses,
                        tabSelectedCssClasses: context.settings.tabSelectedCssClasses,
                        tabHoverCssClasses: context.settings.tabHoverCassClasses,
                        enableResizing: context.settings.enableResizing,
                        tabs:[] })
                    .bind('glowTabSetResize', function(){
                        $(context.internal.state).trigger(_resizeEventName);
                    });
            
                $(this).data('tabbedPanes', context);

                _parseTabs(context, context.settings.tabs, null);
            
                if (context.internal.tabs.length > 0)
                    api.refresh.apply($(this));
            });
        },  
        _tabClick= function(context, tab)
        {
            if (tab.onClick)
            {
                hadHandler = true;
                if ((tab.onClick($(context.internal.state), tab)) == false)
                    return false;
            }
        
            if (tab != context.internal.selectedTab)
            {
                if (tab._tabPane)
                    _setPaneVisibility(context, tab._tabPane, true);
        
                if (context.internal.selectedTab && context.internal.selectedTab._tabPane)
                    _setPaneVisibility(context, context.internal.selectedTab._tabPane, false);
            
                context.internal.selectedTab = tab;
            }
        },
        _setPaneVisibility= function(context, pane, visible)
        {
            if (visible)
            {
                pane.style.visibility = 'visible';
                pane.style.position = 'static';
                pane.style.left = '0px';
            
                if (context.settings.enableResizing)
                {
                    pane.style.width = '100%';
                    pane.style.height = '100%';
                    pane.style.overflow = 'auto';
                }
                else
                {
                    pane.style.width = 'auto';
                    pane.style.height = 'auto';
                    pane.style.overflow = 'visible';
                }
            }
            else
            {
                pane.style.width = pane.offsetWidth + 'px';
                pane.style.height = pane.offsetHeight + 'px';
                pane.style.overflow = 'hidden';
                pane.style.position = 'absolute';
                pane.style.left = (pane.offsetWidth * -4) + 'px';
                pane.style.top = (pane.offsetHeight * -4) + 'px';
                pane.style.visibility = 'hidden';
            }
        },
        _parseTabs= function(context, tabs, selectedTabId)
        {
            context.internal.tabs = new Array();
            context.internal.tabSet.glowTabSet('clear');
            if (!tabs || tabs.length == 0)
                return;
        
            var j = -1;
            for (i = 0; i < tabs.length; i++)
            {
                context.internal.tabs[i] = new TabbedPanesTab({
                	id: tabs[i][0], 
                	text: tabs[i][1], 
                	onClick: tabs[i][2], 
                	disabled: tabs[i][3],
                	cssClass: tabs[i][4]
            	});
            
                j++
                while (j < context.internal.paneContainer.childNodes.length && context.internal.paneContainer.childNodes[j].nodeType != 1)
                    j++;
            
                if (j < context.internal.paneContainer.childNodes.length)
                    context.internal.tabs[i]._tabPane = context.internal.paneContainer.childNodes[j];
                else
                {
                    var pane = document.createElement('div');
                    context.internal.paneContainer.appendChild(pane);
                    tab._tabPane = pane;
                }
            
                context.internal.tabs[i]._tabbedPanes = this;
                if (context.internal.tabs[i].id == selectedTabId)
                    context.internal.selectedTab = context.internal.tabs[i];
            }
        };

    $.fn.glowTabbedPanes = function(method) 
    {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowTabbedPanes');
        }
    };  
    $.extend($.fn.glowTabbedPanes, {
        defaults: {
            cssClass:'',
            tabSetCssClass:'',
            tabCssClasses:[],
            tabSelectedCssClasses:[],
            tabHoverCssClasses:[],
            enableResizing:true,
            tabs:[]
        }       
    });
    // events
    //   glowTabbedPanesResize
    
    var TabbedPanesTab = function(options)
    {
        var settings = 
        {
            id: (new Date()).getTime(),
            text: '',
            onClick: null,
            cssClass: null,
            disabled: false
        };

        if (options)
            $.extend(settings, options);

        this.id = settings.id;
        this.text = settings.text;
        this.onClick = settings.onClick;
        this.disabled = settings.disabled;
        this.cssClass = settings.cssClass;
        this._tabPane = null;
        this._tabsetTab = null;
        this._tabbedPanes = null;
    };

    TabbedPanesTab.prototype.empty = function()
    {
        $(this._tabPane).empty();
    };

    TabbedPanesTab.prototype.html = function(html)
    {
        this.empty();
        $(this._tabPane).html(html);
    };

    TabbedPanesTab.prototype.append = function(node)
    {
        context.internal.tabPane.appendChild(node);
    };

    TabbedPanesTab.prototype.remove = function(node)
    {
        context.internal.tabPane.removeChild(node);
    };

    TabbedPanesTab.prototype.children = function()
    {
        return context.internal.tabPane.childNodes;
    };  
    
})(jQuery);

 
 
/// @name glowTabSet
/// @category jQuery Plugin
/// @description Renders and manages a set of selectable tabs
/// 
/// ### jQuery.fn.glowTabSet
/// 
/// Renders and manages a set of selectable tabs
/// 
/// ### Usage
/// 
///     $('SELECTOR').glowTabSet(options)
/// 
/// where 'SELECTOR' contains div element(s) which will be used as tab container and the options object can contain:
/// 
/// ### Options
/// 
///  * `cssClass`: string class name applied to the containing div
///    * default: *empty string*
///  * `tabCssClasses`: array of string classes to apply to tabs.  The first class is applied to the rendered link for the tab.  Any subsequent classes are transformed into nested `div` elements within the tab with the class names applied, one-per-class.
///    * default: `[]`
///  * `tabSelectedCssClasses`: array of string classes to apply to selected tabs.  The first class is applied to the rendered link for the tab.  Any subsequent classes are transformed into nested `div` elements within the tab with the class names applied, one-per-class.
///    * default: `[]`
///  * `tabHoverCssClasses`: array of string classes to apply to hovered tabs.  The first class is applied to the rendered link for the tab.  Any subsequent classes are transformed into nested `div` elements within the tab with the class names applied, one-per-class.
///    * default: `[]`
///  * `enableResizing`: boolean value of whether to allow resizing of the tab set
///    * default: `true`
///  * `tabs`: declarative array of tab items.  Each tab item is, itself, an array of the following format:
///    * `[0]`: tab id string
///    * `[1]`: tab displaly text
///    * `[2]`: tab URL (or `null`)
///    * `[3]`: callback function when clicked
///    * `[4]`: specific string class name applied to this tab
/// 
/// ### Events
/// 
///  * `glowTabSetResize` - triggered when modifications of the tab set result in a resize
/// 
/// ### Methods
/// 
/// #### add
/// 
/// Adds a new tab item to the tab set
/// 
///     var tab = $('SELECTOR').glowTabSet('createTab', {id:'id',text:'text'});
///     $('SELECTOR').glowTabSet('add', tab);
/// 
/// #### remove
/// 
/// Removes a tab item from the tab set
/// 
///     $('SELECTOR').glowTabSet('remove', tab);
/// 
/// #### clear
/// 
/// Removes all tab items from the tab set
/// 
///     $('SELECTOR').glowTabSet('clear');
/// 
/// #### insert
/// 
/// Adds a new tab item to the tab set at a specific index
/// 
///     var tab = $('SELECTOR').glowTabSet('createTab', {id:'id',text:'text'});
///     var index = 3;
///     $('SELECTOR').glowTabSet('add', tab, index);
/// 
/// #### getById
/// 
/// Retrieves an existing tab by its id
/// 
///     var tab = $('SELECTOR').glowTabSet('getById', 'tabId');
/// 
/// #### getByText
/// 
/// Retrieves an existing tab by its display text
/// 
///     var tab = $('SELECTOR').glowTabSet('getByText', 'text');
/// 
/// #### getByIndex
/// 
/// Retrieves an existing tab by its indexed position
/// 
///     var tab = $('SELECTOR').glowTabSet('getByIndex', 3);
///     
/// #### count
/// 
/// Returns the count of available tabs
/// 
///     var count = $('SELECTOR').glowTabSet('count');
/// 
/// #### selected
/// 
/// Gets and/or sets the currently selected tab item (or `null` if not selected.)
/// 
///     var currentlySelectedTab = $('SELECTOR').glowTabSet('selected');
///     $('SELECTOR').glowTabSet('selected', tabToSelect);
/// 
/// #### createTab
/// 
/// Creates a new tab item suitable for adding/inserting into the tab set
/// 
///     var tab = $('SELECTOR').glowTabSet('createTab', {
///         id:'id', 
///         text:'text', 
///         url:'url', 
///         onClick:function(){}, 
///         cssClass:'perTabClass'});
/// 
/// 
/*
tabs[index]
    [0] = id
    [1] = text
    [2] = url
    [3] = onClick
*/
(function($){

    // public api
    var api = 
    {
        refresh: function()
        {
            return this.each(function()
            {
                var context = $(this).data('tabSet');
            
                context.internal.isRendered = false;
                
                _disposeNodes(context);

                hiddenContainer = document.createElement('div');
                hiddenContainer.style.visibility = 'hidden';
                hiddenContainer.style.position = 'absolute';
                
                context.internal.state.appendChild(hiddenContainer);
                
                while (hiddenContainer.childNodes.length > 0)
                    hiddenContainer.removeChild(hiddenContainer.childNodes[0]); 
                
                var table = document.createElement('table');
                table.cellPadding = '0';
                table.border = '0';
                table.cellSpacing = '0';
                table.appendChild(document.createElement('tbody'));
                table.childNodes[0].appendChild(document.createElement('tr'));
                hiddenContainer.appendChild(table);
                
                var elementDepth = 1;
                if (typeof(context.settings.tabHoverCssClasses) == "object" && context.settings.tabHoverCssClasses.length > elementDepth)
                    elementDepth = context.settings.tabHoverCssClasses.length;
                
                if (typeof(context.settings.tabSelectedCssClasses) == "object" && context.settings.tabSelectedCssClasses.length > elementDepth)
                    elementDepth = context.settings.tabSelectedCssClasses.length;
                
                if (typeof(context.settings.tabCssClasses) == "object" && context.settings.tabCssClasses.length > elementDepth)
                    elementDepth = context.settings.tabCssClasses.length;           
                
                var totalWidth = 0;
                var maxWidth = 0;
                for(var i = 0; i < context.internal.tabs.length; i++)
                {
                    var cell = document.createElement('td');
                    var anchor = document.createElement('a');
                    anchor.href = '#';
                    anchor.style.display = 'block';
                    anchor.style.color = '#000000';
                    anchor.style.textDecoration = 'none';
                    cell.appendChild(anchor);
                    
                    // create child elements (to receive style classes)
                    var element = anchor;
                    for (var j = 0; j < elementDepth; j++)
                    {
                        element.appendChild(document.createElement('div'));
                        element = element.childNodes[0];
                    }   
                    element.innerHTML = context.internal.tabs[i].text;
                    
                    if(context.internal.tabs[i].cssClass) {
                    	$(cell).addClass(context.internal.tabs[i].cssClass)
                    }
                    
                    // place tab into hidden container
                    table.childNodes[0].childNodes[0].appendChild(cell);
                    cell._tab = context.internal.tabs[i];
                    context.internal.tabs[i]._element = cell;

                    // determine the maximum width
                    _setCssClasses(context, cell.childNodes[0], context.settings.tabHoverCssClasses);
                    cell._tab._maxWidth = cell.offsetWidth;
                        
                    _setCssClasses(context, cell.childNodes[0], context.settings.tabSelectedCssClasses);
                    if (cell._tab._maxWidth < cell.offsetWidth)
                        cell._tab._maxWidth = cell.offsetWidth;

                    _setCssClasses(context, cell.childNodes[0], context.settings.tabCssClasses);
                    if (cell._tab._maxWidth < cell.offsetWidth)
                        cell._tab._maxWidth = cell.offsetWidth;
                        
                    totalWidth += cell._tab._maxWidth;
                    if (cell._tab._maxWidth > maxWidth)
                        maxWidth = cell._tab._maxWidth;
                }
                
                var numRows = Math.ceil((((totalWidth * 3) / context.internal.state.offsetWidth) + ((maxWidth * context.internal.tabs.length) / context.internal.state.offsetWidth)) / 4);
                var maxTabsPerRow = Math.ceil(context.internal.tabs.length / numRows);
                if (maxTabsPerRow < 1)
                    maxTabsPerRow = 1;
                
                while (context.internal.state.childNodes.length > 0)
                    context.internal.state.removeChild(context.internal.state.childNodes[0]);
                    
                context.internal.state.className = context.settings.cssClass;
                
                var container = document.createElement('div');
                
                if (context.settings.enableResizing)
                {
                    container.style.width = '100%';
                    container.style.overflow = 'hidden';
                }
                
                context.internal.state.appendChild(container);
                
                context.internal.tabRows = new Array(); 
                
                var selectedTab = context.internal.selectedTab;
                context.internal.selectedTabRow = null;
                context.internal.selectedTab = null;
                
                var j = -1;
                for (var i = 0; i < context.internal.tabs.length; i++)
                {
                    if (j == -1 || (context.settings.enableResizing && (context.internal.tabRows[j]._tabs.length == maxTabsPerRow || (context.internal.tabRows[j]._element.offsetWidth + context.internal.tabs[i]._maxWidth > container.offsetWidth && context.internal.tabRows[j]._tabs.length > 0))))
                    {
                        var table = document.createElement('table');
                        table.cellPadding = '0';
                        table.border = '0';
                        table.cellSpacing = '0';
                        table.appendChild(document.createElement('tbody'));
                        table.childNodes[0].appendChild(document.createElement('tr'));
                        container.appendChild(table);
                        
                        j++;
                        context.internal.tabRows[j] = new TabSetRow(table);
                    }
                    
                    context.internal.tabRows[j]._element.childNodes[0].childNodes[0].appendChild(context.internal.tabs[i]._element);
                    context.internal.tabRows[j]._tabs[context.internal.tabRows[j]._tabs.length] = context.internal.tabs[i];
                
                    context.internal.tabs[i]._tabRow = context.internal.tabRows[j];
                    context.internal.tabs[i]._element.childNodes[0].onclick = $.telligent.glow.utility.makeBoundFunction(_tabClick, null, [context, context.internal.tabs[i], j]);
                    context.internal.tabs[i]._element.childNodes[0].onmouseover = $.telligent.glow.utility.makeBoundFunction(_tabMouseOver, null, [context, context.internal.tabs[i], j]);
                    context.internal.tabs[i]._element.childNodes[0].onmouseout = $.telligent.glow.utility.makeBoundFunction(_tabMouseOut, null, [context, context.internal.tabs[i], j]);
                    
                    if (context.internal.tabs[i] == selectedTab)
                    {
                        context.internal.selectedTab = context.internal.tabs[i];
                        context.internal.selectedTabRow = context.internal.tabRows[j];
                        _setCssClasses(context, context.internal.tabs[i]._element.childNodes[0], context.settings.tabSelectedCssClasses);
                    }
                }
                
                if (context.internal.tabRows.length > 1)
                {
                    for (var i = 0; i < context.internal.tabRows.length; i++)
                    {
                        context.internal.tabRows[i]._element.style.width = '100%';
                    }
                    
                    if (context.internal.selectedTabRow)
                    {
                        container.removeChild(context.internal.selectedTabRow._element);
                        container.appendChild(context.internal.selectedTabRow._element);
                    }
                }
                
                context.internal.isRendered = true;
                
                if (context.internal.tabs.length > 0)
                {
                    if (context.internal.resizeCheckHandle)
                        window.clearTimeout(context.internal.resizeCheckHandle);
                    
                    context.internal.resizeCheckHandle = window.setTimeout(function() { _resizeCheck(context); }, 49);
                }
            });
        },
        add: function(tab)
        {
            api.remove.apply(this, [tab]);
            
            var isFirst = true;
            return this.each(function()
            {
                if (isFirst)
                    isFirst = false;
                else
                    tab = tab._clone();
                
                var context = $(this).data('tabSet');

                if (tab)
                {
                    tab._tabSet = this;
                    context.internal.tabs[context.internal.tabs.length] = tab;
                }
            });
        },
        remove: function(tab)
        {
            return this.each(function()
            {
                var context = $(this).data('tabSet');
                
                var tabs = new Array();
                var found = false;
                for (var i = 0; i < context.internal.tabs.length; i++)
                {
                    if (context.internal.tabs[i].id == tab.id)
                        found = true;
                    else
                        tabs[tabs.length] = context.internal.tabs[i];
                }
                
                if (found)
                    context.internal.tabs = tabs;
            });
        },
        clear: function()
        {
            return this.each(function()
            {
                var context = $(this).data('tabSet');
                
                context.internal.tabs = new Array();
            });
        },
        insert: function(tab, index)
        {
            api.remove.apply(this, [tab]);
            
            var isFirst = true;
            return this.each(function()
            {
                if (isFirst)
                    isFirst = false;
                else
                    tab = tab._clone();
                
                var tabs = new Array();
                var inserted = false;
                for (var i = 0; i < context.internal.tabs.length; i++)
                {
                    if (i == index)
                    {
                        inserted = true;
                        tab._tabSet = this;
                        tabs[tabs.length] = tab;
                    }
                    
                    tabs[tabs.length] = context.internal.tabs[i];
                }
                
                if (!inserted)
                    tabs[tabs.length] = tab;
                
                context.internal.tabs = tabs;
            });
        },
        getById: function(id)
        {
            var context = this.data('tabSet');
            if (context)
            {
                for (var i = 0; i < context.internal.tabs.length; i++)
                {
                    if (context.internal.tabs[i].id == id)
                        return context.internal.tabs[i];
                }
            }
    
            return null;
        },
        getByText: function(text)
        {
            var tabs = new Array();
            
            this.each(function()
            {
                var context = $(this).data('tabSet');
                
                for (var i = 0; i < context.internal.tabs.length; i++)
                {
                    if (context.internal.tabs[i].text == text)
                        tabs[tabs.length] = context.internal.tabs[i];
                }
            });
            
            return tabs;
        },
        getByIndex: function(index)
        {
            var context = this.data('tabSet');
            
            if (context && index >= 0 && index < context.internal.tabs.length)
                return context.internal.tabs[index];

            return null;
        },
        count: function()
        {
            var context = this.data('tabSet');
            if (context)
                return context.internal.tabs.length;
            
            return 0;
        },
        selected: function(tab)
        {
        	if (tab === undefined)
        	{
        		var context = this.data('tabSet');
	            if (context)
	                return context.internal.selectedTab;
	            
	            return null;
	       	}
	       	else
	       	{
	       		if (!tab)
	       			return this;
	       		
	            return this.each(function()
	            {
	                var context = $(this).data('tabSet');
	                var t = api.getById.apply($(this), [tab.id]);
	                if (t)
	                    _tabClick(context, t);
	            });
			}
        },
        createTab: function(options)
        {
            return new TabSetTab(options);
        }
    };

    // private methods
    var _resizeEventName = 'glowTabSetResize',
        _init = function(options) 
        {
            return this.each(function() 
            {
                var context = {
                    settings: $.extend({}, $.fn.glowTabSet.defaults, options || {}),
                    internal: {
                        state: this,
                        selectedTab: null,
                        selectedTabRow: null,
                        tabs: [],
                        tabRows: [],
                        resizeCheckHandle: null,
                        isRendered: false,
                        previousContainerOffsetWidth: -1
                    }
                };                  
            
                $(this).data('tabSet', context);

                _parseTabs(context, context.settings.tabs, null);
            
                if (context.internal.tabs.length > 0)
                    api.refresh.apply($(this));
            });
        },  
        _parseTabs = function(context, tabs, selectedTabId)
        {
            if (typeof(selectedTabId) == 'undefined' && context.internal.selectedTab)
                selectedTabId = context.internal.selectedTab.id;

            context.internal.tabs = new Array();
            if (!tabs || tabs.length == 0)
                return;
                
            for (var i = 0; i < tabs.length; i++)
            {
                context.internal.tabs[i] = new TabSetTab({
                	id:tabs[i][0], 
                	text:tabs[i][1], 
                	url:tabs[i][2], 
                	onClick: tabs[i][3], 
                	cssClass: tabs[i][4]                	
            	});
                context.internal.tabs[i]._tabSet = this;
                if (context.internal.tabs[i].id == selectedTabId)
                    context.internal.selectedTab = context.internal.tabs[i];
            }
        },
        _disposeNodes= function(context)
        {
            for (var i = 0; i < context.internal.tabRows.length; i++)
            {
                if (context.internal.tabRows[i] && context.internal.tabRows[i]._element)
                {
                    if (context.internal.tabRows[i]._element.childNodes[0] && context.internal.tabRows[i]._element.childNodes[0].childNodes[0])
                    {
                        var tr = context.internal.tabRows[i]._element.childNodes[0].childNodes[0];
                        for (var j = 0; j < tr.childNodes.length; j++)
                        {
                            if (tr.childNodes[j] && tr.childNodes[j]._tab)
                            {
                                tr.childNodes[j]._tab._element = null;
                                tr.childNodes[j]._tab = null;
                                tr.childNodes[j].onclick = null;
                                tr.childNodes[j].onmouseover = null;
                                tr.childNodes[j].onmouseout = null;
                            }
                        }
                    }
                
                    context.internal.tabRows[i]._element = null;
                }
            }
        },
        _setCssClasses= function(context, element, classes)
        {
            if (typeof(classes) == 'object')
            {
                var i = 0;
                while (element && i < classes.length)
                {
                    element.className = classes[i];
                    i++;
                    element = element.childNodes[0];
                }
            }
            else if (element)
                element.className = classes;
        },
        _tabMouseOver= function(context, tab)
        {
            if (tab != context.internal.selectedTab)
                _setCssClasses(context, tab._element.childNodes[0], context.settings.tabHoverCssClasses);
                    
            return false;
        },
        _tabMouseOut= function(context, tab)
        {
            if (tab != context.internal.selectedTab)
                _setCssClasses(context, tab._element.childNodes[0], context.settings.tabCssClasses);
            
            return false;
        },
        _tabClick= function(context, tab)
        {
            // execute the client script, if defined
            if (tab.onClick)
            {
                hadHandler = true;
                if ((tab.onClick($(context.internal.state), tab)) == false)
                    return false;
            }
            
            // mark the tab as selected
            if (tab != context.internal.selectedTab)
            {
                if (context.internal.selectedTab)
                    _setCssClasses(context, context.internal.selectedTab._element.childNodes[0], context.settings.tabCssClasses);
                
                context.internal.selectedTab = tab;
                _setCssClasses(context, tab._element.childNodes[0], context.settings.tabSelectedCssClasses);
                
                if (context.internal.tabRows.length > 1)
                {       
                    var found;
                    context.internal.selectedTabRow = null;
                    for (var i = 0; i < context.internal.tabRows.length; i++)
                    {   
                        found = false;
                        for (var j = 0; j < context.internal.tabRows[i]._tabs.length; j++)
                        {
                            if (context.internal.tabRows[i]._tabs[j] == context.internal.selectedTab)
                            {
                                context.internal.selectedTabRow = context.internal.tabRows[i];
                                break;
                            }
                        }
                    
                        if (context.internal.selectedTabRow)
                            break;
                    }
                
                    if (context.internal.selectedTabRow)
                    {
                        context.internal.state.childNodes[0].removeChild(context.internal.selectedTabRow._element);
                        context.internal.state.childNodes[0].appendChild(context.internal.selectedTabRow._element);
                    }
                }
                else
                    context.internal.selectedTabRow = context.internal.tabRows[0];
            }
            
            // navigate to the navigate url, if defined
            if (tab.url) {
            	try	{
                window.location = tab.url;
                }
                catch(e) { }
            }
        
            return false;
        },
        _resizeCheck= function(context)
        {
            if (context.internal.resizeCheckHandle)
                window.clearTimeout(context.internal.resizeCheckHandle);

            if (context.settings.enableResizing && context.internal.isRendered && context.internal.state.offsetWidth != context.internal.previousContainerOffsetWidth)
            {
                api.refresh.apply($(context.internal.state));
                context.internal.previousContainerOffsetWidth = context.internal.state.offsetWidth;
            
                $(context.internal.state).trigger('glowTabSetResize');
            }
        
            context.internal.resizeCheckHandle = window.setTimeout(function() { _resizeCheck(context); }, 999);
        };
    
    $.fn.glowTabSet = function(method) 
    {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowTabSet');
        }
    };  
    $.extend($.fn.glowTabSet, {
        defaults: {
            cssClass:'',
            tabCssClasses:[],
            tabSelectedCssClasses:[],
            tabHoverCssClasses:[],
            enableResizing:true,
            tabs:[]         
        }       
    });
    // events
    //   glowTabSetResize
        
    var TabSetTab = function(options)
    {
        var settings = 
        {
            id: (new Date()).getTime(),
            text: '',
            url: '',
            cssClass: null,
            onClick: null
        };

        if (options)
            $.extend(settings, options);

        this.id = settings.id;
        this.text = settings.text;
        this.url = settings.url;
        this.onClick = settings.onClick;
        this.cssClass = settings.cssClass;
        this._maxWidth = 0;
        this._element = null;
        this._tabSet = null;
        this._tabSetRow = null;
    }

    TabSetTab.prototype._clone = function()
    {
        return new TabSetTab({id:this.id,text:this.text,url:this.url,onClick:this.onClick});
    }

    var TabSetRow = function(element)
    {
        this._element = element;
        this._tabs = new Array();
        this._tabSet = null;
    };  
    
})(jQuery);

 
 
(function($){
    // locally-scoped private methods, no longer exposed on the global object
    var _delayedSetCurrentCursorSelection = function(element)
        {
            var state = $(element).data('telligentUtility');
            if (state)
            {
                util.setCurrentCursorSelection(state.InputElement, state.StartIndex, state.EndIndex, false, state.PersistedValue);
                $(element).data('telligentUtility',null);
            }
        },        
        _hiddenSelects = [],
        _elementsHidingSelects = [],
        _addHiddenSelect = function(elementHidingSelect, selectElement)
        {
            var hiddenSelect = null;
            var emptyIndex = -1;
        
            for (var i = 0; i < _hiddenSelects.length && hiddenSelect === null; i++)
            {
                if (_hiddenSelects[i])
                {
                    if (_hiddenSelects[i].SelectElement == selectElement) {
                        hiddenSelect = _hiddenSelects[i];
                    }
                }
                else {
                    emptyIndex = i;
                }
            }
        
            if (hiddenSelect === null)
            {
                hiddenSelect = {};
                hiddenSelect.SelectElement = selectElement;
                hiddenSelect.ElementsHidingSelectsIndeces = [];
                hiddenSelect.OriginalVisibility = selectElement.style.visibility;
                hiddenSelect.SelectElement.style.visibility = 'hidden';
            
                if (emptyIndex == -1)
                {
                    _hiddenSelects[_hiddenSelects.length] = hiddenSelect;
                    hiddenSelect.Index = _hiddenSelects.length - 1;
                }
                else
                {
                    _hiddenSelects[emptyIndex] = hiddenSelect;
                    hiddenSelect.Index = emptyIndex;
                }
            }
        
            var ignore = false;
            for (var j = 0; j < hiddenSelect.ElementsHidingSelectsIndeces.length && !ignore; j++)
            {
                if (hiddenSelect.ElementsHidingSelectsIndeces[j] == elementHidingSelect.Index) {
                    ignore = true;
                }
            }

            if (!ignore)
            {
                elementHidingSelect.HiddenSelectsIndeces[elementHidingSelect.HiddenSelectsIndeces.length] = hiddenSelect.Index;
                hiddenSelect.ElementsHidingSelectsIndeces[hiddenSelect.ElementsHidingSelectsIndeces.length] = elementHidingSelect.Index;
            }
        },
        _addElementHidingSelect = function(element)
        {
            var elementHidingSelect = null;
            var emptyIndex = -1;
        
            for (var i = 0; i < _elementsHidingSelects.length && elementHidingSelect === null; i++)
            {
                if (_elementsHidingSelects[i])
                {
                    if (_elementsHidingSelects[i].Element == element) {
                        elementHidingSelect = _elementsHidingSelects[i];
                    }
                }
                else {
                    emptyIndex = i;
                }
            }
        
            if (elementHidingSelect === null)
            {
                elementHidingSelect = {};
                elementHidingSelect.Element = element;
                elementHidingSelect.HiddenSelectsIndeces = [];
            
                if (emptyIndex == -1)
                {
                    _elementsHidingSelects[_elementsHidingSelects.length] = elementHidingSelect;
                    elementHidingSelect.Index = _elementsHidingSelects.length - 1;
                }
                else
                {
                    _elementsHidingSelects[emptyIndex] = elementHidingSelect;
                    elementHidingSelect.Index = emptyIndex;
                }
            }

            return elementHidingSelect;
        };
        
    var util = 
    {
        convertHsvColorToRgbColor: function(hsv)
        {
            var h = hsv[0];
            var s = hsv[1];
            var v = hsv[2];

            var r;
            var g;
            var b;

            if (s === 0) {
                return [v, v, v];
            }

            var htemp;

            if (h == 360) {
                htemp = 0;
            } else {
                htemp = h;
            }

            htemp = htemp / 60;
            var i = Math.floor(htemp);
            var f = htemp - i;

            var p = v * (1 - s);
            var q = v * (1 - (s * f));
            var t = v * (1 - (s * (1 - f)));

            if (i === 0) { r = v; g = t; b = p;}
            if (i === 1) { r = q; g = v; b = p;}
            if (i === 2) { r = p; g = v; b = t;}
            if (i === 3) { r = p; g = q; b = v;}
            if (i === 4) { r = t; g = p; b = v;}
            if (i === 5) { r = v; g = p; b = q;}

            r = Math.round(r);
            g = Math.round(g);
            b = Math.round(b);

            return [r, g, b];
        },
        convertRgbColorToHsvColor: function(rgb) 
        {
            var r = rgb[0];
            var g = rgb[1];
            var b = rgb[2];

            var h;
            var s;
            var v = Math.max(Math.max(r, g), b);
            var min = Math.min(Math.min(r, g), b);
            var delta = v - min;

            if (v === 0) {
                s = 0;
            } else {
                s = delta / v;
            }

            if (s === 0) {
                h = 0;
            } else {
                if (r == v) {
                    h = 60 * (g - b) / delta;
                } else if (g == v) {
                    h = 120 + 60 * (b - r) / delta;
                } else if (b == v) {
                    h = 240 + 60 * (r - g) / delta;
                }
            }  

            if (h < 0) {
                h += 360;
            }
            
            return [h, s, v];
        },
        convertDecimalToHexadecimal: function(d, digits)
        {
            var chars = "0123456789abcdef";
            var h = '';
            var mod;
            
            while (d > 0)
            {
                mod = d % 16;
                h = chars.substr(mod, 1) + h;
                d -= mod;
                d /= 16;
            }
            
            if (digits)
            {
                while (h.length < digits)
                {
                    h = "0" + h;
                }
            }
            
            return h;
        },
        convertHtmlColorToRgbColor: function(html)
        {
            html = html.replace(/[^0-9a-f]/ig, '');
    
            if (html.length == 3) {
                return [parseInt(html.substr(0, 1) + html.substr(0, 1), 16), parseInt(html.substr(1, 1) + html.substr(1, 1), 16), parseInt(html.substr(2, 1) + html.substr(2, 1), 16)];
            } else if (html.length == 6) {
                return [parseInt(html.substr(0, 2), 16), parseInt(html.substr(2, 2), 16), parseInt(html.substr(4, 2), 16)];
            } else {
                return [255, 255, 255];
            }
        },
        convertRgbColorToHtmlColor: function(rgbColor)
        {
            return "#" + util.convertDecimalToHexadecimal(rgbColor[0], 2) + util.convertDecimalToHexadecimal(rgbColor[1], 2) + util.convertDecimalToHexadecimal(rgbColor[2], 2);
        },
        getContrastingHtmlColorForRgbColor: function(rgb)
        {
            var illuminance = (rgb[0] * 2) + (rgb[1] * 5) + (rgb[2]);
            if (illuminance > 1024) {
                return "#000000";
            } else {
                return "#ffffff";
            }
        },
        getElementInfo: function(element)
        {
            if(element instanceof jQuery) { element = element.get(0); }
            var elementInfo = {};

            if (!element.getBoundingClientRect)
            {
                var curleft = 0;
                var curtop = 0;
                var obj = element;    
                var leftComplete = false;
                var topComplete = false;
                var isIE = util.isIE();
                while (obj)
                {
                    if (!leftComplete) {
                        curleft += obj.offsetLeft;
                    }

                    if (!topComplete) {
                        curtop += obj.offsetTop;
                    }
                    
                    // this is a hack to attempt to work around a bug in IE that occurs when 
                    // an element is positioned using relative positioning.  It's not 100%
                    // accurate, but it is close
                    if (isIE && obj.offsetParent && obj.offsetParent.style.position == 'relative')
                    {
                        if (!obj.offsetParent.style.width)
                        {
                            leftComplete = true;
                            
                            if ((obj.offsetParent.style.top || obj.offsetParent.style.bottom))
                            {
                                curtop += obj.offsetHeight;
                                topComplete = true;
                            }
                        }
                    }
                
                    obj = obj.offsetParent;
                }
            
                elementInfo.Left = curleft;
                elementInfo.Top = curtop;
                elementInfo.Right = curleft + element.offsetWidth;
                elementInfo.Bottom = curleft + element.offsetHeight;
                elementInfo.Width = element.offsetWidth;
                elementInfo.Height = element.offsetHeight;
            }
            else
            {
                var boundingRect = element.getBoundingClientRect();
                var scrollX = 0, scrollY = 0;
                
                if (typeof(window.pageXOffset) == 'number') 
                {
                    //Netscape compliant
                    scrollX = window.pageXOffset;
                    scrollY = window.pageYOffset;
                } 
                else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) 
                {
                    //DOM compliant
                    scrollX = document.body.scrollLeft;
                    scrollY = document.body.scrollTop;
                } 
                else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) 
                {
                    //IE6 standards compliant mode
                    scrollX = document.documentElement.scrollLeft;
                    scrollY = document.documentElement.scrollTop;
                }
                
                elementInfo.Left = boundingRect.left + scrollX;
                elementInfo.Top = boundingRect.top + scrollY;
                elementInfo.Right = boundingRect.right + scrollX;
                elementInfo.Bottom = boundingRect.bottom + scrollY;

                if (util.isIE() && elementInfo.Left >= 2 && !util.isIE8())
                {
                    elementInfo.Left -= 2;
                    elementInfo.Top -= 2;
                    elementInfo.Right -= 2;
                    elementInfo.Bottom -= 2;
                }
                
                elementInfo.Width = boundingRect.right - boundingRect.left;
                elementInfo.Height = boundingRect.bottom - boundingRect.top;
            }
            
            return elementInfo;
        },
        getWindowInfo: function(w)
        {
        	if (!w)
        	{
        		w = window;
        	}
        	
            var scrollX = 0, scrollY = 0, width = 0, height = 0, contentWidth = 0, contentHeight = 0;

            if (typeof(w.pageXOffset) == 'number') 
            {
                //Netscape compliant
                scrollX = w.pageXOffset;
                scrollY = w.pageYOffset;
            } 
            else if (w.document.body && (w.document.body.scrollLeft || w.document.body.scrollTop)) 
            {
                //DOM compliant
                scrollX = w.document.body.scrollLeft;
                scrollY = w.document.body.scrollTop;
            } 
            else if (w.document.documentElement && (w.document.documentElement.scrollLeft || w.document.documentElement.scrollTop)) 
            {
                //IE6 standards compliant mode
                scrollX = w.document.documentElement.scrollLeft;
                scrollY = w.document.documentElement.scrollTop;
            }

            if (typeof(w.innerWidth) == 'number') 
            {
                //Non-IE
                width = w.innerWidth;
                height = w.innerHeight;
            } 
            else if (w.document.documentElement && (w.document.documentElement.clientWidth || w.document.documentElement.clientHeight)) 
            {
                //IE 6+ in 'standards compliant mode'
                width = w.document.documentElement.clientWidth;
                height = w.document.documentElement.clientHeight;
            } 
            else if (w.document.body && (w.document.body.clientWidth || w.document.body.clientHeight)) 
            {
                //IE 4 compatible
                width = w.document.body.clientWidth;
                height = w.document.body.clientHeight;
            }
            
            if (w.document.body && (w.document.body.scrollHeight || w.document.body.offsetHeight))
            {
                if (w.document.body.scrollHeight || w.document.body.scrollWidth)
                {
                    contentWidth = w.document.body.scrollWidth;
                    contentHeight = w.document.body.scrollHeight;
                }
                else
                {
                    contentWidth = w.document.body.offsetWidth;
                    contentHeight = w.document.body.offsetHeight;
                }
            }
            
            if (w.document.documentElement && (w.document.documentElement.scrollHeight || w.document.documentElement.offsetHeight))
            {
                if (w.document.documentElement.scrollHeight || w.document.documentElement.scrollWidth)
                {
                	if (w.document.documentElement.scrollWidth > contentWidth) { contentWidth = w.document.documentElement.scrollWidth; }
                    if (w.document.documentElement.scrollHeight > contentHeight) { contentHeight = w.document.documentElement.scrollHeight; }
                }
                else
                {
                	if (w.document.documentElement.offsetWidth > contentWidth) { contentWidth = w.document.documentElement.offsetWidth; }
                    if (w.document.documentElement.offsetHeight > contentHeight) { contentHeight = w.document.documentElement.offsetHeight; }
                }
            }
            
            if (contentWidth == 0 && contentHeight == 0)
            {
                contentWidth = width;
                contentHeight = height;
            }
            
            if (util.isSafari())
            {
                if (height > contentHeight) {
                    contentHeight = height;
                }
            }
            else
            {
                if (height > contentHeight) {
                    height = contentHeight;
                }
            }
                
            if (util.isSafari())
            {
                if (width > contentWidth) {
                    contentWidth = width;  
                }
            }
            else
            {
                if (width > contentWidth) {
                    width = contentWidth;
                }
            }
            
            var rect = {};
            rect.ScrollX = scrollX;
            rect.ScrollY = scrollY;
            rect.Width = width;
            rect.Height = height;
            rect.ContentWidth = contentWidth;
            rect.ContentHeight = contentHeight;
            
            return rect;
        },
        getCurrentStyleValue: function(element, styleRule, jsStyleRule, defaultValue)
        {
            if(element instanceof jQuery) { element = element.get(0); }            
            var value = '';
    
            try
            {
                if(document.defaultView && document.defaultView.getComputedStyle) {
                    value = document.defaultView.getComputedStyle(element, "").getPropertyValue(styleRule);
                }  else if(element.currentStyle) {
                    value = element.currentStyle[jsStyleRule];
                }
            } catch (e) {}
            
            if ((value == 'inherit' || value == 'transparent') && element.parentNode !== null) {
                return util.getCurrentStyleValue(element.parentNode, styleRule, jsStyleRule, defaultValue);
            } else if (value !== '' && value !== undefined && value != 'rgba(0, 0, 0, 0)') {
                return value;
            } else {
                return defaultValue;
            }
        },
        getStyleOffset: function(element)
        {
            if(element instanceof jQuery) { element = element.get(0); }
            
            var result = {};
    
            result.Height = util.isNanDefault(parseInt(util.getCurrentStyleValue(element, 'border-top-width', 'borderTopWidth', '0'), 10), 0) + 
                util.isNanDefault(parseInt(util.getCurrentStyleValue(element, 'border-bottom-width', 'borderBottomWidth', '0'), 10), 0) +
                util.isNanDefault(parseInt(util.getCurrentStyleValue(element, 'padding-top', 'paddingTop', '0'), 10), 0) +
                util.isNanDefault(parseInt(util.getCurrentStyleValue(element, 'padding-bottom', 'paddingBottom', '0'), 10), 0);
            
            result.Width = util.isNanDefault(parseInt(util.getCurrentStyleValue(element, 'border-left-width', 'borderLeftWidth', '0'), 10), 0) + 
                util.isNanDefault(parseInt(util.getCurrentStyleValue(element, 'border-right-width', 'borderRightWidth', '0'), 10), 0) +
                util.isNanDefault(parseInt(util.getCurrentStyleValue(element, 'padding-left', 'paddingLeft', '0'), 10), 0) +
                util.isNanDefault(parseInt(util.getCurrentStyleValue(element, 'padding-right', 'paddingRight', '0'), 10), 0);
            
            return result;
        },
        isNanDefault: function(value, defaultValue)
        {
            if (isNaN(value)) {
                return defaultValue;
            } else {
                return value;
            }
        },
        escapeForRegExp: function(value)
        {
            return value.replace(/([\\\(\^\$\*\+\?\{\}\.\)\|\-])/g, '\\$1');
        },
        getSelectedHtmlInElement: function(element, includeAllContentIfNoSelection, includeAllContentIfInvalidSelection, invalidSelectionMessage)
        {
            var selectionIsValid = true;
            var content = null;
            
            if (window.getSelection)
            {
                var selection = window.getSelection();
                if (selection && selection.rangeCount > 0 && selection.toString().length > 0)
                {
                    selectionIsValid = false;
                    var selectedRange = selection.getRangeAt(0);
                    var availableRange = document.createRange();
                    availableRange.selectNode(element);
                    
                    if (availableRange.compareBoundaryPoints(Range.START_TO_START, selectedRange) <= 0 && 
                        availableRange.compareBoundaryPoints(Range.END_TO_END, selectedRange) >= 0)
                    {
                        var temp = document.createElement('div');
                        temp.appendChild(selectedRange.cloneContents());
                        
                        content = temp.innerHTML;    
                    }
                    else if (invalidSelectionMessage) {
                        alert(invalidSelectionMessage);
                    }
                }
            }
            else if (document.selection)
            {
                var range = document.selection.createRange();
                if (range && range.text)
                {
                    selectionIsValid = false;        
                    var parent = range.parentElement();
                    if (parent !== null && util.elementContainsElement(element, parent)) {
                        content = range.htmlText;
                    } else if (invalidSelectionMessage) {
                        alert(invalidSelectionMessage);
                    }
                }
            }

            if (content === null && ((selectionIsValid && includeAllContentIfNoSelection) || includeAllContentIfInvalidSelection)) {
                content = element.innerHTML;
            }
            
            return content;
        },
        elementContainsElement: function(parent, child)
        {
            if(parent instanceof jQuery) { parent = parent.get(0); }
            if(child instanceof jQuery) { child = child.get(0); }
            
            if (!parent || !child) {
                return false;
            }

            if (parent == child) {
                return true;
            }
                
            if (parent && parent.childNodes)
            {
                for (var i = 0; i < parent.childNodes.length; i++)
                {
                    if (parent.childNodes[i] == child || util.elementContainsElement(parent.childNodes[i], child)) {
                        return true;
                    }
                }
            }
            
            return false;
        },
        getCurrentCursorIndex: function(inputElement)
        {
            if(inputElement instanceof jQuery) { inputElement = inputElement.get(0); }
            
            var index = 0;
            if (inputElement.selectionStart || inputElement.selectionStart == '0') {
                index = inputElement.selectionStart;
            } else if (document.selection) {
                var originalValue = inputElement.value;
                var range = document.selection.createRange();
                var escapeChar = String.fromCharCode(1);
                range.text = escapeChar;
                index = inputElement.value.indexOf(escapeChar);
                inputElement.value = originalValue;
            }              
            return index;
        },
        isSafari: function()
        {
            return navigator.userAgent.indexOf('Safari') != -1;
        },
        isOpera: function()
        {
            return window.opera !== null;
        },
        isIE: function()
        {
            return (navigator && navigator.userAgent && navigator.userAgent.indexOf('MSIE') != -1);
        },
        isIE8: function()
        {
            if (util.isIE()) {
                return parseInt(/MSIE ([0-9]*)\./.exec(navigator.userAgent)[1], 9) >= 8;
            }
            return false;
        },
        setCurrentCursorSelection: function(inputElement, startIndex, endIndex, functionKeyWasHandled, persistedValue)
        {
            if(inputElement instanceof jQuery) { inputElement = inputElement.get(0); }
            
            if (util.isSafari() || util.isOpera())
            {
                if (functionKeyWasHandled)
                {
                    var state = $(inputElement).data('telligentUtility');
                    if (state && state.CursorTimeout) {
                        window.clearTimeout(state.CursorTimeout);
                    }
                    
                    state = {};
                    state.InputElement = inputElement;
                    state.StartIndex = startIndex;
                    state.EndIndex = endIndex;
                    state.PersistedValue = persistedValue;
                    
                    $(inputElement).data('telligentUtility', state);
                    
                    state.CursorTimeout = window.setTimeout(function() { _delayedSetCurrentCursorSelection(inputElement); }, 9);
                    
                    return;
                }
                else
                {
                    inputElement.focus();
                    if (persistedValue) {
                        inputElement.value = persistedValue;
                    }
                }
            }
            
            try
            {
                if (document.selection)
                {
                    var range = inputElement.createTextRange();
                    range.move('character', startIndex);
                    range.moveEnd('character', endIndex - startIndex);
                    range.select();
                }
                else if (inputElement.setSelectionRange)
                {
                    inputElement.setSelectionRange(startIndex, endIndex);
                    inputElement.focus();
                }
                else if (inputElement.selectionStart || inputElement.selectionStart == '0')
                { 
                    inputElement.selectionStart = startIndex;
                    inputElement.selectionEnd = endIndex;
                }
            } catch (e) { /* ignore */ }
        },
        hideElementsInWindow: function(element, w, hideAll)
        {
            if(element instanceof jQuery) { element = element.get(0); }
            
            var elementHidingSelect = null;
            var elementRect = util.getElementInfo(element);
            
            var subWindows = w.document.getElementsByTagName('iframe');
            for (var i = 0; i < subWindows.length; i++)
            {
                if (!util.elementContainsElement(element, subWindows[i]))
                {
                    try
                    {
                        util.hideElementsInWindow(element, subWindows[i].contentWindow, hideAll);
                    }
                    catch (e)
                    {
                        var objectInfo = util.getElementInfo(subWindows[i]);
                        if (hideAll || (objectInfo.Top < elementRect.Bottom && elementRect.Top < objectInfo.Bottom && objectInfo.Left < elementRect.Right && elementRect.Left < objectInfo.Right))
                        {
                            if (elementHidingSelect === null) {
                                elementHidingSelect = _addElementHidingSelect(element);
                            }
                            
                            _addHiddenSelect(elementHidingSelect, subWindows[i]);
                            
                             subWindows[i].style.visibility = 'hidden';
                        }
                    }
                }
            }
            
            var tagsToHide = ['object', 'embed', 'applet'];
            if (util.isIE()) {
                tagsToHide[tagsToHide.length] = 'select';
            }
            
            for (var t = 0; t < tagsToHide.length; t++)
            {
                var objects = document.getElementsByTagName(tagsToHide[t]);
                for (var j = 0; j < objects.length; j++)
                {
                	if (!util.elementContainsElement(element, objects[j]))
                	{
	                    var objectInfo = util.getElementInfo(objects[j]);
	                    if (hideAll || (objectInfo.Top < elementRect.Bottom && elementRect.Top < objectInfo.Bottom && objectInfo.Left < elementRect.Right && elementRect.Left < objectInfo.Right))
	                    {
	                        if (elementHidingSelect === null) {
	                            elementHidingSelect = _addElementHidingSelect(element);
	                        }
	                        
	                        _addHiddenSelect(elementHidingSelect, objects[j]);
	                        
	                        objects[j].style.visibility = 'hidden';
	                    }
	                }
                }
            }
        },
        hideSelectBoxes: function(element, hideAll)
        {
            if(element instanceof jQuery) { element = element.get(0); }
            
            util.hideElementsInWindow(element, window.window, hideAll);
        },
        showSelectBoxes: function(element)
        {
            if(element instanceof jQuery) { element = element.get(0); }
            
            var elementHidingSelect = null;
            for (var i = 0; i < _elementsHidingSelects.length && elementHidingSelect === null; i++)
            {
                if (_elementsHidingSelects[i])
                {
                    if (_elementsHidingSelects[i].Element == element) {
                        elementHidingSelect = _elementsHidingSelects[i];
                    }
                }
            }
            
            if (elementHidingSelect === null) {
                return;
            }

            var hiddenSelect;
            for (var h = 0; h < elementHidingSelect.HiddenSelectsIndeces.length; h++)
            {
                hiddenSelect = _hiddenSelects[elementHidingSelect.HiddenSelectsIndeces[h]];
                if (hiddenSelect)
                {
                    if (hiddenSelect.ElementsHidingSelectsIndeces.length == 1)
                    {
                        try
                        {
                            // show select 
                            if (hiddenSelect.SelectElement.style.visibility == 'hidden') {
                                hiddenSelect.SelectElement.style.visibility = hiddenSelect.OriginalVisibility;
                            }
                        }
                        catch (e)
                        {
                        }
                            
                        _hiddenSelects[hiddenSelect.Index] = null;
                    }
                    else
                    {
                        // find in list and remove
                        var elementsHidingSelects = [];
                        for (var j = 0; j < hiddenSelect.ElementsHidingSelectsIndeces.length; j++)
                        {
                            if (hiddenSelect.ElementsHidingSelectsIndeces[j] != elementHidingSelect.Index) {
                                elementsHidingSelects[elementsHidingSelects.length] = hiddenSelect.ElementsHidingSelectsIndeces[h];
                            }
                        }
                        hiddenSelect.ElementsHidingSelectsIndeces = elementsHidingSelects;
                    }
                }
            }

            _elementsHidingSelects[elementHidingSelect.Index] = null;
        },
        makeBoundFunction: function(f, context, args)
        {
            var _f = f;
            var _context = context;
            var _args = args;
            return (function() {
                return _f.apply(_context, _args);
            });
        }
    };

    // expose utility methods publicly
    if(typeof $.telligent === 'undefined') {
        $.telligent = {};
    }
    if(typeof $.telligent.glow === 'undefined') {
        $.telligent.glow = {};
    }
    if(typeof $.telligent.glow.utility === 'undefined') {
        $.telligent.glow.utility = util;
    }    
})(jQuery); 
 
/// @name glowTransition
/// @category jQuery Plugin
/// @description Transitions display of one element with another
/// 
/// ### jQuery.fn.glowTransition
/// 
/// Smoothly transitions an element into another, resizing the first to match the second and blending the second's contents over the first.
/// 
/// ### Usage
/// 
///     $('SOURCE SELECTOR').glowTransition('TARGET SELECTOR', options)
/// 
/// where 'source selector' contains the element(s) which will be transitioned out, 'target selector' contains the elements which will be transitioned in, and the optional `options` object supports the following:
/// 
/// ### Options
/// 
///  * `duration`: animation duration in milliseconds
///    * default: 200
///  * `type`: type of blend to perform: fade, slideUp, slideDown, slideLeft, slideRight
///    * default: 'fade'
///  * `complete`: callback to perform on completion
///  * `resize`: whether or not transition a includes resize from the first to second.  
///    * default: true
/// 

/*
smoothly transitions an element into another, resizing the first to match the second and blending the second's contents over the first.

the blend can replace the first's via fade or directional slides.

$(sourceselector).glowTransition(options)
options:
	duration: 300	// animation duration
	type: 'fade'	// fade, slideUp, slideDown, slideLeft, slideRight
	complete: fn	// on-complete callback
	resize: true	// whether or not transition includes resize from first to second
*/
(function($){

	$.fn.glowTransition = function(content, options) {
		var target = $(content).hide();
		var settings = $.extend({}, $.fn.glowTransition.defaults, options || {});
		return this.each(function(){
			var source = $(this);
			var sourceStyle = {
				width: source.outerWidth(),
				height: source.outerHeight(),
				left: source.css('left'),
				top: source.css('top'),
				position: 'relative',
				overflow: 'hidden'
			};
			var targetStyle = {
				rawWidth: target.width(),
				rawHeight: target.height(),
				width: target.outerWidth(),
				height: target.outerHeight()
			};

			source
				.wrap($('<div></div>').css(sourceStyle))
				.css({ position: 'absolute'});
			var wrapper = source.parent();

			var targetStartingStyle = { position: 'absolute' },
				sourceEndingStyle = {},
				targetEndingStyle = {};

			if(settings.resize) {
				$.extend(targetStartingStyle, {
					width: sourceStyle.width,
					height: sourceStyle.height
				});
				$.extend(sourceEndingStyle, {
					width: targetStyle.width,
					height: targetStyle.height
				});
				$.extend(targetEndingStyle, {
					width: targetStyle.rawWidth,
					height: targetStyle.rawHeight
				});
			}
			switch(settings.type) {
				case 'slideLeft':
					targetStartingStyle.left = ('+' + targetStyle.width + 'px');
					sourceEndingStyle.left = ('-=' + targetStyle.width);
					targetEndingStyle.left = 0;
					break;
				case 'slideRight':
					targetStartingStyle.left = ('-' + targetStyle.width + 'px');
					sourceEndingStyle.left = ('+=' + targetStyle.width);
					targetEndingStyle.left = 0;
					break;
				case 'slideUp':
					targetStartingStyle.top = ('+' + targetStyle.height + 'px');
					sourceEndingStyle.top = ('-=' + targetStyle.height);
					targetEndingStyle.top = 0;
					break;
				case 'slideDown':
					targetStartingStyle.top = ('-' + targetStyle.height + 'px');
					sourceEndingStyle.top = ('+=' + targetStyle.height);
					targetEndingStyle.top = 0;
					break;
				case 'fade':
					targetStartingStyle.opacity = 0.0;
					sourceEndingStyle.opacity = 0.0;
					targetEndingStyle.opacity = 1.0;
					break;
			}

			target
				.prependTo(wrapper)
				.css(targetStartingStyle)
				.show();
			source
				.stop()
				.animate(sourceEndingStyle, settings.duration);
			target
				.stop()
				.animate(targetEndingStyle, settings.duration+1, function() {
					target
						.css({ position: 'static', top: '0px', left: '0px' })
						.unwrap();
					source.remove();
					settings.complete();
				});
			wrapper
				.stop()
				.animate({ width: targetStyle.width, height: targetStyle.height }, settings.duration);
		});
	};
	$.fn.glowTransition.defaults = {
		complete: function() {},
		duration: 200,
		resize: true,
		type: 'fade' // fade, slideLeft, slideRight, slideUp, slideDown
	};

}(jQuery)); 
 
/// @name glowTree
/// @category jQuery Plugin
/// @description Renders a tree of nodes which can be navigated, manipulated, and re-arranged
/// 
/// ### jQuery.fn.glowTree
/// 
/// Renders a tree of nodes which can be navigated, manipulated, and re-arranged
/// 
/// ### Usage
/// 
///     $('SELECTOR').glowTree(options)
/// 
/// where 'SELECTOR' is a `select` element to be replaced with a tree control
/// 
/// ### Options
/// 
///  * `enableDragDrop`: when enabled, allows rearranging of nodes within a tree via drag and drop
///    * default: *false*
///  * `getSubNodes`: callback function which defines how to retrieve sub-nodes for a given tree and node.  Callback is passed parameters:
///    0. tree
///    1. node   
///  * `canHaveChildren`: whether a tree can have child nodes
///    * default: *true*
///  * `nodeHtml`: Array of strings, one per node
///    * default: *[]*
///  * `draggableTrees`: when provided the id of another glowTree, nodes can be dragged from this tree to the tree identified by the id
///    * default: *null*
///  * `expandedImageUrl`: URL of image used alongside expanded tree nodes
///    * default: *''*
///  * `collapsedImageUrl`: URL of image used alongside collapsed tree nodes 
///    * default: *''*
///  * `blankImageUrl`: URL of blank image for leaf nodes
///    * default: *''*
/// 
/// ### Events
/// 
///  * `glowTreeNodeSelected` - triggered when a tree node is selected
///  * `glowTreeContextMenu` - triggered when a tree node is right-clicked
///  * `glowTreeNodeRemoved` - triggered when a tree node is removed from the tree
///  * `glowTreeNodeMoved` - triggered when a tree node is moved in a tree
///  * `glowTreeNodeAdded` - triggered when a tree node is added
/// 
/// ### Methods
/// 
/// #### getParentAndIndex
/// 
/// Returns the parent node and index based on a given coordinate pair.  Generally used internally when dragging nodes between trees.
/// 
///     var nodeAndIndex = $('SELECTOR').glowTree('getParentAndIndex', x, y);
/// 
/// #### highlight
/// 
/// Sets whether a tree is highlighted
/// 
///     $('SELECTOR').glowTree('highlight', true);
///     $('SELECTOR').glowTree('highlight', false);
/// 
/// #### addDraggedNode
/// 
/// Adds a dragged node into a linked tree
/// 
///     $('SELECTOR').glowTree('addDraggedNode', node, nodeAndIndex.parentNode, nodeAndIndex.index, event);
/// 
/// #### selected
/// 
/// When not passed a node, returns the currently-selected node.  When passed a node, selects the node in the tree
/// 
///     var selectedNode = $('SELECTOR').glowTree('selected');
///     $('SELECTOR').glowTree('selected', node);
/// 
/// #### resize
/// 
/// Resizes the tree to a given width and height in pixels
/// 
///     $('SELECTOR').glowTree('resize', x, y)
/// 
/// #### disabled
/// 
/// When not passed a value, returns whether the tree is disabled for interaction.  When passed a boolean value, enables or disables the tree.
/// 
///     var isDisabled = $('SELECTOR').glowTree('disabled');
///     $('SELECTOR').glowTree('disabled', true);
///     $('SELECTOR').glowTree('disabled', false);
/// 
/// #### refresh
/// 
/// Re-renders the tree based on the currently-applied set of tree nodes
/// 
///     $('SELECTOR').glowTree('refresh')
/// 
/// #### add
/// 
/// Adds a new tree node to a tree
/// 
///     var newNode = $('SELECTOR').glowTree('createTreeNode', {
///         value: 'abc',
///         html: 'ABC'        
///     });
///     $('SELECTOR').glowTree('add', newNode);
/// 
/// #### remove
/// 
/// Removes a given tree node instance from a tree
///     
///     var node = $('SELECTOR').glowTree('getByValue', 'abc');
///     $('SELECTOR').glowTree('remove', node);
/// 
/// #### insert
/// 
/// Adds a new tree node to a tree at a given position
/// 
///     var newNode = $('SELECTOR').glowTree('createTreeNode', {
///         value: 'abc',
///         html: 'ABC'        
///     });
///     $('SELECTOR').glowTree('add', newNode, 5);
/// 
/// #### getByValue
/// 
/// Find and return an existing node based on the node's value
/// 
///     var node = $('SELECTOR').glowTree('getByValue', 'abc')
/// 
/// #### getIndex
/// 
/// Return the position index for a given node
/// 
///     var index = $('SELECTOR').glowTree('getIndex', node)
/// 
/// #### getByIndex
/// 
/// Find and return an existing node based on the node's position index
/// 
///     var node = $('SELECTOR').glowTree('getByIndex', 4)
/// 
/// #### count
/// 
/// Returns the number of nodes in a given tree
/// 
///     var count = $('SELECTOR').glowTree('count')
/// 
/// #### createTreeNode
/// 
/// Creates a new tree node suitable for adding or inserting into a tree
/// 
///     var newNode = $('SELECTOR').glowTree('createTreeNode', {
///         value: 'abc',
///         html: 'ABC'        
///     });
/// 

(function($){

    var _eventNames = {
            nodeSelected: 'glowTreeNodeSelected',
            contextMenu: 'glowTreeContextMenu',
            nodeRemoved: 'glowTreeNodeRemoved',
            nodeMoved: 'glowTreeNodeMoved',
            nodeAdded: 'glowTreeNodeAdded'
        },
        _init = function(options) 
        {
            return this.each(function() 
            {               
                var context = {
                    settings: $.extend({}, $.fn.glowTree.defaults, options || {}),
                    internal: {
                        state: this,
                        wrapper: null,
                        container: null,
                        selectedNode: null,
                        nodes: [],
                        backgroundColor: null,
						movingNode: null,
						movingNodeStartParent: null,
						draggingInitialized: false,
						pagePosition: null,
						movingNodeIsFloating: false,
						movingNodeFloat: null,
						nodeHeightsCache: null,
						movingNodeStartIndex: null,
						movingNodeLastIndex: null,
						moveStartY: null,
						moveScrollStartY: null,
						draggableTrees: [],
						initialized: false
                    }
                };                  
            
                $(this).data('tree', context);
                
                _initialize(context);
            });
        },  
		_dispose = function(context)
		{
			if (context.internal.initialized)
			{
				context.internal.initialized = false;
				
				context.internal.state = null;
				context.internal.container = null;
				context.internal.movingNodeFloat = null;
				
				for (var i = 0; i < context.internal.nodes.length; i++)
				{
					context.internal.nodes[i]._dispose();
				}
			}
		},
		_getCommonParent = function (context, node1, node2)
		{
			var parents1 = new Array();
			var tNode = node1;
			while (tNode)
			{
				parents1[parents1.length] = tNode;
				tNode = tNode._parent;
			}
			
			tNode = node2;
			while (tNode)
			{
				for (var i = parents1.length - 1; i >= 0; i--)
				{
					if (parents1[i] == tNode)
						return tNode;
				}
				
				tNode = tNode._parent;
			}
			
			return null;
		}, 
		_setCursor = function(context, cursor)
		{
			for (var i = 0; i < context.internal.nodes.length; i++)
			{
				context.internal.nodes[i]._setCursor(cursor);
			}

			context.internal.wrapper.style.cursor = cursor; 
		}, 
		_isMouseOver = function(context, x, y, allowMargin)
		{
			if (context.internal.pagePosition == null)
				_refreshPagePosition(context);
			
			var left = context.internal.pagePosition.Left;
			var right = context.internal.pagePosition.Left + context.internal.pagePosition.Width
			var top = context.internal.pagePosition.Top;
			var bottom = context.internal.pagePosition.Top + context.internal.pagePosition.Height;
			
			if (allowMargin)
			{
				left -= 10;
				right += 10;
				top -= 10;
				bottom += 10;
			}
			
			return (x > left && x < right && y > top && y < bottom);
		},
		_getNodeHeight = function(context, node)
		{
			if (node.expanded() && node.count() > 0)
				return node._element.offsetHeight - node._element.childNodes[2].offsetHeight;
			else
				return node._element.offsetHeight;
		}, 
		_populateNodeHeightsCache = function(context, nodes, node)
		{
			for (var i = 0; i < nodes.length; i++)
			{
				if (nodes[i]._element)
				{
					if (nodes[i] === node)
					{
						var nodeHeight = new Object();
						nodeHeight.parentNode = nodes[i]._parent;
						nodeHeight.height = nodes[i]._element.offsetHeight;
						nodeHeight.index = i;
						context.internal.nodeHeightsCache[context.internal.nodeHeightsCache.length] = nodeHeight;
						context.internal.movingNodeStartIndex = context.internal.nodeHeightsCache.length - 1;
						context.internal.movingNodeLastIndex = context.internal.nodeHeightsCache.length - 1;
					}
					else
					{
						var nodeHeight = new Object();
						nodeHeight.parentNode = nodes[i]._parent;
						nodeHeight.height = _getNodeHeight(context, nodes[i]);
						nodeHeight.index = i;
						nodeHeight.isBeforeNode = nodes[i]._parent == node._parent;
						context.internal.nodeHeightsCache[context.internal.nodeHeightsCache.length] = nodeHeight;
						
						var fractionHeight = nodeHeight.height * .25;
						
						if (nodes[i].expanded())
						{
							if (nodes[i]._nodes.length > 0)
								_populateNodeHeightsCache(context, nodes[i]._nodes, node);
							else if (nodes[i].canHaveChildren())
							{
								nodeHeight.height -= fractionHeight;
									
								var subNodeHeight = new Object();
								subNodeHeight.parentNode = nodes[i];
								subNodeHeight.height = fractionHeight;
								subNodeHeight.index = 0;
								context.internal.nodeHeightsCache[context.internal.nodeHeightsCache.length] = subNodeHeight;
							}
						}
						
						var afterNodeHeight = null;
						if (nodes[i]._parent == node._parent)
						{
							afterNodeHeight = new Object();
							afterNodeHeight.parentNode = nodes[i]._parent;
							afterNodeHeight.height = nodeHeight.height;
							afterNodeHeight.index = i;
							afterNodeHeight.isAfterNode = true;
							context.internal.nodeHeightsCache[context.internal.nodeHeightsCache.length] = afterNodeHeight;
						}
						
						if (i == nodes.length - 1)
						{
							nodeHeight.height -= fractionHeight;
							if (afterNodeHeight)
								afterNodeHeight.height = nodeHeight.height;
							
							// insert holder for *last node*
							nodeHeight = new Object();
							nodeHeight.parentNode = nodes[i]._parent;
							nodeHeight.height = fractionHeight;
							nodeHeight.index = i + 1;
							context.internal.nodeHeightsCache[context.internal.nodeHeightsCache.length] = nodeHeight;
						}
					}
				}
			}
		}, 
		_getMovingParentNodeAndIndex = function(context, originalIndex, yOffset)
		{
			var parentNodeAndIndex = new Object();
			parentNodeAndIndex.parentNode = null;
			parentNodeAndIndex.index = -1;
			parentNodeAndIndex.nodeHeightsCacheIndex = originalIndex;
			
			if (context.internal.nodeHeightsCache && originalIndex >= 0 && originalIndex < context.internal.nodeHeightsCache.length)
			{
				var newIndex = originalIndex;
				var movingDown = yOffset > 0;
				var done = false;
				while (!done)
				{
					if (yOffset > 0)
					{
						if (newIndex < context.internal.nodeHeightsCache.length - 1 && (yOffset > context.internal.nodeHeightsCache[newIndex + 1].height || context.internal.nodeHeightsCache[newIndex + 1].isBeforeNode))
						{
							newIndex++;
							if (!context.internal.nodeHeightsCache[newIndex].isBeforeNode)
								yOffset -= context.internal.nodeHeightsCache[newIndex].height;
						}
						else
							done = true;
					}
					else if (yOffset < 0)
					{
						if (newIndex > 0 && (-yOffset > context.internal.nodeHeightsCache[newIndex - 1].height || context.internal.nodeHeightsCache[newIndex - 1].isAfterNode))
						{
							newIndex--;
							if (!context.internal.nodeHeightsCache[newIndex].isAfterNode)
								yOffset += context.internal.nodeHeightsCache[newIndex].height;	
						}
						else
							done = true;
					}
					else
						done = true;
				}
				
				if (movingDown)
				{
					while (context.internal.nodeHeightsCache[newIndex].isBeforeNode && newIndex > 0)
						newIndex--;
				}
				else
				{
					while (context.internal.nodeHeightsCache[newIndex].isAfterNode && newIndex < context.internal.nodeHeightsCache.length - 1)
						newIndex++;
				}

				parentNodeAndIndex.parentNode = context.internal.nodeHeightsCache[newIndex].parentNode;
				parentNodeAndIndex.index = context.internal.nodeHeightsCache[newIndex].index;
				parentNodeAndIndex.nodeHeightsCacheIndex = newIndex;
			}
			
			return parentNodeAndIndex;
		}, 
		_initialize = function(context)
		{
			if(!context.internal.state.offsetHeight)
			{
				window.setTimeout(function() { _initialize(context); }, 249);
				return;
			}
			
			if (context.internal.state.disabled)
			{
				context.internal.state.disabled = false;
				context.internal.state.readonly = true;
			}
			
			context.internal.backgroundColor = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'background-color', 'backgroundColor', '#ffffff');
			if (context.internal.backgroundColor.replace(/\#[0-9a-f]*/gi, '') != '')
				context.internal.backgroundColor = '#ffffff';
			
			context.internal.wrapper = document.createElement('div');
			context.internal.wrapper.style.backgroundColor = context.internal.backgroundColor;
			context.internal.wrapper.style.fontFamily = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-family', 'fontFamily', 'Arial, Helvetica');
			context.internal.wrapper.style.fontSize = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'font-size', 'fontSize', '100%');
			context.internal.wrapper.style.lineHeight = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'line-height', 'lineHeight', '100%');
			context.internal.wrapper.style.color = $.telligent.glow.utility.getCurrentStyleValue(context.internal.state, 'color', 'color', '#000000');
			context.internal.wrapper.style.borderStyle = 'outset';
			context.internal.wrapper.style.borderWidth = '1px';
			context.internal.wrapper.style.borderColor = '#999999';
			context.internal.wrapper.style.padding = '2px';
			context.internal.wrapper.style.overflow = 'auto';
			context.internal.wrapper.style.width = (context.internal.state.offsetWidth - 4) + 'px';
		    context.internal.wrapper.style.height = (context.internal.state.offsetHeight - 4) + 'px';
		    context.internal.wrapper.style.cursor = 'default';
		    context.internal.wrapper.style.position = 'relative';

		    context.internal.container = document.createElement('ul');
		    context.internal.container.style.listStyleType = 'none';
		    context.internal.container.style.margin = '0';
		    context.internal.container.style.whiteSpace = 'nowrap';
		    context.internal.container.style.padding = '0';
		    context.internal.wrapper.appendChild(context.internal.container);

			context.internal.state.parentNode.insertBefore(context.internal.wrapper, context.internal.state);

			_parseNodes(context, null, context.internal.state.options, 0, context.settings.nodeHtml, 0);
			
			context.internal.state.style.display = 'none';
			
			context.internal.initialized = true;
			
			api.disabled.apply($(context.internal.state), [context.internal.state.readonly === true]);
			
			if (context.internal.state.selectedIndex >= 0)
			{
				var node = api.getByValue.apply($(context.internal.state), [context.internal.state.options[context.internal.state.selectedIndex].value]);
				if (node)
					api.selected.apply($(context.internal.state), [node]);
			}
		},
		_parseNodes = function(context, parentTreeNode, optionsList, currentOptionIndex, nodeHtml, currentLevel)
		{
			var currentNode, i, html, value, isExpanded, isLoaded, level, matches, canHaveChildren;
			
			for (i = currentOptionIndex; i < optionsList.length; i++)
			{
				var optionText = optionsList[i].text;
				matches = optionText.match(/^(\-*) /);
				if (matches)
				{
					level = matches[1].length / 2;
					optionText = optionText.substr(matches[1].length);
				}
				else
					level = 0;
				
				if (level == currentLevel)
				{
					value = optionsList[i].value;
					html = nodeHtml.shift();
					if (!html)
						html = optionText;
					
					currentNode = new treeNode({html: html, value: value});
					
					if (parentTreeNode)
						parentTreeNode.add(currentNode);
					else
					{
						currentNode._parent = null;
						currentNode._treeContext = context;
						context.internal.nodes[context.internal.nodes.length] = currentNode;
					}
				}
				else if (level > currentLevel && currentNode)
				{
					i += _parseNodes(context, currentNode, optionsList, i, nodeHtml, currentLevel + 1)	
				}
				else
					return i - currentOptionIndex - 1;
			}
			
			return optionsList.length - currentOptionIndex - 1;
		}, 
		_scrollToNode = function(context, node, showChildren)
		{
			var top, left, height, element;

			if (showChildren)
			{
				element = node._element;
				top = element.offsetTop;
				height = _getNodeHeight(context, node);
				left = element.offsetLeft;
			}
			else
			{
				element = node._element.childNodes[1];
				top = element.offsetTop;
				height = _getNodeHeight(context, node);
				left = element.offsetLeft - node._element.childNodes[0].offsetWidth;
			}
			
			top -= 6;
			left -= 6;
			
			var parent = element.offsetParent;
			while (parent && parent != context.internal.wrapper)
			{
				top += parent.offsetTop;
				left += parent.offsetLeft;
				parent = parent.offsetParent;
			}
			
			var wrapperWidth = context.internal.wrapper.offsetWidth - 24;
			if (left + element.offsetWidth > context.internal.wrapper.scrollLeft + wrapperWidth && element.offsetWidth < wrapperWidth)
				context.internal.wrapper.scrollLeft = left;
			else if (left < context.internal.wrapper.scrollLeft || left + element.offsetWidth > context.internal.wrapper.scrollLeft + wrapperWidth)
				context.internal.wrapper.scrollLeft = left;
			
			var wrapperHeight = context.internal.wrapper.offsetHeight - 24;
			if (top + height > context.internal.wrapper.scrollTop + wrapperHeight && height < wrapperHeight)
				context.internal.wrapper.scrollTop = (top + height) - wrapperHeight;
			else if (top < context.internal.wrapper.scrollTop || top + height > context.internal.wrapper.scrollTop + wrapperHeight)
				context.internal.wrapper.scrollTop = top;
		}, 
		_initializeMoveFloat = function(context, node)
		{
			if (!context.internal.movingNodeFloat)
			{
				context.internal.movingNodeFloat = document.createElement('div');
				context.internal.movingNodeFloat.style.position = 'absolute';
				context.internal.movingNodeFloat.style.opacity = '.75';
				if ($.telligent.glow.utility.isIE())
					context.internal.movingNodeFloat.style.filter = 'progid:DXImageTransform.Microsoft.Alpha(opacity=75)';

				context.internal.movingNodeFloat.style.backgroundColor = '#cccccc'; 
				context.internal.movingNodeFloat.style.borderColor = '#666666';
				context.internal.movingNodeFloat.style.fontFamily = context.internal.wrapper.style.fontFamily;
				context.internal.movingNodeFloat.style.fontSize = context.internal.wrapper.style.fontSize;
				context.internal.movingNodeFloat.style.lineHeight = context.internal.wrapper.style.lineHeight;
				context.internal.movingNodeFloat.style.color = context.internal.wrapper.style.color;
				context.internal.movingNodeFloat.style.padding = '2px';
				context.internal.movingNodeFloat.style.cursor = 'move';
				context.internal.movingNodeFloat.style.borderStyle = 'dotted';
				context.internal.movingNodeFloat.style.borderWidth = '1px';
				
				document.body.appendChild(context.internal.movingNodeFloat);
			}
			
			while (context.internal.movingNodeFloat.childNodes.length > 0)
				context.internal.movingNodeFloat.removeChild(context.internal.movingNodeFloat.childNodes[context.internal.movingNodeFloat.childNodes.length - 1]);
			
			context.internal.movingNodeFloat.style.width = node._element.offsetWidth + 'px';
			context.internal.movingNodeFloat.style.height = node._element.offsetHeight + 'px';
			context.internal.movingNodeFloat.innerHTML = node._element.innerHTML;
		}, 
		_initializeDraggableTrees = function(context)
		{
			context.internal.draggableTrees = new Array();
			if (context.settings.draggableTrees)
			{
				context.settings.draggableTrees.each(function()
				{
					try
					{
						if ($(this).glowTree('disabled') === false)
							context.internal.draggableTrees[context.internal.draggableTrees.length] = $(this);
					} catch (e) {};
				});
			}
		}, 
		_initializeDragging = function(context)
		{
			_refreshPagePosition(context);		
			_initializeDraggableTrees(context);
			var i;
			for (i = 0; i < context.internal.draggableTrees.length; i++)
			{
				context.internal.draggableTrees[i].glowTree('highlight', true);
			}

			if (context.internal.draggableTrees.length > 0)
				_setCursor(context, 'move');
			else
				_setCursor(context, 'n-resize');
			
			context.internal.draggingInitalized = true;
		},
		_refreshPagePosition = function(context)
		{
			context.internal.pagePosition = $.telligent.glow.utility.getElementInfo(context.internal.wrapper);
		}, 
		_isMoving = function(context)
		{
			return context.internal.movingNode != null;
		},		
		_nodeMouseDown = function (context, event, node)
		{
			if (!event)
				event = window.event;
			
			if (!_isMoving(context))
			{
				context.internal.movingNode = node;
				context.internal.movingNodeStartParent = node.getParent();
				context.internal.draggingInitalized = false;
				if (typeof(event.pageY) != 'undefined')
					context.internal.moveStartY = event.pageY;
				else
				{
					var windowInfo = $.telligent.glow.utility.getWindowInfo();
					context.internal.moveStartY = event.clientY + windowInfo.ScrollY;
				}
				context.internal.moveScrollStartY = context.internal.wrapper.scrollTop;
				context.internal.nodeHeightsCache = new Array();
				context.internal.wrapper.style.overflow = 'hidden'; 
				_populateNodeHeightsCache(context, context.internal.nodes, node);
				
				$(document)
					.bind('mouseup.glowTree', function(event) { _endMoveTracking(context, event); return false; })
					.bind('mousemove.glowTree', function(event) { _mouseMoveHandler(context, event); return false; });
			}
		},
		_nodeContextMenu = function (context, event, element, node)
		{
			if (!event)
				event = window.event;
			
			$(context.internal.state).trigger(_eventNames.contextMenu, [node, element]);
		}, 
		_mouseMoveHandler = function(context, event)
		{
			if (!event)
				event = window.event;
			
			var eventY = 0, eventX = 0;
			if (typeof(event.pageY) != 'undefined')
			{
				eventY = event.pageY;
				eventX = event.pageX;
			}
			else
			{
				var windowInfo = $.telligent.glow.utility.getWindowInfo();
				eventY = event.clientY + windowInfo.ScrollY;
				eventX = event.clientX + windowInfo.ScrollX;
			}
			
			if (!_isMoving(context))
				return;
			
			if (!context.internal.draggingInitalized)
				_initializeDragging(context);
			
			var yChange = eventY - context.internal.moveStartY;
			yChange += context.internal.wrapper.scrollTop - context.internal.moveScrollStartY;	
			var parentAndIndex = _getMovingParentNodeAndIndex (context, context.internal.movingNodeStartIndex, yChange);
			
			if (context.internal.draggableTrees.length > 0 && !context.internal.movingNodeIsFloating && !_isMouseOver(context, eventX, eventY, true))
			{
				// make floating
				_initializeMoveFloat(context, context.internal.movingNode);
				context.internal.movingNodeIsFloating = true;
				context.internal.movingNodeFloat.style.display = 'block';
			}
			
			if (context.internal.movingNodeIsFloating)
			{
				// check for overlap of other ordered lists
				context.internal.movingNodeFloat.style.left = (eventX - (context.internal.movingNodeFloat.offsetWidth / 2)) + 'px';
				context.internal.movingNodeFloat.style.top = (eventY - (context.internal.movingNodeFloat.offsetHeight / 2)) + 'px';
				
				if (_isMouseOver(context, eventX, eventY, true))
				{
					context.internal.movingNodeFloat.style.display = 'none';
					context.internal.movingNodeIsFloating = false;
				}
				else
				{
					var externalNodeAndIndex = null;
					for (var i = 0; i < context.internal.draggableTrees.length; i++)
					{
						if ((externalNodeAndIndex = context.internal.draggableTrees[i].glowTree('getParentAndIndex', eventX, eventY)).index != -1)
						{
							var node = context.internal.movingNode;
							_endMoveTracking(context, event, true);
							api.remove.apply($(context.internal.state), [node]);
							api.refresh.apply($(context.internal.state), []);
							
							$(context.internal.state).trigger(_eventNames.nodeRemoved, [node]);
							
							context.internal.draggableTrees[i].glowTree('addDraggedNode', node, externalNodeAndIndex.parentNode, externalNodeAndIndex.index, event);
							context.internal.movingNodeFloat.style.display = 'none';
							context.internal.movingNodeIsFloating = false;
						}
					}
				}
			}

			if (_isMoving(context) && !context.internal.movingNodeIsFloating && parentAndIndex.nodeHeightsCacheIndex != context.internal.movingNodeLastIndex)
			{
				context.internal.movingNodeLastIndex = parentAndIndex.nodeHeightsCacheIndex;
				var previousParent = context.internal.movingNode._parent;
				
				if (parentAndIndex.parentNode)
					parentAndIndex.parentNode.insert(context.internal.movingNode, parentAndIndex.index);
				else
					api.insert.apply($(context.internal.state), [context.internal.movingNode, parentAndIndex.index]);
				
				var commonParent = _getCommonParent(context, previousParent, parentAndIndex.parentNode);
				if (commonParent)
					commonParent.refresh();
				else
					api.refresh.apply($(context.internal.state), []);	
				
				api.selected.apply($(context.internal.state), [context.internal.movingNode]);
			}
		}, 
		_endMoveTracking = function(context, event, ignoreMoveEvents)
		{
			_setCursor(context, 'default');
			
			if (!ignoreMoveEvents && context.internal.movingNode && context.internal.movingNodeStartIndex != context.internal.movingNodeLastIndex)
				$(context.internal.state).trigger(_eventNames.nodeMoved, [context.internal.movingNode, context.internal.movingNodeStartParent]);
			
			context.internal.wrapper.style.cursor = 'auto';
			context.internal.wrapper.style.overflow = 'auto';
			
			$(document).unbind('.glowTree');
			
			context.internal.movingNode = null;
			context.internal.nodeHeightsCache = new Array();
			if (context.internal.movingNodeIsFloating)
			{
				context.internal.movingNodeIsFloating = false;
				context.internal.movingNodeFloat.style.display = 'none';
			}
			
			for (var i = 0; i < context.internal.draggableTrees.length; i++)
			{
				context.internal.draggableTrees[i].glowTree('highlight', false);
			}
			
			api.highlight.apply($(context.internal.state), [false]);
		},
		_remove = function(context, treeNode)
		{
			var newNodes = new Array();
			var found = false;
			for (var i = 0; i < context.internal.nodes.length; i++)
			{
				if (context.internal.nodes[i] == treeNode)
					found = true;
				else 
					newNodes[newNodes.length] = context.internal.nodes[i];
			}
			
			if (found)
			{
				treeNode._dispose();
				context.internal.nodes = newNodes;
			}
		};

	var api = 
	{
		getParentAndIndex : function (x, y)
		{
			var context = $(this).data('tree');
			if (!context)
				return null;

			if (context.internal.pagePosition == null)
				_refreshPagePosition(context);
			
			var nodeAndIndex = new Object();
			nodeAndIndex.parentNode = null;
			nodeAndIndex.index = -1;
			
			if (x > context.internal.pagePosition.Left && x < context.internal.pagePosition.Left + context.internal.pagePosition.Width && y > context.internal.pagePosition.Top - 10 && y < context.internal.pagePosition.Top + context.internal.pagePosition.Height + 10)
			{
				y -= context.internal.pagePosition.Top;
				y -= context.internal.container.offsetTop;
				y += context.internal.wrapper.scrollTop;
				nodeAndIndex.index = 0;
				
				for (var i = 0; i < context.internal.nodes.length; i++)
				{
					if (context.internal.nodes[i]._element)
					{
						if (y < context.internal.nodes[i]._element.offsetHeight)
						{
							var childNodeAndIndex = context.internal.nodes[i]._getParentAndIndex(y);
							if (childNodeAndIndex)
							{
								if (childNodeAndIndex.moveNext)
									nodeAndIndex.index++;
								else
									nodeAndIndex = childNodeAndIndex;
							}
							
							break;	
						}
						else
						{
							y -= context.internal.nodes[i]._element.offsetHeight;
							nodeAndIndex.index++;
						}
					}
				}
			}
				
			return nodeAndIndex;
		}, 
		highlight : function(highlight)
		{
			return this.each(function()
			{
				var context = $(this).data('tree');
				
				if (highlight)
				{
					for (var i = 0; i < context.internal.nodes.length; i++)
					{
						context.internal.nodes[i]._highlight();
					}
					
					context.internal.wrapper.style.backgroundColor = '#cccccc'; 
					context.internal.wrapper.style.borderColor = '#666666';
					
					_refreshPagePosition(context);
				}
				else
				{
					for (var i = 0; i < context.internal.nodes.length; i++)
					{
						context.internal.nodes[i]._unhighlight();
					}

					context.internal.wrapper.style.backgroundColor = context.internal.backgroundColor; 
					context.internal.wrapper.style.borderColor = '#999999';
				}
			});
		},
		addDraggedNode : function(node, parentNode, index, event)
		{
			var context = this.data('tree');
			if (context)
			{
				if (!event)
					event = window.event;
				
				if (parentNode)
				{
					parentNode.insert(node, index);
					parentNode.refresh();
				}
				else
				{
					api.insert.apply($(context.internal.state), [node, index]);
					api.refresh.apply($(context.internal.state), []);
				}
				
				$(context.internal.state).trigger(_eventNames.nodeAdded, [node]);
				
				api.selected.apply($(context.internal.state), [node]);
				_nodeMouseDown(context, event, node);
				_initializeDragging(context);
			};
			
			return this;
		}, 
		selected : function(node)
		{
			if (node === undefined)
			{
				var context = this.data('tree');
				
				if (context)
					return context.internal.selectedNode;
				
				return undefined;
			}
			else
			{
				return this.each(function() 
				{
					var context = $(this).data('tree');
					
					var localNode = api.getByValue.apply($(this), [node.value]);
					if (localNode)
					{
						var selectedNode = context.internal.selectedNode;
						
						if (context.internal.selectedNode && context.internal.selectedNode._element)
							context.internal.selectedNode._element.childNodes[1].style.backgroundColor = context.internal.backgroundColor;

						if (localNode._element)
						{
							localNode._element.childNodes[1].style.backgroundColor = '#ccc';
							context.internal.selectedNode = localNode;
							_scrollToNode(context, localNode);
							
							if (selectedNode != localNode)
								$(context.internal.state).trigger(_eventNames.nodeSelected, [context.internal.selectedNode]);
						}
						else
							context.internal.selectedNode = null;
					}
				});
			}
		}, 
		resize : function(width, height)
		{
			if (width <= 4)
				width = 4;
			
			if (height <= 4)
				height = 4;
			
			return this.each(function()
			{
				var context = $(this).data('tree');
				context.internal.wrapper.style.width = (width - 4) + 'px';
			    context.internal.wrapper.style.height = (height - 4) + 'px';
			});			   
		}, 
		disabled : function(disable)
		{
			if (disable === undefined)
			{
				var context = this.data('tree');
				if (context)
					return context.internal.state.readonly;
				
				return undefined;
			}
			else
			{
				return this.each(function()
				{
					var context = $(this).data('tree');
					
					context.internal.state.readonly = disable;
					api.refresh.apply($(context.internal.state), []);
				});
			}			
		},
		refresh : function()
		{
			return this.each(function()
			{
				var context = $(this).data('tree');
				
				if (!context.internal.initialized)
					return;
				
				while (context.internal.container.childNodes.length > 0)
					context.internal.container.removeChild(context.internal.container.childNodes[0]);

				for(var i = 0; i < context.internal.nodes.length; i++)
				{
					context.internal.nodes[i]._dispose(true);
					context.internal.nodes[i].refresh();
				}
				
				if (context.internal.selectedNode)
					api.selected.apply($(context.internal.state), [context.internal.selectedNode]);
			});
		}, 
		add : function(treeNode)
		{
			if (!treeNode)
				return this;
			
			api.remove.apply(this, [treeNode]);
			
			var isFirst = true;
			return this.each(function()
			{
				if (isFirst)
					isFirst = false;
				else
					treeNode = treeNode._clone();
				
				var context = $(this).data('tree');
			
				treeNode._parent = null;
				treeNode._treeContext = context;
				context.internal.nodes[context.internal.nodes.length] = treeNode;
			});
		}, 
		remove : function(treeNode)
		{
			if (!treeNode)
				return;
			
			return this.each(function()
			{
				var context = $(this).data('tree');
				
				var n = api.getByValue.apply($(context.internal.state), [treeNode.value]);
				if (n)
				{
					if (n.getParent() == null)
						_remove(context, n);
					else
						n.getParent()._remove(n);
				}
			});
		}, 
		insert : function(treeNode, index)
		{
			if (!treeNode)
				return this;
			
			api.remove.apply(this, [treeNode]);
			
			var isFirst = true;
			return this.each(function()
			{
				if (isFirst)
					isFirst = false;
				else
					treeNode = treeNode._clone();
				
				var context = $(this).data('tree');
						
				if (index < 0 || index >= context.internal.nodes.length)
					index = context.internal.nodes.length;
				
				for (var i = context.internal.nodes.length; i > index; i--)
				{
					context.internal.nodes[i] = context.internal.nodes[i - 1];
				}
				
				treeNode._parent = null;
				treeNode._treeContext = context;
				context.internal.nodes[index] = treeNode;
			});
		}, 
		getByValue : function(value)
		{
			var context = this.data('tree');
			
			if (context)
			{
				var node;
				for (var i = 0; i < context.internal.nodes.length; i++)
				{
					if (context.internal.nodes[i].value == value)
						return context.internal.nodes[i];
					else if ((node = context.internal.nodes[i].getByValue(value)) != null)
						return node;			
				}
			}
			
			return null;
		}, 
		getIndex : function(node)
		{
			var context = this.data('tree');
			if (context)
			{
				for (var i = 0; i < context.internal.nodes.length; i++)
				{
					if (context.internal.nodes[i] == node)
						return i;
				}
			
				return -1;
			}
			
			return undefined;
		}, 
		getByIndex : function(index)
		{
			var context = this.data('tree');
			
			if (context && index < context.internal.nodes.length && index >= 0)
				return context.internal.nodes[index];

			return undefined;
		}, 
		count : function()
		{
			var context = this.data('tree');
			
			if (context)
				return context.internal.nodes.length;
			
			return undefined;
		}, 
		createTreeNode: function(options)
        {
            return new treeNode(options);
        }
	};

	$.fn.glowTree = function(method) 
    {
        if(method in api) {
            return api[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return _init.apply(this, arguments);
        } else {
            $.error('Method ' + method + ' does not exist on jQuery.fn.glowTree');
        }
    };  
    $.extend($.fn.glowTree, {
        defaults: {
            enableDragDrop:false,
            getSubNodes:null,
            canHaveChildren:null,
            nodeHtml:[],
            draggableTrees:'',
            expandedImageUrl : '',
			collapsedImageUrl : '',
			blankImageUrl : ''
        }
    });
    
    // events
    // glowTreeNodeSelected
    // glowTreeContextMenu
    // glowTreeNodeRemoved
    // glowTreeNodeMoved
    // glowTreeNodeAdded

	var treeNode = function(options)
	{
		var settings = 
        {
            value: (new Date()).getTime(),
            html: ''
        };

        if (options)
            $.extend(settings, options);

        this.html = settings.html;
        this.value = settings.value;
		this._parent = null;
		this._canHaveChildren = true;
		this._isExpanded = false;
		this._isLoaded = false;
		this._treeContext = null;
		this._element = null;
		this._nodes = new Array();
	}
	
	treeNode.prototype._clone = function()
	{
		var newNode = new treeNode({value: this.value, html: this.html});
		
		for (var i = 0; i < this._nodes.length; i++)
		{
			newNode.add(this._nodes[i]._clone());
		}
		
		return newNode;
	}

	treeNode.prototype.add = function(treeNode)
	{
		treeNode._parent = this;
		treeNode._treeContext = this._treeContext;
		this._nodes[this._nodes.length] = treeNode;
	}

	treeNode.prototype._dispose = function(ignoreCascade)
	{
		this._element = null;
		if (!ignoreCascade)
		{
			for (var i = 0; i < this._nodes.length; i++)
			{
				this._nodes[i]._dispose();
			}
		}
	}

	treeNode.prototype.refresh = function()
	{
		var nodeLi;
		var nodeUl;
		var createNewNode = false;
		
		if (!this._element)
			createNewNode = true;

		if (createNewNode)
		{
			this._dispose();	
			nodeLi = document.createElement('li');
			nodeLi.style.margin = '0';
			nodeLi.style.padding = '0';
			nodeLi.onselectstart = function() { return false; };
			try	{ nodeLi.style.MozUserSelect = 'none'; } catch (e) {}
			try	{ nodeLi.style.userSelect = 'none'; } catch (e) {}
			try { nodeLi.style.WebkitUserSelect = 'ignore'; } catch (e) {}
		}
		else
		{
			nodeLi = this._element;
		}
		
		while (nodeLi.childNodes.length > 0)
			nodeLi.removeChild(nodeLi.childNodes[0]);
		
		nodeLi.appendChild(document.createElement('img'));
		nodeLi.appendChild(document.createElement('span'));	
		
		nodeLi.childNodes[1].innerHTML = this.html;
		
		if (!api.disabled.apply($(this._treeContext.internal.state)))
		{
			nodeLi.childNodes[1].onclick = $.telligent.glow.utility.makeBoundFunction(api.selected, $(this._treeContext.internal.state), [this]);

			var tn = this;
			if (this._treeContext.settings.enableDragDrop)
				nodeLi.childNodes[1].onmousedown = function(event) { _nodeMouseDown(tn._treeContext, event, tn); };
			
			nodeLi.childNodes[1].oncontextmenu = function(event) { _nodeContextMenu(tn._treeContext, event, tn); };
		}
		
		this._element = nodeLi;
		
		if (this._treeContext.internal.selectedNode == this)
			this._element.childNodes[1].style.backgroundColor = '#ccc';
		
		if (this._nodes.length > 0)
		{
			this._isLoaded = true;
			nodeLi.childNodes[0].src = this._isExpanded ? this._treeContext.settings.expandedImageUrl : this._treeContext.settings.collapsedImageUrl;
			
			if (!api.disabled.apply($(this._treeContext.internal.state)))
			{
				nodeLi.childNodes[0].style.cursor = 'pointer';
				var currentNode = this;
				nodeLi.childNodes[0].onclick = function() { currentNode.expanded(!currentNode._isExpanded, true); };
			}
			else
				nodeLi.childNodes[0].style.cursor = 'default';
			
			if (this._isExpanded)
			{
				nodeUl = document.createElement('ul');
				nodeUl.style.listStyleType = 'none';
				nodeUl.style.margin = '0';
				nodeUl.style.padding = '0';
				nodeUl.style.paddingLeft = '16px';
				nodeLi.appendChild(nodeUl);	
				
				for (var i = 0; i < this._nodes.length; i++)
				{
					this._nodes[i]._dispose(true);
					this._nodes[i]._parent = this;
					this._nodes[i]._treeContext = this._treeContext;
					this._nodes[i].refresh();
				}
			}
		}
		else if (this._isLoaded || this._isExpanded || !this._treeContext.settings.getSubNodes)
		{
			nodeLi.childNodes[0].src = this._treeContext.settings.blankImageUrl;
			nodeLi.childNodes[0].style.cursor = 'default';
		}
		else
		{
			nodeLi.childNodes[0].src = this._isExpanded ? this._treeContext.settings.expandedImageUrl : this._treeContext.settings.collapsedImageUrl;
			
			if (!api.disabled.apply($(this._treeContext.internal.state)))
			{
				nodeLi.childNodes[0].style.cursor = 'pointer';
				var currentNode = this;
				nodeLi.childNodes[0].onclick = function() { currentNode.expanded(!currentNode._isExpanded, true); };
			}
			else
				nodeLi.childNodes[0].style.cursor = 'default';
		}		
		
		if (createNewNode)
		{
			if (this._parent)
				this._parent._element.childNodes[2].appendChild(nodeLi);
			else
				this._treeContext.internal.container.appendChild(nodeLi);
		}
	}

	treeNode.prototype._getParentAndIndex = function (yOffset)
	{
		if (yOffset > (this._element.childNodes[1].offsetHeight / 2) && (this._nodes.length == 0 || !this._isExpanded))
		{
			var nodeAndIndex = new Object();
			nodeAndIndex.moveNext = true;
			return nodeAndIndex;
		}
		
		yOffset -= this._element.childNodes[1].offsetHeight;
		if (yOffset < 0 || this._nodes.length == 0 || !this._isExpanded)
		{
			return null;
		}
		else
		{
			var nodeAndIndex = new Object();
			nodeAndIndex.parentNode = this;
			nodeAndIndex.index = 0;
			nodeAndIndex.moveNext = false;

			for (var i = 0; i < this._nodes.length; i++)
			{
				if (this._nodes[i]._element)
				{
					if (yOffset < this._nodes[i]._element.offsetHeight)
					{
						var childNodeAndIndex = this._nodes[i]._getParentAndIndex(yOffset);
						if (childNodeAndIndex)
						{
							if (childNodeAndIndex.moveNext)
								nodeAndIndex.index++;
							else
								nodeAndIndex = childNodeAndIndex;
						}
						
						break;	
					}
					else
					{
						yOffset -= this._nodes[i]._element.offsetHeight;
						nodeAndIndex.index++;
					}
				}
			}
				
			return nodeAndIndex;
		}
	}

	treeNode.prototype._highlight = function()
	{
		if (this._element)
		{
			this._element.style.backgroundColor = '#cccccc'; 
			
			if (this._isExpanded)
			{
				for (var i = 0; i < this._nodes.length; i++)
				{
					this._nodes[i]._highlight();
				}
			}
		}
	}

	treeNode.prototype._unhighlight = function()
	{
		if (this._element)
		{
			this._element.style.backgroundColor = this._treeContext.internal.backgroundColor; 
			
			if (this._isExpanded)
			{
				for (var i = 0; i < this._nodes.length; i++)
				{
					this._nodes[i]._unhighlight();
				}
			}
		}
	}

	treeNode.prototype._setCursor = function(cursor)
	{
		if (this._element)
		{
			this._element.style.cursor = cursor; 
			
			if (this._isExpanded)
			{
				for (var i = 0; i < this._nodes.length; i++)
				{
					this._nodes[i]._setCursor(cursor);
				}
			}
		}
	}

	treeNode.prototype.expanded = function(isExpanded, scroll)
	{
		if (isExpanded === undefined)
			return this._isExpanded;
		else
		{
			this._isExpanded = isExpanded ? true : false;
			
			if (!this._treeContext)
				return;
			
			if (this._element && this._element.childNodes.length == 3)
			{
				this._element.childNodes[2].style.display = this._isExpanded ? 'block' : 'none';
				this._element.childNodes[0].src = this._isExpanded ? this._treeContext.settings.expandedImageUrl : this._treeContext.settings.collapsedImageUrl;
			}
			else if (this._isExpanded && !this._isLoaded && this._treeContext.settings.getSubNodes)
			{
				this._treeContext.settings.getSubNodes($(this._treeContext.internal.state), this);
				this._isLoaded = true;
				this.refresh();
			}
			else
				this.refresh();
			
			if (scroll)
			{
				if (this._isExpanded)
					_scrollToNode(this._treeContext, this, true);
				else
					_scrollToNode(this._treeContext, this, false);
			}
		}
	}

	treeNode.prototype.add = function(treeNode)
	{
		api.remove.apply($(this._treeContext.internal.state), [treeNode]);
		
		treeNode._parent = this;
		treeNode._treeContext = this._treeContext;
		this._nodes[this._nodes.length] = treeNode;
	}

	treeNode.prototype._remove = function(treeNode)
	{
		var newNodes = new Array();
		var found = false;
		for (var i = 0; i < this._nodes.length; i++)
		{
			if (this._nodes[i].value == treeNode.value)
				found = true;
			else 
				newNodes[newNodes.length] = this._nodes[i];
		}
		
		if (found)
		{
			treeNode._dispose();
			this._nodes = newNodes;
		}
	}
	
	treeNode.prototype.canHaveChildren = function(canHaveChildren)
	{
		if (canHaveChildren === undefined)
		{
			if (this._treeContext && this._treeContext.settings.canHaveChildren)
				return this._treeContext.settings.canHaveChildrenFunction($(this._treeContext.internal.state), this);
			else
				return this._canHaveChildren;
		}
		else
			this._canHaveChildren = canHaveChildren === true;
	}

	treeNode.prototype.insert = function(treeNode, index)
	{
		if (index >= 0)
		{
			api.remove.apply($(this._treeContext.internal.state), [treeNode]);
			
			if (index > this._nodes.length)
				index = this._nodes.length;
			
			for (var i = this._nodes.length; i > index; i--)
			{
				this._nodes[i] = this._nodes[i - 1];
			}
			
			this._nodes[index] = treeNode;
			treeNode._parent = this;
			treeNode._treeContext = this._treeContext;
		}
	}

	treeNode.prototype.getByValue = function(value)
	{
		var node;
		for (var i = 0; i < this._nodes.length; i++)
		{
			if (this._nodes[i].value == value)
				return this._nodes[i];
			else if ((node = this._nodes[i].getByValue(value)) != null)
				return node;			
		}
		
		return null;
	}

	treeNode.prototype.getIndex = function(node)
	{
		for (var i = 0; i < this._nodes.length; i++)
		{
			if (this._nodes[i].value == node.value)
				return i;
		}
		
		return -1;
	}

	treeNode.prototype.getByIndex = function(index)
	{
		if (index < this._nodes.length && index >= 0)
			return this._nodes[index];
		else
			return null;
	}

	treeNode.prototype.count = function()
	{
		return this._nodes.length;
	}

	treeNode.prototype.getParent = function()
	{
		return this._parent;
	}

	treeNode.prototype.getTree = function()
	{
		return $(this._treeContext.internal.state);
	}

	treeNode.prototype.isLoaded = function()
	{
		return this._isLoaded;
	}
})(jQuery); 
 

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using GoToTraining.Api.Components;

using Newtonsoft.Json;

namespace GoToTraining.Api
{
    public class Context
    {
        private string _accessToken { get; set; }
        private const string URL_BASE = "htt" + "ps://api.citrixonline.com/G2T/rest/";
        private const string URL_GET_TRAINING = URL_BASE + "organizers/{0}/trainings/{1}";
        private const string URL_GET_TRAININGS = URL_BASE + "organizers/{0}/trainings";
        private const string URL_GET_SESSIONS = URL_BASE + "reports/organizers/{0}/sessions";
        private const string URL_GET_SESSIONS_TRAINING = URL_BASE + "reports/organizers/{0}/trainings/{1}";
        private const string URL_GET_ATTENDEES = URL_BASE + "reports/organizers/{0}/sessions/{1}/attendees";
        private const string URL_GET_REGISTRANTS = URL_BASE + "organizers/{0}/trainings/{1}/registrants";
        private const string URL_GET_ORGANIZERS = URL_BASE + "accounts/{0}/organizers";

        public Context(string accessToken) { _accessToken = accessToken; }

        private static Context _instance = null;
        public static Context Instance(string accessToken)
        {
            if (_instance == null)
                _instance = new Context(accessToken);
            return _instance;
        }
        public static Context Instance()
        {
            if (_instance == null)
                throw new Exception("No initialized instance available");
            return _instance;
        }

        public List<Organizer> GetOrganizers(string accountKey)
        {
            string url = string.Format(URL_GET_ORGANIZERS, accountKey);
            string response = GetResponseString(url);
            var organizers = JsonConvert.DeserializeObject<List<Organizer>>(response);
            return organizers;
        }
        public Training GetTraining(string organizerKey, string trainingKey)
        {
            string url = string.Format(URL_GET_TRAINING, organizerKey, trainingKey);
            string response = GetResponseString(url);
            var training = JsonConvert.DeserializeObject<Training>(response);
            return training;
        }
        public List<Training> GetTrainings(string organizerKey)
        {
            string url = string.Format(URL_GET_TRAININGS, organizerKey);
            string response = GetResponseString(url);
            var trainings = JsonConvert.DeserializeObject<List<Training>>(response);
            return trainings;
        }
        public List<Session> GetSessions(string organizerKey, DateTime startDate, DateTime endDate)
        {
            string url = string.Format(URL_GET_SESSIONS, organizerKey);
            string data = string.Format("{{\"startDate\":\"{0}\", \"endDate\":\"{1}\"}}", FormatAPIDateTime(startDate), FormatAPIDateTime(endDate));
            string response = GetResponseString(url, data);
            var sessions = JsonConvert.DeserializeObject<List<Session>>(response);
            return sessions;
        }
        public List<Session> GetSessions(string organizerKey, string trainingKey)
        {
            string url = string.Format(URL_GET_SESSIONS_TRAINING, organizerKey, trainingKey);
            string response = GetResponseString(url);
            if (response != "[]")
            {
                int stopper = 1;
            }
            var sessions = JsonConvert.DeserializeObject<List<Session>>(response);
            return sessions;
        }
        public List<Registrant> GetRegistrants(string organizerKey, string trainingKey)
        {
            string url = string.Format(URL_GET_REGISTRANTS, organizerKey, trainingKey);
            string response = GetResponseString(url);
            var registrants = JsonConvert.DeserializeObject<List<Registrant>>(response);
            return registrants;
        }

        public List<Attendee> GetAttendees(string organizerKey, DateTime utcStartDate, DateTime? utcEndDate = null, string titleFilter = "")
        {
            if (string.IsNullOrEmpty(organizerKey))
                throw new ArgumentNullException("OrganizerKey", "OrganizerKey is required");

            List<Attendee> trainingAttendees = new List<Attendee>();

            if (!utcEndDate.HasValue || utcEndDate.Value < utcStartDate)
                utcEndDate = DateTime.Parse(DateTime.Now.ToString("MM-dd-yyyy 23:59:59")).ToUniversalTime();

            List<Session> sessions = GetSessions(organizerKey, utcStartDate, utcEndDate.Value);

            if (!string.IsNullOrEmpty(titleFilter))
                sessions = sessions.Where(s => s.TrainingName.Contains(titleFilter)).ToList();

            foreach (Session session in sessions)
            {
                List<Attendee> attendees = GetAttendees(organizerKey, session.SessionKey);
                attendees.ForEach(a => a.MapSession(session, string.Empty));
                if (attendees.Count > 0)
                    trainingAttendees.AddRange(attendees);
            }
            return trainingAttendees;
        }
        public List<Attendee> GetAttendees(string organizerKey, string trainingKey, DateTime utcStartDate, DateTime? utcEndDate = null)
        {
            if (string.IsNullOrEmpty(trainingKey))
                throw new ArgumentNullException("TrainingKey", "TrainingKey is required");
            if (string.IsNullOrEmpty(organizerKey))
                throw new ArgumentNullException("OrganizerKey", "OrganizerKey is required");

            List<Attendee> trainingAttendees = new List<Attendee>();
            List<Session> sessions = GetSessions(organizerKey, trainingKey);

            foreach (Session session in sessions.Where(s => s.SessionStartTime.CompareTo(utcStartDate) <= 0 && ((!utcEndDate.HasValue) || (s.SessionStartTime.CompareTo(utcEndDate) <= 0))))
            {
                List<Attendee> attendees = GetAttendees(organizerKey, session.SessionKey);
                attendees.ForEach(a => a.MapSession(session, trainingKey));
                if(attendees.Count > 0)
                trainingAttendees.AddRange(attendees);
            }
            return trainingAttendees;
        }
        private List<Attendee> GetAttendees(string organizerKey, string sessionKey)
        {
            if (string.IsNullOrEmpty(sessionKey))
                throw new ArgumentNullException("SessionKey", "SessionKey is required");
            if (string.IsNullOrEmpty(organizerKey))
                throw new ArgumentNullException("OrganizerKey", "OrganizerKey is required");

            string url = string.Format(URL_GET_ATTENDEES, organizerKey, sessionKey);
            string response = GetResponseString(url);
            var attendees = JsonConvert.DeserializeObject<List<Attendee>>(response);
            return attendees;
        }

        // temporary making methods public
        private string GetResponseString(string url)
        {
            return GetResponseString(url, "GET", string.Empty);
        }
        public string GetResponseString(string url, string postData)
        {
            return GetResponseString(url, "POST", postData);
        }
        private string GetResponseString(string url, string methodType, string postData)
        {
            string responseString = string.Empty;

            HttpWebRequest request = CreateHttpWSebRequest(url, methodType, postData);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            using (var stream = response.GetResponseStream())
            {
                if (stream != null)
                {
                    using (var streamReader = new StreamReader(stream))
                    {
                        responseString = streamReader.ReadToEnd();
                    }
                }
            }

            return responseString;
        }
        private HttpWebRequest CreateHttpWSebRequest(string endPoint, string methodType, string postData)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(endPoint);

            request.ProtocolVersion = HttpVersion.Version11;
            request.Accept = "application/json";
            request.ContentType = "application/json";
            request.Headers.Add(HttpRequestHeader.Authorization,  _accessToken);
            if (methodType.Equals("POST", StringComparison.InvariantCultureIgnoreCase))
            {
                request.Method = "POST";

                ASCIIEncoding encoding = new ASCIIEncoding();
                byte[] body = encoding.GetBytes(postData);
                request.ContentLength = body.Length;

                using (Stream stream = request.GetRequestStream())
                {
                    if (stream != null)
                    {
                        stream.Write(body, 0, body.Length);
                    }
                }
            }
            else
            {
                request.Method = "GET";
            }
            return request;
        }

        // temporary making method public originally private
        public string FormatAPIDateTime(DateTime dt)
        {
            return dt.ToString("yyyy-MM-ddThh:mm:ssZ");
        }
    }
}

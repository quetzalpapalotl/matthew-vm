﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Models.ListViewModel<TelligentEvolution.Mobile.Web.Data.Entities.Message>>" %>
<script type="text/javascript">
	// <![CDATA[
    $(function ()
    {
        if (typeof (refreshActivityList) === "undefined")
        {
            refreshActivityList = function () { $('.list-view').trigger("telligent_mobile_list_reload", []) };
        }
        $('#createMessageContainer').unbind('telligent_mobile_createmessage_success', refreshActivityList).bind('telligent_mobile_createmessage_success', refreshActivityList);
    });
// ]]>
</script>
<% Html.RenderPartial("ListView", Model); %>

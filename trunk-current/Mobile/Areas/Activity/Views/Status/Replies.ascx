<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Data.Entities.Message>" %>
<li class="list-item">
	<div class="item-author">
		<span class="avatar"><%= Html.UserProfileLink(Model.Author, true) %></span>
	</div>
	<div class="message-body user-defined-markup">
        <%= Model.MessageBody%>
    </div>
	<div class="item-date"><%= Html.FormatAgoDate(Model.CreatedDate)%></div>
</li>

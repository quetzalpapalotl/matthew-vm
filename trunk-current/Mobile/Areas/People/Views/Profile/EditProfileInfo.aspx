﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.EditProfileViewModel>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptContent" runat="server">
<link href="<%= Url.Content("~/Content/themes/base/jquery-ui.css") %>" rel="stylesheet" type="text/css" media="screen"/>
<script src="<%= Url.Content("~/Scripts/jquery-ui.min.js") %>" type="text/javascript"></script>
<script type="text/javascript">
    $(function ()
    {
        $('.date-box').datepicker();
    });
</script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<% using (Html.BeginForm(new { area = "People", controller = "Profile", action = "Edit" })) { %>
    <%= Html.AntiForgeryToken() %>
    <ul class="edit-user-profile">
        <li class="user-profile-field biography">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_Biography", Model.User.DisplayName)%>:</span>
            <div class="user-profile-field-value"><%=Html.TextAreaFor(m => m.User.Biography, new { cols = 30, rows = 4 })%></div>
        </li>
        <li class="user-profile-field location">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_Location")%>:</span>
            <div class="user-profile-field-value"><%=Html.TextBoxFor(m => m.User.Location)%></div>
        </li>
        <li class="user-profile-field birthday">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_Birthday")%>:</span>
            <div class="user-profile-field-value"><%=Html.TextBoxFor(m => m.User.Birthday, new { @class="date-box" })%></div>
        </li>
        <li class="user-profile-field gender">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_Gender")%>:</span>
            <div class="user-profile-field-value"><%=Html.DropDownListFor(m => m.User.Gender, Html.GendersList())%></div>
        </li>
        <li class="user-profile-field language">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_Language")%>: </span>
            <div class="user-profile-field-value"><%=Html.DropDownListFor(m => m.User.Language, Html.SupportedLanguagesList())%></div>
        </li>
        <li class="user-profile-field public-email">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_PublicEmail")%>: </span>
            <%= Html.ValidationMessageFor(m => m.User.PublicEmail)%>
            <div class="user-profile-field-value"><%=Html.TextBoxFor(m => m.User.PublicEmail)%></div>
        </li>
        <li class="user-profile-field weburl">   
            <span class="user-profile-field-name"><%=Html.LocalizedString("People_Profile_WebUrl")%>: </span>
            <%= Html.ValidationMessageFor(m => m.User.WebUrl)%>
            <div class="user-profile-field-value"><%=Html.TextBoxFor(m => m.User.WebUrl)%></div>
        </li>
        <% if (Model.User.ProfileFields != null && Model.User.ProfileFields.Count > 0) {
               for (int i=0; i < Model.User.ProfileFields.Count; i++) {
                   var field = Model.User.ProfileFields[i];
                   if (field.Info != null && !String.IsNullOrEmpty(field.Info.Name))
                   { %>
                        <li class="user-profile-field <%:field.Info.Name.ToLower()%>">   
                            <span class="user-profile-field-name"><%:field.Info.DisplayName ?? field.Info.Name%>: </span>
                            <div class="user-profile-field-value"><%=Html.TextBoxFor(m => m.User.ProfileFields[i].Value)%></div>
                            <%=Html.HiddenFor(m => m.User.ProfileFields[i].Info.SerializedData)%>
                        </li>
                <% } %>
            <% } %>
        <% } %>
        <li class="user-profile-submit">
            <span>
                <a href="#" onclick="$(this).parents('form').submit(); return false;" class="internal-link submit"><span></span><%= Html.LocalizedString("People_Profile_Edit_Save")%></a>
            </span>
        </li>
    </ul>
<% } %>
</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.ProfileViewModel>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Models" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Entities" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Areas.People.Controllers" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ScriptContent" runat="server">
    <script type="text/javascript">
	    // <![CDATA[

        $(function ()
        {
            $('#useRedirectUrl').val("False");
            $('.status-message-container').statusMessageContainer('#createMessageContainer', '<%= Model.User.Username %>', null, 
                '<%= Html.LocalizedString("Helpers_DateFormat_Date_MinuteAgo") %>', 
                '<%= Html.ReplyCountButton(new TelligentEvolution.Mobile.Web.Data.Entities.Message(), "")%>', 
                '<%= Html.ReplyCountButton(new TelligentEvolution.Mobile.Web.Data.Entities.Message() { Id="idTemp", ReplyCount = 1 }, "")%>');
            <% if (Model.FriendshipRequest != null) { %>
            var updateUrl = '<%=Url.Action("UpdatePendingRequest", "Profile", new { area = "People" })%>';
            var createUrl = '<%=Url.Action("CreatePendingRequest", "Profile", new { area = "People" })%>';
            $('#userProfile').pendingRequest(updateUrl, createUrl, '<%= Html.EncodeJavaScript(Html.SiteSettings().DefaultFriendRequestMessage)%>', "#createPendingRequestContainer");
            <% } %>
        });

    // ]]>
    </script>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <% if (Html.SiteSettings().RequireFriendRequestMessage != RequireFriendRequestMessageType.No)
    {
        Html.RenderPartial("CreatePendingRequest", new CreateMessageViewModel() { Message = Html.SiteSettings().DefaultFriendRequestMessage });
    }%>
    <div class="user-profile" id="userProfile">
        <div class="user-details">
            <span class="avatar"><img src='<%= Model.User.AvatarUrl %>' border="0" alt="<%= Model.User.DisplayName %>" /></span>
            <span class="user-name"><%= Model.User.DisplayName %></span>
            <% if (Model.FriendshipRequest != null) Html.RenderPartial("FriendshipActions", Model.FriendshipRequest); %>
        </div>
        <div class="status-message-container" style="<%= Model.User.Status == null ? "display:none" : "" %>">
            <div class="status-message-header"><span></span></div>
            <div class="status-message">
                <div class="message-body"><%= Model.User.Status != null ? Model.User.Status.MessageBody : ""%></div>
                <span class="item-date"><%= Model.User.Status != null ? Html.FormatAgoDate(Model.User.Status.CreatedDate) : ""%></span>
                <span class="reply-count">
                    <%= Html.ReplyCountButton(Model.User.Status, "")%>
                </span>
            </div>
            <div class="status-message-footer"><span></span></div>
        </div>

        <% if (Model.FriendshipRequest != null && Model.FriendshipRequest.State == FriendshipState.Pending && Model.FriendshipRequest.RequestorId == Model.User.Id) { %>
            <div class="pending-request-content">
                <div class="pending-request-header">
                    <%= Html.LocalizedString("People_Profile_Pending_Request")%>
                </div>
                <div class="pending-request-message">
                    <% if (Html.SiteSettings().RequireFriendRequestMessage != RequireFriendRequestMessageType.No && !String.IsNullOrEmpty(Model.FriendshipRequest.RequestMessage))  { %>
                    <p>
                        <%= Model.FriendshipRequest.RequestMessage%>
                    </p>
                    <% } %>
                </div>
                <div class="pending-request-actions-area ">
                    <%= Html.UpdatePendingRequestActionLink(Model.FriendshipRequest.RequestorId, Html.LocalizedString("People_Profile_Pending_Request_Reject"), "internal-link reject", (int)PendingRequestState.Reject)%>
                    <span class='action-delimiter'>|</span>
                    <%= Html.UpdatePendingRequestActionLink(Model.FriendshipRequest.RequestorId, Html.LocalizedString("People_Profile_Pending_Request_Approve"), "internal-link approve", (int)PendingRequestState.Approve)%>
                </div>
            </div>
        <% } %>
    </div>
    <% Html.RenderPartial("TabsContainer", Model); %>
</asp:Content>

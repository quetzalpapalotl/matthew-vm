﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Models.CreateForumPostViewModel>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Entities" %>
<script type="text/javascript">
// <![CDATA[
    function showCreatePostContainer(link) { $('#createPostContainer').createMessageContainer(link, "#Subject"); };
    $(function () { $("textarea").maxlength(<%= Html.SiteSettings().MaxMessageLength %>); })
// ]]>
</script>

<div id="createPostContainer" class="post-form-area new-app-post" style="display: none;">
    <% using (Html.BeginForm(new { area = "Places", controller = "Application", action = "CreatePost"})) { %>
        <div class="field-list-header"><span></span></div>
        <fieldset class="field-list">
	        <ul class="field-list">
                <li class="field-item post-content">
                    <label class="field-item-header"><%= Html.LocalizedString("ApplicationContent_CreatePost_CreatePost")%></label>
                </li>
                <li class="field-item post-content">
			        <label for="Subject" class="field-item-header"><%= Html.LocalizedString("ApplicationContent_CreatePost_SubjectHeader")%></label>
			        <span class="field-item-input"><%= Html.TextBoxFor(m => m.Subject, new { maxlength = Html.Config().MaxSubjectLength })%></span>
                    <% Html.ValidateFor(m => m.Subject); %>
		        </li>
		        <li class="field-item post-content">
			        <label for="Message" class="field-item-header"><%= Html.LocalizedString("ApplicationContent_CreatePost_MessageHeader")%></label>
			        <span class="field-item-input"><%= Html.TextAreaFor(m => m.Message, new { cols = 30, rows = 4 })%></span>
                    <% Html.ValidateFor(m => m.Message); %>
			        <%= Html.AntiForgeryToken() %>
		        </li>
                <li class="field-item post-content-checkbox">
                    <span class="field-item-input">
                        <%= Html.CheckBoxFor(m => m.SubscribeToThread)%>
                    </span>
                    <label for="SubscribeToThread" class="field-item-header"><%= Html.LocalizedString("ApplicationContent_CreatePost_SubscribeToThread")%></label>
		        </li>
		        <li class="field-item submit">
			        <span class="field-item-input">
                        <% if (Model.AllowCombinedThreadTypes) { %>
                            <%= Html.CheckBoxFor(m => m.IsQuestion)%>
                            <label for="IsQuestion" class="field-item-label"><%= Html.LocalizedString("ApplicationContent_CreatePost_IsQuestion")%></label>
                        <% } %>
				        <a href="#" class="internal-link cancel-post"><span></span></a>
				        <a href="#" class="internal-link add-post"><span></span></a>
			        </span>
		        </li>
	        </ul>
        </fieldset>
        <div class="field-list-footer comment"><span></span></div>
    <% } %>
</div>
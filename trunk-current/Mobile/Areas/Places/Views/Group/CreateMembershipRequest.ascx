<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Models.CreateMessageViewModel>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Entities" %>

<script type="text/javascript">
// <![CDATA[
    $(function () { $("textarea").maxlength(<%= Html.SiteSettings().MaxMessageLength %>); })
// ]]>
</script>

<div id="createMembershipRequestContainer" class="post-form-area" style="display: none;">
    <% using (Html.BeginForm(new { area = "Places", controller = "Group", action = "CreateMembership"})) { %>
        <div class="field-list-header"><span></span></div>
        <fieldset class="field-list">
	        <ul class="field-list">
		        <li class="field-item post-content">
			        <label for="Message" class="field-item-header"><%=Html.LocalizedString("Group_Membership_Request_Message")%></label>
			        <span class="field-item-input"><%= Html.TextAreaFor(m => m.Message, new { cols = 30, rows = 4 })%></span>
                    <% if (Html.SiteSettings().RequireFriendRequestMessage == RequireFriendRequestMessageType.Yes) Html.ValidateFor(m => m.Message); %>
			        <%= Html.AntiForgeryToken() %>
		        </li>
		        <li class="field-item submit">
			        <span class="field-item-input">
				        <a href="#" class="internal-link cancel-post"><%=Html.LocalizedString("Group_Membership_Request_CancelButton")%> <span></span></a>
				        <a href="#" class="internal-link add-post"><%=Html.LocalizedString("Group_Membership_Request_SendButton")%><span></span></a>
			        </span>
		        </li>
	        </ul>
        </fieldset>
        <div class="field-list-footer"><span></span></div>
    <% } %>
</div>
﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.SignInViewModel>" MasterPageFile="~/Views/Shared/Simple.Master" %>

<asp:Content ContentPlaceHolderID="TitleContent" runat="server">
	<%= Html.LocalizedString("Core_LogOn_Title")%>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
    $(function () {
        $('#UserName').bind('blur', function () {
            if (new RegExp("<%= Html.EncodeJavaScript(Html.SiteSettings().EmailRegex) %>", "ig").test($('#UserName').val())) {
                $('#SignInType[value=email]').attr("checked", true);
            }
            else {
                $('#SignInType[value=username]').attr("checked", true);
            }
        });
    });
</script>
    <% using (Html.BeginForm(new { area = "", controller = "Account", action = "LogOn", ReturnUrl = Model.ReturnUrl })) { %>
        <div class="field-list-header log-on"></div>
        <fieldset class="field-list log-on"><legend><%= Html.LocalizedString("Core_LogOn_SignIn")%></legend>
            <% if (!ViewData.ModelState.IsValid)
                { %>
                <span class="field-validation-summary"><%= Html.LocalizedString(ViewData.ModelState["errorResourceName"].Errors[0].ErrorMessage)%></span>
            <% } %>
            <ul class="field-list">
                <li class="field-item user-name">
                    <div class="user-name-panel">
                        <label for="UserName" class="field-item-text"><%= Html.LocalizedString("Core_LogOn_LoginName")%></label>
                    </div>
                    <span class="field-item-input"><%= Html.TextBoxFor(m => m.UserName) %></span>
                    <% Html.ValidateFor(m => m.UserName); %>
                </li>
                <li class="field-item password">
                    <label for="Password" class="field-item-text"><%= Html.LocalizedString("Core_LogOn_Password")%></label>
                    <span class="field-item-input"><%= Html.PasswordFor(m => m.Password) %></span>
                    <% Html.ValidateFor(m => m.Password); %>
                </li>
                <li class="field-item remember-me">
                    <label for="RememberMe" class="field-item-text"><%= Html.LocalizedString("Core_LogOn_RememberMe")%></label>
                    <span class="field-item-input"><%= Html.CheckBoxFor(m => m.RememberMe) %></span>
                </li>
                <li class="field-item submit">
                    <span class="field-item-input">
                        <a href="#" onclick="$(this).parents('form').submit(); return false;" class="internal-link submit"><span></span><%= Html.LocalizedString("Core_LogOn_Submit")%></a><input type="submit" class="hidden-submit-button" />
                        <a href="#" onclick="window.history.back(); return false;" class="internal-link cancel"><span></span><%= Html.LocalizedString("Core_LogOn_Cancel")%></a>
                    </span>
                </li>
<!---                <% if (Html.SiteSettings().AllowNewUserRegistration && Html.SiteSettings().AccountActivation != TelligentEvolution.Mobile.Web.Data.Entities.AccountActivation.InvitationOnly) { %> 
                    <li class="field-item join">
                        <%= Html.LinkButton(Html.LocalizedString("Core_LogOn_Join"), "Registration", "Account", new { ReturnUrl = Model.ReturnUrl }, "")%>
                    </li>
                <% } %>
-->
            </ul>
        </fieldset>
        <% if (Model.AuthProviders.Count > 0) { %>
        <fieldset class="field-list log-on-using"><legend><%= Html.LocalizedString("Core_LogOn_SignInUsing")%></legend>
            <% foreach (var provider in Model.AuthProviders) { %>
                <%= Html.AuthLoginActionLink(provider, Model.ReturnUrl)%>
            <% } %>
        </fieldset>
        <% } %>
        <div class="field-list-footer log-on"></div>
        
    <% } %>
</asp:Content>

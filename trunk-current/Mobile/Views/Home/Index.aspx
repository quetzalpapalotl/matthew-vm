﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.HomePageViewModel>" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Models" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Entities" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Implementations" %>
<%@ Import Namespace="Telligent.Evolution.Extensibility.Api.Version1" %>
<%@ Import Namespace="Telligent.Evolution.Extensibility.Api.Entities.Version1" %>

<asp:Content ContentPlaceHolderID="ScriptContent" runat="server">
<script type="text/javascript">
// <![CDATA[
    <% if (Model.IsAuthenticated && Model.UnreadMessagesCount > 0) { %>
        $(function () { $(".conversations-link").append("<div class='unreadmsg-count'><%= Model.UnreadMessagesCount %></div>"); })
    <% } %>
// ]]>
</script>
</asp:Content>
<asp:Content ContentPlaceHolderID="ActionTools" runat="server">
    <% if (Model.SupportsLogInLogOut)
       {
           if (!Model.IsAuthenticated)
           {%>
               <%= Html.LinkButton(Html.LocalizedString("Core_Master_Login"), "LogOn", "Account", new { area = "", returnUrl = Request.Url.ToString() }, "internal-link view-login")%>
           <%}
           else
           {%>
               <%= Html.LinkButton(Html.LocalizedString("Core_Master_LogoOut"), "LogOff", "Account", new { area = "" }, "internal-link view-logout")%>
           <%}
       }
       else
       {%>
           <%= Html.LinkButton(Html.LocalizedString("Core_Master_HomeLink"), "Index", "Home", new { area = "" }, "internal-link view-home")%>
       <%} %>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
<div class="landing-page">
    <div class="splash">
        <%= Html.LocalizedString("Home_Splash_Html")%>
    </div>
	<%
	var ur = new UserRepository();
	var user = ur.GetUser(Model.UserContext.UserName);
	var csn = user.ProfileFields.FirstOrDefault(x => x.Info.Name.Contains("CSN")).Value.Trim();
	var allCsns = user.ProfileFields.FirstOrDefault(x => x.Info.Name.Contains("RelatedCSNs")).Value.Trim().Split(',').ToList<string>();
	allCsns.Add(csn);
	
	var chkCsns = new List<string>();
	foreach(string item in allCsns){
		if(!String.IsNullOrEmpty(item) && item.ToLower() != "employee")
			chkCsns.Add(item);
	}
	
	%>
	
	<% if (Model.IsAuthenticated){ %>
		<ul class="navigation-list">
			<li class="navigation-item create-ticket-link">
				<a href="/login.aspx?ReturnUrl=%2Fcustomer_portal/p%2Fcreate-service-request-ticket.aspx" class="internal-link view-searchbycsn"><span></span>Create a New Request</a>
			</li>
		</ul>
	
		<% foreach (var item in Model.Items.OrderBy(m => m.OrderNumber))
			   if (item.AllowedTo == UserStatus.Both || 
				  (item.AllowedTo == UserStatus.AuthenticatedUser && Model.IsAuthenticated) ||
				  (item.AllowedTo == UserStatus.AnonymousUser && !Model.IsAuthenticated)) { %>
				<% if( (item.Name.ToLower() == "searchbysrn" && chkCsns.Count == 0) || (item.Name.ToLower() != "searchbysrn") ){ %>
				<ul class="navigation-list">
					<li class="navigation-item <%= item.Name.ToLower() %>-link">
					<% if(item.Name.ToLower() == "searchbycsn" && chkCsns.Count != 0){
						%><a class="internal-link view-searchbycsn" href="/Mobile/Ticket/SearchByCsn"><span></span>View My Tickets</a><%
						} else {
							%><%= Html.MenuButton(item) %><%
						}
					%>
					</li>
				</ul>
				<% } %>
		<%} %>
    <%} %>	
</div>
</asp:Content>

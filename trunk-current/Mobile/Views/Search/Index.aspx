<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<TelligentEvolution.Mobile.Web.Models.SearchPageViewModel>" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
    // <![CDATA[
        $(document).ready(function () { showSearchForm('<%= Ajax.JavaScriptStringEncode(Model.QueryTerm) %>', true); });
        function hideSearchForm() { window.history.back(); }
	// ]]>
    </script>
    <% if (!string.IsNullOrEmpty(Model.SearchFilterName))
       { %>
       <div class="search-filter">
            <div>
                <%= Html.LocalizedString("Search_Results_Scope")%> <%= Model.SearchFilterName.Equals("Groups") ? Html.LocalizedString("Search_Resuts_Groups") : Html.Encode(Model.SearchFilterName)%> - <a href='<%= Url.Action("Index", new { area = "", controller = "Search", queryterm = Model.QueryTerm, filterType = "", filterValue = "" }) %>' class="internal-link view-search"><span></span><%= Html.LocalizedString("Search_Results_SearchSite") %></a>
            </div>
       </div>
    <% } %>

   <% Html.RenderAction("Search", "Search", new { area = "", queryTerm = Model.QueryTerm, filterType = Model.SearchFilterType, filterValue = Model.SearchFilterValue }); %>

</asp:Content>
﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<TelligentEvolution.Mobile.Web.Models.ListViewModel>" %>

<% bool isViewMore = ViewContext.HttpContext.Request.Unvalidated().QueryString["viewMore"] == "true";
   string viewID = ViewContext.RouteData.Values["Action"] as string; %>

<script type="text/javascript">
// <![CDATA[
    var url = '<%= Url.RouteUrl(Url.ConcatRouteValues(Url.QueryToRouteValues(), new RouteValueDictionary(new { viewMore = "true" }), ViewContext.RouteData.Values))%>';
    var viewMoreParams = '<%= Url.RouteValueToString(Model.ViewMoreParams)%>';
    $(document).ready(function () {
        $('#<%= viewID %>').listView(url, viewMoreParams, '<%= Model.HasMoreItems.ToString().ToLower() %>', '<%= Model.HasItems.ToString().ToLower() %>',
            { errorText: '<%=Html.EncodeJavaScript(Html.LocalizedString("Core_ListView_Loading_Error"))%>', noDataText: '<%=Html.EncodeJavaScript(Model.NoDataText)%>' });
        <% if (!String.IsNullOrEmpty(Model.HeaderText) && isViewMore) { %>
        $("#<%= viewID %>_title").html("<%= Model.HeaderText %>");
        <%} %>
    });
// ]]>
</script>

<% if (isViewMore) {%>
    <% foreach (var item in Model) { Html.RenderPartial(Model.ItemTemplateViewName, item); } %>
<% } else { %>
    <div id="<%= viewID %>"  >
        <% if (!String.IsNullOrEmpty(Model.HeaderText)) { %>
            <div id="<%= viewID %>_title" class="item-list-title <%=Model.ListItemType%>"><%= Model.HeaderText %></div>
        <% } %>
        <% if (Model.DropdownSettings != null && Model.DropdownSettings.HasValues) {
                   var selectedItem = Model.DropdownSettings.Values.Find(item => item.Selected) ?? Model.DropdownSettings.Values.First();
                   %>
            <div class="list-dropdown-view">
                <a class="list-dropdown-link" href="#" ><%= Model.DropdownSettings.LabelText %>:</a>
                <span class="list-dropdown-text"> <%= selectedItem.Text %></span>
                <div class="list-dropdown-popup" id="<%= Model.DropdownSettings.ParamName %>">
                    <ul class="list-dropdown-items">
                        <% foreach(var item in Model.DropdownSettings.Values) { %>
                            <li class="list-dropdown-item <%= item == selectedItem ? "selected" : "" %>" id="<%= item.Value %>"><%= item.Text %></li>
                        <% } %>
                    </ul>
                </div>
            </div>
            <% } %>
        <div class="list-view" style="<%= !Model.HasItems ? "display:none" : "" %>">
            <ul class="list-view-container item-list <%=Model.ListItemType%>">
                <% foreach (var item in Model) { Html.RenderPartial(Model.ItemTemplateViewName, item); } %>
            </ul>
            <% if (Model.HasMoreItems) { %>
                <div class="view-more-area">
                    <a class="internal-link view-more" href="#"><span></span><%= Html.Encode(Model.ViewMoreLinkText)%></a>
		            <div class="view-more-loading"></div>
                    <div class="view-more-complete"><%= Html.Encode(Model.NoMoreText) %></div>
                </div>
            <% } %>
        </div>
        <div class="error-container" style="<%= Model.HasItems || String.IsNullOrEmpty(Model.NoDataText) ? "display:none" : "" %>">
            <div class="error-message"><%= Model.NoDataText %></div>
        </div>
    </div>
<% } %>
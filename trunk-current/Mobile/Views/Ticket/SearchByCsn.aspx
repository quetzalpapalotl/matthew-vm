﻿<%@ Page Title="Field Service Record Update" Language="C#" Inherits="System.Web.Mvc.ViewPage<SearchTicketModel>" MasterPageFile="~/Views/Shared/Site.Master" %>
<%@ Import Namespace="TelligentEvolution.Mobile.Web.Data.Implementations" %>
<%@ Import Namespace="Telligent.Evolution.Extensibility.Api.Version1" %>
<%@ Import Namespace="Telligent.Evolution.Extensibility.Api.Entities.Version1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="landing-page results">
	    <div class="splash">
        <div class="search-form" style="min-height:30px; padding:5px;">
		<%
		// SMR - Added to get the CSNs for the current user
		var ur = new UserRepository();
		var user = ur.GetUser(Model.UserContext.UserName);
		var csn = user.ProfileFields.FirstOrDefault(x => x.Info.Name.Contains("CSN")).Value.Trim();
		var allCsns = user.ProfileFields.FirstOrDefault(x => x.Info.Name.Contains("RelatedCSNs")).Value.Trim().Split(',').ToList<string>();
		allCsns.Add(csn);
		
		var chkCsns = new List<string>();
		foreach(string item in allCsns){
			if(!String.IsNullOrEmpty(item) && item.ToLower() != "employee")
				chkCsns.Add(item);
		}		
		
		%>
		<% if( chkCsns.Count != 0){ %>
			To only view active tickets, check Active Only, then click Search. Otherwise, simply click Search to view all tickets. <br />
		<% } %>
        <% using (Html.BeginForm(new { controller = "Ticket", action = "SearchByCsn" }))
           { %>
		   
		   <% if( chkCsns.Count == 0){ %>
            <%= Html.TextBoxFor(m => m.txtOmniSearch)%>
			<% } %>
			
            <%= Html.HiddenFor(m => m.isCsnSearch)%>
            <% Html.ValidateFor(m => m.txtOmniSearch); %>
            <%--<%= Html.CheckBoxFor(m => m.activeOnly)%> Active Only--%>
			    <input id="btnSearch" type="submit" value="Search" />
        <%} %>
	    </div>
        <%
            if (Model != null && Model.Tickets != null)
            {
                Html.RenderPartial("EditTicketList", Model);
            }
        %>
</div>
    </div>
    <span style="clear:both"></span>
</asp:Content>
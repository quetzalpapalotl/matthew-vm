﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Custom.Components
{
    public class LookupType
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}

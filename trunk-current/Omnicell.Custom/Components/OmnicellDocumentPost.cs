﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.MediaGalleries.Components;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom.Search;
using OData = Omnicell.Data.Model;

namespace Omnicell.Custom.Components
{
    public class OmnicellDocumentPost : OmnicellMediaPost
    {

        readonly IAudienceTypeService _svcAudienceType = Telligent.Common.Services.Get<IAudienceTypeService>();
        readonly IDocumentTypeService _svcDocumentType = Telligent.Common.Services.Get<IDocumentTypeService>();

        public OmnicellDocumentPost() { }
        public OmnicellDocumentPost(int mediaId) : base(mediaId) { }
        public OmnicellDocumentPost(Media media) : base(media) { }

        private int _audienceTypeId = -1;
        private int _documentTypeId = -1;

        private string _versions = String.Empty;
        private string _featureList = String.Empty;
        private string _interfaces = String.Empty;

        public List<AudienceType> AudienceTypes
        {
            get
            {
                string types = GetValue<string>(OmnicellSearchFields.AudienceTypeId, string.Empty);
                return new List<AudienceType>().FromDelimitedString<AudienceType>(types) as List<AudienceType>;
            }
        }
        /// <summary>
        /// Comma delimited list of audience type ids
        /// </summary>
        public string AudienceTypeIdList { get; set; }

        public int DocumentTypeId
        {
            get { return GetValue<int>(OmnicellSearchFields.DocumentTypeId, -1); }
            set { _documentTypeId = value; }
        }
        public string DocumentType
        {
            get
            {
                string value = string.Empty;
                if (DocumentTypeId > 0)
                {
                    DocumentType dt = _svcDocumentType.Get(DocumentTypeId);
                    if (dt != null)
                        value = dt.Value;
                }
                return value;
            }
        }

        public Dictionary<int, string> ProductFilters
        {
            get { return GetValueDictionary(OmnicellSearchFields.ProductIdFilter); }
        }
        /// <summary>
        /// Comma delimited list of product group ids
        /// </summary>
        public string ProductFilterIdList { get; set; }

        public DateTime PublishDate
        {
            get { return GetValue<DateTime>(OmnicellSearchFields.PublishDate, DateTime.MinValue); }
        }

        public bool ContainsAudienceType(int audienceTypeId)
        {
            return AudienceTypes.Count(t => t.Id == audienceTypeId) > 0;
        }

        public string Versions
        {
            get { return GetValue<string>(OmnicellSearchFields.Versions, String.Empty); }
            set { _versions = value; }
        }

        public string FeatureList
        {
            get { return GetValue<string>(OmnicellSearchFields.FeatureList, String.Empty); }
            set { _featureList = value; }
        }

        public string Interfaces
        {
            get { return GetValue<string>(OmnicellSearchFields.Interfaces, String.Empty); }
            set { _interfaces = value; }
        }

        
    }
}

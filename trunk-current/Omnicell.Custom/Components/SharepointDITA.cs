﻿using System;
using System.Collections.Generic;
using System.Linq;
using SPExtv1 = Telligent.Evolution.Extensibility.Api.Entities.Version1;


using Telligent.Evolution.Extensibility.Api.Version1;

namespace Omnicell.Custom.Components
{
    public class SharepointDITA
    {
        public class DitaDocCoreProperties
        {
            public string Title { get; set; }
            public string Subject { get; set; }
            public string Creator { get; set; }
            public string Description { get; set; }
            public string LastModifiedBy { get; set; }
            public string Revision { get; set; }
            public string LastPrinted { get; set; }
            public string Created { get; set; }
            public string Modified { get; set; }
        }

        public class DitaDocCustomProperties
        {

            public string Id { get; set; }
            public string Product { get; set; }
            public string ProductVersion { get; set; }
            public string Interfaces { get; set; }
            public string Features { get; set; }
            public string Audience { get; set; }
            public string DocumentType { get; set; }
            public string TargetGroup { get; set; }
            public string RevNum { get; set; }
            public string PartName { get; set; }
        }

        public class DitaDocMetaData
        {

            public DitaDocCoreProperties CoreProperties = new DitaDocCoreProperties();
            public DitaDocCustomProperties CustomProperties = new DitaDocCustomProperties();
        }

        public class ParseMetaData
        {
            public int ParseDocumentId(string documentType)
            {
                IDocumentTypeService _svc = Telligent.Common.Services.Get<IDocumentTypeService>();

                IList<DocumentType> documentList = _svc.List();

                int documentId = 0;

                foreach (DocumentType type in documentList)
                {
                    if (type.Value.Equals(documentType))
                    {
                        documentId = type.Id;
                    }
                }

                return documentId;

            }

            public string ParseDocumentType(int documentId)
            {
                IDocumentTypeService _svc = Telligent.Common.Services.Get<IDocumentTypeService>();

                IList<DocumentType> documentList = _svc.List();

                string documentType = string.Empty;

                foreach (DocumentType type in documentList)
                {
                    if (type.Id.Equals(documentId))
                    {
                        documentType = type.Value;
                    }
                }

                return documentType;

            }

            public string TranslateAudienceTypes(string strAudiences)
            {
                string[] lstAudience = strAudiences.Split(';');
                string strReturnValue = "";
                foreach (String audience in lstAudience)
                {
                    strReturnValue += ConvertAudienceTypes(audience) + ",";
                }
                return strReturnValue.TrimEnd(',');
            }
            private string ConvertAudienceTypes(string strSharePointAudince)
            {
                IAudienceTypeService _svc = Telligent.Common.Services.Get<IAudienceTypeService>();

                IList<AudienceType> auType = _svc.List();
                string audience = string.Empty;

                foreach (AudienceType type in auType)
                {
                    if (type.Value.Equals(strSharePointAudince))
                    {
                        audience = type.Id.ToString();
                        break;
                    }
                }

                return audience;
            }

            public string TranslateProductsGroup(string strProducts)
            {
                string[] lstProducts = strProducts.Split(';');
                string strReturnValue = "";
                foreach (string product in lstProducts)
                {
                    strReturnValue += ConvertProductsGroup(product) + ",";
                }
                return strReturnValue.TrimEnd(',');
            }
            private string ConvertProductsGroup(string strProductGroup)
            {
                List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group> apiGroups =
                    new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group>();
                int? groupId = 5;
                apiGroups = PublicApi.Groups.List(new GroupsListOptions() { ParentGroupId = groupId, PageSize = 100 }).ToList();

                string productId = string.Empty;

                foreach (var group in apiGroups)
                {
                    if (group.Name.Equals(strProductGroup))
                    {
                        productId = group.Id.ToString();
                        break;
                    }
                }

                return productId;
            }

            public string TranslateAudienceIds(string strIds)
            {
                string[] lstIds = strIds.Split(',');
                string strReturnValue = string.Empty;

                foreach (string id in lstIds)
                {
                    strReturnValue += ConvertAudienceId(Convert.ToInt32(id)) + ";";
                }

                return strReturnValue.TrimEnd(';');
            }

            private string ConvertAudienceId(int id) 
            {
                IAudienceTypeService _svc = Telligent.Common.Services.Get<IAudienceTypeService>();

                IList<AudienceType> auType = _svc.List();
                string audience = string.Empty;

                foreach (AudienceType type in auType)
                {
                    if (type.Id == id)
                    {
                        audience = type.Value;
                        break;
                    }
                }

                return audience;
            }

            public string TranslateProductIds(string strIds)
            {
                string[] lstIds = strIds.Split(',');
                string strReturnValue = string.Empty;

                foreach (string id in lstIds)
                {
                    strReturnValue += ConvertProductId(Convert.ToInt32(id)) + ";";
                }

                return strReturnValue.TrimEnd(';');
            }

            private string ConvertProductId(int Id)
            {
                 List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group> apiGroups =
                    new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group>();
                int? groupId = 5;
                apiGroups = PublicApi.Groups.List(new GroupsListOptions() { ParentGroupId = groupId, PageSize = 100 }).ToList();

                string productId = string.Empty;

                foreach (var group in apiGroups)
                {
                    if (group.Id == Id)
                    {
                        productId = group.Name;
                        break;
                    }
                }

                return productId;
            }
        }
    }
}

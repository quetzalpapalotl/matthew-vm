﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Custom
{
    public static class Constants
    {
        public const string GALLERY_MEDIA_FORMAT_FILTER = "GalleryMediaFormatFilters";
        public const string GALLERY_COURSE_TYPE_FILTER = "GalleryCourseTypeFilters";
        public const string GALLERY_GALLERY_TYPE = "GalleryType";
        public const string GALLERY_SHOW_COURSE_INFO = "ShowCourseInfo";
        public const string GALLERY_COURSE_DATE_TYPE = "CourseDateType";
        public const string GALLERY_LAST_ATTENDEE_REQ_DATE = "AttendeeRequestLastDate";
    }
}

﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

using Telligent.Evolution.Extensibility.Api.Entities.Version1;

using Omnicell.Custom.Components;

namespace Omnicell.Custom.Data
{
    public class ServiceTicketDataProvider : CastIronSqlDataProvider<ServiceTicketDataProvider, ServiceTicket>
    {

        public void BulkUpdate(string xml)
        {
            string procName = ProcNameWithOwner("omnicell_ServiceTicket_BulkUpdate");

            List<SqlParameter> parameters = new List<SqlParameter>()
            {
	            new SqlParameter{ParameterName = "@XML", SqlDbType = SqlDbType.NVarChar, Value = xml}
            };

            ExecuteNonQuery(procName, parameters);
        }

    }
}

﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

using Telligent.Evolution.Extensibility.Api.Entities.Version1;

using Omnicell.Custom.Components;

namespace Omnicell.Custom.Data
{
    public class TrainingAttendeeDataProvider : SqlDataProvider<TrainingAttendeeDataProvider, TrainingAttendee>
    {
        public int Add(TrainingAttendee attendee)
        {
            string procName = ProcNameWithOwner("omnicell_TrainingAttendee_Add");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
	            new SqlParameter{ParameterName = "@EmailAddress", SqlDbType = SqlDbType.NVarChar, Size = 255, Value = attendee.EmailAddress},
	            new SqlParameter{ParameterName = "@GivenName", SqlDbType = SqlDbType.NVarChar, Size = 64, Value = attendee.GivenName},
	            new SqlParameter{ParameterName = "@Surname", SqlDbType = SqlDbType.NVarChar, Size = 64, Value = attendee.Surname},
                new SqlParameter{ParameterName = "@TimeInSession", SqlDbType = SqlDbType.Int, Value = attendee.TimeInSession}
            };
            // optional parameters
            if (!string.IsNullOrEmpty(attendee.SessionKey))
                parameters.Add(new SqlParameter { ParameterName = "@SessionKey", SqlDbType = SqlDbType.NVarChar, Size = 64, Value = attendee.SessionKey });
            if (!string.IsNullOrEmpty(attendee.SessionStart))
                parameters.Add(new SqlParameter { ParameterName = "@SessionStart", SqlDbType = SqlDbType.NVarChar, Size = 64, Value = attendee.SessionStart });
            if (!string.IsNullOrEmpty(attendee.SessionEnd))
                parameters.Add(new SqlParameter { ParameterName = "@SessionEnd", SqlDbType = SqlDbType.NVarChar, Size = 64, Value = attendee.SessionEnd });
            if (!string.IsNullOrEmpty(attendee.TrainingName))
                parameters.Add(new SqlParameter { ParameterName = "@TrainingName", SqlDbType = SqlDbType.NVarChar, Size = 256, Value = attendee.TrainingName });
            if (!string.IsNullOrEmpty(attendee.TrainingKey))
                parameters.Add(new SqlParameter { ParameterName = "@TrainingKey", SqlDbType = SqlDbType.NVarChar, Size = 64, Value = attendee.TrainingKey });

            return Add(procName, parameters);
        }
        public void Update(TrainingAttendee attendee)
        {
            string procName = ProcNameWithOwner("omnicell_TrainingAttendee_Update");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
	            new SqlParameter{ParameterName = "@AttendeeId", SqlDbType = SqlDbType.Int, Value = attendee.AttendeeId},
	            new SqlParameter{ParameterName = "@EmailAddress", SqlDbType = SqlDbType.NVarChar, Size = 255, Value = attendee.EmailAddress},
	            new SqlParameter{ParameterName = "@GivenName", SqlDbType = SqlDbType.NVarChar, Size = 64, Value = attendee.GivenName},
	            new SqlParameter{ParameterName = "@Surname", SqlDbType = SqlDbType.NVarChar, Size = 64, Value = attendee.Surname},
                new SqlParameter{ParameterName = "@TimeInSession", SqlDbType = SqlDbType.Int, Value = attendee.TimeInSession}
            };
            // optional parameters
            if (!string.IsNullOrEmpty(attendee.SessionKey))
                parameters.Add(new SqlParameter { ParameterName = "@SessionKey", SqlDbType = SqlDbType.NVarChar, Size = 64, Value = attendee.SessionKey });
            if (!string.IsNullOrEmpty(attendee.SessionStart))
                parameters.Add(new SqlParameter { ParameterName = "@SessionStart", SqlDbType = SqlDbType.NVarChar, Size = 64, Value = attendee.SessionStart });
            if (!string.IsNullOrEmpty(attendee.SessionEnd))
                parameters.Add(new SqlParameter { ParameterName = "@SessionEnd", SqlDbType = SqlDbType.NVarChar, Size = 64, Value = attendee.SessionEnd });
            if (!string.IsNullOrEmpty(attendee.TrainingName))
                parameters.Add(new SqlParameter { ParameterName = "@TrainingName", SqlDbType = SqlDbType.NVarChar, Size = 256, Value = attendee.TrainingName });
            if (!string.IsNullOrEmpty(attendee.TrainingKey))
                parameters.Add(new SqlParameter { ParameterName = "@TrainingKey", SqlDbType = SqlDbType.NVarChar, Size = 64, Value = attendee.TrainingKey });

            ExecuteNonQuery(procName, parameters);
        }
        public TrainingAttendee Get(int attendeeId)
        {
            string procName = ProcNameWithOwner("omnicell_TrainingAttendee_Get");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@AttendeeId", SqlDbType = SqlDbType.Int, Value = attendeeId}
            };
            return Get(procName, parameters);
        }
        public TrainingAttendee Get(string emailAddress)
        {
            string procName = ProcNameWithOwner("omnicell_TrainingAttendee_GetByEmailAddress");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@EmailAddress", SqlDbType = SqlDbType.NVarChar, Size = 255, Value = emailAddress}
            };
            return Get(procName, parameters);
        }
        public PagedList<TrainingAttendee> List(int pageIndex, int pageSize)
        {
            int totalCount = 0;
            string procName = ProcNameWithOwner("omnicell_TrainingAttendee_List");
            List<SqlParameter> parameters = new List<SqlParameter>
            {
                new SqlParameter{ParameterName = "@PageIndex", SqlDbType = SqlDbType.Int, Value = pageIndex},
                new SqlParameter{ParameterName = "@PageSize", SqlDbType = SqlDbType.Int, Value = pageSize}
            };
            var list = GetList(procName, parameters, out totalCount);
            return new PagedList<TrainingAttendee>(list, pageSize, pageIndex, totalCount);
        }
        public void Delete(int attendeeId)
        {
            string procName = ProcNameWithOwner("omnicell_TrainingAttendee_Delete");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@AttendeeId", SqlDbType = SqlDbType.Int, Value = attendeeId}
            };
            Delete(procName, parameters);
        }
    }
}

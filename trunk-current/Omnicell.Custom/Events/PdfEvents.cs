﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text;

namespace Omnicell.Custom.Events
{
    public class PdfEvents : PdfPageEventHelper
    {
        public string Footer { get; set; }
        public string ImageUrl { get; set; }
        public string FootUrl { get; set; }
        public string CopyRight { get; set; }

       
        public override void OnEndPage(PdfWriter writer, iTextSharp.text.Document document)
        {
            base.OnEndPage(writer, document);

            iTextSharp.text.Font cFont = FontFactory.GetFont("Arial", 8);
            iTextSharp.text.Font tFont = FontFactory.GetFont("Arial", 10);
            iTextSharp.text.Font lFont = FontFactory.GetFont("Arial", 8, BaseColor.BLUE);
            string footString = CopyRight + " Alteration of this material is prohibited without express written permission of the copyright holder. Periodically, check ";
            string footString2 = " for updates to this material.";
        
            PdfPTable table = new PdfPTable(3);
            table.TotalWidth = document.PageSize.Width - document.LeftMargin - document.RightMargin;
           
 
            iTextSharp.text.Phrase fPhrase = new iTextSharp.text.Phrase(Footer, tFont);

            iTextSharp.text.Phrase cPhrase = new iTextSharp.text.Phrase(footString, cFont);
            var link = new Chunk("myOmnicell.com", lFont);
            link.SetAnchor("https://myomnicell.com");
            cPhrase.Add(link);
            
            iTextSharp.text.Phrase dPhrase = new iTextSharp.text.Phrase(footString2, cFont);
            cPhrase.Add(footString2);
           

            PdfPCell cell1 = new PdfPCell(fPhrase);
            table.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cell1.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cell1.HorizontalAlignment = Element.ALIGN_LEFT;
            cell1.VerticalAlignment = Element.ALIGN_TOP;
            cell1.Colspan = 2;
            table.AddCell(cell1);

            PdfPCell cell2 = new PdfPCell(iTextSharp.text.Image.GetInstance(FootUrl));
            table.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cell2.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cell2.HorizontalAlignment = Element.ALIGN_RIGHT;
            cell2.VerticalAlignment = Element.ALIGN_CENTER;
            cell2.Colspan = 1;
            table.AddCell(cell2);

            PdfPCell cell3 = new PdfPCell();
            cell3.AddElement(cPhrase);
            table.DefaultCell.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cell3.Border = iTextSharp.text.Rectangle.NO_BORDER;
            cell3.VerticalAlignment = Element.ALIGN_BASELINE;
            cell3.Colspan = 3;
            table.AddCell(cell3);
           
            table.WriteSelectedRows(0, -1, 60, 65, writer.DirectContent);


        }
    }
}

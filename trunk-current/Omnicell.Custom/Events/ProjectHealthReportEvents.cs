﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Content.Version1;

using TEApi = Telligent.Evolution.Extensibility.Api.Version1.PublicApi;
using InternalEntity = Omnicell.Custom.Components.ProjectHealthReport;
using Omnicell.Custom.Components;

namespace Omnicell.Custom.Events
{
    public delegate void ProjectHealthReportBeforeCreateEventHandler(ProjectHealthReportBeforeCreateEventArgs e);
    public delegate void ProjectHealthReportAfterCreateEventHandler(ProjectHealthReportAfterCreateEventArgs e);
    public delegate void ProjectHealthReportBeforeUpdateEventHandler(ProjectHealthReportBeforeUpdateEventArgs e);
    public delegate void ProjectHealthReportAfterUpdateEventHandler(ProjectHealthReportAfterUpdateEventArgs e);
    public delegate void ProjectHealthReportBeforeDeleteEventHandler(ProjectHealthReportBeforeDeleteEventArgs e);
    public delegate void ProjectHealthReportAfterDeleteEventHandler(ProjectHealthReportAfterDeleteEventArgs e);

    public class ProjectHealthReportEvents : Telligent.Evolution.Extensibility.Events.Version1.EventsBase
    {
        #region Create

        private readonly object BeforeCreateEvent = new object();

        public event ProjectHealthReportBeforeCreateEventHandler BeforeCreate
        {
            add { Add(BeforeCreateEvent, value); }
            remove { Remove(BeforeCreateEvent, value); }
        }

        internal void OnBeforeCreate(InternalEntity healthReport)
        {
            var handlers = Get<ProjectHealthReportBeforeCreateEventHandler>(BeforeCreateEvent);
            if (handlers != null)
            {
                var args = new ProjectHealthReportBeforeCreateEventArgs(healthReport);
                handlers(args);
            }

            TEApi.Content.Events.OnBeforeCreate(healthReport);
        }

        private readonly object AfterCreateEvent = new object();

        public event ProjectHealthReportAfterCreateEventHandler AfterCreate
        {
            add { Add(AfterCreateEvent, value); }
            remove { Remove(AfterCreateEvent, value); }
        }

        internal void OnAfterCreate(InternalEntity healthReport)
        {
            var handlers = Get<ProjectHealthReportAfterCreateEventHandler>(AfterCreateEvent);
            if (handlers != null)
                handlers(new ProjectHealthReportAfterCreateEventArgs(healthReport));

            TEApi.Content.Events.OnAfterCreate(healthReport);
        }

        #endregion Create

        #region Update

        private readonly object BeforeUpdateEvent = new object();

        public event ProjectHealthReportBeforeUpdateEventHandler BeforeUpdate
        {
            add { Add(BeforeUpdateEvent, value); }
            remove { Remove(BeforeUpdateEvent, value); }
        }

        internal void OnBeforeUpdate(InternalEntity healthReport)
        {
            var handlers = Get<ProjectHealthReportBeforeUpdateEventHandler>(BeforeUpdateEvent);
            if (handlers != null)
            {
                var args = new ProjectHealthReportBeforeUpdateEventArgs(healthReport);
                handlers(args);
            }

            TEApi.Content.Events.OnBeforeUpdate(healthReport);
        }

        private readonly object AfterUpdateEvent = new object();

        public event ProjectHealthReportAfterUpdateEventHandler AfterUpdate
        {
            add { Add(AfterUpdateEvent, value); }
            remove { Remove(AfterUpdateEvent, value); }
        }

        internal void OnAfterUpdate(InternalEntity healthReport)
        {
            var handlers = Get<ProjectHealthReportAfterUpdateEventHandler>(AfterUpdateEvent);
            if (handlers != null)
                handlers(new ProjectHealthReportAfterUpdateEventArgs(healthReport));

            TEApi.Content.Events.OnAfterUpdate(healthReport);
        }

        #endregion Update

        #region Delete

        private readonly object BeforeDeleteEvent = new object();

        public event ProjectHealthReportBeforeCreateEventHandler BeforeDelete
        {
            add { Add(BeforeDeleteEvent, value); }
            remove { Remove(BeforeDeleteEvent, value); }
        }

        internal void OnBeforeDelete(InternalEntity healthReport)
        {
            var handlers = Get<ProjectHealthReportBeforeDeleteEventHandler>(BeforeDeleteEvent);
            if (handlers != null)
            {
                var args = new ProjectHealthReportBeforeDeleteEventArgs(healthReport);
                handlers(args);
            }

            TEApi.Content.Events.OnBeforeCreate(healthReport);
        }

        private readonly object AfterDeleteEvent = new object();

        public event ProjectHealthReportAfterDeleteEventHandler AfterDelete
        {
            add { Add(AfterDeleteEvent, value); }
            remove { Remove(AfterDeleteEvent, value); }
        }

        internal void OnAfterDelete(InternalEntity healthReport)
        {
            var handlers = Get<ProjectHealthReportAfterDeleteEventHandler>(AfterDeleteEvent);
            if (handlers != null)
                handlers(new ProjectHealthReportAfterDeleteEventArgs(healthReport));

            TEApi.Content.Events.OnAfterDelete(healthReport);
        }

        #endregion Delete
    }

    #region EventArgs

    public abstract class ProjectHealthReportEventArgsBase
    {
        internal ProjectHealthReportEventArgsBase(InternalEntity healthReport)
        {
            InternalEntity = healthReport;
        }

        internal InternalEntity InternalEntity { get; private set; }

        public Guid ContentId { get { return InternalEntity.ContentId; } }
        public Guid ContentTypeId { get { return InternalEntity.ContentTypeId; } }
        public int Id { get { return InternalEntity.ProjectHealthReportId; } }
        public string Name { get { return InternalEntity.Name; } }
        public int AuthorUserId { get { return InternalEntity.CreatedByUserId; } }
        public int GroupId { get { return InternalEntity.ImplementationRoomGroupId; } }
        public DateTime CreatedDate { get { return InternalEntity.HealthReportDate; } }
    }

    public class ProjectHealthReportBeforeCreateEventArgs : ProjectHealthReportEventArgsBase
    {
        internal ProjectHealthReportBeforeCreateEventArgs(InternalEntity healthReport)
            : base(healthReport)
        {
        }
    }
    public class ProjectHealthReportAfterCreateEventArgs : ProjectHealthReportEventArgsBase
    {
        internal ProjectHealthReportAfterCreateEventArgs(InternalEntity healthReport)
            : base(healthReport)
        {
        }
    }
    public class ProjectHealthReportBeforeUpdateEventArgs : ProjectHealthReportEventArgsBase
    {
        internal ProjectHealthReportBeforeUpdateEventArgs(InternalEntity healthReport)
            : base(healthReport)
        {
        }
    }
    public class ProjectHealthReportAfterUpdateEventArgs : ProjectHealthReportEventArgsBase
    {
        internal ProjectHealthReportAfterUpdateEventArgs(InternalEntity healthReport)
            : base(healthReport)
        {
        }
    }
    public class ProjectHealthReportBeforeDeleteEventArgs : ProjectHealthReportEventArgsBase
    {
        internal ProjectHealthReportBeforeDeleteEventArgs(InternalEntity healthReport)
            : base(healthReport)
        {
        }
    }
    public class ProjectHealthReportAfterDeleteEventArgs : ProjectHealthReportEventArgsBase
    {
        internal ProjectHealthReportAfterDeleteEventArgs(InternalEntity healthReport)
            : base(healthReport)
        {
        }
    }
    #endregion EventArgs
}

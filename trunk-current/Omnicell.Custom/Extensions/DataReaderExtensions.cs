﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Omnicell.Custom
{
    public static class DataReaderExtensions
    {
        public static T MapTo<T>(this IDataReader reader)
            where T : class, new()
        {
            T newObject = new T();
            Type newObjectType = newObject.GetType();

            int fieldCnt = reader.FieldCount;
            for (int i = 0; i < fieldCnt; i++)
            {
                string fieldName = reader.GetName(i);
                PropertyInfo pi = newObjectType.GetProperty(fieldName);
                if (pi != null && (!reader.IsDBNull(i)))
                    pi.SetValue(newObject, reader.GetValue(i), null);
            }
            return newObject;
        }
    }
}

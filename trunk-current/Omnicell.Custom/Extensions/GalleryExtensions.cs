﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Telligent.Common;
using Telligent.Evolution.MediaGalleries.Components;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;

using Omnicell.Custom.Components;
using OData = Omnicell.Data.Model;

namespace Omnicell.Custom
{
    public static class GalleryExtensions
    {
        readonly static IMediaFormatService _svcMF = Telligent.Common.Services.Get<IMediaFormatService>();
        readonly static ICourseTypeService _svcCT = Telligent.Common.Services.Get<ICourseTypeService>();

        public static void SetMediaFormatFilters(this MediaGallery mg, Dictionary<int, string> filters)
        {
            mg.SetExtendedAttribute(Constants.GALLERY_MEDIA_FORMAT_FILTER, String.Join(";", filters.OrderBy(_kv => _kv.Key).Zip(filters, (kv, sec) => String.Join("=", kv.Key, kv.Value))));
        }
        public static Dictionary<int, string> MediaFormatFilters(this MediaGallery mg)
        {
            string filterString = mg.GetExtendedAttribute(Constants.GALLERY_MEDIA_FORMAT_FILTER);
            Dictionary<int, string> filters = null;
            if (!string.IsNullOrEmpty(filterString))
            {
                filters = Regex.Matches(filterString, @"\s*(.*?)\s*=\s*(.*?)\s*(;|$)")
                    .OfType<Match>()
                    .ToDictionary(m => int.Parse(m.Groups[1].Value), m => m.Groups[2].Value);
            }
            else
            {
                filters = new Dictionary<int, string>();
            }
            return filters;
        }

        public static void SetMediaFormatFilters(this Gallery g, IEnumerable<MediaFormat> filters)
        {
            g.ExtendedAttributes.Add(new ExtendedAttribute { Key = Constants.GALLERY_MEDIA_FORMAT_FILTER, Value = filters.ToDelimitedString() });
        }
        public static List<MediaFormat> MediaFormatFilters(this Gallery g)
        {
            IEnumerable<MediaFormat> filters = new List<MediaFormat>();
            ExtendedAttribute ea = g.ExtendedAttributes[Constants.GALLERY_MEDIA_FORMAT_FILTER];
            if (ea != null)
                filters = filters.FromDelimitedString(g.ExtendedAttributes[Constants.GALLERY_MEDIA_FORMAT_FILTER].Value);

            return _svcMF.List().Where(mf => filters.Select(f => f.Id).Contains(mf.Id)).ToList();
        }

        public static void SetCourseTypeFilters(this Gallery g, IEnumerable<CourseType> filters)
        {
            g.ExtendedAttributes.Add(new ExtendedAttribute { Key = Constants.GALLERY_COURSE_TYPE_FILTER, Value = filters.ToDelimitedString() });
        }
        public static List<CourseType> CourseTypeFilters(this Gallery g)
        {
            IEnumerable<CourseType> filters = new List<CourseType>();
            ExtendedAttribute ea = g.ExtendedAttributes[Constants.GALLERY_COURSE_TYPE_FILTER];
            if (ea != null)
                filters = filters.FromDelimitedString(g.ExtendedAttributes[Constants.GALLERY_COURSE_TYPE_FILTER].Value);

            return _svcCT.List().Where(ct => filters.Select(i => i.Id).Contains(ct.Id)).ToList();
        }

        public static void SetGalleryType(this Gallery g, int galleryType)
        {
            g.ExtendedAttributes.Add(new ExtendedAttribute { Key = Constants.GALLERY_GALLERY_TYPE, Value = galleryType.ToString() });
        }
        public static int GalleryType(this Gallery g)
        {
            int returnValue = -1;
            ExtendedAttribute ea = g.ExtendedAttributes[Constants.GALLERY_GALLERY_TYPE];
            if (ea != null)
                int.TryParse(ea.Value, out returnValue);
            return returnValue;
        }

        public static void SetShowCourseInfo(this Gallery g, bool showCourseInfo)
        {
            g.ExtendedAttributes.Add(new ExtendedAttribute { Key = Constants.GALLERY_SHOW_COURSE_INFO, Value = showCourseInfo.ToString() });
        }
        public static bool ShowCourseInfo(this Gallery g)
        {
            bool returnValue = false;
            ExtendedAttribute ea = g.ExtendedAttributes[Constants.GALLERY_SHOW_COURSE_INFO];
            if (ea != null)
                bool.TryParse(ea.Value, out returnValue);
            return returnValue;
        }

        public static void SetCourseDateType(this Gallery g, int dateType)
        {
            g.ExtendedAttributes.Add(new ExtendedAttribute { Key = Constants.GALLERY_COURSE_DATE_TYPE, Value = dateType.ToString() });
        }
        public static int CourseDateType(this Gallery g)
        {
            int returnValue = -1;
            ExtendedAttribute ea = g.ExtendedAttributes[Constants.GALLERY_COURSE_DATE_TYPE];
            if (ea != null)
                int.TryParse(ea.Value, out returnValue);
            return returnValue;
        }

    }
}

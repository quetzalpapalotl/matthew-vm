﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using Telligent.Common;
using Telligent.Evolution.MediaGalleries.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Entities.Version1;

using Omnicell.Custom.Components;
using Omnicell.Custom.Search;
using ODb = Omnicell.Data.Model;

namespace Omnicell.Custom
{
    public static class GalleryPostExtensions
    {
        readonly static IMediaFormatService _svcMediaFormat = Telligent.Common.Services.Get<IMediaFormatService>();
        readonly static ICourseTypeService _svcCourseType = Telligent.Common.Services.Get<ICourseTypeService>();
        readonly static IDocumentTypeService _svcDocumentType = Telligent.Common.Services.Get<IDocumentTypeService>();
        readonly static IAudienceTypeService _svcAudienceType = Telligent.Common.Services.Get<IAudienceTypeService>();

        public static Dictionary<int, string> ProductFilters(this MediaGalleryPost p)
        {
            string filterString = p.GetExtendedAttribute(OmnicellSearchFields.ProductIdFilter);

            return new Dictionary<int, string>().FromDelimitedString(filterString);
        }

        public static string CourseInfo(this MediaGalleryPost p)
        {
            return p.GetExtendedAttribute(OmnicellSearchFields.CourseInfo);
        }

        public static DateTime BeginDate(this MediaGalleryPost p)
        {
            DateTime beginDate = DateTime.MinValue;
            string beginDateAttr = p.GetExtendedAttribute(OmnicellSearchFields.CourseBeginDate);
            if (beginDateAttr.Length > 0)
                DateTime.TryParse(beginDateAttr, out beginDate);
            return beginDate;
        }
        public static DateTime EndDate(this MediaGalleryPost p)
        {
            DateTime endDate = DateTime.MinValue;
            string endDateAttr = p.GetExtendedAttribute(OmnicellSearchFields.CourseEndDate);
            if (endDateAttr.Length > 0)
                DateTime.TryParse(endDateAttr, out endDate);
            return endDate;
        }

        public static int MediaFormatId(this MediaGalleryPost p)
        {
            int id = -1;
            int.TryParse(p.GetExtendedAttribute(OmnicellSearchFields.MediaFormatId), out id);
            return id;
        }
        public static string MediaFormat(this MediaGalleryPost p)
        {
            var format = _svcMediaFormat.Get(p.MediaFormatId());
            return format != null ? format.Value : string.Empty;
        }

        public static int CourseTypeId(this MediaGalleryPost p)
        {
            int id = -1;
            int.TryParse(p.GetExtendedAttribute(OmnicellSearchFields.CourseTypeId), out id);
            return id;
        }
        public static string CourseType(this MediaGalleryPost p)
        {
            var type = _svcCourseType.Get(p.CourseTypeId());
            return type != null ? type.Value : string.Empty;
        }

        public static int DocumentTypeId(this MediaGalleryPost p)
        {
            int id = -1;
            int.TryParse(p.GetExtendedAttribute(OmnicellSearchFields.DocumentTypeId), out id);
            return id;
        }
        public static string DocumentType(this MediaGalleryPost p)
        {
            var type = _svcDocumentType.Get(p.DocumentTypeId());
            return type != null ? type.Value : string.Empty;
        }

        public static DateTime PublishDate(this MediaGalleryPost p)
        {
            DateTime beginDate = DateTime.MinValue;
            string beginDateAttr = p.GetExtendedAttribute(OmnicellSearchFields.PublishDate);
            if (beginDateAttr.Length > 0)
                DateTime.TryParse(beginDateAttr, out beginDate);
            return beginDate;
        }

        public static List<AudienceType> AudienceTypes(this MediaGalleryPost p)
        {
            string types = p.GetExtendedAttribute(OmnicellSearchFields.AudienceTypeId);
            return new List<AudienceType>().FromDelimitedString<AudienceType>(types) as List<AudienceType>;
        }

        public static string Versions(this MediaGalleryPost p)
        {
            var versions = p.GetExtendedAttribute(OmnicellSearchFields.Versions);
            return versions != null ? versions : string.Empty;
        }

        public static string FeatureList(this MediaGalleryPost p)
        {
            var featureList = p.GetExtendedAttribute(OmnicellSearchFields.FeatureList);
            return featureList != null ? featureList : string.Empty;
        }

        public static string Interfaces(this MediaGalleryPost p)
        {
            var interfaces = p.GetExtendedAttribute(OmnicellSearchFields.Interfaces);
            return interfaces != null ? interfaces : string.Empty;
        }

    }
}

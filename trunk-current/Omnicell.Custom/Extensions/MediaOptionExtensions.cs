﻿using System;
using System.Collections.Generic;
using System.Linq;
using Omnicell.Custom.Components;
using Omnicell.Custom.Search;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;

namespace Omnicell.Custom
{
    public static class MediaOptionExtensions
    {
        readonly static IAudienceTypeService _svcAudienceType = Telligent.Common.Services.Get<IAudienceTypeService>();
        /// <summary>
        /// Adds Product Associations to the course post
        /// </summary>
        /// <param name="filters">comma seperated list of product group ids</param>
        public static void ProductFilters(this MediaCreateOptions o, string filters)
        {
            Dictionary<int, string> prods = new Dictionary<int, string>();
            List<int> ids = filters.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToList();

            foreach (int id in ids)
            {
                var group = PublicApi.Groups.Get(new GroupsGetOptions { Id = id });
                if (group != null)
                {
                    prods.Add(id, group.Name);
                }
            }
            o.ProductFilters(prods);
        }
        public static void ProductFilters(this MediaCreateOptions o, Dictionary<int, string> filters)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.ProductIdFilter, Value = filters.ToDelimitedString() });
        }
        public static void AudienceTypes(this MediaCreateOptions o, string audienceTypeIdList)
        {
            List<AudienceType> types = new List<AudienceType>();
            List<int> ids = audienceTypeIdList.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToList();

            foreach (int id in ids)
            {
                var type = _svcAudienceType.Get(id);
                if (type != null)
                    types.Add(type);
            }
            o.AudienceTypes(types);
        }
        // SMR - overloaded methods to handle semi-colons
        public static void AudienceTypes(this MediaCreateOptions o, string audienceTypeIdList, string strSplitOn)
        {
            List<AudienceType> types = new List<AudienceType>();
            List<String> ids = audienceTypeIdList.Split(strSplitOn.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(i => i).ToList();

            
            foreach (String id in ids)
            {
                if (id != null)
                {
                    // types.Add(id.GetType);
                }
            }
            o.AudienceTypes(types);
        }
        public static void ProductFilters(this MediaCreateOptions o, string filters, string strSplitOn)
        {
            Dictionary<int, string> prods = new Dictionary<int, string>();
            List<int> ids = filters.Split(strSplitOn.ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToList();

            foreach (int id in ids)
            {
                var group = PublicApi.Groups.Get(new GroupsGetOptions { Id = id });
                if (group != null)
                {
                    prods.Add(id, group.Name);
                }
            }
            o.ProductFilters(prods);
        }

        // SMR - overloaded methods to handle semi-colons
        public static void AudienceTypes(this MediaCreateOptions o, List<AudienceType> audienceTypes)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (audienceTypes.Count > 0)
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.AudienceTypeId, Value = audienceTypes.ToDelimitedString() });
        }
        public static void CourseInfo(this MediaCreateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.CourseInfo, Value = value });
        }
        public static void MediaFormatId(this MediaCreateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.MediaFormatId, Value = value });
        }
        public static void CourseTypeId(this MediaCreateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.CourseTypeId, Value = value });
        }
        public static void DocumentTypeId(this MediaCreateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.DocumentTypeId, Value = value });
        }
        public static void BeginDate(this MediaCreateOptions o, DateTime value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (value > DateTime.MinValue)
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.CourseBeginDate, Value = value.ToString("yyyy-MM-ddThh:mm:ssZ") });

        }
        public static void EndDate(this MediaCreateOptions o, DateTime value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (value > DateTime.MinValue)
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.CourseEndDate, Value = value.ToString("yyyy-MM-ddThh:mm:ssZ") });
        }
        public static void PublishDate(this MediaCreateOptions o, DateTime value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (value > DateTime.MinValue)
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.PublishDate, Value = value.ToString() });

        }

        public static void Versions(this MediaCreateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (!String.IsNullOrEmpty(value))
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.Versions, Value = value });
        }

        public static void FeatureList(this MediaCreateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (!String.IsNullOrEmpty(value))
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.FeatureList, Value = value });
        }

        public static void Interfaces(this MediaCreateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (!String.IsNullOrEmpty(value))
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.Interfaces, Value = value });
        }

        public static void Action(this MediaCreateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (!String.IsNullOrEmpty(value))
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.Action, Value = value });
        }

        public static void Sku(this MediaCreateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (!String.IsNullOrEmpty(value))
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.Sku, Value = value });
        }


        /// <summary>
        /// Adds Product Associations to the course post
        /// </summary>
        /// <param name="filters">comma seperated list of product group ids</param>
        public static void ProductFilters(this MediaUpdateOptions o, string filters)
        {
            Dictionary<int, string> prods = new Dictionary<int, string>();
            List<int> ids = filters.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToList();

            foreach (int id in ids)
            {
                var group = PublicApi.Groups.Get(new GroupsGetOptions { Id = id });
                if (group != null)
                {
                    prods.Add(id, group.Name);
                }
            }
            o.ProductFilters(prods);
        }
        public static void ProductFilters(this MediaUpdateOptions o, Dictionary<int, string> filters)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.ProductIdFilter, Value = filters.ToDelimitedString() });
        }
        public static void AudienceTypes(this MediaUpdateOptions o, string audienceTypeIdList)
        {
            List<AudienceType> types = new List<AudienceType>();
            List<int> ids = audienceTypeIdList.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries).Select(i => int.Parse(i)).ToList();


            foreach (int id in ids)
            {
                var type = _svcAudienceType.Get(id);
                if (type != null)
                    types.Add(type);
            }
          
           
            o.AudienceTypes(types);
        }
        public static void AudienceTypes(this MediaUpdateOptions o, List<AudienceType> audienceTypes)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

           // if (audienceTypes.Count > 0)
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.AudienceTypeId, Value = audienceTypes.ToDelimitedString() });
        }
        public static void CourseInfo(this MediaUpdateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.CourseInfo, Value = value });
        }
        public static void MediaFormatId(this MediaUpdateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.MediaFormatId, Value = value });
        }
        public static void CourseTypeId(this MediaUpdateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.CourseTypeId, Value = value });
        }
        public static void DocumentTypeId(this MediaUpdateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.DocumentTypeId, Value = value });
        }
        public static void BeginDate(this MediaUpdateOptions o, DateTime value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (value > DateTime.MinValue)
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.CourseBeginDate, Value = value.ToString("yyyy-MM-ddThh:mm:ssZ") });
        }
        public static void EndDate(this MediaUpdateOptions o, DateTime value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (value > DateTime.MinValue)
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.CourseEndDate, Value = value.ToString("yyyy-MM-ddThh:mm:ssZ") });
        }
        public static void PublishDate(this MediaUpdateOptions o, DateTime value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (value > DateTime.MinValue)
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.PublishDate, Value = value.ToString("yyyy-MM-ddThh:mm:ssZ") });

        }

        public static void Versions(this MediaUpdateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

           // if (!String.IsNullOrEmpty(value))
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.Versions, Value = value });
        }

        public static void FeatureList(this MediaUpdateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();
               
            //if (!String.IsNullOrEmpty(value))
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.FeatureList, Value = value });
        }

        public static void Interfaces(this MediaUpdateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

          //  if (!String.IsNullOrEmpty(value))
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.Interfaces, Value = value });
        }

        public static void Action(this MediaUpdateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (!String.IsNullOrEmpty(value))
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.Action, Value = value });
        }

        public static void Sku(this MediaUpdateOptions o, string value)
        {
            if (o.ExtendedAttributes == null)
                o.ExtendedAttributes = new List<ExtendedAttribute>();

            if (!String.IsNullOrEmpty(value))
                o.ExtendedAttributes.Add(new ExtendedAttribute { Key = OmnicellSearchFields.Sku, Value = value });
        }

    }
}

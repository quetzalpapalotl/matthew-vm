﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;

using TEV1 = Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom.Components;

namespace Omnicell.Custom
{
    public class PreReqTrainingModule : TEV1.IPlugin
    {
        readonly IUserDataService _svcUsers = Telligent.Common.Services.Get<IUserDataService>();
        readonly ITrainingAttendeeService _svcAttendees = Telligent.Common.Services.Get<ITrainingAttendeeService>();
        readonly IUserProfileService _svcUserProfile = Telligent.Common.Services.Get<IUserProfileService>();
        readonly GoToTrainingPlugin gtt = TEV1.PluginManager.Get<GoToTrainingPlugin>().FirstOrDefault();

        public void Initialize() { CSApplication.Instance().UserValidated += PreReqTrainingModule_UserValidated; }
        public string Name { get { return "Go To Training Prerequisite Module"; } }
        public string Description { get { return "Updates a new user to show that the user has taken the Prerequisite training course."; } }

        private void PreReqTrainingModule_UserValidated(User user, CSEventArgs e)
        {
            if (gtt != null && !string.IsNullOrEmpty(gtt.PreReqProfileFieldName))
            {
                UserProfileDataBase data = _svcUserProfile.GetProfileData(user);

                if (!data.GetBoolValue(gtt.PreReqProfileFieldName, false))
                {
                    var attendee = _svcAttendees.Get(user.Email);

                    if (attendee != null)
                    {
                        if (attendee.TimeInSession > (gtt.PreReqCompletionMinutes * 60) || gtt.PreReqCompletionMinutes <= 0)
                        {
                            data.SetBoolValue(gtt.PreReqProfileFieldName, true);
                            _svcUserProfile.SaveProfileData(data);
                        }
                        _svcAttendees.Delete(attendee.AttendeeId);
                    }
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telligent.Evolution.Extensibility.UI.Version1;

namespace Omnicell.Custom.Plugins
{
    public class FactoryDefaultPlugin :IScriptedContentFragmentFactoryDefaultProvider
    {
       
        public string Description
        {
            get { return "Test prep practical application"; }
        }

        public void Initialize()
        {
            
        }

        public string Name
        {
            get { return "Test prep default widget provider"; }
        }

        private readonly Guid identifier = new Guid("a5108e12f0b0467ab4e2cb19646f0241");

        public Guid ScriptedContentFragmentFactoryDefaultIdentifier
        {
            get { return identifier; }
        }
    }
}

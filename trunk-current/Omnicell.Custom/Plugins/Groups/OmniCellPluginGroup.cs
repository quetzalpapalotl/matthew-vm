﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Evolution.Extensibility.Version1;

using Omnicell.Custom.Search;

namespace Omnicell.Custom
{
    public class OmnicellPluginGroup : IPluginGroup
    {
        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Training Core Plugins"; }
        }
        public string Description
        {
            get { return "Contains all plugins defining core functionality for the Omnicell Training Service. This should not be removed."; }
        }
        public IEnumerable<Type> Plugins
        {
            get {
                return new[] {
                    typeof(OmnicellTrainingWidgetPlugin),
                    typeof(WidgetProviderPlugin),
                    typeof(OmnicellMediaFormatPlugin),
                    typeof(OmnicellCoursePlugin),
                    typeof(OmnicellCourseTypePlugin),
                    typeof(OmnicellTrainingGalleryPlugin),
                    typeof(OmnicellTrainingUserPlugin),
                    typeof(OmnicellPagePlugin),
                    typeof(OmnicellAudienceTypePlugin),
                    typeof(OmnicellDocumentGalleryPlugin),
                    typeof(OmnicellDocumentTypePlugin),
                    typeof(OmnicellDocumentPlugin),
                    typeof(GalleryPostMapper),
                    typeof(OmnicellProductPlugin),
                    typeof(OmnicellUtilitiesPlugin)
                };
            }
        }
    }
}

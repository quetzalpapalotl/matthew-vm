﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;

using Omnicell.Custom;
using Omnicell.Custom.Components;

namespace Omnicell.Custom
{
    public class OmnicellAudienceTypePlugin : IScriptedContentFragmentExtension
    {
        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Audience Type Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Audience Type."; }
        }
        public object Extension
        {
            get { return new OmnicellAudienceTypeWidgetExtension(); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_audiencetype"; }
        }
    }
    public class OmnicellAudienceTypeWidgetExtension
    {
        private IAudienceTypeService _svc = null;

        public OmnicellAudienceTypeWidgetExtension()
        {
            _svc = Telligent.Common.Services.Get<IAudienceTypeService>();
        }
        [Documentation(Description = "Create a new Audience Type item")]
        public AudienceType Create(string value)
        {
            return _svc.Save(new AudienceType { Id = -1, Value = value });
        }
        [Documentation(Description = "Delete a current Audience Type item")]
        public V1Entities.AdditionalInfo Delete(int id)
        {
            V1Entities.AdditionalInfo ai = null;

            try
            {
                _svc.Delete(id);
            }
            catch (Exception ex)
            {
                ai = new V1Entities.AdditionalInfo(new CSException(CSExceptionType.UnknownError, ex.Message));
            }

            return ai != null ? ai : new V1Entities.AdditionalInfo();
        }
        [Documentation(Description = "Retrieves a Audience Type item")]
        public AudienceType Get(int id)
        {
            return _svc.Get(id);
        }
        [Documentation(Description = "Lists Audience Type items")]
        public IList<AudienceType> List()
        {
            return _svc.List();
        }
        [Documentation(Description = "Update a Audience Type item")]
        public AudienceType Update(int id, string value)
        {
            return _svc.Save(new AudienceType { Id = id, Value = value });
        }
    }
}

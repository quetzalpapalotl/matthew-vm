﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;

using Omnicell.Custom;
using Omnicell.Custom.Components;

namespace Omnicell.Custom
{
    public class OmnicellCourseTypePlugin : IScriptedContentFragmentExtension
    {
        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Course Type Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Course Type."; }
        }
        public object Extension
        {
            get { return new OmnicellCourseTypeWidgetExtension(); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_coursetype"; }
        }
    }
    public class OmnicellCourseTypeWidgetExtension
    {
        private ICourseTypeService _svcCT = null;

        public OmnicellCourseTypeWidgetExtension()
        {
            _svcCT = Telligent.Common.Services.Get<ICourseTypeService>();
        }
        [Documentation(Description = "Create a new Course Type item")]
        public CourseType Create(string value)
        {
            return _svcCT.Save(new CourseType { Id = -1, Value = value });
        }
        [Documentation(Description = "Delete a current Course Type item")]
        public V1Entities.AdditionalInfo Delete(int id)
        {
            V1Entities.AdditionalInfo ai = null;

            try
            {
                _svcCT.Delete(id);
            }
            catch (Exception ex)
            {
                ai = new V1Entities.AdditionalInfo(new CSException(CSExceptionType.UnknownError, ex.Message));
            }

            return ai != null ? ai : new V1Entities.AdditionalInfo();
        }
        [Documentation(Description = "Retrieves a Course Type item")]
        public CourseType Get(int id)
        {
            return _svcCT.Get(id);
        }
        [Documentation(Description = "Lists Course Type items")]
        public IList<CourseType> List(
            [
                Documentation(Name = "GalleryId", Type = typeof(int), Description = "Retrieve filters from the gallery")
            ] IDictionary options = null)
        {
            int galleryId = options.GetValue<int>("GalleryId", -1);

            if (galleryId > 0)
                return _svcCT.List(galleryId);
            else
                return _svcCT.List();
        }
        [Documentation(Description = "Update a Course Type item")]
        public CourseType Update(int id, string value)
        {
            return _svcCT.Save(new CourseType { Id = id, Value = value });
        }
    }
}

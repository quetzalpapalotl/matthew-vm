﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Components.Search;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;

using Omnicell.Custom;
using Omnicell.Custom.Components;
using Omnicell.Custom.Search;

namespace Omnicell.Custom
{
    public class OmnicellDocumentPlugin : IScriptedContentFragmentExtension
    {
        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Document Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Document."; }
        }
        public object Extension
        {
            get { return new OmnicellDocumentWidgetExtension(); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_document"; }
        }
    }
    public class OmnicellDocumentWidgetExtension
    {
        private IDocumentService _svcDocument = null;

        public OmnicellDocumentWidgetExtension()
        {
            _svcDocument = Telligent.Common.Services.Get<IDocumentService>();
        }

        public OmnicellDocumentPost Get(int documentPostId)
        {
            return _svcDocument.Get(documentPostId);
        }
             
        
        //public Media Create(int galleryId, string name, string contentType, string filename,
        public OmnicellDocumentPost Create(int galleryId, string name, string contentType, string filename,
            [
                Documentation(Name = "Description", Type = typeof(string), Description = "Description"),
                Documentation(Name = "IsFeatured", Type = typeof(bool), Description = "Is Featured", Options = new string[] { "true", "false" }),
                Documentation(Name = "FeaturedImage", Type = typeof(string), Description = "Used to include a featured image when IsFeatured is true."),
                Documentation(Name = "FileData", Type = typeof(byte[]), Description = "FileData or FileUrl is required."),
                Documentation(Name = "FileUrl", Type = typeof(string), Description = "FileData or FileUrl is required."),
                Documentation(Name = "Tags", Type = typeof(string), Description = "A comma seperated list of tags."),
                Documentation(Name = "DocumentTypeId", Type = typeof(int), Description = "Document Type"),
                Documentation(Name = "Audiences", Type = typeof(int), Description = "A comma seperated list of audience type ids"),
                Documentation(Name = "Products", Type = typeof(string), Description = "A comma seperated list of products ids"),
                Documentation(Name = "PublishDate", Type = typeof(DateTime), Description = "If not specified, there will be no publish date."), 
                Documentation(Name = "Versions", Type = typeof(string), Description = "Version of the Product"),
                Documentation(Name = "FeatureList", Type = typeof(string), Description = "A comma seperated list of features "),
                Documentation(Name = "Interfaces", Type = typeof(string), Description = "A comma seperated list of interfaces ") 
				
            ]
            IDictionary options)
        {
            string description = options.GetValue("Description");
            bool isFeatured = options.GetValue<bool>("IsFeatured", false);
            string featuredImage = options.GetValue("FeaturedImage");
            string fileUrl = options.GetValue("FileUrl");
            string tags = options.GetValue("Tags");
            int documentTypeId = options.GetValue<int>("DocumentTypeId");
            string audiences = options.GetValue("Audiences");
            string products = options.GetValue("Products");
            DateTime publishDate = options.GetValue<DateTime>("PublishDate", DateTime.MinValue);
            string versions = options.GetValue("Versions", String.Empty);
            string featureList = options.GetValue("FeatureList", String.Empty);
            string interfaces = options.GetValue("Interfaces", String.Empty);
            
            MediaCreateOptions createOptions = new MediaCreateOptions
            {
                Description = description,
                IsFeatured = isFeatured,
                FeaturedImage = featuredImage,
                FileUrl = fileUrl,
                Tags = tags
            };
            if (options["FileData"] != null)
                createOptions.FileData = (byte[])options["FileData"];
            if (options["ExtendedAttributes"] != null)
            {
                var attributes = options["ExtendedAttributes"] as IDictionary;
                var keys = attributes.Keys.Cast<int>().ToList();

                var attrList = keys.Select(k => new V1Entities.ExtendedAttribute { Key = k.ToString(), Value = attributes[k].ToString() }).ToList();
                createOptions.ExtendedAttributes = attrList;
            }
            if (documentTypeId > 0)
                createOptions.DocumentTypeId(documentTypeId.ToString());
            if (!string.IsNullOrEmpty(audiences))
                createOptions.AudienceTypes(audiences);
            if (!string.IsNullOrEmpty(products))
                createOptions.ProductFilters(products);
            if (publishDate > DateTime.MinValue)
                createOptions.PublishDate(publishDate);
				
            if (!string.IsNullOrEmpty(versions))
                createOptions.Versions(versions);
            if (!string.IsNullOrEmpty(featureList))
                createOptions.FeatureList(featureList);
            if (!string.IsNullOrEmpty(interfaces))
                createOptions.Interfaces(interfaces);


            var media = PublicApi.Media.Create(galleryId, name, contentType, filename, createOptions);

           // return this.Get(media.Id.Value);

            OmnicellMediaOptions omnicellOptions = new OmnicellMediaOptions
            {
                PostId = media.Id.Value,
                DocumentTypeId = documentTypeId,
                AudienceTypeIdList = audiences,
                ProductIdList = products,
                PublishDate = publishDate,
                Versions = versions,
                FeatureList = featureList,
                Interfaces = interfaces
            };

            return _svcDocument.Save(omnicellOptions);
        }

        public OmnicellDocumentPost Update(int galleryId, int fileId,
            [
                Documentation(Name = "Description", Type = typeof(string), Description = "Description"),
                Documentation(Name = "IsFeatured", Type = typeof(bool), Description = "Is Featured", Options = new string[] { "true", "false" }),
                Documentation(Name = "FeaturedImage", Type = typeof(string), Description = "Used to include a featured image when IsFeatured is true."),
                Documentation(Name = "FileData", Type = typeof(byte[]), Description = "FileData or FileUrl is required."),
                Documentation(Name = "FileName", Type = typeof(string), Description = "Required when updating FileData or FileUrl."),
                Documentation(Name = "FileUrl", Type = typeof(string), Description = "FileData or FileUrl is required."),
                Documentation(Name = "Tags", Type = typeof(string), Description = "A comma seperated list of tags."),
                Documentation(Name = "DocumentTypeId", Type = typeof(int), Description = "Document Type"),
                Documentation(Name = "Audiences", Type = typeof(int), Description = "A comma seperated list of audience type ids"),
                Documentation(Name = "Products", Type = typeof(string), Description = "A comma seperated list of products ids"),
                Documentation(Name = "PublishDate", Type = typeof(DateTime), Description = "If not specified, there will be no publish date."),
                Documentation(Name = "ContentType", Type = typeof(string), Description = "Required when updating FileData or FileUrl."),
                Documentation(Name = "Name", Type = typeof(string), Description = "Name"),
                Documentation(Name = "Versions", Type = typeof(string), Description = "Version of the Product"),
                Documentation(Name = "FeatureList", Type = typeof(string), Description = "A comma seperated list of features "),
                Documentation(Name = "Interfaces", Type = typeof(string), Description = "A comma seperated list of interfaces ")
            ]
            IDictionary options)
        {
            string name = options.GetValue("Name");
            string contentType = options.GetValue("ContentType");
            string description = options.GetValue("Description");
            bool isFeatured = options.GetValue<bool>("IsFeatured", false);
            string featuredImage = options.GetValue("FeaturedImage");
            string fileName = options.GetValue("FileName");
            string fileUrl = options.GetValue("FileUrl");
            string tags = options.GetValue("Tags");
            int documentTypeId = options.GetValue<int>("DocumentTypeId");
            string audiences = options.GetValue("Audiences");
           // string products = options.GetValue("Products");
            DateTime publishDate = options.GetValue<DateTime>("PublishDate", DateTime.MinValue);
			string versions = options.GetValue("Versions", String.Empty);
			string featureList = options.GetValue("FeatureList", String.Empty);
			string interfaces = options.GetValue("Interfaces", String.Empty);
            string products = options.GetValue("Products");

            MediaUpdateOptions updateOptions = new MediaUpdateOptions();

            if (options != null)
            {
               // string products = options.GetValue("Products");
                int mediaFormatId = options.GetValue<int>("MediaFormatId");

                if (options["Name"] != null)
                    updateOptions.Name = options["Name"].ToString();

                if (options["ContentType"] != null)
                    updateOptions.ContentType = options["ContentType"].ToString();

                if (options["FileName"] != null)
                    updateOptions.FileName = options["FileName"].ToString();

                if (options["Description"] != null)
                    updateOptions.Description = options["Description"].ToString();

                if (options["FileData"] != null)
                    updateOptions.FileData = (byte[])options["FileData"];

                if (options["FileUrl"] != null)
                    updateOptions.FileUrl = options["FileUrl"].ToString();

                if (options["Tags"] != null)
                    updateOptions.Tags = options["Tags"].ToString();

                if (options["IsFeatured"] != null)
                    updateOptions.IsFeatured = new bool?(Convert.ToBoolean(options["IsFeatured"]));

                if (options["FeaturedImage"] != null)
                    updateOptions.FeaturedImage = options["FeaturedImage"].ToString();

                if (options["ExtendedAttributes"] != null)
                {
                    var attributes = options["ExtendedAttributes"] as IDictionary;
                    var keys = attributes.Keys.Cast<int>().ToList();

                    var attrList = keys.Select(k => new V1Entities.ExtendedAttribute { Key = k.ToString(), Value = attributes[k].ToString() }).ToList();
                    updateOptions.ExtendedAttributes = attrList;
                }

                if (options["MediaFormatId"] != null)
                    updateOptions.MediaFormatId(options["MediaFormatId"].ToString());

                if (options["CourseTypeId"] != null)
                    updateOptions.CourseTypeId(options["CourseTypeId"].ToString());

                if (options["CourseInfo"] != null)
                    updateOptions.CourseInfo(options["CourseInfo"].ToString());

                if (options["Products"] != null && !string.IsNullOrWhiteSpace(options["Products"].ToString()))
                    updateOptions.ProductFilters(options["Products"].ToString());
                else
                    updateOptions.ProductFilters(string.Empty);
            }

            if (documentTypeId > 0)
                updateOptions.DocumentTypeId(documentTypeId.ToString());
            if (audiences != null)
                updateOptions.AudienceTypes(audiences);
            //if (!string.IsNullOrEmpty(products))
              //  updateOptions.ProductFilters(products);
            if (publishDate > DateTime.MinValue)
                updateOptions.PublishDate(publishDate);
				
            if (versions != null)
                updateOptions.Versions(versions);
            if (featureList != null)
                updateOptions.FeatureList(featureList);
            if (interfaces != null)
                updateOptions.Interfaces(interfaces);
				

            var media = PublicApi.Media.Update(galleryId, fileId, updateOptions);

           // return this.Get(media.Id.Value);

            OmnicellMediaOptions omnicellOptions = new OmnicellMediaOptions
            {
                PostId = media.Id.Value,
                DocumentTypeId = documentTypeId,
                AudienceTypeIdList = audiences,
                ProductIdList = products,
                PublishDate = publishDate,
                Versions = versions,
                FeatureList = featureList,
                Interfaces = interfaces
            };

            return _svcDocument.Save(omnicellOptions);

        }

        public V1Entities.PagedList<OmnicellDocumentPost> List(int galleryId,
            [
                Documentation(Name = "Product", Type = typeof(string), Description = "Product GroupId(s) to be found. For many groups use format '(1 OR 2 OR 3)'"),
                Documentation(Name = "AudienceType", Type = typeof(int), Description = "Audience Type to be found"),
                Documentation(Name = "DocumentType", Type = typeof(int), Description = "Document Type to be found"),
                Documentation(Name = "PageIndex", Type = typeof(int), Description = "Specify the page number of paged results to return. Zero-based index.", Default = 0),
                Documentation(Name = "PageSize", Type = typeof(int), Description = "Specify the number of results to return per page.", Default = 20),
                Documentation(Name = "Query", Type = typeof(string), Description = "Query is not required but you should use either Query or Filters otherwise you'll get all documents in the search index."),
                Documentation(Name = "Sort", Type = typeof(string), Description = "Sort", Default = "date+desc", Options = new string[] { "date", "date+asc", "date+desc", "titlesort", "titlesort+asc", "titlesort+desc", "publishdate", "publishdate+asc", "publishdate+desc" })
            ]
            IDictionary options)
        {
            string query = options.GetValue("Query");
            string product = options.GetValue("Product");
            int audienceTypeId = options.GetValue<int>("AudienceType");
            int documentTypeId = options.GetValue<int>("DocumentType");
            int pageIndex = options.GetValue<int>("PageIndex", 0);
            int pageSize = options.GetValue<int>("PageSize", 20);
            string sort = options.GetValue("Sort", "date+desc").Replace("+", " ");

            int currId = 0;

            List<FieldFilter> fieldFilters = new List<FieldFilter>
            { 
                new FieldFilter { Id = currId++, FieldName = SearchFields.ContentType, FieldValue = "file" },
                new FieldFilter { Id = currId++, FieldName = SearchFields.SectionID, FieldValue = galleryId.ToString() }
            };

            if (!string.IsNullOrEmpty(product))
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.ProductIdFilter, FieldValue = product.ToString() });
            if (audienceTypeId > 0)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.AudienceTypeId, FieldValue = audienceTypeId.ToString() });
            if (documentTypeId > 0)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.DocumentTypeId, FieldValue = documentTypeId.ToString() });

            SearchResultsListOptions searchOptions = new SearchResultsListOptions
            {
                Query = query,
                PageIndex = pageIndex,
                PageSize = pageSize < 100 ? pageSize : 100,
                FieldFilters = fieldFilters,
                Sort = sort.Replace("publishdate", OmnicellSearchFields.PublishDate)
            };

            V1Entities.SearchResults results = PublicApi.Search.List(searchOptions);

            List<OmnicellDocumentPost> posts = new List<OmnicellDocumentPost>();
            results.ToList().ForEach(r => posts.Add(new OmnicellDocumentPost(int.Parse(r.ContentId))));
            posts = posts.Where(x => x.Id != null).ToList();

            return new V1Entities.PagedList<OmnicellDocumentPost>(posts, pageSize, pageIndex, results.TotalCount);
        }

        public V1Entities.PagedList<OmnicellDocumentPost> ProductList(int galleryId,
            [
                Documentation(Name = "Product", Type = typeof(string), Description = "Product GroupId(s) to be found. For many groups use format '(1 OR 2 OR 3)'"),
                Documentation(Name = "AudienceType", Type = typeof(int), Description = "Audience Type to be found"),
                Documentation(Name = "Interfaces", Type = typeof(string), Description = "Interface Name to to be found"),
                Documentation(Name = "Features", Type = typeof(string), Description = "Feature Type to be found"),
                Documentation(Name = "Versions", Type = typeof(string), Description = "Version Number to be found"),
                Documentation(Name = "DocumentType", Type = typeof(int), Description = "Document Type to be found"),
                Documentation(Name = "PageIndex", Type = typeof(int), Description = "Specify the page number of paged results to return. Zero-based index.", Default = 0),
                Documentation(Name = "PageSize", Type = typeof(int), Description = "Specify the number of results to return per page.", Default = 20),
                Documentation(Name = "Query", Type = typeof(string), Description = "Query is not required but you should use either Query or Filters otherwise you'll get all documents in the search index."),
                Documentation(Name = "Sort", Type = typeof(string), Description = "Sort", Default = "date+desc", Options = new string[] { "titlesort", "titlesort+asc", "titlesort+desc", "doctype", "doctype+asc", "doctype+desc"  })
            ]
            IDictionary options)
        {
            string query = options.GetValue("Query");
            string product = options.GetValue("Product");
            int audienceTypeId = options.GetValue<int>("AudienceType");
            int documentTypeId = options.GetValue<int>("DocumentType");
            int pageIndex = options.GetValue<int>("PageIndex", 0);
            int pageSize = options.GetValue<int>("PageSize", 20);
            string sort = options.GetValue("Sort", "date+desc").Replace("+", " ");
            string interfaceName = options.GetValue("Interfaces");
            string featureName = options.GetValue("Features");
            string versionName = options.GetValue("Versions");

            int currId = 0;

            List<FieldFilter> fieldFilters = new List<FieldFilter>
            { 
                new FieldFilter { Id = currId++, FieldName = SearchFields.ContentType, FieldValue = "file" },
                new FieldFilter { Id = currId++, FieldName = SearchFields.SectionID, FieldValue = galleryId.ToString() }
            };

            if (!string.IsNullOrEmpty(product))
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.ProductIdFilter, FieldValue = product.ToString() });
            if (audienceTypeId > 0)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.AudienceTypeId, FieldValue = audienceTypeId.ToString() });
            if (documentTypeId > 0)
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.DocumentTypeId, FieldValue = documentTypeId.ToString() });
             if (!String.IsNullOrEmpty(interfaceName))
                fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.Interfaces, FieldValue = interfaceName.ToString() });
             if (!String.IsNullOrEmpty(featureName))
                 fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.FeatureList, FieldValue = featureName.ToString() });
             if (!String.IsNullOrEmpty(versionName))
                 fieldFilters.Add(new FieldFilter { Id = currId++, FieldName = OmnicellSearchFields.Versions, FieldValue = versionName.ToString() });

            SearchResultsListOptions searchOptions = new SearchResultsListOptions
            {
                Query = query,
                PageIndex = pageIndex,
                PageSize = pageSize < 100 ? pageSize : 100,
                FieldFilters = fieldFilters,
                Sort = sort.Replace("doctype", OmnicellSearchFields.DocumentType)
            };

            V1Entities.SearchResults results = PublicApi.Search.List(searchOptions);
           
            List<OmnicellDocumentPost> posts = new List<OmnicellDocumentPost>();
            results.ToList().ForEach(r => posts.Add(new OmnicellDocumentPost(int.Parse(r.ContentId))));
            posts = posts.Where(x => x.Id != null).ToList();
            
            return new V1Entities.PagedList<OmnicellDocumentPost>(posts, pageSize, pageIndex, results.TotalCount);
        }
    }
}

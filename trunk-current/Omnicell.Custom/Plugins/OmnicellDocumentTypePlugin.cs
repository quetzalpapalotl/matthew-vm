﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;

using Omnicell.Custom;
using Omnicell.Custom.Components;


namespace Omnicell.Custom
{
    public class OmnicellDocumentTypePlugin : IScriptedContentFragmentExtension
    {
        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Document Type Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Document Type."; }
        }
        public object Extension
        {
            get { return new OmnicellDocumentTypeWidgetExtension(); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_documenttype"; }
        }
    }
    public class OmnicellDocumentTypeWidgetExtension
    {
        private IDocumentTypeService _svc = null;

        public OmnicellDocumentTypeWidgetExtension()
        {
            _svc = Telligent.Common.Services.Get<IDocumentTypeService>();
        }
        [Documentation(Description = "Create a new Document Type item")]
        public DocumentType Create(string value)
        {
            return _svc.Save(new DocumentType { Id = -1, Value = value });
        }
        [Documentation(Description = "Delete a current Document Type item")]
        public V1Entities.AdditionalInfo Delete(int id)
        {
            V1Entities.AdditionalInfo ai = null;

            try
            {
                _svc.Delete(id);
            }
            catch (Exception ex)
            {
                ai = new V1Entities.AdditionalInfo(new CSException(CSExceptionType.UnknownError, ex.Message));
            }

            return ai != null ? ai : new V1Entities.AdditionalInfo();
        }
        [Documentation(Description = "Retrieves a Document Type item")]
        public DocumentType Get(int id)
        {
            return _svc.Get(id);
        }
        [Documentation(Description = "Lists Document Type items")]
        public IList<DocumentType> List()
        {
            return _svc.List();
        }
        [Documentation(Description = "Update a Document Type item")]
        public DocumentType Update(int id, string value)
        {
            return _svc.Save(new DocumentType { Id = id, Value = value });
        }
    }
}

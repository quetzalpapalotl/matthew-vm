﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;

using Omnicell.Custom;
using Omnicell.Custom.Components;

namespace Omnicell.Custom
{
    public class OmnicellMediaFormatPlugin : IScriptedContentFragmentExtension
    {
        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Media Format Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Media Format."; }
        }
        public object Extension
        {
            get { return new OmnicellMediaFormatWidgetExtension(); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_mediaformat"; }
        }
    }
    public class OmnicellMediaFormatWidgetExtension
    {
        private IMediaFormatService _svcMF = null;

        public OmnicellMediaFormatWidgetExtension()
        {
            _svcMF = Telligent.Common.Services.Get<IMediaFormatService>();
        }

        [Documentation(Description = "Create a new Media Format item")]
        public MediaFormat Create(string value)
        {
            return _svcMF.Save(new MediaFormat { Id = -1, Value = value });
        }
        [Documentation(Description = "Delete a current Media Format item")]
        public V1Entities.AdditionalInfo Delete(int id)
        {
            V1Entities.AdditionalInfo ai = null;

            try
            {
                _svcMF.Delete(id);
            }
            catch (Exception ex)
            {
                ai = new V1Entities.AdditionalInfo(new CSException(CSExceptionType.UnknownError, ex.Message));
            }

            return ai != null ? ai : new V1Entities.AdditionalInfo();
        }
        [Documentation(Description = "Retrieves a Media Format item")]
        public MediaFormat Get(int id)
        {
            return _svcMF.Get(id);
        }
        [Documentation(Description = "Lists Media Format items")]
        public IList<MediaFormat> List(
            [
                Documentation(Name = "GalleryId", Type = typeof(int), Description = "Retrieve filters from the gallery")
            ] IDictionary options)
        {
            int galleryId = options.GetValue<int>("GalleryId", -1);

            if (galleryId > 0)
                return _svcMF.List(galleryId);
            else
                return _svcMF.List();
        }
        [Documentation(Description = "Update a Media Format item")]
        public MediaFormat Update(int id, string value)
        {
            return _svcMF.Save(new MediaFormat { Id = id, Value = value });
        }
    }
}

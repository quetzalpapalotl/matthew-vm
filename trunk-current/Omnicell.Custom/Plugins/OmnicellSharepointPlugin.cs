﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using iTextSharp.text;
using Omnicell.Custom.Components;
using Omnicell.Data.Model;
using Telligent.DynamicConfiguration.Components;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Storage.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;
using SPApi = Telligent.Evolution.Extensions.SharePoint.Client.Api.Version1;
using SPExtv1 = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using System.Text.RegularExpressions;


namespace Omnicell.Custom
{
    public class OmnicellSharepointPlugin : IConfigurablePlugin, IScriptedContentFragmentExtension
    {
        private ISharepointService _svcSharePoint = null;
        //private IMoxiService _svcMoxi = null;

        #region IConfigurablePlugin Members

        public string Name
        {
            get { return "Omnicell Sharepoint Components and Utilities"; }
        }
        public string Description
        {
            get { return "Enables Synchronization between Telligent Connect and other Telligent Applications."; }
        }

        IPluginConfiguration Configuration { get; set; }

        public Telligent.DynamicConfiguration.Components.PropertyGroup[] ConfigurationOptions
        {
            get
            {
                PropertyGroup[] groups = new[] { 
                    new PropertyGroup("mapping", "Document Library to Media Gallery Mapping Options", 0), 
                    new PropertyGroup("access", "API Access Options", 1),
                    new PropertyGroup("note Images", "Image settings for note elements.", 2)
                };

                Property ospWebFullUrlProp = new Property("ospWebFullUrl", "Full SharePoint URL", PropertyType.Url, 0, "http://sp.defaktolabs.com/Repository/");
                ospWebFullUrlProp.DescriptionText = "Enter the full url for the sharepoint site. (http://www.mysharepointdomain.com)";
                groups[0].Properties.Add(ospWebFullUrlProp);

                Property ospDomainProp = new Property("ospDomain", "SharePoint Domain", PropertyType.String, 0, "DEFAKTOWEB004");
                ospDomainProp.DescriptionText = "Enter the domain for the sharepoint site. (DEFAKTOWEB004)";
                groups[0].Properties.Add(ospDomainProp);
                
                Property ospSpLoginNameProp = new Property("ospSpLoginName", "SharePoint Login", PropertyType.String, 0, "SPIntegration");
                ospSpLoginNameProp.DescriptionText = "Enter the sharepoint account login name.";
                groups[0].Properties.Add(ospSpLoginNameProp);

                Property ospSpPasswordProp = new Property("ospSpPassword", "SharePoint Password", PropertyType.String, 0, "Omnicell101");
                ospSpPasswordProp.DescriptionText = "Enter the sharepoint account password.";
                groups[0].Properties.Add(ospSpPasswordProp);

                Property ospDocListMediaGalleryMapProp = new Property("ospDocListMediaGalleryMap", "obsolete: Source Document Library Guid to Media Gallery Map", PropertyType.String, 0, "Omnicell101");
                ospDocListMediaGalleryMapProp.DescriptionText = "obsolete: Do Not Use";
                groups[1].Properties.Add(ospDocListMediaGalleryMapProp);

                Property ospSrcDocLibId = new Property("ospSrcDocLibId", "Source Document Library Guid", PropertyType.String, 0, "E33033EE-611F-4ADE-A38E-D14323E055E0");
                ospSrcDocLibId.DescriptionText = "Document Library ID in Telligent used to synchronize target wiki groups and media galleries.";
                groups[1].Properties.Add(ospSrcDocLibId);

                //Property ospMapGuidProp = new Property("ospMapGuidProp", "Document Library Map Guid", PropertyType.String, 0, "667D94D4-BC13-42A9-B7D4-FA06E0869564");
                //ospMapGuidProp.DescriptionText = "(obsolete: no longer used)";
                //groups[1].Properties.Add(ospMapGuidProp);

                Property osMapGroupId = new Property("osMapGroupId", "Document Library Target Group to Wiki GroupId", PropertyType.String, 0, "Service Biomed=46");
                osMapGroupId.DescriptionText = "Associates a Target Group value with a GroupID. Ex: Service BioMed = 46";
                groups[1].Properties.Add(osMapGroupId);

                Property osMapGalleryId = new Property("osMapGalleryId", "Document Library Target Group to Media Gallery ID", PropertyType.String, 0, "Service Biomed=115");
                osMapGalleryId.DescriptionText = "Associates a Target Group value with a MediaGalleryID: Service Biomed=115";
                groups[1].Properties.Add(osMapGalleryId);

                Property omRootUrl = new Property("omRootUrl", "myOmnicell Url Root", PropertyType.String, 0, "http://dev.myomnicell.com");
                omRootUrl.DescriptionText = "The Root url for the myomnicell instance. Ex: http://dev.myomnicell.com";
                groups[1].Properties.Add(omRootUrl);

                Property omInternalMetadata = new Property("omInternalMetadata", "Omnicell SharePoint Internal Keywords", PropertyType.String, 0, "N/A - Boilerplate");
                omInternalMetadata.DescriptionText = "These metadata values are for Omnicell internal use only and should not be used for documents and wikis on the production site";
                groups[1].Properties.Add(omInternalMetadata);

                Property omTimeZone = new Property("omTimeZone", "SharePoint instance time zone", PropertyType.String, 0, "Pacific Standard Time");
                omTimeZone.DescriptionText = "The time zone of the SharePoint instance.";
                groups[1].Properties.Add(omTimeZone);

                Property omNoteGeneric = new Property("omNoteGeneric", "Image for generic note div.", PropertyType.String, 0, "http://omnicell.dev/cfs-file.ashx/__key/communityserver-components-sitefiles/DxNoteGeneric.png");
                omNoteGeneric.DescriptionText = "The image used for generic note div tags.";
                groups[2].Properties.Add(omNoteGeneric);

                Property omNoteCaution = new Property("omNoteCaution", "Image for caution note div.", PropertyType.String, 0, "http://omnicell.dev/cfs-file.ashx/__key/communityserver-components-sitefiles/DxNoteGeneric.png");
                omNoteGeneric.DescriptionText = "The image used for caution note div tags.";
                groups[2].Properties.Add(omNoteCaution);

                Property omNoteImportant = new Property("omNoteImportant", "Image for important note div.", PropertyType.String, 0, "http://omnicell.dev/cfs-file.ashx/__key/communityserver-components-sitefiles/DxNoteGeneric.png");
                omNoteGeneric.DescriptionText = "The image used for important note div tags.";
                groups[2].Properties.Add(omNoteImportant);

                Property omNoteOther = new Property("omNoteOther", "Image for other note div.", PropertyType.String, 0, "http://omnicell.dev/cfs-file.ashx/__key/communityserver-components-sitefiles/DxNoteGeneric.png");
                omNoteGeneric.DescriptionText = "The image used for other note div tags.";
                groups[2].Properties.Add(omNoteOther);

                Property omApiKey = new Property("omApiKey", "Api key for rest requests.", PropertyType.String, 0, "gmjak34qvtveqvyg0ua");
                omApiKey.DescriptionText = "Rest Api key";
                groups[2].Properties.Add(omApiKey);
                
                return groups;
            }
        }
        public void Update(IPluginConfiguration configuration)
        {
            Configuration = configuration;

            SPFullUrl = configuration.GetString("ospWebFullUrl");
            SPDomain = configuration.GetString("ospDomain");
            SPLogin = configuration.GetString("ospSpLoginName");
            SPPassword = configuration.GetString("ospSpPassword");
            DLMGMaps = configuration.GetString("ospDocListMediaGalleryMap");
            SrcDocLibId = configuration.GetString("ospSrcDocLibId");
            //SPMapLibrary = configuration.GetString("ospMapGuidProp");
            MoxiLibraryMap = configuration.GetString("osMapGroupId");
            MoxiMediaGalleryMap = configuration.GetString("osMapGalleryId");
            SPRootUrl = configuration.GetString("omRootUrl");
            InternalMetadata = configuration.GetString("omInternalMetadata");
            omTimeZone = configuration.GetString("omTimeZone");
            omNoteGeneric = configuration.GetString("omNoteGeneric");
            omNoteCaution = configuration.GetString("omNoteCaution");
            omNoteImportant = configuration.GetString("omNoteImportant");
            omNoteOther = configuration.GetString("omNoteOther");
            omApiKey = configuration.GetString("omApiKey");

        }

        public string SPFullUrl { get; private set; }
        public string SPDomain { get; private set; }
        public string SPLogin { get; private set; }
        public string SPPassword { get; private set; }
        public string DLMGMaps { get; private set; }
        public string SrcDocLibId { get; private set; }
        //public string SPMapLibrary { get; private set; }
        public string MoxiLibraryMap { get; private set; }
        public string MoxiMediaGalleryMap { get; private set; }
        public string SPRootUrl { get; private set; }
        public string InternalMetadata { get; private set; }
        public string omTimeZone { get; private set; }
        public string omNoteGeneric { get; private set; }
        public string omNoteCaution { get; private set; }
        public string omNoteImportant { get; private set; }
        public string omNoteOther { get; private set; }
        public string omApiKey { get; private set; }

        #endregion IConfigurablePlugin Members


        #region IScriptedContentFragmentExtension Members

        public object Extension
        {
            get { return new OmnicellSharpointWidgetExtension(); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_sharepoint"; }
        }

        #endregion IScriptedContentFragmentExtension Members

        public void Initialize()
        {
            _svcSharePoint = Telligent.Common.Services.Get<ISharepointService>();
            //_svcMoxi = Telligent.Common.Services.Get<IMoxiService>();

            SPApi.PublicApi.Documents.Events.AfterCreate += new SPApi.DocumentAfterCreateEventHandler(Events_AfterCreate);
            SPApi.PublicApi.Documents.Events.AfterUpdate += new SPApi.DocumentAfterUpdateEventHandler(Events_AfterUpdate);
            SPApi.PublicApi.Documents.Events.BeforeDelete += new SPApi.DocumentBeforeDeleteEventHandler(Events_BeforeDelete);
            SPApi.PublicApi.Documents.Events.AfterDelete += new SPApi.DocumentAfterDeleteEventHandler(Events_AfterDelete);
        }


        void Events_AfterCreate(SPApi.DocumentAfterCreateEventArgs e)
        {
            //_svcSharePoint.CreateUpdateDocument(e.ContentId);
            //_svcMoxi.CreateUpdateDocument(e.ContentId);
        }

        void Events_AfterUpdate(SPApi.DocumentAfterUpdateEventArgs e)
        {
           // _svcSharePoint.CreateUpdateDocument(e.ContentId);
            //_svcMoxi.CreateUpdateDocument(e.ContentId);
            // SPApi.PublicApi.Documents.Delete(e.ContentId);
        }

        void Events_BeforeDelete(SPApi.DocumentBeforeDeleteEventArgs e)
        {
            // string strDocName = "";
          //  _svcSharePoint. CreateUpdateDocument(e.ContentId);
            // SMR - This event does not get called. Look below.
        }

        void Events_AfterDelete(SPApi.DocumentAfterDeleteEventArgs e)
        {
            //_svcSharePoint.DeleteDocument(e.ContentId);
        }
    }

    public class OmnicellSharpointWidgetExtension
    {
        private ISharepointService _svcSharePoint = null;
       

        public OmnicellSharpointWidgetExtension()
        {
            _svcSharePoint = Telligent.Common.Services.Get<ISharepointService>();
        }

        /*
        public string BatchCreate()
        {
            string error = "No Error";
            try
            {
                // _svcSharePoint.BatchCreate();
                _svcMoxi.BatchCreate();
            }
            catch (Exception ex)
            {
                error = ex.ToString();
            }

            return error;

        }
         */

        public SPExtv1.PagedList<SPApi.Library> GetSharepointLibraryList(int groupID)
        {
            return _svcSharePoint.GetLibraryList(groupID);
        }

        public SPExtv1.PagedList<SPApi.Document> GetSharepointLibrary(string guidID)
        {
            return _svcSharePoint.GetSharepointLibrary(guidID);
        }

        public DateTime GetWebCreatedDate()
        {
            return _svcSharePoint.GetWebCreatedDate();
        }

        public string RetrieveWikiPageShortDesc(int? pageId)
        {
            string shortDesc = String.Empty;
            var db = new OmnicellEntities();
            try
            {
                var wikis = db.WikiPageMetaDatas.FirstOrDefault(x => x.WikiPageId == pageId);
                shortDesc = wikis.ShortDesc;

            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving wiki page with Id: {0}", pageId), ex).Log();
            }

            return shortDesc;
        }

        public int? RetrievePDFileId(int? groupId, int? wikiId)
        {
            int? mediaId = 0;
            var db = new OmnicellEntities();
            MediaListOptions options = new MediaListOptions();
            options.GroupId = groupId;
            options.PageSize = 5000;
            Guid docGuid;

            try
            {

                var files = PublicApi.Media.List(options);
                var wiki = db.Omnicell_WikiMetaData2.FirstOrDefault(x => x.WikiId == wikiId);

                if (files.Count > 0)
                {
                    foreach (SPExtv1.Media file in files)
                    {
                        string wikiGuid = file.Tags.FirstOrDefault(x => x.StartsWith("WikiGUID:"));

                        if (!string.IsNullOrEmpty(wikiGuid))
                        {
                            wikiGuid = wikiGuid.Replace("WikiGUID:", "");

                            if (Guid.TryParse(wikiGuid, out docGuid))
                            {
                                if (docGuid.CompareTo(wiki.ContentId) == 0)
                                {
                                    mediaId = file.Id;
                                }

                            }

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving media files with group Id: {0}", groupId), ex).Log();
            }


            return mediaId;
        }

        public string RetrieveWikiUrl(IList<string> tags)
        {
            string url = "";
            Guid docId;
            var db = new OmnicellEntities();
            try
            {
                foreach (string tag in tags)
                {
                    if (tag.Contains("WikiGUID:"))
                    {
                        string guid = tag.Replace("WikiGUID:", "");

                        if (Guid.TryParse(guid, out docId))
                        {
                            var wiki = db.Omnicell_WikiMetaData2.FirstOrDefault(x => x.ContentId == docId);

                            if (wiki != null)
                            {
                                WikisGetOptions options = new WikisGetOptions();
                                options.Id = wiki.WikiId;
                                var file = PublicApi.Wikis.Get(options);

                                if (file != null)
                                {
                                    url = file.Url;
                                }
                            }
                        }
                    }


                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving wiki by tag."), ex).Log();
            }

            return url;


        }

        public string RetrieveTocList(int wikiId, string imgUrl)
        {
            string html = "<div><ul class='tree'> </ul></div>";
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);
            HtmlNode pNode = doc.DocumentNode.Descendants("div").FirstOrDefault();

            try
            {
                List<OmnicellWikiPage> wikiPages = new List<OmnicellWikiPage>();
                var db = new OmnicellEntities();

                var pWikis = db.WikiPageMetaDatas.Where(x => x.WikiId == wikiId && x.OrderNumber != null).OrderBy(c => c.ParentPageId).ThenBy(n => n.OrderNumber);

                foreach (var page in pWikis)
                {
                    int pageId = (int)page.WikiPageId;
                    OmnicellWikiPage wPage = new OmnicellWikiPage(pageId);

                    if (wPage != null)
                    {
                        wikiPages.Add(wPage);
                    }
                }

                if (wikiPages.Count > 0)
                {
                    wikiPages.ToList().ForEach(item => item.wikiPages = wikiPages.Where(c => c.ParentPageId == item.Id).ToList());
                    wikiPages.ForEach(i => i.wikiPages = wikiPages.Where(ch => ch.ParentPageId == i.Id).ToList());
                    wikiPages = wikiPages.Where(x => x.ParentPageId == -1).ToList();
                    pNode.InnerHtml = CreateNestedList(wikiPages, pNode, imgUrl);
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error creating table of contents for wiki with id: {0}", wikiId), ex).Log();
            }

            return pNode.InnerHtml;
        }

        public string CreateNestedList(List<OmnicellWikiPage> pages, HtmlNode node, string imgUrl)
        {
            OmnicellSharepointPlugin _plgnSP = new OmnicellSharepointPlugin();
            HtmlNode pNode = node.Descendants("ul").FirstOrDefault();

            foreach (OmnicellWikiPage page in pages)
            {

                string liHtml = string.Empty;
                if (page.wikiPages.Count > 0)
                {
                    liHtml = "<li class='top-level'></li>";
                }
                else
                {
                    liHtml = "<li class='second-level'></li>";
                }


                var liNode = HtmlNode.CreateNode(liHtml);
                pNode.AppendChild(liNode);
                string anHtml = String.Format("<a class='page-anchor' href='{0}'>{1}</a>", page.Url, page.Title);
                var aNode = HtmlNode.CreateNode(anHtml);

                if (page.wikiPages.Count > 0)
                {
                    string expandedUrl = imgUrl;
                    string img = string.Format("<img src='{0}' />", expandedUrl);
                    var imgNode = HtmlNode.CreateNode(img);
                    liNode.AppendChild(imgNode);
                }
               
                liNode.AppendChild(aNode);

                string hiddenV = string.Format("<input type='hidden' id='{0}' value='{1}' />", page.Id, page.Id);
                var inputNode = HtmlNode.CreateNode(hiddenV);
                liNode.AppendChild(inputNode);

                if (page.wikiPages.Count > 0)
                {
                    string cUl = "<ul class='toc-ul'></ul>";
                    var cNode = HtmlNode.CreateNode(cUl);
                    liNode.AppendChild(cNode);

                    CreateNestedList(page.wikiPages, liNode, imgUrl);

                }

            }

            return node.InnerHtml;
        }

        public void cleanHtmlPdf(string title, string html, string css, string copyYear, int? pageId, string name, string logoUrl, string rootUrl, string footerImg)
        {
            try
            {
                string apiKey = "gmjak34qvtveqvyg0ua";
               
                string handUrl = rootUrl + logoUrl;

                string footUrl = footerImg;

                
                var db = new OmnicellEntities();
                
                var wikiMeta = db.WikiPageMetaDatas.SingleOrDefault(x => x.WikiPageId == pageId);
                
                string copyTitle = name;

                if (wikiMeta != null)
                {
                    if (!string.IsNullOrEmpty(wikiMeta.PartName))
                    {
                        copyTitle += " | " + wikiMeta.PartName + " " + wikiMeta.RevNum;
                    }
                    //else
                    //{
                    //    copyTitle += " | ";
                    //}
                    
                }

                //html = html.Replace("</p>", "</p><br />");
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(html);
                doc.OptionFixNestedTags = true;
                doc.OptionWriteEmptyNodes = false;

                // trying to add horizontal rule above footer.

           
                //HtmlNode main = doc.DocumentNode.Descendants("div").FirstOrDefault();
                HtmlNode main = doc.DocumentNode.SelectSingleNode("//html");
                HtmlNode firstChild = main.FirstChild;
                
                List<HtmlNode> links = doc.DocumentNode.Descendants("a").ToList();
               
                main = ProcessImages(main, rootUrl);
                main = ProcessListItemLinks(main, rootUrl);
                
                //NoteProcesser(main);
                
                //NestedListFix(main);
                //NestedOlListFix(main);
                //ListPBreakFix(main);

                var mHrefs = main.Descendants("a").Where(n => !n.Attributes.Contains("href")).ToList();

                foreach (HtmlNode refs in mHrefs)
                {
                    refs.Remove();
                }

                if (main.SelectSingleNode("//table[@class='table']") != null)
                {
                    List<HtmlNode> table = main.SelectNodes("//table[@class='table']").ToList();

                    

                    foreach (HtmlNode cNode in table)
                    {

                        HtmlNode tParent = cNode.ParentNode;
                        HtmlNode lParent = tParent.ParentNode;
                       
                        HtmlNode pSib = cNode.PreviousSibling;

                        if (pSib != null && pSib.Name == "a")
                        {
                            pSib.Remove();
                        }

                        if (cNode.SelectNodes("//caption") != null)
                        {
                            HtmlNode cap = cNode.SelectSingleNode("//caption");
                            string innerCap = "<span style=\"text-align: center; vertical-align: bottom;\">" + cap.InnerHtml + "</span><br />";
                            string breakTag = "<br /><br />";
                            var bNode = HtmlNode.CreateNode(breakTag);
                            var newNode = HtmlNode.CreateNode(innerCap);

                            HtmlNode parent = cNode.ParentNode;
                            cap.Remove();
                            parent.InsertBefore(newNode, cNode);
                            parent.InsertBefore(bNode, cNode);
                        }

                        if (cNode.SelectSingleNode("//th") != null)
                        {

                            List<HtmlNode> ths = cNode.SelectNodes("//th").ToList();

                            foreach (HtmlNode th in ths)
                            {
                                th.Attributes.Remove("width");
                                th.Attributes.Add("style", "width:auto;");
                            }
                        }

                        if (cNode.SelectSingleNode("//td") != null)
                        {
                            List<HtmlNode> tds = cNode.SelectNodes("//td").ToList();

                            foreach (HtmlNode td in tds)
                            {
                                td.Attributes.Add("style", "width:auto;");
                            }

                        }
                    }
                }
                
                string text = main.OuterHtml;
                text = text.Replace("\n", string.Empty).Replace("\r", string.Empty).Replace("\t", string.Empty);
                text = text.Replace("<BR>", "<br />").Replace("<br>", "<br />");

                WikiPageExtensions.CreateHiQPdf(css, text, title, copyTitle, handUrl, footUrl, copyYear);

                //WikiPageExtensions.CreatePdf(css, text, title, copyTitle, handUrl, footUrl, copyYear);
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error creating pdf for wiki page with title: {0}", title), ex).Log();
            }
            
 
        }

        public void NestedListFix(HtmlNode node)
        {
            if (node.SelectSingleNode("//ol") != null)
            {
                List<HtmlNode> list = node.SelectNodes("//ol").ToList();

                foreach (HtmlNode lNode in list)
                {
                    List<HtmlNode> uList = lNode.Descendants("ul").ToList();

                    foreach (HtmlNode uNode in uList)
                    {
                        var next = uNode.NextSibling;

                        if (next != null)
                        {
                            var lastChild = uNode.Descendants("li").LastOrDefault();

                            if (lastChild != null)
                            {
                                RemoveNode(next, lastChild);
                            }
                        }
                    }
                }
            }
        }

        public void NestedOlListFix(HtmlNode node)
        {
            if (node.SelectSingleNode("//ol") != null)
            {
                List<HtmlNode> list = node.SelectNodes("//ol").ToList();

                foreach (HtmlNode lNode in list)
                {
                    List<HtmlNode> uList = lNode.Descendants("ol").ToList();

                    foreach (HtmlNode uNode in uList)
                    {
                        var next = uNode.NextSibling;

                        if (next != null)
                        {
                            var lastChild = uNode.Descendants("li").LastOrDefault();

                            if (lastChild != null)
                            {
                                RemoveNode(next, lastChild);
                            }
                        }
                    }
                }
            }
        }

        public void ListPBreakFix(HtmlNode node)
        {
            if (node.SelectSingleNode("//ul") != null)
            {
                List<HtmlNode> list = node.SelectNodes("//ul").ToList();

                foreach (HtmlNode lNode in list)
                {
                    List<HtmlNode> listItem = lNode.Descendants("li").ToList();

                    foreach (HtmlNode uNode in listItem)
                    {

                        if (node.SelectSingleNode("//p") != null)
                        {
                            List<HtmlNode> pNodes = uNode.Descendants("p").ToList();

                           foreach(HtmlNode pNode in pNodes)
                           {
                               if (pNode.SelectSingleNode("//br") != null)
                               {
                                   var breakTag = pNode.Descendants("br").LastOrDefault();

                                   breakTag.Remove();
                               }
                           }

                        }
                    }
                }
            }
        }

        public void NoteProcesser(HtmlNode node)
        {
            List<HtmlNode> notes = node.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("note"))).ToList();
            string table = "<table class=\"note\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>&nbsp;</td></tr></table>";
            var tableNode = HtmlNode.CreateNode(table);
            foreach (HtmlNode note in notes)
            {
                note.InnerHtml = table + note.InnerHtml + table;
            }
        }

       

        public void RemoveNode(HtmlNode node, HtmlNode last)
        {
            HtmlNode nextNode = node.NextSibling;

            node.Remove();
            last.AppendChild(node);

            if (nextNode != null)
            {
                RemoveNode(nextNode, last);
            }



        }

        public HtmlNode ProcessImages(HtmlNode node, string rootUrl)
        {
            string apiKey = "gmjak34qvtveqvyg0ua";
            List<HtmlNode> images = node.Descendants("img").ToList();
            List<HtmlNode> dupNodes = new List<HtmlNode>();
            string fileName = String.Empty;
            string bImage = String.Empty;
         
            foreach (HtmlNode image in images)
            {
                dupNodes.Add(image);
            }

            for (int i = dupNodes.Count - 1; i >= 0; i--)
            {
                string style = string.Empty;
                fileName = images[i].Attributes["src"].Value;
                string fileUrl = fileName.Replace("/resized-image.ashx/__size/400x0/", "/cfs-file.ashx/");

              //  string handUrl = string.Format("{0}/custom/DitaImageHandler.ashx?apikey={1}&url={2}", rootUrl, apiKey, fileUrl);
                string handUrl = string.Format("{0}/custom/DitaImageHandler.ashx?apikey={1}&url={2}", rootUrl, apiKey, fileName);
                images[i].SetAttributeValue("src", handUrl);
               string width = images[i].GetAttributeValue("width", "1");
               string height = images[i].GetAttributeValue("height", "1");

               //if (!width.Equals("1"))
               //{
               //    int sWidth = Convert.ToInt32(width);
               //    double tempWidth = sWidth * .5;
               //    int iWidth = Convert.ToInt32(tempWidth) + sWidth;
               //    width = iWidth.ToString() + "px";

               //    images[i].SetAttributeValue("width", width);
               //}

               //if (!height.Equals("1"))
               //{
               //    int sHeight = Convert.ToInt32(height);
               //    double tempHeight = sHeight * .5;
               //    int iHeight = Convert.ToInt32(tempHeight) + sHeight;
               //    height = iHeight.ToString() + "px";
               //    images[i].SetAttributeValue("height", height);

               //}

               //if (!height.Equals("1") && !width.Equals("1"))
               //{
               //    style = string.Format("width:{0};height:{1};", width, height);
               //    images[i].SetAttributeValue("style", style);
               //}


                HtmlNode aNode = images[i].ParentNode;
                aNode.ParentNode.RemoveChild(aNode, true);
                images.Remove(images[i]);
            }

            return node;
        }

        private HtmlNode ProcessListItemLinks(HtmlNode node, string rootUrl)
        {
            OmnicellSharepointPlugin plg = new OmnicellSharepointPlugin();
            List<HtmlNode> linkNodes = node.Descendants("a").Where(a => a.Attributes.Contains("title")).ToList();
            List<HtmlNode> dupNodes = new List<HtmlNode>();
            string linkText = String.Empty;

            foreach (HtmlNode cNode in linkNodes)
            {
                dupNodes.Add(cNode);
            }

            for (int i = dupNodes.Count - 1; i >= 0; i--)
            {
              
                string url = linkNodes[i].Attributes["href"].Value;

                if (url.Contains("http://") || url.Contains("https://"))
                {
                    if (url.Contains(rootUrl))
                    {
                        linkText = "<span>" + linkNodes[i].InnerHtml + "</span>";
                        var newNode = HtmlNode.CreateNode(linkText);
                        linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                        linkNodes.Remove(linkNodes[i]);
                    }
                }
                else
                {
                    linkText = "<span>" + linkNodes[i].InnerHtml + "</span>";
                    var newNode = HtmlNode.CreateNode(linkText);
                    linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                    linkNodes.Remove(linkNodes[i]);
                }
            }


            return node;
        }

        public string RetrieveFileStore(string pKey)
        {
            string key = "invalid url";
            var cfs = CentralizedFileStorage.GetCentralizedFileByUrl(pKey);

            if (cfs != null)
            {
                key = cfs.FileName;
            }

            return key;
        }

        public List<OmnicellWikiPage> RetrieveSubPages(int? wikiPageId)
        {
            List<OmnicellWikiPage> rPages = new List<OmnicellWikiPage>();

            try
            {
                using (var db = new OmnicellEntities())
                {
                   var dbEntity = db.WikiPageMetaDatas.Where(x => x.ParentPageId == wikiPageId).ToList();

                   dbEntity = dbEntity.OrderBy(x => x.OrderNumber).ToList();

                   foreach (WikiPageMetaData meta in dbEntity)
                   {
                       OmnicellWikiPage page = new OmnicellWikiPage((int)meta.WikiPageId);

                       if (page != null)
                       {
                           rPages.Add(page);
                       }

                   }
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving subpages for wiki page with id: {0}", wikiPageId), ex).Log();
            }

           
            return rPages;
        }

    }
}

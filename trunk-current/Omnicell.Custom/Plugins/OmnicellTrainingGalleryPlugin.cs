﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using TComp = Telligent.Evolution.Components;
using Telligent.DynamicConfiguration.Components;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;

using GTT = GoToTraining.Api.Components;

using Omnicell.Custom.Components;

namespace Omnicell.Custom
{
    public class OmnicellTrainingGalleryPlugin : IConfigurablePlugin, IScriptedContentFragmentExtension
    {

        #region IConfigurablePlugin Members

        IPluginConfiguration Configuration { get; set; }

        public Telligent.DynamicConfiguration.Components.PropertyGroup[] ConfigurationOptions
        {
            get
            {
                PropertyGroup[] groups = new[] { new PropertyGroup("groups", "Groups", 0), new PropertyGroup("galleries", "Training Galleries", 1) };

                Property productGroupIdProp = new Property("productGroupId", "Product Group", PropertyType.Custom, 0, "Group=5");
                productGroupIdProp.DescriptionText = "Select the parent Product group.";
                productGroupIdProp.ControlType = typeof(Telligent.Evolution.Controls.GroupSelectionList);
                groups[0].Properties.Add(productGroupIdProp);

                Property trainingGroupIdProp = new Property("trainingGroupId", "Training Group", PropertyType.Custom, 0, "Group=27");
                trainingGroupIdProp.DescriptionText = "Select the parent Training group.";
                trainingGroupIdProp.ControlType = typeof(Telligent.Evolution.Controls.GroupSelectionList);
                groups[0].Properties.Add(trainingGroupIdProp);

                Property subscriptionGroupIdProp = new Property("subscriptionGroupId", "Subscription Group", PropertyType.Custom, 0, "");
                subscriptionGroupIdProp.DescriptionText = "Select the parent Subscription group.";
                subscriptionGroupIdProp.ControlType = typeof(Telligent.Evolution.Controls.GroupSelectionList);
                groups[0].Properties.Add(subscriptionGroupIdProp);

                Property instructorLedTgIdProp = new Property("instructorLedTgId", "Instructor Led Training Gallery", PropertyType.Custom, 0, "");
                instructorLedTgIdProp.DescriptionText = "Enter the name of the training gallery that contains Instructor Led courses.";
                instructorLedTgIdProp.ControlType = typeof(Telligent.Evolution.MediaGalleries.Controls.MediaGallerySelectionControl);
                instructorLedTgIdProp.Attributes.Add("maximumMediaGallerySelections", "1");
                instructorLedTgIdProp.Attributes.Add("enableGroupSelection", "false");
                groups[1].Properties.Add(instructorLedTgIdProp);

                Property onlineTgIdProp = new Property("onlineTgId", "Online Training Gallery", PropertyType.Custom, 0, "");
                onlineTgIdProp.DescriptionText = "Enter the name of the training gallery that contains Online courses.";
                onlineTgIdProp.ControlType = typeof(Telligent.Evolution.MediaGalleries.Controls.MediaGallerySelectionControl);
                onlineTgIdProp.Attributes.Add("maximumMediaGallerySelections", "1");
                onlineTgIdProp.Attributes.Add("enableGroupSelection", "false");
                groups[1].Properties.Add(onlineTgIdProp);

                Property subSchedTgIdProp = new Property("subSchedTgId", "Subscriber Scheduled Training Gallery", PropertyType.Custom, 0, "");
                subSchedTgIdProp.DescriptionText = "Enter the name of the training gallery that contains Subscriber Scheduled courses.";
                subSchedTgIdProp.ControlType = typeof(Telligent.Evolution.MediaGalleries.Controls.MediaGallerySelectionControl);
                subSchedTgIdProp.Attributes.Add("maximumMediaGallerySelections", "1");
                subSchedTgIdProp.Attributes.Add("enableGroupSelection", "false");
                groups[1].Properties.Add(subSchedTgIdProp);

                Property subRecTgIdProp = new Property("subRecTgId", "Subscriber Recorded Training Gallery", PropertyType.Custom, 0, "");
                subRecTgIdProp.DescriptionText = "Enter the name of the training gallery that contains Subscriber Recorded courses.";
                subRecTgIdProp.ControlType = typeof(Telligent.Evolution.MediaGalleries.Controls.MediaGallerySelectionControl);
                subRecTgIdProp.Attributes.Add("maximumMediaGallerySelections", "1");
                subRecTgIdProp.Attributes.Add("enableGroupSelection", "false");
                groups[1].Properties.Add(subRecTgIdProp);

                return groups;
            }
        }
        public void Update(IPluginConfiguration configuration)
        {
            Configuration = configuration;
            ExtractConfiguration(configuration);
        }

        #endregion IConfigurablePlugin Members

        #region IScriptedContentFragmentExtension Members

        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Training Gallery Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Training Gallery."; }
        }
        public object Extension
        {
            get { return new OmnicellTrainingGalleryWidgetExtension(Configuration); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_traininggallery"; }
        }

        public int ProductGroupId { get; private set; }
        public int TrainingGroupId { get; private set; }
        public int SubscriptionGroupId { get; private set; }
        public int InstructorLedGalleryId { get; private set; }
        public int OnlineGalleryId { get; private set; }
        public int SubscriberScheduledGalleryId { get; private set; }
        public int SubscriberRecordedGalleryId { get; private set; }

        #endregion IScriptedContentFragmentExtension Members

        private void ExtractConfiguration(IPluginConfiguration configuration)
        {
            ProductGroupId = GetValue(configuration.GetCustom("productGroupId"), -1);
            TrainingGroupId = GetValue(configuration.GetCustom("trainingGroupId"), -1);
            SubscriptionGroupId = GetValue(configuration.GetCustom("subscriptionGroupId"), -1);
            InstructorLedGalleryId = GetValue(configuration.GetCustom("instructorLedTgId"), -1);
            OnlineGalleryId = GetValue(configuration.GetCustom("onlineTgId"), -1);
            SubscriberScheduledGalleryId = GetValue(configuration.GetCustom("subSchedTgId"), -1);
            SubscriberRecordedGalleryId = GetValue(configuration.GetCustom("subRecTgId"), -1);
        }
        private int GetValue(string pair, int defaultValue)
        {
            int returnValue = defaultValue;
            if (!string.IsNullOrEmpty(pair) && pair.Contains("="))
            {
                if (pair.Contains("&"))
                    pair = pair.Split("&".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];

                string[] segs = pair.Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (segs.Length > 1)
                    int.TryParse(segs[1], out returnValue);
            }
            return returnValue;
        }
    }
    public class OmnicellTrainingGalleryWidgetExtension
    {
        private ITrainingGalleryService _svcTrainingGallery = null;

        public int ProductGroupId { get; private set; }
        public int TrainingGroupId { get; private set; }
        public int SubscriptionGroupId { get; private set; }
        public int InstructorLedGalleryId { get; private set; }
        public int OnlineGalleryId { get; private set; }
        public int SubscriberScheduledGalleryId { get; private set; }
        public int SubscriberRecordedGalleryId { get; private set; }

        public OmnicellTrainingGalleryWidgetExtension(IPluginConfiguration configuration)
        {
            _svcTrainingGallery = Telligent.Common.Services.Get<ITrainingGalleryService>();
            ExtractConfiguration(configuration);
        }

        public TrainingGalleryFilters GetFilterChoices(int trainingGalleryId)
        {
            if (trainingGalleryId > 0)
                return _svcTrainingGallery.GetFilters(trainingGalleryId);
            else
                return new TrainingGalleryFilters();
        }
        public AdditionalInfo SaveFilterChoices(int trainingGalleryId, int galleryType, bool showCourseInfo, int courseDateType, string mediaFormatFilterIdList, string courseTypeIdList)
        {
            AdditionalInfo ai = null;
            try
            {
                char[] commaSep = new char[] { ',' };
                TrainingGalleryFilters filters = new TrainingGalleryFilters
                {
                    GalleryId = trainingGalleryId,
                    GalleryType = galleryType,
                    ShowCourseInfo = showCourseInfo,
                    CourseDateType = courseDateType
                };

                if (!string.IsNullOrEmpty(mediaFormatFilterIdList))
				{
                    filters.MediaFormatFilters = mediaFormatFilterIdList.Split(commaSep, StringSplitOptions.RemoveEmptyEntries).Select(i => new MediaFormat { Id = int.Parse(i) }).ToList();
				}
                if (!string.IsNullOrEmpty(courseTypeIdList))
				{
                    filters.CourseTypeFilters = courseTypeIdList.Split(commaSep, StringSplitOptions.RemoveEmptyEntries).Select(i => new CourseType { Id = int.Parse(i) }).ToList();
				}
                _svcTrainingGallery.SaveFilters(filters);
            }
            catch (Exception ex)
            {
                ai = new AdditionalInfo(new CSException(CSExceptionType.UnknownError, ex.Message, ex));
            }
            return ai == null ? new AdditionalInfo() : ai;
        }

        private void ExtractConfiguration(IPluginConfiguration configuration)
        {
            ProductGroupId = GetValue(configuration.GetCustom("productGroupId"), -1);
            TrainingGroupId = GetValue(configuration.GetCustom("trainingGroupId"), -1);
            SubscriptionGroupId = GetValue(configuration.GetCustom("subscriptionGroupId"), -1);
            InstructorLedGalleryId = GetValue(configuration.GetCustom("instructorLedTgId"), -1);
            OnlineGalleryId = GetValue(configuration.GetCustom("onlineTgId"), -1);
            SubscriberScheduledGalleryId = GetValue(configuration.GetCustom("subSchedTgId"), -1);
            SubscriberRecordedGalleryId = GetValue(configuration.GetCustom("subRecTgId"), -1);
        }
        private int GetValue(string pair, int defaultValue)
        {
            int returnValue = defaultValue;
            if (!string.IsNullOrEmpty(pair) && pair.Contains("="))
            {
                if (pair.Contains("&"))
                    pair = pair.Split("&".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0];

                string[] segs = pair.Split("=".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (segs.Length > 1)
                    int.TryParse(segs[1], out returnValue);
            }
            return returnValue;
        }

    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.MediaGalleries.Components;
using Telligent.Evolution.Components.Search;

using Omnicell.Custom;
using Omnicell.Custom.Components;
using Omnicell.Custom.Search;
using ODb = Omnicell.Data.Model;

namespace Omnicell.Custom
{
    public class OmnicellTrainingUserPlugin : IScriptedContentFragmentExtension
    {
        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Training User Plugin"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality from the "; }
        }
        public object Extension
        {
            get { return new OmnicellTrainingUserPluginExtension(); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_traininguser"; }
        }
    }

    public class OmnicellTrainingUserPluginExtension
    {
        private IMediaGalleryDataService _svcMediaGallery = null;
        private IMediaGalleryPostDataService _svcMediaGalleryPost = null;
        private ITrainingService _svcTraining = null;

        public OmnicellTrainingUserPluginExtension()
        {
            _svcMediaGalleryPost = Telligent.Common.Services.Get<IMediaGalleryPostDataService>();
            _svcMediaGallery = Telligent.Common.Services.Get<IMediaGalleryDataService>();
            _svcTraining = Telligent.Common.Services.Get<ITrainingService>();
        }

        // SMR - 20131113 - New method to return a paged list instead of a generic list
        public V1Entities.PagedList<V1Entities.GroupUser> PagedListMembers(int groupId, string privateEmail = "", string csn = "", int pageIndex = 0, int pageSize = 10)
        {
            var allGroupMembers = PublicApi.GroupUserMembers.List(groupId, new GroupUserMembersListOptions { MembershipType = "Member", PageSize = pageSize });
            List<V1Entities.GroupUser> filteredList = new List<V1Entities.GroupUser>();

            foreach (V1Entities.GroupUser groupMember in allGroupMembers)
            {
                if (groupMember != null)
                    filteredList.Add(groupMember);
            }

            if (!string.IsNullOrEmpty(privateEmail))
            {
                filteredList = filteredList.Where(m => m.User.PrivateEmail != null && m.User.PrivateEmail == privateEmail).ToList();
            }
            else
            {
                if (!string.IsNullOrEmpty(csn))
                {
                    filteredList = allGroupMembers.Where(m => m.User.ProfileFields["CSN"] != null && m.User.ProfileFields["CSN"].Value == csn).ToList();
                }
                else
                {
                    filteredList = allGroupMembers.ToList();
                }
            }
            // Convert the simple list to a telligent user list
            V1Entities.PagedList<V1Entities.GroupUser> filteredPagedList = new V1Entities.PagedList<V1Entities.GroupUser>();
            foreach (V1Entities.GroupUser user in filteredList)
                filteredPagedList.Add(user);

            return filteredPagedList;
        }
        // SMR - 20131113 - Paged List of Non-Members
        public V1Entities.PagedList<V1Entities.User> PagedListNonMembers(int groupId, string privateEmail = "", string csn = "", int pageIndex = 0, int pageSize = 10)
        {
            var allSiteMembers = PublicApi.Users.List(new UsersListOptions { PageSize = 5000 });

            List<V1Entities.User> filteredList = new List<V1Entities.User>();
            foreach (V1Entities.User user in allSiteMembers)
            {
                V1Entities.GroupUser groupMember = PublicApi.GroupUserMembers.Get(groupId, new GroupUserMembersGetOptions { UserId = user.Id });
                if (groupMember == null)
                {
                    filteredList.Add(user);
                }
            }
            if (!string.IsNullOrEmpty(privateEmail))
            {
                filteredList = filteredList.Where(u => u.PrivateEmail != null && u.PrivateEmail == privateEmail).ToList();
            }
            else
            {
                if (!string.IsNullOrEmpty(csn))
                {
                    filteredList = filteredList.Where(u => u.ProfileFields["CSN"] != null && u.ProfileFields["CSN"].Value == csn).ToList();
                }
            }
            
            V1Entities.PagedList<V1Entities.User> filteredPagedList = new V1Entities.PagedList<V1Entities.User>();
            foreach (V1Entities.User user in filteredList)
            {
                filteredPagedList.Add(user);
            }
            filteredPagedList.PageIndex = pageIndex;
            filteredPagedList.PageSize = pageSize;
            filteredPagedList.TotalCount = filteredPagedList.Count;
            return filteredPagedList;
        }

        public List<V1Entities.GroupUser> ListMembers(int groupId, string privateEmail = "", string csn = "", int pageIndex = 0, int pageSize = 10)
        {
            var allGroupMembers = PublicApi.GroupUserMembers.List(groupId, new GroupUserMembersListOptions { MembershipType = "Member", PageSize = pageSize });
            List<V1Entities.GroupUser> filteredList = new List<V1Entities.GroupUser>();

            foreach (V1Entities.GroupUser groupMember in allGroupMembers)
            {
                if (groupMember != null)
                {
                    filteredList.Add(groupMember);
                }
            }

            if (!string.IsNullOrEmpty(privateEmail))
            {
                filteredList = filteredList.Where(m => m.User.PrivateEmail != null && m.User.PrivateEmail == privateEmail).ToList();
            }
            else
            {
                if (!string.IsNullOrEmpty(csn))
                {
                    filteredList = allGroupMembers.Where(m => m.User.ProfileFields["CSN"] != null && m.User.ProfileFields["CSN"].Value == csn).ToList();
                }
                else
                {
                    filteredList = allGroupMembers.ToList();
                }
            }
            return filteredList;
        }

        public List<V1Entities.User> ListNonMembers(int groupId, string privateEmail = "", string csn = "", int pageIndex = 0, int pageSize = 10)
        {
            var allSiteMembers = PublicApi.Users.List(new UsersListOptions { PageSize = pageSize });

            List<V1Entities.User> filteredList = new List<V1Entities.User>();
            foreach (V1Entities.User user in allSiteMembers)
            {
                V1Entities.GroupUser groupMember = PublicApi.GroupUserMembers.Get(groupId, new GroupUserMembersGetOptions { UserId = user.Id });
                if (groupMember == null)
                {
                    filteredList.Add(user);
                }
            }
            if (!string.IsNullOrEmpty(privateEmail))
            {
                filteredList = filteredList.Where(u => u.PrivateEmail != null && u.PrivateEmail == privateEmail).ToList();
            }
            else
            {
                if (!string.IsNullOrEmpty(csn))
                {
                    filteredList = filteredList.Where(u => u.ProfileFields["CSN"] != null && u.ProfileFields["CSN"].Value == csn).ToList();
                }
            }

            return filteredList;
        }

        public V1Entities.AdditionalInfo RemoveMembers(int groupId, string ids)
        {
            V1Entities.AdditionalInfo ai = new V1Entities.AdditionalInfo();

            try
            {
                if (ids.Length > 0)
                {
                    string[] idArray = ids.Split(',');
                    if (idArray.Length > 0)
                    {
                        foreach (string id in idArray)
                        {
                            int userId = int.Parse(id);
                            PublicApi.GroupUserMembers.Delete(groupId, new GroupUserMembersDeleteOptions() { UserId = userId });
                            var user = PublicApi.Users.Get(new UsersGetOptions { Id = userId });

                            if (user != null && PublicApi.UserProfileFields.Get("AuthDate") != null)
                            {
                                V1Entities.ProfileField pf = new V1Entities.ProfileField { Label = "AuthDate", Value = string.Empty };
                                PublicApi.Users.Update(new UsersUpdateOptions { Id = user.Id, ProfileFields = new List<V1Entities.ProfileField> { pf } });
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ai.Errors.Add(new V1Entities.Error(ex.Message));
            }
        
            return ai;
        }

        public V1Entities.AdditionalInfo AddMembers(int groupId, string ids)
        {
            V1Entities.AdditionalInfo ai = new V1Entities.AdditionalInfo();
            
            try
            {
                if (ids.Length > 0)
                {
                    string[] idArray = ids.Split(',');
                    if (idArray.Length > 0)
                    {
                        foreach (string id in idArray)
                        {
                            int userId = int.Parse(id);
                            PublicApi.GroupUserMembers.Create(groupId, userId, new GroupUserMembersCreateOptions() { GroupMembershipType = "Member" });

                            var user = PublicApi.Users.Get(new UsersGetOptions { Id = userId });

                            if (user != null && PublicApi.UserProfileFields.Get("AuthDate") != null)
                            {
                               V1Entities.ProfileField pf = new V1Entities.ProfileField { Label = "AuthDate", Value =  DateTime.Now.ToShortDateString() };
                               PublicApi.Users.Update(new UsersUpdateOptions { Id = user.Id, ProfileFields = new List<V1Entities.ProfileField> { pf } });
                            }
                        }
                    }
                }
           }
           catch(Exception ex)
            {
                ai.Errors.Add(new V1Entities.Error(ex.Message));
            }
            
            return ai;
        }

        public void AddMembertoRole(int userId, string roleName)
        {
            User accessUser = CSContext.Current.User;
            User adminUser = Telligent.Evolution.Users.GetUser("admin");

            CSContext.Current.User = adminUser;

            PublicApi.Roles.Create("", "", new RolesCreateOptions { UserId = userId, RoleName = roleName, Include = "user" });

            CSContext.Current.User = accessUser;
        }

        public void RemoveMemberFromRole(int userId, int roleId)
        {
            User accessUser = CSContext.Current.User;
            User adminUser = Telligent.Evolution.Users.GetUser("admin");

            CSContext.Current.User = adminUser;

            PublicApi.Roles.Delete(roleId, new RolesDeleteOptions { UserId = userId, Include = "user" });


            CSContext.Current.User = accessUser;



        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.MediaGalleries.Components;
using Telligent.Evolution.Components.Search;

using Omnicell.Custom;
using Omnicell.Custom.Components;
using Omnicell.Custom.Search;
using ODb = Omnicell.Data.Model;

namespace Omnicell.Custom
{
    public class OmnicellTrainingWidgetPlugin : IScriptedContentFragmentExtension
    {
        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Training Widgets"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality from the "; }
        }
        public object Extension
        {
            get { return new OmnicellTrainingWidgetExtension(); }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_training"; }
        }
    }

    public class OmnicellTrainingWidgetExtension
    {
        private IMediaGalleryDataService _svcMediaGallery = null;
        private IMediaGalleryPostDataService _svcMediaGalleryPost = null;
        private ITrainingService _svcTraining = null;

        public OmnicellTrainingWidgetExtension()
        {
            _svcMediaGalleryPost = Telligent.Common.Services.Get<IMediaGalleryPostDataService>();
            _svcMediaGallery = Telligent.Common.Services.Get<IMediaGalleryDataService>();
            _svcTraining = Telligent.Common.Services.Get<ITrainingService>();
        }
   

    }

}

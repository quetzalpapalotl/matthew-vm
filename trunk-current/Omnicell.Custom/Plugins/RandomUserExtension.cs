﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;

namespace Omnicell.Custom.Plugins
{
    public class RandomUserExtension : IScriptedContentFragmentExtension
    {
        public object Extension
        {
            get { return new RandomUser(); }
        }

        public string ExtensionName
        {
            get { return "telligent_v1_test_prep"; }
        }

        public string Description
        {
            get { return "Widget Extension for test preperation"; }
        }

        public void Initialize()
        {
            
        }

        public string Name
        {
            get { return "Random user test prep (telligent_v1_test_prep)"; }
        }
    }

    public class RandomUser
    {
        public User GetRandomUser()
        {
            var random = new Random(DateTime.UtcNow.Millisecond);

            var users = PublicApi.Users.List(
                new UsersListOptions {
                    SortBy = "JoinedDate",
                    SortOrder = "Descending",
                    PageSize = 50,
                    PageIndex = 0

            });

            if (users.Count > 0)
                return users[random.Next(users.Count - 1)];


            return null;
        }
    }
}

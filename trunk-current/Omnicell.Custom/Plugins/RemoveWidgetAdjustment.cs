﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Evolution.Extensibility.UI.Version1;
namespace Omnicell.Custom.Plugins
{
    public class RemoveWidgetAdjustment : IContentFragmentImportAdjustment
    {
        public void Adjust(ref string contentFragmentId, ref string configuration)
        {
            if(contentFragmentId != ContentFragments.GetScriptedContentFragmentTypeString(new Guid("18be891126204553a6fba048937b908f")))
                return;

            contentFragmentId = ContentFragments.GetScriptedContentFragmentTypeString(new Guid("50c816b46c7647d5a7cea85b3f47d4d6"));

            configuration = "html=" +
                    System.Web.HttpUtility.UrlEncode("<b>The Have We Met widget is no longer supported.</b>") +
                    "&title=" +
                    System.Web.HttpUtility.UrlEncode("Not Supported");

        }

        public string Description
        {
            get { return "Removes widget from page versions."; }
        }

        public void Initialize()
        {
            
        }

        public string Name
        {
            get { return "Remove Widget Plugin"; }
        }
    }
}

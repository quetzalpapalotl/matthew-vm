﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;

namespace Omnicell.Custom.Plugins
{
    public class SampleCsModule : ICSModule
    {
          private string userNameToAdd = "quetzalpapalotl";
        public void Init(CSApplication csa, System.Xml.XmlNode node)
        {
            
            GroupEvents.AfterAddGroup += new GroupEventHandler(GroupEvents_AfterAddGroup);

            var attributes = node.Attributes;
            if (attributes != null)
            {
                if (attributes["userNameToAdd"].Value != null) 
                {
                    userNameToAdd = attributes["userNameToAdd"].Value;
                }
            }

        }

        void GroupEvents_AfterAddGroup(Group group, EventArgs e)
        {
            Telligent.Evolution.Extensibility.Api.Entities.Version1.Group apiGroup =
                    PublicApi.Groups.Get(new GroupsGetOptions() { Id = group.ID });

            if (string.IsNullOrEmpty(userNameToAdd)) return;

            Telligent.Evolution.Extensibility.Api.Entities.Version1.User user =
                    PublicApi.Users.Get(new UsersGetOptions() { Username = userNameToAdd });

            if (user != null)
            {
                PublicApi.GroupUserMembers.Create(apiGroup.Id.Value, user.Id.Value,
                    new GroupUserMembersCreateOptions() { GroupMembershipType = "Owner" });
                        
            }
        }
    }
}
    
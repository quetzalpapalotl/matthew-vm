﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;

using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;

namespace Omnicell.Custom.Plugins
{
   public class SecretContextProvider : IScriptedContentFragmentContextProvider
    {

        public string Description
        {
            get { return "Hides widgets when secret=1 is add to the page's querystring."; }
        }

        public void Initialize()
        {
            
        }

        public string Name
        {
            get { return "Secret Widget Context Provider"; }
        }

        private readonly Guid SecretId = new Guid("ebb6f115ed0c422791cee138934acb8b");

        public IEnumerable<ContextItem> GetSupportedContextItems()
        {
            var items = new List<ContextItem>();

            items.Add(new ContextItem("Secret", SecretId));

            return items;

        }

        public bool HasContextItem(Page page, Guid contextItemId)
        {
            if (contextItemId != SecretId)
                return false;

            return page.Request.QueryString["secret"] == "1";
        }
    }
}

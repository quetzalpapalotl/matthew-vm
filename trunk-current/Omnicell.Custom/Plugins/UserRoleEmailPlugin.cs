﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Email.Version1;

namespace Omnicell.Custom.Plugins
{
    public class UserRoleEmailPlugin : IEmailTemplateExtension
    {

        public object Extension
        {
            get { return new User_Role(); }
        }

        public string ExtensionName
        {
            get { return "omnicell_v1_userroleemail"; }
        }

        public string Description
        {
            get { return "Enables email Templates to retrieve user roles."; }
        }

        public void Initialize()
        {
        }

        public string Name
        {
            get { return "Omnicell Email User Role (omnicell_v1_userroleemail)"; }
        }
    }

    public class User_Role
    {
        public string InvalidUserRole(string currentUser)
        {
            string roleType = "";
            bool userIsInRole = false;
            string[] roleInternational = { "International Partners Unverified" };
            string[] roleEmployee = { "Employee Inactive" };
            string[] roleCustomer = { "Customer Unverified" };

            Telligent.Evolution.Extensibility.Api.Entities.Version1.User user =
                   PublicApi.Users.Get(new UsersGetOptions() { Username = currentUser});

            if (user != null)
            {
                userIsInRole = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.RoleUsers.IsUserInRoles(user.Username, roleInternational);

                if (userIsInRole == true)
                {
                    roleType = "International Partner";
                }

                userIsInRole = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.RoleUsers.IsUserInRoles(user.Username, roleEmployee);

                if (userIsInRole == true)
                {
                    roleType = "Employee";
                }

                userIsInRole = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.RoleUsers.IsUserInRoles(user.Username, roleCustomer);

                if (userIsInRole == true)
                {
                    roleType = "Customer";
                }

            }
            else
            {
                return "";
            }


            return roleType;
        }
    }
}

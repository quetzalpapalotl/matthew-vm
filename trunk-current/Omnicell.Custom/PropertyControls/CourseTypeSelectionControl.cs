﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

using Omnicell.Custom.Components;

using Telligent.Common;
using Telligent.DynamicConfiguration.Components;

namespace Omnicell.Custom
{
    public class CourseTypeSelectionControl : Control, IPropertyControl
    {
        readonly ICourseTypeService _svcCT = Telligent.Common.Services.Get<ICourseTypeService>();
        DropDownList _list = null;
        PlaceHolder _wrapper = null;

        #region Control Members

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            EnsureChildControls();
        }
        protected override void CreateChildControls()
        {
            base.CreateChildControls();

            var formats = _svcCT.List();

            _wrapper = new PlaceHolder();
            _list = new DropDownList();
            _list.Items.Add(new ListItem("All Course Types", "-1"));
            _list.Items.AddRange(_svcCT.List().OrderBy(f => f.Value).Select(f => new ListItem(f.Value, f.Id.ToString())).ToArray());

            _wrapper.Controls.Add(_list);

            Controls.Add(_wrapper);
        }

        #endregion Control Members

        #region IPropertyControl

        public ConfigurationDataBase ConfigurationData { get; set; }
        public Property ConfigurationProperty { get; set; }
        public event ConfigurationPropertyChanged ConfigurationValueChanged
        {
            add { throw new NotSupportedException(); }
            remove { }
        }
        public new Control Control { get { return this; } }

        public object GetConfigurationPropertyValue()
        {
            EnsureChildControls();
            return _list.SelectedValue;
        }
        public void SetConfigurationPropertyValue(object value)
        {
            EnsureChildControls();

            if (!string.IsNullOrEmpty((value.ToString())))
                _list.SelectedValue = value.ToString();
        }

        #endregion IPropertyControl

    }
}

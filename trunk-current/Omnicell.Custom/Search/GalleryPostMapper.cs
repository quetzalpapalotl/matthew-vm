﻿using System;
using System.Collections.Generic;
using System.Linq;
using Omnicell.Custom.Components;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Version1;

namespace Omnicell.Custom.Search
{
    public class GalleryPostMapper : IPlugin
    {
        readonly IMediaFormatService _svcMF = Telligent.Common.Services.Get<IMediaFormatService>();
        readonly ICourseTypeService _svcCT = Telligent.Common.Services.Get<ICourseTypeService>();
        readonly IAudienceTypeService _svcAT = Telligent.Common.Services.Get<IAudienceTypeService>();
        readonly IDocumentTypeService _svcDT = Telligent.Common.Services.Get<IDocumentTypeService>();

        public void Initialize() { PublicApi.SearchIndexing.Events.BeforeBulkIndex += Events_BeforeBulkIndex; }
        public string Name { get { return "Omnicell Custom Search Plugin"; } }
        public string Description { get { return "Maps all Omnicell custom meta-data to be indexed in the search provider."; } }

        protected void Events_BeforeBulkIndex(BeforeBulkIndexingEventArgs e)
        {
            foreach (var doc in e.Documents.Where(d => d.ContentTypeId == PublicApi.Media.ContentTypeId))
            {
                try
                {
                    var file = new OmnicellMediaPost(doc.ContentId);

                    if (file != null && file.ContentId == doc.ContentId)
                    {

                        string courseInfo = file.GetValue(OmnicellSearchFields.CourseInfo);
                        if (!string.IsNullOrEmpty(courseInfo))
                            doc.AddField(OmnicellSearchFields.CourseInfo, courseInfo);

                        int mfId = file.GetValue<int>(OmnicellSearchFields.MediaFormatId, -1);
                        if (mfId > 0)
                        {
                            var mf = _svcMF.Get(mfId);
                            if (mf != null)
                            {
                                doc.AddField(OmnicellSearchFields.MediaFormatId, mfId.ToString());
                                doc.AddField(OmnicellSearchFields.MediaFormat, mf.Value);
                            }
                        }

                        int ctId = file.GetValue<int>(OmnicellSearchFields.CourseTypeId, -1);
                        if (ctId > 0)
                        {
                            var ct = _svcCT.Get(ctId);
                            if (ct != null)
                            {
                                doc.AddField(OmnicellSearchFields.CourseTypeId, ctId.ToString());
                                doc.AddField(OmnicellSearchFields.CourseType, ct.Value);
                            }
                        }

                        // SMR - Added this check as a hack. TODO fix this to the right kind
                        int dtId =  file.GetValue<int>(OmnicellSearchFields.DocumentTypeId, -1);
                        if (dtId > 0)
                        {
                            var dt = _svcDT.Get(dtId);
                            if (dt != null)
                            {
                                doc.AddField(OmnicellSearchFields.DocumentTypeId, dtId.ToString());
                                doc.AddField(OmnicellSearchFields.DocumentType, dt.Value);
                            }
                        }

                        DateTime bd = file.GetValue<DateTime>(OmnicellSearchFields.CourseBeginDate, DateTime.MinValue);
                        if (bd > DateTime.MinValue)
                            doc.AddField(OmnicellSearchFields.CourseBeginDate, FormatDateTime(bd));

                        DateTime ed = file.GetValue<DateTime>(OmnicellSearchFields.CourseEndDate, DateTime.MinValue);
                        if (ed > DateTime.MinValue)
                            doc.AddField(OmnicellSearchFields.CourseEndDate, FormatDateTime(ed));

                        DateTime pd = file.GetValue<DateTime>(OmnicellSearchFields.PublishDate, DateTime.MinValue);
                        if (pd > DateTime.MinValue)
                            doc.AddField(OmnicellSearchFields.PublishDate, FormatDateTime(pd));

                        Dictionary<int, string> prods = file.GetValueDictionary(OmnicellSearchFields.ProductIdFilter);
                        if (prods != null && prods.Count > 0)
                        {
                            prods.Select(f => f.Key.ToString()).ToList().ForEach(k => doc.AddField(OmnicellSearchFields.ProductIdFilter, k));
                            var prodNames = prods.Select(f => f.Value).ToList();
                            prodNames.ForEach(v => doc.AddField(OmnicellSearchFields.ProductValueFilter, v));
                        }

                        string aTypesValue = file.GetValue(OmnicellSearchFields.AudienceTypeId);
                        var aTypes = new List<AudienceType>().FromDelimitedString<AudienceType>(aTypesValue) as List<AudienceType>;
                        if (aTypes != null && aTypes.Count > 0)
                        {
                            aTypes.Select(t => t.Id.ToString()).ToList().ForEach(a => doc.AddField(OmnicellSearchFields.AudienceTypeId, a));
                            var typeNames = aTypes.Select(t => t.Value).ToList();
                            typeNames.ForEach(n => doc.AddField(OmnicellSearchFields.AudienceType, n));
                        }

                        //// SMR - 081313 - interfaces change to handle search
                        string aInterfacesValues = file.GetValue(OmnicellSearchFields.Interfaces);
                        var aInterfaces = new List<InterfaceType>().FromDelimitedString<InterfaceType>(aInterfacesValues, ",") as List<InterfaceType>;
                        foreach (InterfaceType ift in aInterfaces)
                            doc.AddField(OmnicellSearchFields.Interfaces, ift.Value);
                        // SMR - 081313 - featurelist changed to handle search
                        string afeatureListValues = file.GetValue(OmnicellSearchFields.FeatureList);
                        var aFeatureslists = new List<FeatureListType>().FromDelimitedString<FeatureListType>(afeatureListValues, ",") as List<FeatureListType>;
                        foreach (FeatureListType flt in aFeatureslists)
                            doc.AddField(OmnicellSearchFields.FeatureList, flt.Value);
                        // SMR - 081313 - versions changed to handle search
                        string aVersionsValues = file.GetValue(OmnicellSearchFields.Versions);
                        var aVersionslists = new List<VersionsType>().FromDelimitedString<VersionsType>(aVersionsValues, ",") as List<VersionsType>;
                        foreach (VersionsType vert in aVersionslists)
                            doc.AddField(OmnicellSearchFields.Versions, vert.Value);

                        // END of changes for search by SMR - 081313   

                    }
                }
                catch (Exception ex)
                {
                    throw new CSException(CSExceptionType.SearchIndexingError, string.Format(
                                              "Error while mapping media post '{0}'. Message: {1} Object Type:{2}. See inner exception for more details",
                                              doc.Id, ex.Message, doc.GetType()), ex);
                }
            }

        }

        private string FormatDateTime(DateTime dt)
        {
            return dt.ToString("yyyy-MM-ddThh:mm:ssZ");
        }

       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.Search.Mapping;
using Telligent.Evolution.Components;
using Telligent.Evolution.Components.Search;
using Telligent.Evolution.MediaGalleries.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Version1;

using Omnicell.Custom.Components;
using Omnicell.Data.Model;

namespace Omnicell.Custom.Search
{
    public class OmnicellWikiSearch : IPlugin
    {
        readonly IAudienceTypeService _svcAT = Telligent.Common.Services.Get<IAudienceTypeService>();
        readonly IDocumentTypeService _svcDT = Telligent.Common.Services.Get<IDocumentTypeService>();

        public void Initialize() { PublicApi.SearchIndexing.Events.BeforeBulkIndex += Events_BeforeBulkIndex; }
        public string Name { get { return "Omnicell Wiki Search Plugin"; } }
        public string Description { get { return "Maps all Omnicell custom meta-data to be indexed in the search provider."; } }

        protected void Events_BeforeBulkIndex(BeforeBulkIndexingEventArgs e)
        {
            foreach (var doc in e.Documents.Where(d => d.ContentTypeId == PublicApi.Wikis.ContentTypeId))
            {
                try
                {
                    var wiki = new OmnicellWiki(doc.ContentId);

                    if (wiki != null && wiki.ContentId == doc.ContentId)
                    {
                        var db = new OmnicellEntities();
                        var wikiMeta = db.Omnicell_WikiMetaData2.FirstOrDefault(x => x.WikiId ==  wiki.Id);

                        if (wikiMeta != null)
                        {
                            int docType = Convert.ToInt32(wikiMeta.DocumentType);
                            var dt = _svcDT.Get(docType);

                            if (dt != null)
                            {
                                doc.AddField(OmnicellSearchFields.DocumentTypeId, docType.ToString());
                                doc.AddField(OmnicellSearchFields.DocumentType, dt.Value);
                            }

                            string wmInterfaces = (!String.IsNullOrEmpty(wikiMeta.Interfaces)) ? wikiMeta.Interfaces: String.Empty;
                            var aInterfaces = new List<InterfaceType>().FromDelimitedString(wmInterfaces, ";") as List<InterfaceType>;
                            foreach (InterfaceType ift in aInterfaces)
                                doc.AddField(OmnicellSearchFields.Interfaces, ift.Value);

                            string wmFeatures = (!String.IsNullOrEmpty(wikiMeta.Features))? wikiMeta.Features  : String.Empty;
                            var aFeatureslists = new List<FeatureListType>().FromDelimitedString<FeatureListType>(wmFeatures, ";") as List<FeatureListType>;
                            foreach (FeatureListType flt in aFeatureslists)
                                doc.AddField(OmnicellSearchFields.FeatureList, flt.Value);

                            string wmProductVersion = (!String.IsNullOrEmpty(wikiMeta.ProductVersion)) ? wikiMeta.ProductVersion : String.Empty;
                            var aVersionslists = new List<VersionsType>().FromDelimitedString<VersionsType>(wmProductVersion, ",") as List<VersionsType>;
                            foreach (VersionsType vert in aVersionslists)
                                doc.AddField(OmnicellSearchFields.Versions, vert.Value);

                            string wmAudiences = (!String.IsNullOrEmpty(wikiMeta.Audiences))? wikiMeta.Audiences : string.Empty;
                            if (!string.IsNullOrEmpty(wmAudiences))
                            {

                                string tatAudiences = TranslateAudienceTypes(wmAudiences);

                                var aVTypes = new List<AudienceType>().FromDelimitedString<AudienceType>(tatAudiences, ",") as List<AudienceType>;
                                var aTypes = new List<AudienceType>().FromDelimitedString<AudienceType>(wmAudiences, ",") as List<AudienceType>;
                                if (aTypes != null && aTypes.Count > 0)
                                {
                                    aTypes.Select(t => t.Id.ToString()).ToList().ForEach(a => doc.AddField(OmnicellSearchFields.AudienceTypeId, a));
                                    var typeNames = aVTypes.Select(t => t.Value).ToList();
                                    typeNames.ForEach(n => doc.AddField(OmnicellSearchFields.AudienceType, n));
                                }
                            }

                            // to do add product filter
                            string wmProducts = (!String.IsNullOrEmpty(wikiMeta.Product)) ? wikiMeta.Product : string.Empty;
                            if (!string.IsNullOrEmpty(wmProducts))
                            {
                                string tpgProducts = TranslateProductsGroup(wikiMeta.Product);
                                var pVTypes = new List<ProdType>().FromDelimitedString<ProdType>(tpgProducts, ",") as List<ProdType>;
                                var prods = new List<ProdType>().FromDelimitedString<ProdType>(wmProducts, ",") as List<ProdType>;
                                if (prods != null && prods.Count > 0)
                                {
                                    prods.Select(t => t.Id.ToString()).ToList().ForEach(a => doc.AddField(OmnicellSearchFields.ProductIdFilter, a));

                                    var typeNames = pVTypes.Select(t => t.Value).ToList();
                                    typeNames.ForEach(n => doc.AddField(OmnicellSearchFields.ProductValueFilter, n));
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new CSException(CSExceptionType.SearchIndexingError, string.Format(
                                              "Error while mapping wiki post '{0}'. Message: {1} Object Type:{2}. See inner exception for more details",
                                              doc.Id, ex.Message, doc.GetType()), ex);
                }
            }
        }

        private string TranslateAudienceTypes(string strAudiences)
        {
            string[] lstAudience = strAudiences.Split(',');
            string strReturnValue = "";
            foreach (String audience in lstAudience)
            {
                strReturnValue += ConvertAudienceTypes(audience) + ",";
            }
            return strReturnValue.TrimEnd(',');
        }
        private string ConvertAudienceTypes(string strSharePointAudince)
        {
            int strAud = Convert.ToInt32(strSharePointAudince);
            IAudienceTypeService _svc = Telligent.Common.Services.Get<IAudienceTypeService>();

            IList<AudienceType> auType = _svc.List();
            string audience = string.Empty;

            foreach (AudienceType type in auType)
            {
                if (type.Id == strAud)
                {
                    audience = type.Value.ToString();
                }
            }

            return audience;
        }

        private string TranslateProductsGroup(string strProducts)
        {
            string[] lstProducts = strProducts.Split(',');
            string strReturnValue = "";
            foreach (string product in lstProducts)
            {
                strReturnValue += ConvertProductsGroup(product) + ",";
            }
            return strReturnValue.TrimEnd(',');
        }
        private string ConvertProductsGroup(string strProductGroup)
        {
            int strProd = Convert.ToInt32(strProductGroup);
            List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group> apiGroups =
                new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group>();
            int? groupId = 5;
            apiGroups = PublicApi.Groups.List(new GroupsListOptions() { ParentGroupId = groupId, PageSize = 100 }).ToList();

            string productId = string.Empty;

            foreach (var group in apiGroups)
            {
                if (group.Id == strProd)
                {
                    productId = group.Name.ToString();
                }
            }

            return productId;

        }
    }
}

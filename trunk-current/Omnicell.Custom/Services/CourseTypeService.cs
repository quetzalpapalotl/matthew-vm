﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom.Components;
using Omnicell.Custom.Data;
using Omnicell.Custom.Search;

using OData = Omnicell.Data.Model;

namespace Omnicell.Custom.Components
{
    public interface ICourseTypeService
    {
        CourseType Get(int id);
        CourseType Save(CourseType courseType);
        IList<CourseType> List();
        IList<CourseType> List(int coursePostId);
        void Delete(int id);
    }
    public class CourseTypeService : ICourseTypeService
    {
        private readonly ICacheService _cache = null;

        public CourseTypeService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
        }

        public CourseType Get(int id)
        {
            CourseType ct = GetCourseTypeFromCache(id);
            if (ct == null)
            {
                ct = CourseTypeDataProvider.Instance.Get(id);
                if (ct != null)
                    PushCourseTypeToCache(ct);
            }
            return ct;
        }
        public CourseType Save(CourseType courseType)
        {
            RemoveCourseTypeListFromCache();
            int id = courseType.Id;
            if (id > 0)
            {
                RemoveCourseTypeFromCache(id);
                CourseTypeDataProvider.Instance.Update(courseType);
            }
            else
            {
                id = CourseTypeDataProvider.Instance.Add(courseType);
            }

            // keep the custom db in sync
            using (var db = new OData.OmnicellEntities())
            {
                var dbEntity = db.Omnicell_CourseType.SingleOrDefault(x => x.Id == courseType.Id);
                if (dbEntity == null)
                {
                    dbEntity = new OData.Omnicell_CourseType();
                    db.Omnicell_CourseType.AddObject(dbEntity);
                }

                dbEntity.Value = courseType.Value;
                db.SaveChanges();
            }

            return Get(id);
        }
        public IList<CourseType> List()
        {
            IList<CourseType> list = GetCourseTypeListFromCache();
            if (list == null)
            {
                var newList = CourseTypeDataProvider.Instance.List();
                list = new PagedList<CourseType>(newList);
                if (list.Count > 0)
                    PushCourseTypeListToCache(list);
            }
            return list;
        }
        public IList<CourseType> List(int galleryId)
        {
            IList<CourseType> list = GetCourseTypeListFromCache(galleryId);
            if (list == null)
            {
                Gallery gallery = PublicApi.Galleries.Get(new GalleriesGetOptions { Id = galleryId });
                if (gallery != null)
                {
                    list = gallery.CourseTypeFilters().ToList();

                    if (list != null && list.Count > 0)
                        PushCourseTypeListToCache(galleryId, list);
                }
            }
            return list;
        }
        public void Delete(int id)
        {
            RemoveCourseTypeListFromCache();
            RemoveCourseTypeFromCache(id);

            CourseTypeDataProvider.Instance.Delete(id);

            // keep the custom db in sync
            using (var db = new OData.OmnicellEntities())
            {
                var dbEntity = db.Omnicell_CourseType.SingleOrDefault(x => x.Id == id);
                if (dbEntity != null)
                {
                    db.DeleteObject(dbEntity);
                    db.SaveChanges();
                }
            }
        }

        #region Cache

        private string GetCourseTypeListCacheKey(int galleryId)
        {
            return string.Format("GALLERY::{0}", galleryId);
        }
        private string GetCourseTypeListCacheKey(string key)
        {
            return string.Format("PK_COURSE_TYPE::LIST-{0}", key);
        }
        private string GetCourseTypeCacheKey(int id)
        {
            return string.Format("PK_COURSE_TYPE::{0}", id);
        }

        private void PushCourseTypeListToCache(IList<CourseType> filters)
        {
            PushCourseTypeListToCache("ALL-FORMATS", filters);
        }
        private void PushCourseTypeListToCache(int galleryId, IList<CourseType> List)
        {
            PushCourseTypeListToCache( GetCourseTypeListCacheKey(galleryId), List);
        }
        private void PushCourseTypeListToCache(string key, IList<CourseType> filters)
        {
            _cache.Put(GetCourseTypeListCacheKey(key), filters, CacheScope.All);
        }
        private void PushCourseTypeToCache(CourseType type)
        {
            _cache.Put(GetCourseTypeCacheKey(type.Id), type, CacheScope.All);
        }

        private IList<CourseType> GetCourseTypeListFromCache()
        {
            return GetCourseTypeListFromCache("ALL-FORMATS");
        }
        private IList<CourseType> GetCourseTypeListFromCache(int galleryId)
        {
            return GetCourseTypeListFromCache(GetCourseTypeListCacheKey(galleryId));
        }
        private IList<CourseType> GetCourseTypeListFromCache(string key)
        {
            return _cache.Get(GetCourseTypeListCacheKey(key), CacheScope.All) as IList<CourseType>;
        }
        private CourseType GetCourseTypeFromCache(int id)
        {
            return _cache.Get(GetCourseTypeCacheKey(id), CacheScope.All) as CourseType;
        }

        private void RemoveCourseTypeListFromCache()
        {
            RemoveCourseTypeListFromCache("ALL-FORMATS");
        }
        private void RemoveCourseTypeListFromCache(int galleryId)
        {
            RemoveCourseTypeListFromCache(GetCourseTypeListCacheKey(galleryId));
        }
        private void RemoveCourseTypeListFromCache(string key)
        {
            _cache.Remove(GetCourseTypeListCacheKey(key), CacheScope.All);
        }
        private void RemoveCourseTypeFromCache(int id)
        {
            _cache.Remove(GetCourseTypeCacheKey(id), CacheScope.All);
        }

        #endregion Cache
    }
}

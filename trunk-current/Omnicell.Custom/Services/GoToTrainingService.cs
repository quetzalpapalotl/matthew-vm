﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using TComp = Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;

using GTT = GoToTraining.Api.Components;
using Newtonsoft.Json;

using System.Threading.Tasks;
using System.Collections.Specialized;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;

namespace Omnicell.Custom.Components
{
    public interface IGoToTrainingService
    {
        GTT.Training GetTrainingPreRequisite();
        GTT.Training GetTraining(string trainingKey);
        List<GTT.Session> GetSessions(DateTime startDate, DateTime endDate);
        List<GTT.Session> GetSessions(string trainingKey);
        List<GTT.Attendee> GetAttendees(string trainingKey, DateTime startDate);
        List<GTT.Attendee> GetAttendees(string trainingKey, DateTime startDate, DateTime endDate);
        List<GTT.Attendee> GetAttendees(DateTime startDate);
        List<GTT.Attendee> GetAttendees(DateTime startDate, DateTime endDate);
        List<GTT.Attendee> GetAttendees(DateTime startDate, string titleFilter);
        List<GTT.Attendee> GetAttendees(DateTime startDate, DateTime endDate, string titleFilter);
        List<GTT.Registrant> GetRegistrants(string trainingKey);
    }
    public class GoToTrainingService : IGoToTrainingService
    {
        private readonly TComp.ICacheService _cache = null;
        private readonly GoToTrainingPlugin _gotoTraining = null;
        private readonly GoToTraining.Api.Context _gttContext = null;

        private string _accessToken { get; set; }
        private const string URL_BASE = "htt" + "ps://api.citrixonline.com/G2T/rest/";
        private const string URL_GET_TRAINING = URL_BASE + "organizers/{0}/trainings/{1}";
        private const string URL_GET_TRAININGS = URL_BASE + "organizers/{0}/trainings";
        private const string URL_GET_SESSIONS = URL_BASE + "reports/organizers/{0}/sessions";
        private const string URL_GET_SESSIONS_TRAINING = URL_BASE + "reports/organizers/{0}/trainings/{1}";
        private const string URL_GET_ATTENDEES = URL_BASE + "reports/organizers/{0}/sessions/{1}/attendees";
        private const string URL_GET_REGISTRANTS = URL_BASE + "organizers/{0}/trainings/{1}/registrants";
        private const string URL_GET_ORGANIZERS = URL_BASE + "accounts/{0}/organizers";

        public GoToTrainingService()
        {
            _cache = Telligent.Common.Services.Get<TComp.ICacheService>();
            _gotoTraining = Telligent.Evolution.Extensibility.Version1.PluginManager.Get<GoToTrainingPlugin>().FirstOrDefault();
            _gttContext = GoToTraining.Api.Context.Instance(_gotoTraining.AccessToken);
        }

        public List<GTT.Organizer> GetOrganizers()
        {
            List<GTT.Organizer> organizerList = GetOrganizerListFromCache();
            if (organizerList == null)
            {
                try
                {
                    organizerList = _gttContext.GetOrganizers(_gotoTraining.AccountToken);

                    if (organizerList != null && organizerList.Count > 0)
                        PutOrganizerListInCache(organizerList);
                }
                catch (Exception ex)
                {
                    organizerList = new List<GTT.Organizer>();
                    new TComp.CSException(TComp.CSExceptionType.UnknownError, string.Format("Error requesting organizers: AccountToken - {0}", _gotoTraining.AccountToken), ex).Log();
                }
            }
            return organizerList;
        }
        public GTT.Training GetTrainingPreRequisite()
        {
            return GetTraining(_gotoTraining.PreRequisiteKey);
        }
        public GTT.Training GetTraining(string trainingKey)
        {
            GTT.Training training = GetTrainingFromCache(trainingKey);
            if (training == null)
            {
                try
                {
                    training = _gttContext.GetTraining(_gotoTraining.OrganizerKey, trainingKey);

                    if (training != null)
                        PutTrainingInCache(training);
                }
                catch (Exception ex)
                {
                    training = new GTT.Training();
                    new TComp.CSException(TComp.CSExceptionType.UnknownError, string.Format("Error requesting training: {0}", trainingKey), ex).Log();
                }
            }
            return training;
        }
        public List<GTT.Training> GetTrainings()
        {
            return GetTrainings(_gotoTraining.OrganizerKey);
        }
        public List<GTT.Training> GetTrainings(string organizerKey)
        {
            List<GTT.Training> trainings = GetTrainingListFromCache(organizerKey);
            if (trainings == null)
            {
                try
                {
                    trainings = _gttContext.GetTrainings(organizerKey);
                    if (trainings != null && trainings.Count > 0)
                        PutTrainingListInCache(organizerKey, trainings);
                }
                catch (Exception ex)
                {
                    trainings = new List<GTT.Training>();
                    new TComp.CSException(TComp.CSExceptionType.UnknownError, string.Format("Error requesting trainings: Organizer Key - {0}", organizerKey), ex).Log();
                }
            }
            return trainings;
        }
        public List<GTT.Session> GetSessions(DateTime startDate, DateTime endDate)
        {
            List<GTT.Session> sessions = GetSessionListFromCache(startDate, endDate);
            if (sessions == null)
            {
                try
                {
                    sessions = _gttContext.GetSessions(_gotoTraining.OrganizerKey, startDate, endDate);

                    if (sessions != null && sessions.Count > 0)
                        PutSessionListInCache(startDate, endDate, sessions);
                }
                catch (Exception ex)
                {
                    sessions = new List<GTT.Session>();
                    new TComp.CSException(TComp.CSExceptionType.UnknownError, string.Format("Error requesting sessions: Start - {0} - End - {1}", startDate, endDate), ex).Log();
                }
            }
            return sessions;
        }
        public List<GTT.Session> GetSessions(string trainingKey)
        {
            List<GTT.Session> sessions = GetSessionListFromCache(trainingKey);
            if (sessions == null)
            {
                try
                {
                    sessions = _gttContext.GetSessions(_gotoTraining.OrganizerKey, trainingKey);

                    if (sessions != null && sessions.Count > 0)
                        PutSessionListInCache(trainingKey, sessions);
                }
                catch (Exception ex)
                {
                    sessions = new List<GTT.Session>();
                    new TComp.CSException(TComp.CSExceptionType.UnknownError, string.Format("Error requesting sessions: Training Key - {0}", trainingKey), ex).Log();
                }
            }
            return sessions;
        }
        public List<GTT.Attendee> GetAttendees(string trainingKey, DateTime startDate)
        {
            return GetAttendees(trainingKey, startDate, DateTime.MinValue);
        }
        public List<GTT.Attendee> GetAttendees(string trainingKey, DateTime startDate, DateTime endDate)
        {
            List<GTT.Attendee> attendees = GetAttendeeListFromCache(trainingKey, startDate, endDate);
            if (attendees == null)
            {
                attendees = _gttContext.GetAttendees(_gotoTraining.OrganizerKey, trainingKey, startDate, (endDate > DateTime.MinValue ? endDate : (DateTime?)null));
                if (attendees != null && attendees.Count > 0)
                    PutAttendeeListInCache(trainingKey, startDate, endDate, attendees);
            }
            return attendees;
        }
        public List<GTT.Attendee> GetAttendees(DateTime startDate)
        {
            return GetAttendees(startDate, DateTime.MinValue, string.Empty);
        }
        public List<GTT.Attendee> GetAttendees(DateTime startDate, DateTime endDate)
        {
            return GetAttendees(startDate, endDate, string.Empty);
        }
        public List<GTT.Attendee> GetAttendees(DateTime startDate, string titleFilter)
        {
            return GetAttendees(startDate, DateTime.MinValue, titleFilter);
        }
        public List<GTT.Attendee> GetAttendees(DateTime startDate, DateTime endDate, string titleFilter)
        {
            List<GTT.Attendee> attendees = GetAttendeeListFromCache(startDate, endDate, titleFilter);
            if (attendees == null)
            {
                attendees = _gttContext.GetAttendees(_gotoTraining.OrganizerKey, startDate, (endDate > DateTime.MinValue ? endDate : (DateTime?)null), titleFilter);
                if (attendees != null && attendees.Count > 0)
                    PutAttendeeListInCache(startDate, endDate, titleFilter, attendees);
            }
            return attendees;
        }
        public List<GTT.Registrant> GetRegistrants(string trainingKey)
        {
            List<GTT.Registrant> registrants = GetRegistrantListFromCache(trainingKey);
            if (registrants == null)
            {
                try
                {
                    registrants = _gttContext.GetRegistrants(_gotoTraining.OrganizerKey, trainingKey);

                    if (registrants != null && registrants.Count > 0)
                        PutRegistrantListInCache(trainingKey, registrants);
                }
                catch (Exception ex)
                {
                    registrants = new List<GTT.Registrant>();
                    new TComp.CSException(TComp.CSExceptionType.UnknownError, string.Format("Error requesting registrants: Training Key - {0}", trainingKey), ex).Log();
                }
            }
            return registrants;
        }

        public bool UserHasTakenTrainingPreRequisite()
        {
            return UserHasTakenTrainingPreRequisite(PublicApi.Users.AccessingUser);
        }
        public bool UserHasTakenTrainingPreRequisite(int userId)
        {
            V1Entities.User user = PublicApi.Users.Get(new UsersGetOptions { Id = userId });
            return UserHasTakenTrainingPreRequisite(user);
        }
        public bool UserHasTakenTrainingPreRequisite(V1Entities.User user)
        {
            bool hasPreReq = false;
            if (PublicApi.UserProfileFields.Get(_gotoTraining.PreReqProfileFieldName) != null)
            {
                bool.TryParse(user.ProfileFields.Get(_gotoTraining.PreReqProfileFieldName).Value, out hasPreReq);
            }
            return hasPreReq;
        }

        public DateTime UpdateTrainingPreReq(DateTime startTime)
        {
            ITrainingAttendeeService svcAttendee = Telligent.Common.Services.Get<ITrainingAttendeeService>();
            IGoToTrainingService svcGTT = Telligent.Common.Services.Get<IGoToTrainingService>();
            GoToTrainingPlugin gtt = Telligent.Evolution.Extensibility.Version1.PluginManager.Get<GoToTrainingPlugin>().FirstOrDefault();
            OmnicellTrainingGalleryPlugin otg = Telligent.Evolution.Extensibility.Version1.PluginManager.Get<OmnicellTrainingGalleryPlugin>().FirstOrDefault();
           Telligent.Evolution.Extensibility.Api.Entities.Version1.Group trainingGroup = PublicApi.Groups.Get(new GroupsGetOptions { Id = otg.TrainingGroupId });
           TComp.User admUser = Telligent.Evolution.Users.GetUser("admin");
           string message;

           bool tester = false;


           if (gtt != null && otg != null && !string.IsNullOrEmpty(gtt.PreReqProfileFieldName) && otg.TrainingGroupId > 0)
           {
               try
               {
                  
                   DateTime lastDate = startTime;

                   List<GTT.Attendee> attendees = svcGTT.GetAttendees(lastDate, gtt.PreRequisiteKey);
                   message = string.Format("Attendee list count: {0}", attendees.Count());
                   LogEntry(message);

                   int completionSecs = gtt.PreReqCompletionMinutes * 60;

                   foreach (var attendee in attendees)
                   {
                       if (attendee.TimeInSession >= completionSecs || gtt.PreReqCompletionMinutes <= 0)
                       {
                           User user = PublicApi.Users.Get(new UsersGetOptions { Email = attendee.Email.Trim() });

                           if (user != null)
                           {

                               ProfileField pf = new ProfileField { Label = gtt.PreReqProfileFieldName, Value = true.ToString() };
                               RunAsUser(() => PublicApi.Users.Update(new UsersUpdateOptions { Id = user.Id, ProfileFields = new List<ProfileField> { pf } }), new TComp.ContextService().GetExecutionContext(), admUser);
                               message = string.Format("RunAsUser completed for: {0}", user.Username);
                               LogEntry(message);
                           }
                           else
                           {
                               svcAttendee.Save(new TrainingAttendee
                               {
                                   EmailAddress = attendee.Email,
                                   GivenName = attendee.GivenName,
                                   Surname = attendee.Surname,
                                   TimeInSession = attendee.TimeInSession,
                                   SessionKey = attendee.SessionKey,
                                   SessionStart = attendee.SessionStartTime.ToString(),
                                   SessionEnd = attendee.SessionEndTime.ToString(),
                                   TrainingKey = attendee.TrainingKey,
                                   TrainingName = attendee.TrainingName
                               });
                           }
                       }
                   }


                   ExtendedAttribute ea = new ExtendedAttribute { Key = Constants.GALLERY_LAST_ATTENDEE_REQ_DATE, Value = DateTime.UtcNow.ToString() };
                   message = string.Format("Extended Attribute Value: {0}", ea.Value);
                   LogEntry(message);
                   Group uGroup = PublicApi.Groups.Update(trainingGroup.Id.Value, new GroupsUpdateOptions { ExtendedAttributes = new List<ExtendedAttribute> { ea } });
                   message = string.Format("Last update time: {0}", uGroup.LastAttendeeRequestDate());
                   LogEntry(message);
               }

               catch (Exception ex)
               {
                   new TComp.CSException(TComp.CSExceptionType.UnknownError, "Error Processing GoToTraining. ", ex).Log();
               }
           }
           else
           {
               TComp.EventLogEntry pluginEntry = new TComp.EventLogEntry();
               pluginEntry.Category = "Plugins";
               pluginEntry.EventType = TComp.EventType.Warning;
               pluginEntry.Message = "CANNOT PROCESS ATTENDEES BECAUSE PLUGINS ARE NOT ENABLED AND CONFIGURED.";
               pluginEntry.EventDate = DateTime.Now;
               pluginEntry.EventID = 200;
               pluginEntry.SettingsID = 1000;
               pluginEntry.MachineName = Dns.GetHostName();
               TComp.EventLogs.Write(pluginEntry);
           }

           return DateTime.Now;
        }

        public List<GTT.Session> GetSessions(string organizerKey, DateTime startDate, DateTime endDate)
        {
            string url = string.Format(URL_GET_SESSIONS, organizerKey);
            string data = string.Format("{{\"startDate\":\"{0}\", \"endDate\":\"{1}\"}}", _gttContext.FormatAPIDateTime(startDate), _gttContext.FormatAPIDateTime(endDate));
            string response = _gttContext.GetResponseString(url, data);
            var sessions = JsonConvert.DeserializeObject<List<GTT.Session>>(response);
            return sessions;
        }

        public static void LogEntry(string message)
        {
            TComp.EventLogEntry pluginEntry = new TComp.EventLogEntry();
            pluginEntry.Category = "Plugins";
            pluginEntry.EventType = TComp.EventType.Information;
            pluginEntry.Message = message;
            pluginEntry.EventDate = DateTime.Now;
            pluginEntry.EventID = 700;
            pluginEntry.SettingsID = -1;
            pluginEntry.MachineName = Dns.GetHostName();
            TComp.EventLogs.Write(pluginEntry);
        }


        private static object _runAsUserLock = new object();
        public void RunAsUser(Action a, TComp.IExecutionContext context, TComp.User u)
        {
            lock (_runAsUserLock)
            {
                var originalUser = context.User;
                try
                {
                    context.User = u;
                    a();
                }
                finally
                {
                    context.User = originalUser;
                }
            }
        }

        #region Caching

        #region Training

        private string GetTrainingCacheKey(string trainingKey)
        {
            return string.Format("TRAINING-KEY:{0}", trainingKey);
        }
        private GTT.Training GetTrainingFromCache(string trainingKey)
        {
            return _cache.Get(GetTrainingCacheKey(trainingKey), CacheScope.All) as GTT.Training;
        }
        private void PutTrainingInCache(GTT.Training training)
        {
            _cache.Put(GetTrainingCacheKey(training.TrainingKey), training, CacheScope.All);
        }
        private void RemoveTrainingFromCache(string trainingKey)
        {
            _cache.Remove(GetTrainingCacheKey(trainingKey), CacheScope.All);
        }

        private string GetTrainingListCacheKey(string organizerKey)
        {
            return string.Format("TRAINING-LIST-KEY:ORGANIZER:{0}", organizerKey);
        }
        private List<GTT.Training> GetTrainingListFromCache(string organizerKey)
        {
            return _cache.Get(GetTrainingListCacheKey(organizerKey), CacheScope.All) as List<GTT.Training>;
        }
        private void PutTrainingListInCache(string organizerKey, List<GTT.Training> list)
        {
            _cache.Put(GetTrainingListCacheKey(organizerKey), list, CacheScope.All);
        }
        private void RemoveTrainingListFromCache(string organizerKey)
        {
            _cache.Remove(GetTrainingListCacheKey(organizerKey), CacheScope.All);
        }

        #endregion Training

        #region Sessions

        private string GetSessionListCacheKey(string key)
        {
            return string.Format("SESSIONLIST-{0}", key);
        }
        private string GetSessionListCacheKey(DateTime startDate, DateTime endDate)
        {
            string key = string.Format("FROM::{0}--TO::{1}", startDate.ToString("yyyy-MM-ddThh:mm:ssZ"), endDate.ToString("yyyy-MM-ddThh:mm:ssZ"));
            return GetSessionListCacheKey(key);
        }
        private List<GTT.Session> GetSessionListFromCache(DateTime startDate, DateTime endDate)
        {
            return GetSessionListFromCache(GetSessionListCacheKey(startDate, endDate));
        }
        private List<GTT.Session> GetSessionListFromCache(string key)
        {
            return _cache.Get(key, CacheScope.All) as List<GTT.Session>;
        }
        private void PutSessionListInCache(DateTime startDate, DateTime endDate, List<GTT.Session> sessions)
        {
            PutSessionListInCache(GetSessionListCacheKey(startDate, endDate), sessions);
        }
        private void PutSessionListInCache(string key, List<GTT.Session> sessions)
        {
            _cache.Put(GetSessionListCacheKey(key), sessions, CacheScope.All);
        }
        private void RemoveSessionListFromCache(DateTime startDate, DateTime endDate)
        {
            RemoveSessionListFromCache(GetSessionListCacheKey(startDate, endDate));
        }
        private void RemoveSessionListFromCache(string key)
        {
            _cache.Remove(key, CacheScope.All);
        }

        #endregion Sessions

        #region Attendees

        private string GetAttendeeListCacheKey(DateTime startDate, DateTime endDate, string titleFilter)
        {
            return GetAttendeeListCacheKey(string.Format("STARTDATE:{0}-ENDDATE:{1}-TITLEFILTER:{2}", startDate, endDate, titleFilter));
        }
        private string GetAttendeeListCacheKey(string trainingKey, DateTime startDate, DateTime endDate)
        {
            return GetAttendeeListCacheKey(string.Format("TRAININGKEY:{0}-STARTDATE:{1}-ENDDATE:{2}", trainingKey, startDate, endDate));
        }
        private string GetAttendeeListCacheKey(string key)
        {
            return string.Format("ATTENDEELIST-{0}", key);
        }

        private List<GTT.Attendee> GetAttendeeListFromCache(DateTime startDate, DateTime endDate, string titleFilter)
        {
            return GetAttendeeListFromCache(GetAttendeeListCacheKey(startDate, endDate, titleFilter));
        }
        private List<GTT.Attendee> GetAttendeeListFromCache(string trainingKey, DateTime startDate, DateTime endDate)
        {
            return GetAttendeeListFromCache(GetAttendeeListCacheKey(trainingKey, startDate, endDate));
        }
        private List<GTT.Attendee> GetAttendeeListFromCache(string key)
        {
            return _cache.Get(GetAttendeeListCacheKey(key), CacheScope.All) as List<GTT.Attendee>;
        }

        private void PutAttendeeListInCache(DateTime startDate, DateTime endDate, string titleFilter, List<GTT.Attendee> attendees)
        {
            PutAttendeeListInCache(GetAttendeeListCacheKey(startDate, endDate, titleFilter), attendees);
        }
        private void PutAttendeeListInCache(string trainingKey, DateTime startDate, DateTime endDate, List<GTT.Attendee> attendees)
        {
            PutAttendeeListInCache(GetAttendeeListCacheKey(trainingKey, startDate, endDate), attendees);
        }
        private void PutAttendeeListInCache(string key, List<GTT.Attendee> attendees)
        {
            _cache.Put(GetAttendeeListCacheKey(key), attendees, CacheScope.All);
        }

        private void RemoveAttendeeListFromCache(DateTime startDate, DateTime endDate, string titleFilter)
        {
            RemoveAttendeeListFromCache(GetAttendeeListCacheKey(startDate, endDate, titleFilter));
        }
        private void RemoveAttendeeListFromCache(string trainingKey, DateTime startDate, DateTime endDate)
        {
            RemoveAttendeeListFromCache(GetAttendeeListCacheKey(trainingKey, startDate, endDate));
        }
        private void RemoveAttendeeListFromCache(string key)
        {
            _cache.Remove(GetAttendeeListCacheKey(key), CacheScope.All);
        }

        #endregion Attendees

        #region Registrants

        private string GetRegistrantListCacheKey(string key)
        {
            return string.Format("ATTENDEELIST-{0}", key);
        }
        private List<GTT.Registrant> GetRegistrantListFromCache(string key)
        {
            return _cache.Get(GetRegistrantListCacheKey(key), CacheScope.All) as List<GTT.Registrant>;
        }
        private void PutRegistrantListInCache(string key, List<GTT.Registrant> attendees)
        {
            _cache.Put(GetRegistrantListCacheKey(key), attendees, CacheScope.All);
        }
        private void RemoveRegistrantListFromCache(string key)
        {
            _cache.Remove(GetRegistrantListCacheKey(key), CacheScope.All);
        }

        #endregion Registrants

        #region Organizers

        private string GetOrganizerListCacheKey(string key)
        {
            return string.Format("ORGANIZER-LIST:{0}", key);
        }
        private List<GTT.Organizer> GetOrganizerListFromCache()
        {
            return GetOrganizerListFromCache("ALL-ACCOUNT");
        }
        private List<GTT.Organizer> GetOrganizerListFromCache(string key)
        {
            return _cache.Get(GetOrganizerListCacheKey(key), CacheScope.All) as List<GTT.Organizer>;
        }
        private void PutOrganizerListInCache(List<GTT.Organizer> organizerList)
        {
            PutOrganizerListInCache("ALL-ACCOUNT", organizerList);
        }
        private void PutOrganizerListInCache(string key, List<GTT.Organizer> organizerList)
        {
            _cache.Put(GetOrganizerListCacheKey(key), organizerList, CacheScope.All);
        }
        private void RemoveOrganizerListFromCache()
        {
            RemoveOrganizerListFromCache("ALL-ACCOUNT");
        }
        private void RemoveOrganizerListFromCache(string key)
        {
            _cache.Remove(GetOrganizerListCacheKey(key), CacheScope.All);
        }

        #endregion Organizers

        #endregion Caching
    }
}

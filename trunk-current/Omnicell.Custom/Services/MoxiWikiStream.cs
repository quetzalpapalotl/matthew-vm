﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.SharePoint.Client;
using Omnicell.Data.Model;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using SPApi = Telligent.Evolution.Extensions.SharePoint.Client.Api.Version1;
using SPClient = Telligent.Evolution.Extensions.SharePoint.Client;
using SPExtv1 = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;
using TEntities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TExV1 = Telligent.Evolution.Extensibility.Version1;

namespace Omnicell.Custom.Components
{
    public interface IMoxiWikiStream
    {
        SPExtv1.PagedList<SPApi.Document> GetSharepointLibrary(string title);
        DateTime GetWebCreatedDate();
        SPExtv1.PagedList<SPApi.Library> GetLibraryList(int groupID);
    }

    public class MoxiWikiStream : IMoxiWikiStream
    {
        private readonly ICacheService _cache = null;
        private readonly OmnicellSharepointPlugin _osp = null;
        private OmnicellSharepointPlugin _plgnSP = null;
        private List<string> documentIds = new List<string>();
        private List<OmnicellWikiPage> interLinks = new List<OmnicellWikiPage>();

        public MoxiWikiStream()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
            _osp = Telligent.Evolution.Extensibility.Version1.PluginManager.Get<OmnicellSharepointPlugin>().FirstOrDefault();
            _plgnSP = TExV1.PluginManager.Get<OmnicellSharepointPlugin>().FirstOrDefault();
        }

        public SPExtv1.PagedList<SPApi.Library> GetLibraryList(int groupID)
        {
            SPApi.LibraryListOptions options = new SPApi.LibraryListOptions();
            options.PageIndex = 0;
            options.PageSize = 5;
            SPExtv1.PagedList<SPApi.Library> lstLibraries = SPApi.PublicApi.Libraries.List(groupID, options);

            return lstLibraries;
        }
        public SPExtv1.PagedList<SPApi.Document> GetSharepointLibrary(string guidID)
        {
            // connect to Sharepoint and get a list of the libraries
            SPClient.version2.SharePointLibrary spLib2 = new SPClient.version2.SharePointLibrary();
            SPExtv1.PagedList<SPApi.Document> lstDocs = new SPExtv1.PagedList<SPApi.Document>();

            SPApi.Library splib = new SPApi.Library();
            try
            {
                // convert the library ID to GUID
                Guid LibIDGuid = new Guid(guidID);
                splib = spLib2.Get(LibIDGuid);
                Guid AppGuidID = new Guid(splib.ApplicationId.ToString());
                // this library has a groupID, get all the files associated with the group ID
                int igroupID = splib.GroupId;
                SPApi.DocumentListOptions options = new SPApi.DocumentListOptions();
                options.FolderPath = splib.Root;  // /Repository/Publications /?
                options.PageSize = 5000;
                lstDocs = SPApi.PublicApi.Documents.List(LibIDGuid, options);
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error converting library id to guid: {0}", guidID), ex).Log();
            }

            return lstDocs;
        }
        public DateTime GetWebCreatedDate()
        {
            using (ClientContext context = new ClientContext(_osp.SPFullUrl))
            {
                Web web = context.Web;
                context.Load(web, w => w.Created);
                context.ExecuteQuery();
                return web.Created;
            }
        }

        public ZipInputStream GetRemoteZipStream(string remoteUrl, string domain, string userName, string passWord)
        {
            ZipInputStream zistream;
            WebResponse response = null;
            WebRequest request = WebRequest.Create(remoteUrl);
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(userName + ":" + passWord));

            request.PreAuthenticate = true;
            request.Credentials = new NetworkCredential(userName, passWord, domain);

            if (request != null)
            {
                try
                {
                    response = request.GetResponse();
                    if (response != null)
                    {
                        zistream = new ZipInputStream(response.GetResponseStream());
                        return zistream;
                    }
                }
                catch (Exception)
                {
                    return null;
                }
            }
            return null;

        }


        public SharepointDITA.DitaDocMetaData ExtractMetaDataFromTOCFile(HtmlDocument doc)
        {
            SharepointDITA.DitaDocMetaData ddmd = new SharepointDITA.DitaDocMetaData();
            try
            {
                // Html Agility Code


                IEnumerable<HtmlNode> collection = doc.DocumentNode.Descendants("meta").Where(a => a.Attributes.Contains("name"));
                HtmlNode title = doc.DocumentNode.SelectSingleNode("//title");
                ddmd.CoreProperties.Title = title.InnerHtml.Replace('\uFF06', '&').Replace('\uFF09', ')').Replace('\uFF08', '(').Replace("&amp;", "&").Replace("&#39;", @"'"); 

                foreach (var meta in collection)
                {
                    string nodeName = meta.Attributes["name"].Value;
                    string value = meta.Attributes["content"].Value.Replace('\uFF06', '&').Replace('\uFF09', ')').Replace('\uFF08', '(').Replace("&amp;", "&");
                    //value = ConvertUTF16(value);
                    switch (nodeName)
                    {

                        case "Id": ddmd.CustomProperties.Id = meta.Attributes["content"].Value; break;
                        case "Product": ddmd.CustomProperties.Product = meta.Attributes["content"].Value; break;
                        case "productversion": ddmd.CustomProperties.ProductVersion = meta.Attributes["content"].Value; break;
                        case "Audience": ddmd.CustomProperties.Audience = value; break;
                        case "Interfaces": ddmd.CustomProperties.Interfaces = value; break;
                        case "OmniFeatures": ddmd.CustomProperties.Features = value; break;
                        case "OmniDocumentType": ddmd.CustomProperties.DocumentType = meta.Attributes["content"].Value; break;
                        case "ShortDescription": ddmd.CoreProperties.Description = value; break;
                        case "targetgroup": ddmd.CustomProperties.TargetGroup = meta.Attributes["content"].Value; break;
                        case "DxRev": ddmd.CustomProperties.RevNum = meta.Attributes["content"].Value; break;
                        case "PartNumber": ddmd.CustomProperties.PartName = meta.Attributes["content"].Value; break;
                    }

                }

            }
            catch (Exception ex)
            {
                throw ex;

            }


            return ddmd;
        } // end of method

        public void HandleCreateUpdateWiki(SPApi.Document doc)
        {

            string strDocName = "";
            strDocName = GetFileNameFromUrl(doc.Path);
            string docName = doc.Name.Replace(" - DITA Map XHTML.zip", "").ToLower();
            if (!String.IsNullOrEmpty(strDocName) && strDocName.Contains(" - DITA Map XHTML.zip"))
            {
                try
                {
                    SPApi.Document spZIPdoc = new SPApi.Document();

                    SPExtv1.PagedList<SPApi.Document> spdoclist = new SPExtv1.PagedList<SPApi.Document>();
                    spdoclist = GetSharepointLibrary(doc.Library.Id.ToString());

                    foreach (SPApi.Document document in spdoclist)
                    {
                        if (document.Name.ToLower() == strDocName.ToLower())
                        {
                            spZIPdoc = document;
                            break;
                        }
                    }
                    if (spZIPdoc != null)
                    {

                        if (spZIPdoc.Path != "" && doc.Path != "")
                        {

                            string directory = Environment.CurrentDirectory + Path.DirectorySeparatorChar + "WikiTemp";
                            Telligent.Evolution.Components.User admUser = Telligent.Evolution.Users.GetUser("admin");
                            RunAsUser(() => CreateOrUpdateWiki(spZIPdoc, directory, docName, spdoclist, doc), new Telligent.Evolution.Components.ContextService().GetExecutionContext(), admUser);

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

        }

        public void CreateNewWiki(int groupId, string key, Guid contentId, Dictionary<string, MemoryStream> files, out int wikiId)
        {

           wikiId = 0;

            try
            {
                
                // Html Agility Code
                HtmlDocument doc = new HtmlDocument();
                files["toc.html"].Position = 0;
                doc.Load(files["toc.html"], Encoding.UTF8);
                SharepointDITA.DitaDocMetaData smdt = ExtractMetaDataFromTOCFile(doc);


                HtmlNode title = doc.DocumentNode.SelectSingleNode("//title");
                string wikiName = title.InnerHtml;
                wikiName = wikiName.Replace("&amp;", "&");

                WikisCreateOptions options = new WikisCreateOptions();
                options.Key = key;
                options.Description = smdt.CoreProperties.Description;

                var newWiki = PublicApi.Wikis.Create(groupId, wikiName, options);

                if (newWiki != null)
                {
                    OmnicellWiki oWiki = new OmnicellWiki(newWiki);
                    // process wiki metadata
                    OmnicellWikiMetadata wikiMetadata = PopulateMetadata(smdt);
                    wikiMetadata.GroupId = groupId;
                    wikiMetadata.WikiId = (int)oWiki.Id;
                    wikiMetadata.DocumentId = contentId;
                    oWiki.wikiMeta = wikiMetadata;
                    WikiPageExtensions.SaveMetaData(wikiMetadata);

                    string defaultPageKey = "DefaultWikiPage";
                    var parentWikiPage = PublicApi.WikiPages.Get(new TApi.WikiPagesGetOptions { WikiId = newWiki.Id, PageKey = defaultPageKey });

                    if (parentWikiPage != null)
                    {

                        WikiPagesUpdateOptions up = new WikiPagesUpdateOptions();
                        string defaultTags = "DocGUID:" + contentId.ToString();
                        up.Tags = defaultTags;
                        up.Body = smdt.CoreProperties.Description;
                        int newWikiId = (int)parentWikiPage.Id;
                        var wikiUpdate = PublicApi.WikiPages.Update(newWikiId, up);

                        OmnicellWikiPage dPage = new OmnicellWikiPage(wikiUpdate);



                        OmnicellWikiPageMetadata pageMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);
                        pageMeta.contentId = defaultTags;
                        pageMeta.WikiPageId = (int)wikiUpdate.Id;
                        pageMeta.ParentPageId = 0;
                        pageMeta.OrderNumber = 0;
                        pageMeta.PageKey = dPage.PageKey;
                        pageMeta.ShortDesc = smdt.CoreProperties.Description;
                        pageMeta.FileName = string.Empty;

                        WikiPageExtensions.SavePageMetaData(pageMeta);
                        dPage.metaData = pageMeta;

                        oWiki.wikiPages.Add(dPage);

                        documentIds.Add(defaultTags);
                        wikiId = Convert.ToInt32(newWiki.Id);

                        CreateWikiPages(oWiki, dPage, files);
                        
                    }

                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error creating wiki with Key: {0}, and contentId: {1}", key, contentId), ex).Log();

            }

        } // end of method

        public void EditWiki(int groupId, int wikiId, string key, Guid contentId, SharepointDITA.DitaDocMetaData smdt, Dictionary<string, MemoryStream> files)
        {

            try
            {

                // Html Agility Code
                HtmlDocument doc = new HtmlDocument();
                files["toc.html"].Position = 0;
                doc.Load(files["toc.html"], Encoding.UTF8);
               

                HtmlNode title = doc.DocumentNode.SelectSingleNode("//title");
                string wikiName = title.InnerHtml;
                WikisUpdateOptions options = new WikisUpdateOptions();
                options.Name = title.InnerHtml;
                options.Key = key;
                options.GroupId = groupId;
                options.Description = smdt.CoreProperties.Description;
                var newWiki = PublicApi.Wikis.Update(wikiId, options);

                if (newWiki != null)
                {

                    OmnicellWiki oWiki = new OmnicellWiki(newWiki);
                    //process wiki metadata
                    OmnicellWikiMetadata wikiMetadata = PopulateMetadata(smdt);
                    wikiMetadata.GroupId = groupId;
                    wikiMetadata.WikiId = (int)oWiki.Id;
                    wikiMetadata.DocumentId = contentId;
                    oWiki.wikiMeta = wikiMetadata;
                    WikiPageExtensions.SaveMetaData(wikiMetadata);

                    string defaultPageKey = "DefaultWikiPage";
                    var parentWikiPage = PublicApi.WikiPages.Get(new TApi.WikiPagesGetOptions { WikiId = newWiki.Id, PageKey = defaultPageKey });

                    if (parentWikiPage != null)
                    {
                        WikiPagesUpdateOptions up = new WikiPagesUpdateOptions();
                        int newWikiId = (int)parentWikiPage.Id;
                        string defaultTags = "DocGUID:" + contentId.ToString();
                        documentIds.Add(contentId.ToString());
                        up.Tags = defaultTags;
                        up.Body = smdt.CoreProperties.Description;
                        var wikiUpdate = PublicApi.WikiPages.Update(newWikiId, up);

                        OmnicellWikiPage dPage = new OmnicellWikiPage(wikiUpdate);



                        OmnicellWikiPageMetadata pageMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);
                        pageMeta.contentId = defaultTags;
                        pageMeta.WikiPageId = (int)dPage.Id;
                        pageMeta.ParentPageId = 0;
                        pageMeta.OrderNumber = 0;
                        pageMeta.PageKey = dPage.PageKey;
                        pageMeta.ShortDesc = smdt.CoreProperties.Description;
                        pageMeta.FileName = string.Empty;

                        WikiPageExtensions.SavePageMetaData(pageMeta);
                        dPage.metaData = pageMeta;

                        oWiki.wikiPages.Add(dPage);

                        documentIds.Add(defaultTags);

                        EditWikiPages(oWiki, dPage, files);
                        DeleteWikiPages(wikiId);

                    }

                }


            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error updating wiki with Id: {0}, and contentId: {1}", wikiId, contentId), ex).Log();

            }
          
        } // end of method

        private void CreateWikiPages(OmnicellWiki oWiki, OmnicellWikiPage pWikipage, Dictionary<string, MemoryStream> files)
        {
            HtmlDocument tDoc = new HtmlDocument();
            MemoryStream tocLink = files["toc.html"];
            tocLink.Position = 0;
            tDoc.Load(tocLink, Encoding.UTF8);
            HtmlNode bULTag = tDoc.DocumentNode.Descendants("ul").FirstOrDefault();

            IEnumerable<HtmlNode> tocNodes = from node in bULTag.ChildNodes
                                             where node.Name == "li"
                                             select node;

            TraverseNodes(tocNodes, pWikipage, oWiki, files);
        }

        private OmnicellWikiPage CreateNewWikiPage(OmnicellWiki oWiki, OmnicellWikiPage pWikipage, string nodePath, Dictionary<string, MemoryStream> files, ref int count)
        {
            HtmlDocument pageDoc = new HtmlDocument();
            int index = nodePath.IndexOf("#");
            string nodeStream = nodePath;

            if (index != -1)
            {
                nodePath = nodePath.Remove(index);
            }

            files[nodePath].Position = 0;
            pageDoc.Load(files[nodePath], Encoding.UTF8);
            string description = String.Empty;
            string Id = String.Empty;
            string fileName = string.Empty;
            HtmlNode bodyNode = pageDoc.DocumentNode.Descendants("body").FirstOrDefault();



            WikiPagesCreateOptions options = new WikiPagesCreateOptions();

            HtmlNode title = pageDoc.DocumentNode.SelectSingleNode("//title");
            HtmlNode sClass = bodyNode.SelectSingleNode("//p[@class='shortdesc']");


            if (bodyNode.SelectSingleNode("//table[@class='table']") != null)
            {
                List<HtmlNode> table = bodyNode.SelectNodes("//table[@class='table']").ToList();
                foreach (HtmlNode cNode in table)
                {
                    List<HtmlNode> trs = cNode.SelectNodes("//tr").ToList();
                    List<HtmlNode> tds = cNode.SelectNodes("//td").ToList();

                    foreach (HtmlNode tr in trs)
                    {
                        tr.Attributes.Remove("width");
                    }

                    foreach (HtmlNode td in tds)
                    {
                        td.Attributes.Remove("width");
                        ProcessTableElements(td);
                    }
                }
            }

           
            string wikiPageName = title.InnerHtml.Replace("&amp;", "&");
            wikiPageName = wikiPageName.Replace(":", "");
               
            IEnumerable<HtmlNode> collection = pageDoc.DocumentNode.Descendants("meta").Where(a => a.Attributes.Contains("name"));
            foreach (var meta in collection)
            {
                string nodeName = meta.Attributes["name"].Value;

                switch (nodeName)
                {
                    case "description": description = meta.Attributes["content"].Value; break;
                    case "DC.Identifier": Id = meta.Attributes["content"].Value; break;
                    case "LinkName": fileName = meta.Attributes["content"].Value; break;
                }

            }

            if (!string.IsNullOrEmpty(fileName))
            {
                fileName = fileName.Replace(".xml", "");
            }

            Id = "DocGUID:" + Id;
            documentIds.Add(Id);

            HtmlNode body = ProcessImages(bodyNode, oWiki.Id, files);

            body.InnerHtml = body.InnerHtml.Replace("&amp;", "&");

            string tagId = nodePath;
            int indexHtml = nodePath.IndexOf("#");

            if (indexHtml != -1)
            {
                tagId = nodePath.Remove(indexHtml);
            }

            tagId = tagId.Replace(".html", "");

            // call method to replace links with telligent wiki links
            body = ProcessLinks(body);
            body = AddHttp(body);
           // body = ProcessNotes(body, (int)oWiki.Id);

            int pWikiId = (int)oWiki.Id;
            string docIdTag = Id;
            string docTags = docIdTag;
            options.ParentPageId = pWikipage.Id;
            options.Body = body.InnerHtml;
            options.Tags = docTags;
            var wikiPage = PublicApi.WikiPages.Create(pWikiId, wikiPageName, options);
            OmnicellWikiPage nWikiPage = null;

            if (wikiPage.Id != null)
            {
                nWikiPage = new OmnicellWikiPage(wikiPage);
                OmnicellWikiPageMetadata nMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);

                if (!string.IsNullOrEmpty(Id))
                    nMeta.contentId = Id;

                nMeta.WikiPageId = (int)nWikiPage.Id;
                nMeta.ParentPageId = (int)pWikipage.Id;
                nMeta.PageKey = nWikiPage.PageKey;
                nMeta.OrderNumber = ++count;
                nMeta.ShortDesc = description;
                nMeta.FileName = fileName;

                WikiPageExtensions.SavePageMetaData(nMeta);
                nWikiPage.metaData = nMeta;

                List<HtmlNode> linkNodes = body.Descendants("li").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("xref"))).ToList();
                List<HtmlNode> listNodes = body.Descendants("a").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("xref"))).ToList();

                if (linkNodes.Count > 0 || listNodes.Count > 0)
                {
                    interLinks.Add(nWikiPage);
                    //ProcessInlineLinks(body);

                }

               
            }

            return nWikiPage;
        }

        private OmnicellWikiPage UpdateWikiPage(OmnicellWiki oWiki, OmnicellWikiPage pWikipage, string refPage, Dictionary<string, MemoryStream> files, int wikiPageId, ref int count)
        {
            string tagId = String.Empty;
            WikiPagesGetOptions gOptions = new WikiPagesGetOptions();
            gOptions.WikiId = oWiki.Id;
            gOptions.Id = wikiPageId;
            OmnicellWikiPage nWikiPage = null;
            
            try
            {
                var wikiPage = PublicApi.WikiPages.Get(gOptions);

                if (wikiPage != null)
                {
                    WikiPagesUpdateOptions uOptions = new WikiPagesUpdateOptions();

                    HtmlDocument pageDoc = new HtmlDocument();
                    int index = refPage.IndexOf("#");
                    if (index != -1)
                    {
                        refPage = refPage.Remove(index);
                    }

                    files[refPage].Position = 0;
                    pageDoc.Load(files[refPage], Encoding.UTF8);
                    string description = String.Empty;
                    string Id = String.Empty;
                    string fileName = string.Empty;
                    HtmlNode bodyNode = pageDoc.DocumentNode.Descendants("body").FirstOrDefault();

                    HtmlNode body = ProcessImages(bodyNode, oWiki.Id, files);
                    body.InnerHtml = body.InnerHtml.Replace("&amp;", "&");
                    HtmlNode title = pageDoc.DocumentNode.SelectSingleNode("//title");
                    string wikiPageName = title.InnerHtml.Replace("&amp;", "&");
                    HtmlNode sClass = body.SelectSingleNode("//p[@class='shortdesc']");

                    if (bodyNode.SelectSingleNode("//table[@class='table']") != null)
                    {
                        List<HtmlNode> table = bodyNode.SelectNodes("//table[@class='table']").ToList();
                        foreach (HtmlNode cNode in table)
                        {
                            List<HtmlNode> trs = cNode.SelectNodes("//tr").ToList();
                            List<HtmlNode> tds = cNode.SelectNodes("//td").ToList();

                            foreach (HtmlNode tr in trs)
                            {
                                tr.Attributes.Remove("width");
                            }

                            foreach (HtmlNode td in tds)
                            {
                                td.Attributes.Remove("width");
                            }
                        }
                    }


                    IEnumerable<HtmlNode> collection = pageDoc.DocumentNode.Descendants("meta").Where(a => a.Attributes.Contains("name"));
                    foreach (var meta in collection)
                    {
                        string nodeName = meta.Attributes["name"].Value;

                        switch (nodeName)
                        {
                            case "description": description = meta.Attributes["content"].Value; break;
                            case "DC.Identifier": Id = meta.Attributes["content"].Value; break;
                            case "LinkName": fileName = meta.Attributes["content"].Value; break;
                        }

                    }

                    if (!string.IsNullOrEmpty(fileName))
                    {
                        fileName = fileName.Replace(".xml", "");
                    }

                    Id = "DocGUID:" + Id;
                    body = ProcessLinks(body);
                    body = AddHttp(body);

                    int pWikiId = Convert.ToInt32(oWiki.Id);
                    string docIdTag = Id;
                    string docTags = docIdTag;
                    uOptions.ParentPageId = pWikipage.Id;
                    uOptions.Body = body.InnerHtml;
                    uOptions.WikiId = oWiki.Id;
                    uOptions.Tags = docTags;
                    uOptions.Title = wikiPageName;

                    var wikiUpdatePage = PublicApi.WikiPages.Update(wikiPageId, uOptions);

                    nWikiPage = new OmnicellWikiPage(wikiUpdatePage);
                    OmnicellWikiPageMetadata nMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);


                    nMeta.contentId = Id;

                    nMeta.WikiPageId = (int)nWikiPage.Id;
                    nMeta.ParentPageId = (int)pWikipage.Id;
                    nMeta.PageKey = nWikiPage.PageKey;
                    nMeta.OrderNumber = ++count;
                    nMeta.ShortDesc = description;
                    nMeta.FileName = fileName;

                    WikiPageExtensions.SavePageMetaData(nMeta);
                    nWikiPage.metaData = nMeta;

                    List<HtmlNode> linkNodes = body.Descendants("li").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("xref"))).ToList();
                    List<HtmlNode> listNodes = body.Descendants("a").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("xref"))).ToList();
                 

                    if (linkNodes.Count > 0 || listNodes.Count > 0)
                    {
                        interLinks.Add(nWikiPage);
                    }

                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error updating wiki page with Id: {0}, and contentId: {1}", wikiPageId, tagId), ex).Log();
            }

            return nWikiPage;

        }

        private void EditWikiPages(OmnicellWiki oWiki, OmnicellWikiPage pWikipage, Dictionary<string, MemoryStream> files)
        {
            HtmlDocument tDoc = new HtmlDocument();
            files["toc.html"].Position = 0;
            tDoc.Load(files["toc.html"], Encoding.UTF8);
            HtmlNode bULTag = tDoc.DocumentNode.Descendants("ul").FirstOrDefault();

            IEnumerable<HtmlNode> tocNodes = from node in bULTag.ChildNodes
                                             where node.Name == "li"
                                             select node;

            TraverseUpdateNodes(tocNodes, pWikipage, oWiki, files);
        }

        private void DeleteWikiPages(int wikiId)
        {
            int pageSize = 5000;
            List<string> contentIds = new List<string>();
            WikiPagesListOptions pOptions = new WikiPagesListOptions();
            WikiPagesUpdateOptions uOptions = new WikiPagesUpdateOptions();
            bool? isPublished = false;
            bool? includeDisabled = true;
            pOptions.PageSize = pageSize;
            pOptions.IncludeDisabledPages = includeDisabled;
            var children = PublicApi.WikiPages.List(wikiId, pOptions).ToList();
            var db = new OmnicellEntities();

            foreach (var child in children)
            {

                string docId = child.Tags.FirstOrDefault(s => s.StartsWith("DocGUID:"));
               // docId = docId.Replace("DocGUID:", "");

                if (!documentIds.Any(a => a.Equals(docId)))
                {
                    
                    uOptions.IsPublished = isPublished;
                    int id = Convert.ToInt32(child.Id);
                    var page = PublicApi.WikiPages.Update(id, uOptions);

                    if (page != null)
                    {
                        var wikiPage = db.WikiPageMetaDatas.FirstOrDefault(x => x.WikiPageId == page.Id);
                   

                        if (wikiPage != null)
                        {

                          var  newWikiPage = new WikiPageMetaData
                            {
                              ContentId = wikiPage.ContentId,
                              Audience = wikiPage.Audience,
                              Product = wikiPage.Product,
                              Interfaces = wikiPage.Interfaces,
                              Features = wikiPage.Features,
                              DocumentType = wikiPage.DocumentType,
                              GroupId = wikiPage.GroupId,
                              WikiId = wikiPage.WikiId,
                              ParentPageId = wikiPage.ParentPageId,
                              OrderNumber = null,
                              PageKey = wikiPage.PageKey,
                              WikiPageId = wikiPage.WikiPageId,
                              ShortDesc = wikiPage.ShortDesc,
                              RevNum = wikiPage.RevNum,
                              PartName = wikiPage.PartName

                            };

                            db.WikiPageMetaDatas.DeleteObject(wikiPage);
                            db.WikiPageMetaDatas.AddObject(newWikiPage);
                            db.SaveChanges();

                        }
                      
                    }


                }
                else
                {
                    isPublished = true;
                    uOptions.IsPublished = isPublished;
                    int id = Convert.ToInt32(child.Id);
                    var page = PublicApi.WikiPages.Update(id, uOptions);
                }
            }

        }

        private OmnicellWikiMetadata PopulateMetadata(SharepointDITA.DitaDocMetaData propsDITA)
        {
            OmnicellWikiMetadata meta = new OmnicellWikiMetadata();
            SharepointDITA.ParseMetaData pmeta = new SharepointDITA.ParseMetaData();

            try
            {
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.DocumentType))
                {
                    int docType = pmeta.ParseDocumentId(propsDITA.CustomProperties.DocumentType);
                    meta.DocumentType = docType.ToString();
                }

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Audience))
                {
                    meta.Audiences = pmeta.TranslateAudienceTypes(propsDITA.CustomProperties.Audience);
                }

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Product))
                {
                    meta.Products = pmeta.TranslateProductsGroup(propsDITA.CustomProperties.Product);
                }

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.ProductVersion))
                    meta.ProductVersion = propsDITA.CustomProperties.ProductVersion;
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Interfaces))
                    meta.Interfaces = propsDITA.CustomProperties.Interfaces;
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Features))
                    meta.Features = propsDITA.CustomProperties.Features;
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.RevNum))
                    meta.RevNum = propsDITA.CustomProperties.RevNum;
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.PartName))
                    meta.PartName = propsDITA.CustomProperties.PartName;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return meta;
        }

        private HtmlNode AddHttp(HtmlNode node)
        {
           
            if (node.SelectSingleNode("//a[@class='xref']") != null)
            {
                List<HtmlNode> linkNodes = node.SelectNodes("//a[@class='xref']").ToList();

                foreach (HtmlNode link in linkNodes)
                {
                    string url = link.Attributes["href"].Value;

                    if (!url.Contains("http://") && !url.Contains("https://") && !url.StartsWith("#"))
                    {
                        link.Attributes["href"].Value = "http://infodevcms.omnicell.com/Repository/Topics/" + url;
                    }
                }
            }
            
            return node;
        }

        private HtmlNode ProcessLinks(HtmlNode node)
        {
            List<HtmlNode> linkNodes = node.Descendants("a").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("link"))).ToList();
            List<HtmlNode> dupNodes = new List<HtmlNode>();
            string linkText = String.Empty;

            foreach (HtmlNode cNode in linkNodes)
            {
                dupNodes.Add(cNode);
            }


            for (int i = dupNodes.Count - 1; i >= 0; i--)
            {
                string url = linkNodes[i].Attributes["href"].Value;
                
                if (!url.Contains("http://") && !url.Contains("https://"))
                {
                    string link = linkNodes[i].InnerHtml.Replace(":", "");

                    linkText = "<span>[[" + link + "]]</span>";
                    var newNode = HtmlNode.CreateNode(linkText);
                    linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                    linkNodes.Remove(linkNodes[i]);
                }
                    
            }

            HtmlNode wikiBody = ProcessListItemLinks(node);

            return wikiBody;

        }

        private HtmlNode ProcessListItemLinks(HtmlNode node)
        {
            List<HtmlNode> linkNodes = node.Descendants("li").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("link"))).ToList();
            List<HtmlNode> dupNodes = new List<HtmlNode>();
            string linkText = String.Empty;

            foreach (HtmlNode cNode in linkNodes)
            {
                dupNodes.Add(cNode);
            }

            for (int i = dupNodes.Count - 1; i >= 0; i--)
            {
                HtmlNode linkNode = linkNodes[i].Descendants("a").FirstOrDefault();
                string url = linkNode.Attributes["href"].Value;

               
                if (!url.Contains("http://") && !url.Contains("https://"))
                {
                    string link = linkNode.InnerHtml.Replace(":", "");

                    int colon = link.IndexOf(':');
                    if (colon != -1)
                        link = link.Remove(colon);

                    linkText = "<span>[[" + link + "]]</span>";
                    var newNode = HtmlNode.CreateNode(linkText);
                    linkNode.ParentNode.ReplaceChild(newNode, linkNode);
                    linkNodes.Remove(linkNodes[i]);
                }
               
            }

          //  node = ProcessNoClassLinks(node);

            return node;
        }

        public void ProcessListImages(HtmlNode node)
        {
            var listImages = node.Descendants().Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Split(' ').Any(b => b.Equals("image"))).ToList();

            foreach (HtmlNode img in listImages)
            {
                img.SetAttributeValue("class", "listImg");
            }
        }

        public void ProcessTableElements(HtmlNode node)
        {
            var tImages = node.Descendants().Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Split(' ').Any(b => b.Equals("image"))).ToList();
            var tPara = node.Descendants().Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Split(' ').Any(b => b.Equals("p"))).ToList();
            var tUl = node.Descendants().Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Split(' ').Any(b => b.Equals("ul"))).ToList();
            var tli = node.Descendants().Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Split(' ').Any(b => b.Equals("li"))).ToList();

            foreach (HtmlNode image in tImages)
            {
                image.SetAttributeValue("class", "tableImg");
                var prev = image.PreviousSibling;

                if (prev != null && prev.Name == "br")
                    image.ParentNode.RemoveChild(prev);
            }

            foreach (HtmlNode para in tPara)
            {
                para.SetAttributeValue("class", "tp");
            }

            foreach (HtmlNode ul in tUl)
            {
                ul.SetAttributeValue("class", "tul");
            }

            foreach (HtmlNode li in tli)
            {
                li.SetAttributeValue("class", "tli");
            }
        }

        public HtmlNode ProcessNotes(HtmlNode node, int wikiId)
        {
            List<HtmlNode> notes = node.Descendants("div").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("note"))).ToList();
            string apiKey = _plgnSP.omApiKey;
            string genImgUrl = _plgnSP.omNoteGeneric;
            string impImgUrl = _plgnSP.omNoteImportant;
            string warImgUrl = _plgnSP.omNoteOther;
            string cauImgUrl = _plgnSP.omNoteCaution;
            byte[] imageByte = null;

            foreach (HtmlNode note in notes)
            {
                string noteClass = note.Attributes["class"].Value;

                if(noteClass.Contains("important"))
                {
                     imageByte = WikiPageExtensions.CentralFileImageToByteArray(impImgUrl, apiKey);
                }
                else if (noteClass.Contains("warning"))
                {
                    imageByte = WikiPageExtensions.CentralFileImageToByteArray(warImgUrl, apiKey);
                }
                else if (noteClass.Contains("caution"))
                {
                    imageByte = WikiPageExtensions.CentralFileImageToByteArray(cauImgUrl, apiKey);
                }
                else
                {
                     imageByte = WikiPageExtensions.CentralFileImageToByteArray(genImgUrl, apiKey);
                }

                string fileName = Guid.NewGuid().ToString();

                var wikiFile = PublicApi.WikiFiles.Create(wikiId, fileName, imageByte);

                if (wikiFile.FileName != null)
                {
                    string imageText = string.Format("<img src='{0}' />", wikiFile.FileUrl);
                    string tableText = string.Format("<table class='noteTable'><tr><td><img src='{0}' /></td><td>{1}</td></tr></table>", wikiFile.FileUrl, note.InnerHtml);
                 
                    var newNode = HtmlNode.CreateNode(tableText);
                    note.ParentNode.ReplaceChild(newNode, note);
                   
                   
                    
                }

            }
          
            return node;
        }

        public HtmlNode ProcessImages(HtmlNode node, int? wikiId, Dictionary<string, MemoryStream> files)
        {
            List<HtmlNode> images = node.Descendants("img").ToList();
            List<HtmlNode> dupNodes = new List<HtmlNode>();
            string fileName = String.Empty;

            foreach (HtmlNode image in images)
            {
                dupNodes.Add(image);
            }

            for (int i = dupNodes.Count - 1; i >= 0; i--)
            {
                fileName = images[i].Attributes["src"].Value;
                files[fileName].Position = 0;
                MemoryStream url = files[fileName];

                byte[] imageByte = WikiPageExtensions.StreamToByteArray(url);
                int wikiFileId = (int)wikiId;
                var wikiFile = PublicApi.WikiFiles.Create(wikiFileId, fileName, imageByte);

                if (wikiFile.FileName != null)
                {
                   // string src = wikiFile.FileUrl.Replace("/cfs-file.ashx/", "/resized-image.ashx/__size/400x0/");
                   // images[i].SetAttributeValue("src", src);
                    images[i].SetAttributeValue("src", wikiFile.FileUrl);
                }
               

                string linkText = String.Format("<a href='{0}' target='_blank'>{1}</a>", wikiFile.FileUrl, images[i].OuterHtml);
                var newNode = HtmlNode.CreateNode(linkText);
                images[i].ParentNode.ReplaceChild(newNode, images[i]);
                images.Remove(images[i]);
            }

            return node;
        }

        public bool BatchDelete()
        {
            bool boolReturn = true;
            try
            {
                var db = new OmnicellEntities();
                var dbEntity = db.Omnicell_WikiMetaData2.ToList();
                // get the list of libraries and mediagalleries
                string[] arrMGs = GetMediaGalleryList(_plgnSP.DLMGMaps);
                string[] arrLibs = GetSPLibraryList(_plgnSP.DLMGMaps);
                // get the list of documents in the MG and the Lib
                SPExtv1.PagedList<SPApi.Document> spdoclist = new SPExtv1.PagedList<SPApi.Document>();
                // set up list of pdf documents
                TEntities.PagedList<TEntities.Media> mgdoclist = new TEntities.PagedList<TEntities.Media>();

                for (int i = 0; i < arrLibs.Length; i++)
                {
                    spdoclist = GetSharepointLibrary(_plgnSP.SrcDocLibId);
                    // original delete
                    foreach (var wik in dbEntity)
                    {

                        if (spdoclist.Any(x => x.ContentId.CompareTo(wik.ContentId) == 0))
                        {

                        }
                        else
                        {
                            int wikId = (int)wik.WikiId;
                            var result = PublicApi.Wikis.Delete(wikId);
                            var wikiPages = db.WikiPageMetaDatas.Where(x => x.WikiId == wikId);

                            foreach (var page in wikiPages)
                            {

                                db.DeleteObject(page);
                            }

                            db.SaveChanges();
                            db.DeleteObject(wik);
                            db.SaveChanges();
                        }
                    }

                }
                MoxiPDFExtensions mdf = new MoxiPDFExtensions();
                mdf.BatchDelete();

            }
            catch (Exception ex)
            {
                boolReturn = false;
                new CSException(CSExceptionType.UnknownError, string.Format("Error deleting wikis"), ex).Log();
            }
            return boolReturn;
        }

        public bool BatchCreate()
        {
            bool boolReturn = true;
            try
            {
                // get the list of documents in the Source Library
                SPExtv1.PagedList<SPApi.Document> spdoclist = new SPExtv1.PagedList<SPApi.Document>();
                spdoclist = GetSharepointLibrary(_plgnSP.SrcDocLibId);

                documentIds.Clear();
                foreach (SPApi.Document doc in spdoclist)
                {
                    interLinks.Clear();
                    Telligent.Evolution.Components.User admUser = Telligent.Evolution.Users.GetUser("admin");
                    HandleCreateUpdateWiki(doc);
                }

            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error creating wikis"), ex).Log();
            }
            return boolReturn;
        }

        private void CreateOrUpdateWiki(SPApi.Document zDoc, string filePath, string docName, SPExtv1.PagedList<SPApi.Document> spdoclist, SPApi.Document doc)
        {

            string[] arrGroupId = GetSPLibraryList(_plgnSP.MoxiLibraryMap);
            string groupName = String.Empty;
            int groupId;
            int wikiGroupId = 0;
            string wKey = zDoc.DisplayName;
            wKey = wKey.Replace(" - DITA Map XHTML", "");
            wKey = Regex.Replace(wKey, @"[^A-Za-z0-9]+", "");
            string strSharepointRootURL = _plgnSP.SPFullUrl;
            string directory = String.Empty;
            string tocLink = filePath + Path.DirectorySeparatorChar + "toc.html";
            SharepointDITA.DitaDocMetaData smdt = null;
            Dictionary<string, MemoryStream> files = new Dictionary<string, MemoryStream>();
            MoxiPDFExtensions mpf = new MoxiPDFExtensions();

            try
            {

                // will this retrieve the Target Group Meta Tag?
                GetTargetGroup(_plgnSP.MoxiLibraryMap, zDoc.MetaInfo, out groupId, out groupName);

                if (groupId > 0)
                {

                    TEntities.Wiki toWiki = TApi.PublicApi.Wikis.Get(new TApi.WikisGetOptions { GroupId = groupId, Key = wKey });

                    if (toWiki != null)
                    {
                        TimeZoneInfo sTimeZone = TimeZoneInfo.FindSystemTimeZoneById(_plgnSP.omTimeZone);
                        
                        DateTime utcDate = Convert.ToDateTime(toWiki.LastModifiedUtcDate);
                        DateTime pDate = TimeZoneInfo.ConvertTimeFromUtc(utcDate, sTimeZone);
                        bool isDayLight = sTimeZone.IsDaylightSavingTime(pDate);
                        int offset = isDayLight == true ? 5 : 6;
                        int currentWikiId = (int)toWiki.Id;
                        OmnicellWiki currentWiki = new OmnicellWiki(toWiki);
                        //DateTime date = TimeZoneInfo.ConvertTime(zDoc.Modified, sTimeZone);
                        DateTime date = zDoc.Modified.AddHours(offset);
                        //date.AddHours(7);
                        var result = DateTime.Compare(date, pDate);

                        if (result > 0)
                        {
                            // add code to retrieve the XHTML zip stream
                           ZipInputStream strmzipfile = GetRemoteZipStream(strSharepointRootURL.ToString() + zDoc.Path, _plgnSP.SPDomain, _plgnSP.SPLogin, _plgnSP.SPPassword);
                           files = ExtractFiles(strmzipfile);

                            // code to extract metadata
                            HtmlDocument tdoc = new HtmlDocument();
                            files["toc.html"].Position = 0;
                            tdoc.Load(files["toc.html"], Encoding.UTF8);
                            smdt = ExtractMetaDataFromTOCFile(tdoc);

                            // code to edit wiki
                            wikiGroupId = Convert.ToInt32(toWiki.Group.Id);
                            int wikiId = Convert.ToInt32(toWiki.Id);
                            EditWiki(wikiGroupId, wikiId, wKey, zDoc.ContentId, smdt, files);
                            AdjustWikiLinks(wikiId);

                        }

                    }
                    else
                    {
                        // code to retrieve the XHTML zip stream
                        ZipInputStream strmzipfile = GetRemoteZipStream(strSharepointRootURL.ToString() + zDoc.Path, _plgnSP.SPDomain, _plgnSP.SPLogin, _plgnSP.SPPassword);
                        files = ExtractFiles(strmzipfile);

                        // code to create a new wiki
                       int wikiId = 0;
                      // CreateNewWiki(groupId, wKey, zDoc.ContentId, files, out wikiId);
                       CreateNewWiki(groupId, wKey, zDoc.ContentId, files, out wikiId);
                       AdjustWikiLinks(wikiId);

                    }

                    string pdfDocName = docName.ToLower() + ".pdf";
                    SPApi.Document pdfDoc = null;

                    foreach (SPApi.Document document in spdoclist)
                    {
                        if (document.Name.ToLower() == pdfDocName.ToLower())
                        {
                            pdfDoc = document;
                            break;
                        }
                    }
                    if (pdfDoc != null)
                    {

                        if (pdfDoc.Path != "" && doc.Path != "")
                        {
                           // string wikiKey = zDoc.ContentId.ToString();

                            // code to call pdf create and update events smdt pdfDoc
                            mpf.CreateUpdateDocument(pdfDoc, smdt, zDoc.ContentId, groupName, wKey, groupId);

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error processing document with contentId: {1}", zDoc.ContentId), ex).Log();
            }
            finally
            {
                    if(files.Count > 0)
                         DeleteFiles(files);
            }

        }

        private void DeleteFiles(Dictionary<string, MemoryStream> files)
        {
            foreach (KeyValuePair<string, MemoryStream> entry in files)
            {
                entry.Value.Dispose();
            }

            GC.Collect();
        }

        // hopefully this will solve the recursive issue
        private void TraverseNodes(IEnumerable<HtmlNode> nodes, OmnicellWikiPage pWikipage, OmnicellWiki oWiki, Dictionary<string, MemoryStream> files)
        {
            OmnicellWikiPage nWikiPage = null;
            int count = 0;
            // int interCopy = 0;
            foreach (HtmlNode child in nodes)
            {
                HtmlNode aNode = child.Descendants("a").FirstOrDefault();
                HtmlNode bnode = child.FirstChild;

                string refPage = string.Empty;


                if (bnode.Name == "a")
                {
                   
                    refPage = aNode.Attributes["href"].Value;

                    nWikiPage = CreateNewWikiPage(oWiki, pWikipage, refPage, files, ref count);
                   
                    oWiki.wikiPages.Add(nWikiPage);
                    pWikipage.wikiPages.Add(nWikiPage);

                }
                else
                {
                    int wIndex = child.InnerHtml.IndexOf("<");
                    string mainText = String.Empty;
                    string title = String.Empty;
                    string description = String.Empty;
                    string pKey = String.Empty;
                    if (wIndex != -1)
                    {
                        mainText = child.InnerHtml.Remove(wIndex);
                        title = mainText.Replace("&amp;", "&").Replace(":", "");

                        description = "";
                        pKey = Regex.Replace(mainText, @"\s+", "");
                        pKey = pKey.Replace(",", "");
                    }
                    else
                    {
                        pKey = Regex.Replace(child.InnerText, @"\s+", "");
                        pKey = pKey.Replace(",", "");
                        title = child.InnerText.Replace("&amp;", "&").Replace(":", "");


                        description = "";
                    }

                    int pWikiId = Convert.ToInt32(oWiki.Id);
                    WikiPagesCreateOptions options = new WikiPagesCreateOptions();
                    options.ParentPageId = pWikipage.Id;
                    options.Body = title;
                    options.Tags = "DocGUID:" + pKey;
                    var wikiPage = PublicApi.WikiPages.Create(pWikiId, title, options);


                    nWikiPage = new OmnicellWikiPage(wikiPage);
                    OmnicellWikiPageMetadata nMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);
                    nMeta.contentId = "DocGUID:" + pKey;
                    nMeta.WikiPageId = (int)nWikiPage.Id;
                    nMeta.ParentPageId = (int)pWikipage.Id;
                    nMeta.PageKey = nWikiPage.PageKey;
                    nMeta.OrderNumber = ++count;
                    nMeta.ShortDesc = string.Empty;
                    nMeta.FileName = string.Empty;

                    WikiPageExtensions.SavePageMetaData(nMeta);

                    nWikiPage.metaData = nMeta;

                    oWiki.wikiPages.Add(nWikiPage);
                    pWikipage.wikiPages.Add(nWikiPage);
                }
                int index = -1;

                if (!string.IsNullOrEmpty(refPage))
                {
                    index = refPage.IndexOf("#");
                }


                //cycle through child nodes hopefully
                HtmlNode nestedUl = child.Descendants("ul").FirstOrDefault();


                if (nestedUl != null && index == -1)
                {
                    IEnumerable<HtmlNode> childList = from node in nestedUl.ChildNodes
                                                      where node.Name == "li"
                                                      select node;

                    if (childList.Any())
                    {
                        TraverseNodes(childList, nWikiPage, oWiki, files);
                    }
                }



            }
        }

        private void TraverseUpdateNodes(IEnumerable<HtmlNode> nodes, OmnicellWikiPage pWikipage, OmnicellWiki oWiki, Dictionary<string, MemoryStream> files)
        {
            OmnicellWikiPage nWikiPage = null;
            int count = 0;
            foreach (HtmlNode child in nodes)
            {
                HtmlNode aNode = child.Descendants("a").FirstOrDefault();
                HtmlNode bnode = child.FirstChild;
                string refPage = string.Empty;


                if (bnode.Name == "a")
                {
                 
                    refPage = aNode.Attributes["href"].Value;


                    int tagIndex = refPage.IndexOf("#");
                    string contentId = RetrieveDocId(refPage, files);
        
                    int wikiPageId = Convert.ToInt32(oWiki.Id);

                    int wikiPageExists = WikiPageExtensions.WikiPageExists(wikiPageId, contentId);

                    if (wikiPageExists > 0)
                    {
                        // wire in update method
                        nWikiPage = UpdateWikiPage(oWiki, pWikipage, refPage, files, wikiPageExists, ref count);

                        oWiki.wikiPages.Add(nWikiPage);
                        pWikipage.wikiPages.Add(nWikiPage);

                    }
                    else
                    {

                        nWikiPage = CreateNewWikiPage(oWiki, pWikipage, refPage, files, ref count);
                       
                        oWiki.wikiPages.Add(nWikiPage);
                        pWikipage.wikiPages.Add(nWikiPage);
                    }

                }
                else
                {
                    int wIndex = child.InnerHtml.IndexOf("<");
                    string mainText = String.Empty;
                    string title = String.Empty;
                    string description = String.Empty;

                    if (wIndex != -1)
                    {
                        mainText = child.InnerHtml.Remove(wIndex);
                        title = mainText.Replace("&amp;", "&");
                        description = string.Empty;
                    }
                    else
                    {
                        title = child.InnerText.Replace("&amp;", "&"); 
                        description = string.Empty;
                    }

                    title = title.Replace("\n", string.Empty);
                    string contentId = Regex.Replace(title, @"\s+", "");
                    contentId = contentId.Replace(",", "");
                    contentId = "DocGUID:" + contentId;
                    documentIds.Add(contentId);
                    int pWikiId = Convert.ToInt32(oWiki.Id);
                    int wikiPageId = WikiPageExtensions.WikiPageExists(pWikiId, contentId);

                    if (wikiPageId > 0)
                    {
                        WikiPagesUpdateOptions options = new WikiPagesUpdateOptions();
                        options.ParentPageId = pWikipage.Id;
                        options.Body = title;
                        options.Title = title;
                        options.WikiId = oWiki.Id;
                        options.Tags = contentId;
                        var wikiPage = PublicApi.WikiPages.Update(wikiPageId, options);

                        nWikiPage = new OmnicellWikiPage(wikiPage);
                        OmnicellWikiPageMetadata oMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);
                        oMeta.WikiPageId = (int)nWikiPage.Id;
                        oMeta.ParentPageId = (int)pWikipage.Id;
                        oMeta.PageKey = nWikiPage.PageKey;
                        oMeta.OrderNumber = ++count;
                        oMeta.contentId = contentId;
                        oMeta.ShortDesc = string.Empty;
                        oMeta.FileName = string.Empty;

                        WikiPageExtensions.SavePageMetaData(oMeta);
                        nWikiPage.metaData = oMeta;
                        oWiki.wikiPages.Add(nWikiPage);
                        pWikipage.wikiPages.Add(nWikiPage);
                    }
                    else
                    {
                        WikiPagesCreateOptions options = new WikiPagesCreateOptions();
                        options.ParentPageId = pWikipage.Id;
                        options.Body = title;
                        options.Tags = contentId;
                        var wikiPage = PublicApi.WikiPages.Create(pWikiId, title, options);

                        nWikiPage = new OmnicellWikiPage(wikiPage);
                        OmnicellWikiPageMetadata oMeta = new OmnicellWikiPageMetadata(oWiki.wikiMeta);
                        oMeta.WikiPageId = (int)nWikiPage.Id;
                        oMeta.ParentPageId = (int)pWikipage.Id;
                        oMeta.PageKey = nWikiPage.PageKey;
                        oMeta.OrderNumber = ++count;
                        oMeta.contentId = contentId;
                        oMeta.ShortDesc = string.Empty;
                        oMeta.FileName = string.Empty;

                        WikiPageExtensions.SavePageMetaData(oMeta);
                        nWikiPage.metaData = oMeta;
                        oWiki.wikiPages.Add(nWikiPage);
                        pWikipage.wikiPages.Add(nWikiPage);

                    }
                }

                int index = -1;

                if (!string.IsNullOrEmpty(refPage))
                {
                    index = refPage.IndexOf("#");
                }


                //cycle through child nodes hopefully
                HtmlNode nestedUl = child.Descendants("ul").FirstOrDefault();


                if (nestedUl != null && index == -1)
                {
                    IEnumerable<HtmlNode> childList = from node in nestedUl.ChildNodes
                                                      where node.Name == "li"
                                                      select node;
                    if (childList.Any())
                    {
                        TraverseUpdateNodes(childList, nWikiPage, oWiki, files);
                    }
                }

            }
        }

        private void AdjustWikiLinks(int wikiId)
        {
            foreach (OmnicellWikiPage page in interLinks)
            {
                WikiPagesGetOptions op = new WikiPagesGetOptions();
                op.Id = page.Id;

                var wiki = PublicApi.WikiPages.Get(op);

                if (wiki.Id != null)
                {
                    string body = wiki.Body();
                    string html = "<div>" + body + "</div>";
                    // Html Agility Code
                    HtmlDocument doc = new HtmlDocument();

                    doc.LoadHtml(html);

                    HtmlNode main = doc.DocumentNode.SelectSingleNode("//div");

                  main = ProcessInlineLinks(main, wikiId);
                  //  main = ProcessListItemCrossLinks(main, wikiId);


                    WikiPagesUpdateOptions up = new WikiPagesUpdateOptions();
                    up.Body = main.InnerHtml;
                    int pageId = (int)page.Id;
                    var wikiUpdate = PublicApi.WikiPages.Update(pageId, up);
                }



            }
        }

        private HtmlNode ProcessInlineLinks(HtmlNode node, int wikiId)
        {
           // List<HtmlNode> linkNodes = node.Descendants("a").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("xref"))).ToList();
            List<HtmlNode> linkNodes = node.SelectNodes("//*[contains(@class, 'xref')]").ToList();
            List<HtmlNode> dupNodes = new List<HtmlNode>();
            string linkText = String.Empty;
            string[] linkHash;

            foreach (HtmlNode cNode in linkNodes)
            {
                dupNodes.Add(cNode);
            }


            for (int i = dupNodes.Count - 1; i >= 0; i--)
            {
                if (linkNodes[i].Attributes["href"] != null)
                {
                    string url = linkNodes[i].Attributes["href"].Value;
                    if (url.Contains("http://") || url.Contains("https://"))
                    {
                        if (url.Contains("infodevcms.omnicell.com"))
                        // if (url.Contains(_plgnSP.SPFullUrl))
                        {
                            string splitUrl = url.Split('/').Last();

                            if (splitUrl.Contains("#"))
                            {
                                linkHash = splitUrl.Split('#');

                                //string id = "DocGUID:" + linkHash[0];
                                string id = linkHash[0];

                                using (var db = new OmnicellEntities())
                                {
                                    WikiPageMetaData dbEntity = null;

                                    dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.FileName) && x.WikiId == wikiId);
                                    //dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.ContentId) && x.WikiId == wikiId);

                                    if (dbEntity != null)
                                    {
                                        int wikiPageId = (int)dbEntity.WikiPageId;
                                        OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);
                                        string link = page.Title.Replace(":", "");

                                        linkText = "<span>[[" + link + "|" + linkNodes[i].InnerHtml + "]]</span>";
                                        var newNode = HtmlNode.CreateNode(linkText);
                                        string linkClass = linkHash[0].Replace(".html", "__") + linkHash[1];
                                        newNode.Attributes.Add("class", linkHash[1]);
                                        linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                                        linkNodes.Remove(linkNodes[i]);
                                    }
                                    else
                                    {
                                        string innerHtml = linkNodes[i].InnerHtml;
                                        // previous version used PageKey
                                        dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => x.PageKey.Contains(innerHtml) && x.WikiId == wikiId);

                                        if (dbEntity != null)
                                        {
                                            int wikiPageId = (int)dbEntity.WikiPageId;
                                            OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);
                                            string link = page.Title.Replace(":", "");

                                            linkText = "<span>[[" + link + "|" + linkNodes[i].InnerHtml + "]]</span>";
                                            var newNode = HtmlNode.CreateNode(linkText);
                                            linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                                            linkNodes.Remove(linkNodes[i]);
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string id = splitUrl;

                                using (var db = new OmnicellEntities())
                                {
                                    WikiPageMetaData dbEntity = null;

                                    dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.FileName) && x.WikiId == wikiId);
                                    //dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.ContentId) && x.WikiId == wikiId);

                                    if (dbEntity != null)
                                    {
                                        int wikiPageId = (int)dbEntity.WikiPageId;
                                        OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);
                                        string link = page.Title.Replace(":", "");


                                        linkText = "<span>[[" + link + "|" + linkNodes[i].InnerHtml + "]]</span>";
                                        var newNode = HtmlNode.CreateNode(linkText);
                                        linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                                        linkNodes.Remove(linkNodes[i]);
                                    }
                                    else
                                    {
                                        string innerHtml = linkNodes[i].InnerHtml;

                                        dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => x.PageKey.Contains(innerHtml) && x.WikiId == wikiId);

                                        if (dbEntity != null)
                                        {
                                            int wikiPageId = (int)dbEntity.WikiPageId;
                                            OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);
                                            string link = page.Title.Replace(":", "");

                                            linkText = "<span>[[" + link + "|" + linkNodes[i].InnerHtml + "]]</span>";
                                            var newNode = HtmlNode.CreateNode(linkText);
                                            linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                                            linkNodes.Remove(linkNodes[i]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return node;
        }

        private HtmlNode ProcessListItemCrossLinks(HtmlNode node)
        {
            List<HtmlNode> linkNodes = node.Descendants("li").ToList();
            List<HtmlNode> dupNodes = new List<HtmlNode>();
            string linkText = String.Empty;
            string[] linkHash;

            foreach (HtmlNode cNode in linkNodes)
            {
                dupNodes.Add(cNode);
            }

            for (int i = dupNodes.Count - 1; i >= 0; i--)
            {
                HtmlNode linkNode = linkNodes[i].Descendants("a").Where(d => d.Attributes.Contains("class") && d.Attributes["class"].Value.Split(' ').Any(b => b.Equals("xref"))).FirstOrDefault();

                if (linkNodes[i].Attributes["href"] != null)
                {

                    string url = linkNode.Attributes["href"].Value;

                    if (url.Contains("http://") || url.Contains("https://"))
                    {
                        if (url.Contains(_plgnSP.SPFullUrl))
                        {
                            string splitUrl = url.Split('/').Last();

                            if (splitUrl.Contains("#"))
                            {
                                linkHash = splitUrl.Split('#');

                                string id = "DocGUID:" + linkHash[0];

                                using (var db = new OmnicellEntities())
                                {
                                    WikiPageMetaData dbEntity = null;

                                    dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.ContentId));

                                    if (dbEntity != null)
                                    {
                                        int wikiPageId = (int)dbEntity.WikiPageId;
                                        OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);

                                        linkText = "<span>[[" + page.Title + "|" + linkNodes[i].InnerHtml + "]]</span>";
                                        var newNode = HtmlNode.CreateNode(linkText);
                                        linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                                        linkNodes.Remove(linkNodes[i]);
                                    }
                                }
                            }
                            else
                            {
                                string id = "DocGUID:" + splitUrl;

                                using (var db = new OmnicellEntities())
                                {
                                    WikiPageMetaData dbEntity = null;

                                    dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.ContentId));

                                    if (dbEntity != null)
                                    {
                                        int wikiPageId = (int)dbEntity.WikiPageId;
                                        OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);

                                        linkText = "<span>[[" + page.Title + "|" + linkNodes[i].InnerHtml + "]]</span>";
                                        var newNode = HtmlNode.CreateNode(linkText);
                                        linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                                        linkNodes.Remove(linkNodes[i]);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        // code for relative links including glossary links
                        if (url.Contains(".html"))
                        {
                            if (url.Contains('#'))
                            {
                                string splitUrl = url.Split('#').Last();
                                linkHash = splitUrl.Split('_');
                                string id = "DocGUID:" + linkHash[0];

                                using (var db = new OmnicellEntities())
                                {
                                    WikiPageMetaData dbEntity = null;

                                    dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.ContentId));

                                    if (dbEntity != null)
                                    {
                                        int wikiPageId = (int)dbEntity.WikiPageId;
                                        OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);

                                        linkText = "<span>[[" + page.Title + "|" + linkNodes[i].InnerHtml + "]]</span>";
                                        var newNode = HtmlNode.CreateNode(linkText);
                                        linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                                        linkNodes.Remove(linkNodes[i]);
                                    }
                                }

                            }
                            else
                            {
                                linkHash = url.Split('.');
                                string id = "DocGUID:" + linkHash[0];

                                using (var db = new OmnicellEntities())
                                {
                                    WikiPageMetaData dbEntity = null;

                                    dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.ContentId));

                                    if (dbEntity != null)
                                    {
                                        int wikiPageId = (int)dbEntity.WikiPageId;
                                        OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);

                                        linkText = "<span>[[" + page.Title + "|" + linkNodes[i].InnerHtml + "]]</span>";
                                        var newNode = HtmlNode.CreateNode(linkText);
                                        linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                                        linkNodes.Remove(linkNodes[i]);
                                    }
                                }
                            }

                        }
                        else
                        {
                            if (url.Contains('#'))
                            {
                                linkHash = url.Split('#');

                                string id = "DocGUID:" + linkHash[0];

                                using (var db = new OmnicellEntities())
                                {
                                    WikiPageMetaData dbEntity = null;

                                    dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.ContentId));

                                    if (dbEntity != null)
                                    {
                                        int wikiPageId = (int)dbEntity.WikiPageId;
                                        OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);

                                        linkText = "<span>[[" + page.Title + "|" + linkNodes[i].InnerHtml + "]]</span>";
                                        var newNode = HtmlNode.CreateNode(linkText);
                                        linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                                        linkNodes.Remove(linkNodes[i]);
                                    }
                                }
                            }
                            if (url.Contains('#'))
                            {
                                linkHash = url.Split('#');

                                string id = "DocGUID:" + linkHash[0];

                                using (var db = new OmnicellEntities())
                                {
                                    WikiPageMetaData dbEntity = null;

                                    dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.ContentId));

                                    if (dbEntity != null)
                                    {
                                        int wikiPageId = (int)dbEntity.WikiPageId;
                                        OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);

                                        linkText = "<span>[[" + page.Title + "|" + linkNodes[i].InnerHtml + "]]</span>";
                                        var newNode = HtmlNode.CreateNode(linkText);
                                        linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                                        linkNodes.Remove(linkNodes[i]);
                                    }
                                }
                            }
                        }
                     }  
                }
                else 
                {
                    string url = linkNodes[i].InnerHtml;
                    string id = "DocGUID:" + url;
                    using (var db = new OmnicellEntities())
                    {
                        WikiPageMetaData dbEntity = null;

                        dbEntity = db.WikiPageMetaDatas.FirstOrDefault(x => id.Contains(x.ContentId));

                        if (dbEntity != null)
                        {
                            int wikiPageId = (int)dbEntity.WikiPageId;
                            OmnicellWikiPage page = new OmnicellWikiPage(wikiPageId);

                            linkText = "<span>[[" + page.Title + "|" + linkNodes[i].InnerHtml + "]]</span>";
                            var newNode = HtmlNode.CreateNode(linkText);
                            linkNodes[i].ParentNode.ReplaceChild(newNode, linkNodes[i]);
                            linkNodes.Remove(linkNodes[i]);
                        }
                    }
                }
            }

            return node;
        }

        private void GetTargetGroup(string strGroupMap, string metaData, out int groupId, out string targetGroup)
        {
            groupId = 0;
            targetGroup = String.Empty;
            try
            {
                // split the string and find the match.
                var keyValuePairs = strGroupMap.Split(';')
                    .Select(x => x.Split('='))
                    .ToDictionary(x => x.First(), x => x.Last());


                foreach (KeyValuePair<string, string> entry in keyValuePairs)
                {
                    int n = metaData.ToLower().IndexOf(entry.Key.ToLower());

                    if (n != -1)
                    {
                        groupId = Convert.ToInt32(entry.Value);
                        targetGroup = entry.Key;
                        break;
                    }

                }
            }
            catch (Exception ex)
            {

                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving media gallery id with target group: {0}", strGroupMap), ex).Log();
            }

        }

        // methods to save the files in the zip stream to a temp folder
        private Dictionary<string, MemoryStream> ExtractFiles(ZipInputStream ziStream)
        {
            // this should lead to the job scheduler folder
            var result = new Dictionary<string, MemoryStream>();
            ZipEntry theEntry;

            try
            {

                while ((theEntry = ziStream.GetNextEntry()) != null)
                {
                    if (!theEntry.IsFile)
                    {
                        continue;
                    }

                    MemoryStream stream = new MemoryStream();
                    
                    byte[] data = new byte[theEntry.Size];

                    int size = ziStream.Read(data, 0, data.Length);

                    if (size > 0)
                    {
                        string[] name = theEntry.Name.Split('/');
                        stream.Write(data, 0, size);
                        if (name.Length > 1)
                        {
                            result.Add(name[1], stream);
                        }
                        else
                        {
                            result.Add(name[0], stream);
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ziStream.Close();
                ziStream.Dispose();
            }

            return result;
        }

        private string RetrieveDocId(string docLink, Dictionary<string, MemoryStream> files)
        {
            int index = docLink.IndexOf("#");

            if (index != -1)
            {
                docLink = docLink.Remove(index);
            }
            HtmlDocument pageDoc = new HtmlDocument();
            files[docLink].Position = 0;
            pageDoc.Load(files[docLink], Encoding.UTF8);
            string description = String.Empty;
            string Id = String.Empty;

            IEnumerable<HtmlNode> collection = pageDoc.DocumentNode.Descendants("meta").Where(a => a.Attributes.Contains("name"));
            foreach (var meta in collection)
            {
                string nodeName = meta.Attributes["name"].Value;

                switch (nodeName)
                {

                    case "DC.Identifier": Id = meta.Attributes["content"].Value; break;
                }

            }
            Id = "DocGUID:" + Id;
            documentIds.Add(Id);
            return Id;

        }

        private int GetGroupID(string strGroupMap, string strGroup)
        {
            int MGId = 0;
            try
            {
                // split the string and find the match.
                var keyValuePairs = strGroupMap.Split(';')
                    .Select(x => x.Split('='))
                    .ToDictionary(x => x.First(), x => x.Last());


                if (keyValuePairs.ContainsKey(strGroup.ToUpper()))
                {
                    MGId = Convert.ToInt32(keyValuePairs[strGroup.ToUpper()].ToString());
                }

            }
            catch (Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error retrieving media gallery id with target group: {0}", strGroupMap), ex).Log();
            }
            return MGId;
        }


        private string[] GetMediaGalleryList(string strMap)
        {
            string[] strReturn;
            var keyValuePairs = strMap.Split(';')
                .Select(x => x.Split('='))
                .ToDictionary(x => x.First(), x => x.Last());

            strReturn = keyValuePairs.Keys.ToArray();

            return strReturn;
        }

        private string[] GetSPLibraryList(string strMap)
        {
            string[] strReturn;
            var keyValuePairs = strMap.Split(';')
                .Select(x => x.Split('='))
                .ToDictionary(x => x.First(), x => x.Last());

            strReturn = keyValuePairs.Values.ToArray();

            return strReturn;
        }

        public string GetFileNameFromUrl(string url)
        {
            try
            {
                string[] fileUrl = url.Split('/');
                return fileUrl[fileUrl.Count() - 1];
            }
            catch (Exception)
            {

            }

            return String.Empty;
        }

        private static object _runAsUserLock = new object();
        public void RunAsUser(Action a, Telligent.Evolution.Components.IExecutionContext context, Telligent.Evolution.Components.User u)
        {
            lock (_runAsUserLock)
            {
                var originalUser = context.User;
                try
                {
                    context.User = u;
                    a();
                }
                finally
                {
                    context.User = originalUser;
                }
            }
        }
    }
}

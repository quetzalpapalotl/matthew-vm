﻿using Omnicell.Custom.Data;
using Telligent.Caching;
using Telligent.Evolution.Components;

namespace Omnicell.Custom.Components
{
    public interface IRelatedDocumentsService
    {
        RelatedDocuments Get(int groupId);
        RelatedDocuments Save(RelatedDocuments relatedDocuments);

    }

    public class RelatedDocumentsService : IRelatedDocumentsService
    {
        public readonly ICacheService _cache = null;

        public RelatedDocumentsService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
        }

        public RelatedDocuments Get(int groupId)
        {
            RelatedDocuments rd = GetRelatedDocumentsFromCache(groupId);
            if (rd == null)
            {
                rd = RelatedDocumentsDataProvider.Instance.Get(groupId);

                if (rd != null)
                    PushRelatedDocumentsToCache(rd);
            }
            return rd;
        }

        public RelatedDocuments Save(RelatedDocuments rd)
        {
            RemoveRelatedDocumentsFromCache(rd.GroupId);
            RelatedDocumentsDataProvider.Instance.Save(rd);

            return Get(rd.GroupId);
        }


        #region caching

        private string GetRelatedDocumentsCacheKey(int groupId)
        {
            return string.Format("PK_RELATED_DOCUMENTS::{0}", groupId);
        }
        private string GetRelatedDocumentsTag(int groupId)
        {
            return string.Format("TAG:RELATED_DOCUMENTS:ID:{0}", groupId);
        }

        private RelatedDocuments GetRelatedDocumentsFromCache(int groupId)
        {
            return _cache.Get(GetRelatedDocumentsCacheKey(groupId), CacheScope.All) as RelatedDocuments;
        }

        private void PushRelatedDocumentsToCache(RelatedDocuments rd)
        {
            _cache.Put(GetRelatedDocumentsCacheKey(rd.GroupId), rd, CacheScope.All);

            string[] idTag = new string[] { GetRelatedDocumentsTag(rd.GroupId) };
            _cache.Put(GetRelatedDocumentsCacheKey(rd.GroupId), rd, CacheScope.All, idTag);
        }

        private void RemoveRelatedDocumentsFromCache(int groupId)
        {
            _cache.Remove(GetRelatedDocumentsCacheKey(groupId), CacheScope.All);
            _cache.RemoveByTags(new string[] { GetRelatedDocumentsTag(groupId) }, CacheScope.All);
        }



        #endregion







    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml.Linq;
using ICSharpCode.SharpZipLib.Zip;
using Microsoft.SharePoint.Client;
using Telligent.Evolution.Components;
using Telligent.Evolution.Components.Search;
using Telligent.Evolution.Extensibility.Api.Version1;
using SPApi = Telligent.Evolution.Extensions.SharePoint.Client.Api.Version1;
using SPClient = Telligent.Evolution.Extensions.SharePoint.Client;
using SPExtv1 = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;
using TEntities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TExV1 = Telligent.Evolution.Extensibility.Version1;
using V1Entities = Telligent.Evolution.Extensibility.Api.Entities.Version1;

namespace Omnicell.Custom.Components
{
    public interface ISharepointService
    {
        SPExtv1.PagedList<SPApi.Document> GetSharepointLibrary(string title);
        DateTime GetWebCreatedDate();
        SPExtv1.PagedList<SPApi.Library> GetLibraryList(int groupID);
        void CreateUpdateDocument(Guid contentId);
        void DeleteDocument(Guid contentId);
        bool BatchCreate();
        bool testCreate(int groupID, string strDocName);
        bool testDelete(int galleryID, string strDocName);
    }

    public class SharepointService : ISharepointService
    {
        private enum enumAudience {
            Pharmacy = 1,
            Perioperative = 3,
            Nursing = 4,
            [IODescription("Supply Chain/Materials Management")]
            SupplyChain = 5,
            [IODescription("Long-Term Care")]
            LongTermCare = 6,
            [IODescription("Healthcare Executives")]
            HealthCareExecutive = 7,
            BioMed = 8,
            [IODescription("Information Technology")]
            InformationTechnology = 10
        }

        private readonly ICacheService _cache = null;
        private readonly OmnicellSharepointPlugin _osp = null;
        private OmnicellSharepointPlugin _plgnSP = null;

        public SharepointService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
            _osp = Telligent.Evolution.Extensibility.Version1.PluginManager.Get<OmnicellSharepointPlugin>().FirstOrDefault();
            _plgnSP = TExV1.PluginManager.Get<OmnicellSharepointPlugin>().FirstOrDefault();
        }

        public SPExtv1.PagedList<SPApi.Library> GetLibraryList(int groupID)
        {
            SPApi.LibraryListOptions options = new SPApi.LibraryListOptions();
            options.PageIndex = 0;
            options.PageSize = 5;
            SPExtv1.PagedList<SPApi.Library> lstLibraries = SPApi.PublicApi.Libraries.List(groupID, options);

            return lstLibraries;
        }
        public SPExtv1.PagedList<SPApi.Document> GetSharepointLibrary(string guidID)
        {
            // connect to Sharepoint and get a list of the libraries
            SPClient.version2.SharePointLibrary spLib2 = new SPClient.version2.SharePointLibrary();
            SPExtv1.PagedList<SPApi.Document> lstDocs = new SPExtv1.PagedList<SPApi.Document>();

            SPApi.Library splib = new SPApi.Library();
            try
            {
                // convert the library ID to GUID
                Guid LibIDGuid = new Guid(guidID);
                splib = spLib2.Get(LibIDGuid);
                Guid AppGuidID = new Guid(splib.ApplicationId.ToString());
                // this library has a groupID, get all the files associated with the group ID
                int igroupID = splib.GroupId;
                SPApi.DocumentListOptions options = new SPApi.DocumentListOptions();
                options.FolderPath = splib.Root;  // /Repository/Publications /?
                options.PageSize = 5000;
                lstDocs = SPApi.PublicApi.Documents.List(LibIDGuid, options);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message);
            }

            return lstDocs;
        }
        public DateTime GetWebCreatedDate()
        {
            using (ClientContext context = new ClientContext(_osp.SPFullUrl))
            {
                Web web = context.Web;
                context.Load(web, w => w.Created);
                context.ExecuteQuery();
                return web.Created;
            }
        }
        public ZipInputStream GetRemoteZipStream(string remoteUrl, string domain, string userName, string passWord)
        {
            ZipInputStream zistream;
            WebResponse response = null;
            WebRequest request = WebRequest.Create(remoteUrl);
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(userName + ":" + passWord));

            request.PreAuthenticate = true;
            request.Credentials = new NetworkCredential(userName, passWord, domain);

            if (request != null)
            {
                try
                {
                    response = request.GetResponse();
                    if (response != null)
                    {
                        zistream = new ZipInputStream(response.GetResponseStream());
                        return zistream;
                    }
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;

        }
        // method to get a stream of the file from sharepoint when a url is provided
        public Stream GetRemoteFileStream(string remoteUrl)
        {
            WebResponse response = null;
            WebRequest request = WebRequest.Create(remoteUrl);
            if (request != null)
            {
                try
                {
                    response = request.GetResponse();
                    return (response != null) ? response.GetResponseStream() : null;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;
        }

        // method to get a stream of the file from sharepoint when a url is provided along with credentials.
        public Stream GetRemoteFileStream(string remoteUrl, string domain, string userName, string passWord)
        {
            WebResponse response = null;
            WebRequest request = WebRequest.Create(remoteUrl);
            String encoded = System.Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(userName + ":" + passWord));

            request.PreAuthenticate = true;
            request.Credentials = new NetworkCredential(userName, passWord, domain);
            if (request != null)
            {
                try
                {
                    response = request.GetResponse();
                    return (response != null) ? response.GetResponseStream() : null;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
            return null;
        }
        // function to convert utf8 to utf16
        public string ConvertUTF16(string value)
        {
            
            char[] chars = HttpUtility.HtmlEncode(value).ToCharArray();
            StringBuilder sb = new StringBuilder( value.Length + (int)(value.Length * 0.1));

            foreach (char c in chars)
            {
                int cValue = Convert.ToInt32(c);
                if(cValue > 127) 
                {
                    sb.AppendFormat("&#{0};", cValue);
                }
                else 
                {
                    sb.Append(c);
                }

            }
           
            return sb.ToString();
        }

        public SharepointDITA.DitaDocMetaData ExtractMetaDataFromZipStream(ZipInputStream ziStream, string targetFileName)
        {
            SharepointDITA.DitaDocMetaData ddmd = new SharepointDITA.DitaDocMetaData();
            ZipEntry theEntry;
            while ((theEntry = ziStream.GetNextEntry()) != null)
            {
                // if the file has a name, attempt to process it
                if (theEntry.Name.Trim().Length > 1)
                {
                    try
                    {
                        // in case we have badly formed XML
                        XDocument xml = XDocument.Load(ziStream);
                        IEnumerable<XElement> coreProps = (from cp in xml.Descendants(XNamespace.Get("http://schemas.openxmlformats.org/package/2006/metadata/core-properties") + "coreProperties").Descendants() select cp);
                        foreach (XElement cp in coreProps)
                        {
                            // ddmd.CoreProperties.Title += cp.Name.LocalName + "=" + cp.Value + "<br />";
                            switch (cp.Name.LocalName)
                            {
                                case "title": ddmd.CoreProperties.Title = cp.Value; break;
                                case "subject": ddmd.CoreProperties.Subject = cp.Value; break;
                                case "creator": ddmd.CoreProperties.Creator = cp.Value; break;
                                case "description": ddmd.CoreProperties.Description = cp.Value; break;
                                case "lastModifiedBy": ddmd.CoreProperties.LastModifiedBy = cp.Value; break;
                                case "revision": ddmd.CoreProperties.Revision = cp.Value; break;
                                case "lastPrinted": ddmd.CoreProperties.LastPrinted = cp.Value; break;
                                case "created": ddmd.CoreProperties.Created = cp.Value; break;
                                case "modified": ddmd.CoreProperties.Modified = cp.Value; break;
                                default: break;
                            }
                        }
                        IEnumerable<XElement> custProps = (from p in xml
                                                               .Descendants(XNamespace.Get("http://schemas.openxmlformats.org/officeDocument/2006/custom-properties") + "Properties")
                                                               .Descendants(XNamespace.Get("http://schemas.openxmlformats.org/officeDocument/2006/custom-properties") + "property")
                                                           select p);
                        foreach (XElement p in custProps)
                        {
                            // In Case Attribute is missing
                            try
                            {
                               string value = p.Value.Replace('\uFF06', '&').Replace('\uFF09', ')').Replace('\uFF08', '(');
                                switch (p.Attribute("name").Value)
                                {
                                    case "dx_id": ddmd.CustomProperties.Id = p.Value; break;
                                    case "dx_product": ddmd.CustomProperties.Product = p.Value; break;
                                    case "ProductVersion": ddmd.CustomProperties.ProductVersion = p.Value; break;
                                    case "Interfaces": ddmd.CustomProperties.Interfaces = value; break;
                                    case "OmniFeatures": ddmd.CustomProperties.Features = value; break;
                                    case "Audience": ddmd.CustomProperties.Audience = value; break;
                                    case "OmniDocumentType": ddmd.CustomProperties.DocumentType = p.Value; break;
                                    default: break;
                                }
                            }
                            catch (Exception ex)
                            {
                                WriteMessageToFile(DateTime.Now.ToString() + " - ERROR: ExtractMetaDataFromZipStream:PROC. Message: " + ex.Message);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        // handle the error, go to the next file?
                        WriteMessageToFile(DateTime.Now.ToString() + " - ERROR: ExtractMetaDataFromZipStream:PROC. Message: " + ex.Message);
                    }
                } // if entry match
                // if we have data then break out, otherwise, go to the next entry.
                if (ddmd.CoreProperties.Title != null)
                    break;
            } // ziStream next entry
            return ddmd;
        } // end of method
        //
        // Get list of documents in the SP library
        //
        public SPExtv1.PagedList<SPApi.Document> GetLibraryListing(SPApi.Document objDocument)
        {
            SPExtv1.PagedList<SPApi.Document> lstDocs = new SPExtv1.PagedList<SPApi.Document>();
            // get the applicaiton/library ID for the document that was passed in.
            lstDocs = GetSharepointLibrary(objDocument.Application.ApplicationId.ToString());
            return lstDocs;
        } // end of GetLibraryListing
        //
        // Get Zip where name contains N
        //
        public SPApi.Document GetCorrespondingZip(SPApi.Document objDocument)
        {
            SPApi.Document zipDoc = new SPApi.Document();
            SPExtv1.PagedList<SPApi.Document> lstDocs = new SPExtv1.PagedList<SPApi.Document>();
            lstDocs = GetSharepointLibrary(objDocument.Application.ApplicationId.ToString());
            // find the document with the same name but of type zip in this library
            foreach (SPApi.Document doc in lstDocs)
            {
                if (doc.DisplayName.Contains(objDocument.DisplayName.Replace(".PDF", "").ToString())) 
                {
                    zipDoc = doc;
                }
            }
            return zipDoc;
        } // end of get zip where name contains N
        //
        // function to see if this document already exists in a media gallery
        //
        public bool FileAlreadyExists(SPApi.Document objDocument)
        {
            bool FileExists = false;

            SearchResultsListOptions searchOptions = new SearchResultsListOptions();
            //             searchOptions.Tags = options["Tags"].ToString();
            List<String> lstTags = new List<String>();
            lstTags.Add("docid:" + objDocument.Id.ToString());
            searchOptions.Tags = lstTags;
            V1Entities.SearchResults results = PublicApi.Search.List(searchOptions);

            if (results.Count > 0)
                FileExists = true;

            return FileExists;
        }

        public string GetFileNameFromUrl(string url)
        {
            try
            {
                string[] fileUrl = url.Split('/');
                return fileUrl[fileUrl.Count() - 1];
            }
            catch (Exception ex)
            {
            }

            return String.Empty;
        }
        public void DeleteDocument(Guid contentId)
        {
            
            // get the document from the sharepoint library
            SPApi.Document spPDFdoc = SPApi.PublicApi.Documents.Get(contentId);
            // ge the ID of the file from the media gallery and then pass to the function to delete.
            int fileID = -1;
            int MediaGalleryId = GetMediaGalleryID(_plgnSP.DLMGMaps,spPDFdoc.Library.Id.ToString());
            fileID = MediaGalleryDocExists(MediaGalleryId, contentId);
            if (fileID > -1)
                DeleteMediaGalleryDocument(fileID);
        }
        //
        // Creates or Updates Document depending of if Document exists
        //
        public void CreateUpdateDocument(Guid contentId)
        {
            WriteMessageToFile(DateTime.Now.ToString() + " - CreateUpdateDocument:START - ContentId: " + contentId.ToString());
            // get the document from the sharepoint library
            SPApi.Document spPDFdoc = SPApi.PublicApi.Documents.Get(contentId);
            string strDocName = "";
            strDocName = GetFileNameFromUrl(spPDFdoc.Path);
            if (!String.IsNullOrEmpty(strDocName) && strDocName.ToLower().Contains(".pdf"))
            {
                try
                {
                    string zipDocName = strDocName.ToLower().Replace(".pdf", ".zip").ToString();
                    WriteMessageToFile(DateTime.Now.ToString() + " - CreateUpdateDocument:PROC - zipDocName: " + zipDocName.ToString());

                    SPApi.Document spZIPdoc = new SPApi.Document();
                    //SPApi.Document spPDFdoc = new SPApi.Document();
                    SPExtv1.PagedList<SPApi.Library> spliblist = new SPExtv1.PagedList<SPApi.Library>();
                    SPExtv1.PagedList<SPApi.Document> spdoclist = new SPExtv1.PagedList<SPApi.Document>();
                    spdoclist = GetSharepointLibrary(spPDFdoc.Library.Id.ToString());
                    WriteMessageToFile(DateTime.Now.ToString() + " - CreateUpdateDocument:PROC - Got SharePoint List.");
                    foreach (SPApi.Document document in spdoclist)
                    {
                        if (document.Name.ToLower() == zipDocName.ToLower())
                        {
                            spZIPdoc = document;
                            break;
                        }
                    }
                    if (spZIPdoc != null)
                    {
                        WriteMessageToFile(DateTime.Now.ToString() + " - CreateUpdateDocument:PROC - spZipDoc is not null.");
                        if (spZIPdoc.Path != "" && spPDFdoc.Path != "")
                        {
                            WriteMessageToFile(DateTime.Now.ToString() + " - CreateUpdateDocument:PROC - Zip File Name Not Blank.");
                            // Stream strmfiledata = GetRemoteFileStream("http://dev.myomnicell.com/" + spPDFdoc.Url);
                            // ZipInputStream strmzipfile = GetRemoteZipStream("http://dev.myomnicell.com/" + spZIPdoc.Url);
                            string strSharepointRootURL = _plgnSP.SPFullUrl;

                            Stream strmfiledata = GetRemoteFileStream(strSharepointRootURL.ToString() + spPDFdoc.Path, _plgnSP.SPDomain, _plgnSP.SPLogin, _plgnSP.SPPassword);
                            ZipInputStream strmzipfile = GetRemoteZipStream(strSharepointRootURL.ToString() + spZIPdoc.Path, _plgnSP.SPDomain, _plgnSP.SPLogin, _plgnSP.SPPassword);
                            string strTargetFileName = System.Web.HttpUtility.UrlDecode(strDocName).ToLower().Replace(".pdf", ".xml");
                            // if this doc exists, the update, otherwise create
                            int fileID = -1;
                            int MediaGalleryID = GetMediaGalleryID(_plgnSP.DLMGMaps, spPDFdoc.Library.Id.ToString());
                            fileID = MediaGalleryDocExists(MediaGalleryID, contentId);
                            WriteMessageToFile(DateTime.Now.ToString() + " - CreateUpdateDocument:PROC - Before Getting FileId: " + fileID.ToString());
                            if (fileID > 0)
                            {
                                UpdateMediaGalleryDocument(MediaGalleryID, strmfiledata, ExtractMetaDataFromZipStream(strmzipfile, strTargetFileName), spPDFdoc, contentId, fileID);
                            }
                            else
                            {
                                CreateMediaGalleryDocument(MediaGalleryID, strmfiledata, ExtractMetaDataFromZipStream(strmzipfile, strTargetFileName), spPDFdoc, contentId);
                            }
                            WriteMessageToFile(DateTime.Now.ToString() + " - CreateUpdateDocument:PROC - After Write.");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error while reading files from Sharepoint. Message: " + ex.Message);
                }
            }


        }

        // Create Media Gallery Document
        public bool CreateMediaGalleryDocument(int TargetGalleryId, Stream filedata, SharepointDITA.DitaDocMetaData propsDITA, SPApi.Document objDocument, Guid contentId)
        {
            bool actionstatus = false;
            string strFileDisplayName = objDocument.DisplayName.ToString();
            try
            {
                TEntities.Gallery toGallery = TApi.PublicApi.Galleries.Get(new TApi.GalleriesGetOptions { Id = TargetGalleryId });
                if (toGallery != null && toGallery.Id == TargetGalleryId)
                {
                    if (strFileDisplayName.Substring(objDocument.DisplayName.Length - 4).ToLower() != ".pdf")
                        strFileDisplayName += ".pdf";
                    // objDocument.ContentTypeId
                    WriteMessageToFile(DateTime.Now.ToString() + " - CreateMediaGalleryDocument:PROC. Before Create.");
                    CreateDocument(TargetGalleryId, propsDITA, "2", strFileDisplayName.ToString(), filedata, contentId);
                    WriteMessageToFile(DateTime.Now.ToString() + " - CreateMediaGalleryDocument:PROC. After Create.");
                }
                actionstatus = true;
            }
            catch (Exception ex)
            {
                // write to log?
                WriteMessageToFile(DateTime.Now.ToString() + " - ERROR: CreateMediaGalleryDocument:PROC. Message: " + ex.Message);
            }
            return actionstatus;
        }

        // this method is for the OmnicellDocument Type both create and update
        private OmnicellMediaOptions updateOmnicellMediaOptions(SharepointDITA.DitaDocMetaData propsDITA, int fileId)
        {
            SharepointDITA.ParseMetaData pmeta = new SharepointDITA.ParseMetaData();

            int docType = 0;
            string audienceTypes = string.Empty;
            string productFilters = string.Empty;
            DateTime publishDate = DateTime.Now;
            string productVersion = string.Empty;
            string features = string.Empty;
            string interfaces = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.DocumentType))
                {
                    // attempt to get document type id from the sharepoint metadata fields.
                    docType = pmeta.ParseDocumentId(propsDITA.CustomProperties.DocumentType);
                }
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Audience))
                    audienceTypes = TranslateAudienceTypes(propsDITA.CustomProperties.Audience);
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Product))
                    productFilters = TranslateProductsGroup(propsDITA.CustomProperties.Product);

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.ProductVersion))
                    productVersion = propsDITA.CustomProperties.ProductVersion;
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Features))
                    features = propsDITA.CustomProperties.Features.Replace(";", ",");
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Interfaces))
                    interfaces = propsDITA.CustomProperties.Interfaces.Replace(";", ",");

            }
            catch (Exception ex)
            {
                WriteMessageToFile(DateTime.Now.ToString() + " - updateMediaCreateOptions:ERROR. Creating. Message: " + ex.Message);
            }
            
              OmnicellMediaOptions omnicellOptions = new OmnicellMediaOptions
                {
                    PostId = fileId,
                    DocumentTypeId = docType,
                    AudienceTypeIdList = audienceTypes,
                    ProductIdList = productFilters,
                    PublishDate = publishDate,
                    Versions = productVersion,
                    FeatureList = features,
                    Interfaces = interfaces
                };
           
            return omnicellOptions;

        }
        private MediaUpdateOptions updateMediaCreateOptions(SharepointDITA.DitaDocMetaData propsDITA, Stream stream, Guid contentId, SPApi.Document objDocument)
        {
            SharepointDITA.ParseMetaData pmeta = new SharepointDITA.ParseMetaData();

            MediaUpdateOptions updateOptions = new MediaUpdateOptions
            {
                Description = propsDITA.CoreProperties.Description,
                IsFeatured = false,
                FileData = (byte[])stream.ToByteArray(),
                ContentType = "2",
                FileName = objDocument.DisplayName + ".pdf"
            };
            try
            {
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.DocumentType))
                {
                  // attempt to get document type id from the sharepoint metadata fields.
                   int docType = pmeta.ParseDocumentId(propsDITA.CustomProperties.DocumentType);
                   // propsDITA.CustomProperties.DocumentType.ToString();
                   updateOptions.DocumentTypeId(docType.ToString());
                }
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Audience))
                    updateOptions.AudienceTypes(TranslateAudienceTypes(propsDITA.CustomProperties.Audience));
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Product))
                    updateOptions.ProductFilters(TranslateProductsGroup(propsDITA.CustomProperties.Product));
                // use current date/ time
                updateOptions.PublishDate(DateTime.Now);

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.ProductVersion))
                    updateOptions.Versions(propsDITA.CustomProperties.ProductVersion);
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Features))
                    updateOptions.FeatureList(propsDITA.CustomProperties.Features.Replace(";", ","));
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Interfaces))
                    updateOptions.Interfaces(propsDITA.CustomProperties.Interfaces.Replace(";", ","));
            }
            catch (Exception ex)
            {
                WriteMessageToFile(DateTime.Now.ToString() + " - updateMediaCreateOptions:ERROR. Creating. Message: " + ex.Message);
            }
            return updateOptions;
        }

        private MediaCreateOptions createMediaCreateOptions(SharepointDITA.DitaDocMetaData propsDITA, Stream stream, Guid contentId)
        {
            SharepointDITA.ParseMetaData pmeta = new SharepointDITA.ParseMetaData();

            MediaCreateOptions createOptions = new MediaCreateOptions
            {
                Description = propsDITA.CoreProperties.Description,
                IsFeatured = false,
                FileData = stream.ToByteArray(),
                Tags = "DocGUID:" + contentId.ToString()
            };
            try
            {
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.DocumentType))
                {
                    int docType = pmeta.ParseDocumentId(propsDITA.CustomProperties.DocumentType);
                   //  createOptions.DocumentTypeId(propsDITA.CustomProperties.DocumentType.ToString());
                   createOptions.DocumentTypeId(docType.ToString());
                }
                     
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Audience))
                    createOptions.AudienceTypes(TranslateAudienceTypes(propsDITA.CustomProperties.Audience));
                   //  createOptions.AudienceTypes(propsDITA.CustomProperties.Audience,";");
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Product))
                   createOptions.ProductFilters(TranslateProductsGroup(propsDITA.CustomProperties.Product));
                    // createOptions.ProductFilters(propsDITA.CustomProperties.Product, ";");
                // use current date/ time
                createOptions.PublishDate(DateTime.Now);

                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.ProductVersion))
                    createOptions.Versions(propsDITA.CustomProperties.ProductVersion);
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Features))
                    createOptions.FeatureList(propsDITA.CustomProperties.Features);
                if (!string.IsNullOrEmpty(propsDITA.CustomProperties.Interfaces))
                    createOptions.Interfaces(propsDITA.CustomProperties.Interfaces);
            }
            catch (Exception ex)
            {
                WriteMessageToFile(DateTime.Now.ToString() + " - createMediaCreateOptions:ERROR. Creating. Message: " + ex.Message);
            }
            return createOptions;
        }
        private void CreateDocument(int TargetGalleryID, SharepointDITA.DitaDocMetaData propsDITA, string strContentType, string strDisplayName, Stream stream, Guid contentId)
        {
            try
            {
                // testing to see if caching is the issue.
                IDocumentService _svc = Telligent.Common.Services.Get<IDocumentService>();
                MediaCreateOptions createOptions = createMediaCreateOptions(propsDITA, stream, contentId);
               var media = TApi.PublicApi.Media.Create(TargetGalleryID, propsDITA.CoreProperties.Title, "2", strDisplayName, createOptions);

                OmnicellDocumentPost post = new OmnicellDocumentPost();
                OmnicellMediaOptions omnicellOptions = updateOmnicellMediaOptions(propsDITA, media.Id.Value);
                post = _svc.Save(omnicellOptions);

            }
            catch (Exception ex)
            {
                WriteMessageToFile(DateTime.Now.ToString() + " - CreateDocument:ERROR. Creating. Message: " + ex.Message);
            }
        }
        // update media gallery document
        public bool UpdateMediaGalleryDocument(int TargetGalleryId, Stream filedata, SharepointDITA.DitaDocMetaData propsDITA, SPApi.Document objDocument, Guid contentId, int fileID)
        {
            bool actionstatus = false;
           
            TEntities.Gallery toGallery = TApi.PublicApi.Galleries.Get(new TApi.GalleriesGetOptions { Id = TargetGalleryId });

            if (toGallery != null && toGallery.Id == TargetGalleryId)
            {
                try
                {

                    UpdateDocument(TargetGalleryId, fileID, propsDITA, filedata, contentId, objDocument);
                    actionstatus = true;
                }
                catch (Exception ex)
                {
                    // write to log
                    WriteMessageToFile(DateTime.Now.ToString() + " - UpdateMediaGalleryDocument:ERROR. Creating. Message: " + ex.Message);
                }
            }
            return actionstatus;
        }
        private void UpdateDocument(int TargetGalleryID, int fileID, SharepointDITA.DitaDocMetaData propsDITA, Stream fs, Guid contentId, SPApi.Document objDocument)
        {
            try
            {
                // testing to see if caching is the issue.
                IDocumentService _svc = Telligent.Common.Services.Get<IDocumentService>();

                MediaUpdateOptions updateOptions = updateMediaCreateOptions(propsDITA, fs, contentId, objDocument);
                OmnicellDocumentPost post = new OmnicellDocumentPost();

                var media = PublicApi.Media.Update(TargetGalleryID, fileID, updateOptions);
                OmnicellMediaOptions omnicellOptions = updateOmnicellMediaOptions(propsDITA, media.Id.Value);

                post = _svc.Save(omnicellOptions);

            }
            catch (Exception ex)
            {
                WriteMessageToFile(DateTime.Now.ToString() + " - UpdateDocument:ERROR. Updating. Message: " + ex.Message);
            }
        }
        // delete media gallery document
        public bool DeleteMediaGalleryDocument(int fileID)
        {
            bool actionstatus = false;
            // check to make sure the doc exists
            try
            {
                if (fileID != null && fileID > 0)
                {
                    // actually delete the item
                    TApi.PublicApi.Media.Delete(fileID);
                }
                actionstatus = true;
            }
            catch (Exception ex)
            {
                WriteMessageToFile(DateTime.Now.ToString() + " - DeleteMediaGalleryDocument:ERROR. Message: " + ex.Message);
                actionstatus = false;
            }
            return actionstatus;
        }
        private void DeleteDocument(int fileID)
        {
            try
            {
                TEntities.AdditionalInfo additionalInfo = new TEntities.AdditionalInfo();
                additionalInfo = TApi.PublicApi.Media.Delete(fileID);
                // call SOLR? - Tthe PublicApi should handle it for us
            }
            catch (Exception ex)
            {
                WriteMessageToFile(DateTime.Now.ToString() + " - DeleteDocument:ERROR. Message: " + ex.Message);
            }
        }

        private int MediaGalleryDocExists(int mediaGalleryId, Guid contentId)
        {
            int fileID = -1;
            try
            {
                // get a list of docs in the media gallery. and see if this one exists.
                List<FieldFilter> fieldFilters = new List<FieldFilter>
            { 
                new FieldFilter { Id = mediaGalleryId, FieldName = SearchFields.SectionID, FieldValue = mediaGalleryId.ToString() }
            };
                List<String> lstTags = new List<String>();
                lstTags.Add("DocGUID:" + contentId.ToString());

                SearchResultsListOptions searchOptions = new SearchResultsListOptions
                {
                    PageIndex = 0,
                    PageSize = 10000,
                    FieldFilters = fieldFilters,
                    Tags = lstTags
                };
                V1Entities.SearchResults results = PublicApi.Search.List(searchOptions);
                if (results.Count > 0)
                    fileID = Convert.ToInt32(results[0].ContentId);
            }
            catch (Exception ex)
            {
                WriteMessageToFile(DateTime.Now.ToString() + " - MediaGalleryDocExists:ERROR. Message: " + ex.Message);
            }
            return fileID;
        }
        private int GetMediaGalleryID(string strLibraryMap, string strSPLibrary)
        {
            int MGId = 0;
            try
            {
                // split the string and find the match.
                var keyValuePairs = strLibraryMap.Split(';')
                    .Select(x => x.Split('='))
                    .ToDictionary(x => x.First(), x => x.Last());

                //            for (int i = 0; i < keyValuePairs.Count; i++)
                //            {
                if (keyValuePairs.ContainsKey(strSPLibrary.ToUpper()))
                {
                    MGId = Convert.ToInt32(keyValuePairs[strSPLibrary.ToUpper()].ToString());
                }
                //            }
            }
            catch (Exception ex)
            {
                WriteMessageToFile(DateTime.Now.ToString() + " - GetMediaGalleryID:ERROR. Message: " + ex.Message);
            }
            return MGId;
        }

        private string TranslateAudienceTypes(string strAudiences)
        {
            string[] lstAudience = strAudiences.Split(';');
            string strReturnValue = "";
            foreach (String audience in lstAudience)
            {
                strReturnValue += ConvertAudienceTypes(audience) + ",";
            }
            return strReturnValue.TrimEnd(',');
        }
        private string ConvertAudienceTypes(string strSharePointAudince)
        {
            IAudienceTypeService _svc = Telligent.Common.Services.Get<IAudienceTypeService>();

            IList<AudienceType> auType = _svc.List();
            string audience = string.Empty;

            foreach (AudienceType type in auType)
            {
                if(type.Value.Equals(strSharePointAudince)) 
                {
                    audience = type.Id.ToString();
                }
            }

            return audience;
        }

        private string TranslateProductsGroup(string strProducts)
        {
            string[] lstProducts = strProducts.Split(';');
            string strReturnValue = "";
            foreach (string product in lstProducts)
            {
                strReturnValue += ConvertProductsGroup(product) + ",";
            }
            return strReturnValue.TrimEnd(',');
        }
        private string ConvertProductsGroup(string strProductGroup)
        {
            List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group> apiGroups =
                new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group>();
            int? groupId = 5;
            apiGroups = PublicApi.Groups.List(new GroupsListOptions() { ParentGroupId = groupId, PageSize = 100 }).ToList();

            string productId = string.Empty;

            foreach(var group in apiGroups) 
            {
                if (group.Name.Equals(strProductGroup))
                {
                    productId = group.Id.ToString();
                }
            }

            return productId;

        }

        public bool BatchCreate()
        {
            bool boolReturn = true;
            try
            {
                // get the list of libraries and mediagalleries
                string[] arrMGs = GetMediaGalleryList(_plgnSP.DLMGMaps);
                string[] arrLibs = GetSPLibraryList(_plgnSP.DLMGMaps);
                // get the list of documents in the MG and the Lib
                SPExtv1.PagedList<SPApi.Document> spdoclist = new SPExtv1.PagedList<SPApi.Document>();
                TEntities.PagedList<TEntities.Media> mgdoclist = new TEntities.PagedList<TEntities.Media>();
                TEntities.PagedList<TEntities.Media> mgDELdoclist = new TEntities.PagedList<TEntities.Media>();
                // List<String> mgDELdoclist = new List<String>();
                for (int i = 0; i < arrLibs.Length; i++)
                {
                    spdoclist = GetSharepointLibrary(arrMGs[i].ToString());
                    mgdoclist = TApi.PublicApi.Media.List(new TApi.MediaListOptions { GalleryId = Convert.ToInt32(arrLibs[i]), PageSize = int.MaxValue, PageIndex = 0 });
                    // we have two lists, compare them and call the delete on any items that are not in the MG
                    List<String> lstSPFiles = new List<String>();
                    List<String> lstMGFiles = new List<String>();
                    foreach (SPApi.Document doc in spdoclist)
                    {
                        //if (mgdoclist.Any(a => doc.Title != a.Title)) // TODO: change condition to check tags for DocID:<GUID>SPDocID
                        //{
                        CreateUpdateDocument(doc.ContentId);
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                boolReturn = false;
            }
            return boolReturn;
        }
        // function to delete any MG files that do not exist in the corresponding SP Library
        public bool BatchDelete()
        {
            bool boolReturn = true;
            try
            {
                // get the list of libraries and mediagalleries
                string[] arrMGs = GetMediaGalleryList(_plgnSP.DLMGMaps);
                string[] arrLibs = GetSPLibraryList(_plgnSP.DLMGMaps);
                // get the list of documents in the MG and the Lib
                SPExtv1.PagedList<SPApi.Document> spdoclist = new SPExtv1.PagedList<SPApi.Document>();
                TEntities.PagedList<TEntities.Media> mgdoclist = new TEntities.PagedList<TEntities.Media>();
                TEntities.PagedList<TEntities.Media> mgDELdoclist = new TEntities.PagedList<TEntities.Media>();
                // List<String> mgDELdoclist = new List<String>();
                for (int i = 0; i < arrLibs.Length; i++)
                {
                    spdoclist = GetSharepointLibrary(arrLibs[i].ToString());
                    mgdoclist = TApi.PublicApi.Media.List(new TApi.MediaListOptions { GalleryId = Convert.ToInt32(arrMGs[i]), PageSize = int.MaxValue, PageIndex = 0 });
                    // we have two lists, compare them and call the delete on any items that are not in the MG
                    List<String> lstSPFiles = new List<String>();
                    List<String> lstMGFiles = new List<String>();
                    foreach (TEntities.Media doc in mgdoclist)
                    {
                        if (spdoclist.Any(a => doc.Title != a.Title)) // TODO: change condition to check tags for DocID:<GUID>SPDocID
                        {
                            mgDELdoclist.Add(doc); // TODO: Not Used; slate for removal
                            DeleteMediaGalleryDocument(doc.Id.GetValueOrDefault());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                boolReturn = false;
            }
            return boolReturn;
        }
        private string[] GetMediaGalleryList(string strMap)
        {
            string[] strReturn;
            var keyValuePairs = strMap.Split(';')
                .Select(x => x.Split('='))
                .ToDictionary(x => x.First(), x => x.Last());

            strReturn = keyValuePairs.Keys.ToArray();
            
            return strReturn;
        }
        private string[] GetSPLibraryList(string strMap)
        {
            string[] strReturn;
            var keyValuePairs = strMap.Split(';')
                .Select(x => x.Split('='))
                .ToDictionary(x => x.First(), x => x.Last());

            strReturn = keyValuePairs.Values.ToArray();

            return strReturn;
        }
        // TEST METHODS - TODO: PROBABLY SHOULD BE HIDDEN/DELETED BEFORE RELEASE

        // LOG FILE - TODO: add debug switch in Sharepoint Plugin to enable trace logging.  Update trace log path to writable /~/custom/data/ folder.
        private void WriteMessageToFile(string strMessage)
        {
            // write trace log
            try
            {
                // string fileNameWithPath = System.Web.HttpContext.Current.Server.MapPath("SharePointConnect.txt");
                string fileNameWithPath = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath,"SharePointConnect.txt");
                using (StreamWriter sw = System.IO.File.AppendText(fileNameWithPath))
                {
                    sw.WriteLine(strMessage.ToString());
                }
            }
            catch (Exception ex)
            {
                // do something with the error
            }
        }
        // LOG FILE
        public bool testDelete(int fromGalleryId, string strDocName)
        {
            int iMGFileId = 0;
            // get the gallery list and find the matching file
            TEntities.PagedList<TEntities.Media> items = TApi.PublicApi.Media.List(new TApi.MediaListOptions { GalleryId = fromGalleryId, PageSize = int.MaxValue, PageIndex = 0 });
            foreach (TEntities.Media item in items)
            {
                if (item.Title.ToString().ToLower() == strDocName.ToLower())
                {
                    iMGFileId = item.Id.GetValueOrDefault();
                    DeleteMediaGalleryDocument(iMGFileId);
                }
            }
            return true;
        }
        public bool testUpdate(int GalleryId, string strDocName)
        {
            bool bReturnValue = false;
            string zipDocName = strDocName.Replace(".PDF", ".ZIP").ToString();
            SPApi.Document spZIPdoc = new SPApi.Document();
            SPApi.Document spPDFdoc = new SPApi.Document();
            SPExtv1.PagedList<SPApi.Library> spliblist = new SPExtv1.PagedList<SPApi.Library>();
            spliblist = GetLibraryList(GalleryId);
            foreach (SPApi.Library library in spliblist)
            {
                SPExtv1.PagedList<SPApi.Document> spdoclist = new SPExtv1.PagedList<SPApi.Document>();
                spdoclist = GetSharepointLibrary(library.Id.ToString());
                foreach (SPApi.Document document in spdoclist)
                {
                    if (document.Name.ToLower() == zipDocName.ToLower())
                    {
                        spZIPdoc = document;
                        break;
                    }
                }
                foreach (SPApi.Document document in spdoclist)
                {
                    if (document.Name.ToLower() == strDocName.ToLower())
                    {
                        spPDFdoc = document;
                        break;
                    }
                }
                if (spZIPdoc.Url != "" && spPDFdoc.Url != "")
                {
                    // Stream strmfiledata = GetRemoteFileStream("http://dev.myomnicell.com/" + spPDFdoc.Url);
                    // ZipInputStream strmzipfile = GetRemoteZipStream("http://dev.myomnicell.com/" + spZIPdoc.Url);
                    string strSharepointRootURL = "http://sp.defaktolabs.com";
                    Stream strmfiledata = GetRemoteFileStream(strSharepointRootURL.ToString() + spPDFdoc.Path, "DEFAKTOWEB004", "Administrator", "DGAdmin!1");
                    ZipInputStream strmzipfile = GetRemoteZipStream(strSharepointRootURL.ToString() + spZIPdoc.Path, "DEFAKTOWEB004", "Administrator", "DGAdmin!1");
                    string strTargetFileName = System.Web.HttpUtility.UrlDecode(strDocName).ToLower().Replace(".pdf", ".xml");

                    // CreateMediaGalleryDocument(114, strmfiledata, ExtractMetaDataFromZipStream(strmzipfile, strTargetFileName), spPDFdoc);
                }
            }
            return bReturnValue;
        }

        public bool testCreate(int groupID, string strDocName)
        {
            // get the document from the sharepoint library
            bool bReturnValue = false;
            string zipDocName = strDocName.Replace(".PDF", ".ZIP").ToString();
            SPApi.Document spZIPdoc = new SPApi.Document();
            SPApi.Document spPDFdoc = new SPApi.Document();
            SPExtv1.PagedList<SPApi.Library> spliblist = new SPExtv1.PagedList<SPApi.Library>();
            spliblist = GetLibraryList(groupID);
            foreach (SPApi.Library library in spliblist)
            {
                SPExtv1.PagedList<SPApi.Document> spdoclist = new SPExtv1.PagedList<SPApi.Document>();
                spdoclist = GetSharepointLibrary(library.Id.ToString());
                foreach (SPApi.Document document in spdoclist)
                {
                    if (document.Name.ToLower() == zipDocName.ToLower())
                    {
                        spZIPdoc = document;
                        break;
                    }
                }
                foreach (SPApi.Document document in spdoclist)
                {
                    if (document.Name.ToLower() == strDocName.ToLower())
                    {
                        spPDFdoc = document;
                        break;
                    }
                }
                if (spZIPdoc.Url != "" && spPDFdoc.Url != "")
                {
                    // Stream strmfiledata = GetRemoteFileStream("http://dev.myomnicell.com/" + spPDFdoc.Url);
                    // ZipInputStream strmzipfile = GetRemoteZipStream("http://dev.myomnicell.com/" + spZIPdoc.Url);
                    string strSharepointRootURL = "http://sp.defaktolabs.com";
                    Stream strmfiledata = GetRemoteFileStream(strSharepointRootURL.ToString() + spPDFdoc.Path, "DEFAKTOWEB004", "Administrator", "DGAdmin!1");
                    ZipInputStream strmzipfile = GetRemoteZipStream(strSharepointRootURL.ToString() + spZIPdoc.Path, "DEFAKTOWEB004", "Administrator", "DGAdmin!1");
                    string strTargetFileName = System.Web.HttpUtility.UrlDecode(strDocName).ToLower().Replace(".pdf", ".xml");

                    // CreateMediaGalleryDocument(114, strmfiledata, ExtractMetaDataFromZipStream(strmzipfile, strTargetFileName), spPDFdoc);
                }
            }
            return bReturnValue;
        }


        // TEST METHODS - PROBABLY SHOULD BE HIDDEN/DELETED BEFORE RELEASE

    }
}

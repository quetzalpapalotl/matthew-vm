﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Caching;
using Telligent.Common;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom.Data;

namespace Omnicell.Custom.Components
{
    public interface ITrainingAttendeeService
    {
        TrainingAttendee Get(int attendeeId);
        TrainingAttendee Get(string emailAddress);
        TrainingAttendee Save(TrainingAttendee attendee);
        PagedList<TrainingAttendee> List(int pageIndex, int pageSize);
        void Delete(int attendeeId);
    }
    public class TrainingAttendeeService : ITrainingAttendeeService
    {
        public readonly ICacheService _cache = null;

        public TrainingAttendeeService()
        {
            _cache = Telligent.Common.Services.Get<ICacheService>();
        }

        public TrainingAttendee Get(int attendeeId)
        {
            TrainingAttendee attendee = GetTrainingAttendeeFromCache(attendeeId);
            if (attendee == null)
            {
                attendee = TrainingAttendeeDataProvider.Instance.Get(attendeeId);

                if (attendee != null)
                    PushTrainingAttendeeToCache(attendee);
            }
            return attendee;
        }
        public TrainingAttendee Get(string emailAddress)
        {
            TrainingAttendee attendee = GetTrainingAttendeeFromCache(emailAddress);
            if (attendee == null)
            {
                attendee = TrainingAttendeeDataProvider.Instance.Get(emailAddress);

                if (attendee != null)
                    PushTrainingAttendeeToCache(attendee);
            }
            return attendee;
        }
        public TrainingAttendee Save(TrainingAttendee attendee)
        {
            int newId = attendee.AttendeeId;
            if (newId > 0)
            {
                RemoveTrainingAttendeeFromCache(attendee.AttendeeId);

                TrainingAttendeeDataProvider.Instance.Update(attendee);
            }
            else
            {
                newId = TrainingAttendeeDataProvider.Instance.Add(attendee);
            }

            return Get(newId);
        }
        public PagedList<TrainingAttendee> List(int pageIndex, int pageSize)
        {
            PagedList<TrainingAttendee> page = GetTrainingAttendeePageFromCache(pageIndex, pageSize);
            if (page == null)
            {
                page = TrainingAttendeeDataProvider.Instance.List(pageIndex, pageSize);

                if (page != null && page.Count > 0)
                    PushTrainingAttendeePageToCache(page);
            }
            return page;
        }
        public void Delete(int attendeeId)
        {
            RemoveTrainingAttendeeFromCache(attendeeId);
            TrainingAttendeeDataProvider.Instance.Delete(attendeeId);
        }

        #region Caching

        private string GetTrainingAttendeePageCacheKey(int pageIndex, int pageSize)
        {
            return string.Format("PK_TRAINING_ATTENDEE_LIST::Index:{0}-Size:{1}", pageIndex, pageSize);
        }
        private string GetTrainingAttendeePageTag()
        {
            return string.Format("TAG:TRAINING_ATTENDEE_PAGE");
        }
        private string GetTrainingAttendeeCacheKey(int attendeeId)
        {
            return string.Format("PK_TRAINING_ATTENDEE::{0}", attendeeId);
        }
        private string GetTrainingAttendeeCacheKey(string emailAddress)
        {
            return string.Format("PK_TRAINING_ATTENDEE::EMAIL-ADDRESS:{0}", emailAddress);
        }
        private string GetAttendeeTag(int attendeeId)
        {
            return string.Format("TAG:ATTENDEE:ID:{0}", attendeeId);
        }


        private void PushTrainingAttendeePageToCache(PagedList<TrainingAttendee> page)
        {
            _cache.Put(GetTrainingAttendeePageCacheKey(page.PageIndex, page.PageSize), page, CacheScope.All, new string[] { GetTrainingAttendeePageTag() });
        }
        private void PushTrainingAttendeeToCache(TrainingAttendee attendee)
        {
            _cache.Put(GetTrainingAttendeeCacheKey(attendee.AttendeeId), attendee, CacheScope.All);

            string[] idTag = new string[] { GetAttendeeTag(attendee.AttendeeId) };
            _cache.Put(GetTrainingAttendeeCacheKey(attendee.EmailAddress), attendee, CacheScope.All, idTag);
        }

        private PagedList<TrainingAttendee> GetTrainingAttendeePageFromCache(int pageIndex, int pageSize)
        {
            return _cache.Get(GetTrainingAttendeePageCacheKey(pageIndex, pageSize), CacheScope.All) as PagedList<TrainingAttendee>;
        }
        private TrainingAttendee GetTrainingAttendeeFromCache(int attendeeId)
        {
            return _cache.Get(GetTrainingAttendeeCacheKey(attendeeId), CacheScope.All) as TrainingAttendee;
        }
        private TrainingAttendee GetTrainingAttendeeFromCache(string emailAddress)
        {
            return _cache.Get(GetTrainingAttendeeCacheKey(emailAddress), CacheScope.All) as TrainingAttendee;
        }

        private void RemoveTrainingAttendeeFromCache(int attendeeId)
        {
            _cache.Remove(GetTrainingAttendeeCacheKey(attendeeId), CacheScope.All);
            _cache.RemoveByTags(new string[] { GetAttendeeTag(attendeeId), GetTrainingAttendeePageTag() }, CacheScope.All);
        }

        #endregion Caching
    }
}

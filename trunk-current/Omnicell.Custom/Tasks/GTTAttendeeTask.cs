﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using TComp = Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;

using Quartz;

using GTT = GoToTraining.Api.Components;

using Omnicell.Custom.Components;
using System.Net;
using System.Collections.Specialized;


namespace Omnicell.Custom
{
    public class GTTAttendeeTask : IStatefulJob
    {

        public void Execute(JobExecutionContext context)
        {

            ITrainingAttendeeService svcAttendee = Telligent.Common.Services.Get<ITrainingAttendeeService>();
            IGoToTrainingService svcGTT = Telligent.Common.Services.Get<IGoToTrainingService>();
            GoToTrainingPlugin gtt = PluginManager.Get<GoToTrainingPlugin>().FirstOrDefault();
            OmnicellTrainingGalleryPlugin otg = PluginManager.Get<OmnicellTrainingGalleryPlugin>().FirstOrDefault();
            TComp.User admUser = Telligent.Evolution.Users.GetUser("admin");
            string message;

            bool tester = false;


            if (gtt != null && otg != null && !string.IsNullOrEmpty(gtt.PreReqProfileFieldName) && otg.TrainingGroupId > 0)
            {
                try
                {
                    Group trainingGroup = PublicApi.Groups.Get(new GroupsGetOptions { Id = otg.TrainingGroupId });

                    DateTime lastDate = trainingGroup.LastAttendeeRequestDate();

                    List<GTT.Attendee> attendees = svcGTT.GetAttendees(lastDate, gtt.PreRequisiteKey);
                    message = string.Format("Attendee list count: {0} with the date of {1}", attendees.Count(), lastDate);
                    LogEntry(message);

                    int completionSecs = gtt.PreReqCompletionMinutes * 60;

                    foreach (var attendee in attendees)
                    {
                        if (attendee.TimeInSession >= completionSecs || gtt.PreReqCompletionMinutes <= 0)
                        {
                            User user = PublicApi.Users.Get(new UsersGetOptions { Email = attendee.Email.Trim() });

                            if (user != null)
                            {
                            
                                ProfileField pf = new ProfileField { Label = gtt.PreReqProfileFieldName, Value = true.ToString() };
                                RunAsUser(() => PublicApi.Users.Update(new UsersUpdateOptions { Id = user.Id, ProfileFields = new List<ProfileField> { pf } }), new TComp.ContextService().GetExecutionContext(), admUser);
                                message = string.Format("RunAsUser completed for: {0}", user.Username);
                                LogEntry(message);
                            }
                            else
                            {
                                svcAttendee.Save(new TrainingAttendee
                                {
                                    EmailAddress = attendee.Email,
                                    GivenName = attendee.GivenName,
                                    Surname = attendee.Surname,
                                    TimeInSession = attendee.TimeInSession,
                                    SessionKey = attendee.SessionKey,
                                    SessionStart = attendee.SessionStartTime.ToString(),
                                    SessionEnd = attendee.SessionEndTime.ToString(),
                                    TrainingKey = attendee.TrainingKey,
                                    TrainingName = attendee.TrainingName
                                });
                            }
                        }
                    }


                    ExtendedAttribute ea = new ExtendedAttribute { Key = Constants.GALLERY_LAST_ATTENDEE_REQ_DATE, Value = DateTime.UtcNow.ToString() };
                    message = string.Format("Extended Attribute Value: {0}", ea.Value);
                    LogEntry(message);
                    Group uGroup = PublicApi.Groups.Update(trainingGroup.Id.Value, new GroupsUpdateOptions { ExtendedAttributes = new List<ExtendedAttribute> { ea } });
                    message = string.Format("Last update time: {0}", uGroup.LastAttendeeRequestDate());
                    LogEntry(message);
                }
               
                catch (Exception ex)
                {
                    new TComp.CSException(TComp.CSExceptionType.UnknownError, "Error Processing GoToTraining. ", ex).Log();
                }
            }
            else
            {
                TComp.EventLogEntry pluginEntry = new TComp.EventLogEntry();
                pluginEntry.Category = "Plugins";
                pluginEntry.EventType = TComp.EventType.Warning;
                pluginEntry.Message = "CANNOT PROCESS ATTENDEES BECAUSE PLUGINS ARE NOT ENABLED AND CONFIGURED.";
                pluginEntry.EventDate = DateTime.Now;
                pluginEntry.EventID = 200;
                pluginEntry.SettingsID = 1000;
                pluginEntry.MachineName = Dns.GetHostName();
                TComp.EventLogs.Write(pluginEntry); 
            }
        }

        public static void LogEntry(string message)
        {
            TComp.EventLogEntry pluginEntry = new TComp.EventLogEntry();
            pluginEntry.Category = "Plugins";
            pluginEntry.EventType = TComp.EventType.Information;
            pluginEntry.Message = message;
            pluginEntry.EventDate = DateTime.Now;
            pluginEntry.EventID = 700;
            pluginEntry.SettingsID = -1;
            pluginEntry.MachineName = Dns.GetHostName();
            TComp.EventLogs.Write(pluginEntry); 
        }

        private static object _runAsUserLock = new object();
        public void RunAsUser(Action a, TComp.IExecutionContext context, TComp.User u)
        {
            lock (_runAsUserLock)
            {
                var originalUser = context.User;
                try
                {
                    context.User = u;
                    a();
                }
                finally
                {
                    context.User = originalUser;
                }
            }
        }

    }
}

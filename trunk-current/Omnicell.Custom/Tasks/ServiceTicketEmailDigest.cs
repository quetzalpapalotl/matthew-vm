﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Telligent.Common;
using TComp = Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Version1;
using Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Quartz;
using Omnicell.Custom.Components;
using System.Net;
using System.Net.Mail;
using System.Collections.Specialized;
using Omnicell.Data.Model;
using Omnicell.Services;

namespace Omnicell.Custom
{
    public class ServiceTicketEmailDigest : IJob
    {
        public void Execute(JobExecutionContext context)
        {

            DateTime today = DateTime.Now;
            DayOfWeek weekday = DateTime.Today.DayOfWeek;
            DateTime firstDay = new DateTime(today.Year, today.Month, 1);
            DateTime lastMonthFirst = firstDay.AddMonths(-1);

            DailyEmails(today);
       
            if (weekday == DayOfWeek.Friday)
            {
                WeeklyEmails(today.AddDays(-7));
            }

            if (firstDay.Date == today.Date)
            {
                MonthlyEmails(lastMonthFirst);
            }
        }

        public void DailyEmails(DateTime today)
        {

            try
            {

                using (var db = new OmnicellEntities())
                {
                    var users = db.Omnicell_TicketEmailInfo.Where(x => x.IsImmediateUpdate == 0);
                    var tickets = new List<Omnicell_Ticket>();

                    foreach (var item in users)
                    {
                        var subscriptions = db.Omnicell_TicketSubscription.Where(x => x.UserId == item.UserId);
                        
                        foreach (var sub in subscriptions)
                        {
                            if (sub.TicketId != null)
                            {
                                var ticket = db.Omnicell_Ticket.FirstOrDefault(x => x.Id == sub.TicketId);

                                if (ticket.Updated.Value.Date == today.Date)
                                {
                                    tickets.Add(ticket);
                                }
                            }

                        }

                        if (tickets.Count != 0)
                        {
                            BuildEmail(tickets, item.Email, item.UserId);
                        }

                        tickets.Clear();

                    }
                }
            }
            catch (Exception ex)
            {
                LogToTelligent("Exception occurred while sending daily subscription emails:" + ex.ToString());
            }
        }

        public void WeeklyEmails(DateTime today)
        {
            try
            {

                using (var db = new OmnicellEntities())
                {
                    var users = db.Omnicell_TicketEmailInfo.Where(x => x.IsImmediateUpdate == 2);
                    var tickets = new List<Omnicell_Ticket>();

                    foreach (var item in users)
                    {
                        var subscriptions = db.Omnicell_TicketSubscription.Where(x => x.UserId == item.UserId);

                        foreach (var sub in subscriptions)
                        {
                            if (sub.TicketId != null)
                            {
                                var ticket = db.Omnicell_Ticket.FirstOrDefault(x => x.Id == sub.TicketId);

                                if (ticket.Updated > today)
                                {
                                    tickets.Add(ticket);
                                }
                            }

                        }

                        if (tickets.Count != 0)
                        {
                            BuildEmail(tickets, item.Email, item.UserId);
                        }

                        tickets.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                LogToTelligent("Exception occurred while sending weekly subscription emails:" + ex.ToString());
            }
        }

        public void MonthlyEmails(DateTime first)
        {
            try
            {


                using (var db = new OmnicellEntities())
                {
                    var users = db.Omnicell_TicketEmailInfo.Where(x => x.IsImmediateUpdate == 3);
                    var tickets = new List<Omnicell_Ticket>();

                    foreach (var item in users)
                    {
                        var subscriptions = db.Omnicell_TicketSubscription.Where(x => x.UserId == item.UserId);

                        foreach (var sub in subscriptions)
                        {
                            if (sub.TicketId != null)
                            {
                                var ticket = db.Omnicell_Ticket.FirstOrDefault(x => x.Id == sub.TicketId);

                                if (ticket.Updated > first)
                                {
                                    tickets.Add(ticket);
                                }
                            }

                        }

                        if (tickets.Count != 0)
                        {
                            BuildEmail(tickets, item.Email, item.UserId);
                        }

                        tickets.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                LogToTelligent("Exception occurred while sending monthly subscription emails:" + ex.ToString());
            }
        }

        public void BuildEmail(List<Omnicell_Ticket> tickets, string email, int? userId)
        {
            GoToTrainingPlugin gtt = PluginManager.Get<GoToTrainingPlugin>().FirstOrDefault();
            var content = new StringBuilder();
            content.Append("Below is your digest of updated service request tickets from myOmnicell:");
            content.Append(Environment.NewLine);
            content.Append(Environment.NewLine);
            string csn = "";

            List<string> exists = new List<string>();

            foreach (var ticket in tickets)
            {
                content.Append("Ticket Number: ");
                content.Append(ticket.Number);
                content.Append(Environment.NewLine);
                content.Append("CSN: ");
                content.Append(ticket.CSN);
                content.Append(Environment.NewLine);
                content.Append("Abstract: ");
                content.Append(ticket.Abstract);
                content.Append(Environment.NewLine);
                content.Append("Status: ");
                content.Append(ticket.Status);
                content.Append(Environment.NewLine);
                content.Append("Sub Status: ");
                content.AppendLine(ticket.SubStatus);
                content.Append(Environment.NewLine);
                content.Append("Last Update Date: ");
                content.AppendLine(ticket.Updated.Value.Date.ToString("MM/dd/yyyy"));
                content.Append(Environment.NewLine);
                content.AppendLine("___________________________________________________________________");
                content.Append(Environment.NewLine);

                if (exists.Count == 0)
                {
                    exists.Add(ticket.CSN);
                    csn += ticket.CSN + ",";
                }
                if (!exists.Exists(x => x == ticket.CSN))
                {
                    exists.Add(ticket.CSN);
                    csn += ticket.CSN + ",";
                }

            }


            string allCsn = csn.Remove(csn.LastIndexOf(','));
            string baseUrl = gtt.RestAPIUrl;
            string smtp = gtt.Smtp;
            string unsubscribe = string.Format(baseUrl + "/customer_portal/p/service-request-tickets.aspx?CSN={0}", allCsn);
            content.Append("To unsubscribe from service ticket notifications, click the link below and follow the instructions on the Service Request Tickets homepage.");
            content.Append(Environment.NewLine);
            content.Append(unsubscribe);

            string fromEmail = "noreply-myomnicell@omnicell.com";
            string toEmail = email;
            string toName = email;
            string fromName = "noreply-myomnicell@omnicell.com";
            string subject = "myOmnicell Service Request Ticket Digest";
            string body = content.ToString();

            SendEmail(fromEmail, fromName, toEmail, toName, subject, body, smtp);
            // Write insert email record sent code here
            insertEmailCount(userId);
        }

        public void SendEmail(string fromEmail, string fromName, string toEmail, string toName, string subject, string body, string smtp)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            try
            {

                if (String.IsNullOrEmpty(fromName)) fromName = fromEmail;
                if (String.IsNullOrEmpty(toName)) toName = toEmail;

                mail.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                mail.Subject = subject;
                mail.Body = body.ToString();
                mail.To.Add(new System.Net.Mail.MailAddress(toEmail, toName));


                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(smtp);
                client.Credentials = CredentialCache.DefaultNetworkCredentials;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);

            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("2 - Message: {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);

            }

        }

        public static void insertEmailCount(int? userId)
        {
            var entities = new OmnicellEntities();
            try
            {
                var dbEntity = new Omnicell_EmailCounter()
                {
                    UserId = Convert.ToInt32(userId),
                    Date = DateTime.Now
                };
                entities.Omnicell_EmailCounter.AddObject(dbEntity);
                entities.SaveChanges();
            }
            catch (Exception ex)
            {
                // log the problem
                LogToTelligent(ex.ToString());
            }

        }

        public static void LogToTelligent(string eventString)
        {
            if (!string.IsNullOrWhiteSpace(eventString))
            {
                try
                {
                    Telligent.Evolution.Components.EventLogs.Info(eventString, "Application", 999);
                    var statusMessage = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.StatusMessages.Create(eventString);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }
                finally
                {
                    Debug.WriteLine(eventString);
                }
            }
        }
    }
}

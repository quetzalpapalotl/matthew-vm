﻿using Omnicell.Custom.Components;
using Quartz;

namespace Omnicell.Custom
{
    public class SharePointSync : IStatefulJob
    {
        public void Execute(JobExecutionContext context)
        {
            // create a reference to the sharepoint class and call the delete method
            MoxiWikiStream wMoxi = new MoxiWikiStream();

            wMoxi.BatchDelete();
            wMoxi.BatchCreate();

        }
    }
}

(function ($) {
    if (typeof $.telligent === 'undefined')
        $.telligent = {};

    if (typeof $.telligent.evolution === 'undefined')
        $.telligent.evolution = {};

    if (typeof $.telligent.evolution.widgets === 'undefined')
        $.telligent.evolution.widgets = {};

    _buildBreadcrumbs = function ($container, groupId, galleryId, currDir, media, customerPortalName) {
        $container.find(".vl-breadcrumb-item, .vl-separator").remove();
        if (typeof (currDir) == "undefined" || currDir.length == 0)
            return;
        var dirs = currDir.replace(/^\/+/, "").replace(/\/+$/, "").split("/");
        var path = "";
        for (var i = 0, len = dirs.length; i < len; i++) {
            var dir = dirs[i];
            path = (path.length > 0 ? (path + "/") : "") + dir;
            if (i == 0) {
                var title = _getCookie(["vltitle_", groupId, "_", galleryId].join(""));
                if (title.length > 0)
                    dir = title;
            }
            if (dir != customerPortalName)
            {
                $container.append(_buildBreadcrumbSeparator);
                $container.append(_buildBreadcrumbItem(dir, path, groupId, galleryId, media ? media.GalleryUrl : null));
            }
        }
        if (media) {
			$container.append(_buildBreadcrumbSeparator);
            $container.append(_buildBreadcrumbItem(media.Title, null, null, galleryId, media.Url));
        }
    };

    _buildBreadcrumbItem = function (title, path, groupId, galleryId, href) {
        var $item = jQuery("<span class='breadcrumb-item vl-breadcrumb-item'></span>");
        var $a = jQuery("<a class='internal-link'></a>").text(title);
        $a.attr("href", href ? href : "javascript:void(0);");
        if (path) {
            (function (path, groupId) {
                $a.click(function () { _openDir(path, groupId, galleryId); });
            })(path, groupId);
        }
        $item.append($a);
        return $item;
    };

    _buildBreadcrumbSeparator = function () {
        return jQuery("<span class='separator vl-separator'> » </span>");
    };

    _openDir = function (dir, groupId, galleryId) {
        document.cookie = ["vlpath_", groupId, "_", galleryId, "=", encodeURIComponent(dir)].join("");
        if (typeof $.telligent.evolution.widgets.sharepointLibrary != "undefined")
            $.telligent.evolution.widgets.sharepointLibrary.openDirectory();
    };

    _getCookie = function (name) {
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            var i = cookies.length;
            name += "=";
            while (i-- > 0) {
                var cookie = jQuery.trim(cookies[i]);
                if (cookie.substring(0, name.length) == name) {
                    return decodeURIComponent(cookie.substring(name.length));
                }
            }
        }
        return "";
    };

    $.telligent.evolution.widgets.vlBreadcrumbs = {
        BuildBreadcrumbs: function ($container, groupId, galleryId, currDir, media) {
            _buildBreadcrumbs($container, groupId, galleryId, currDir, media);
        },
        GetCurrentDirectory: function (groupId, galleryId) {
            return _getCookie(["vlpath_", groupId, "_", galleryId].join(""));
        }
    };
})(jQuery);
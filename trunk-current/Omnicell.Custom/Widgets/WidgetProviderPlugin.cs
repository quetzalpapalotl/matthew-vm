﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;

namespace Omnicell.Custom
{
    public class WidgetProviderPlugin : IScriptedContentFragmentFactoryDefaultProvider, IInstallablePlugin
    {
        #region IScriptedContentFragmentFactoryDefaultProvider Members

        public Guid ScriptedContentFragmentFactoryDefaultIdentifier
        {
            get { return Guid.Parse("C1197335-5D88-4C62-9B80-E41C4AAF1D96"); }
        }

        #endregion

        #region IPlugin Members

        public void Initialize()
        {
        }
        public string Name
        {
            get { return "Omnicell Training Factory Default Widgets"; }
        }
        public string Description
        {
            get { return "Contains all factory default widgets for the Omnicell Training service."; }
        }

        #endregion

        #region IInstallablePlugin Members

        public Version Version
        {
            get
            {
                Version v = GetType().Assembly.GetName().Version;
#if DEBUG
                return new Version(v.Major, v.Minor, v.Revision, v.Build);
#endif
                return new Version(v.Major, v.Minor, v.Revision);
            }
        }
        public void Install(Version lastInstalledVersion)
        {
            // only install new widgets if the version has changed
            if (Version.CompareTo(lastInstalledVersion) > 0)
            {
                // process deprecated widgets first
                RemoveDeprecatedWidgets();

                // This installs the factory defaults into the appropriate location when the plugin
                // is deployed and enabled.
                Func<string, Guid> getGuidFromResourceString = delegate(string inGuid)
                {
                    Guid outGuid;
                    if (Guid.TryParse(inGuid.StartsWith("_") ? inGuid.Substring(1) : inGuid, out outGuid))
                        return outGuid;
                    return Guid.Empty;
                };

                var assembly = GetType().Assembly;
                var assemblyName = assembly.GetName().Name;
                var assemblyNameLength = assemblyName.Length + 1;
                var fileBeginsName = string.Format("{0}.Widgets.", assemblyName);
                var allResourceNames = assembly.GetManifestResourceNames();
                var resourceNames = allResourceNames.Where(n => n.StartsWith(fileBeginsName) && n.EndsWith(".xml"));

                // loop through actual widget definition file
                foreach (string resourceName in resourceNames)
                {
                    System.Diagnostics.Debug.WriteLine(resourceName);

                    string fileName = resourceName.Substring(fileBeginsName.Length);
                    string folderName = fileName.Substring(0, fileName.Length - 4);

                    // extract the guid instance id from the widget file
                    Stream resourceStream = assembly.GetManifestResourceStream(resourceName);
                    string widgetFile = (new StreamReader(resourceStream)).ReadToEnd();
                    int beginPos = widgetFile.IndexOf("instanceIdentifier=") + 20;
                    int endPos = widgetFile.IndexOf("\"", beginPos + 1);
                    string widgetGuid = widgetFile.Substring(beginPos, (endPos - beginPos));
                    Guid instanceId = Guid.Parse(widgetGuid);

                    // add the widget file
                    FactoryDefaultScriptedContentFragmentProviderFiles.AddUpdateDefinitionFile(this, fileName, resourceStream);

                    // get all of the suplemental files for that widget
                    string supplementalFileBegins = string.Format("{0}{1}.", fileBeginsName, folderName);
                    var supplementFiles = allResourceNames.Where(n => n.StartsWith(supplementalFileBegins) && !n.EndsWith(".xml"));


                    foreach (string supplementName in supplementFiles)
                    {
                        string supFileName = supplementName.Substring(supplementalFileBegins.Length);
                        Stream supResourceStream = assembly.GetManifestResourceStream(supplementName);

                        FactoryDefaultScriptedContentFragmentProviderFiles.AddUpdateSupplementaryFile(this, instanceId, supFileName, supResourceStream);
                    }
                }
                Telligent.Common.Services.Get<ICacheService>().Clear(Telligent.Caching.CacheScope.All);
            }
        }
        public void Uninstall()
        {
            FactoryDefaultScriptedContentFragmentProviderFiles.DeleteAllFiles(this);
        }

        #endregion IInstallablePlugin Members

        private void RemoveDeprecatedWidgets()
        {
            foreach (WidgetInfo widget in DeprecatedWidgetList.Widgets)
            {
                // only remove if the version where the widget was deprecated has passed
                if (Version.CompareTo(widget.Version) > 0)
                {
                    if (FactoryDefaultScriptedContentFragmentProviderFiles.DefinitionFileExists(this, widget.FileName))
                        FactoryDefaultScriptedContentFragmentProviderFiles.DeleteDefinitionFile(this, widget.FileName);

                    if (widget.SupplementaryFileNames != null && widget.SupplementaryFileNames.Count > 0)
                    {
                        foreach (string fileName in widget.SupplementaryFileNames)
                        {
                            if (FactoryDefaultScriptedContentFragmentProviderFiles.SupplemenaryFileExists(this, widget.InstanceIdentifier, fileName))
                                FactoryDefaultScriptedContentFragmentProviderFiles.DeleteSupplementaryFile(this, widget.InstanceIdentifier, fileName);
                        }
                    }
                }
            }
        }
    }
    internal static class DeprecatedWidgetList
    {
        public static List<WidgetInfo> Widgets
        {
            get
            {
                return new List<WidgetInfo>
                {
                    new WidgetInfo{ Version = new Version(1,0,1), FileName = "OmnicellOnlineCourseList.xml", InstanceIdentifier = new Guid("078c8de475464b28a3d26ae9db06f0c6"), SupplementaryFileNames = new List<string>{ "btnFilter.png", "format-list.vm", "online.css", "product-list.vm" } },
                    new WidgetInfo{ Version = new Version(1,0,1), FileName = "OmnicellInstructorLedCourseList.xml", InstanceIdentifier = new Guid("b45265adc0a74f5bb367a44b4fbce10a"), SupplementaryFileNames = new List<string>{ "btnFilter.png", "instuctor-led.css", "product-list.vm" } },
                    new WidgetInfo{ Version = new Version(1,0,1), FileName = "OmnicellSubscriptionScheduledCourseList.xml", InstanceIdentifier = new Guid("febad9efaa7f457ea32545800d50e7f5"), SupplementaryFileNames = new List<string>{ "btnFilter.png", "coursetype-list.vm", "scheduled-training.css", "product-list.vm" } },
                    new WidgetInfo{ Version = new Version(1,0,1), FileName = "OmnicellSubscriptionRecordedCourseList.xml", InstanceIdentifier = new Guid("be6db8a7f96c4c7e99de49e26672e0db"), SupplementaryFileNames = new List<string>{ "btnFilter.png", "format-list.vm", "subscription-recorded.css", "product-list.vm" } }
                };
            }
        }
    }
    internal class WidgetInfo
    {
        public Version Version { get; set; }
        public string FileName { get; set; }
        public Guid InstanceIdentifier { get; set; }
        public List<string> SupplementaryFileNames { get; set; }
    }
}
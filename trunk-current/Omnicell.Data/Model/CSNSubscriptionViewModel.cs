﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Data.Model
{
    public class CSNSubscriptionViewModel
    {
        public int Id { get; set; }
        public string CSN { get; set; }
        public string Facility { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Omnicell.Data.Model
{
    public class OmnicellCSNGroup
    {
        public OmnicellCSNGroup()
        {
        }

        public OmnicellCSNGroup(Omnicell_CSNGroup csnGroup)
        {
            CSNGroup = csnGroup;
        }

        private Omnicell_CSNGroup csnGroup = null;
        private Omnicell_CSNGroup CSNGroup
        {
            get
            {
                if (csnGroup == null)
                {
                    csnGroup = new Omnicell_CSNGroup();
                }

                return csnGroup;
            }

            set
            {
                csnGroup = value;
            }
        }

        private string[] csns = null;
        public string[] CSNs
        {
            get
            {
                if (csns == null)
                {
                    if (csnGroup.Omnicell_CSN != null)
                    {
                        csns = csnGroup.Omnicell_CSN.ToList().Select(x => x.CSN).ToArray();
                    }
                }

                return csns;
            }

            set
            {
                csns = value;
            }
        }

        public int Id
        {
            get
            {
                return CSNGroup.Id;
            }

            set
            {
                CSNGroup.Id = value;
            }
        }


    }
}

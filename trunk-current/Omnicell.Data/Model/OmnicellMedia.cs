﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;

namespace Omnicell.Data.Model
{
    public class OmnicellMedia : ICloneable//: Media
    {

        object ICloneable.Clone()
        {
            return this.Clone();
        }

        public OmnicellMedia Clone()
        {
            return (OmnicellMedia)this.MemberwiseClone();
        }

        public OmnicellMedia()
        {
        }

        public OmnicellMedia(Media media)
        {
            Media = media;
        }

        public LookupType MediaGalleryType { get; set; }
        public LookupType DocumentType { get; set; }
        public LookupType MediaFormat { get; set; }
        public LookupType ProductType { get; set; }
        public DateTime? PublishDate { get; set; }
        public DateTime? ScheduledDate { get; set; }

        public Group Group { get; set; }

        private IList<int> groupIds = null;
        public IList<int> GroupIds 
        {
            get
            {
                if (groupIds == null)
                {
                    groupIds = new List<int>();
                }

                return groupIds;
            }

            set
            {
                groupIds = value;
            }
        }

        private IList<Group> groups = null;
        public IList<Group> Groups
        {
            get
            {
                if (groups == null)
                {
                    groups = new List<Group>();
                }

                return groups;
            }

            set
            {
                groups = value;
            }
        }


        private IList<int> audienceIds = null;
        public IList<int> AudienceIds
        {
            get
            {
                if (audienceIds == null)
                {
                    audienceIds = new List<int>();
                }

                return audienceIds;
            }

            set
            {
                audienceIds = value;
            }
        }

        private IList<LookupType> audiences = null;
        public IList<LookupType> Audiences
        {
            get
            {
                if (audiences == null)
                {
                    audiences = new List<LookupType>();
                }

                return audiences;
            }

            set
            {
                audiences = value;
            }
        }

        private IList<LookupType> markets = null;
        public IList<LookupType> Markets
        {
            get
            {
                if (markets == null)
                {
                    markets = new List<LookupType>();
                }

                return markets;
            }

            set
            {
                markets = value;
            }
        }

        private IList<int> marketIds = null;
        public IList<int> MarketIds
        {
            get
            {
                if (marketIds == null)
                {
                    marketIds = new List<int>();
                }

                return marketIds;
            }

            set
            {
                marketIds = value;
            }
        }

        #region Telligent Media type support
        private Media media = null;
        private Media Media
        {
            get
            {
                if (media == null)
                {
                    media = new Media();
                }

                return media;
            }

            set
            {
                media = value;
            }
        }

        public User Author 
        {
            get
            {
                return Media.Author;
            }

            set
            {
                Media.Author = value;
            }
        }

        public int? CommentCount
        {
            get
            {
                return Media.CommentCount;
            }

            set
            {
                Media.CommentCount = value;
            }
        }

        public DateTime? Date
        {
            get
            {
                return Media.Date;
            }

            set
            {
                Media.Date = value;
            }
        }

        public int? Downloads 
        {
            get
            {
                return Media.Downloads;
            }

            set
            {
                Media.Downloads = value;
            }
        }

        public string EmbedCode 
        {
            get
            {
                return Media.EmbedCode;
            }

            set
            {
                Media.EmbedCode = value;
            }
        }

        public string FeaturedImage 
        {
            get
            {
                return Media.FeaturedImage;
            }

            set
            {
                Media.FeaturedImage = value;
            }
        }

        public MediaFile File 
        {
            get
            {
                return Media.File;
            }

            set
            {
                Media.File = value;
            }
        }

        public int? GroupId 
        {
            get
            {
                return Media.GroupId;
            }

            set
            {
                Media.GroupId = value;
            }
        }

        public int? Id 
        {
            get
            {
                return Media.Id;
            }

            set
            {
                Media.Id = value;
            }
        }

        public bool? IsApproved 
        {
            get
            {
                return Media.IsApproved;
            }

            set
            {
                Media.IsApproved = value;
            }
        }

        public bool? IsFeatured 
        {
            get
            {
                return Media.IsFeatured;
            }

            set
            {
                Media.IsFeatured = value;
            }
        }

        public int? MediaGalleryId 
        {
            get
            {
                return Media.MediaGalleryId;
            }

            set
            {
                Media.MediaGalleryId = value;
            }
        }

        public int? RatingCount 
        {
            get
            {
                return Media.RatingCount;
            }

            set
            {
                Media.RatingCount = value;
            }
        }

        public int? RatingSum 
        {
            get
            {
                return Media.RatingSum;
            }

            set
            {
                Media.RatingSum = value;
            }
        }

        public IList<string> Tags 
        {
            get
            {
                return Media.Tags;
            }

            set
            {
                Media.Tags = value;
            }
        }

        public string Title 
        {
            get
            {
                return Media.Title;
            }

            set
            {
                Media.Title = value;
            }
        }

        public string Url 
        {
            get
            {
                return Media.Url;
            }

            set
            {
                Media.Url = value;
            }
        }

        public int? Views 
        {
            get
            {
                return Media.Views;
            }

            set
            {
                Media.Views = value;
            }
        }

        public IList<Error> Errors 
        {
            get
            {
                return Media.Errors;
            }

        }

        public IList<Warning> Warnings 
        {
            get
            {
                return Media.Warnings;
            }

        }

        public ApiList<ExtendedAttribute> ExtendedAttributes 
        {
            get
            {
                return Media.ExtendedAttributes;
            }

            set
            {
                Media.ExtendedAttributes = value;
            }
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

using Telligent.Common;
using Telligent.Evolution.Components;
using TEntities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom.Search;

namespace Omnicell.Fetch.Components
{
    public class ImplementationRoom : TEntities.Group
    {
        private IFactSheetService _svcFactSheet = null;

        public ImplementationRoom() 
        {
            _svcFactSheet = Services.Get<IFactSheetService>();
        }
        public ImplementationRoom (Guid contentId) : this()
        {
            TEntities.Group group = TApi.PublicApi.Groups.Get(contentId);
            if(group != null && group.ContainerId == contentId)
                MapFromGroup(group);
        }
        public ImplementationRoom (int groupId) : this()
        {            
            TEntities.Group group = TApi.PublicApi.Groups.Get(new TApi.GroupsGetOptions{ Id = groupId });
            if (group != null && group.Id.HasValue && group.Id.Value == groupId)
                MapFromGroup(group);
        }
        public ImplementationRoom (TEntities.Group group) : this()
        {
            if (group != null)
                MapFromGroup(group);
        }

        private FactSheet _factSheet = null;
        public FactSheet FactSheet
        {
            get
            {
                if (_factSheet == null && this.Id.HasValue)
                {
                    _factSheet = _svcFactSheet.GetByGroup(this.Id.Value);
                }
                return _factSheet;
            }
        }

        private void MapFromGroup(TEntities.Group group)
        {
            PropertyInfo[] basePIs = typeof(TEntities.Group).GetProperties();
            Type thisObjectType = this.GetType();

            foreach (PropertyInfo basePI in basePIs)
            {
                PropertyInfo newPI = thisObjectType.GetProperty(basePI.Name);
                if (newPI != null)
                {
                    try
                    {
                        newPI.SetValue(this, basePI.GetValue(group, null), null);
                    }
                    catch { }
                }
            }
        }
    }
}

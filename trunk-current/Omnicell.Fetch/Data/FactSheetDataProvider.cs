﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using Omnicell.Fetch.Components;
using Omnicell.Custom.Data;

namespace Omnicell.Fetch.Data
{
    public class FactSheetDataProvider : SqlDataProvider<FactSheetDataProvider, FactSheet>
    {
        public int Add(FactSheet factSheet)
        {
            string procName = ProcNameWithOwner("omnicell_FactSheet_Add");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@GroupId", SqlDbType = SqlDbType.Int, Value = factSheet.GroupId},
                new SqlParameter{ParameterName = "@FacilityName", SqlDbType = SqlDbType.NVarChar, Size = 128, Value = factSheet.FacilityName},
                new SqlParameter{ParameterName = "@FacilityCsn", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = factSheet.FacilityCsn},
                new SqlParameter{ParameterName = "@TerritoryOpMgr", SqlDbType = SqlDbType.Int, Value = factSheet.TerritoryOpMgr},
                new SqlParameter{ParameterName = "@DivisionalOpDir", SqlDbType = SqlDbType.Int, Value = factSheet.DivisionalOpDir},
                new SqlParameter{ParameterName = "@SystemSalesDir", SqlDbType = SqlDbType.Int, Value = factSheet.SystemSalesDir},
                new SqlParameter{ParameterName = "@ContactName", SqlDbType = SqlDbType.NVarChar, Size = 128, Value = factSheet.ContactName},
                new SqlParameter{ParameterName = "@ContactTitle", SqlDbType = SqlDbType.NVarChar, Size = 128, Value = factSheet.ContactTitle},
                new SqlParameter{ParameterName = "@ContactPhone", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = factSheet.ContactPhone},
                new SqlParameter{ParameterName = "@ContactEmail", SqlDbType = SqlDbType.NVarChar, Size = 255, Value = factSheet.ContactEmail},
                new SqlParameter{ParameterName = "@ContactStreet", SqlDbType = SqlDbType.NVarChar, Size = 255, Value = factSheet.ContactStreet},
                new SqlParameter{ParameterName = "@ContactCity", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = factSheet.ContactCity},
                new SqlParameter{ParameterName = "@ContactState", SqlDbType = SqlDbType.NVarChar, Size = 5, Value = factSheet.ContactState},
                new SqlParameter{ParameterName = "@ContactZip", SqlDbType = SqlDbType.NVarChar, Size = 25, Value = factSheet.ContactZip},
                new SqlParameter{ParameterName = "@ContactCountry", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = factSheet.ContactCountry},
            };

            if (!string.IsNullOrEmpty(factSheet.FacilityTimeZone))
                parameters.Add(new SqlParameter { ParameterName = "@FacilityTimeZone", SqlDbType = SqlDbType.NVarChar, Size = 512, Value = factSheet.FacilityTimeZone });
            if (!string.IsNullOrEmpty(factSheet.IdnParent))
                parameters.Add(new SqlParameter { ParameterName = "@IdnParent", SqlDbType = SqlDbType.NVarChar, Size = 128, Value = factSheet.IdnParent });
            if (!string.IsNullOrEmpty(factSheet.IdnCsn))
                parameters.Add(new SqlParameter { ParameterName = "@IdnCsn", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = factSheet.IdnCsn });
            if (factSheet.ProjectDeployMgr.HasValue)
                parameters.Add(new SqlParameter { ParameterName = "@ProjectDeployMgr", SqlDbType = SqlDbType.Int, Value = factSheet.ProjectDeployMgr.Value });
            if (!string.IsNullOrEmpty(factSheet.Notes))
                parameters.Add(new SqlParameter { ParameterName = "@Notes", SqlDbType = SqlDbType.NVarChar, Size = 750, Value = factSheet.Notes });

            return Add(procName, parameters);
        }
        public void Update(FactSheet factSheet)
        {
            string procName = ProcNameWithOwner("omnicell_FactSheet_Update");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@FactSheetId", SqlDbType = SqlDbType.Int, Value = factSheet.FactSheetId},
                new SqlParameter{ParameterName = "@GroupId", SqlDbType = SqlDbType.Int, Value = factSheet.GroupId},
                new SqlParameter{ParameterName = "@FacilityName", SqlDbType = SqlDbType.NVarChar, Size = 128, Value = factSheet.FacilityName},
                new SqlParameter{ParameterName = "@FacilityCsn", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = factSheet.FacilityCsn},
                new SqlParameter{ParameterName = "@TerritoryOpMgr", SqlDbType = SqlDbType.Int, Value = factSheet.TerritoryOpMgr},
                new SqlParameter{ParameterName = "@DivisionalOpDir", SqlDbType = SqlDbType.Int, Value = factSheet.DivisionalOpDir},
                new SqlParameter{ParameterName = "@SystemSalesDir", SqlDbType = SqlDbType.Int, Value = factSheet.SystemSalesDir},
                new SqlParameter{ParameterName = "@ContactName", SqlDbType = SqlDbType.NVarChar, Size = 128, Value = factSheet.ContactName},
                new SqlParameter{ParameterName = "@ContactTitle", SqlDbType = SqlDbType.NVarChar, Size = 128, Value = factSheet.ContactTitle},
                new SqlParameter{ParameterName = "@ContactPhone", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = factSheet.ContactPhone},
                new SqlParameter{ParameterName = "@ContactEmail", SqlDbType = SqlDbType.NVarChar, Size = 255, Value = factSheet.ContactEmail},
                new SqlParameter{ParameterName = "@ContactStreet", SqlDbType = SqlDbType.NVarChar, Size = 255, Value = factSheet.ContactStreet},
                new SqlParameter{ParameterName = "@ContactCity", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = factSheet.ContactCity},
                new SqlParameter{ParameterName = "@ContactState", SqlDbType = SqlDbType.NVarChar, Size = 5, Value = factSheet.ContactState},
                new SqlParameter{ParameterName = "@ContactZip", SqlDbType = SqlDbType.NVarChar, Size = 25, Value = factSheet.ContactZip},
                new SqlParameter{ParameterName = "@ContactCountry", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = factSheet.ContactCountry},
            };

            if (!string.IsNullOrEmpty(factSheet.FacilityTimeZone))
                parameters.Add(new SqlParameter { ParameterName = "@FacilityTimeZone", SqlDbType = SqlDbType.NVarChar, Size = 512, Value = factSheet.FacilityTimeZone });
            if (!string.IsNullOrEmpty(factSheet.IdnParent))
                parameters.Add(new SqlParameter { ParameterName = "@IdnParent", SqlDbType = SqlDbType.NVarChar, Size = 128, Value = factSheet.IdnParent });
            if (!string.IsNullOrEmpty(factSheet.IdnCsn))
                parameters.Add(new SqlParameter { ParameterName = "@IdnCsn", SqlDbType = SqlDbType.NVarChar, Size = 50, Value = factSheet.IdnCsn });
            if (factSheet.ProjectDeployMgr.HasValue)
                parameters.Add(new SqlParameter { ParameterName = "@ProjectDeployMgr", SqlDbType = SqlDbType.Int, Value = factSheet.ProjectDeployMgr.Value });
            if (!string.IsNullOrEmpty(factSheet.Notes))
                parameters.Add(new SqlParameter { ParameterName = "@Notes", SqlDbType = SqlDbType.NVarChar, Size = 750, Value = factSheet.Notes });

            ExecuteNonQuery(procName, parameters);
        }
        public FactSheet Get(int id)
        {
            string procName = ProcNameWithOwner("omnicell_FactSheet_Get");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@FactSheetId", SqlDbType = SqlDbType.Int, Value = id}
            };
            return Get(procName, parameters);
        }
        public FactSheet GetByGroup(int groupId)
        {
            string procName = ProcNameWithOwner("omnicell_FactSheet_GetByGroup");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@GroupId", SqlDbType = SqlDbType.Int, Value = groupId}
            };
            return Get(procName, parameters);
        }
        public void Delete(int id)
        {
            string procName = ProcNameWithOwner("omnicell_FactSheet_Delete");
            List<SqlParameter> parameters = new List<SqlParameter>()
            {
                new SqlParameter{ParameterName = "@Id", SqlDbType = SqlDbType.Int, Value = id}
            };
            Delete(procName, parameters);
        }
    }
}

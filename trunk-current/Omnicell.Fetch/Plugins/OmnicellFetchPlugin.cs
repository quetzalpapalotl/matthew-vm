﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Telligent.Common;
using Telligent.DynamicConfiguration.Components;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;
using TEntities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;

using Omnicell.Custom;
using Omnicell.Custom.Components;

namespace Omnicell.Fetch
{
    public class OmnicellFetchPlugin : IConfigurablePlugin, IScriptedContentFragmentExtension
    {
        private readonly IUrlManipulation UrlManipulation = Telligent.Common.Services.Get<IUrlManipulation>();

        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Fetch Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Fetch System."; }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_fetch"; }
        }
        public object Extension
        {
            get { return new OmnicellFetchWidgetExtension(_configuration); }
        }

        IPluginConfiguration _configuration = null;
        public Telligent.DynamicConfiguration.Components.PropertyGroup[] ConfigurationOptions
        {
            get
            {
                PropertyGroup[] groups = new[] { new PropertyGroup("fetch", "Fetch Settings", 0) };

                Property fetchParentGroupProp = new Property("fetchParentGroup", "Fetch Parent Group", PropertyType.String, 0, "Group=1");
                fetchParentGroupProp.DescriptionText = "Select the parent group for all Fetch Implementation Rooms";
                fetchParentGroupProp.ControlType = typeof(Telligent.Evolution.Controls.GroupSelectionList);
                groups[0].Properties.Add(fetchParentGroupProp);

                return groups;
            }
        }
        public void Update(IPluginConfiguration configuration)
        {
            _configuration = configuration;

            FetchParentGroupId = FetchUtilities.GetLookupValue(configuration.GetCustom("fetchParentGroup"), -1);
        }

        public int FetchParentGroupId { get; private set; }
    }
    public class OmnicellFetchWidgetExtension
    {
        private readonly IUrlManipulation UrlManipulation = Telligent.Common.Services.Get<IUrlManipulation>();

        public OmnicellFetchWidgetExtension(IPluginConfiguration configuration)
        {
            FetchParentGroupId = FetchUtilities.GetLookupValue(configuration.GetCustom("fetchParentGroup"), -1);
        }

        public int FetchParentGroupId { get; private set; }

        private TEntities.Group _fetchParentGroup = null;
        public TEntities.Group FetchParentGroup
        {
            get
            {
                if (_fetchParentGroup == null && FetchParentGroupId > 0)
                {
                    _fetchParentGroup = TApi.PublicApi.Groups.Get(new TApi.GroupsGetOptions { Id = FetchParentGroupId });
                }
                return _fetchParentGroup;
            }
        }
    }
}

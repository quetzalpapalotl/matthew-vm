﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Omnicell.Custom;
using Omnicell.Fetch.Components;
using Telligent.Common;
using Telligent.DynamicConfiguration.Components;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1;
using TEntities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TExV1 = Telligent.Evolution.Extensibility.Version1;

namespace Omnicell.Fetch
{
    public class OmnicellImplementationRoomPlugin : IScriptedContentFragmentExtension, IConfigurablePlugin
    {
        public void Initialize() { }
        public string Name
        {
            get { return "Omnicell Implementation Room Widget Methods"; }
        }
        public string Description
        {
            get { return "Enables scripted content fragments to use functionality for the Omnicell Implementation Group."; }
        }
        public string ExtensionName
        {
            get { return "omnicell_v1_implementationroom"; }
        }
        public object Extension
        {
            get { return new OmnicellImplementationRoomWidgetExtension(_configuration); }
        }

        IPluginConfiguration _configuration = null;
        public Telligent.DynamicConfiguration.Components.PropertyGroup[] ConfigurationOptions
        {
            get
            {
                PropertyGroup grpSettings = new PropertyGroup("impRoomSettings", "Implementation Room Settings", 0);
                grpSettings.DescriptionText = "Enter the names for the applications for the applications created with an implementation group.\n"
                                          + "Use the following tokens to customize the names...\n"
                                          + "   {0} = Facility Name\n"
                                          + "   {1} = Facility CSN\n";

                grpSettings.Properties.AddRange(new List<Property> {
                    new Property("publicForumName", "Public Forum Name", PropertyType.String, 0, "PUBLIC Implementation Room Discussion"),
                    new Property("privateDiscussionGroupName", "Private Discussion Group Name", PropertyType.String, 1, "{0} - Private Discussions"),
                    new Property("privateDiscussionForumName", "Private Discussion Forum Name", PropertyType.String, 2, "PRIVATE Employee Implementation Room Discussion"),
                    new Property("projectPlanGalleryName", "Project Plan Gallery Name", PropertyType.String, 3, "Project Plans"),
                    new Property("meetingNotesGalleryName", "Meeting Notes Gallery Name", PropertyType.String, 4, "Meeting Notes"),
                    new Property("testingStatusGalleryName", "Testing Status Gallery Name", PropertyType.String, 5, "Testing Status"),
                    new Property("irTechDocsGalleryName", "IR Tech Docs Gallery Name", PropertyType.String, 6, "IR Tech Documents"),
                    new Property("factSheetGalleryName", "Fact Sheet Gallery Name", PropertyType.String, 7, "Fact Sheets")
                });

                PropertyGroup grpDefaults = new PropertyGroup("defaultItemSettings", "Room Template Settings", 1);
                grpDefaults.DescriptionText = "Select the galleries that contain the template items to be added to new implementation rooms;";

                Property galPrjPlanProp = new Property("galProjectPlanTemplates", "Project Plan Template Gallery", PropertyType.String, 0, "");
                galPrjPlanProp.DescriptionText = "Select the gallery that contains all template items for the project plan gallery.";
                galPrjPlanProp.ControlType = typeof(Telligent.Evolution.MediaGalleries.Controls.MediaGallerySelectionControl);
                galPrjPlanProp.Attributes.Add("maximumMediaGallerySelections", "1");
                galPrjPlanProp.Attributes.Add("enableGroupSelection", "false");

                Property galMtgNotesProp = new Property("galMeetingNotesTemplates", "Meeting Notes Template Gallery", PropertyType.String, 1, "");
                galMtgNotesProp.DescriptionText = "Select the gallery that contains all template items for the meeting notes gallery.";
                galMtgNotesProp.ControlType = typeof(Telligent.Evolution.MediaGalleries.Controls.MediaGallerySelectionControl);
                galMtgNotesProp.Attributes.Add("maximumMediaGallerySelections", "1");
                galMtgNotesProp.Attributes.Add("enableGroupSelection", "false");

                Property galTstStatusProp = new Property("galTestStatusTemplates", "Test Status Template Gallery", PropertyType.String, 2, "");
                galTstStatusProp.DescriptionText = "Select the gallery that contains all template items for the testing status gallery.";
                galTstStatusProp.ControlType = typeof(Telligent.Evolution.MediaGalleries.Controls.MediaGallerySelectionControl);
                galTstStatusProp.Attributes.Add("maximumMediaGallerySelections", "1");
                galTstStatusProp.Attributes.Add("enableGroupSelection", "false");

                Property galFactSheetProp = new Property("galFactSheetTemplates", "Fact Sheet Template Gallery", PropertyType.String, 3, "");
                galFactSheetProp.DescriptionText = "Select the gallery that contains all template items for the fact sheet gallery.";
                galFactSheetProp.ControlType = typeof(Telligent.Evolution.MediaGalleries.Controls.MediaGallerySelectionControl);
                galFactSheetProp.Attributes.Add("maximumMediaGallerySelections", "1");
                galFactSheetProp.Attributes.Add("enableGroupSelection", "false");

                grpDefaults.Properties.AddRange(new List<Property>
                {
                    galPrjPlanProp, galMtgNotesProp, galTstStatusProp, galFactSheetProp
                });

                return new PropertyGroup[] { grpSettings, grpDefaults };
            }
        }
        public void Update(IPluginConfiguration configuration)
        {
            _configuration = configuration;
            PublicForumName = configuration.GetString("publicForumName");
            PrivateDiscussionGroupName = configuration.GetString("privateDiscussionGroupName");
            PrivateDiscussionForumName = configuration.GetString("privateDiscussionForumName");
            ProjectPlanGalleryName = configuration.GetString("projectPlanGalleryName");
            MeetingNotesGalleryName = configuration.GetString("meetingNotesGalleryName");
            TestingStatusGalleryName = configuration.GetString("testingStatusGalleryName");
            IRTechDocumentsGalleryName = configuration.GetString("irTechDocsGalleryName");
            FactSheetGalleryName = configuration.GetString("factSheetGalleryName");

            ProjectPlanTemplateGalleryId = FetchUtilities.GetLookupValue(configuration.GetCustom("galProjectPlanTemplates"), -1);
            MeetingNotesTemplateGalleryId = FetchUtilities.GetLookupValue(configuration.GetCustom("galMeetingNotesTemplates"), -1);
            TestingStatusTemplateGalleryId = FetchUtilities.GetLookupValue(configuration.GetCustom("galTestStatusTemplates"), -1);
            FactSheetTemplateGalleryId = FetchUtilities.GetLookupValue(configuration.GetCustom("galFactSheetTemplates"), -1);
        }

        public string PublicForumName { get; private set; }
        public string PrivateDiscussionGroupName { get; private set; }
        public string PrivateDiscussionForumName { get; private set; }
        public string ProjectPlanGalleryName { get; private set; }
        public string MeetingNotesGalleryName { get; private set; }
        public string TestingStatusGalleryName { get; private set; }
        public string IRTechDocumentsGalleryName { get; private set; }
        public string FactSheetGalleryName { get; private set; }

        public int ProjectPlanTemplateGalleryId { get; private set; }
        public int MeetingNotesTemplateGalleryId { get; private set; }
        public int TestingStatusTemplateGalleryId { get; private set; }
        public int FactSheetTemplateGalleryId { get; private set; }
    }
    public class OmnicellImplementationRoomWidgetExtension
    {
        private IFactSheetService _svcFactSheet = null;
        private IProjectHealthService _svcProjectHealth = null;
        private OmnicellFetchPlugin _plgnFetch = null;

        public OmnicellImplementationRoomWidgetExtension(IPluginConfiguration configuration)
        {
            _svcFactSheet = Services.Get<IFactSheetService>();
            _svcProjectHealth = Services.Get<IProjectHealthService>();
            _plgnFetch = TExV1.PluginManager.Get<OmnicellFetchPlugin>().FirstOrDefault();
            PublicForumName = configuration.GetString("publicForumName");
            PrivateDiscussionGroupName = configuration.GetString("privateDiscussionGroupName");
            PrivateDiscussionForumName = configuration.GetString("privateDiscussionForumName");
            ProjectPlanGalleryName = configuration.GetString("projectPlanGalleryName");
            MeetingNotesGalleryName = configuration.GetString("meetingNotesGalleryName");
            TestingStatusGalleryName = configuration.GetString("testingStatusGalleryName");
            IRTechDocumentsGalleryName = configuration.GetString("irTechDocsGalleryName");
            FactSheetGalleryName = configuration.GetString("factSheetGalleryName");

            ProjectPlanTemplateGalleryId = FetchUtilities.GetLookupValue(configuration.GetCustom("galProjectPlanTemplates"), -1);
            MeetingNotesTemplateGalleryId = FetchUtilities.GetLookupValue(configuration.GetCustom("galMeetingNotesTemplates"), -1);
            TestingStatusTemplateGalleryId = FetchUtilities.GetLookupValue(configuration.GetCustom("galTestStatusTemplates"), -1);
            FactSheetTemplateGalleryId = FetchUtilities.GetLookupValue(configuration.GetCustom("galFactSheetTemplates"), -1);
        }

        private string PublicForumName { get; set; }
        private string PrivateDiscussionGroupName { get; set; }
        private string PrivateDiscussionForumName { get; set; }
        private string PrivateBlogName { get; set; }
        private string ProjectPlanGalleryName { get; set; }
        private string MeetingNotesGalleryName { get; set; }
        private string TestingStatusGalleryName { get; set; }
        public string IRTechDocumentsGalleryName { get; private set; }
        public string FactSheetGalleryName { get; private set; }

        public int ProjectPlanTemplateGalleryId { get; private set; }
        public int MeetingNotesTemplateGalleryId { get; private set; }
        public int TestingStatusTemplateGalleryId { get; private set; }
        public int FactSheetTemplateGalleryId { get; private set; }

        public ImplementationRoom Create(string name, string type, string facilityName, string facilityCSN, string territoryOpMgr, string divisionalOpDir,
            string systemSalesDir, string contactName, string contactTitle, string contactPhone, string contactEmail, string contactStreet, string contactCity,
            string contactState, string contactZip, string contactCountry,
            [
                Documentation(Name = "FacilityTimeZone", Type = typeof(string), Description = "Time Zone of Facility"),
                Documentation(Name = "IDNParent", Type = typeof(string), Description = "Name of IDN Parent"),
                Documentation(Name = "IDNCSN", Type = typeof(string), Description = "CSN of IDN Parent"),
                Documentation(Name = "ProjectDeployMgr", Type = typeof(int), Description = "UserId for Project Deployment Manager"),
                Documentation(Name = "Notes", Type = typeof(string), Description = "Notes for fact sheet")
            ] IDictionary options = null)
        {
            int? territoryOpMgrId = GetUserId(territoryOpMgr);
            int? divisionalOpDirId = GetUserId(divisionalOpDir);
            int? systemSalesDirId = GetUserId(systemSalesDir);

            if (!territoryOpMgrId.HasValue || !divisionalOpDirId.HasValue || !systemSalesDirId.HasValue ||
                string.IsNullOrEmpty(name) || string.IsNullOrEmpty(type) || string.IsNullOrEmpty(facilityName) ||
                string.IsNullOrEmpty(facilityCSN) || string.IsNullOrEmpty(contactName) || string.IsNullOrEmpty(contactTitle) ||
                string.IsNullOrEmpty(contactPhone) || string.IsNullOrEmpty(contactEmail) || string.IsNullOrEmpty(contactStreet) ||
                string.IsNullOrEmpty(contactCity) || string.IsNullOrEmpty(contactStreet) || string.IsNullOrEmpty(contactZip) ||
                string.IsNullOrEmpty(contactCountry))
            {

                ImplementationRoom errorRoom = new ImplementationRoom();
                errorRoom.Errors.Add(new TEntities.Error("Required Field Missing", "Please ensure that all required fields have values."));

                return errorRoom;
            }
            else
            {


                int parentGroup = 1;
                if (_plgnFetch != null && _plgnFetch.FetchParentGroupId > 0)
                    parentGroup = _plgnFetch.FetchParentGroupId;

                TEntities.Group impRoomGroup = TApi.PublicApi.Groups.Create(name, "PrivateUnlisted",
                    new TApi.GroupsCreateOptions
                    {
                        AutoCreateApplications = false,
                        Description = type,
                        ParentGroupId = parentGroup
                    });

                if (!impRoomGroup.HasErrors())
                {

                    TEntities.Forum publicForum = TApi.PublicApi.Forums.Create(impRoomGroup.Id.Value, ResolveNameTokens(PublicForumName, facilityName, facilityCSN),
                        new TApi.ForumsCreateOptions
                        {
                            AllowedThreadTypes = "Discussion,QuestionAndAnswer",
                            DefaultThreadType = "Discussion"
                        });

                    TEntities.Gallery projectPlanGallery = TApi.PublicApi.Galleries.Create(impRoomGroup.Id.Value, ResolveNameTokens(ProjectPlanGalleryName, facilityName, facilityCSN),
                        new TApi.GalleriesCreateOptions
                        {
                            Enabled = true
                        });
                    if (ProjectPlanTemplateGalleryId > 0 && projectPlanGallery.Id.HasValue)
                        CopyGalleryItems(ProjectPlanTemplateGalleryId, projectPlanGallery.Id.Value);


                    TEntities.Gallery meetingNotesGallery = TApi.PublicApi.Galleries.Create(impRoomGroup.Id.Value, ResolveNameTokens(MeetingNotesGalleryName, facilityName, facilityCSN),
                        new TApi.GalleriesCreateOptions
                        {
                            Enabled = true
                        });
                    if (MeetingNotesTemplateGalleryId > 0 && meetingNotesGallery.Id.HasValue)
                        CopyGalleryItems(MeetingNotesTemplateGalleryId, meetingNotesGallery.Id.Value);

                    TEntities.Gallery testingStatusGallery = TApi.PublicApi.Galleries.Create(impRoomGroup.Id.Value, ResolveNameTokens(TestingStatusGalleryName, facilityName, facilityCSN),
                        new TApi.GalleriesCreateOptions
                        {
                            Enabled = true
                        });
                    if (TestingStatusTemplateGalleryId > 0 && testingStatusGallery.Id.HasValue)
                        CopyGalleryItems(TestingStatusTemplateGalleryId, testingStatusGallery.Id.Value);

                    TEntities.Gallery irTechDocGallery = TApi.PublicApi.Galleries.Create(impRoomGroup.Id.Value, ResolveNameTokens(IRTechDocumentsGalleryName, facilityName, facilityCSN),
                        new TApi.GalleriesCreateOptions
                        {
                            Enabled = true
                        });
                    
                    TEntities.Gallery factSheetGallery = TApi.PublicApi.Galleries.Create(impRoomGroup.Id.Value, ResolveNameTokens(FactSheetGalleryName, facilityName, facilityCSN),
                        new TApi.GalleriesCreateOptions
                        {
                            Enabled = true
                        });
                    if (FactSheetTemplateGalleryId > 0 && factSheetGallery.Id.HasValue)
                        CopyGalleryItems(FactSheetTemplateGalleryId, factSheetGallery.Id.Value);

                    TEntities.Group privateGroup = TApi.PublicApi.Groups.Create(ResolveNameTokens(PrivateDiscussionGroupName, facilityName, facilityCSN), "PrivateUnlisted",
                        new TApi.GroupsCreateOptions
                        {
                            AutoCreateApplications = false,
                            ParentGroupId = impRoomGroup.Id
                        });

                    TEntities.Forum privateForum = TApi.PublicApi.Forums.Create(privateGroup.Id.Value, ResolveNameTokens(PrivateDiscussionForumName, facilityName, facilityCSN),
                        new TApi.ForumsCreateOptions
                        {
                            AllowedThreadTypes = "Discussion,QuestionAndAnswer",
                            DefaultThreadType = "Discussion"
                        });


                    FactSheet sheet = new FactSheet
                    {
                        FactSheetId = -1,
                        GroupId = impRoomGroup.Id.Value,
                        FacilityName = facilityName,
                        FacilityCsn = facilityCSN,
                        TerritoryOpMgr = territoryOpMgrId.Value,
                        DivisionalOpDir = divisionalOpDirId.Value,
                        SystemSalesDir = systemSalesDirId.Value,
                        ContactName = contactName,
                        ContactTitle = contactTitle,
                        ContactPhone = contactPhone,
                        ContactEmail = contactEmail,
                        ContactStreet = contactStreet,
                        ContactCity = contactCity,
                        ContactState = contactState,
                        ContactZip = contactZip,
                        ContactCountry = contactCountry
                    };

                    string facilityTimeZone = options.GetValue("FacilityTimeZone", sheet.FacilityTimeZone);
                    string idnParent = options.GetValue("IDNParent", sheet.IdnParent);
                    string idnCsn = options.GetValue("IDNCSN", sheet.IdnCsn);
                    string pdm = options.GetValue("ProjectDeployMgr");
                    int? projectDeployMgr = GetUserId(pdm);
                    string notes = options.GetValue("Notes", sheet.Notes);

                    if (!string.IsNullOrEmpty(facilityTimeZone))
                        sheet.FacilityTimeZone = facilityTimeZone;
                    if (!string.IsNullOrEmpty(idnParent))
                        sheet.IdnParent = idnParent;
                    if (!string.IsNullOrEmpty(idnCsn))
                        sheet.IdnCsn = idnCsn;
                    if (projectDeployMgr.HasValue)
                        sheet.ProjectDeployMgr = projectDeployMgr.Value;
                    if (!string.IsNullOrEmpty(notes))
                        sheet.Notes = notes;

                    FactSheet newSheet = _svcFactSheet.Save(sheet);

                    _svcProjectHealth.CreateInitial(impRoomGroup.Id.Value);

                }
                return new ImplementationRoom(impRoomGroup);
            }
        }

        public ImplementationRoom Get(int id)
        {
            return new ImplementationRoom(id);
        }

        public bool IsOwner(int groupId, int userId)
        {
            return IsMembershipType(groupId, userId, "Owner");
        }

        public bool IsManager(int groupId, int userId)
        {
            return IsMembershipType(groupId, userId, "Manager");
        }

        public bool IsAdmin()
        {
            bool isAdmin = false;
            string roleName = "Administrators";
            isAdmin = TApi.PublicApi.RoleUsers.IsUserInRoles( CSContext.Current.User.Username, new string[] {roleName});
            return isAdmin;
        }

        public bool IsMembershipType(int groupId, int userId, string membershipType)
        {
            bool isMembershipType = false;
            TEntities.GroupUser groupUser = TApi.PublicApi.GroupUserMembers.Get(groupId, new TApi.GroupUserMembersGetOptions() { UserId = userId });
            if (groupUser != null)
            {
                if (groupUser.MembershipType == membershipType)
                {
                    isMembershipType = true;
                }
            }

            return isMembershipType;
        }

        private string ResolveNameTokens(string name, string facilityName, string facilityCSN)
        {
            return string.Format(name, facilityName, facilityCSN);
        }
        private int? GetUserId(string value)
        {
            int? userId = null;

            if (!string.IsNullOrEmpty(value))
            {
                string[] segs = value.Split(":".ToCharArray());
                int id = -1;
                if (segs.Length > 1)
                {
                    if (int.TryParse(segs[1], out id))
                        userId = id;
                }
            }

            return userId;
        }
        private void CopyGalleryItems(int fromGalleryId, int toGalleryId)
        {
            TEntities.Gallery fromGallery = TApi.PublicApi.Galleries.Get(new TApi.GalleriesGetOptions { Id = fromGalleryId });
            if (fromGallery != null && fromGallery.Id == fromGalleryId)
            {
                TEntities.Gallery toGallery = TApi.PublicApi.Galleries.Get(new TApi.GalleriesGetOptions { Id = toGalleryId });
                if (toGallery != null && toGallery.Id == toGalleryId)
                {
                    TEntities.PagedList<TEntities.Media> items = TApi.PublicApi.Media.List(new TApi.MediaListOptions { GalleryId = fromGalleryId, PageSize = int.MaxValue, PageIndex = 0 });

                    foreach (var item in items)
                    {

                        TApi.PublicApi.Media.Create(toGalleryId, item.Title, item.File.ContentType.ToString(), item.File.FileName,
                            new TApi.MediaCreateOptions
                            {
                                Description = item.Body(),
                                FileData = item.File.Data()
                            }
                        );
                    }
                }
            }
        }
    }
}

(function($){

    if (typeof $.omnicell === 'undefined') { $.omnicell = {}; }
    if (typeof $.omnicell.fetch === 'undefined') { $.omnicell.fetch = {}; }
    if (typeof $.omnicell.fetch.widgets === 'undefined') { $.omnicell.fetch.widgets = {}; }    

    var spinner = '<div style="text-align: center;"><img src="' + $.telligent.evolution.site.getBaseUrl() + 'utility/spinner.gif" /></div>',
        searchUsers = function(context, textbox, searchText) {
    	    window.clearTimeout(context.inviteMemberByNameUserNameTimeout);
            if (searchText && searchText.length >= 2) {
                
    			textbox.glowLookUpTextBox('updateSuggestions', [
                	textbox.glowLookUpTextBox('createLookUp', '', spinner, spinner, false)
                ]);                
                
                context.inviteMemberByNameUserNameTimeout = window.setTimeout(function () {
                    $.telligent.evolution.get({
                        url: context.findUsersOrRolesUrl,
                        data: { w_SearchText: searchText, w_IncludeRoles: 'False' },
                        success: function (response) {
                            if (response && response.matches.length > 1) {
                                var suggestions = [];
                                for (var i = 0; i < response.matches.length; i++) {
                                    var item = response.matches[i];
                                    if (item && item.userId) {
                                        suggestions[suggestions.length] = textbox.glowLookUpTextBox('createLookUp', 'user:' + item.userId, item.title, item.title, true);
                                    }
                                    else if (item && item.ldapUserId) {
                                        suggestions[suggestions.length] = textbox.glowLookUpTextBox('createLookUp', 'ldapUser:' + item.ldapUserId, item.title, item.title, true);
                                    }
                                }
            
                                textbox.glowLookUpTextBox('updateSuggestions', suggestions);
                            }
                            else
                                textbox.glowLookUpTextBox('updateSuggestions', [textbox.glowLookUpTextBox('createLookUp', '', context.noUserOrRoleMatchesText, context.noUserOrRoleMatchesText, false)]);
                        }
                    });
                }, 749);
            }
		},
        attachHandlers = function(context) {
            var userBoxes = new Array();
            userBoxes[0] = context.territoryOpMgrInput;
            userBoxes[1] = context.divisionalOpDirInput;
            userBoxes[2] = context.systemSalesDirInput;
            userBoxes[3] = context.projectDeployMgrInput;
            
            $.each(userBoxes, function(n) {
                if(this.length > 0) {
                    var userVal = this.val();
                    var userId = this.attr('data-id');
                    
                    this.glowLookUpTextBox({
                    	delimiter: ',',
                    	allowDuplicates: true,
                    	maxValues: 1,
                    	onGetLookUps: function(tb, searchText) {
                    		searchUsers(context, tb, searchText);
                    	},
                    	emptyHtml: '',
                    	selectedLookUpsHtml: [],
                    	deleteImageUrl: ''
                    });
                    
                    if(userVal.length > 0) {
                        var userItem = this.glowLookUpTextBox('createLookUp', userId, userVal, userVal, true);
                        this.glowLookUpTextBox('add', userItem);
                    }
                }
            });
            
            context.editForm.hide();
            context.editButton.click(function(e) {
                e.preventDefault();
                showEditMode(context);
            });
            
        },
        attachValidation = function(context) {

            $.validator.addMethod("phoneUS", function(phone_number, element) {
                phone_number = phone_number.replace(/\s+/g, "");
            	return this.optional(element) || phone_number.length > 9 &&
            		phone_number.match(/^(\+?1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
            }, "Please specify a valid phone number");

            context.createButton
                .evolutionValidation({
                	onValidated: function(isValid, buttonClicked, c) {
                		if (isValid) {
                			context.createButton.removeClass('disabled');
                		} else {
                			context.createButton.addClass('disabled');
                		}
                	},
                	onSuccessfulClick: function(e) {
                		e.preventDefault();
                		context.createButton.parent().addClass('processing');
                		context.createButton.addClass('disabled');
                		// submit the form
                		updateFactSheet(context);
                	}
                }).evolutionValidation('addField', context.facilityNameInput, {
                        required: true,
						messages: { required: context.facilityNameRequired }
                    }, context.facilityNameInput.closest('.field-item').find('.field-item-validation'), null
                ).evolutionValidation('addField', context.facilityCsnInput, {
                        required: true,
						messages: { required: context.facilityCSNRequired }
                    }, context.facilityCsnInput.closest('.field-item').find('.field-item-validation'), null
                ).evolutionValidation('addField', context.contactNameInput, {
                        required: true,
						messages: { required: context.contactNameRequired }
                    }, context.contactNameInput.closest('.field-item').find('.field-item-validation'), null
                ).evolutionValidation('addField', context.contactTitleInput, {
                        required: true,
						messages: { required: context.contactTitleRequired }
                    }, context.contactTitleInput.closest('.field-item').find('.field-item-validation'), null
                ).evolutionValidation('addField', context.contactPhoneInput, {
                        required: true,
                        phoneUS: true,
						messages: { required: context.contactPhoneRequired }
                    }, context.contactPhoneInput.closest('.field-item').find('.field-item-validation'), null
                ).evolutionValidation('addField', context.contactEmailInput, {
                        required: true,
                        email: true,
						messages: { required: context.contactEmailRequired }
                    }, context.contactEmailInput.closest('.field-item').find('.field-item-validation'), null
                ).evolutionValidation('addField', context.contactStreetInput, {
                        required: true,
						messages: { required: context.contactStreetRequired }
                    }, context.contactStreetInput.closest('.field-item').find('.field-item-validation'), null
                ).evolutionValidation('addField', context.contactCityInput, {
                        required: true,
						messages: { required: context.contactCityRequired }
                    }, context.contactCityInput.closest('.field-item').find('.field-item-validation'), null
                ).evolutionValidation('addField', context.contactStateInput, {
                        required: true,
						messages: { required: context.contactStateRequired }
                    }, context.contactStateInput.closest('.field-item').find('.field-item-validation'), null
                ).evolutionValidation('addField', context.contactZipInput, {
                        required: true,
						messages: { required: context.contactZipRequired }
                    }, context.contactZipInput.closest('.field-item').find('.field-item-validation'), null
                ).evolutionValidation('addField', context.contactCountryInput, {
                        required: true,
						messages: { required: context.contactCountryRequired }
                    }, context.contactCountryInput.closest('.field-item').find('.field-item-validation'), null
                );
			
			context.territoryOpMgrInput.bind('glowLookUpTextBoxChange', 
				context.createButton.evolutionValidation('addCustomValidation', 'omnicell_TerritoryOpMgrRequired', 
					function () {
						return context.territoryOpMgrInput.glowLookUpTextBox('count') > 0;
					},context.territoryOpMgrRequired, context.territoryOpMgrInput.closest('.field-item').find('.field-item-validation'), null
				)
			);
			context.divisionalOpDirInput.bind('glowLookUpTextBoxChange', 
				context.createButton.evolutionValidation('addCustomValidation', 'omnicell_DivisionalOpDirRequired', 
					function () {
						return context.divisionalOpDirInput.glowLookUpTextBox('count') > 0;
					},context.divisionalOpDirRequired, context.divisionalOpDirInput.closest('.field-item').find('.field-item-validation'), null
				)
			);
			context.systemSalesDirInput.bind('glowLookUpTextBoxChange', 
				context.createButton.evolutionValidation('addCustomValidation', 'omnicell_SystemSalesDirRequired', 
					function () {
						return context.systemSalesDirInput.glowLookUpTextBox('count') > 0;
					},context.systemSalesDirRequired, context.systemSalesDirInput.closest('.field-item').find('.field-item-validation'), null
				)
			);
        },
        updateFactSheet = function(context) {
            var fsData = {
                w_factSheetId: context.factSheetId.val(),
                w_groupId: context.groupId.val(),
                w_facilityName: context.facilityNameInput.val(),
                w_facilityCsn: context.facilityCsnInput.val(),
                w_facilityTimeZone: '',
                w_idnParent: context.idnParentInput.val(),
                w_idnCsn: context.idnCsnInput.val(),
                w_territoryOpMgrInput: context.territoryOpMgrInput.val(),
                w_divisionalOpDirInput: context.divisionalOpDirInput.val(),
                w_systemSalesDirInput: context.systemSalesDirInput.val(),
                w_projectDeployMgrInput: context.projectDeployMgrInput.val(),
                w_contactName: context.contactNameInput.val(),
                w_contactTitle: context.contactTitleInput.val(),
                w_contactPhone: context.contactPhoneInput.val(),
                w_contactEmail: context.contactEmailInput.val(),
                w_contactStreet: context.contactStreetInput.val(),
                w_contactCity: context.contactCityInput.val(),
                w_contactState: '',
                w_contactZip: context.contactZipInput.val(),
                w_contactCountry: context.contactCountryInput.val(),
                w_notes: context.notesInput.val(),                
            };
            
            var selTimeZone = context.facilityTimeZoneInput.find('option:selected');
            if(selTimeZone.length > 0)
                fsData.w_facilityTimeZone = selTimeZone.val();
                
            var selState = context.contactStateInput.find('option:selected');
            if(selState.length > 0)
                fsData.w_contactState = selState.val();
                
            $.telligent.evolution.post({
                url: context.udateUrl,
                data: fsData,
                success: function(response) {
                    if(response.success)
                        window.location.href=window.location.href;
                    else
                        alert('An error occured while updating. Please try to update again.');
                },
                error: function(a, b, c){
                    var stopper = true;
                }
                
            });
            
        },
        showEditMode = function(context) {
            context.displayForm.hide();
            context.editForm.css('height', 'auto');
            context.editForm.show();
        };
    
    $.omnicell.fetch.widgets.createImplementationRoom = {
    	register: function(context) {
            
            context.groupId = $(context.groupId);
            context.factSheetId = $(context.factSheetId);
            context.editForm = $(context.editForm);
            context.displayForm = $(context.displayForm);
            context.facilityNameInput = $(context.facilityNameInput);
            context.facilityCsnInput = $(context.facilityCsnInput);
            context.facilityTimeZoneInput = $(context.facilityTimeZoneInput);
            context.idnParentInput = $(context.idnParentInput);
            context.idnCsnInput = $(context.idnCsnInput);
            context.territoryOpMgrInput = $(context.territoryOpMgrInput);
            context.divisionalOpDirInput = $(context.divisionalOpDirInput);
            context.systemSalesDirInput = $(context.systemSalesDirInput);
            context.projectDeployMgrInput = $(context.projectDeployMgrInput);
            context.contactNameInput = $(context.contactNameInput);
            context.contactTitleInput = $(context.contactTitleInput);
            context.contactPhoneInput = $(context.contactPhoneInput);
            context.contactEmailInput = $(context.contactEmailInput);
            context.contactStreetInput = $(context.contactStreetInput);
            context.contactCityInput = $(context.contactCityInput);
            context.contactStateInput = $(context.contactStateInput);
            context.contactZipInput = $(context.contactZipInput);
            context.contactCountryInput = $(context.contactCountryInput);
            context.notesInput = $(context.notesInput);
            context.createButton = $(context.createButton);
            context.editButton = $(context.editButton);

            attachHandlers(context);
            attachValidation(context);
            
    	}
	};
    
}(jQuery));

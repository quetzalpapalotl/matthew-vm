﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Linq;
using System.Web;
using Omnicell.Data.Model;
using System.Net;
using System.Net.Mail;
using System.Xml;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using System.Diagnostics;
using System.Text.RegularExpressions;
using Omnicell.Data;

using SPClient = Telligent.Evolution.Extensions.SharePoint.Client;
using Telligent.Evolution.Components;

namespace Omnicell.Services
{
    
    public class Utilities
    {
        // BEGIN Sharepoint Adds
        public string GetSPDateCreated()
        {
            // connect to Sharepoint and get a list of the libraries
            SPClient.version1.SharePointLibrary spLibrary = new SPClient.version1.SharePointLibrary();
            SPClient.version2.SharePointLibrary spLib2 = new SPClient.version2.SharePointLibrary();
            try
            {
                // convert the library ID to GUID
                Guid LibIDGuid = new Guid("2e59f92e-fb3a-41c8-8fb0-94d0ac358d6b");
                spLib2.Get(LibIDGuid);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Message: " + ex.Message);
            }
            return spLib2.Current.Created.ToString();
        }

        // END Sharepoint Adds
        private OmnicellEntities _db = null;

        public OmnicellEntities Db
        {
            get
            {
                if (_db == null)
                {
                    _db = new OmnicellEntities();
                }

                return _db;
            }

            set
            {
                _db = value;
            }
        }
        public void UploadDataFeed(string dataFile, string fileName)
        {
            string fileNameWithPath = System.Web.HttpContext.Current.Server.MapPath("~/custom/data/" + fileName);
            using (FileStream fs = new FileStream(fileNameWithPath, FileMode.Create))
            {
                using (BinaryWriter bw = new BinaryWriter(fs))
                {
                    byte[] data = System.Convert.FromBase64String(dataFile);
                    bw.Write(data);
                    bw.Close();
                }
            }
        }

        public void UploadDataFeed(HttpPostedFile dataFile, string fileName)
        {

            string fileNameWithPath = System.Web.HttpContext.Current.Server.MapPath("~/custom/data/" + fileName);
            dataFile.SaveAs(fileNameWithPath);

        }
        // SMR - 2013097 - escape out special characters for XML parsing
        public String HandleSpecialXMLCharacters(String InputStream)
        {
            // read each line of the input stream, replace the characters and then append it back.
            StringBuilder strOutput = new StringBuilder();
            String strLine = "";
            StreamReader sr = new StreamReader(InputStream);
            // abstract, status, sub-status, symptom_code, resolution_code
            try
            {
                while ((strLine = sr.ReadLine()) != null)
                {
                    strLine = strLine.Trim();
                    // Is this a line with actual data
                    if (strLine.Length < 18)
                    {
                        strOutput.Append(strLine.ToString());
                    }
                    else
                    {
                        // the simple replaces
                        if (strLine.IndexOf("&") > 0)
                            strLine=strLine.Replace("&", "&amp;");
                        if (strLine.IndexOf("'") > 0)
                            strLine=strLine.Replace("'", "&apos;");
                        // now handle the complex ones
                        if (strLine.Substring(0, 10).ToLower() == "<abstract>")
                        {
                            strLine = strLine.Replace("<Abstract>", "").Replace("</Abstract>","");
                            strLine = strLine.Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;");
                            strLine = "<Abstract>" + strLine.ToString() + "</Abstract>";
                        }
                        if (strLine.Substring(0, 12).ToLower() == "<sub-status>")
                        {
                            strLine = strLine.Replace("<Sub-Status>", "").Replace("</Sub-Status>", "");
                            strLine = strLine.Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;"); ;
                            strLine = "<Sub-Status>" + strLine.ToString() + "</Sub-Status>";
                        }
                        if (strLine.Substring(0, 14).ToLower() == "<symptom_code>")
                        {
                            strLine = strLine.Replace("<Symptom_Code>","").Replace("</Symptom_Code>","");
                            strLine = strLine.Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;"); ;
                            strLine = "<Symptom_Code>" + strLine.ToString() + "</Symptom_Code>";
                        }
                        if (strLine.Substring(0, 17).ToLower() == "<resolution_code>")
                        {
                            strLine = strLine.Replace("<Resolution_Code>", "").Replace("</Resolution_Code>", "");
                            strLine = strLine.Replace("\"", "&quot;").Replace("<", "&lt;").Replace(">", "&gt;");
                            strLine = "<Resolution_Code>" + strLine.ToString() + "</Resolution_Code>";
                        }
                        strOutput.Append(strLine.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                strOutput.Clear();
                strOutput.Append("ERROR");
            }
            return strOutput.ToString();
        }

        public string ParseCustomerXmlFile(Stream fileStream)
        {
            try
            {
                var now = DateTime.Now;
                fileStream.Position = 0;
                var reader = new XmlTextReader(fileStream);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                reader.Read();
                using (var db = new OmnicellEntities())
                {
                    var customers = db.Omnicell_Customer.Where(x => x.IsActive).ToList();
                    while (!reader.EOF)
                    {
                        if (reader.Name == "Customers" && !reader.IsStartElement())
                        {
                            break;
                        }

                        while (reader.Name != "Customer" || !reader.IsStartElement())
                        {
                            reader.Read(); // advance to <Customer> tag
                        }

                        reader.Read(); // advance to CSN tag

                        var csn = reader.ReadElementString("CSN");
                        csn = csn == null ? null : csn.Trim();

                        var customer = customers.SingleOrDefault(x => csn.Equals(x.CSN));

                        // could be inactive now
                        if (customer == null)
                        {
                            customer = db.Omnicell_Customer.SingleOrDefault(x => csn.Equals(x.CSN));
                        }

                        if (customer == null)
                        {
                            customer = new Omnicell_Customer
                            {
                                CSN = csn
                            };
                            db.Omnicell_Customer.AddObject(customer);
                        }

                        customer.IsActive = true;
                        customer.Updated = now;
                        customer.AccountName = reader.ReadElementString("Account");
                        customer.AccountName = customer.AccountName == null ? null : customer.AccountName.Trim();

                        customer.City = reader.ReadElementString("City");
                        customer.City = customer.City == null ? null : customer.City.Trim();

                        customer.State = reader.ReadElementString("State");
                        customer.State = customer.State == null ? null : customer.State.Trim();

                        customer.PostalCode = reader.ReadElementString("PostalCode");
                        customer.PostalCode = customer.PostalCode == null ? null : customer.PostalCode.Trim();

                        customer.BusinessPartnerType = reader.ReadElementString("BusinessPartnerType");
                        customer.BusinessPartnerType = customer.BusinessPartnerType == null ? null : customer.BusinessPartnerType.Trim();

                        var idnString = reader.ReadElementString("IDN");
                        customer.IDN = idnString == null ? 0 : int.Parse(idnString.Trim());

                        customer.IDName = reader.ReadElementString("IDNName");
                        customer.IDName = customer.IDName == null ? null : customer.IDName.Trim();

                        var gpoString = reader.ReadElementString("GPO");
                        customer.GPO = gpoString == null ? 0 : int.Parse(gpoString.Trim());

                        customer.GPOName = reader.ReadElementString("GPOName");
                        customer.GPOName = customer.GPOName == null ? null : customer.GPOName.Trim();

                        // now we should be at the </Customer> tag

                        reader.Read(); // now at either </Customers> or <Customer>
                        customers.Remove(customer);
                    }

                    foreach (var missingCustomer in customers.Where(x => string.IsNullOrWhiteSpace(x.Name) ||  x.Updated < now))
                    {
                        if (missingCustomer.EntityState != System.Data.EntityState.Modified || (missingCustomer.IsActive && (!missingCustomer.Updated.HasValue || !now.Equals(missingCustomer.Updated.Value))))
                        {
                            missingCustomer.Updated = now;
                            missingCustomer.IsActive = false;
                        }
                    }

                    db.SaveChanges();
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                // log the problem
                LogToTelligent(ex.ToString());
                return ex.ToString();
            }

            return "success";
        }

        public static void LogToTelligent(string eventString)
        {
            if (!string.IsNullOrWhiteSpace(eventString))
            {
                try
                {
                    Telligent.Evolution.Components.EventLogs.Info(eventString, "Application", 999);
                    var statusMessage = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.StatusMessages.Create(eventString);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.ToString());
                }
                finally
                {
                    Debug.WriteLine(eventString);
                }
            }
        }


        public string ParseEmployeeXmlFile(Stream fileStream)
        {
            try
            {
                var now = DateTime.Now;
                fileStream.Position = 0;
                var reader = new XmlTextReader(fileStream);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                reader.Read();
                using (var db = new OmnicellEntities())
                {
                    var employees = db.Omnicell_Employee.Where(x => x.IsActive).ToList();

                    while (!reader.EOF)
                    {
                        if (reader.Name == "Employees" && !reader.IsStartElement())
                        {
                            break;
                        }

                        while (reader.Name != "Employee" || !reader.IsStartElement())
                        {
                            reader.Read(); // advance to <Employee> tag
                        }

                        reader.Read(); // advance to First Name tag



                        var firstName = reader.ReadElementString("FirstName");
                        firstName = firstName == null ? null : firstName.Trim();

                        var lastName = reader.ReadElementString("LastName");
                        lastName = lastName == null ? null : lastName.Trim();
                        
                        var email = reader.ReadElementString("Email");
                        email = email == null ? null : email.Trim();
                        
                        var adid = reader.ReadElementString("ADID");
                        adid = adid == null ? null : adid.Trim();

                        var employeeNumberString = reader.ReadElementString("EmployeeNumber");
                        var employeeNumber = employeeNumberString == null ? 0 : int.Parse(employeeNumberString.Trim());

                        var employee = employees.SingleOrDefault(x => employeeNumber == x.EmployeeNumber);

                        // could be inactive now
                        if (employee == null)
                        {
                            employee = db.Omnicell_Employee.SingleOrDefault(x => employeeNumber == x.EmployeeNumber);
                        }

                        if (employee == null)
                        {
                            employee = new Omnicell_Employee
                            {
                                EmployeeNumber = employeeNumber
                            };
                            db.Omnicell_Employee.AddObject(employee);
                        }

                        employee.IsActive = true;
                        employee.Updated = now;
                        employee.FirstName = firstName;
                        employee.LastName = lastName;
                        employee.Email = email;
                        employee.ADID = adid;

                        // now we should be at the </Employee> tag

                        reader.Read(); // now at either </Employees> or <Employee>
                        employees.Remove(employee);
                    }


                    foreach (var missingEmployee in employees)
                    {
                        if (missingEmployee.EntityState != System.Data.EntityState.Modified || (missingEmployee.IsActive && (!missingEmployee.Updated.HasValue || !now.Equals(missingEmployee.Updated.Value))))
                        {
                            missingEmployee.Updated = now;
                            missingEmployee.IsActive = false;
                        }
                    }

                    db.SaveChanges();
                }

                reader.Close();
            }
            catch (Exception ex)
            {
                // log the problem
                LogToTelligent(ex.ToString());
                return ex.ToString();
            }

            return "success";
        }

        private MemoryStream ScrubTicketXml(out List<String> badTickets, Stream ticketStream)
        {
            badTickets = new List<String>();

            // Scrub non-printable ascii characters
            StreamReader sr = new StreamReader(ticketStream);

//            string bufferStreamToString = sr.ReadToEnd();
//            bufferStreamToString = bufferStreamToString.Replace(Convert.ToChar((byte)0x1F), ' ');
//            byte[] byteArray = Encoding.ASCII.GetBytes(bufferStreamToString);
//            MemoryStream parsedMs = new MemoryStream(byteArray);

//            StreamReader pSr = new StreamReader(parsedMs);

            StringBuilder sbScrubbedXML = new StringBuilder();
            string line = String.Empty;
            string lineNumber = String.Empty;

            StringBuilder sbTicketNode = new StringBuilder();
            
            while ((line = sr.ReadLine()) != null )
            {
                line = line.Replace(Convert.ToChar((byte)0x1F), ' ');

                if (line.Contains("<?xml version") || line.Contains("<Tickets>") || line.Contains("</Tickets>")){
                    sbScrubbedXML.Append(line);
                }else{

                    if(line.Contains("<Number>"))
                        lineNumber = line.Replace("<Number>", "").Replace("</Number>", "");

                    sbTicketNode.Append(line);
                }

                if(sbTicketNode.ToString().Contains("</Ticket>")){
                    //var strTicketNode = HandleSpecialXMLCharacters(sbTicketNode.ToString());
                    var strTicketNode = sbTicketNode.ToString();

                    try{
                        XmlDocument testNode = new XmlDocument();
                        testNode.LoadXml(strTicketNode);
                        var testIt = testNode.SelectSingleNode("/Ticket/Abstract");

                        sbScrubbedXML.Append(strTicketNode);
                        sbTicketNode.Clear();
                    }catch(Exception ex){
                        badTickets.Add(lineNumber);
                        sbTicketNode.Clear();
                    }

                }

            }

            byte[] byteArray = Encoding.ASCII.GetBytes(sbScrubbedXML.ToString());
            return new MemoryStream(byteArray);
        }

        //public string ParseTicketXmlFile(Stream fileStream)
        //{
            //int count = 0;
            //var result = String.Empty;
            //List<String> lstErrorTickets = new List<String>();

            ////try
            ////{
            //    var now = DateTime.Now;
            //    fileStream.Position = 0;

            //    MemoryStream parsedStream = ScrubTicketXml(out lstErrorTickets, fileStream);

            //    var updatedTickets = new List<Omnicell_Ticket>();
            //    var reader = new XmlTextReader(parsedStream);
            //    reader.WhitespaceHandling = WhitespaceHandling.None;

            //    reader.Read();
            //    using (var db = new OmnicellEntities())
            //    {
            //        var tickets = db.Omnicell_Ticket.Where(x => x.IsActive).ToList();
            //        var ticketAreas = db.Omnicell_TicketArea.ToList();
            //        var ticketResolutionCodes = db.Omnicell_TicketResolutionCode.ToList();
            //        var ticketSymptomCodes = db.Omnicell_TicketSymptomCode.ToList();
            //        var ticketStatuses = db.Omnicell_TicketStatus.ToList();
            //        var ticketSubStatuses = db.Omnicell_TicketSubStatus.ToList();
            //        var hasUpdate = false;

            //        while (!reader.EOF)
            //        {
            //            hasUpdate = false;
            //            if (reader.Name == "Tickets" && !reader.IsStartElement())
            //            {
            //                break;
            //            }

            //            while (reader.Name != "Ticket" || !reader.IsStartElement())
            //            {
            //                reader.Read(); // advance to <Ticket> tag
            //            }

            //            reader.Read(); // advance to <Number> tag

            //            var number = reader.ReadElementString("Number");
            //            number = number == null ? null : number.Trim();

            //            var ticket = tickets.FirstOrDefault(x => number.Equals(x.Number));

            //            // could be inactive now
            //            if (ticket == null)
            //            {
            //                ticket = db.Omnicell_Ticket.FirstOrDefault(x => number.Equals(x.Number));
            //            }

            //            if (ticket == null)
            //            {
            //                ticket = new Omnicell_Ticket
            //                {
            //                    Number = number
            //                };
            //                db.Omnicell_Ticket.AddObject(ticket);
            //            }

            //            ticket.IsActive = true;
            //            ticket.Updated = now;
            //            //var xmlAbstract = reader.readelement

            //            try{


            //            var xmlAbstract = reader.ReadElementString("Abstract");
            //            xmlAbstract = xmlAbstract == null ? null : xmlAbstract.Trim();

            //            var xmlStatus = reader.ReadElementString("Status");
            //            xmlStatus = xmlStatus == null ? null : xmlStatus.Trim();

            //            var xmlSubStatus = reader.ReadElementString("Sub-Status");
            //            xmlSubStatus = xmlSubStatus == null ? null : xmlSubStatus.Trim();

            //            var xmlCSN = reader.ReadElementString("CSN");
            //            xmlCSN = xmlCSN == null ? null : xmlCSN.Trim();

            //            var xmlSymptomCode = reader.ReadElementString("Symptom_Code");
            //            xmlSymptomCode = xmlSymptomCode == null ? null : xmlSymptomCode.Trim();

            //            var xmlResolutionCode = reader.ReadElementString("Resolution_Code");
            //            xmlResolutionCode = xmlResolutionCode == null ? null : xmlResolutionCode.Trim();

            //            var xmlLastActivityUpdate = reader.ReadElementString("Last_Activity_Update");
            //            xmlLastActivityUpdate = xmlLastActivityUpdate == null ? null : xmlLastActivityUpdate.Trim();

            //            var xmlSerialNumber = reader.ReadElementString("Serial_Number");
            //            xmlSerialNumber = xmlSerialNumber == null ? null : xmlSerialNumber.Trim();

            //            if (ticket.SerialNumber != xmlSerialNumber)
            //            {
            //                ticket.SerialNumber = xmlSerialNumber;
            //                hasUpdate = true;
            //            }

            //            if (ticket.Abstract != xmlAbstract)
            //            {
            //                ticket.Abstract = xmlAbstract;
            //                hasUpdate = true;
            //            }

            //            if (ticket.CSN != xmlCSN)
            //            {
            //                ticket.CSN = xmlCSN;
            //                hasUpdate = true;
            //            }                        
                        
            //            if (ticket.Status != xmlStatus)
            //            {
            //                ticket.Status = xmlStatus;
            //                hasUpdate = true;
            //                if (!string.IsNullOrWhiteSpace(xmlStatus))
            //                {
            //                    ticket.TicketStatus = ticketStatuses.FirstOrDefault(x => ticket.Status.Equals(x.Value));
            //                    if (ticket.TicketStatus == null)
            //                    {
            //                        ticket.TicketStatus = new TicketStatus
            //                        {
            //                            Value = ticket.Status
            //                        };

            //                        ticketStatuses.Add(ticket.TicketStatus);
            //                    }
            //                }
            //                else
            //                {
            //                    ticket.TicketStatus = null;
            //                }
            //            }

            //            if (ticket.SubStatus != xmlSubStatus)
            //            {
            //                ticket.SubStatus = xmlSubStatus;
            //                hasUpdate = true;
            //                if (!string.IsNullOrWhiteSpace(ticket.SubStatus))
            //                {
            //                    ticket.TicketSubStatus = ticketSubStatuses.FirstOrDefault(x => ticket.SubStatus.Equals(x.Value));
            //                    if (ticket.TicketSubStatus == null)
            //                    {
            //                        ticket.TicketSubStatus = new TicketSubStatus
            //                        {
            //                            Value = ticket.SubStatus
            //                        };

            //                        ticketSubStatuses.Add(ticket.TicketSubStatus);
            //                    }
            //                }
            //                else
            //                {
            //                    ticket.TicketSubStatus = null;
            //                }
            //            }

            //            if (ticket.SymptomCode != xmlSymptomCode)
            //            {
            //                ticket.SymptomCode = xmlSymptomCode;
            //                hasUpdate = true;
            //                if (!string.IsNullOrWhiteSpace(ticket.SymptomCode))
            //                {
            //                    var values = ticket.SymptomCode.Split(new string[] { " - " }, 2, StringSplitOptions.None);
            //                    var symptomCode = values.ElementAtOrDefault(0);
            //                    symptomCode = symptomCode == null ? null : symptomCode.Trim();
            //                    var symptomArea = values.ElementAtOrDefault(1);
            //                    symptomArea = symptomArea == null ? null : symptomArea.Trim();

            //                    if (!string.IsNullOrWhiteSpace(symptomCode))
            //                    {
            //                        ticket.TicketSymptomCode = ticketSymptomCodes.FirstOrDefault(x => symptomCode.Equals(x.Value));
            //                        if (ticket.TicketSymptomCode == null)
            //                        {
            //                            ticket.TicketSymptomCode = new TicketSymptomCode
            //                            {
            //                                Value = symptomCode
            //                            };

            //                            ticketSymptomCodes.Add(ticket.TicketSymptomCode);
            //                        }
            //                    }

            //                    if (!string.IsNullOrWhiteSpace(symptomArea))
            //                    {
            //                        ticket.SymptomTicketArea = ticketAreas.FirstOrDefault(x => symptomArea.Equals(x.Value));
            //                        if (ticket.SymptomTicketArea == null)
            //                        {
            //                            ticket.SymptomTicketArea = new TicketArea
            //                            {
            //                                Value = symptomArea
            //                            };

            //                            ticketAreas.Add(ticket.SymptomTicketArea);
            //                        }
            //                    }
            //                }
            //                else
            //                {
            //                    ticket.TicketSymptomCode = null;
            //                    ticket.SymptomTicketArea = null;
            //                }
            //            }
            //                if (ticket.ResolutionCode != xmlResolutionCode)
            //                {
            //                    ticket.ResolutionCode = xmlResolutionCode;
            //                    hasUpdate = true;
            //                    if (!string.IsNullOrWhiteSpace(ticket.ResolutionCode))
            //                    {
            //                        var values = ticket.ResolutionCode.Split(new string[] { " - " }, 2, StringSplitOptions.None);
            //                        var resolutionCode = values.ElementAtOrDefault(0);
            //                        resolutionCode = resolutionCode == null ? null : resolutionCode.Trim();
            //                        var resolutionArea = values.ElementAtOrDefault(1);
            //                        resolutionArea = resolutionArea == null ? null : resolutionArea.Trim();

            //                        if (!string.IsNullOrWhiteSpace(resolutionCode))
            //                        {
            //                            ticket.TicketResolutionCode = ticketResolutionCodes.FirstOrDefault(x => resolutionCode.Equals(x.Value));
            //                            if (ticket.TicketResolutionCode == null)
            //                            {
            //                                ticket.TicketResolutionCode = new TicketResolutionCode
            //                                {
            //                                    Value = resolutionCode
            //                                };

            //                                ticketResolutionCodes.Add(ticket.TicketResolutionCode);
            //                            }
            //                        }

            //                        if (!string.IsNullOrWhiteSpace(resolutionArea))
            //                        {
            //                            ticket.ResolutionTicketArea = ticketAreas.FirstOrDefault(x => resolutionArea.Equals(x.Value));
            //                            if (ticket.ResolutionTicketArea == null)
            //                            {
            //                                ticket.ResolutionTicketArea = new TicketArea
            //                                {
            //                                    Value = resolutionArea
            //                                };

            //                                ticketAreas.Add(ticket.ResolutionTicketArea);
            //                            }
            //                        }
            //                    }
            //                    else
            //                    {
            //                        ticket.TicketResolutionCode = null;
            //                        ticket.ResolutionTicketArea = null;
            //                    }
            //                }
            //                if (ticket.LastActivityUpdate != xmlLastActivityUpdate)
            //                {
            //                    ticket.LastActivityUpdate = xmlLastActivityUpdate;
            //                    hasUpdate = true;
            //                    if (!string.IsNullOrWhiteSpace(ticket.LastActivityUpdate))
            //                    {
            //                        ticket.LastActivityUpdateDate = new DateTime?(Convert.ToDateTime(ticket.LastActivityUpdate));
            //                    }
            //                    else
            //                    {
            //                        ticket.LastActivityUpdateDate = null;
            //                    }
            //                }
            //                // now we should be at the </Ticket> tag
            //                if (hasUpdate)
            //                {
            //                    updatedTickets.Add(ticket);
            //                }
            //                reader.Read(); // now at either </Tickets> or <Ticket>

            //            }
            //            catch (Exception ex)
            //            {
            //                // log the problem
            //                LogToTelligent(ex.ToString());
            //                // add to bad ticket list

            //                // return ex.ToString();
            //            }

            //            tickets.Remove(ticket);


            //        foreach (var missingTicket in tickets)
            //        {
            //            if (!lstErrorTickets.Contains(missingTicket.Number))
            //            {
            //                if (missingTicket.EntityState != System.Data.EntityState.Modified || (missingTicket.IsActive && (!missingTicket.Updated.HasValue || !now.Equals(missingTicket.Updated.Value))))
            //                {
            //                    missingTicket.Updated = now;
            //                    missingTicket.IsActive = false;
            //                }
            //            }
            //        }

            //        //db.SaveChanges(System.Data.Objects.SaveOptions.DetectChangesBeforeSave);
            //        //db.SaveChanges(System.Data.Objects.SaveOptions.AcceptAllChangesAfterSave);
            //        //db.AcceptAllChanges();
            //        db.SaveChanges();
                   
            //    }

            //    reader.Close();

            //    //foreach (var ticket in updatedTickets)
            //    //{
            //    //    var content = new StringBuilder();
            //    //    content.Append("Ticket Number:");
            //    //    content.AppendLine(ticket.Number);
            //    //    content.Append("CSN:");
            //    //    content.AppendLine(ticket.CSN);
            //    //    content.Append("Abstract:");
            //    //    content.AppendLine(ticket.Abstract);
            //    //    content.Append("Status:");
            //    //    content.AppendLine(ticket.Status);
            //    //    content.Append("Sub Status:");
            //    //    content.AppendLine(ticket.SubStatus);
            //    //    content.Append("Last Update Date:");
            //    //    content.AppendLine(ticket.LastActivityUpdate);
            //    //    var db = new Entities();
            //    //    var relatedCsn = db.OmnicellCSNs.SingleOrDefault(x => ticket.CSN.Equals(x.CSN));

            //    //}
            //}
            //  //  count = ImmediateUpdate(updatedTickets);
            ////catch (Exception ex)
            ////{
            ////    // log the problem
            ////    LogToTelligent(ex.ToString());
            ////    return ex.ToString();
            ////}
            //// append to the tickets.txt file
        //        return count.ToString(); //"success. List of Bad SRNs: " + string.Join(",", lstErrorTickets.ToArray()); ;
        //}

        public int ImmediateUpdate(List<Omnicell_Ticket> tickets)
        {
            int count = 0;
            using (var db = new OmnicellEntities())
            {
                
                var users = db.Omnicell_TicketEmailInfo.Where(x => x.IsImmediateUpdate == 1);
 
                var emailTickets = new List<Omnicell_Ticket>();

                foreach(var user in users) 
                {
    
                    foreach (var ticket in tickets)
                    {
                        var sticket = db.Omnicell_TicketSubscription.FirstOrDefault(x => x.TicketId == ticket.Id && x.UserId == user.UserId); ;

                       if (sticket != null)
                       {
                           emailTickets.Add(ticket);
                       }

                    }

                   if (emailTickets.Count != 0)
                   {
                        BuildEmail(emailTickets, user.Email, user.UserId);
                   }

                   emailTickets.Clear();
                }

            }

            return count;
            
        }


        public void SendEmail(string fromEmail, string fromName, string toEmail, string toName, string subject, string body)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            try
            {

                if (String.IsNullOrEmpty(fromName)) fromName = fromEmail;
                if (String.IsNullOrEmpty(toName)) toName = toEmail;

                mail.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                mail.Subject = subject;
                mail.Body = body.ToString();
                mail.To.Add(new System.Net.Mail.MailAddress(toEmail, toName));

                Telligent.Evolution.Components.CSContext context = Telligent.Evolution.Components.CSContext.Current;

                //string first_name = context.Context.Request["first_name"];

                // SMR - 20130828 - Leave this here because it is used by Telligent. 
                HttpFileCollection fileAttachments = context.Context.Request.Files;
                if (fileAttachments != null)
                {
                    foreach (String fileKey in fileAttachments)
                    {
                        mail.Attachments.Add(new System.Net.Mail.Attachment(fileAttachments[fileKey].InputStream, fileAttachments[fileKey].FileName.ToString()));
                    }
                }
                
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient("localhost");
                client.Credentials = CredentialCache.DefaultNetworkCredentials;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.Send(mail);

            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("2 - Message: {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);

            }

        }


        public void SendEmail(string fromEmail, string fromName, string toEmail, string toName, string subject, string body, HttpFileCollection fileAttachments)
        {
            MailMessage mail = new MailMessage();
            try
            {

                if (String.IsNullOrEmpty(fromName)) fromName = fromEmail;
                if (String.IsNullOrEmpty(toName)) toName = toEmail;

                mail.From = new MailAddress(fromEmail, fromName);
                mail.Subject = subject;
                mail.Body = body.ToString();
                mail.To.Add(new MailAddress(toEmail, toName));

                if (fileAttachments != null)
                {
                    foreach (String fileKey in fileAttachments)
                    {
                        mail.Attachments.Add(new System.Net.Mail.Attachment(fileAttachments[fileKey].InputStream, fileAttachments[fileKey].FileName.ToString()));
                    }
                }

                SmtpClient client = new SmtpClient("localhost");
                client.Send(mail);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("3 - Message: {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);
            }

        }



        public string SendEmail(string fromEmail, string fromName, string toEmail, string toName, string subject, string body, string smtpServer, string contextId = "", bool isMobile = false)
        {
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            try
            {

                if (String.IsNullOrEmpty(fromName)) fromName = fromEmail;
                if (String.IsNullOrEmpty(toName)) toName = toEmail;

                mail.From = new System.Net.Mail.MailAddress(fromEmail, fromName);
                mail.Subject = subject;
                mail.Body = body.ToString();
                mail.To.Add(new System.Net.Mail.MailAddress(toEmail, toName));
                System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(smtpServer);
                if (isMobile)
                {
                    mail.IsBodyHtml = true;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                }
                else
                {
                    //HttpContext context = HttpContext.Current;
                    Telligent.Evolution.Components.CSContext context = Telligent.Evolution.Components.CSContext.Current;
               
                    // SMR - 20130828 - Leave this here because it is used by Telligent.
                    
                    HttpFileCollection fileAttachments = context.Context.Request.Files;
                    
                    if (fileAttachments != null)
                    {
                        foreach (String fileKey in fileAttachments)
                        {
                            fileAttachments[fileKey].InputStream.Position = 0;
                            mail.Attachments.Add(new System.Net.Mail.Attachment(fileAttachments[fileKey].InputStream, fileAttachments[fileKey].FileName.ToString()));
                        }
                    }
                }
                
                client.Send(mail);

                return "Send Successful";
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Message: {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);

                return sb.ToString();
            }
        }

        public string SendEmail(string fromEmail, string fromName, string toEmail, string toName, string subject, string body, string smtpServer, int port, bool enableSsl, string loginName, string password)
        {
            try
            {

                var fromAddress = new MailAddress(fromEmail, fromName);
                var toAddress = new MailAddress(toEmail, toName);

                var smtp = new SmtpClient
                {
                    Host = smtpServer,
                    Port = port,
                    EnableSsl = enableSsl,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(loginName, password)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {

                    //HttpContext context = HttpContext.Current;
                    Telligent.Evolution.Components.CSContext context = Telligent.Evolution.Components.CSContext.Current;

                    HttpFileCollection fileAttachments = context.Context.Request.Files;
                    if (fileAttachments != null)
                    {
                        foreach (String fileKey in fileAttachments)
                        {
                            fileAttachments[fileKey].InputStream.Position = 0;
                            message.Attachments.Add(new System.Net.Mail.Attachment(fileAttachments[fileKey].InputStream, fileAttachments[fileKey].FileName.ToString()));
                        }
                    }


                    smtp.Send(message);
                }


                return "Send Successful";

            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Message: {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);

                return sb.ToString();
            }

        }

        public static bool IsValidEmail(string email)
        {
            if (String.IsNullOrEmpty(email)) return false;

            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            Regex re = new Regex(strRegex);

            return (re.IsMatch(email)) ? true : false;
        }

        public static bool IsValidGuid(string guid)
        {
            if (String.IsNullOrEmpty(guid)) return false;

            string strRegex = @"^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$";

            Regex re = new Regex(strRegex);

            return (re.IsMatch(guid)) ? true : false;
        }

        //public string ParseTicketAsXMLDoc(Stream fileStream)
        //{
//            try
//            {
//                var now = DateTime.Now;
//                fileStream.Position = 0;

//                // Scrub non-printable ascii characters
//                StreamReader sr = new StreamReader(fileStream);

//                string bufferStreamToString = sr.ReadToEnd();

//                bufferStreamToString = bufferStreamToString.Replace(Convert.ToChar((byte)0x1F), ' ');
////                Regex regex = new Regex(@">\n\r*<");
////                string cleanedXml = regex.Replace(bufferStreamToString, "><",10);
//                string cleanedXml = bufferStreamToString.Replace("\n\r", "");
//                cleanedXml = cleanedXml.Replace("\n", "").Replace("\r", "");
////                string cleanedXml = bufferStreamToString.Replace(System.Environment.NewLine, "");

//                // we have a string. Load it into an xmldoc
//                XmlDocument xdoc = new XmlDocument();
//                xdoc.PreserveWhitespace = false;
//                xdoc.LoadXml(cleanedXml);

//                var updatedTickets = new List<Omnicell_Ticket>();

//                XmlElement root = xdoc.DocumentElement;
//                XmlNodeList nodes = root.SelectNodes("Ticket");
//                foreach (XmlNode node in nodes)
//                {
//                    string tktNumber = node["Number"].InnerText;
//                    string tktAbstract = node["Abstract"].InnerText;
//                    string tktStatus = node["Status"].InnerText;
//                    string tktSubStatus = node["Sub-Status"].InnerText;
//                    string tktCSN = node["CSN"].InnerText;
//                    string tktSymptom_Code = node["Symptom_Code"].InnerText;
//                    string tktResolution_Code = node["Resolution_Code"].InnerText;
//                    string tktLast_Activity_Update = node["Last_Activity_Update"].InnerText;
//                    string tktSerial_Number = node["Serial_Number"].InnerText;

//                    using (var db = new OmnicellEntities())
//                    {
//                        var tickets = db.Omnicell_Ticket.Where(x => x.IsActive).ToList();
//                        var ticketAreas = db.Omnicell_TicketArea.ToList();
//                        var ticketResolutionCodes = db.Omnicell_TicketResolutionCode.ToList();
//                        var ticketSymptomCodes = db.Omnicell_TicketSymptomCode.ToList();
//                        var ticketStatuses = db.Omnicell_TicketStatus.ToList();
//                        var ticketSubStatuses = db.Omnicell_TicketSubStatus.ToList();
//                        var hasUpdate = false;

//                        tktNumber = tktNumber == null ? null : tktNumber.Trim();
//                        var ticket = tickets.FirstOrDefault(x => tktNumber.Equals(x.Number));
//                        // could be inactive now
//                        if (ticket == null)
//                        {
//                            ticket = db.Omnicell_Ticket.FirstOrDefault(x => tktNumber.Equals(x.Number));
//                        }

//                        if (ticket == null)
//                        {
//                            ticket = new Omnicell_Ticket
//                            {
//                                Number = tktNumber
//                            };
//                            db.Omnicell_Ticket.AddObject(ticket);
//                        }
//                        ticket.IsActive = true;
//                        ticket.Updated = now;
//                        if (ticket.SerialNumber != tktSerial_Number)
//                        {
//                            ticket.SerialNumber = tktSerial_Number;
//                            hasUpdate = true;
//                        }

//                        if (ticket.Abstract != tktAbstract)
//                        {
//                            ticket.Abstract = tktAbstract;
//                            hasUpdate = true;
//                        }

//                        if (ticket.CSN != tktCSN)
//                        {
//                            ticket.CSN = tktCSN;
//                            hasUpdate = true;
//                        }

//                        if (ticket.Status != tktStatus)
//                        {
//                            ////ticket.Status = tktStatus;
//                            ////hasUpdate = true;
//                            ////if (!string.IsNullOrWhiteSpace(tktStatus))
//                            ////{
//                            ////    ticket.TicketStatus = ticketStatuses.FirstOrDefault(x => ticket.Status.Equals(x.Value));
//                            ////    if (ticket.TicketStatus == null)
//                            ////    {
//                            ////        ticket.TicketStatus = new TicketStatus
//                            ////        {
//                            ////            Value = ticket.Status
//                            ////        };

//                            ////        ticketStatuses.Add(ticket.TicketStatus);
//                            ////    }
//                            //}
//                            //else
//                            //{
//                            //    ticket.TicketStatus = null;
//                            //}
//                        }
//                        //if (ticket.SubStatus != tktSubStatus)
//                        //{
//                        //    ticket.SubStatus = tktSubStatus;
//                        //    hasUpdate = true;
//                        //    if (!string.IsNullOrWhiteSpace(ticket.SubStatus))
//                        //    {
//                        //        ticket.TicketSubStatus = ticketSubStatuses.FirstOrDefault(x => ticket.SubStatus.Equals(x.Value));
//                        //        if (ticket.TicketSubStatus == null)
//                        //        {
//                        //            ticket.TicketSubStatus = new TicketSubStatus
//                        //            {
//                        //                Value = ticket.SubStatus
//                        //            };

//                        //            ticketSubStatuses.Add(ticket.TicketSubStatus);
//                        //        }
//                        //    }
//                        //    else
//                        //    {
//                        //        ticket.TicketSubStatus = null;
//                        //    }
//                        //}

//                        if (ticket.SymptomCode != tktSymptom_Code)
//                        {
//                            ticket.SymptomCode = tktSymptom_Code;
//                            hasUpdate = true;
//                            if (!string.IsNullOrWhiteSpace(ticket.SymptomCode))
//                            {
//                                var values = ticket.SymptomCode.Split(new string[] { " - " }, 2, StringSplitOptions.None);
//                                var symptomCode = values.ElementAtOrDefault(0);
//                                symptomCode = symptomCode == null ? null : symptomCode.Trim();
//                                var symptomArea = values.ElementAtOrDefault(1);
//                                symptomArea = symptomArea == null ? null : symptomArea.Trim();

//                                if (!string.IsNullOrWhiteSpace(symptomCode))
//                                {
//                                    ticket.TicketSymptomCode = ticketSymptomCodes.FirstOrDefault(x => symptomCode.Equals(x.Value));
//                                    if (ticket.TicketSymptomCode == null)
//                                    {
//                                        ticket.TicketSymptomCode = new TicketSymptomCode
//                                        {
//                                            Value = symptomCode
//                                        };

//                                        ticketSymptomCodes.Add(ticket.TicketSymptomCode);
//                                    }
//                                }

//                                if (!string.IsNullOrWhiteSpace(symptomArea))
//                                {
//                                    ticket.SymptomTicketArea = ticketAreas.FirstOrDefault(x => symptomArea.Equals(x.Value));
//                                    if (ticket.SymptomTicketArea == null)
//                                    {
//                                        ticket.SymptomTicketArea = new TicketArea
//                                        {
//                                            Value = symptomArea
//                                        };

//                                        ticketAreas.Add(ticket.SymptomTicketArea);
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                ticket.TicketSymptomCode = null;
//                                ticket.SymptomTicketArea = null;
//                            }
//                        }

//                        if (ticket.ResolutionCode != tktResolution_Code)
//                        {
//                            ticket.ResolutionCode = tktResolution_Code;
//                            hasUpdate = true;
//                            if (!string.IsNullOrWhiteSpace(ticket.ResolutionCode))
//                            {
//                                var values = ticket.ResolutionCode.Split(new string[] { " - " }, 2, StringSplitOptions.None);
//                                var resolutionCode = values.ElementAtOrDefault(0);
//                                resolutionCode = resolutionCode == null ? null : resolutionCode.Trim();
//                                var resolutionArea = values.ElementAtOrDefault(1);
//                                resolutionArea = resolutionArea == null ? null : resolutionArea.Trim();

//                                if (!string.IsNullOrWhiteSpace(resolutionCode))
//                                {
//                                    ticket.TicketResolutionCode = ticketResolutionCodes.FirstOrDefault(x => resolutionCode.Equals(x.Value));
//                                    if (ticket.TicketResolutionCode == null)
//                                    {
//                                        ticket.TicketResolutionCode = new TicketResolutionCode
//                                        {
//                                            Value = resolutionCode
//                                        };

//                                        ticketResolutionCodes.Add(ticket.TicketResolutionCode);
//                                    }
//                                }

//                                if (!string.IsNullOrWhiteSpace(resolutionArea))
//                                {
//                                    ticket.ResolutionTicketArea = ticketAreas.FirstOrDefault(x => resolutionArea.Equals(x.Value));
//                                    if (ticket.ResolutionTicketArea == null)
//                                    {
//                                        ticket.ResolutionTicketArea = new TicketArea
//                                        {
//                                            Value = resolutionArea
//                                        };

//                                        ticketAreas.Add(ticket.ResolutionTicketArea);
//                                    }
//                                }
//                            }
//                            else
//                            {
//                                ticket.TicketResolutionCode = null;
//                                ticket.ResolutionTicketArea = null;
//                            }
//                        }
//                        if (ticket.LastActivityUpdate != tktLast_Activity_Update)
//                        {
//                            ticket.LastActivityUpdate = tktLast_Activity_Update;
//                            hasUpdate = true;
//                            if (!string.IsNullOrWhiteSpace(ticket.LastActivityUpdate))
//                            {
//                                ticket.LastActivityUpdateDate = new DateTime?(Convert.ToDateTime(ticket.LastActivityUpdate));
//                            }
//                            else
//                            {
//                                ticket.LastActivityUpdateDate = null;
//                            }
//                        }
//                        if (hasUpdate)
//                        {
//                            updatedTickets.Add(ticket);
//                        }
//                        tickets.Remove(ticket);
//                        foreach (var missingTicket in tickets)
//                        {
//                            if (missingTicket.EntityState != System.Data.EntityState.Modified || (missingTicket.IsActive && (!missingTicket.Updated.HasValue || !now.Equals(missingTicket.Updated.Value))))
//                            {
//                                missingTicket.Updated = now;
//                                missingTicket.IsActive = false;
//                            }
//                        }
//                        db.SaveChanges();
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                // log the problem
//                LogToTelligent(ex.ToString());
//                return ex.ToString();
//            }
//            return "success";
      //}

        // this method builds the email for the service request ticket digest.
        public void BuildEmail(List<Omnicell_Ticket> tickets, string email, int? userId)
        {
            var content = new StringBuilder();
            content.Append("Below is your daily digest of updated service request tickets from myOmnicell:");
            content.Append(Environment.NewLine);
            content.Append(Environment.NewLine);
            string csn = "";

            List<string> exists = new List<string>();

            foreach (var ticket in tickets)
            {
                content.Append("Ticket Number: ");
                content.Append(ticket.Number);
                content.Append(Environment.NewLine);
                content.Append("CSN: ");
                content.Append(ticket.CSN);
                content.Append(Environment.NewLine);
                content.Append("Abstract: ");
                content.Append(ticket.Abstract);
                content.Append(Environment.NewLine);
                content.Append("Status: ");
                content.Append(ticket.Status);
                content.Append(Environment.NewLine);
                content.Append("Sub Status: ");
                content.AppendLine(ticket.SubStatus);
                content.Append(Environment.NewLine);
                content.Append("Last Update Date: ");
                content.AppendLine(ticket.Updated.Value.Date.ToString("MM/dd/yyyy"));
                content.Append(Environment.NewLine);
                content.AppendLine("___________________________________________________________________");
                content.Append(Environment.NewLine);

                if (exists.Count == 0)
                {
                    exists.Add(ticket.CSN);
                    csn += ticket.CSN + ",";
                }
                if (!exists.Exists(x => x == ticket.CSN))
                {
                    exists.Add(ticket.CSN);
                    csn += ticket.CSN + ",";
                }

            }


            string allCsn = csn.Remove(csn.LastIndexOf(','));
            string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string unsubscribe = string.Format(baseUrl + "/customer_portal/p/service-request-tickets.aspx?CSN={0}", allCsn);
            content.Append("To unsubscribe from service ticket notifications, click the link below and follow the instructions on the Service Request Tickets homepage.");
            content.Append(Environment.NewLine);
            content.Append(unsubscribe);

            string fromEmail = "noreply-myomnicell@omnicell.com";
            string toEmail = email;
            string toName = email;
            string fromName = "noreply-myomnicell@omnicell.com";
            string subject = "myOmnicell Service Request Ticket Digest";
            string body = content.ToString();

            SendEmail(fromEmail, fromName, toEmail, toName, subject, body);
            insertEmailCount(userId);
        }

        public void insertEmailCount(int? userId)
        {
            var entities = new OmnicellEntities();
            try
            {
                var dbEntity = new Omnicell_EmailCounter()
                {
                    UserId = Convert.ToInt32(userId),
                    Date = DateTime.Now
                };
                entities.Omnicell_EmailCounter.AddObject(dbEntity);
                entities.SaveChanges();
            }
            catch (Exception ex)
            {
                // log the problem
                LogToTelligent(ex.ToString());
            }
            
        }
    }
}

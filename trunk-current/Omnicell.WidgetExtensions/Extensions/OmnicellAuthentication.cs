﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Security;
using Omnicell.Data.Model;
using Telligent.Evolution.Extensibility.Api.Entities.Version1;
using Telligent.Evolution.Extensibility.UI.Version1;
using Telligent.Evolution.Extensibility.Version1;
using Omnicell.Services;
using System.Data.Objects;
//using Omnicell.Services.OmnicellSalesforceService;
using System.Configuration;
using WidgetExtensions.SforceService;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace WidgetExtensions.Extensions
{
    public class OmnicellAuthentication
    {

        #region Media Audience Type

        public AdditionalInfo DeleteAudienceType(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_MediaAudienceType.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetAudienceType(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_MediaAudienceType.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateAudienceType(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_MediaAudienceType.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_MediaAudienceType();
                        db.Omnicell_MediaAudienceType.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListAudienceTypes()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_MediaAudienceType.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region Media Document Types

        public AdditionalInfo DeleteDocumentType(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_MediaDocumentType.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetDocumentType(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_MediaDocumentType.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateDocumentType(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_MediaDocumentType.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_MediaDocumentType();
                        db.Omnicell_MediaDocumentType.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListDocumentTypes()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_MediaDocumentType.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region Media Format

        public AdditionalInfo DeleteFormatType(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_MediaFormat.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetFormatType(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_MediaFormat.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateFormatType(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_MediaFormat.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_MediaFormat();
                        db.Omnicell_MediaFormat.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListFormatTypes()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_MediaFormat.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region Media Gallery Type

        public AdditionalInfo DeleteGalleryType(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_MediaGalleryType.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetGalleryType(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_MediaGalleryType.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateGalleryType(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_MediaGalleryType.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_MediaGalleryType();
                        db.Omnicell_MediaGalleryType.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListGalleryTypes()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_MediaGalleryType.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region Media Market

        public AdditionalInfo DeleteMarketType(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_MediaMarket.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetMarketType(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_MediaMarket.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateMarketType(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_MediaMarket.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_MediaMarket();
                        db.Omnicell_MediaMarket.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListMarketTypes()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_MediaMarket.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region Product Type

        public AdditionalInfo DeleteProductType(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_ProductType.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetProductType(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_ProductType.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateProductType(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_ProductType.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_ProductType();
                        db.Omnicell_ProductType.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListProductTypes()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_ProductType.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region TicketArea

        public AdditionalInfo DeleteTicketArea(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_TicketArea.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetTicketArea(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_TicketArea.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateTicketArea(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_TicketArea.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_TicketArea();
                        db.Omnicell_TicketArea.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListTicketAreas()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_TicketArea.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region Ticket Resolution Code

        public AdditionalInfo DeleteTicketResolutionCode(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_TicketResolutionCode.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetTicketResolutionCode(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_TicketResolutionCode.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateTicketResolutionCode(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_TicketResolutionCode.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_TicketResolutionCode();
                        db.Omnicell_TicketResolutionCode.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListTicketResolutionCodes()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_TicketResolutionCode.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region Ticket Status

        public AdditionalInfo DeleteTicketStatus(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_TicketStatus.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetTicketStatus(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_TicketStatus.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateTicketStatus(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_TicketStatus.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_TicketStatus();
                        db.Omnicell_TicketStatus.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListTicketStatuss()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_TicketStatus.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region Ticket Sub Status

        public AdditionalInfo DeleteTicketSubStatus(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_TicketSubStatus.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetTicketSubStatus(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_TicketSubStatus.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateTicketSubStatus(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_TicketSubStatus.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_TicketSubStatus();
                        db.Omnicell_TicketSubStatus.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListTicketSubStatuss()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_TicketSubStatus.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region Ticket Symptom Code

        public AdditionalInfo DeleteTicketSymptomCode(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_TicketSymptomCode.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetTicketSymptomCode(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_TicketSymptomCode.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateTicketSymptomCode(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_TicketSymptomCode.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_TicketSymptomCode();
                        db.Omnicell_TicketSymptomCode.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListTicketSymptomCodes()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_TicketSymptomCode.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region Email Batch Status

        public AdditionalInfo DeleteEmailBatchStatus(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_EmailBatchStatus.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetEmailBatchStatus(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_EmailBatchStatus.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateEmailBatchStatus(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_EmailBatchStatus.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_EmailBatchStatus();
                        db.Omnicell_EmailBatchStatus.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListEmailBatchStatuss()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_EmailBatchStatus.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region Email Batch Recipient Status

        public AdditionalInfo DeleteEmailBatchRecipientStatus(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_EmailBatchRecipientStatus.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public LookupType GetEmailBatchRecipientStatus(int id)
        {
            LookupType lookupType = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_EmailBatchRecipientStatus.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                lookupType = new LookupType
                {
                    Id = dbEntity.Id,
                    Value = dbEntity.Value
                };
            }

            return lookupType;
        }

        public LookupType UpdateEmailBatchRecipientStatus(int id, string value)
        {
            LookupType lookupType = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_EmailBatchRecipientStatus.SingleOrDefault(x => x.Id == id);
                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_EmailBatchRecipientStatus();
                        db.Omnicell_EmailBatchRecipientStatus.AddObject(dbEntity);
                    }

                    dbEntity.Value = value;
                    db.SaveChanges();
                }

                lookupType = new LookupType
                {
                    Id = id,
                    Value = value
                };
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Lookup Type (id=" + id + ", value=" + value + "):" + ex.ToString());
            }

            return lookupType;
        }

        public List<LookupType> ListEmailBatchRecipientStatuss()
        {
            var db = new OmnicellEntities();
            var list = new List<LookupType>();
            var dbList = db.Omnicell_EmailBatchRecipientStatus.ToList();
            dbList.ForEach(x => list.Add(new LookupType { Id = x.Id, Value = x.Value }));
            return list;
        }

        #endregion

        #region Ticket Subscription

        public AdditionalInfo DeleteTicketSubscription(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_TicketSubscription.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Lookup Type (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public AdditionalInfo DeleteTicketSubscription(int userId, int ticketId)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    Omnicell_TicketSubscription dbEntity = null;

                    if (ticketId != 0)
                    {
                        dbEntity = db.Omnicell_TicketSubscription.SingleOrDefault(x => x.UserId.HasValue && x.UserId.Value == userId && x.TicketId.HasValue && x.TicketId.Value == ticketId);
                    }
                    else
                    {
                        dbEntity = db.Omnicell_TicketSubscription.SingleOrDefault(x => x.UserId.HasValue && x.UserId.Value == userId);
                    }

                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Ticket Subscription (userId=" + userId + ", ticketId=" + ticketId + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }

        public OmnicellTicketSubscription GetTicketSubscription(int id)
        {
            OmnicellTicketSubscription ticketSubscription = null;

            var db = new OmnicellEntities();
            var dbEntity = db.Omnicell_TicketSubscription.SingleOrDefault(x => x.Id == id);
            if (dbEntity != null)
            {
                ticketSubscription = new OmnicellTicketSubscription(dbEntity);
                ticketSubscription.Ticket = new OmnicellTicket(dbEntity.Omnicell_Ticket);
            }

            return ticketSubscription;
        }

        public OmnicellTicketSubscription GetTicketSubscription(int userId, int ticketId)
        {
            OmnicellTicketSubscription ticketSubscription = null;

            var db = new OmnicellEntities();
            Omnicell_TicketSubscription dbEntity = null;
            if (ticketId != 0)
            {
                dbEntity = db.Omnicell_TicketSubscription.SingleOrDefault(x => x.UserId.HasValue && x.UserId.Value == userId && x.TicketId.HasValue && x.TicketId.Value == ticketId);
            }
            else
            {
                dbEntity = db.Omnicell_TicketSubscription.SingleOrDefault(x => x.UserId.HasValue && x.UserId.Value == userId);
            }

            if (dbEntity != null)
            {
                ticketSubscription = new OmnicellTicketSubscription(dbEntity);
                ticketSubscription.Ticket = new OmnicellTicket(dbEntity.Omnicell_Ticket);
            }

            return ticketSubscription;
        }

        public OmnicellTicketSubscription UpdateTicketSubscription(int userId, int ticketId)
        {
            OmnicellTicketSubscription ticketSubscription = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    Omnicell_TicketSubscription dbEntity = null;
                    if (ticketId > 0)
                    {
                        dbEntity = db.Omnicell_TicketSubscription.SingleOrDefault(x => x.UserId == userId && x.TicketId == ticketId);
                        var deleteEntity = db.Omnicell_TicketSubscription.SingleOrDefault(x => x.UserId == userId && !x.TicketId.HasValue);
                        if (deleteEntity != null)
                        {
                            db.Omnicell_TicketSubscription.DeleteObject(deleteEntity);
                        }
                    }
                    else
                    {
                        dbEntity = db.Omnicell_TicketSubscription.SingleOrDefault(x => x.UserId == userId && !x.TicketId.HasValue);
                        foreach (var deleteEntity in db.Omnicell_TicketSubscription.Where(x => x.UserId == userId && x.TicketId.HasValue))
                        {
                            db.Omnicell_TicketSubscription.DeleteObject(deleteEntity);
                        }
                    }

                    if (dbEntity == null)
                    {
                        dbEntity = new Omnicell_TicketSubscription
                        {
                            UserId = userId,
                            TicketId = ticketId == 0 ? (int?)null : ticketId
                        };
                        db.Omnicell_TicketSubscription.AddObject(dbEntity);
                    }

                    db.SaveChanges();
                    ticketSubscription = new OmnicellTicketSubscription(dbEntity);
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating Ticket Subscription (userId=" + userId + ", ticketId=" + ticketId + "):" + ex.ToString());
            }

            return ticketSubscription;
        }

        public List<OmnicellTicketSubscription> ListTicketSubscriptions()
        {
            var db = new OmnicellEntities();
            var list = new List<OmnicellTicketSubscription>();
            var dbList = db.Omnicell_TicketSubscription.ToList();
            dbList.ForEach(x => list.Add(new OmnicellTicketSubscription(x) { Ticket = new OmnicellTicket(x.Omnicell_Ticket) }));
            return list;
        }

        public PagedList<OmnicellTicket> ListTickets()
        {
            var db = new OmnicellEntities();
            var list = new List<OmnicellTicket>();
            var dbList = db.Omnicell_Ticket.ToList();
            dbList.ForEach(x =>
            {
                var ticket = new OmnicellTicket(x);
                x.Omnicell_TicketSubscription.ToList().ForEach(y => ticket.TicketSubscriptions.Add(new OmnicellTicketSubscription(y)));
                list.Add(ticket);
            });
            return new PagedList<OmnicellTicket>(list);
        }

        public AdditionalInfo UpdateTicketSubscriptions(int userId, IDictionary dictionary)
        {
            var additionalInfo = new AdditionalInfo();
            try
            {

                bool shouldReceiveAllUpdates = false; 
                int isImmediateUpdate = 1;
                string sendToEmail = null;
                string csn = null;
                List<int> subscribedTicketIds = null;

                // parse the dictionary
                foreach (string item in dictionary.Keys)
                {
                    if (dictionary[item] != null && !string.IsNullOrWhiteSpace(dictionary[item].ToString()))
                    {
                        switch (item)
                        {
                            case "ReceiveAllUpdates":
                                {
                                    var val = new bool?(Convert.ToBoolean(dictionary[item]));
                                    shouldReceiveAllUpdates = !val.HasValue || val.Value;
                                    break;
                                }
                            case "UpdateFrequency":
                                {
                                   int val = Convert.ToInt32(dictionary[item].ToString());
                                    isImmediateUpdate = val;
                                    break;
                                }
                            case "SendToEmail":
                                {
                                    sendToEmail = dictionary[item].ToString();
                                    break;
                                }
                            case "SubscribedTickets":
                                {
                                    var list = dictionary[item].ToString().Split(',').ToList();
                                    if (list.Any())
                                    {
                                        subscribedTicketIds = list.Select(x => int.Parse(x)).ToList();
                                    }

                                    break;
                                }
                            case "CSN":
                                {
                                    csn = dictionary[item].ToString();
                                    break;
                                }
                            default:
                                break;
                        }
                    }
                }


                using (var db = new OmnicellEntities())
                {
                   
                    var ticketEmailInfo = db.Omnicell_TicketEmailInfo.FirstOrDefault(x => x.UserId == userId);
                   
                    if (ticketEmailInfo == null)
                    {
                        ticketEmailInfo = new Omnicell_TicketEmailInfo
                        {
                            UserId = userId
                        };

                        db.Omnicell_TicketEmailInfo.AddObject(ticketEmailInfo);
                    }

                    ticketEmailInfo.IsImmediateUpdate = isImmediateUpdate;
                    ticketEmailInfo.Email = sendToEmail;

                   
                    var existingTicketSubsciptions = db.Omnicell_TicketSubscription.Where(x => x.UserId == userId);

                    if (subscribedTicketIds != null)
                    {

                        string[] csnArray = csn.Split(',');

                        for (int i = 0; i < csnArray.Length; i++)
                        {
                            
                            string current = csnArray[i].Trim();

                            // delete excluded entries
                            foreach (var item in existingTicketSubsciptions.Where(x => x.CSN.Equals(current) && !subscribedTicketIds.Contains(x.TicketId.Value)))
                            {
                                db.Omnicell_TicketSubscription.DeleteObject(item);
                            }

                            // add missing entries
                            var existingTicketIds = existingTicketSubsciptions.Where(x => x.TicketId.HasValue).Select(x => x.TicketId.Value);
                            foreach (var item in subscribedTicketIds.Where(x => !existingTicketIds.Contains(x)))
                            {
                                var ticket = db.Omnicell_Ticket.Where(x => x.Id.Equals(item));
                                if (ticket.Any(x => x.CSN.Equals(current)))
                                {
                                    var ticketSubscription = new Omnicell_TicketSubscription
                                    {
                                        UserId = userId,
                                        TicketId = item,
                                        CSN = current
                                    };

                                    db.Omnicell_TicketSubscription.AddObject(ticketSubscription);
                                }


                            }
                        }

                    }
                    else
                    {
                        if (string.IsNullOrEmpty(csn) == false)
                        {
                            string[] csnArray = csn.Split(',');

                            for (int i = 0; i < csnArray.Length; i++)
                            {

                                string current = csnArray[i].Trim();

                                // delete excluded entries
                                foreach (var item in existingTicketSubsciptions.Where(x => x.CSN.Equals(current)))
                                {
                                    db.Omnicell_TicketSubscription.DeleteObject(item);
                                }
                            }
                        }

                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var error = new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString());
                additionalInfo.Errors.Add(error);
            }

            return additionalInfo;

        }

        public ServiceRequestViewModel GetServiceRequests(int userId, IDictionary dictionary)
        {
            var viewModel = new ServiceRequestViewModel
            {
                UserId = userId
            };

            string sortBy = null, sortOrder = null;
            List<string> csns = null;
            List<OmnicellTicket> tickets = new List<OmnicellTicket>();
            var includeRelatedCsns = false;
            var db = new OmnicellEntities();
            var ticketList = db.Omnicell_Ticket.Include("Omnicell_TicketSubscription").AsQueryable();

            // parse the dictionary
            foreach (string item in dictionary.Keys)
            {
                if (dictionary[item] != null && !string.IsNullOrWhiteSpace(dictionary[item].ToString()))
                {
                    switch (item)
                    {
                        case "IncludeRelatedCsns":
                            {
                                var include = new bool?(Convert.ToBoolean(dictionary[item]));
                                includeRelatedCsns = include.HasValue && include.Value;
                                break;
                            }
                        case "CSN":
                            {
                                var list = dictionary[item].ToString().Split(',').ToList();
                                if (list.Any())
                                {
                                    csns = list.Select(x => x.Trim()).ToList();
                                }

                                viewModel.CSNSearchText = dictionary[item].ToString();
                                break;
                            }
                        case "SortBy":
                            {
                                sortBy = dictionary[item].ToString();
                                break;
                            }
                        case "SortOrder":
                            {
                                sortOrder = dictionary[item].ToString();
                                break;
                            }

                        default:
                            break;
                    }
                }
            }

            // apply the filters
            if (csns != null && csns.Any())
            {
                if (includeRelatedCsns)
                {
                    foreach (var csnItem in csns.ToList())
                    {
                        var dbCsn = db.Omnicell_CSN.SingleOrDefault(x => csnItem.Equals(x.CSN));
                        if (dbCsn != null)
                        {
                            dbCsn.Omnicell_CSNGroup.Omnicell_CSN.ToList().ForEach(x =>
                            {
                                if (!csns.Contains(x.CSN))
                                {
                                    csns.Add(x.CSN);
                                }
                            });
                        }
                    }
                }

                ticketList = ticketList.Where(x => csns.Contains(x.CSN));


                // apply the sorting
                if ("CSN".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        ticketList = ticketList.OrderByDescending(x => x.CSN);
                    }
                    else
                    {
                        ticketList = ticketList.OrderBy(x => x.CSN);
                    }
                }
                else if ("Status".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        ticketList = ticketList.OrderByDescending(x => x.Status);
                    }
                    else
                    {
                        ticketList = ticketList.OrderBy(x => x.Status);
                    }
                }
                else if ("SubStatus".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        ticketList = ticketList.OrderByDescending(x => x.SubStatus);
                    }
                    else
                    {
                        ticketList = ticketList.OrderBy(x => x.SubStatus);
                    }
                }
                else if ("LastUpdateDate".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        ticketList = ticketList.OrderByDescending(x => x.LastActivityUpdateDate.HasValue ? x.LastActivityUpdateDate.Value : DateTime.Now);
                    }
                    else
                    {
                        ticketList = ticketList.OrderBy(x => x.LastActivityUpdateDate.HasValue ? x.LastActivityUpdateDate.Value : DateTime.Now);
                    }
                }
                else if ("TicketNumber".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        ticketList = ticketList.OrderByDescending(x => x.Number);
                    }
                    else
                    {
                        ticketList = ticketList.OrderBy(x => x.Number);
                    }
                }
                else
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        ticketList = ticketList.OrderByDescending(x => x.Abstract);
                    }
                    else
                    {
                        ticketList = ticketList.OrderBy(x => x.Abstract);
                    }
                }

                // populate and map to view model fields
                foreach (var ticket in ticketList)
                {
                    var omnicellTicket = new OmnicellTicket(ticket);
                    if (ticket.Omnicell_TicketSubscription.Any(x => x.UserId.HasValue && x.UserId.Value == userId))
                    {
                        omnicellTicket.TicketSubscription = new OmnicellTicketSubscription(ticket.Omnicell_TicketSubscription.Single(x => x.UserId.HasValue && x.UserId.Value == userId));
                    }
                    else
                    {
                        omnicellTicket.TicketSubscription = new OmnicellTicketSubscription();
                    }

                    tickets.Add(omnicellTicket);
                }

                viewModel.Tickets = tickets;
            }

            var ticketEmailInfo = db.Omnicell_TicketEmailInfo.FirstOrDefault(x => x.UserId == userId);
            if (ticketEmailInfo != null)
            {
                viewModel.HasAllUpdates = db.Omnicell_TicketSubscription.Any(x => x.UserId == userId && !x.TicketId.HasValue);
                viewModel.IsImmediateUpdate = ticketEmailInfo.IsImmediateUpdate;
                viewModel.SendToEmail = ticketEmailInfo.Email;
            }

            return viewModel;
        }

        public List<OmnicellTicket> ListTickets(int userId, IDictionary dictionary)
        {
            string sortBy = null, sortOrder = null;
            List<string> csns = null;
            List<OmnicellTicket> tickets = new List<OmnicellTicket>();
            var includeRelatedCsns = false;

            // parse the dictionary
            foreach (string item in dictionary.Keys)
            {
                if (dictionary[item] != null && !string.IsNullOrWhiteSpace(dictionary[item].ToString()))
                {
                    switch (item)
                    {
                        case "IncludeRelatedCsns":
                            {
                                var include = new bool?(Convert.ToBoolean(dictionary[item]));
                                includeRelatedCsns = include.HasValue && include.Value;
                                break;
                            }
                        case "CSN":
                            {
                                var list = dictionary[item].ToString().Split(',').ToList();
                                if (list.Any())
                                {
                                    csns = list.Select(x => x.Trim()).ToList();
                                }

                                break;
                            }
                        case "SortBy":
                            {
                                sortBy = dictionary[item].ToString();
                                break;
                            }
                        case "SortOrder":
                            {
                                sortOrder = dictionary[item].ToString();
                                break;
                            }

                        default:
                            break;
                    }
                }
            }

            var db = new OmnicellEntities();
            var ticketList = db.Omnicell_Ticket.Include("TicketSubscriptions").AsQueryable();

            // apply the filters
            if (csns != null && csns.Any())
            {
                if (includeRelatedCsns)
                {
                    foreach (var csnItem in csns.ToList())
                    {
                        var dbCsn = db.Omnicell_CSN.SingleOrDefault(x => csnItem.Equals(x.CSN));
                        if (dbCsn != null)
                        {
                            dbCsn.Omnicell_CSNGroup.Omnicell_CSN.ToList().ForEach(x =>
                            {
                                if (!csns.Contains(x.CSN))
                                {
                                    csns.Add(x.CSN);
                                }
                            });
                        }
                    }
                }

                ticketList = ticketList.Where(x => csns.Contains(x.CSN));
            }

            // apply the sorting
            if ("CSN".Equals(sortBy))
            {
                if ("Descending".Equals(sortOrder))
                {
                    ticketList = ticketList.OrderByDescending(x => x.CSN);
                }
                else
                {
                    ticketList = ticketList.OrderBy(x => x.CSN);
                }
            }
            else if ("Status".Equals(sortBy))
            {
                if ("Descending".Equals(sortOrder))
                {
                    ticketList = ticketList.OrderByDescending(x => x.Status);
                }
                else
                {
                    ticketList = ticketList.OrderBy(x => x.Status);
                }
            }
            else if ("SubStatus".Equals(sortBy))
            {
                if ("Descending".Equals(sortOrder))
                {
                    ticketList = ticketList.OrderByDescending(x => x.SubStatus);
                }
                else
                {
                    ticketList = ticketList.OrderBy(x => x.SubStatus);
                }
            }
            else if ("LastUpdateDate".Equals(sortBy))
            {
                if ("Descending".Equals(sortOrder))
                {
                    ticketList = ticketList.OrderByDescending(x => x.LastActivityUpdateDate.HasValue ? x.LastActivityUpdateDate.Value : DateTime.Now);
                }
                else
                {
                    ticketList = ticketList.OrderBy(x => x.LastActivityUpdateDate.HasValue ? x.LastActivityUpdateDate.Value : DateTime.Now);
                }
            }
            else if ("TicketNumber".Equals(sortBy))
            {
                if ("Descending".Equals(sortOrder))
                {
                    ticketList = ticketList.OrderByDescending(x => x.Number);
                }
                else
                {
                    ticketList = ticketList.OrderBy(x => x.Number);
                }
            }
            else
            {
                if ("Descending".Equals(sortOrder))
                {
                    ticketList = ticketList.OrderByDescending(x => x.Abstract);
                }
                else
                {
                    ticketList = ticketList.OrderBy(x => x.Abstract);
                }
            }

            // populate and map to view model fields
            foreach (var ticket in ticketList)
            {
                var omnicellTicket = new OmnicellTicket(ticket);
                if (ticket.Omnicell_TicketSubscription.Any(x => x.UserId.HasValue && x.UserId.Value == userId))
                {
                    omnicellTicket.TicketSubscription = new OmnicellTicketSubscription(ticket.Omnicell_TicketSubscription.Single(x => x.UserId.HasValue && x.UserId.Value == userId));
                }
                else
                {
                    omnicellTicket.TicketSubscription = new OmnicellTicketSubscription();
                }

                tickets.Add(omnicellTicket);
            }

            return tickets;
        }

        public List<OmnicellTicket> ListTop5Tickets(string csn)
        {
            List<string> csns = new List<string>();
            List<OmnicellTicket> tickets = new List<OmnicellTicket>();
            var db = new OmnicellEntities();
            var defaultSortDate = new DateTime();

            csns.Add(csn);
            var dbCsn = db.Omnicell_CSN.SingleOrDefault(x => csn.Equals(x.CSN));
            if (dbCsn != null)
            {
                dbCsn.Omnicell_CSNGroup.Omnicell_CSN.ToList().ForEach(x =>
                {
                    if (!csn.Equals(x.CSN))
                    {
                        csns.Add(x.CSN);
                    }
                });
            }

            var ticketList = db.Omnicell_Ticket
                .Where(x => csns.Contains(x.CSN))
                .OrderByDescending(x => x.LastActivityUpdateDate.HasValue ? x.LastActivityUpdateDate : defaultSortDate)
                .Take(5);

            // populate and map to view model fields
            foreach (var ticket in ticketList)
            {
                var omnicellTicket = new OmnicellTicket(ticket);
                tickets.Add(omnicellTicket);
            }

            return tickets;
        }

        #endregion

        private void LogThis(string text)
        {
            Utilities.LogToTelligent(text);
            //Debug.WriteLine(text);
            //var statusMessage = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.StatusMessages.Create(text);
        }

        [Documentation("Creates a new media post")]
        public OmnicellMedia CreateMedia(int mediaGalleryId, string name, string contentType, string fileName, [Documentation(Name = "Description", Type = typeof(string))][Documentation(Name = "FeaturedImage", Type = typeof(string), Description = "Used to include a featured image when IsFeatured is true.")][Documentation(Name = "FileData", Type = typeof(Byte[]), Description = "FileData or FileUrl is required.")][Documentation(Name = "FileUrl", Type = typeof(string), Description = "FileData or FileUrl is required.")][Documentation(Name = "Tags", Type = typeof(string), Description = "A comma separated list of tags.")][Documentation(Name = "IsFeatured", Type = typeof(bool), Default = false)] IDictionary options)
        {
            Telligent.Evolution.Extensibility.Api.Version1.MediaCreateOptions mediaCreateOption = new Telligent.Evolution.Extensibility.Api.Version1.MediaCreateOptions();
            if (options != null)
            {
                if (options["Description"] != null)
                {
                    mediaCreateOption.Description = options["Description"].ToString();
                }
                if (options["FileData"] != null)
                {
                    mediaCreateOption.FileData = (byte[])options["FileData"];
                }
                if (options["FileUrl"] != null)
                {
                    mediaCreateOption.FileUrl = options["FileUrl"].ToString();
                }
                if (options["Tags"] != null)
                {
                    mediaCreateOption.Tags = options["Tags"].ToString();
                }
                if (options["IsFeatured"] != null)
                {
                    mediaCreateOption.IsFeatured = new bool?(Convert.ToBoolean(options["IsFeatured"]));
                }
                if (options["FeaturedImage"] != null)
                {
                    mediaCreateOption.FeaturedImage = options["FeaturedImage"].ToString();
                }
                List<ExtendedAttribute> extendedAttributes = this.GetExtendedAttributes(options);
                if (extendedAttributes != null)
                {
                    mediaCreateOption.ExtendedAttributes = extendedAttributes;
                }
            }

            var media = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.Media.Create(mediaGalleryId, name, contentType, fileName, mediaCreateOption);

            return UpdateMedia(media, options);

        }

        private List<ExtendedAttribute> GetExtendedAttributes(IDictionary options)
        {
            IDictionary dictionaries = options;
            var attributes = dictionaries["ExtendedAttributes"] as IDictionary;
            var attributesList = new List<ExtendedAttribute>();

            if (attributes != null)
            {
                foreach (var item in attributes.Keys)
                {
                    var attribute = new ExtendedAttribute
                    {
                        Key = item.ToString(),
                        Value = attributes[item].ToString()
                    };

                    attributesList.Add(attribute);
                }

            }

            return attributesList;
        }

        [Documentation("Deletes a media gallery post")]
        public AdditionalInfo DeleteMedia(int mediaGalleryId, int fileId)
        {
            using (var db = new OmnicellEntities())
            {
                while (db.Omnicell_MediaGallery.Any(x => x.MediaGalleryId == fileId))
                {
                    var mediaGallery = db.Omnicell_MediaGallery.First(x => x.MediaGalleryId == fileId);
                    db.Omnicell_MediaGallery.DeleteObject(mediaGallery);
                }

                db.SaveChanges();
            }
            return Telligent.Evolution.Extensibility.Api.Version1.PublicApi.Media.Delete(fileId);
        }

        [Documentation("Gets a media gallery post")]
        public OmnicellMedia GetMedia(int fileId)
        {
            return GetMedia(Telligent.Evolution.Extensibility.Api.Version1.PublicApi.Media.Get(fileId));
        }

        [Documentation("Gets a media gallery post")]
        public OmnicellMedia GetMedia(int mediaGalleryId, int fileId)
        {
            return GetMedia(Telligent.Evolution.Extensibility.Api.Version1.PublicApi.Media.Get(mediaGalleryId, fileId));
        }


        [Documentation("Updates a media post")]
        public OmnicellMedia UpdateMedia(int mediaGalleryId, int fileId, [Documentation(Name = "FeaturedImage", Type = typeof(string), Description = "Used to include a featured image when IsFeatured is true.")][Documentation(Name = "Name", Type = typeof(string))][Documentation(Name = "FileName", Type = typeof(string), Description = "Required when updating FileData or FileUrl.")][Documentation(Name = "Tags", Type = typeof(string), Description = "A comma separated list of tags.")][Documentation(Name = "IsFeatured", Type = typeof(bool))][Documentation(Name = "ContentType", Type = typeof(string), Description = "Required when updating FileData or FileUrl.")][Documentation(Name = "Description", Type = typeof(string))][Documentation(Name = "FileUrl", Type = typeof(string))][Documentation(Name = "FileData", Type = typeof(Byte[]))] IDictionary options)
        {
            Telligent.Evolution.Extensibility.Api.Version1.MediaUpdateOptions mediaUpdateOption = new Telligent.Evolution.Extensibility.Api.Version1.MediaUpdateOptions();
            if (options != null)
            {
                if (options["Name"] != null)
                {
                    mediaUpdateOption.Name = options["Name"].ToString();
                }
                if (options["Description"] != null)
                {
                    mediaUpdateOption.Description = options["Description"].ToString();
                }
                if (options["ContentType"] != null)
                {
                    mediaUpdateOption.ContentType = options["ContentType"].ToString();
                }
                if (options["FileName"] != null)
                {
                    mediaUpdateOption.FileName = options["FileName"].ToString();
                }
                if (options["FileData"] != null)
                {
                    mediaUpdateOption.FileData = (byte[])options["FileData"];
                }
                if (options["FileUrl"] != null)
                {
                    mediaUpdateOption.FileUrl = options["FileUrl"].ToString();
                }
                if (options["Tags"] != null)
                {
                    mediaUpdateOption.Tags = options["Tags"].ToString();
                }
                if (options["IsFeatured"] != null)
                {
                    mediaUpdateOption.IsFeatured = new bool?(Convert.ToBoolean(options["IsFeatured"]));
                }
                if (options["FeaturedImage"] != null)
                {
                    mediaUpdateOption.FeaturedImage = options["FeaturedImage"].ToString();
                }
                List<ExtendedAttribute> extendedAttributes = GetExtendedAttributes(options);
                if (extendedAttributes != null)
                {
                    mediaUpdateOption.ExtendedAttributes = extendedAttributes;
                }
            }

            var media = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.Media.Update(mediaGalleryId, fileId, mediaUpdateOption);
            return UpdateMedia(media, options);
        }

        private HashSet<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group> groups = null;

        private Telligent.Evolution.Extensibility.Api.Entities.Version1.Group GetGroup(int id)
        {
            Telligent.Evolution.Extensibility.Api.Entities.Version1.Group group = null;

            if (groups == null)
            {
                groups = new HashSet<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group>();
            }

            if (groups.Any(x => x.Id == id))
            {
                //group = groups.Single(x => x.Id == id);
                group = groups.First(x => x.Id == id);
            }
            else
            {
                group = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.Groups.Get(new Telligent.Evolution.Extensibility.Api.Version1.GroupsGetOptions { Id = id });
                if (group != null)
                {
                    groups.Add(group);
                }
            }

            return group;
        }

        public PagedList<OmnicellMedia> ListMedia(IDictionary dictionary)
        {
            int? groupId = null, pageIndex = null, pageSize = null, documentTypeId = null, userId = null, mediaGalleryTypeId = null, mediaFormatId = null, productTypeId = null;
            // create nonnullable page size and page index because linq methods do not accept nullable types. 
            int nPageSize = 0;
            int nPageIndex = 0;
            string sortBy = null, sortOrder = null, documentTitle = null;
            IEnumerable<int> documentTypeIds = null, groupIds = null, mediaGalleryTypeIds = null;
            bool isManualSort = true, isManualFilter = false;
            var options = new Telligent.Evolution.Extensibility.Api.Version1.MediaListOptions();
            options.PageSize = int.MaxValue;
            var groupMembership = new Hashtable();

            // parse the dictionary
            foreach (string item in dictionary.Keys)
            {
                if (dictionary[item] != null && !string.IsNullOrWhiteSpace(dictionary[item].ToString()))
                {
                    switch (item)
                    {
                        case "GalleryId":
                            {
                                options.GalleryId = new int?(Convert.ToInt32(dictionary[item]));
                                break;
                            }
                        case "GalleryIds":
                            {
                                options.GalleryIds = dictionary[item].ToString();
                                break;
                            }
                        case "UserId":
                            {
                                userId = new int?(Convert.ToInt32(dictionary[item]));
                                isManualFilter = true;
                                break;
                            }
                        case "GroupId":
                            {
                                var list = dictionary[item].ToString().Split(',').ToList();
                                if (list.Any())
                                {
                                    if (list.Count() > 1)
                                    {
                                        groupIds = list.Select(x => int.Parse(x));
                                    }
                                    else
                                    {
                                        groupId = new int?(Convert.ToInt32(dictionary[item]));
                                    }

                                    isManualFilter = true;
                                }

                                break;
                            }
                        case "PageIndex":
                            {
                                pageIndex = new int?(Convert.ToInt32(dictionary[item]));
                                // set the page index for the linq list sorting code
                                nPageIndex = Convert.ToInt32(pageIndex);
                                break;
                            }
                        case "PageSize":
                            {
                                pageSize = new int?(Convert.ToInt32(dictionary[item]));
                                // set the page size for the linq sorting code
                                nPageSize = Convert.ToInt32(pageSize);
                                break;
                            }
                        case "SortBy":
                            {
                                sortBy = dictionary[item].ToString();
                                break;
                            }
                        case "SortOrder":
                            {
                                sortOrder = dictionary[item].ToString();
                                break;
                            }
                        case "DocumentTypeId":
                            {
                                var list = dictionary[item].ToString().Split(',').ToList();
                                if (list.Any())
                                {
                                    if (list.Count() > 1)
                                    {
                                        documentTypeIds = list.Select(x => int.Parse(x));
                                    }
                                    else
                                    {
                                        documentTypeId = new int?(Convert.ToInt32(dictionary[item]));
                                    }

                                    isManualFilter = true;
                                }

                                break;
                            }
                        case "MediaGalleryTypeId":
                            {
                                var list = dictionary[item].ToString().Split(',').ToList();
                                if (list.Any())
                                {
                                    if (list.Count() > 1)
                                    {
                                        mediaGalleryTypeIds = list.Select(x => int.Parse(x));
                                    }
                                    else
                                    {
                                        mediaGalleryTypeId = new int?(Convert.ToInt32(dictionary[item]));
                                    }

                                    isManualFilter = true;
                                }

                                break;
                            }
                        case "MediaFormatId":
                            {
                                mediaFormatId = new int?(Convert.ToInt32(dictionary[item]));
                                isManualFilter = true;
                                break;
                            }
                        case "ProductTypeId":
                            {
                                productTypeId = new int?(Convert.ToInt32(dictionary[item]));
                                isManualFilter = true;
                                break;
                            }
                        case "DocumentTitle":
                            {
                                documentTitle = dictionary[item].ToString();
                                isManualFilter = true;
                                break;
                            }
                        default:
                            break;
                    }
                }
            }

            // if (sortBy == null || sortBy.Equals("PostDate") || sortBy.Equals("Subject"))
            if (sortBy == null || sortBy.Equals("PostDate"))
            {
                isManualSort = false;
                options.SortOrder = sortOrder;
                options.SortBy = sortBy;
            }
            if (sortBy.Equals("Subject"))
            {
                isManualSort = true;
                options.SortOrder = sortOrder;
                options.SortBy = sortBy;
            }

            //var medias = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.Media.List(options).ToList();
            var medias = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.Media.List(options).ToList();
            var omnicellMedias = new List<OmnicellMedia>();
            var db = new OmnicellEntities();

            foreach (var media in medias)
            {
                var omnicellMedia = GetMedia(media);

                if (isManualFilter && documentTypeId.HasValue)
                {
                    if (omnicellMedia.DocumentType == null || omnicellMedia.DocumentType.Id != documentTypeId)
                    {
                        continue;
                    }
                }

                if (isManualFilter && documentTypeIds != null && documentTypeIds.Any())
                {
                    if (omnicellMedia.DocumentType == null || !documentTypeIds.Contains(omnicellMedia.DocumentType.Id))
                    {
                        continue;
                    }
                }

                if (isManualFilter && mediaGalleryTypeId.HasValue)
                {
                    if (omnicellMedia.MediaGalleryType == null || omnicellMedia.MediaGalleryType.Id != mediaGalleryTypeId)
                    {
                        continue;
                    }
                }

                if (isManualFilter && mediaGalleryTypeIds != null && mediaGalleryTypeIds.Any())
                {
                    if (omnicellMedia.MediaGalleryType == null || !mediaGalleryTypeIds.Contains(omnicellMedia.MediaGalleryType.Id))
                    {
                        continue;
                    }
                }

                if (isManualFilter && mediaFormatId.HasValue)
                {
                    if (omnicellMedia.MediaFormat == null || omnicellMedia.MediaFormat.Id != mediaFormatId)
                    {
                        continue;
                    }
                }

                if (isManualFilter && productTypeId.HasValue)
                {
                    if (omnicellMedia.ProductType == null || omnicellMedia.ProductType.Id != productTypeId)
                    {
                        continue;
                    }
                }

                var groupMemberOptions = new Telligent.Evolution.Extensibility.Api.Version1.GroupUserMembersGetOptions();
                // groupMemberOptions.UserId = userId;

                if (isManualFilter && groupId.HasValue && !omnicellMedia.GroupIds.Contains(groupId.Value))
                {
                    continue;
                }

                if (omnicellMedia.Groups.Any())
                {
                    if (isManualFilter && groupId.HasValue && !omnicellMedia.Groups.Any(x => x.Id == groupId.Value))
                    {
                        continue;
                    }

                    if (isManualFilter && groupIds != null && !omnicellMedia.Groups.Any(x => x.Id.HasValue && groupIds.Contains(x.Id.Value)))
                    {
                        continue;
                    }

                    var hasAccessToMedia = false;

                    foreach (var group in omnicellMedia.Groups.ToList())
                    {

                        if (userId.HasValue && group.Id.HasValue)
                        {
                            if (!groupMembership.Contains(group.Id.Value))
                            {
                                var groupMember = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.GroupUserMembers.Get(group.Id.Value, groupMemberOptions);
                                if (groupMember == null || !groupMember.IsDirectMember.HasValue || groupMember.IsDirectMember.Value)
                                {
                                    groupMembership[group.Id.Value] = false;
                                }
                                else
                                {
                                    groupMembership[group.Id.Value] = true;
                                }
                            }

                            if ((bool)groupMembership[group.Id.Value])
                            {
                                hasAccessToMedia = true;
                            }
                        }

                    }

                    if (hasAccessToMedia)
                    {
                        omnicellMedias.Add(omnicellMedia);
                    }
                }

                omnicellMedias.Add(omnicellMedia);
            }

            // set the total count for the pagedlist
            int totalCount = omnicellMedias.Count;
            if (sortBy.Equals("Name"))
            {
                omnicellMedias = omnicellMedias.OrderBy(x => x.Title).ToList();
            }
            // fill the generic list with only the items to be currently displayed

            omnicellMedias = omnicellMedias.Skip((nPageIndex) * nPageSize).Take(nPageSize).ToList();
            PagedList<OmnicellMedia> pagedOmnicellMedias;

            if (isManualSort)
            {
                if ("MediaGalleryType".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderByDescending(x => x.MediaGalleryType != null ? x.MediaGalleryType.Value : null));
                    }
                    else
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderBy(x => x.MediaGalleryType != null ? x.MediaGalleryType.Value : null));
                    }
                }
                else if ("DocumentType".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderByDescending(x => x.DocumentType != null ? x.DocumentType.Value : null));
                    }
                    else
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderBy(x => x.DocumentType != null ? x.DocumentType.Value : null));
                    }
                }
                else if ("Format".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderByDescending(x => x.MediaFormat != null ? x.MediaFormat.Value : null));
                    }
                    else
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderBy(x => x.MediaFormat != null ? x.MediaFormat.Value : null));
                    }
                }
                else if ("ProductType".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderByDescending(x => x.ProductType != null ? x.ProductType.Value : null));
                    }
                    else
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderBy(x => x.ProductType != null ? x.ProductType.Value : null));
                    }
                }
                else if ("PublishDate".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderByDescending(x => x.PublishDate.HasValue ? x.PublishDate.Value : DateTime.Now));
                    }
                    else
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderBy(x => x.PublishDate.HasValue ? x.PublishDate.Value : DateTime.Now));
                    }
                }
                else if ("ScheduledDate".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderByDescending(x => x.ScheduledDate.HasValue ? x.ScheduledDate.Value : new DateTime()));
                    }
                    else
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderBy(x => x.ScheduledDate.HasValue ? x.ScheduledDate.Value : new DateTime()));
                    }
                }
                else if ("FileType".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderByDescending(x => x.File.ContentType));
                        //pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderByDescending(x => x.DocumentType.Value));
                    }
                    else
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderBy(x => x.File.ContentType));
                        //pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderBy(x => x.DocumentType.Value));
                    }
                }
                else if ("Size".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderByDescending(x => x.File.FileSize));
                    }
                    else
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderBy(x => x.File.FileSize));
                    }
                }
                else if ("Subject".Equals(sortBy))
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderByDescending(x => x.Title));
                    }
                    else
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderBy(x => x.Title));
                    }
                }
                else
                {
                    if ("Descending".Equals(sortOrder))
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderByDescending(x => x.Group.Name));
                    }
                    else
                    {
                        pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias.OrderBy(x => x.Group.Name));
                    }
                }
            }
            else
            {
                pagedOmnicellMedias = new PagedList<OmnicellMedia>(omnicellMedias);
            }

            if (pageIndex.HasValue)
            {
                pagedOmnicellMedias.PageIndex = pageIndex.Value;
            }

            if (pageSize.HasValue)
            {
                pagedOmnicellMedias.PageSize = pageSize.Value;
            }

            // set the pagedlist total count for paging purposes
            pagedOmnicellMedias.TotalCount = totalCount;

            return pagedOmnicellMedias;
        }

        private OmnicellMedia GetMedia(Media media)
        {
            var db = new OmnicellEntities();
            var dbMediaGallery = db.Omnicell_MediaGallery.SingleOrDefault(x => x.MediaGalleryId == media.Id);
            return GetMedia(media, dbMediaGallery);
        }

        private OmnicellMedia GetMedia(Media media, Omnicell_MediaGallery dbMedia)
        {
            var omnicellMedia = new OmnicellMedia(media);

            if (omnicellMedia.GroupId.HasValue)
            {
                omnicellMedia.Group = GetGroup(omnicellMedia.GroupId.Value);
            }

            if (dbMedia != null)
            {
                if (dbMedia.PublishDate.HasValue)
                {
                    omnicellMedia.PublishDate = dbMedia.PublishDate;
                }

                if (dbMedia.ScheduledDate.HasValue)
                {
                    omnicellMedia.ScheduledDate = dbMedia.ScheduledDate;
                }

                if (dbMedia.Omnicell_MediaFormat != null)
                {
                    omnicellMedia.MediaFormat = new LookupType
                    {
                        Id = dbMedia.Omnicell_MediaFormat.Id,
                        Value = dbMedia.Omnicell_MediaFormat.Value
                    };
                }

                if (dbMedia.Omnicell_MediaDocumentType != null)
                {
                    omnicellMedia.DocumentType = new LookupType
                    {
                        Id = dbMedia.Omnicell_MediaDocumentType.Id,
                        Value = dbMedia.Omnicell_MediaDocumentType.Value
                    };
                }

                if (dbMedia.Omnicell_ProductType != null)
                {
                    omnicellMedia.ProductType = new LookupType
                    {
                        Id = dbMedia.Omnicell_ProductType.Id,
                        Value = dbMedia.Omnicell_ProductType.Value
                    };
                }

                if (dbMedia.Omnicell_MediaGalleryType != null)
                {
                    omnicellMedia.MediaGalleryType = new LookupType
                    {
                        Id = dbMedia.Omnicell_MediaGalleryType.Id,
                        Value = dbMedia.Omnicell_MediaGalleryType.Value
                    };
                }

                if (dbMedia.Omnicell_MediaGalleryGroup.Any())
                {
                    var groupIds = dbMedia.Omnicell_MediaGalleryGroup.Select(x => x.GroupId.Value).ToList();
                    omnicellMedia.GroupIds = groupIds;
                    groupIds.ForEach(x => omnicellMedia.Groups.Add(GetGroup(x)));
                }

                if (dbMedia.Omnicell_MediaItemMarket.Any())
                {
                    var mediaMarkets = dbMedia.Omnicell_MediaItemMarket.Select(x => x.Omnicell_MediaMarket).ToList();
                    omnicellMedia.MarketIds = mediaMarkets.Select(x => x.Id).ToList();
                    mediaMarkets.ForEach(x => omnicellMedia.Markets.Add(new LookupType { Id = x.Id, Value = x.Value }));
                }

                if (dbMedia.Omnicell_MediaItemAudience.Any())
                {
                    var mediaAudiences = dbMedia.Omnicell_MediaItemAudience.Select(x => x.Omnicell_MediaAudienceType).ToList();
                    omnicellMedia.AudienceIds = mediaAudiences.Select(x => x.Id).ToList();
                    mediaAudiences.ForEach(x => omnicellMedia.Audiences.Add(new LookupType { Id = x.Id, Value = x.Value }));
                }
            }

            return omnicellMedia;
        }


        private OmnicellMedia UpdateMedia(Media media, IDictionary options)
        {
            OmnicellMedia omnicellMedia = null;

            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbMediaGallery = db.Omnicell_MediaGallery.SingleOrDefault(x => x.MediaGalleryId == media.Id);
                    if (dbMediaGallery == null)
                    {
                        dbMediaGallery = new Omnicell_MediaGallery
                        {
                            MediaGalleryId = media.Id
                        };
                        db.Omnicell_MediaGallery.AddObject(dbMediaGallery);
                    }

                    if (options["Groups"] != null && !string.IsNullOrWhiteSpace(options["Groups"].ToString()))
                    {
                        var groupIds = options["Groups"].ToString().Split(',').ToList().Select(x => int.Parse(x));
                        foreach (var groupId in groupIds)
                        {
                            if (!dbMediaGallery.Omnicell_MediaGalleryGroup.Any(x => x.GroupId == groupId))
                            {
                                var mediaGroup = new Omnicell_MediaGalleryGroup
                                {
                                    MediaGalleryId = media.Id,
                                    GroupId = groupId,
                                    Omnicell_MediaGallery = dbMediaGallery
                                };
                            }
                        }

                        foreach (var mediaGalleryGroup in dbMediaGallery.Omnicell_MediaGalleryGroup.Where(x => x.GroupId.HasValue && !groupIds.Contains(x.GroupId.Value)).ToList())
                        {
                            db.Omnicell_MediaGalleryGroup.DeleteObject(mediaGalleryGroup);
                        }
                    }
                    else
                    {
                        foreach (var mediaGalleryGroup in dbMediaGallery.Omnicell_MediaGalleryGroup.ToList())
                        {
                            db.Omnicell_MediaGalleryGroup.DeleteObject(mediaGalleryGroup);
                        }
                    }

                    if (options["Audiences"] != null && !string.IsNullOrWhiteSpace(options["Audiences"].ToString()))
                    {
                        var audienceIds = options["Audiences"].ToString().Split(',').ToList().Select(x => int.Parse(x));
                        foreach (var audienceId in audienceIds)
                        {
                            if (!dbMediaGallery.Omnicell_MediaItemAudience.Any(x => x.MediaAudienceTypeId == audienceId))
                            {
                                var mediaItemAudience = new Omnicell_MediaItemAudience
                                {
                                    Omnicell_MediaGallery = dbMediaGallery,
                                    MediaAudienceTypeId = audienceId
                                };
                            }
                        }

                        foreach (var mediaItemAudience in dbMediaGallery.Omnicell_MediaItemAudience.Where(x => x.MediaAudienceTypeId.HasValue && !audienceIds.Contains(x.MediaAudienceTypeId.Value)).ToList())
                        {
                            db.Omnicell_MediaItemAudience.DeleteObject(mediaItemAudience);
                        }
                    }
                    else
                    {
                        foreach (var mediaItemAudience in dbMediaGallery.Omnicell_MediaItemAudience.ToList())
                        {
                            db.Omnicell_MediaItemAudience.DeleteObject(mediaItemAudience);
                        }
                    }

                    if (options["Markets"] != null && !string.IsNullOrWhiteSpace(options["Markets"].ToString()))
                    {
                        var marketIds = options["Markets"].ToString().Split(',').ToList().Select(x => int.Parse(x));
                        foreach (var marketId in marketIds)
                        {
                            if (!dbMediaGallery.Omnicell_MediaItemMarket.Any(x => x.MediaMarketId == marketId))
                            {
                                var mediaItemMarket = new Omnicell_MediaItemMarket
                                {
                                    MediaMarketId = marketId,
                                    Omnicell_MediaGallery = dbMediaGallery
                                };
                            }
                        }

                        foreach (var mediaItemMarket in dbMediaGallery.Omnicell_MediaItemMarket.Where(x => x.MediaMarketId.HasValue && !marketIds.Contains(x.MediaMarketId.Value)).ToList())
                        {
                            db.Omnicell_MediaItemMarket.DeleteObject(mediaItemMarket);
                        }
                    }
                    else
                    {
                        foreach (var mediaItemMarket in dbMediaGallery.Omnicell_MediaItemMarket.ToList())
                        {
                            db.Omnicell_MediaItemMarket.DeleteObject(mediaItemMarket);
                        }
                    }

                    if (options["MediaGalleryTypeId"] != null && !string.IsNullOrWhiteSpace(options["MediaGalleryTypeId"].ToString()))
                    {
                        dbMediaGallery.MediaGaleryTypeId = new int?(Convert.ToInt32(options["MediaGalleryTypeId"]));
                    }
                    else
                    {
                        dbMediaGallery.MediaGaleryTypeId = null;
                    }

                    if (options["DocumentTypeId"] != null && !string.IsNullOrWhiteSpace(options["DocumentTypeId"].ToString()))
                    {
                        dbMediaGallery.DocumentTypeId = new int?(Convert.ToInt32(options["DocumentTypeId"]));
                    }
                    else
                    {
                        dbMediaGallery.DocumentTypeId = null;
                    }

                    if (options["MediaFormatId"] != null && !string.IsNullOrWhiteSpace(options["MediaFormatId"].ToString()))
                    {
                        dbMediaGallery.MediaFormatId = new int?(Convert.ToInt32(options["MediaFormatId"]));
                    }
                    else
                    {
                        dbMediaGallery.MediaFormatId = null;
                    }

                    if (options["PublishDate"] != null && !string.IsNullOrWhiteSpace(options["PublishDate"].ToString()))
                    {
                        dbMediaGallery.PublishDate = new DateTime?(Convert.ToDateTime(options["PublishDate"]));
                    }
                    else
                    {
                        dbMediaGallery.PublishDate = null;
                    }

                    if (options["ScheduledDate"] != null && !string.IsNullOrWhiteSpace(options["ScheduledDate"].ToString()))
                    {
                        dbMediaGallery.ScheduledDate = new DateTime?(Convert.ToDateTime(options["ScheduledDate"]));
                    }
                    else
                    {
                        dbMediaGallery.ScheduledDate = null;
                    }

                    if (options["ProductTypeId"] != null && !string.IsNullOrWhiteSpace(options["ProductTypeId"].ToString()))
                    {
                        dbMediaGallery.ProductTypeId = new int?(Convert.ToInt32(options["ProductTypeId"]));
                    }
                    else
                    {
                        dbMediaGallery.ProductTypeId = null;
                    }

                    db.SaveChanges();

                    omnicellMedia = GetMedia(media, dbMediaGallery);
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Updating a Custom Media Gallery Item (id=" + media.Id + "):" + ex.ToString());
            }

            return omnicellMedia;
        }

        public void DeleteGroupOrder(int groupId)
        {
            using (var entities = new OmnicellEntities())
            {
                var groupOrder = entities.Omnicell_GroupOrder.SingleOrDefault(x => x.GroupId == groupId);
                if (groupOrder != null)
                {
                    entities.DeleteObject(groupOrder);
                    entities.SaveChanges();
                }
            }
        }

        public void SaveGroupOrder(int groupId, int ordNo)
        {
            using (var entities = new OmnicellEntities())
            {
                var groupOrder = entities.Omnicell_GroupOrder.SingleOrDefault(x => x.GroupId == groupId);
                if (groupOrder == null)
                {
                    groupOrder = new Omnicell_GroupOrder
                    {
                        GroupId = groupId
                    };
                    entities.Omnicell_GroupOrder.AddObject(groupOrder);
                }
                groupOrder.OrdNo = ordNo;
                entities.SaveChanges();
            }
        }

        public List<Omnicell_GroupOrder> ListGroupOrders()
        {
            var entities = new OmnicellEntities();
            var groupOrders = entities.Omnicell_GroupOrder.OrderBy(x => x.OrdNo);
            return groupOrders.ToList();
        }

        public List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group> ListProductGroups(int parentGroupId)
        {
            var options = new Telligent.Evolution.Extensibility.Api.Version1.GroupsListOptions();
            options.ParentGroupId = parentGroupId;
            options.IncludeAllSubGroups = true;
            options.PageSize = int.MaxValue;
            var groups = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.Groups.List(options).ToList();

            var entities = new OmnicellEntities();
            var groupOrders = entities.Omnicell_GroupOrder.OrderBy(x => x.OrdNo);
            if (groupOrders.Any())
            {
                var groupList = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Group>();

                foreach (var groupOrder in groupOrders)
                {
                    var group = groups.SingleOrDefault(x => x.Id == groupOrder.GroupId);
                    groups.Remove(group);
                    groupList.Add(group);
                }

                //add any left
                groupList.AddRange(groups.OrderBy(x => x.Name));

                return groupList;
            }

            return groups.OrderBy(x => x.Name).ToList();
            // var groups = Telligent.Evolution.Components.Groups.GroupService.GetGroups(new Telligent.Evolution.Components.GroupQuery());
            //Telligent.Evolution.Components.GroupService..Groups group = new Telligent.Evolution.VelocityExtensions.Groups(
        }

        public bool IsAuthorizedIpAddress(string ipAddress)
        {
            var entities = new OmnicellEntities();
            if (entities.Omnicell_RestrictedAddress.Any(x => !x.IsDomain && x.Address.StartsWith(ipAddress)))
            {
                return false;
            }

            var context = System.Web.HttpContext.Current;
            ipAddress = context.Request.UserHostAddress;
            if (entities.Omnicell_RestrictedAddress.Any(x => !x.IsDomain && x.Address.StartsWith(ipAddress)))
            {
                return false;
            }

            ipAddress = context.Request.UrlReferrer.Host;
            if (entities.Omnicell_RestrictedAddress.Any(x => !x.IsDomain && x.Address.StartsWith(ipAddress)))
            {
                return false;
            }

            return true;
        }

        public bool IsBannedDomainName(string domainName)
        {
            if (domainName.Contains('@'))
            {
                domainName = domainName.Substring(domainName.IndexOf('@'));
            }

            var entities = new OmnicellEntities();
            if (entities.Omnicell_RestrictedAddress.Any(x => x.IsDomain && domainName.ToLower().EndsWith(x.Address.ToLower())))
            {
                return true;
            }

            return false;
        }

        public bool IsActiveEmployee(string email)
        {
            var entities = new OmnicellEntities();

            if (entities.Omnicell_Employee.Any(x => x.IsActive && email.Equals(x.Email)))
            {
                return true;
            }

            return false;
        }

        public bool IsActiveEmployeeByNumber(int employeeNumber)
        {
            var entities = new OmnicellEntities();
            if (entities.Omnicell_Employee.Any(x => x.IsActive && x.EmployeeNumber == employeeNumber))
            {
                return true;
            }

            return false;
        }


        public bool IsInActiveEmployee(string email)
        {
            var entities = new OmnicellEntities();
            if (entities.Omnicell_Employee.Any(x => !x.IsActive && email.Equals(x.Email)))
            {
                return true;
            }

            return false;
        }

        public bool IsInActiveEmployeeByNumber(int employeeNumber)
        {
            var entities = new OmnicellEntities();
            if (entities.Omnicell_Employee.Any(x => !x.IsActive && x.EmployeeNumber == employeeNumber))
            {
                return true;
            }

            return false;
        }

        public bool IsActiveCSN(string csn)
        {
            var entities = new OmnicellEntities();
            if (entities.Omnicell_Customer.Any(x => x.IsActive && csn.Equals(x.CSN)))
            {
                return true;
            }

            return false;
        }

        public bool IsInActiveCSN(string csn)
        {
            var entities = new OmnicellEntities();
            if (entities.Omnicell_Customer.Any(x => !x.IsActive && csn.Equals(x.CSN)))
            {
                return true;
            }

            return false;
        }

        public bool HasEmployeeOverride(string email)
        {
            var entities = new OmnicellEntities();
            return entities.Omnicell_EmployeeOverride.Any(x => x.Email.Equals(email));
        }

        public bool HasCustomerOverride(string email)
        {
            var entities = new OmnicellEntities();
            return entities.Omnicell_CustomerOverride.Any(x => x.Email.Equals(email));
        }

        public AdditionalInfo AddCustomerOverride(string email)
        {
            var additionalInfo = new AdditionalInfo();
            try
            {
                using (var entities = new OmnicellEntities())
                {
                    var customerOverride = new Omnicell_CustomerOverride
                    {
                        Email = email,
                        Created = DateTime.Now
                    };

                    entities.Omnicell_CustomerOverride.AddObject(customerOverride);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var error = new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString());
                additionalInfo.Errors.Add(error);
            }

            return additionalInfo;
        }

        public OmnicellCSNGroup SaveOmnicellCSNGroup(OmnicellCSNGroup omnicellCsnGroup)
        {
            return SaveOmnicellCSNGroup(omnicellCsnGroup.Id, omnicellCsnGroup.CSNs);
        }

        public OmnicellCSNGroup SaveOmnicellCSNGroup(int id, string[] csns)
        {
            var omnicellCSNGroup = new OmnicellCSNGroup();
            Omnicell_CSNGroup csnGroup = null;
            try
            {
                using (var entities = new OmnicellEntities())
                {
                    csnGroup = entities.Omnicell_CSNGroup.Single(x => x.Id == id);
                    var csnList = csns.ToList();

                    var csnsToDelete = csnGroup.Omnicell_CSN.Where(x => !csnList.Contains(x.CSN));
                    foreach (var csn in csnsToDelete)
                    {
                        csnGroup.Omnicell_CSN.Remove(csn);
                        entities.Omnicell_CSN.DeleteObject(csn);
                    }

                    foreach (var csnString in csnList)
                    {
                        var csn = entities.Omnicell_CSN.SingleOrDefault(x => csnString.Equals(x.CSN));
                        if (csn != null)
                        {
                            if (csn.Omnicell_CSNGroup != csnGroup)
                            {
                                MergeCsnGroups(entities, csnGroup, csn.Omnicell_CSNGroup);
                            }
                        }
                        else
                        {
                            csn = new Omnicell_CSN
                            {
                                Omnicell_CSNGroup = csnGroup,
                                CSN = csnString
                            };
                        }
                    }

                    entities.SaveChanges();
                }

                omnicellCSNGroup = new OmnicellCSNGroup(csnGroup);
            }
            catch (Exception ex)
            {
                LogThis(ex.ToString());
            }

            return omnicellCSNGroup;
        }

        public OmnicellCSNGroup AddOmnicellCSNGroup(string[] csns)
        {
            var omnicellCSNGroup = new OmnicellCSNGroup();
            Omnicell_CSNGroup csnGroup = null;
            try
            {
                using (var entities = new OmnicellEntities())
                {
                    if (entities.Omnicell_CSN.Any(x => csns.Contains(x.CSN)))
                    {
                        csnGroup = entities.Omnicell_CSN
                            .First(x => csns.Contains(x.CSN)).Omnicell_CSNGroup;
                        var csnList = csns.ToList();
                        foreach (var csnString in csnList)
                        {
                            var csn = entities.Omnicell_CSN.SingleOrDefault(x => csnString.Equals(x.CSN));
                            if (csn != null)
                            {
                                if (csn.Omnicell_CSNGroup != csnGroup)
                                {
                                    MergeCsnGroups(entities, csnGroup, csn.Omnicell_CSNGroup);
                                }
                            }
                            else
                            {
                                csn = new Omnicell_CSN
                                {
                                    Omnicell_CSNGroup = csnGroup,
                                    CSN = csnString
                                };
                            }
                        }
                    }
                    else
                    {
                        csnGroup = new Omnicell_CSNGroup();
                        var csnList = csns.ToList();
                        foreach (var csnString in csnList)
                        {
                            var csn = new Omnicell_CSN
                            {
                                Omnicell_CSNGroup = csnGroup,
                                CSN = csnString
                            };
                        }

                        entities.Omnicell_CSNGroup.AddObject(csnGroup);
                    }

                    entities.SaveChanges();
                }

                omnicellCSNGroup = new OmnicellCSNGroup(csnGroup);
            }
            catch (Exception ex)
            {
                LogThis(ex.ToString());
            }

            return omnicellCSNGroup;
        }

        private void MergeCsnGroups(OmnicellEntities db, Omnicell_CSNGroup dest, Omnicell_CSNGroup source)
        {
            if (source.EntityState != System.Data.EntityState.Deleted)
            {
                foreach (var csn in source.Omnicell_CSN)
                {
                    csn.Omnicell_CSNGroup = dest;
                }

                db.Omnicell_CSNGroup.DeleteObject(source);
                db.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
        }

        public AdditionalInfo AddEmployeeOverride(string email)
        {
            var additionalInfo = new AdditionalInfo();
            try
            {
                using (var entities = new OmnicellEntities())
                {
                    var employeeOverride = new Omnicell_EmployeeOverride
                    {
                        Email = email,
                        Created = DateTime.Now
                    };

                    entities.Omnicell_EmployeeOverride.AddObject(employeeOverride);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var error = new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString());
                additionalInfo.Errors.Add(error);
            }

            return additionalInfo;
        }

        public AdditionalInfo DeleteCustomerOverrides(IList<string> customerOverrides)
        {
            var additionalInfo = new AdditionalInfo();
            try
            {
                using (var entities = new OmnicellEntities())
                {
                    foreach (var customerOverride in customerOverrides)
                    {
                        int result;
                        if (int.TryParse(customerOverride, out result))
                        {
                            var entity = entities.Omnicell_CustomerOverride.Single(x => x.Id == result);
                            entities.Omnicell_CustomerOverride.DeleteObject(entity);
                        }
                    }

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var error = new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString());
                additionalInfo.Errors.Add(error);
            }

            return additionalInfo;
        }

        public AdditionalInfo DeleteCsnGroups(IList<string> csnGroups)
        {
            var additionalInfo = new AdditionalInfo();
            try
            {
                using (var entities = new OmnicellEntities())
                {
                    foreach (var csnGroupIdString in csnGroups)
                    {
                        int result;
                        if (int.TryParse(csnGroupIdString, out result))
                        {
                            var entity = entities.Omnicell_CSNGroup.Single(x => x.Id == result);
                            entities.Omnicell_CSNGroup.DeleteObject(entity);
                        }
                    }

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var error = new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString());
                additionalInfo.Errors.Add(error);
                LogThis(ex.ToString());
            }

            return additionalInfo;
        }

        public AdditionalInfo DeleteEmployeeOverrides(IList<string> employeeOverrides)
        {
            var additionalInfo = new AdditionalInfo();
            try
            {
                using (var entities = new OmnicellEntities())
                {
                    foreach (var employeeOverride in employeeOverrides)
                    {
                        int result;
                        if (int.TryParse(employeeOverride, out result))
                        {
                            var entity = entities.Omnicell_EmployeeOverride.Single(x => x.Id == result);
                            entities.Omnicell_EmployeeOverride.DeleteObject(entity);
                        }
                    }

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var error = new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString());
                additionalInfo.Errors.Add(error);
            }

            return additionalInfo;
        }

        public PagedList<OverrideViewModel> GetOverrides()
        {
            var list = new PagedList<OverrideViewModel>();
            var entities = new OmnicellEntities();
            foreach (var email in entities.Omnicell_CustomerOverride)
            {
                var viewModel = new OverrideViewModel
                {
                    Id = email.Id,
                    Email = email.Email,
                    Type = "Customer",
                    Created = email.Created.Value
                };

                list.Add(viewModel);
            }

            foreach (var email in entities.Omnicell_EmployeeOverride)
            {
                var viewModel = new OverrideViewModel
                {
                    Id = email.Id,
                    Email = email.Email,
                    Type = "Employee",
                    Created = email.Created.Value
                };

                list.Add(viewModel);
            }

            return list;
        }

        public List<OmnicellCSNGroup> ListCSNGroups()
        {
            var list = new List<OmnicellCSNGroup>();
            var entities = new OmnicellEntities();
            foreach (var csnGroup in entities.Omnicell_CSNGroup.ToList())
            {
                var viewModel = new OmnicellCSNGroup(csnGroup);
                list.Add(viewModel);
            }

            return list;
        }

        public PagedList<BannedAddressViewModel> GetBannedAddresses()
        {
            var list = new PagedList<BannedAddressViewModel>();
            var entities = new OmnicellEntities();
            foreach (var address in entities.Omnicell_RestrictedAddress)
            {
                var viewModel = new BannedAddressViewModel
                {
                    Id = address.Id,
                    Address = address.Address,
                    CreateDate = address.Created,
                    Type = address.IsDomain ? "Domain Name" : "IP Address"
                };

                list.Add(viewModel);
            }
            return list;
        }

        public List<Telligent.Evolution.Extensibility.Api.Entities.Version1.User> GetUsersInRole(string role)
        {
            var usernameList = Roles.GetUsersInRole(role);
            var options = new Telligent.Evolution.Extensibility.Api.Version1.UsersListOptions();
            options.Usernames = string.Join(",", usernameList);
            options.PageSize = int.MaxValue;
            var userList = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.User>(Telligent.Evolution.Extensibility.Api.Version1.PublicApi.Users.List(options));
            return userList;
        }

        public AdditionalInfo AddFailedAccountAttempt(string CSN, string Email, string FailReason, string FirstName, string LastName, string Zip, string IpAddress)
        {
            if (string.IsNullOrWhiteSpace(IpAddress))
            {
                var context = System.Web.HttpContext.Current;
                IpAddress = context.Request.UserHostAddress;
                if (string.IsNullOrWhiteSpace(IpAddress))
                {
                    IpAddress = context.Request.UrlReferrer.Host;
                }
            }

            var additionalInfo = new AdditionalInfo();
            try
            {
                var attempt = new Omnicell_FailedAccountAttempts
                {
                    CSN = CSN,
                    Email = Email,
                    FailReason = FailReason,
                    FirstName = FirstName,
                    LastName = LastName,
                    Zip = Zip,
                    Date = DateTime.Now,
                    IpAddress = IpAddress
                };

                using (var entities = new OmnicellEntities())
                {
                    entities.Omnicell_FailedAccountAttempts.AddObject(attempt);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var error = new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString());
                additionalInfo.Errors.Add(error);
                throw ex;
            }

            return additionalInfo;
        }

        public string SalesForceTest()
        {
            var userName = Convert.ToString(ConfigurationManager.AppSettings["SalesForceLoginName"]);
            var password = Convert.ToString(ConfigurationManager.AppSettings["SalesForcePassword"]);
            var token = Convert.ToString(ConfigurationManager.AppSettings["SalesForceSecurityToken"]);

            var service = new SforceService.SforceService();
            service.Timeout = 6000;
            var loginResult = service.login(userName, password + token);
            service.Url = loginResult.serverUrl;
            service.SessionHeaderValue = new SforceService.SessionHeader { sessionId = loginResult.sessionId };

            // describeGlobal() returns an array of object results that  
            // includes the object names that are available to the logged-in user. 
            var dgr = service.describeGlobal();

            var sb = new StringBuilder();
            sb.AppendLine("\nDescribe Global Results:\n");
            // Loop through the array echoing the object names to the console             
            for (int i = 0; i < dgr.sobjects.Length; i++)
            {
                sb.AppendLine(dgr.sobjects[i].name);
            }

            return sb.ToString();
        }

        //public string SalesForceTestWCF()
        //{
        //    var userName = Convert.ToString(ConfigurationManager.AppSettings["SalesForceLoginName"]);
        //    var password = Convert.ToString(ConfigurationManager.AppSettings["SalesForcePassword"]);
        //    var token = Convert.ToString(ConfigurationManager.AppSettings["SalesForceSecurityToken"]);

        //    var serviceClient = new SoapClient("Soap");
        //    var loginResult = serviceClient.login(null, userName, password + token);
        //    serviceClient.Endpoint.Address = new System.ServiceModel.EndpointAddress(loginResult.serverUrl);

        //    var session = new SessionHeader();
        //    session.sessionId = loginResult.sessionId;

        //    //service = 6000;
        //    //var loginResult = service.login(userName, password);
        //    //service.Url = loginResult.serverUrl;

        //    // describeGlobal() returns an array of object results that  
        //    // includes the object names that are available to the logged-in user. 
        //    var dgr = serviceClient.describeGlobal(session, null);

        //    var sb = new StringBuilder();
        //    sb.AppendLine("\nDescribe Global Results:\n");
        //    // Loop through the array echoing the object names to the console             
        //    for (int i = 0; i < dgr.sobjects.Length; i++)
        //    {
        //        sb.AppendLine(dgr.sobjects[i].name);
        //    }

        //    return sb.ToString();
        //}


        private SforceService.SforceService GetService()
        {
            var userName = Convert.ToString(ConfigurationManager.AppSettings["SalesForceLoginName"]);
            var password = Convert.ToString(ConfigurationManager.AppSettings["SalesForcePassword"]);
            var token = Convert.ToString(ConfigurationManager.AppSettings["SalesForceSecurityToken"]);

            LoginResult loginResult = null;
            var serviceClient = new SforceService.SforceService();
            serviceClient.Timeout = 6000;
            try
            {
                loginResult = serviceClient.login(userName, password + token);
                serviceClient.Url = loginResult.serverUrl;
                serviceClient.SessionHeaderValue = new SforceService.SessionHeader { sessionId = loginResult.sessionId };

            }
            catch (System.Web.Services.Protocols.SoapException e)
            {
                // This is likley to be caused by bad username or password
                serviceClient = null;
                throw (e);
            }
            catch (Exception e)
            {
                // This is something else, probably communication
                serviceClient = null;
                throw (e);
            }

            return serviceClient;
        }

        public List<Account> GetAccountsWithCSNs()
        {
            var serviceClient = GetService();
            var list = new List<Account>();

            try
            {

                var queryString = "select * from Account where CSN is not null and csn <> ''";
                var queryResults = serviceClient.query(queryString);
                var records = queryResults.records;

                foreach (var record in records)
                {
                    var account = (Account)record;
                    list.Add(account);
                }

            }
            catch (System.Web.Services.Protocols.SoapException e)
            {
                // This is likley to be caused by bad username or password
                serviceClient = null;
                throw (e);
            }
            catch (Exception e)
            {
                // This is something else, probably communication
                serviceClient = null;
                throw (e);
            }

            return list;
        }

        public List<Account> GetAccounts()
        {
            var serviceClient = GetService();
            var list = new List<Account>();

            try
            {
                var date = DateTime.Now.AddYears(-1).AddMonths(-1).ToString("s");
                //SELECT Name, AccountNumber, CSN__c, Account_Status__c FROM Account Where Account_Status__c = 'Active' and CreatedDate != null and CSN__c != null and CSN__c != 0 and CreatedDate > 2011-08-12T14:52:00-08:00
                var queryString = "SELECT Name, AccountNumber, CSN__c FROM Account Where Account_Status__c = 'Active' and CreatedDate != null and CSN__c != null and CSN__c != 0 and CreatedDate > " + date + "-08:00";
                var queryResults = serviceClient.query(queryString);
                var records = queryResults.records;

                foreach (var record in records)
                {
                    var account = (Account)record;
                    list.Add(account);
                }

            }
            catch (System.Web.Services.Protocols.SoapException e)
            {
                // This is likley to be caused by bad username or password
                serviceClient = null;
                throw (e);
            }
            catch (Exception e)
            {
                // This is something else, probably communication
                serviceClient = null;
                throw (e);
            }

            return list;
        }

        public bool IsImportedFromSalesForce(string csn)
        {
            var serviceClient = GetService();
            var result = 0;
            if (!int.TryParse(csn, out result) || result == 0)
            {
                return false;
            }

            try
            {

                var date = DateTime.Now.AddYears(-1).AddMonths(-1);
                //var date = DateTime.Now.AddYears(-1).AddMonths(-1).ToString("s");
                //SELECT Name, AccountNumber, CSN__c, Account_Status__c FROM Account Where Account_Status__c = 'Active' and CreatedDate != null and CSN__c != null and CSN__c != 0 and CreatedDate > 2011-08-12T14:52:00-08:00
                //var queryString = "SELECT CSN__c FROM Account Where CSN__c = " + csn + " and Account_Status__c = 'Active' and CreatedDate != null and CreatedDate > " + date + "-08:00";
                var queryString = "SELECT CSN__c, Account_Status__c, CreatedDate, GPO__c, ShippingPostalCode, ShippingCity, Business_Type__c FROM Account Where CSN__c = " + csn;
                var queryResults = serviceClient.query(queryString);
                var records = queryResults.records;

                //var queryString = "select CSN__c from Account where CSN__c = " + csn + "";
                //var queryResults = serviceClient.query(queryString);

                // dummy approach for now....
                if (queryResults.size > 0)
                {
                    for (int i = 0; i < records.Length; i++)
                    {
                        var account = (Account)records[i];
                        if ((string.IsNullOrWhiteSpace(account.Account_Status__c) || "Active".Equals(account.Account_Status__c)) && account.CreatedDate.HasValue && account.CreatedDate.Value > date)
                        {
                            var customer = new Omnicell_Customer
                            {
                                CSN = csn,
                                Name = "SalesForceImportRecord",
                                Updated = account.CreatedDate.Value.AddMonths(6).AddYears(1),
                                GPOName = account.GPO__c,
                                PostalCode = account.ShippingPostalCode,
                                City = account.ShippingCity,
                                BusinessPartnerType = account.Business_Type__c,
                                State = account.ShippingState,
                                AccountName = account.Name,
                                IsActive = true
                            };

                            using (var db = new OmnicellEntities())
                            {
                                db.Omnicell_Customer.AddObject(customer);
                                db.SaveChanges();
                            }

                            return true;
                        }
                    }
                }

                //queryString = "select email from Lead where email = '" + email + "'";
                //queryResults = serviceClient.query(queryString);

                //// dummy approach for now....
                //if (queryResults.size > 0)
                //{
                //    return true;
                //}

                //queryString = "select email from Contact where email = '" + email + "'";
                //queryResults = serviceClient.query(queryString);

                //if (queryResults.size > 0)
                //{
                //    return true;
                //}
            }
            catch (System.Web.Services.Protocols.SoapException e)
            {
                // This is likley to be caused by bad username or password
                serviceClient = null;
                throw (e);
            }
            catch (Exception e)
            {
                // This is something else, probably communication
                serviceClient = null;
                throw (e);
            }

            // no matches found...
            return false;
        }

        public PagedList<FailedAccountAttemptsViewModel> GetFailedAccountAttempts()
        {
            var list = new PagedList<FailedAccountAttemptsViewModel>();
            var entities = new OmnicellEntities();
            foreach (var item in entities.Omnicell_FailedAccountAttempts)
            {
                var attempt = new FailedAccountAttemptsViewModel
                {
                    Id = item.Id,
                    CSN = item.CSN,
                    Email = item.Email,
                    FailReason = item.FailReason,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    Zip = item.Zip,
                    CreateDate = item.Date.Value,
                    IpAddress = item.IpAddress
                };

                list.Add(attempt);
            }
            return list;
        }

        public AdditionalInfo DeleteFailedAccountAttempt(int id)
        {
            var returnValue = new AdditionalInfo();
            try
            {
                using (var db = new OmnicellEntities())
                {
                    var dbEntity = db.Omnicell_FailedAccountAttempts.SingleOrDefault(x => x.Id == id);
                    if (dbEntity != null)
                    {
                        db.DeleteObject(dbEntity);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting a Failed Account Attempt (id=" + id + "):" + ex.ToString());
                returnValue.Errors = new List<Telligent.Evolution.Extensibility.Api.Entities.Version1.Error>() { new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString()) };
            }

            return returnValue;
        }


        public AdditionalInfo DeleteBannedAddress(string address)
        {
            var additionalInfo = new AdditionalInfo();
            try
            {
                using (var entities = new OmnicellEntities())
                {
                    int result;
                    if (int.TryParse(address, out result))
                    {
                        var entity = entities.Omnicell_RestrictedAddress.Single(x => x.Id == result);
                        entities.Omnicell_RestrictedAddress.DeleteObject(entity);
                        entities.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                var error = new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString());
                additionalInfo.Errors.Add(error);
            }

            return additionalInfo;
        }

        public AdditionalInfo DeleteBannedAddresses(IList<string> selectedAddresses)
        {
            var additionalInfo = new AdditionalInfo();
            try
            {
                using (var entities = new OmnicellEntities())
                {
                    foreach (var address in selectedAddresses)
                    {
                        int result;
                        if (int.TryParse(address, out result))
                        {
                            var entity = entities.Omnicell_RestrictedAddress.Single(x => x.Id == result);
                            entities.Omnicell_RestrictedAddress.DeleteObject(entity);
                        }
                    }

                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var error = new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString());
                additionalInfo.Errors.Add(error);
            }

            return additionalInfo;
        }

        public AdditionalInfo AddBannedAddress(bool isDomain, string address)
        {
            var additionalInfo = new AdditionalInfo();
            try
            {
                using (var entities = new OmnicellEntities())
                {
                    var bannedAddress = new Omnicell_RestrictedAddress
                    {
                        IsDomain = isDomain,
                        Created = DateTime.Now,
                        Address = address
                    };

                    entities.Omnicell_RestrictedAddress.AddObject(bannedAddress);
                    entities.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                var error = new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString());
                additionalInfo.Errors.Add(error);
            }

            return additionalInfo;
        }

        // this is the functionality for the new add user's to training subscription by csn.
        public PagedList<CSNSubscriptionViewModel> GetCSNSubscriptions(IDictionary dictionary)
        {
            int pageSize = 0;
            int pageIndex = 0;

            foreach (string item in dictionary.Keys)
            {
                if (dictionary[item] != null && !string.IsNullOrWhiteSpace(dictionary[item].ToString()))
                {
                    switch (item)
                    {
                        case "PageSize":
                            {
                                pageSize = Convert.ToInt32(dictionary[item]);
                                break;
                            }
                        case "PageIndex":
                            {
                                pageIndex = Convert.ToInt32(dictionary[item]);
                                break;
                            }
                        default:
                            break;
                    }
                }
            }
            List<CSNSubscriptionViewModel> tempList = new List<CSNSubscriptionViewModel>();

            var list = new PagedList<CSNSubscriptionViewModel>();
            var entities = new OmnicellEntities();

            try
            {
                foreach (var item in entities.Omnicell_CSNSubscription)
                {
                    var attempt = new CSNSubscriptionViewModel
                    {
                        Id = item.Id,
                        CSN = item.CSN,
                        Facility = item.Facility
                    };

                   tempList.Add(attempt);
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred retrieving CSNs from subscription table" + ex.ToString());
            }
            int totalCount = tempList.Count;
            tempList = tempList.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            return new PagedList<CSNSubscriptionViewModel>(tempList, pageSize, pageIndex, totalCount);
        }

        public PagedList<CSNSubscriptionViewModel> GetCSNSubscriptions(string csn, IDictionary dictionary)
        {
            int pageSize = 0;
            int pageIndex = 0;
            List<CSNSubscriptionViewModel> tempList = new List<CSNSubscriptionViewModel>();

            foreach (string item in dictionary.Keys)
            {
                if (dictionary[item] != null && !string.IsNullOrWhiteSpace(dictionary[item].ToString()))
                {
                    switch (item)
                    {
                        case "PageSize":
                            {
                                pageSize = Convert.ToInt32(dictionary[item]);
                                break;
                            }
                        case "PageIndex":
                            {
                                pageIndex = Convert.ToInt32(dictionary[item]);
                                break;
                            }
                        default:
                            break;
                    }
                }
            }

            var list = new PagedList<CSNSubscriptionViewModel>();
            var entities = new OmnicellEntities();

            try
            {
                if (csn.Length > 0)
                {
                    string[] csns = csn.Split(',');

                    foreach (string number in csns)
                    {
                        var dbEntity = entities.Omnicell_CSNSubscription.SingleOrDefault(x => x.CSN == number);

                        if (dbEntity != null)
                        {
                            var attempt = new CSNSubscriptionViewModel
                            {
                                Id = dbEntity.Id,
                                CSN = dbEntity.CSN,
                                Facility = dbEntity.Facility
                            };

                            tempList.Add(attempt);
                        }

                    }

                    
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred retrieving CSNs from subscription table" + ex.ToString());
            }


            int totalCount = tempList.Count;
            tempList = tempList.Skip((pageIndex) * pageSize).Take(pageSize).ToList();
            return new PagedList<CSNSubscriptionViewModel>(tempList, pageSize, pageIndex, totalCount);
        }

        public bool AddCSNSubscription(string csn)
        {
            bool added = false;
            var additionalInfo = new AdditionalInfo();

            var entities = new OmnicellEntities();
            var customer = entities.Omnicell_Customer.FirstOrDefault(x => x.CSN == csn);
          
            try
            {
                if (customer != null)
                {

                    if (!entities.Omnicell_CSNSubscription.Any(x => x.CSN == csn))
                    {

                        var attempt = new Omnicell_CSNSubscription
                        {
                            CSN = csn,
                            Facility = customer.AccountName
                        };

                        using (entities)
                        {
                            entities.Omnicell_CSNSubscription.AddObject(attempt);
                            entities.SaveChanges();
                            added = true;
                           
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                var error = new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString());
                additionalInfo.Errors.Add(error);
               
            }

            return added;
        }

        public bool DeleteCSNSubscription(string csn)
        {
            bool deleted = false;
            var additionalInfo = new AdditionalInfo();

            try
            {
                if (csn.Length > 0)
                {
                    string[] csns = csn.Split(',');

                    using (var db = new OmnicellEntities())
                    {
                        foreach (string number in csns)
                        {
                            var dbEntity = db.Omnicell_CSNSubscription.SingleOrDefault(x => x.CSN == number);
                            if (dbEntity != null)
                            {
                                db.DeleteObject(dbEntity);
                                db.SaveChanges();
                                deleted = true;
                            }
                        }
                        
                    }
                }
            }
            catch (Exception ex)
            {
                LogThis("Exception occurred Deleting CSN from subscription table(csn=" + csn + "):" + ex.ToString());
                var error = new Telligent.Evolution.Extensibility.Api.Entities.Version1.Error(ex.ToString());
                additionalInfo.Errors.Add(error);
               
            }

            return deleted;
        }

        public bool CSNSubscribe(string csn)
        {
            bool subscribe = false;

            try
            {
                if (csn.Length > 0)
                {
                    string[] csns = csn.Split(',');

                    var entities = new OmnicellEntities();

                    foreach (string number in csns)
                    {
                        if (entities.Omnicell_CSNSubscription.Any(x => x.CSN == number.Trim()))
                        {
                            subscribe = true;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                LogThis("Exception occurred Deleting CSN from subscription table(csn=" + csn + "):" + ex.ToString());
            }
            
            return subscribe;

        }

        public void LogString(string stringToLog)
        {
            Utilities.LogToTelligent(stringToLog);
            //var isStringNull = string.IsNullOrWhiteSpace(stringToLog);
            //Debug.Print(stringToLog);
            //var statusMessage = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.StatusMessages.Create(stringToLog);
        }

        public string SendEmail(string fromEmail, string fromName, string toEmail, string toName, string subject, string body, string smtpServer)
        {
            Utilities u = new Utilities();
            return u.SendEmail(fromEmail, fromName, toEmail, toName, subject, body.Replace("<br />", "\n"), smtpServer);
        }

        public string SendEmail(string fromEmail, string fromName, string toEmail, string toName, string subject, string body, string smtpServer, int port, bool enableSsl, string loginName, string password)
        {
            Utilities u = new Utilities();
            return u.SendEmail(fromEmail, fromName, toEmail, toName, subject, body.Replace("<br />", "\n"), smtpServer, port, enableSsl, loginName, password);

        }

        // code for reading and writing encrypted query string
        public string Decrypt(string query)
        {
            byte[] cipherBytes = Convert.FromBase64String(query);
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(_Pwd, _Salt);
            byte[] decrpytedData = Decrypt(cipherBytes, pdb.GetBytes(32), pdb.GetBytes(16));
            return System.Text.Encoding.Unicode.GetString(decrpytedData);
        }

        private byte[] Decrypt(byte[] cipherData, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = null;

            try
            {
                Rijndael alg = Rijndael.Create();
                alg.Key = Key;
                alg.IV = IV;
                cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write);
                cs.Write(cipherData, 0, cipherData.Length);
                cs.FlushFinalBlock();
                return ms.ToArray();
            }
            catch
            {
                return null;
            }
            finally
            {
                cs.Close();
            }
        }

        public string Encrypt(string clearText)
        {
            byte[] clearBytes = System.Text.Encoding.Unicode.GetBytes(clearText);
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(_Pwd, _Salt);
            byte[] encryptedData = Encrypt(clearBytes, pdb.GetBytes(32), pdb.GetBytes(16));
            return Convert.ToBase64String(encryptedData);

        }

        private static byte[] Encrypt(byte[] clearData, byte[] Key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = null;

            try
            {
                Rijndael alg = Rijndael.Create();
                alg.Key = Key;
                alg.IV = IV;
                cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);
                cs.Write(clearData, 0, clearData.Length);
                cs.FlushFinalBlock();
                return ms.ToArray();
            }
            catch
            {
                return null;
            }
            finally
            {
                cs.Close();
            }
        }

        static string _Pwd = "Happy0$Day";
        static byte[] _Salt = new byte[] { 0x88, 0xF7, 0x77, 0x7d, 0x23, 0x6b, 0x47, 0x6c, 0x12, 0x4f, 0x25, 0x87, 0x57 };

        // these are email digest test methods they are meant to be removed once done testing the functionality
        public bool testEmailDigest()
        {
            bool worked = true;

            DateTime today = DateTime.Now;
            DayOfWeek weekday = DateTime.Today.DayOfWeek;
            DateTime firstDay = new DateTime(today.Year, today.Month, 1);
            DateTime lastMonthFirst = firstDay.AddMonths(-1);

            DailyEmails(today);

            if (weekday == DayOfWeek.Friday)
            {
                WeeklyEmails(today.AddDays(-7));
            }

            if (firstDay.Date == today.Date)
            {
                MonthlyEmails(lastMonthFirst);
            }


            return worked;

        }

        private static void DailyEmails(DateTime today)
        {
            Utilities util = new Utilities();

            try
            {

                using (var db = new OmnicellEntities())
                {
                    var users = db.Omnicell_TicketEmailInfo.Where(x => x.IsImmediateUpdate == 0);
                    var tickets = new List<Omnicell_Ticket>();

                    foreach (var item in users)
                    {
                        var subscriptions = db.Omnicell_TicketSubscription.Where(x => x.UserId == item.UserId);

                        foreach (var sub in subscriptions)
                        {
                            if (sub.TicketId != null)
                            {
                                var ticket = db.Omnicell_Ticket.FirstOrDefault(x => x.Id == sub.TicketId);

                                if (ticket.Updated.Value.Date == today.Date)
                                {
                                    tickets.Add(ticket);
                                }
                            }

                        }

                        if (tickets.Count != 0)
                        {
                            util.BuildEmail(tickets, item.Email, item.UserId);
                        }

                        tickets.Clear();

                    }
                }
            }
            catch (Exception ex)
            {
               // new CSException(CSExceptionType.UnknownError, string.Format("Error updating wiki page with Id: {0}, and contentId: {1}", ,), ex).Log();
            }
        }

        private static void WeeklyEmails(DateTime today)
        {
            Utilities util = new Utilities();

            try
            {

                using (var db = new OmnicellEntities())
                {
                    var users = db.Omnicell_TicketEmailInfo.Where(x => x.IsImmediateUpdate == 2);
                    var tickets = new List<Omnicell_Ticket>();

                    foreach (var item in users)
                    {
                        var subscriptions = db.Omnicell_TicketSubscription.Where(x => x.UserId == item.UserId);

                        foreach (var sub in subscriptions)
                        {
                            var ticket = db.Omnicell_Ticket.FirstOrDefault(x => x.Id == sub.TicketId);

                            if (ticket.Updated > today)
                            {
                                tickets.Add(ticket);
                            }
                        }

                        if (tickets.Count != 0)
                        {
                            util.BuildEmail(tickets, item.Email, item.UserId);
                        }

                        tickets.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.LogToTelligent("Exception occurred while sending weekly subscription emails:" + ex.ToString());
            }
        }

        private static void MonthlyEmails(DateTime first)
        {
            Utilities util = new Utilities();

            try
            {


                using (var db = new OmnicellEntities())
                {
                    var users = db.Omnicell_TicketEmailInfo.Where(x => x.IsImmediateUpdate == 3);
                    var tickets = new List<Omnicell_Ticket>();

                    foreach (var item in users)
                    {
                        var subscriptions = db.Omnicell_TicketSubscription.Where(x => x.UserId == item.UserId);

                        foreach (var sub in subscriptions)
                        {
                            var ticket = db.Omnicell_Ticket.FirstOrDefault(x => x.Id == sub.TicketId);

                            if (ticket.Updated > first)
                            {
                                tickets.Add(ticket);
                            }
                        }

                        if (tickets.Count != 0)
                        {
                            util.BuildEmail(tickets, item.Email, item.UserId);
                        }

                        tickets.Clear();
                    }
                }
            }
            catch (Exception ex)
            {
                Utilities.LogToTelligent("Exception occurred while sending monthly subscription emails:" + ex.ToString());
            }
        }
    }

    public class OmnicellAuthenticationExtension : IScriptedContentFragmentExtension
    {
        public string ExtensionName
        {
            get
            {
                return "omnicell_authentication";
            }
        }

        //return an instace of the object you want exposed.     
        public object Extension { get { return new OmnicellAuthentication(); } }

        public string Name
        {
            get
            {
                return "OmnicellAuthentication Extension";
            }
        }

        public string Description
        {
            get
            {
                return "Enables scripted content fragments to use OmnicellAuthentication.";
            }
        }

        public void Initialize()
        {
        }
    }
}
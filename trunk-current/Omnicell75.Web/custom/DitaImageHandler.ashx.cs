﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Xml.Serialization;
using Omnicell.Custom.Data;
using Omnicell.Data.Model;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1.PublicApi;
using TEntities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TExV1 = Telligent.Evolution.Extensibility.Api.Version1;
using Telligent.Evolution.Extensibility.Storage.Version1;

namespace Omnicell75.Web.custom
{
    /// <summary>
    /// Summary description for DitaImageHandler
    /// </summary>
    public class DitaImageHandler : IHttpHandler
    {
        private static object _runAsUserLock = new object();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string url = string.Empty;
            try
            {
                
                 string apiKey = context.Request.QueryString["apikey"];
                 

                 if (apiKey != "gmjak34qvtveqvyg0ua")
                 {
                     context.Response.Write("invalid api key");
                 }
                 else
                 {
                     if (context.Request.QueryString["url"] != null)
                     {
                        User admUser = Telligent.Evolution.Users.GetUser("admin");
                        url = context.Request.QueryString["url"];
                        
                         ICentralizedFile cfs = null;
                         RunAsUser(() => cfs = CentralizedFileStorage.GetCentralizedFileByUrl(url), new ContextService().GetExecutionContext(), admUser);

                        if (cfs != null)
                        {
                            string contentType = String.Empty;
                            Image img = null;
                            RunAsUser(() => img = Image.FromStream(cfs.OpenReadStream()), new ContextService().GetExecutionContext(), admUser);
                            Image img2 = new Bitmap(img.Width, img.Height);
                            Graphics g = Graphics.FromImage(img2);
                            g.DrawImage(img, 0, 0, img.Width, img.Height);
                            g.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                            g.Dispose();
                            img.Dispose();
                            MemoryStream str = new MemoryStream();
                            img2.Save(str, System.Drawing.Imaging.ImageFormat.Png);
                            img2.Dispose();
                            str.WriteTo(context.Response.OutputStream);
                            str.Dispose();
                            str.Close();
                            context.Response.ContentType = ".png";
                            context.Response.End();

                        }
                     }
                     
                 }
            }
            catch(Exception ex)
            {
                new CSException(CSExceptionType.UnknownError, string.Format("Error creating image with url: {0}", url), ex).Log();
            }  
        }

        public void RunAsUser(Action a, IExecutionContext context, User u)
        {
            lock (_runAsUserLock)
            {
                var originalUser = context.User;
                try
                {
                    context.User = u;
                    a();
                }
                finally
                {
                    context.User = originalUser;
                }
            }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Omnicell.Services;

namespace Omnicell75.Web.custom
{
    /// <summary>
    /// Summary description for SPConnect
    /// </summary>
    public class SPConnect : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Write("Hello World");

            string strCreated = "";
            SharepointConnect spconn = new SharepointConnect();
            strCreated = spconn.GetSPDateCreated();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
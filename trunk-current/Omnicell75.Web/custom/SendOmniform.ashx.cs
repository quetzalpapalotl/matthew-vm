﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.SessionState;
using System.IO;
using Omnicell.Services;


namespace Omniform.custom
{
    /// <summary>
    /// Summary description for SendOmniform
    /// </summary>
    public class SendOmniform : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            if (context.Request.RequestType != "POST")
                return;

            try {

                string returnUrl = context.Request.Form["returnUrl"];

                string senderEmail = context.Request.Form["senderEmail"];
                string subjectLine = context.Request.Form["subjectLine"];
                string rcptEmail = context.Request.Form["rcptEmail"];
                string emailBodyHeader = context.Request.Form["emailBodyHeader"];
                string emailBodyFooter = context.Request.Form["emailBodyFooter"];

                string request_date = context.Request.Form["request_date"];
                string first_name = context.Request.Form["first_name"];
                string last_name = context.Request.Form["last_name"];
                string title = context.Request.Form["title"];
                string email = context.Request.Form["email"];
                string facility = context.Request.Form["facility"];
                string zip_code = context.Request.Form["zip_code"];
                string csn = context.Request.Form["csn"];
                string phone = context.Request.Form["phone"];
                string question_type = context.Request.Form["question_type"];
                string question = context.Request.Form["question"];
                string severity = context.Request.Form["severity"];
                string product = context.Request.Form["product"];
                string unit_serial = context.Request.Form["unit_serial"];
                string unit_name = context.Request.Form["unit_name"];
                string reported = context.Request.Form["reported"];
                string ticket_number = context.Request.Form["ticket_number"];
                string problem_description = context.Request.Form["problem_description"];


                StringBuilder body = new StringBuilder();

                body.AppendFormat("{0}\n\n", emailBodyHeader);

                body.AppendFormat("Request Date: {0}\n", request_date);
                body.AppendFormat("First Name: {0}\n", first_name);
                body.AppendFormat("Last Name: {0}\n", last_name);
                body.AppendFormat("Title: {0}\n", title);
                body.AppendFormat("Email: {0}\n", email);
                body.AppendFormat("Facility Name: {0}\n", facility);
                body.AppendFormat("Zip Code: {0}\n", zip_code);
                body.AppendFormat("CSN: {0}\n", csn);
                body.AppendFormat("Phone: {0}\n", phone);
                body.AppendFormat("Service Ticket Request Type: {0}\n", question_type);
                body.AppendFormat("Question: \n{0}\n\n", question);
                body.AppendFormat("Severity: {0}\n", severity);
                body.AppendFormat("Product: {0}\n", product);
                body.AppendFormat("Unit Serial: {0}\n", unit_serial);
                body.AppendFormat("Unit Name: {0}\n", unit_name);
                body.AppendFormat("Previously Reported?: {0}\n", reported);
                body.AppendFormat("Previous Ticket Number: {0}\n", ticket_number);
                body.AppendFormat("Description of Problem: \n{0}\n\n", problem_description);
            

                body.Append("\n\n");
                body.AppendFormat("{0}\n\n", emailBodyFooter);

                if (Utilities.IsValidEmail(senderEmail) && Utilities.IsValidEmail(rcptEmail))
                {
                    Utilities t = new Utilities();
                    t.SendEmail(senderEmail.ToString(), senderEmail.ToString(), rcptEmail.ToString(), rcptEmail.ToString(), subjectLine.ToString(), body.ToString(), context.Request.Files);
                }

                context.Response.Redirect(returnUrl);

                // context.Response.Write("success");

            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("1 - Message: {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);
                context.Response.Write(sb.ToString());
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
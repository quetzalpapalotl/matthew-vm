﻿using System.IO;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System;
using Telligent.Evolution.Components;
using Omnicell.Custom;
using Omnicell.Custom.Components;


namespace Omnicell.Web.custom
{
    /// <summary>
    /// Summary description for WikiPDF
    /// </summary>
    public class WikiPDF : IHttpHandler
    {
        private static object _runAsUserLock = new object();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            string body = context.Request["body"];
            string cssString = @".dita-page .title.topictitle1{
                            font-family: Myriad Pro Light Cond;
                            font-size: 26pt;
                            color: #7AC142;}";

            User admUser = Telligent.Evolution.Users.GetUser("admin");

            //RunAsUser(() => body = WikiPageExtensions.cleanHtmlPdf(body), new ContextService().GetExecutionContext(), admUser);

            //----variable declarations
            int startPoint = 0; //used to mark the start of a section of a string
            int pointer = 0; // used to move to the next character in the string
            string className = "";
            int classLength = 0; //temporarily stored the length of the class in the string section
            string styleName = "";
            int styleNameLength = 0;
            string styleValue = "";
            int styleValueLength = 0;
            StyleSheet myStyle = new StyleSheet();

            cssString = cssString.Replace("\r\n", ""); //have to remove the \r\n in the css string otherwise it will not work

            //--------Find topic title from the body string -------

            string toFind1 = "<h1 class=\"title topictitle1\">";
            string toFind2 = "</h1>";
            int start = body.IndexOf(toFind1) + toFind1.Length;
            int end = body.IndexOf(toFind2, start); //Start after the index of 'my' since 'is' appears twice
            string topicTitle = body.Substring(start, end - start);

            //------------Fix the body html to ad margins to lists since css margins is not supported



            //----------------------------------------------------------------------------------------

            //-----------------------------LOOP CLASS NAME --------------------------------------------------
            while (pointer < cssString.Length - 3) //loop until the end of the string
            {
                // --------------  Get the class in the string ---------------------------------------
                //read the string until it gets to a {
                while (cssString[pointer].ToString() != "{")
                {
                    pointer += 1;
                }
                classLength = pointer - startPoint;
                //  Create the class name. Some fixing has to be done to get the class name from the css string
                className = cssString.Substring(startPoint, classLength);
                className = className.Replace(" ", "");
                className = className.Replace("}", "");
                className = className.Replace(".dita-page.", ""); // remove the .dita-page since that is not needed
                className = className.Replace(".", " "); // remove the dot since the LoadStyle method does not take the dot  
                className = className.Replace("\r\n", "");
                //TextBox1.Text = className;
                //move startPoint to the next position in the string
                startPoint = pointer;

                //---------------------------------- LOOP STYLE NAME AND STYLE VALUE -------------------------------
                while (cssString[pointer].ToString() != "}")
                {
                    //---------------- Get Style Name -----------------------------------------------


                    // Get the style name. Get the string up to the :
                    while (cssString[pointer].ToString() != ":")
                    {
                        pointer += 1;
                    }
                    styleNameLength = pointer - startPoint;
                    styleName = cssString.Substring(startPoint, styleNameLength);
                    styleName = styleName.Replace("font-size", "size"); //iTextSharp does not support font-size. Have to change to size
                    styleName = styleName.Replace("font-color", "color");
                    styleName = styleName.Replace("{", ""); //remove the { and spaces
                    styleName = styleName.Replace(" ", "");
                    styleName = styleName.Replace(";", "");

                    startPoint = pointer;



                    // ------------------Get the style value ---------------------------------------------------


                    // get style value up to the ;
                    while (cssString[pointer].ToString() != ";")
                    {
                        pointer += 1;
                    }
                    styleValueLength = pointer - startPoint;
                    styleValue = cssString.Substring(startPoint, styleValueLength);
                    styleValue = styleValue.Replace("Myriad Pro Light Cond", "times"); //Myriad font not supported so change it to times
                    styleValue = styleValue.Replace("Myriad Pro Light SemiCond", "helvetica");
                    styleValue = styleValue.Replace(": ", "");
                    styleValue = styleValue.Replace(":", "");


                    //---------------- style the PDF --------------------------------------------------

                    myStyle.LoadStyle(className, styleName, styleValue);
                    pointer += 1;
                    startPoint = pointer;

                } //-------------------------  END WHILE LOOP FOR STYLE NAME AND STYLE VALUE -----------------------------

            } //----------------------------END WHILE LOOP FOR CLASS --------------------------------------------------------------
            pointer += 1;
            startPoint = pointer;
            //----------------------- Generate PDF -------------------------------------------
            StringReader htmlString = new StringReader(body);
            Document pdfDoc = new Document(PageSize.A4, 30f, 30f, 40f, 30f);

            HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
            PdfWriter.GetInstance(pdfDoc, context.Response.OutputStream);
            pdfDoc.Open();
            htmlparser.SetStyleSheet(myStyle);
            RunAsUser(() => htmlparser.Parse(htmlString), new ContextService().GetExecutionContext(), admUser);
            pdfDoc.Close();
            context.Response.ContentType = "application/pdf";
            context.Response.AddHeader("content-disposition", "attachment;filename=" + topicTitle + ".pdf");
            context.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            context.Response.Write(pdfDoc);
            context.Response.End();
        }

        public void RunAsUser(Action a, IExecutionContext context, User u)
        {
            lock (_runAsUserLock)
            {
                var originalUser = context.User;
                try
                {
                    context.User = u;
                    a();
                }
                finally
                {
                    context.User = originalUser;
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
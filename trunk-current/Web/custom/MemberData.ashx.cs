﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using System.Xml.Serialization;
using Omnicell.Custom.Data;
using Omnicell.Data.Model;
using Telligent.Evolution.Components;
using Telligent.Evolution.Extensibility.Api.Version1;
using TApi = Telligent.Evolution.Extensibility.Api.Version1.PublicApi;
using TEntities = Telligent.Evolution.Extensibility.Api.Entities.Version1;
using TExV1 = Telligent.Evolution.Extensibility.Api.Version1;
using Omnicell.Services;

namespace Omnicell75.Web.custom
{
    /// <summary>
    /// Summary description for MemberData
    /// </summary>
    [Serializable]
    [XmlType("User")]
    public class UserDetail
    {
        public string CSN { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string Products { get; set; }
        // [XmlArray("ProfileFieldPair")]
        public List<ProfileField> ProfileFields;
    }

    [Serializable]
    public class ProfileField
    {
        [XmlAttribute("Key")]
        public string key { get; set; }
        [XmlAttribute("Value")]
        public string value { get; set; }
    }

    public class MemberData : IHttpHandler
    {
        public string strIncludedColHeaders = "";
        protected List<TEntities.User> users = new List<TEntities.User>();
        private static object _runAsUserLock = new object();
        protected string strKol = "";

        public void RunAsUser(Action a, IExecutionContext context, User u)
        {
            lock (_runAsUserLock)
            {
                var originalUser = context.User;
                try
                {
                    context.User = u;
                    a();
                }
                finally
                {
                    context.User = originalUser;
                }
            }
        }

        public void PopulateUserList(int parmMaxRecords, int pageNumber)
        {
            // process the whole list if we have less than 4000 users
            if (parmMaxRecords < 4000)
            {
                foreach (TEntities.User user in TApi.Users.List(new TExV1.UsersListOptions { PageSize = parmMaxRecords, PageIndex = pageNumber, SortBy = "LastActiveDate" }))
                    users.Add(user);
            }
            else
            {
                int varPageSize = 2000;
                int varPageNumber = 0;
                int varUserCount = parmMaxRecords;

                while (varPageSize * varPageNumber < varUserCount)
                {
                    foreach (TEntities.User user in TApi.Users.List(new TExV1.UsersListOptions { PageSize = varPageSize, PageIndex = varPageNumber, SortBy = "LastActiveDate" }))
                        users.Add(user);
                    varPageNumber++;
                }

            }
        }


        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            StringWriter outStream = new StringWriter();
            MemberInfoDataProvider mdp = new MemberInfoDataProvider();
            WriteMessageToFile("1 handler start for: " + context.Request.UserAgent.ToString() + " from: " + context.Request.UserHostAddress.ToString() + DateTime.Now.ToString("yyyy-dd-MM_hh-mm-ss"));

            try
            {
                string apiKey = context.Request.QueryString["apikey"];

                if (apiKey != "gmjak34qvtveqvyg0ua")
                {
                    WriteMessageToFile("2.1 invalid api key. " + DateTime.Now.ToString("yyyy-dd-MM_hh-mm-ss"));
                    context.Response.Write("invalid api key");
                }
                else
                {
                    WriteMessageToFile("2.2 Starting Product processing. " + DateTime.Now.ToString("yyyy-dd-MM_hh-mm-ss"));
                    // get the parent product group from the querystring
                    int iParentProductGroup = 5;
                    if (context.Request.QueryString["productsparentgroupId"] != null)
                        iParentProductGroup = int.Parse(context.Request.QueryString["productsparentgroupId"]);

                    // valid API Key was passed in so parse the querystring
                    string strProducts = "";
                    if (context.Request.QueryString["products"] != null)
                        strProducts = context.Request.QueryString["products"];

                    WriteMessageToFile("2.3 Starting KOL processing. " + DateTime.Now.ToString("yyyy-dd-MM_hh-mm-ss"));
                    // handle kol querystring
                    if (context.Request.QueryString["kol"] != null)
                        strKol = context.Request.QueryString["kol"];

                    string strRole = "";
                    if (context.Request.QueryString["role"] != null)
                        strRole = context.Request.QueryString["role"];

                    WriteMessageToFile("2.4 Starting MaxRecords processing. " + DateTime.Now.ToString("yyyy-dd-MM_hh-mm-ss"));
                    int iMaxRecords = 0;
                    if (context.Request.QueryString["maxrecords"] != null)
                    {
                        var rtnvalue = Int32.TryParse(context.Request.QueryString["maxrecords"], out iMaxRecords);
                        if (iMaxRecords < 0)
                            iMaxRecords = 0;
                    }

                    WriteMessageToFile("2.5 Starting PageNumber processing. " + DateTime.Now.ToString("yyyy-dd-MM_hh-mm-ss"));
                    //add page number to the user list functionality
                    int pageNumber = 0;
                    if (context.Request.QueryString["page"] != null)
                    {
                        var rtnValue = Int32.TryParse(context.Request.QueryString["page"], out pageNumber);
                        if (pageNumber < 0)
                            pageNumber = 0;
                    }

                    string strOutputStyle = "";
                    if (context.Request.QueryString["output"] != null)
                        strOutputStyle = context.Request.QueryString["output"];

                    List<string> lstIncludeColumns = new List<String>();
                    lstIncludeColumns = context.Request.QueryString["includecols"].ToString().Split(',').ToList();
                    strIncludedColHeaders = context.Request.QueryString["includecols"].ToString();

                    User admUser = Telligent.Evolution.Users.GetUser("admin");
                    RunAsUser(() => PopulateUserList(iMaxRecords, pageNumber), new ContextService().GetExecutionContext(), admUser);
                    List<UserDetail> lstUserDetails = new List<UserDetail>();

                    WriteMessageToFile("3.0 Starting User processing. " + DateTime.Now.ToString("yyyy-dd-MM_hh-mm-ss"));
                    foreach (TEntities.User user in users)
                    {

                        // add this users details to the list
                        UserDetail usrdeets = new UserDetail();
                        usrdeets = ParseUserDetails(user, lstIncludeColumns, context);
                        if (strProducts.ToString() == "YES")
                            // try to use the RunAsUser method to grant the appropriate permissions to view group members. 
                            // usrdeets.Products = GetUsersProducts(user, context, iParentProductGroup);
                            // RunAsUser(() => usrdeets.Products = GetUsersProducts(user, context, iParentProductGroup), new ContextService().GetExecutionContext(), admUser);
                            System.Convert.ToInt32(user.Id);
                        usrdeets.Products = mdp.ListUserProducts(System.Convert.ToInt32(user.Id));
                        lstUserDetails.Add(usrdeets);
                    }
                    WriteMessageToFile("3.5 Starting Output Serialization. " + DateTime.Now.ToString("yyyy-dd-MM_hh-mm-ss"));
                    // get the list of roles and see if any passed in have a match
                    if (strOutputStyle == "XML")
                    {
                        XmlSerializer s = new XmlSerializer(typeof(List<UserDetail>), new XmlRootAttribute("Users"));
                        s.Serialize(outStream, lstUserDetails);
                    }
                    else
                    {
                        outStream = ConvertObjectToString(lstUserDetails);
                    }
                }
                WriteMessageToFile("4.0 Sending stream out for processing. " + DateTime.Now.ToString("yyyy-dd-MM_hh-mm-ss"));
                // finally send the item back
                context.Response.Write(outStream.ToString());
            }
            catch (Exception ex)
            {
                StringBuilder sberror = new StringBuilder();
                sberror.AppendFormat("Message:  {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sberror.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sberror.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sberror.AppendFormat("Source: {0}\n", ex.Source);

                context.Response.Write(sberror.ToString());
            }
        }

        // method to parse the profile fields and return a userdetails object loaded with proper data
        public UserDetail ParseUserDetails(TEntities.User user, List<String> lstColumns, HttpContext ctxt)
        {
            // object to load data into            
            UserDetail userdeets = new UserDetail();
            try
            {
                userdeets.CSN = user.ProfileFields["CSN"].Value.Replace(',', ';').ToString();
                userdeets.Email = (user.PrivateEmail != null) ? user.PrivateEmail.Replace(',', ';').ToString() : "";
                userdeets.FirstName = user.ProfileFields["FirstName"].Value.Replace(',', ';').ToString();
                userdeets.LastName = user.ProfileFields["LastName"].Value.Replace(',', ';').ToString();
                userdeets.Position = user.ProfileFields["Title"].Value.Replace(',', ';').ToString();
                userdeets.CSN += ";" + user.ProfileFields["RelatedCSNs"].Value.Replace(',', ';').ToString();
                // get any other columns and add them to the name value pairs
                userdeets.ProfileFields = new List<ProfileField>();

                foreach (string column in lstColumns)
                {
                    for (int i = 0; i < user.ProfileFields.Count; i++)
                    {
                        if (column.ToString() == user.ProfileFields[i].Label.ToString())
                        {
                            ProfileField inclcols = new ProfileField();
                            inclcols.key = user.ProfileFields[i].Label.ToString();
                            inclcols.value = user.ProfileFields[i].Value.Replace(',', ';').ToString();
                            userdeets.ProfileFields.Add(inclcols);
                        }
                    }

                    if (column.ToString() == "kol")
                    {
                        ProfileField inckol = new ProfileField();
                        inckol.key = "kol";
                        inckol.value = retrieveKolOptin(user.Id);
                        userdeets.ProfileFields.Add(inckol);
                    }

                }

            }

              //  
            catch (Exception ex)
            {
                ctxt.Response.Write("ERROR: Message: " + ex.Message);
            }
            userdeets.CSN = userdeets.CSN.TrimEnd(';');

            return userdeets;
        }
        // method to return the list of products that this user has access to
        public String GetUsersProducts(TEntities.User user, HttpContext ctxt, int iParentGroupID)
        {
            String strReturnValue = "";
            try
            {

                //// get the list of groups taht the current user has access to
                ////   WIDGET CODE: #set ($groups = $core_v2_group.List("%{ParentGroupId=$productsGroupId,PageSize=100, IncludeAllSubGroups=1}"))
                //TExV1.GroupsListOptions grpListOptions = new TExV1.GroupsListOptions();
                //grpListOptions.ParentGroupId = iParentGroupID; // Will have to look into this to make sure this is the parent products group
                //grpListOptions.IncludeAllSubGroups = true;
                //List<TEntities.Group> lstGroups = new List<TEntities.Group>();
                //List<int> lstUserGroups = new List<int>();
                //List<string> lstProduct = new List<string>();

                //foreach (TEntities.Group group in TApi.Groups.List(grpListOptions))
                //{
                //    lstGroups.Add(group);
                //    // also add this group to the list if the use belongs to it.
                //    TExV1.GroupUserMembersGetOptions grpuseroptions = new TExV1.GroupUserMembersGetOptions();
                //    grpuseroptions.UserId = user.Id;
                //    TEntities.GroupUser groupUser = new TEntities.GroupUser();
                //    groupUser = TApi.GroupUserMembers.Get((int)group.Id, grpuseroptions);
                //    if (groupUser != null)
                //    {
                //        bool? iismember = groupUser.IsDirectMember.Value;
                //        if (!iismember.Equals(null))
                //        {
                //            lstUserGroups.Add((int)groupUser.Group.Id);
                //            lstProduct.Add(group.Name.ToString());
                //            strReturnValue += group.Name.ToString() + ";";
                //           // strReturnValue += group.Name.ToString();
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                ctxt.Response.Write("ERROR: Message: " + ex.Message);
            }
            // strReturnValue = strReturnValue.TrimEnd(',') + ">";
            //if (strReturnValue.ToString() == "<>")
            //    strReturnValue = "";
            return strReturnValue.TrimEnd(';').ToString();
        }

        private List<int> List<T1>()
        {
            throw new NotImplementedException();
        }


        public static IEnumerable<string> ToCsv<T>(string separator, IEnumerable<T> objectlist)
        {
            FieldInfo[] fields = typeof(T).GetFields();
            PropertyInfo[] properties = typeof(T).GetProperties();
            yield return String.Join(separator, fields.Select(f => f.Name).Union(properties.Select(p => p.Name)).ToArray());
            foreach (var o in objectlist)
            {
                yield return string.Join(separator, fields.Select(f => (f.GetValue(o) ?? "").ToString())
                    .Union(properties.Select(p => (p.GetValue(o, null) ?? "").ToString())).ToArray());
            }
        }

        // method to return a string version of the data object
        public StringWriter ConvertObjectToString(List<UserDetail> userdetails)
        {
            StringWriter sw = new StringWriter();
            /*
                        for (int i = 0; i < userdetails.Count; i++)
                        {
                            // Dictionary<string, string> FD = (from x in userdetails.GetType().GetProperties() select x).ToDictionary(x => x.Name, x => (x.GetGetMethod().Invoke(userdetails, null) == null ? "" : x.GetGetMethod().Invoke(userdetails, null).ToString()));
                            // sw.WriteLine(String.Join(",", userdetails[i]).ToString());
                        }
             */
            Type type = typeof(UserDetail);
            System.Reflection.PropertyInfo[] properties = type.GetProperties();
            var sb = new StringBuilder();
            foreach (System.Reflection.PropertyInfo prp in properties)
            {
                if (prp.CanRead)
                {
                    sb.Append(prp.Name).Append(',');
                }
            }
            //            sb.Length--;
            if (strIncludedColHeaders.Length > 0)
                sb.Append(strIncludedColHeaders);

            sw.WriteLine(sb.ToString());
            sb.Clear();

            foreach (UserDetail userdeets in userdetails)
            {
                foreach (System.Reflection.PropertyInfo prp in properties)
                {
                    if (prp.CanRead)
                    {
                        sb.Append(prp.GetValue(userdeets, null)).Append(',');
                    }
                }
                //                 sb.Length--;
                // does it have included columns
                foreach (ProfileField prfield in userdeets.ProfileFields)
                {
                    if (prfield.key != "kol")
                    {
                        string strAddField = "";
                        strAddField = (prfield.value != null && prfield.value != "0") ? prfield.value.Replace(',', ' ').ToString() : "";
                        // sb.Append(strAddField.ToString()).Append(';');
                        sb.Append(strAddField.ToString()).Append(',');
                    }
                    else
                    {
                        string strAddField = "";
                        strAddField = (prfield.value != null) ? prfield.value.Replace(',', ' ').ToString() : "";
                        // sb.Append(strAddField.ToString()).Append(';');
                        sb.Append(strAddField.ToString()).Append(',');
                    }

                }
                sb.Length--;
                sw.WriteLine(sb.ToString());
                sb.Clear();
            }
            return sw;
        }

        // Method for retrieving key opinion leaders survey optin status
        public string retrieveKolOptin(int? userId)
        {
            var db = new OmnicellEntities();
            bool userIsInRole = false;
            string[] roleCustomer = { "Customer" };
            string optIn = string.Empty;

            try
            {

                Telligent.Evolution.Extensibility.Api.Entities.Version1.User user =
                      TApi.Users.Get(new UsersGetOptions() { Id = userId });

                if (user != null)
                {
                    userIsInRole = Telligent.Evolution.Extensibility.Api.Version1.PublicApi.RoleUsers.IsUserInRoles(user.Username, roleCustomer);

                    if (userIsInRole)
                    {
                        int userID = System.Convert.ToInt32(userId);
                        //    var record = db.Omnicell_KeyOpinionOptIn.FirstOrDefault(x => x.UserId == userID);

                        //    if (record != null)
                        //    {
                        //        optIn = record.Opt_In.ToString();
                        //    }
                        //    else
                        //    {
                        //        optIn = "-1";
                        //    }
                    }
                }

            }
            catch
            {
                return optIn;
            }


            return optIn;

        }
        private void WriteMessageToFile(string strMessage)
        {
            // write trace log
            try
            {
                string fileNameWithPath = System.Web.HttpContext.Current.Server.MapPath("~/custom/data/" + "MemberData.txt");
                using (StreamWriter sw = File.AppendText(fileNameWithPath))
                {
                    sw.WriteLine(strMessage.ToString());
                }
            }
            catch (Exception ex)
            {
                // do something with the error
            }
        }


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
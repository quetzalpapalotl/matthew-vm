﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using Omnicell.Services;
using Omnicell.Custom.Components;
using Omnicell.Custom.Components;
using System.Threading;
using Omnicell.Data.Model;
using Omnicell.Custom;
using Telligent.Evolution.Extensibility.Version1;

namespace Omnicell.Web.custom
{
    /// <summary>
    /// Summary description for tickets
    /// </summary>
    public class tickets : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //System.Web.Configuration.
            DateTime? rightNow = DateTime.Now;
            StringBuilder sbTrace = new StringBuilder();
            WriteMessageToFile("1 Handler start for: " + context.Request.UserAgent.ToString() + " from: " + context.Request.UserHostAddress.ToString() + " on: " + DateTime.Now.ToString());

            try
            {
                Utilities util = new Utilities();
                string apiKey = context.Request.QueryString["apikey"];
                if (apiKey != "gmjak34qvtveqvyg0ua")
                {
                    WriteMessageToFile("2.1 invalid api key");

                    context.Response.Write("invalid api key");
                }
                else
                {
                    StreamReader sr = new StreamReader(context.Request.InputStream);
                    string strSr = sr.ReadToEnd();
                    context.Request.InputStream.Position = 0;

                    // check to see if the bulkupdate flag was passed in
                    string BulkUpdate = "No";
                    if (context.Request.QueryString["BulkUpdate"] != null && context.Request.QueryString["BulkUpdate"] != "")
                        BulkUpdate = context.Request.QueryString["BulkUpdate"];
                    if (BulkUpdate == "Yes")
                    {
                        IServiceTicketService _svcServiceTickets = Telligent.Common.Services.Get<ServiceTicketService>();
                        _svcServiceTickets.BulkUpdate(strSr);
                    }
                    else
                    {


                        // check to see if the batchprocess flag was passed in
                        string BatchProcess = "No";
                        if (context.Request.QueryString["NoProcess"] != null && context.Request.QueryString["NoProcess"] != "")
                            BatchProcess = context.Request.QueryString["NoProcess"];
                        
                        // the code below is commented out because the old ticket process is no longer being used

                        //if (BatchProcess.ToString() == "Yes")
                        //{
                            WriteMessageToFile("2.5 Write out the contents to a file only.");
                            // Just write the file out.
                            var fileName = "ticket.xml";
                            string xmlfileNameWithPath = System.Web.HttpContext.Current.Server.MapPath("~/custom/data/" + fileName);
                            String holdXmlData = HttpUtility.HtmlDecode(strSr);
                            File.WriteAllText(xmlfileNameWithPath, holdXmlData);
                            // write the file name to the trace log
                            WriteMessageToFile("2.6 Wrote Stream to File ticket.xml.");
                        //}
                        //else
                        //{
                        //    WriteMessageToFile("3 ParseTicketXmlFile InputStream. ");
                        //    var result = "";

                        //    //byte[] byteArray = Encoding.UTF8.GetBytes(util.HandleSpecialXMLCharacters(context.Request.InputStream));
                        //    //MemoryStream CleanedStream = new MemoryStream(byteArray);

                        //    result = util.ParseTicketXmlFile(context.Request.InputStream);

                        //    WriteMessageToFile("4 ParseTicketXmlFile result: " + result.ToString());

                        //    context.Response.Write(result);

                        //    // SMR - 20130904 -- Added to write out the feed that came in
                        //    string WriteFileFlag = "No";
                        //    if (context.Request.QueryString["WriteFile"] != null && context.Request.QueryString["WriteFile"] != "")
                        //        WriteFileFlag = context.Request.QueryString["WriteFile"];

                        //    if (WriteFileFlag == "Yes")
                        //    {
                        //        var fileName = "tickets_" + DateTime.Now.ToString("yyyy-dd-MM_hh-mm-ss") + ".txt";
                        //        string xmlfileNameWithPath = System.Web.HttpContext.Current.Server.MapPath("~/custom/data/" + fileName);
                        //        String holdXmlData = HttpUtility.HtmlDecode(strSr);
                        //        File.WriteAllText(xmlfileNameWithPath, holdXmlData);
                        //        // write the file name to the trace log
                        //        WriteMessageToFile("4.1 Wrote Stream to File: " + xmlfileNameWithPath.ToString());
                        //    }
                        //}
                    }
                }

                ImmediateUpdate(rightNow);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("Message:  {0}\n", ex.Message);
                if (ex.InnerException != null)
                    sb.AppendFormat("InnerException: {0}\n", ex.InnerException);
                if (ex.StackTrace != null)
                    sb.AppendFormat("StackTrace: {0}\n", ex.StackTrace);
                if (ex.Source != null)
                    sb.AppendFormat("Source: {0}\n", ex.Source);

                WriteMessageToFile("4.5 ParseTicketXmlFile error: " + sb.ToString());

                context.Response.Write(sb.ToString());
            }
            WriteMessageToFile("10 Handler End for: " + context.Request.UserAgent.ToString() + " from: " + context.Request.UserHostAddress.ToString() + " on: " + DateTime.Now.ToString());

            
        }

        private void WriteMessageToFile(string strMessage)
        {
            // write trace log
            try
            {
                string fileNameWithPath = System.Web.HttpContext.Current.Server.MapPath("~/custom/data/" + "tickets.txt");
                using (StreamWriter sw = File.AppendText(fileNameWithPath))
                {
                    sw.WriteLine(strMessage.ToString());
                }
            }
            catch (Exception ex)
            {
                // do something with the error
            }
        }

        public void ImmediateUpdate(DateTime? rightNow)
        {
            Utilities util = new Utilities();
            var db = new OmnicellEntities();
            
            // the below lamba must be checked to ensure the information is correct

            var tickets = db.Omnicell_Ticket.Where(x => x.Updated > rightNow);
            var users = db.Omnicell_TicketEmailInfo.Where(x => x.IsImmediateUpdate == 1);
            var emailTickets = new List<Omnicell_Ticket>();

            foreach (var user in users)
            {

                foreach (var ticket in tickets)
                {
                    var sticket = db.Omnicell_TicketSubscription.FirstOrDefault(x => x.TicketId == ticket.Id && x.UserId == user.UserId); ;

                    if (sticket != null)
                    {
                        emailTickets.Add(ticket);
                    }

                }

                if (emailTickets.Count != 0)
                {
                    BuildEmail(emailTickets, user.Email, user.UserId);
                }

                emailTickets.Clear();
            }

        }

        public void BuildEmail(List<Omnicell_Ticket> tickets, string email, int? userId)
        {
            Utilities util = new Utilities();
            GoToTrainingPlugin gtt = PluginManager.Get<GoToTrainingPlugin>().FirstOrDefault();
            var content = new StringBuilder();
            content.Append("Below is your digest of updated service request tickets from myOmnicell:");
            content.Append(Environment.NewLine);
            content.Append(Environment.NewLine);
            string csn = "";

            List<string> exists = new List<string>();

            foreach (var ticket in tickets)
            {
                content.Append("Ticket Number: ");
                content.Append(ticket.Number);
                content.Append(Environment.NewLine);
                content.Append("CSN: ");
                content.Append(ticket.CSN);
                content.Append(Environment.NewLine);
                content.Append("Abstract: ");
                content.Append(ticket.Abstract);
                content.Append(Environment.NewLine);
                content.Append("Status: ");
                content.Append(ticket.Status);
                content.Append(Environment.NewLine);
                content.Append("Sub Status: ");
                content.AppendLine(ticket.SubStatus);
                content.Append(Environment.NewLine);
                content.Append("Last Update Date: ");
                content.AppendLine(ticket.Updated.Value.Date.ToString("MM/dd/yyyy"));
                content.Append(Environment.NewLine);
                content.AppendLine("___________________________________________________________________");
                content.Append(Environment.NewLine);

                if (exists.Count == 0)
                {
                    exists.Add(ticket.CSN);
                    csn += ticket.CSN + ",";
                }
                if (!exists.Exists(x => x == ticket.CSN))
                {
                    exists.Add(ticket.CSN);
                    csn += ticket.CSN + ",";
                }

            }


            string allCsn = csn.Remove(csn.LastIndexOf(','));
            string baseUrl = gtt.RestAPIUrl;
            string smtp = gtt.Smtp;
            string unsubscribe = string.Format(baseUrl + "/customer_portal/p/service-request-tickets.aspx?CSN={0}", allCsn);
            content.Append("To unsubscribe from service ticket notifications, click the link below and follow the instructions on the Service Request Tickets homepage.");
            content.Append(Environment.NewLine);
            content.Append(unsubscribe);

            string fromEmail = "noreply-myomnicell@omnicell.com";
            string toEmail = email;
            string toName = email;
            string fromName = "noreply-myomnicell@omnicell.com";
            string subject = "myOmnicell Service Request Ticket Digest";
            string body = content.ToString();

            util.SendEmail(fromEmail, fromName, toEmail, toName, subject, body, smtp);
            // Write insert email record sent code here
            util.insertEmailCount(userId);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
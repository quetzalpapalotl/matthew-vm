(function($)
{
	if (typeof $.telligent === 'undefined')
			$.telligent = {};

	if (typeof $.telligent.evolution === 'undefined')
		$.telligent.evolution = {};

	if (typeof $.telligent.evolution.widgets === 'undefined')
		$.telligent.evolution.widgets = {};

	var _attachHandlers = function(context)
	{
		var w = $('#' + context.wrapperId);

		$('.pager .previous', w).click(function()
		{
			_page(context, context.pageIndex - 1);
			return false;
		});

		$('.pager .next', w).click(function()
		{
			_page(context, context.pageIndex + 1);
			return false;
		});
	},
	_page = function (context, pageIndex)
	{
		if (context.searchText)
			_search(context, context.searchText, pageIndex);
		else
			_list(context, pageIndex);
	},
	_refresh = function(context)
	{
		if(context.searchText)
			_search(context, context.searchText, pageIndex);
		else
			_list(context, context.pageIndex);
	},
	_list = function(context, pageIndex)
	{
		$.telligent.evolution.get({
			url: context.listUrl,
			data: { w_pageIndex: pageIndex },
			success: function(response)
			{
				context.list.html(response);
				context.pageIndex = pageIndex;
				context.searchText = '';
				_attachHandlers(context);
			},
			defaultErrorMessage: context.error,
			error: function(xhr, desc, ex)
			{
				context.list.html('<div class="message error error__message">' + desc + '</div>');
			}
		});
	},
	_search = function(context, query, pageIndex)
	{
		$.telligent.evolution.get({
			url: context.searchUrl,
			data: { w_searchText: query, w_pageIndex: pageIndex },
			success: function(response)
			{
				context.list.html(response);
				context.pageIndex = pageIndex;
				context.searchText = query;
				_attachHandlers(context);
			},
			defaultErrorMessage: context.error,
			error: function(xhr, desc, ex)
			{
				context.list.html('<div class="message error error__message">' + desc + '</div>');
			}
		});
	};

	$.telligent.evolution.widgets.browseWikis =
	{
		register: function(context)
		{
			var w = $('#' + context.wrapperId);

			$('.field-item-input input', w)
				.click(function()
				{
					if ($(this).val() == context.defaultSearchText)
						$(this).val('');
				})
				.keyup(function(e)
				{
					context._lastKeyCode = e.keyCode;
					if (context._searchTimeout)
						clearTimeout(context._searchTimeout);

					var searchText = $(this).val();

					if (searchText.length > 0)
					{
						$('.internal-link.clear-search', w).show();
						context._searchTimeout = setTimeout(function()	{ _search(context, searchText, 0); }, 500);
					}
					else
					{
						$('.internal-link.clear-search', w).hide();
						context._searchTimeout = setTimeout(function() { _list(context, 0); }, 500);
					}
				});

			$('.internal-link.clear-search', w).click(function()
			{
				$('.field-item-input input', w).val('');
				_list(context, 0);
				$(this).hide();
				return false;
			}).hide();

			_attachHandlers(context);
		}
	};
})(jQuery);

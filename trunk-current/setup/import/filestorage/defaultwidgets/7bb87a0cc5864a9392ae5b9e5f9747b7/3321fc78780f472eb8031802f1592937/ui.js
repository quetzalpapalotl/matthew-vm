(function ($) {
    if (typeof $.telligent === 'undefined') { $.telligent = {}; }
    if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
    if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

    (function () {
        this.TaskSequence = {
            create: function (options) {
                var settings = $.extend({}, TaskSequence.create.defaults, options || {}),
					tasks = [], hasError = false, runCount = -1,
					proceed = function () {
					    runCount++;
					    if (tasks.length > 0 && runCount < tasks.length) {
					        if (!hasError) {
					            tasks[runCount]();
					        }
					    } else {
					        settings.onComplete();
					    }
					};
                var api = {
                    add: function (task) {
                        tasks.push(task);
                    },
                    success: function () {
                        proceed();
                    },
                    error: function () {
                        hasError = true;
                    },
                    run: function () {
                        proceed();
                    }
                };
                return api;
            }
        };
        TaskSequence.create.defaults = {
            onComplete: function () { }
        };
    } ());

    var _save = function (context) {
        $.telligent.evolution.post({
            url: context.saveUrl,
            data: _populateData(context),
            success: function (response) {
                if (response.redirectUrl) {
                    window.location = response.redirectUrl;
                }
            },
            defaultErrorMessage: context.saveErrorText,
            error: function (xhr, desc, ex) {
                $.telligent.evolution.notifications.show(desc, { type: 'error' });
                $('#' + context.wrapperId + ' a.save-post').parent().removeClass('processing');
                $('#' + context.wrapperId + ' a.save-post').removeClass('disabled');
            }
        });
    },
    _disableLastOwner = function(context) {
    	$('#' + context.wrapperId + ' select.membership-status').removeAttr('disabled');
    	$('#' + context.wrapperId + ' .delete-member a').css('visibility', 'visible');
    	if ($('#' + context.wrapperId + ' select.membership-status[data-userid]:has(:selected[value="Owner"])').length == 1) {
    		$('#' + context.wrapperId + ' select.membership-status[data-userid]:has(:selected[value="Owner"])').each(function() {
    			$(this).attr('disabled','disabled');
    			$('#' + context.wrapperId + ' .delete-member a[data-userid="' + $(this).attr('data-userid') + '"]').css('visibility', 'hidden');
    		});
    	}
    },
	_attachHandlers = function (context) {
		_disableLastOwner(context);
		
	    $('#' + context.wrapperId + ' .field-item.delete-member a').click(function () {
	        var userId = $(this).attr('data-userid');
	        var roleId = $(this).attr('data-roleid');
	        if (userId) {
	            if (window.confirm(context.deleteMemberConfirmationText)) {
	                $.telligent.evolution.del({
	                    url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/groups/{GroupId}/members/users/{UserId}.json',
	                    data: { GroupId: context.groupId, UserId: userId },
	                    dataType: 'json',
	                    success: function (response) {
	                        window.location.reload();
	                    },
	                    defaultErrorMessage: context.ajaxErrorText
	                });
	            }
	        }
	        else if (roleId) {
	            if (window.confirm(context.deleteMemberConfirmationText)) {
	                $.telligent.evolution.del({
	                    url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/groups/{GroupId}/members/roles/{RoleId}.json',
	                    data: { GroupId: context.groupId, RoleId: roleId },
	                    dataType: 'json',
	                    success: function (response) {
	                        window.location.reload();
	                    },
	                    defaultErrorMessage: context.ajaxErrorText
	                });
	            }
	        }

	        return false;
	    });

	    $('#' + context.wrapperId + ' select.membership-status').each(function () {
	        $(this).data('initialValue', $(this).val());
	    });

	    $('#' + context.wrapperId + ' select.membership-status').change(function () {
	        var selectObject = $(this);
	        var userId = selectObject.attr('data-userid');
	        var roleId = selectObject.attr('data-roleid');
	        var membershipType = selectObject.val();
	        var endpoint = null;
	        var data = null;
	        if (userId > 0) {
	        	endpoint = 'api.ashx/v2/groups/{GroupId}/members/users.json';
	        	data = { GroupId: context.groupId, UserId: userId, GroupMembershipType: membershipType };
	        } else if (roleId > 0) {
	        	endpoint = 'api.ashx/v2/groups/{GroupId}/members/roles.json';
	        	data = { GroupId: context.groupId, RoleId: roleId, GroupMembershipType: membershipType };
	        }
	        
	        if (endpoint) {
	            $.telligent.evolution.post({
	                url: $.telligent.evolution.site.getBaseUrl() + endpoint,
	                data: data,
	                dataType: 'json',
	                success: function (response) {
	                	selectObject.data('initialValue', selectObject.val());
	                    $.telligent.evolution.notifications.show(context.editCompleteNotification, { type: 'success', duration: 1000 });
	                    if (userId > 0) {
	                    	if (userId == context.userId) {
	                    		window.location.reload(true);
	                    	} else {
	                    		_disableLastOwner(context);
	                    	}
	                    }
	                },
	                error: function (response) {
	                    selectObject.val(selectObject.data('initialValue'));
	                    var responseObject = jQuery.parseJSON(response.responseText);
	                    var errorString = "";
	                    var altErrorString = "";
	                    var newErrorString = "";

	                    for (var x = 0; x < responseObject.Errors.length; x++) {
	                        var test = responseObject.Errors[x].indexOf("Conflict");

	                        if (test === -1) {

	                            if (altErrorString.length !== 0) {
	                                altErrorString = altErrorString + "<br>";
	                            }
	                            altErrorString = altErrorString + altErrorString;

	                        }
	                        else {
	                            newErrorString = responseObject.Errors[x].substring(9);

	                            if (errorString.length !== 0) {
	                                errorString = errorString + "<br>";
	                            }
	                            errorString = errorString + newErrorString;
	                        }
	                    }

	                    if (newErrorString.length > 0) {
	                        $.telligent.evolution.notifications.show(errorString, { type: 'success', duration: 7000 });
	                    }
	                    else {
	                        $.telligent.evolution.notifications.show(altErrorString, { type: 'success', duration: 7000 });
	                    }
	                }
	            });
	        }

	        return false;
	    });

	    $('#' + context.wrapperId + ' .field-item.accept-member a').click(function () {
	        var userId = $(this).attr('data-userid');
	        var membershipType = $(this).attr('data-membershiptype');
	        if (userId && membershipType) {
	            $.telligent.evolution.post({
	                url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/groups/{GroupId}/members/users.json',
	                data: { GroupId: context.groupId, UserId: userId, GroupMembershipType: membershipType },
	                dataType: 'json',
	                success: function (response) {
	                    window.location.reload();
	                },
	                defaultErrorMessage: context.ajaxErrorText
	            });
	        }

	        return false;
	    });

	    $('#' + context.wrapperId + ' .field-item.reject-member a').click(function () {
	        var userId = $(this).attr('data-userid');
	        if (userId) {
	            $.telligent.evolution.del({
	                url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/groups/{GroupId}/members/users/{UserId}.json',
	                data: { GroupId: context.groupId, UserId: userId },
	                dataType: 'json',
	                success: function (response) {
	                    window.location.reload();
	                },
	                defaultErrorMessage: context.ajaxErrorText
	            });
	        }

	        return false;
	    });

	    if (context.addMemberUserName && context.addMemberUserName.length > 0) {
	        context.addMemberSave.evolutionValidation({
	            onValidated: function (isValid, buttonClicked, c) {
	                if (isValid) {
	                    context.addMemberSave.removeClass('disabled');
	                }
	                else {
	                    context.addMemberSave.addClass('disabled');
	                }
	            },
	            onSuccessfulClick: function (e) {
					if (!context.addMemberSave.parent().hasClass('processing'))
						context.addMemberSave.parent().addClass('processing');
	                context.addMemberSave.addClass('disabled');

	                var tasks = TaskSequence.create({
	                    onComplete: function () {
	                        window.location.reload();
	                    }
	                });

	                var count = context.addMemberUserName.glowLookUpTextBox('count');
	                for (var i = 0; i < count; i++) {
	                    (function () {

	                        var item = context.addMemberUserName.glowLookUpTextBox('getByIndex', i);
	                        if (item) {
	                            var v = item.Value.split(/:/, 2);
	                            if (v[0] == 'user') {
	                                var userId = v[1];
	                                tasks.add(function () {
	                                    $.telligent.evolution.post({
	                                        url: context.addMemberUrl,
	                                        data: { Type: 'user', GroupId: context.groupId, UserId: userId, GroupMembershipType: context.addMemberMembershipType.val() },
	                                        dataType: 'json',
	                                        success: function (response) {
	                                            tasks.success();
	                                        },
	                                        defaultErrorMessage: context.ajaxErrorText,
	                                        error: function (xhr, desc, ex) {
	                                            tasks.error();
	                                            $.telligent.evolution.notifications.show(desc, { type: 'error' });
	                                            context.addMemberSave.removeClass('disabled');
												context.addMemberSave.parent().removeClass('processing');
	                                        }
	                                    });
	                                });
	                            }
	                            else if (v[0] == 'role') {
	                                var roleId = v[1];
	                                tasks.add(function () {
	                                    $.telligent.evolution.post({
	                                        url: context.addMemberUrl,
	                                        data: { Type: 'role', GroupId: context.groupId, RoleId: roleId, GroupMembershipType: context.addMemberMembershipType.val() },
	                                        dataType: 'json',
	                                        success: function (response) {
	                                            tasks.success();
	                                        },
	                                        defaultErrorMessage: context.ajaxErrorText,
	                                        error: function (xhr, desc, ex) {
	                                            tasks.error();
	                                            $.telligent.evolution.notifications.show(desc, { type: 'error' });
	                                            context.addMemberSave.removeClass('disabled');
												context.addMemberSave.parent().removeClass('processing');
	                                        }
	                                    });
	                                });
	                            }
	                            else if (v[0] == 'ldapUser') {
	                                var userName = v[1];
	                                tasks.add(function () {
	                                    $.telligent.evolution.post({
	                                        url: context.addMemberUrl,
	                                        data: { Type: 'ldapUser', GroupId: context.groupId, LdapName: userName, GroupMembershipType: context.addMemberMembershipType.val() },
	                                        dataType: 'json',
	                                        success: function (response) {
	                                            tasks.success();
	                                        },
	                                        defaultErrorMessage: context.ajaxErrorText,
	                                        error: function (xhr, desc, ex) {
	                                            tasks.error();
	                                            $.telligent.evolution.notifications.show(desc, { type: 'error' });
	                                            context.addMemberSave.removeClass('disabled');
												context.addMemberSave.parent().removeClass('processing');
	                                        }
	                                    });
	                                });
	                            }
	                            else if (v[0] == 'ldapRole') {
	                                var roleName = v[1];
	                                tasks.add(function () {
	                                    $.telligent.evolution.post({
	                                        url: context.addMemberUrl,
	                                        data: { Type: 'ldapRole', GroupId: context.groupId, LdapName: roleName, GroupMembershipType: context.addMemberMembershipType.val() },
	                                        dataType: 'json',
	                                        success: function (response) {
	                                            tasks.success();
	                                        },
	                                        defaultErrorMessage: context.ajaxErrorText,
	                                        error: function (xhr, desc, ex) {
	                                            tasks.error();
	                                            $.telligent.evolution.notifications.show(desc, { type: 'error' });
	                                            context.addMemberSave.removeClass('disabled');
												context.addMemberSave.parent().removeClass('processing');
	                                        }
	                                    });
	                                });
	                            }
	                        }
	                    })();
	                }

	                tasks.run();
	                return false;
	            }
	        });

	        context.addMemberUserName.bind('glowLookUpTextBoxChange', context.addMemberSave.evolutionValidation('addCustomValidation', 'requiredusername', function () {
	            return context.addMemberUserName.glowLookUpTextBox('count') > 0;
	        },
				context.addMemberUserNameMissingText,
				'#' + context.wrapperId + ' .field-item.username .field-item-validation',
				null));
	    }

	    if (context.inviteMemberByNameUserName && context.inviteMemberByNameUserName.length > 0) {
	        context.inviteMemberByNameInvite.evolutionValidation({
	            onValidated: function (isValid, buttonClicked, c) {
	                if (isValid) {
	                    context.inviteMemberByNameInvite.removeClass('disabled');
	                }
	                else {
	                    context.inviteMemberByNameInvite.addClass('disabled');
	                }
	            },
	            onSuccessfulClick: function (e) {
	                context.message.hide();
					if (!context.inviteMemberByNameInvite.parent().hasClass('processing'))
						context.inviteMemberByNameInvite.parent().addClass('processing');
	                context.inviteMemberByNameInvite.addClass('disabled');

	                var tasks = TaskSequence.create({
	                    onComplete: function () {
	                        context.message.show();
	                        context.inviteMemberByNameInvite.removeClass('disabled');
							context.inviteMemberByNameInvite.parent().removeClass('processing');
	                        while (context.inviteMemberByNameUserName.glowLookUpTextBox('count') > 0) {
	                            context.inviteMemberByNameUserName.glowLookUpTextBox('removeByIndex', 0);
	                        }
	                        context.inviteMemberByNameMessage.val('');
	                    }
	                });

	                var count = context.inviteMemberByNameUserName.glowLookUpTextBox('count');
	                for (var i = 0; i < count; i++) {
	                    (function () {

	                        var item = context.inviteMemberByNameUserName.glowLookUpTextBox('getByIndex', i);
	                        if (item) {
	                            var v = item.Value.split(/:/);
	                            if (v[0] == 'user') {
	                                var userId = v[1];
	                                tasks.add(function () {
	                                    $.telligent.evolution.post({
	                                        url: context.sendInvitationUrl,
	                                        data: { GroupId: context.groupId, UserId: userId, MembershipType: context.inviteMemberByNameMembershipType.val(), Message: context.inviteMemberByNameMessage.val() },
	                                        dataType: 'json',
	                                        success: function (response) {
	                                            tasks.success();
	                                        },
	                                        defaultErrorMessage: context.ajaxErrorText,
	                                        error: function (xhr, desc, ex) {
	                                            tasks.error();
	                                            $.telligent.evolution.notifications.show(desc, { type: 'error' });
	                                            context.inviteMemberByNameInvite.removeClass('disabled');
												context.inviteMemberByNameInvite.parent().removeClass('processing');
	                                        }
	                                    });
	                                });
	                            }
	                            else if (v[0] == 'ldapUser') {
	                                var userName = v[1];
	                                tasks.add(function () {
	                                    $.telligent.evolution.post({
	                                        url: context.sendInvitationUrl,
	                                        data: { GroupId: context.groupId, LdapName: userName, MembershipType: context.inviteMemberByNameMembershipType.val(), Message: context.inviteMemberByNameMessage.val() },
	                                        dataType: 'json',
	                                        success: function (response) {
	                                            tasks.success();
	                                        },
	                                        defaultErrorMessage: context.ajaxErrorText,
	                                        error: function (xhr, desc, ex) {
	                                            tasks.error();
	                                            $.telligent.evolution.notifications.show(desc, { type: 'error' });
												context.inviteMemberByNameInvite.removeClass('disabled');
												context.inviteMemberByNameInvite.parent().removeClass('processing');
	                                        }
	                                    });
	                                });
	                            }
	                        }
	                    })();
	                }

	                tasks.run();
	                return false;
	            }
	        });

	        context.inviteMemberByNameUserName.bind('glowLookUpTextBoxChange', context.inviteMemberByNameInvite.evolutionValidation('addCustomValidation', 'requiredusername', function () {
	            return context.inviteMemberByNameUserName.glowLookUpTextBox('count') > 0;
	        },
				context.inviteMemberByNameUserNameMissingText,
				'#' + context.wrapperId + ' .field-item.user-name .field-item-validation',
				null));
	    }

	    if (context.inviteMemberByEmailEmailAddressesSelector) {
	        context.inviteMemberByEmailInvite.evolutionValidation({
	            onValidated: function (isValid, buttonClicked, c) {
	                if (isValid) {
	                    context.inviteMemberByEmailInvite.removeClass('disabled');
	                }
	                else {
	                    context.inviteMemberByEmailInvite.addClass('disabled');
	                }
	            },
	            onSuccessfulClick: function (e) {
	                context.message.hide();
					if (!context.inviteMemberByEmailInvite.parent().hasClass('processing'))
						context.inviteMemberByEmailInvite.parent().addClass('processing');
	                context.inviteMemberByEmailInvite.addClass('disabled');

	                var tasks = TaskSequence.create({
	                    onComplete: function () {
	                        context.message.show();
	                        context.inviteMemberByEmailInvite.removeClass('disabled');
							context.inviteMemberByEmailInvite.parent().removeClass('processing');
	                        $(context.inviteMemberByEmailEmailAddressesSelector).val('');
	                        $(context.inviteMemberByEmailMessageSelector).val('');
	                    }
	                });

	                var addresses = $(context.inviteMemberByEmailEmailAddressesSelector).val().split(/[,;]/g);
	                $.each(addresses, function (i, address) {
	                    address = $.trim(address);
	                    if (address) {
	                        tasks.add(function () {
	                            $.telligent.evolution.post({
	                                url: context.sendInvitationUrl,
	                                data: { GroupId: context.groupId, Email: address, MembershipType: context.inviteMemberByEmailMembershipType.val(), Message: $(context.inviteMemberByEmailMessageSelector).val() },
	                                dataType: 'json',
	                                success: function (response) {
	                                    tasks.success();
	                                },
	                                defaultErrorMessage: context.ajaxErrorText,
	                                error: function (xhr, desc, ex) {
	                                    tasks.error();
	                                    $.telligent.evolution.notifications.show(desc, { type: 'error' });
	                                    context.inviteMemberByNameInvite.removeClass('disabled');
										context.inviteMemberByNameInvite.parent().removeClass('processing');
	                                }
	                            });
	                        });
	                    }
	                });

	                tasks.run();
	                return false;
	            }
	        });

	        context.inviteMemberByEmailInvite.evolutionValidation('addField', context.inviteMemberByEmailEmailAddressesSelector,
			{
			    required: true,
			    emails: true,
			    messages:
				{
				    required: context.inviteMemberByEmailEmailAddressRequiredText,
				    emails: context.inviteMemberByEmailEmailAddressInvalidText
				}
			}, '#' + context.wrapperId + ' .field-item.email .field-item-validation', null);
	    }
	};

    $.telligent.evolution.widgets.groupMembershipList = {
        register: function (context) {
            if (context.membershipTabs && context.membershipTabs.length > 0) {
                context.membershipTabs.glowTabbedPanes({
                    cssClass: 'tab-pane',
                    tabSetCssClass: 'tab-set with-panes',
                    tabCssClasses: ['tab'],
                    tabSelectedCssClasses: ['tab selected'],
                    tabHoverCssClasses: ['tab hover'],
                    enableResizing: false,
                    tabs: [
						[context.membersTabId, context.membersTabText, null],
						[context.roleMembersTabId, context.roleMembersTabText, null]
					]
                });

                var tab = context.membershipTabs.glowTabbedPanes('getByIndex', context.membershipTabSelectedIndex);
                if (tab) {
                    context.membershipTabs.glowTabbedPanes('selected', tab);
                }
            }

            if (context.addMemberTabs && context.addMemberTabs.length > 0) {
                context.addMemberTabs.glowTabbedPanes({
                    cssClass: 'tab-pane',
                    tabSetCssClass: 'tab-set with-panes',
                    tabCssClasses: ['tab'],
                    tabSelectedCssClasses: ['tab selected'],
                    tabHoverCssClasses: ['tab hover'],
                    enableResizing: false,
                    tabs: [
						[context.addMemberTabId, context.addMemberTabText, null],
						[context.inviteMemberByNameTabId, context.inviteMemberByNameTabText, null],
						[context.inviteMemberByEmailTabId, context.inviteMemberByEmailTabText, null]
					]
                });
                if (window.location.hash === '#invite') {
                    var inviteTab = context.addMemberTabs.glowTabbedPanes('getById', context.inviteMemberByNameTabId);
                    if (inviteTab) {
                        context.addMemberTabs.glowTabbedPanes('selected', inviteTab);
                    }
                }
            }

            if (context.addMemberUserName && context.addMemberUserName.length > 0) {
                context.addMemberUserName.glowLookUpTextBox(
				{
				    emptyHtml: '',
				    maxValues: 20,
				    onGetLookUps: function (tb, searchText) {
				        window.clearTimeout(context.addMemberUserNameTimeout);
				        if (searchText && searchText.length >= 2) {
				            tb.glowLookUpTextBox('updateSuggestions', [tb.glowLookUpTextBox('createLookUp', '', '<div style="text-align: center;"><img src="' + $.telligent.evolution.site.getBaseUrl() + 'utility/spinner.gif" /></div>', '<div style="text-align: center;"><img src="' + $.telligent.evolution.site.getBaseUrl() + 'utility/spinner.gif" /></div>', false)]);
				            context.addMemberUserNameTimeout = window.setTimeout(function () {
				                $.telligent.evolution.get({
				                    url: context.findUsersOrRolesUrl,
				                    data: { w_SearchText: searchText, w_IncludeRoles: 'True' },
				                    success: function (response) {
				                        if (response && response.matches.length > 1) {
				                            var suggestions = [];
				                            for (var i = 0; i < response.matches.length; i++) {
				                                var item = response.matches[i];
				                                if (item && item.userId) {
				                                    suggestions[suggestions.length] = tb.glowLookUpTextBox('createLookUp', 'user:' + item.userId, item.title, item.title, true);
				                                }
				                                else if (item && item.roleId) {
				                                    suggestions[suggestions.length] = tb.glowLookUpTextBox('createLookUp', 'role:' + item.roleId, item.title, item.title, true);
				                                }
				                                else if (item && item.ldapRoleId) {
				                                    suggestions[suggestions.length] = tb.glowLookUpTextBox('createLookUp', 'ldapRole:' + item.ldapRoleId + ':' + item.ldapRoleDn, item.title, item.title, true);
				                                }
				                                else if (item && item.ldapUserId) {
				                                    suggestions[suggestions.length] = tb.glowLookUpTextBox('createLookUp', 'ldapUser:' + item.ldapUserId, item.title, item.title, true);
				                                }
				                            }

				                            tb.glowLookUpTextBox('updateSuggestions', suggestions);
				                        }
				                        else
				                            tb.glowLookUpTextBox('updateSuggestions', [tb.glowLookUpTextBox('createLookUp', '', context.noUserOrRoleMatchesText, context.noUserOrRoleMatchesText, false)]);
				                    }
				                });
				            }, 749);
				        }
				    },
				    selectedLookUpsHtml: []
				});
            }

            if (context.inviteMemberByNameUserName && context.inviteMemberByNameUserName.length > 0) {
                context.inviteMemberByNameUserName.glowLookUpTextBox(
				{
				    emptyHtml: '',
				    maxValues: 20,
				    onGetLookUps: function (tb, searchText) {
				        window.clearTimeout(context.inviteMemberByNameUserNameTimeout);
				        if (searchText && searchText.length >= 2) {
				            tb.glowLookUpTextBox('updateSuggestions', [tb.glowLookUpTextBox('createLookUp', '', '<div style="text-align: center;"><img src="' + $.telligent.evolution.site.getBaseUrl() + 'utility/spinner.gif" /></div>', '<div style="text-align: center;"><img src="' + $.telligent.evolution.site.getBaseUrl() + 'utility/spinner.gif" /></div>', false)]);
				            context.inviteMemberByNameUserNameTimeout = window.setTimeout(function () {
				                $.telligent.evolution.get({
				                    url: context.findUsersOrRolesUrl,
				                    data: { w_SearchText: searchText, w_IncludeRoles: 'False' },
				                    success: function (response) {
				                        if (response && response.matches.length > 1) {
				                            var suggestions = [];
				                            for (var i = 0; i < response.matches.length; i++) {
				                                var item = response.matches[i];
				                                if (item && item.userId) {
				                                    suggestions[suggestions.length] = tb.glowLookUpTextBox('createLookUp', 'user:' + item.userId, item.title, item.title, true);
				                                }
				                                else if (item && item.ldapUserId) {
				                                    suggestions[suggestions.length] = tb.glowLookUpTextBox('createLookUp', 'ldapUser:' + item.ldapUserId, item.title, item.title, true);
				                                }
				                            }

				                            tb.glowLookUpTextBox('updateSuggestions', suggestions);
				                        }
				                        else
				                            tb.glowLookUpTextBox('updateSuggestions', [tb.glowLookUpTextBox('createLookUp', '', context.noUserOrRoleMatchesText, context.noUserOrRoleMatchesText, false)]);
				                    }
				                });
				            }, 749);
				        }
				    },
				    selectedLookUpsHtml: []
				});
            }

            _attachHandlers(context);
        }
    };
})(jQuery);

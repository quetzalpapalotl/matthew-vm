(function($)
{
	if (typeof $.telligent === 'undefined') {$.telligent = {};}
	if (typeof $.telligent.evolution === 'undefined') {$.telligent.evolution = {};}
	if (typeof $.telligent.evolution.widgets === 'undefined') {$.telligent.evolution.widgets = {};}

	var approveContactRequest = function(context, contactRequestId)
	{
		$.telligent.evolution.put({
			url: context.approveContactUrl,
			data: { GroupId: context.groupId, ContactRequestId: contactRequestId },
			success: function(response)
			{
				reloadPage();
			}
		});
	},
	deleteContactRequest = function(context, contactRequestId)
	{
		$.telligent.evolution.del({
			url: context.deleteContactUrl,
			data: { GroupId: context.groupId, ContactRequestId: contactRequestId },
			success: function(response)
			{
				reloadPage();
			}
		});
	},
	reloadPage = function()
	{
		window.location = window.location;
	};

	$.telligent.evolution.widgets.groupContactRequestList =
	{
		register: function(context)
		{
			$(context.wrapperSelector).find('.internal-link.deny-contact-request').click(
			function()
			{
				if (confirm(context.deleteConfirmationText))
				{
					var p = $(this).parents('.content-item');
					if (p.length > 0)
					{
						var id = p.attr('data-request-id');
						deleteContactRequest(context, id);
					}
				}

				return false;
			});

			$(context.wrapperSelector).find('.internal-link.approve-contact-request').click(
			function()
			{
				var p = $(this).parents('.content-item');
				if (p.length > 0)
				{
					var id = p.attr('data-request-id');
					approveContactRequest(context, id);
				}

				return false;
			});
		}
	};
}(jQuery));

(function($)
{
	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

	var _createFolder = function(context, folderName)
		{
			$.telligent.evolution.post({
				url: context.createUrl,
				data: { folderName:folderName, userId:context.userId, parentFolderPath:context.parentFolderPath },
				dataType: 'json',
				defaultErrorMessage: context.createFolderErrorMessage,
				success: function(response)
				{
					window.location = context.viewParentFolderUrl;
				}
			});
		};

	$.telligent.evolution.widgets.addUserFolder =
	{
		register: function(context)
		{
			$(context.folderNameInputSelector).keyup(function(e)
			{
				if ((e.keyCode || e.which) == 13 && $(context.saveFolderButton).evolutionValidation('validate'))
				{
					var folderName = $(context.folderNameInputSelector).val();
					_createFolder(context, folderName);
				}				
			});
			
			context.saveFolderButton.evolutionValidation(
			{
				validateOnLoad : false,
				onValidated: function(isValid, buttonClicked, c) 
				{ 
					if (isValid) 
						context.saveFolderButton.removeAttr('disabled'); 
					else
						context.saveFolderButton.attr('disabled', 'disabled');
				},
				onSuccessfulClick: function(e) 
				{
					context.saveFolderButton.attr('disabled', 'disabled');
					var folderName = $(context.folderNameInputSelector).val();
					_createFolder(context, folderName);
				}
			}).evolutionValidation('addField', context.folderNameInputSelector,	
			{ 
				required: true,
				messages: 
				{ 
					required: context.folderNameRequiredText
				} 
			}, '#' + context.wrapperId + ' .field-item.folder-name .field-item-validation', null);
		}
	};
})(jQuery);

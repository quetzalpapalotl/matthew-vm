(function($)
{
	if (typeof $.telligent === 'undefined')
	        $.telligent = {};

	if (typeof $.telligent.evolution === 'undefined')
	    $.telligent.evolution = {};

	if (typeof $.telligent.evolution.widgets === 'undefined')
	    $.telligent.evolution.widgets = {};

	var _attachHandlers = function(context) {
		var w = $('#' + context.wrapperId);
		
		$('.pager .previous', w).click(function() {
			_page(context, context.pageIndex - 1);
			return false;
		});
		
		$('.pager .next', w).click(function() {
			_page(context, context.pageIndex + 1);
			return false;
		});
		
		$('.table-header-column', w).click(function() {
			if ($('a', this).length > 0)
			{
				var sortBy = $(this).attr('class').split(' ')[1];
				context.sortBy = _convertClassToSortBy(context, sortBy);
				var sortOrder = $('a', this).attr('class').split(' ')[1];
				context.sortOrder = _convertClassToSortOrder(context, sortOrder, true);
				_list(context, context.pageIndex);
			}
		});
		
		$('.table-header-column a', w).click(function(e) {
			$(this).parent().click();
			return false;
		});
	},
	_page = function (context, pageIndex) {
		if (context.searchText)
			_search(context, context.searchText, pageIndex);
		else
			_list(context, pageIndex);
	},
	_refresh = function(context) {
		if(context.searchText)
			_search(context, context.searchText, pageIndex);
		else
			_list(context, context.pageIndex);
	},
	_list = function(context, pageIndex) {
		$.telligent.evolution.get({
			url: context.listUrl,
			data: { w_pageIndex: pageIndex, w_sortBy: (context.sortBy || context.defaultSortBy), w_sortOrder: (context.sortOrder || context.defaultSortOrder) },
			success: function(response) {
				context.list.html(response);
				context.pageIndex = pageIndex;
				context.searchText = '';
				_attachHandlers(context);
			},
			defaultErrorMessage: context.error,
			error: function(xhr, desc, ex) {
				context.list.html('<div class="message error error__message">' + desc + '</div>');
			}
		});
	},
	_search = function(context, query, pageIndex) {
		$.telligent.evolution.get({
			url: context.searchUrl,
			data: { w_searchText: query, w_pageIndex: pageIndex },
			success: function(response) {
				context.list.html(response);
				context.pageIndex = pageIndex;
				context.searchText = query;
				_attachHandlers(context);
			},
			defaultErrorMessage: context.error,
			error: function(xhr, desc, ex) {
				context.list.html('<div class="message error error__message">' + desc + '</div>');
			}
		});
	},
	_convertClassToSortBy = function(context, sortByClass) {
		switch(sortByClass) {
			case 'post-count': return 'TotalPosts';
			case 'post-date': return 'LastPost';
			case 'application-title':
			case 'application-group':
			default: return 'Name';
		}
	},
	_convertClassToSortOrder = function(context, sortOrderClass, reverse) {
		switch(sortOrderClass) {
			case 'sort-ascending': 
				if(!reverse)
					return 'Ascending';
				else
					return 'Descending';
				break;
			case 'sort-descending': 
			default: 
				if(!reverse)
					return 'Descending';
				else
					return 'Ascending';
				break;
		}
	};
		
	$.telligent.evolution.widgets.browseForums = {
		register: function(context) {
			var w = $('#' + context.wrapperId);
		
			$('.field-item-input input', w).click(function() {
					if ($(this).val() == context.defaultSearchText)
						$(this).val('');
				}).keyup(function(e) {
					context._lastKeyCode = e.keyCode;
					if (context._searchTimeout)
						clearTimeout(context._searchTimeout);
					
					var searchText = $(this).val();
					
					if (searchText.length > 0) {
						$('.internal-link.clear-search', w).show();
						context._searchTimeout = setTimeout(function()	{ _search(context, searchText, 0); }, 500);
					}
					else {
						$('.internal-link.clear-search', w).hide();
						context._searchTimeout = setTimeout(function() { _list(context, 0); }, 500);
					}
				});
			
			$('.internal-link.clear-search', w).click(function() {
				$('.field-item-input input', w).val('');
				_list(context, 0);
				$(this).hide();
				return false;
			}).hide();
				
			_attachHandlers(context);
		}
	};
})(jQuery);

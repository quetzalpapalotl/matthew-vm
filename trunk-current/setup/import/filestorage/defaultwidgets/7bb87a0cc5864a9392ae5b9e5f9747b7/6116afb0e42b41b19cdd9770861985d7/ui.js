(function($) {
    
    var refresh = function(context, link) {
        
        var update = function() {
            $.telligent.evolution.get({
                url: context.refreshUrl,
                cache: false,
                success: function(response) {
                    var response = $(response);
                    $('#' + context.wrapperId + ' ul.content-list').html(response);
                    addEventHandlers(context);
                }
            });
        };
        
        if (link) {
            link.closest('li.content-item').slideUp('fast', update); 
        } else {
            update();
        }
    },
    addEventHandlers = function(context) {
        // friending
        $('#' + context.wrapperId + ' a.request-friendship').on('click', function(){
            var link = $(this);
            var url = link.attr('href');
        	Telligent_Modal.Open(url, 550, 360, function() { refresh(context, link) });
        	return false;
        });
        
        // follow
        $('#' + context.wrapperId + ' a.follow-user').on('click', function(){
            var link = $(this);
            var oldHtml = link.html();
            link.html(context.followProcessingHtml);
        	$.telligent.evolution.post({
        		url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/users/{FollowerId}/following.json',
        		data: {
        			FollowerId: context.userId,
        			FollowingId: link.data('userid')
        		},
        		success: function(response) { 
                    window.setTimeout(function() { refresh(context, link); }, 499);
                },
        		error: function(xhr, desc, ex) { link.html(oldHtml); }
        	});
            
            return false;
        });
        
        // ignore
        $('#' + context.wrapperId + ' a.ignore-suggestion').on('click', function() {
            var link = $(this);
            $.telligent.evolution.post({
            	url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/user/recommendation/ignore/{UserId}.json',
            	data: {
            		UserId: link.data('userid')
            	},
            	success: function(response) { 
                    refresh(context, link);
                }
            });
            
            return false;
        }).hide();
        
        $('#' + context.wrapperId + ' li.content-item').on('mouseover swiperight', function(e) {
            var elm = $(this);
            $('a.ignore-suggestion', elm).show();
            
            if (e.type == 'swiperight') {
                $(document).on('click', function() {
                    $('a.ignore-suggestion', elm).hide();
                });
            }            
        }).on('mouseout', function() {
            $('a.ignore-suggestion', $(this)).hide();
        });
    };

	var api = {
		register: function(context) {
			addEventHandlers(context);
		}
	};



	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.peopleYouMayKnow = api;

}(jQuery));
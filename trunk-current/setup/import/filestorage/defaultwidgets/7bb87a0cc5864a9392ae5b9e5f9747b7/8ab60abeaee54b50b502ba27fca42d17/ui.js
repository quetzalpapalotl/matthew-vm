(function(j, global){

	if (typeof j.telligent === 'undefined') {
		j.telligent = {};
	}

	if (typeof j.telligent.evolution === 'undefined') {
		j.telligent.evolution = {};
	}

	if (typeof j.telligent.evolution.widgets === 'undefined') {
		j.telligent.evolution.widgets = {};
	}

	var _processPage = function(context, jQ)
		{
			jQ.filter('.slideshow-item')
				.each(function() {
					var t = j(this);
					t.data('userFileSlideshow', { fileName: t.find('input').val(), index: context._files.length});
					context._files[context._files.length] = t;

					t
						.bind('mouseover', function() {
							if (j(this).data('userFileSlideshow').index !== context._currentIndex) {
								j(this).children().css('background-color', '#eee');
							}
						})
						.bind('mouseout', function() {
							if (j(this).data('userFileSlideshow').index === context._currentIndex) {
								j(this).children().css('background-color', '#ccc');
							} else {
								j(this).children().css('background-color','');
							}
						})
						.bind('click', function() {
							_showItem(context, j(this).data('userFileSlideshow').index);
						});
				});
		},
		_showItem = function(context, index)
		{
			if (context._currentIndex === index || index >= context._files.length || index < 0) {
				return;
			}

			context._files[context._currentIndex].children().css('background-color', '');
			context._currentIndex = index;
			context._files[index].children().css('background-color', '#ccc');

			j.telligent.evolution.get({
					url: context.viewUrl,
					data: { w_fileName: context._files[index].data('userFileSlideshow').fileName },
					success: function(response)
					{
						context.currentFile.html(response);
						if (context._playing)
						{
							global.clearTimeout(context._playHandle);
							context._playHandle = global.setTimeout(function() { _moveNext(context); }, context.itemDuration);
						}
					},
					error: function(xhr, desc)
					{
						j.telligent.evolution.notifications.show(desc,{type:'error'});
						if (context._playing)
						{
							global.clearTimeout(context._playHandle);
							context._playHandle = global.setTimeout(function() { _moveNext(context); }, context.itemDuration);
						}
					}
				});

			var scrollIndex = (index - Math.floor((context.allFiles.parent().width() / (context.itemWidth * 2)) - 0.5)) * -context.itemWidth;
			if (scrollIndex > 0) {
				scrollIndex = 0;
			}

			context.allFiles.animate({ left: scrollIndex + 'px' }, 499, function() { _validatePageLoaded(context); });

			if (index > 0) {
				context.previous.find('img').attr('src', context.previousImageUrl);
			} else {
				context.previous.find('img').attr('src', context.previousDisabledImageUrl);
			}

			if (index + 1 < context.totalItems) {
				context.next.find('img').attr('src', context.nextImageUrl);
			} else {
				context.next.find('img').attr('src', context.nextDisabledImageUrl);
			}

			_validatePageLoaded(context);
		},
		_validatePageLoaded = function(context)
		{
			if (context._currentIndex + 1 + Math.ceil(context.allFiles.parent().width() / context.itemWidth) > context._files.length && context._files.length !== context.totalItems)
			{
				var pageToLoad = Math.ceil(context._files.length / context.itemsPerPage);
				if (pageToLoad > context._lastPageLoaded)
				{
					context._lastPageLoaded = pageToLoad;
					j.telligent.evolution.get({
						url: context.pageUrl,
						data: { w_pageIndex: pageToLoad, w_offset: (context._files.length * context.itemWidth) },
						success: function(response)
						{
							var p = j(response);
							_processPage(context, p);
							context.allFiles.append(p);
						},
						error: function(xhr, desc)
						{
							j.telligent.evolution.notifications.show(desc,{type:'error'});
							context._lastPageLoaded = pageToLoad - 1;
						}
					});
				}
			}
		},
		_moveNext = function(context)
		{
			if (context._currentIndex + 1 < context.totalItems) {
				_showItem(context, context._currentIndex + 1);
			} else {
				_showItem(context, 0);
			}
		},
		_movePrevious = function(context)
		{
			if (context._currentIndex > 0) {
				_showItem(context, context._currentIndex - 1);
			}
		},
		_togglePlay = function(context)
		{
			if (context._playing)
			{
				context._playing = false;
				global.clearTimeout(context._playHandle);
				context.play.html(context.pausedText)
					.removeClass('slideshow-playing')
					.addClass('slideshow-paused');
			}
			else
			{
				context._playing = true;
				context._playHandle = global.setTimeout(function() { _moveNext(context); }, context.itemDuration);
				context.play.html(context.playingText)
					.removeClass('slideshow-paused')
					.addClass('slideshow-playing');
			}
		};

	j.telligent.evolution.widgets.userFileSlideshow = {
		register: function(context) {
			context._files = [];
			_processPage(context, context.allFiles.children());
			context._currentIndex = 0;
			context._playing = true;
			context._lastPageLoaded = 0;
			context._playHandle = global.setTimeout(function() { _moveNext(context); }, context.itemDuration);
			context.play.click(function() { _togglePlay(context); return false; });
			context.next.click(function() { _moveNext(context); return false; });
			context.previous.click(function() { _movePrevious(context); return false; });

			if (context._files.length > 0) {
				context._files[0].children().css('background-color', '#ccc');
			}

			if (context.totalItems <= 1) {
				context.next.find('img').attr('src', context.nextDisabledImageUrl);
			}
		}
	};

})(jQuery, window);

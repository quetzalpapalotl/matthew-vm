(function($)
{
	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }

	var _deleteFile = function(context)
		{
			$.telligent.evolution.post({
				url: context.deleteUrl,
				data: { fileName:context.fileName, folderPath:context.folderPath, userId:context.userId },
				dataType: 'json',
				success: function(response)
				{
					if (response.fileDeleted)
						window.location = context.viewFolderUrl;
				}
			});
		};

	$.telligent.evolution.widgets.userFileLinks =
	{
		register: function(context)
		{
			$(context.deleteButtonSelector).click(function()
			{
				if (confirm(context.deleteFileConfirmation))
					_deleteFile(context);
					
				return false;
			});
		}
	};
})(jQuery);

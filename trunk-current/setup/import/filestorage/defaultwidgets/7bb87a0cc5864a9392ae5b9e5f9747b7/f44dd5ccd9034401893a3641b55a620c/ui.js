(function($)
{
	if (typeof $.telligent === 'undefined')
		$.telligent = {};

	if (typeof $.telligent.evolution === 'undefined')
		$.telligent.evolution = {};

	if (typeof $.telligent.evolution.widgets === 'undefined')
		$.telligent.evolution.widgets = {};

	var tabClick = function(context, tab)
		{
			var t = $(tab);
			var wrapper = $('#' + context.elementId);
			$('.filter-option', wrapper).removeClass('selected');
			$(t.parent().get(0)).addClass('selected');
			var ids = tab.id.split('_');
			var filter = ids[ids.length - 1];
			context.selectedTab = filter;

			load(context, filter);
		},
		submitClick = function(context)
		{
			var inTags = context.tags.val().split(/[,;]/g);
			var tags = [];
			for(var i = 0; i < inTags.length; i++)
			{
				var tag = $.trim(inTags[i]);
				if (tag)
					tags[tags.length] = tag;
			}
			tags = tags.join(',');

			var data = {
				Title: context.title.evolutionComposer('val'),
				Body: context.getBody(),
				Tags: tags,
				WikiId: context.wikiId
			};

			if (context.userCanMovePage && context.parent)
				data.ParentPageId = context.parent.val() ? context.parent.val() : -1;

			if (context.threadId > 0)
				data.ForumThreadId = context.threadId;

			if (context.feature && context.feature.length > 0)
			{
				data.IsFeatured = context.feature.is(':checked') ? true : false;
				if (context.featuredImage && context.featuredImage.length > 0)
					data.FeaturedImage = context.featuredImage.val();
			}

			if (context.pageId > 0)
			{
				data.Id = context.pageId;

				$.telligent.evolution.get({
					url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/wikis/{WikiId}/pages/{Id}.json?IncludeFields=WikiPage.RevisionNumber,User,WikiPage.IsLocked',
					cache: false,
					data: {
						WikiId: data.WikiId,
						Id: data.Id
					},
					success: function(response)
					{
						if (response.WikiPage.IsLocked && !context.userCanLock)
						{
							context.message.text(context.lockedText);
							context.message.show();
							$('.processing', context.save.parent()).css("visibility", "hidden");
							context.save.removeClass('disabled');
							return;
						}
						else
						{
							context.message.hide();
						}
						if (response.WikiPage.RevisionNumber == context.originalRevision)
						{
							data.IsLocked=response.WikiPage.IsLocked
							$.telligent.evolution.put({
								url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/wikis/{WikiId}/pages/{Id}.json?IncludeFields=WikiPage.Id,WikiPage.Url',
								data: data,
								success: function(response)
								{
									$.telligent.evolution.post({
										url: context.subscribeUrl,
										data: {wikiPageId: response.WikiPage.Id, subscribe: context.subscribe.is(':checked')},
										success: function(response2)
										{
											window.location = response.WikiPage.Url;
										},
										error: function(xhr, desc, ex)
										{
											$.telligent.evolution.notifications.show(desc,{type:'error'});
											$('.processing', context.save.parent()).css("visibility", "hidden");
											context.save.removeClass('disabled');
										}
									});
								},
								error: function(xhr, desc, ex)
								{
									$.telligent.evolution.notifications.show(desc,{type:'error'});
									$('.processing', context.save.parent()).css("visibility", "hidden");
									context.save.removeClass('disabled');
								}
							});
						}
						else
						{
							var mergeUrl = context.mergeUrl.replace(/998877/g, context.originalRevision).replace(/778899/g, response.WikiPage.RevisionNumber);

							context.message.html(context.mergeMessage.replace(/{AuthorUrl}/g, response.WikiPage.User.ProfileUrl).replace(/{AuthorName}/g, response.WikiPage.User.DisplayName).replace(/{MergeUrl}/g, 'window.open(\'' + mergeUrl + '\',\'merge\',\'width=530,height=500,resizable=1,scrollbars=1\'); return false;')).show();

							context.save.html('<span></span>' + context.overwriteText);
							$('.processing', context.save.parent()).css("visibility", "hidden");
							context.save.removeClass('disabled');

							context.viewChanges.unbind('click');
							context.viewChanges.click(function()
							{
								window.open(mergeUrl, 'merge', 'width=530,height=500,resizable=1,scrollbars=1');
								return false;
							});
							context.viewChanges.show();

							context.originalRevision = response.WikiPage.RevisionNumber;

							$('html, body').animate({scrollTop: context.message.offset().top - 100}, 500);
						}
					},
					error: function(xhr, desc, ex)
					{
						$.telligent.evolution.notifications.show(desc,{type:'error'});
						$('.processing', context.save.parent()).css("visibility", "hidden");
						context.save.removeClass('disabled');
					}
				});
			}
			else
			{
				$.telligent.evolution.post({
					url: $.telligent.evolution.site.getBaseUrl() + 'api.ashx/v2/wikis/{WikiId}/pages.json?IncludeFields=WikiPage.Id,WikiPage.Url',
					data: data,
					success: function(response)
					{
						$.telligent.evolution.post({
							url: context.subscribeUrl,
							data: {wikiPageId: response.WikiPage.Id, subscribe: context.subscribe.is(':checked')},
							success: function(response2)
							{
								window.location = response.WikiPage.Url;
							},
							error: function(xhr, desc, ex)
							{
								$.telligent.evolution.notifications.show(desc,{type:'error'});
								$('.processing', context.save.parent()).css("visibility", "hidden");
								context.save.removeClass('disabled');
							}
						});
					},
					error: function(xhr, desc, ex)
					{
						$.telligent.evolution.notifications.show(desc,{type:'error'});
						$('.processing', context.save.parent()).css("visibility", "hidden");
						context.save.removeClass('disabled');
					}
				});
			}
		};

	$.telligent.evolution.widgets.pageEdit =
	{
	    register: function (context) 
        {
		    $('textarea').evolutionResize();

			if (context.tags)
			{
				context.tags.evolutionTagTextBox({allTags:context.allTags});
				if (context.selectTags)
				{
					if (context.allTags && context.allTags.length > 0)
						context.selectTags.click(function() { context.tags.evolutionTagTextBox('openTagSelector'); return false; });
					else
						context.selectTags.hide();
				}
			}

			context.title.evolutionComposer({
				plugins: ['hashtags']
			}).evolutionComposer('onkeydown', function(e) {
				if(e.which === 13) {
					return false;
				} else {
					return true;
				}
			});

			if (context.parent && context.lookupPagesUrl && context.noPageMatchesText)
			{
				var timeout = null;
				context.parent.glowLookUpTextBox(
				{
					'maxValues':1,
					'emptyHtml':'',
					'onGetLookUps':function(tb, searchText)
					{
						window.clearTimeout(timeout);
						if(searchText && searchText.length >= 2)
						{
							tb.glowLookUpTextBox('updateSuggestions', [tb.glowLookUpTextBox('createLookUp','','<div style="text-align: center;"><img src="' + $.telligent.evolution.site.getBaseUrl() + 'utility/spinner.gif" /></div>','<div style="text-align: center;"><img src="' + $.telligent.evolution.site.getBaseUrl() + 'utility/spinner.gif" /></div>', false)]);
							timeout = window.setTimeout(function()
							{
								$.telligent.evolution.get({
									url: context.lookupPagesUrl,
									data: {w_SearchText:searchText,w_ParentPageIdToExclude: (context.pageId ? context.pageId : -1)},
									success: function(response)
									{
										if (response && response.pages.length > 1)
										{
											var suggestions = [];
											for (var i = 0; i < response.pages.length - 1; i++)
												suggestions[suggestions.length] = tb.glowLookUpTextBox('createLookUp',response.pages[i].pageId,response.pages[i].title,response.pages[i].title,true);
											tb.glowLookUpTextBox('updateSuggestions', suggestions);
										}
										else
											tb.glowLookUpTextBox('updateSuggestions', [tb.glowLookUpTextBox('createLookUp','',context.noPageMatchesText,context.noPageMatchesText,false)]);
									}
								});
							}, 749);
						}
					},
					'selectedLookUpsHtml':(context.parentPageTitle ? [context.parentPageTitle] : [])
				});
			}

			context.save.click(function()
			{
				$(this).evolutionValidation('validate');
				if (!$(this).evolutionValidation('isValid')) {
					return;
				}

				submitClick(context);
			});
			$.telligent.evolution.navigationConfirmation.enable();
			$.telligent.evolution.navigationConfirmation.register(context.save);

			if (context.viewchanges)
				context.viewchanges.click(function() { showMerge(context); });

			if (context.featuredImage)
			{
				context.featuredImage.evolutionUserFileTextBox({initialPreviewHtml:context.featuredImagePreview});
				var fi = context.featureForm.css('display','none');
				var fc = context.feature.click(function(){fi.toggle();});
				if (fc.is(":checked"))
					fi.show("fast");
			}

			context.save.evolutionValidation(
			{
				validateOnLoad: context.pageId <= 0 ? false : null,
				onValidated: function(isValid, buttonClicked, c)
				{
					if (isValid)
						context.save.removeClass('disabled');
					else
						context.save.addClass('disabled');
				},
				onSuccessfulClick: function(e)
				{
					$('.processing', context.save.parent()).css("visibility", "visible");
					context.save.addClass('disabled');
				}
			});

			context.save.evolutionValidation('addField',context.titleSelector,
			{
				required: true,
				wikipageexists: { wikiId: context.wikiId, pageId: context.pageId, parentPageId: function(){return context.parent && context.parent.val() ? context.parent.val() : -1;} },
				messages:
				{
					required: context.titleRequiredText,
					wikipageexists: context.pageExistsText
				}
			}, '#' + context.wrapperId + ' .field-item.post-name .field-item-validation', null);
			context.save.evolutionValidation('validate');

			var f = context.save.evolutionValidation('addCustomValidation', 'pagetext', function()
			{
				return context.getBody().length > 0;
			},
			context.bodyRequiredText, '#' + context.wrapperId + ' .field-item.post-body .field-item-validation', null
			);

			context.attachBodyChangeHandler(f);
		}
	};
})(jQuery);

(function ($) {
    var deleteEmailOverrides = function(context, customerEmailOverrideIds, employeeEmailOverrideIds) {
            $.telligent.evolution.post({
                    url: context.deleteCustomersUrl,
                    data: {
                            selectedEmailOverrides: customerEmailOverrideIds.join()
                    },
                    success: function(response) {
			$.telligent.evolution.post({
				url: context.deleteEmployeesUrl,
				data: {
					selectedEmailOverrides: employeeEmailOverrideIds.join()
				},
				success: function(response) {
					//window.location.href = window.location.href;
					window.location.reload();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					context.errorDiv.html(jqXHR + textStatus + errorThrown);
					context.errorDiv.show();
				}
			});
                    },
		  error: function(jqXHR, textStatus, errorThrown) {
			context.errorDiv.html(jqXHR + textStatus + errorThrown);
			context.errorDiv.show();
		  }
             });

        },
        addEmailOverride = function(context, email, type) {
            $.telligent.evolution.post({
                    url: context.addUrl,
                    data: {
                            email: email,
                            emailOverrideType: type
                    },
                    success: function(response) {
		          //window.location.href = window.location.href;
		          window.location.reload();
                    },
		  error: function(jqXHR, textStatus, errorThrown) {
			context.errorDiv.html(jqXHR + textStatus + errorThrown);
			context.errorDiv.show();
		  }

            });
        };
    var api = {
//    $.widgets.listEmailOverridesWidget = {
        register: function (context) {
			context.wrapperId = $(context.wrapperId);
			context.errorDiv = $(context.errorDiv);
			$('a.addEmailOverride', context.wrapperId).bind('click', function(e,data){
				var email = $('input#email', context.wrapperId).val();
				var type = $('select#emailOverrideType', context.wrapperId).val();
				
                                    if(!type || !email || type.length == 0 || email.length == 0) {
                                            alert('You must enter an email address and select an email override type before you can add it to the list of overrides.');
                                    }
                                    else if(confirm(context.addEmailOverrideConfirmMessage)){
					addEmailOverride(context, email, type);
				}
				return false;
			});
			$('a.deleteEmailOverrides', context.wrapperId).bind('click', function(e,data){
				var customerEmailOverrides = new Array();
				var employeeEmailOverrides = new Array();
                                    $('input:checked', context.wrapperId).each(function() {
                                        if ($(this).attr('data-record-type') == 'Customer')
                                        {
                                            customerEmailOverrides.push($(this).val());
                                        } else {
                                            employeeEmailOverrides.push($(this).val());
                                        }
                                    });
				
                                    if((!customerEmailOverrides || customerEmailOverrides .length == 0) && (!employeeEmailOverrides || employeeEmailOverrides .length == 0)) {
                                            alert('You must select one or more email addresses, before you can delete them from the list of overrides.');
                                    }
			         else if(confirm(context.deleteConfirmMessage)){
					deleteEmailOverrides(context, customerEmailOverrides, employeeEmailOverrides);
				}
				return false;
			});

        }
    };
    
    if (typeof $.widgets == 'undefined') { $.widgets = {}; }
    $.widgets.listEmailOverridesWidget = api;

}(jQuery));
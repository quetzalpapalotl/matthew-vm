(function ($) {
    if (typeof $.omnicell === 'undefined') {
		$.omnicell = {};
	}
	if (typeof $.omnicell.widgets === 'undefined') {
		$.omnicell.widgets = {};
	}

	var _omniDocAttachHandlers = function (context) {
		$(context.fileUploadId).click(function () {
			$.glowModal(context.uploadFileUrl, {
				width : 500,
				height : 300,
				onClose : function (returnValue) {
					_omniDocModalClosed(context, returnValue);
				}
			});
			return false;
		});

		var selectTagsBox = $(context.selectTagsId).click(function () {
				$(context.tagBoxId).evolutionTagTextBox('openTagSelector');
				return false;
			});
		if (!context.tags || context.tags.length === 0) {
			selectTagsBox.hide();
		}

		$(context.featuredImageUrl).evolutionUserFileTextBox({
			removeText : context.removeResource,
			selectText : context.selectUploadResource,
			noFileText : context.noFileSelectedResource,
			initialPreviewHtml : context.featuredImagePreview
		});

		$(context.featuredPostId).click(function () {
			if (this.checked) {
				$(context.featuredImageId).show();
			} else {
				$(context.featuredImageId).hide();
			}
		});

		var saveButton = $(context.saveButton);

		$.telligent.evolution.navigationConfirmation.enable();
		$.telligent.evolution.navigationConfirmation.register(saveButton);
	},
	_omniDocAddValidation = function (context) {
		var saveButton = $(context.saveButton);

		saveButton.evolutionValidation({
			validateOnLoad : context.mediaId <= 0 ? false : null,
			onValidated : function (isValid, buttonClicked, c) {
				if (isValid) {
					saveButton.removeClass('disabled');
				} else {
					saveButton.addClass('disabled');
					var tabbedPane = $(context.tabId).glowTabbedPanes('getByIndex', 0);
					if (tabbedPane) {
						$(context.tabId).glowTabbedPanes('selected', tabbedPane);
					}
				}
			},
			onSuccessfulClick : function (e) {
				e.preventDefault();
				saveButton.parent().addClass('processing');
				saveButton.addClass('disabled');
				_omniDocSave(context);
			}
		});

		// File uploaded
		saveButton.evolutionValidation('addCustomValidation', 'mediafileuploaded', function () {
			return context.fileUploaded;
		},
			context.noFileUploadedError,
			context.wrapperId + ' .field-item.post-attachment .field-item-validation',
			null);

		// Has name
		saveButton.evolutionValidation('addField', context.postNameId, {
			required : true,
			messages : {
				required : context.postNameMissing
			}
		}, context.wrapperId + ' .field-item.post-name .field-item-validation', null);

		if ($(context.publishDateId).length > 0) {
			saveButton.evolutionValidation('addCustomValidation', 'validPublishDate', function () {
				var isValid = true;

				if ($.trim($(context.publishDateId).val()).length > 0) {
					isValid = /^((((0[13578])|([13578])|(1[02]))[\/](([1-9])|([0-2][0-9])|(3[01])))|(((0[469])|([469])|(11))[\/](([1-9])|([0-2][0-9])|(30)))|((2|02)[\/](([1-9])|([0-2][0-9]))))[\/]\d{4}$|^\d{4}$/.test($(context.publishDateId).val());
				}
				return isValid;
			}, context.publishDateErrorText, context.wrapperId + ' .field-item.publish-date .field-item-validation', null);
		}
	},
	_omniDocModalClosed = function (context, file) {
		if (file) {
			$(context.fileNameId).text(file.fileName);
			context.fileUploaded = true;
			context.file = file;
			$(context.saveButton).evolutionValidation('validateCustom', 'mediafileuploaded');
		}
	},
	_omniDocSave = function (context) {
		var data = _omniDocCreatePostRequestData(context);

		$.telligent.evolution.post({
			url : context.saveUrl,
			data : data,
			success : function (response) {
				if (response.redirectUrl) {
					window.location = response.redirectUrl;
				}
			},
			defaultErrorMessage : context.saveErrorText,
			error : function (xhr, desc, ex) {
				$.telligent.evolution.notifications.show(desc, {
					type : 'error'
				});
				$(context.saveButton).parent().removeClass('processing');
				$(context.saveButton).removeClass('disabled');
			}
		});
	},
	_omniDocCreatePostRequestData = function (context) {
		var inTags = $(context.tagBoxId).val().split(/[,;]/g);
		var tags = [];
		for (var i = 0; i < inTags.length; i++) {
			var tag = $.trim(inTags[i]);
			if (tag) {
				tags[tags.length] = tag;
			}
		}
		tags = tags.join(',');

		var selProducts = $(context.productListId + ' input:checked').map(function () {
				return $(this).val();
			}).get().join(',');
		var selAudiences = $(context.audienceListId + ' input:checked').map(function () {
				return $(this).val();
			}).get().join(',');

		var data = {
			Title : $(context.postNameId).val(),
			Body : context.getBody(),
			Tags : tags,
			GalleryId : context.galleryId,
			FileChanged : context.file ? '1' : '0',
			SelectedProductGroups : selProducts,
			SelectedAudiences : selAudiences
		};

		var documentType = $(context.documentTypeId);
		if (documentType.length > 0)
			data.DocumentTypeId = documentType.val();

		var publishDate = $(context.publishDateId);
		if (publishDate.length > 0 && publishDate.val().length > 0)
			data.PublishDate = publishDate.val();

		var subscribe = $(context.subscribeId);
		data.Subscribe = subscribe.length > 0 ? (subscribe.is(':checked') ? 1 : 0) : -1;

		var featured = $(context.featuredPostId);
		if (featured.length > 0) {
			data.Featured = featured.is(':checked') ? 1 : 0;
			var featuredImage = $(context.featuredImageUrl);
			if (featuredImage.length > 0)
				data.FeaturedImageUrl = featuredImage.val();
		} else
			data.Featured = -1;

		if (context.file) {
			if (context.file.isRemote) {
				data.FileName = context.file.fileName;
				data.FileUrl = context.file.url;
				data.FileIsRemote = '1';
			} else {
				data.FileName = context.file.fileName;
				data.FileContextId = context.file.contextId;
				data.FileIsRemote = '0';
			}
		}

		if (context.mediaId > 0) {
			data.Id = context.mediaId;
		}

		return data;
	},
	_omniDocPreview = function (context) {
		var data = _omniDocCreatePostRequestData(context);

		$(context.previewTabId).html('<div class="message loading loading__message">' + context.previewLoadingText + '</div>');

		$.telligent.evolution.post({
			url : context.previewUrl,
			data : data,
			success : function (response) {
				$(context.previewTabId).hide().html(response).fadeIn('fast');
			},
			defaultErrorMessage : context.previewErrorText,
			error : function (xhr, desc, ex) {
				$(context.previewTabId).html('<div class="message error error__message">' + desc + '</div>');
			}
		});
	};
	$.omnicell.widgets.documentPostUploadEdit = {
		register : function (context) {
			$(context.tabId).glowTabbedPanes({
				cssClass : 'tab-pane',
				tabSetCssClass : 'tab-set with-panes',
				tabCssClasses : ['tab'],
				tabSelectedCssClasses : ['tab selected'],
				tabHoverCssClasses : ['tab hover'],
				enableResizing : false,
				tabs :
				[
					[context.composeTabId, context.composeTabText, null],
					[context.previewTabId, context.previewTabText, function () {
							_omniDocPreview(context);
						}
					]
				]
			});

			$(context.tagBoxId).evolutionTagTextBox({
				allTags : context.tags
			});

			_omniDocAttachHandlers(context);
			_omniDocAddValidation(context);
		}
	};

})(jQuery);

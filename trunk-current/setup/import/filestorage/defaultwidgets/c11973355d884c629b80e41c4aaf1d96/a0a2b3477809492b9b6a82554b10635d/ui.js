(function($) {

	var unfavorite = function(context, contentUrl, favoriteType, favoriteItem) {
		$.telligent.evolution.post({
			url: context.unfavoriteUrl,
			data: {
				userId: context.userId,
				favoriteContentUrl: contentUrl,
				favoriteType: favoriteType
			},
			success: function(response) {
				favoriteItem.fadeOut(200, function(){
					favoriteItem.remove();
				});
			}
		});
	};

	var api = {
		register: function(context) {
			$(context.wrapperId + ' a.favorite-on').live('click', function(e,data){
				var favoriteItem = $(this).closest('li.content-item');
				var link = favoriteItem.find('a.view-post');
				var contentUrl = link.attr('href');
				var favoriteType = link.attr('favoritetype');
				unfavorite(context, contentUrl, favoriteType, favoriteItem);
				return false;
			});
		}
	};

	if (typeof $.telligent === 'undefined') { $.telligent = {}; }
	if (typeof $.telligent.evolution === 'undefined') { $.telligent.evolution = {}; }
	if (typeof $.telligent.evolution.widgets === 'undefined') { $.telligent.evolution.widgets = {}; }
	$.telligent.evolution.widgets.favorableContentList = api;

}(jQuery));

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

DECLARE @Major int, @Minor int, @Patch int, @Installed DateTime, @Prereqs int,@ComponentId uniqueidentifier

Set @Major = 1;
Set @Minor = 0;
Set @Patch = 2;
Set @ComponentId = '00000000-0000-0000-0000-000000000000';

Select @Prereqs = isnull(Count(InstallDate),0)  from [telligent_SchemaVersion] where Major=@Major and Minor=@Minor and ComponentId =  @ComponentId and Patch<@Patch


Select @Installed = InstallDate  from [telligent_SchemaVersion] where Major=@Major and Minor=@Minor and ComponentId =  @ComponentId  and Patch=@Patch

If(@Installed is null AND @Prereqs = @Patch)
	BEGIN
--## Schema Patch ##

exec te_Theme_AddUpdate @ThemeTypeId='0C647246-6735-42F9-875D-C8B991FE739B', @ThemeId='dd584bcc-f8fc-4f92-86c9-bc70ea5c9b85', @ThemeName=N'Omnicell75',@PreviewImageUrl=default,@Description=N''
exec te_Theme_AddUpdate @ThemeTypeId='A3B17AB0-AF5F-11DD-A350-1FCF55D89593', @ThemeId='dd584bcc-f8fc-4f92-86c9-bc70ea5c9b85', @ThemeName=N'Omnicell75',@PreviewImageUrl=default,@Description=N''
exec te_Theme_AddUpdate @ThemeTypeId='C6108064-AF65-11DD-B074-DE1A56D89593', @ThemeId='dd584bcc-f8fc-4f92-86c9-bc70ea5c9b85', @ThemeName=N'Omnicell75',@PreviewImageUrl=default,@Description=N''


--## END Schema Patch ##
Insert into [telligent_SchemaVersion](Major, Minor, Patch, InstallDate,ComponentId) values (@Major, @Minor, @Patch, GetDate(),@ComponentId)

Print 'Schema Patch v' + Convert(Varchar(2),@Major) + '.' + Convert(Varchar(2),@Minor) + '.' +  Convert(Varchar(3),@Patch) + '  was applied successfully For Component ' + COALESCE(Convert(varchar(128),@ComponentId),'BASE')

	END
ELSE IF(@Installed is not null)
	BEGIN
Print 'Schema Patch v' + Convert(Varchar(2),@Major) + '.' + Convert(Varchar(2),@Minor) + '.' +  Convert(Varchar(3),@Patch) + ' For Component ' + COALESCE(Convert(varchar(128),@ComponentId),'BASE') + ' was already applied on ' + Convert(varchar(50), @Installed)  
	END 
ELSE
	BEGIN
Print 'The patch could not be applied For Component ' + COALESCE(Convert(varchar(128),@ComponentId),'BASE') +' because your current schema is missing previous updates (Schema Patch v' + Convert(Varchar(2),@Major) + '.' + Convert(Varchar(2),@Minor) + '.' +  Convert(Varchar(3),@Patch) + ')' 
	END 



SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[telligent_Settings_Get]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop PROCEDURE [dbo].[telligent_Settings_Get]
GO

CREATE      PROCEDURE [dbo].telligent_Settings_Get
(
	@SettingsType		nvarchar(256)=null
)
AS
SELECT 
		SettingsXML
	FROM telligent_Settings 
		WHERE SettingsType=@SettingsType

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

grant execute on [dbo].[telligent_Settings_Get] to public